package pruebas;





import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.WebDriver;

import junit.framework.TestCase;
import utils.Log;
import utils.Resultados;
import utils.TestCaseCore;
import utils.TestProperties;
import utils.Utils;
import utils.Parallelized;

import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

@RunWith(value = Parallelized.class)
public class PendientesAgruparParametrizado extends TestCase {
	private String codigo = "GMM_08";
	private String pais;	
	private WebDriver driver;
	private String m_nombre = this.getClass().getSimpleName();
	private Resultados resultado;
		
	
	@Parameters
	public static Collection<String[]> data() {
		return Utils.devuelveDataCollection("paisesPruebas");	
	}

	@Before
	public void setUp() throws Exception {
		codigo = codigo+"_"+this.pais;
		m_nombre = m_nombre+"_"+this.pais;
		resultado = new Resultados(codigo);
		driver = Utils.inicializarCaso(driver, codigo, m_nombre);
		
		
		
	}

	public PendientesAgruparParametrizado(String pais) {
		this.pais = pais;

	}

	@Test
	public void testCase() throws Exception {
		try {
			
			Log.write("Prueba: "+pais);
			Log.write();
			Log.write("Prueba2: "+pais);
			
			int a = 2/0;
			Log.write("");
			
			throw new Exception ("as");

		} catch (Exception e) {
			utils.Log.writeException(e);
			resultado.appendError(e.getMessage());
		}
	}
	@After
	public void tearDown() throws Exception
	{
		Utils.guardarResultado(driver, resultado, codigo+"_"+pais, m_nombre);
	}
}
