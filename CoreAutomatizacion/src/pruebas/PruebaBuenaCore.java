package pruebas;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import utils.Funciones;
import utils.Log;
import utils.Resultados;
import utils.Utils;


public class PruebaBuenaCore extends TestCase
{
	private WebDriver driver;

	public String m_nombre = this.getClass().getSimpleName();

	public String m_codigo = "CPR_0092";

	Resultados resultado;



	@Before
	public void setUp() throws Exception
	{
		resultado = new Resultados(m_nombre);
		driver = Utils.inicializarCaso(driver, m_codigo, m_nombre);
	}


	@Test
	public void testModificarUsuarioFirmanteANoFirmante() throws Exception
	{

		try
		{
			
			
			
			Utils.parseDouble("5098.000524564");
			
			Utils.devuelvePosicionPrimerDigito("Min. 600.00 �");
			
			Utils.getOnlyDigitosPuntosComasFromString("600.00");
			

			Utils.loginPorDefecto(driver);
			
			Funciones.altaUsuarioPteActivar(driver, resultado);
						
			Log.write("HOLA QUE TAL");


			// Se establece el resultado de la prueba a OK
			Utils.setResultadoOK(resultado, "Alta de usuario con estado Pendiente de validar correcta");


		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			resultado.appendError(e.getMessage());

		}

	}


	@After
	public void tearDown() throws Exception
	{
		Utils.guardarResultado(driver, resultado, m_codigo, m_nombre);

	}
}