package pruebas;


import org.junit.Test;

import utils.Constantes;
import utils.Funciones;
import utils.Log;
import utils.TestCaseCore;
import utils.Utils;




public class PruebaInterface extends TestCaseCore
{
	
	public static String m_codigo = "CPR_159";


	public PruebaInterface() {
		super(m_codigo);
		
	}

	@Test
	public void testAltaUsuarioFirmanteCopiaOtro() throws Exception
	{

		try
		{
			
			new Constantes();
			
			driver.get("https://ei-portalintranet.es.igrupobbva/atpi_es_web/PortalLogon");
			
			Utils.loginUsuarioPasswordSinComprobar(driver, "20075224", "PRUEBA10", "qwerty23");
			
			Funciones.accederEntradaDeMenu(driver, "OT Pymes");
			
			Utils.adivinarNetworkError(driver);
			

		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			resultado.appendError(e.getMessage());
		}


	}

}
