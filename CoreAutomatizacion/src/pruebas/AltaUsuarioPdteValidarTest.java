package pruebas;


import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import utils.Funciones;
import utils.Resultados;
import utils.Utils;




public class AltaUsuarioPdteValidarTest extends TestCase
{
	private WebDriver driver;

	public String m_nombre = this.getClass().getSimpleName();

	public String m_codigo = "CPR_0014";

	private Resultados resultado;


	@Before
	public void setUp() throws Exception
	{
		resultado = new Resultados(m_nombre);
		driver = Utils.inicializarCaso(driver, m_codigo, m_nombre);
	}


	@Test
	public void testAltaUsuarioFirmanteCopiaOtro() throws Exception
	{

		try
		{

			Utils.capturaIntermedia(driver, resultado, "Antes de login");
			Utils.loginPorDefecto(driver);
			Utils.capturaIntermedia(driver, resultado, "Despu�s de login");
			
			Funciones.altaUsuarioPteActivar(driver, resultado);
			
			Utils.setResultadoOK(resultado, "Alta de usuario con estado Pendiente de activar");


		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			resultado.appendError(e.getMessage());
		}


	}


	@After
	public void tearDown() throws Exception
	{
		Utils.guardarResultado(driver, resultado, m_codigo, m_nombre);
	}
}
