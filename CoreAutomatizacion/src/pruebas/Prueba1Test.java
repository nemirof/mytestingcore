package pruebas;


import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import utils.Constantes;
import utils.Log;
import utils.Resultados;
import utils.Utils;




public class Prueba1Test extends TestCase
{
	private WebDriver driver;

	public String m_nombre = this.getClass().getSimpleName();

	public String m_codigo = "CPR_001";

	private Resultados resultado;


	@Before
	public void setUp() throws Exception
	{
		resultado = new Resultados(m_nombre);
		driver = Utils.inicializarCaso(driver, m_codigo, m_nombre);
	}


	@Test
	public void testAltaUsuarioFirmanteCopiaOtro() throws Exception
	{

		try
		{

			Utils.capturaIntermedia(driver, resultado, "Antes de Login");
			Utils.loginPorDefecto(driver);
			Utils.capturaIntermedia(driver, resultado, "Cositas cosotas");

//			// acceder a administración de usuarios
//			Utils.accederAdmUsuarios(driver);
//
//			// cambiar a frame
//			Utils.cambiarFramePrincipal(driver);
//			Utils.clickEnElVisiblePrimero(driver, Constantes.BotonDescargarPDF);
//			Utils.capturaIntermedia(driver, resultado, "PDF");
//			String nombreFichero = Utils.devuelveNombreFicheroDescargado(driver, "pdf");
//			
//			Log.write("Fichero:" + nombreFichero);
//			
//			boolean qpasa = Utils.checkPDFcontainsText(nombreFichero, "TC");
//			
//			Utils.capturaIntermedia(driver, resultado, "Cositas cosotas = "+qpasa);
			Utils.setResultadoOK(resultado, "Debuti");


		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			resultado.appendError(e.getMessage());
		}


	}


	@After
	public void tearDown() throws Exception
	{
		Utils.guardarResultado(driver, resultado, m_codigo, m_nombre);
	}
}
