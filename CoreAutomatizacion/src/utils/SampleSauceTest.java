package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.URL;

public class SampleSauceTest {

  public static final String USERNAME = "antonio.gomez.yunta";
  public static final String ACCESS_KEY = "05928dfa-b7c8-4ef4-8307-1eb8c6b3e758";
//  public static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";
  
  public static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.eu-central-1.saucelabs.com:443/wd/hub";
  public static void main(String[] args) throws Exception {

    DesiredCapabilities caps = DesiredCapabilities.chrome();
    caps.setCapability("platform", "Windows 10");
    caps.setCapability("version", "latest");
    caps.setCapability("tunnelIdentifier", "netcash2");
//    caps.setCapability("extendedDebug", true);

    WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
     // Goes to Sauce Lab&#39;s guinea-pig page and prints title
//    driver.get("https://eup-grupobbva2.igrupobbva/local_kyop/KYOPSolicitarCredenciales.html");
    driver.get("https://www.bbvanetcash.com/local_kyop/KYOPSolicitarCredenciales.html");

    System.out.println("title of page is: " + driver.getTitle());

    driver.quit();
  }
}