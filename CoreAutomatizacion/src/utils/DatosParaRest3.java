package utils;


import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


public class DatosParaRest3 extends TestCase
{
	private WebDriver driver;

	public String m_nombre = this.getClass().getSimpleName();

	public String m_codigo = "REST_DATOS";

	private Resultados resultado;


	@Before
	public void setUp() throws Exception
	{
		resultado = new Resultados(m_nombre);
		driver = Utils.inicializarEjecucion(driver, m_codigo, m_nombre);
	}


	@Test
	public void testDatosParaRest() throws Exception
	{

		UtilsRest.borrarFicheroRest();

		try
		{

			new Constantes();

			Utils.loginPorDefecto(driver);

			TestProperties params = new TestProperties(UtilsRest.getProperties());
			params.cargarPropiedades();

			TestProperties params1 = new TestProperties(Utils.getProperties());
			params1.cargarPropiedades();
			String entorno = params1.getValorPropiedad("entorno");

			if(entorno.equalsIgnoreCase("Produccion"))
			{
				Log.write("La prueba de los servicios REST s�lo aplica en Entornos de Desarrollo e Integrado");
			}

			else
			{

				try
				{
					// ADM APODERADO

					Usuario AdmApoderado5 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado5 == null)
					{
						AdmApoderado5 = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado5.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado5.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuario*****", AdmApoderado5.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario*****");
				}

				try
				{
					// ADM APODERADO

					Usuario AdmApoderado6 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado6 == null)
					{
						AdmApoderado6 = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado6.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado6.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuario******", AdmApoderado6.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario******");
				}

				try
				{
					// ADM APODERADO

					Usuario AdmApoderado7 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado7 == null)
					{
						AdmApoderado7 = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado7.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado7.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioValidador*", AdmApoderado7.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioValidador*");
				}

				try
				{
					// ADM APODERADO

					Usuario AdmApoderado7 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "NO", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado7 == null)
					{
						AdmApoderado7 = Funciones.altaAdministrador(driver, resultado, "Solidario-Indistinto");
					}
					else
					{
						AdmApoderado7.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado7.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioValidador****", AdmApoderado7.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioValidador****");
				}

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente2 = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente2.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente2.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "listaUsuariosAValidar", AdmPendiente2.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, listaUsuariosAValidar");
				}

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente2 = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente2.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente2.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "listaUsuariosAValidar***", AdmPendiente2.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, listaUsuariosAValidar***");
				}

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente2 = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente2.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente2.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "listaUsuariosAValidar****", AdmPendiente2.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario listaUsuariosAValidar****");
				}

			}


		}
		catch(Exception e)
		{

		}
	}


	@After
	public void tearDown() throws Exception
	{
		Utils.guardarEjecucion(driver, resultado);
	}
}
