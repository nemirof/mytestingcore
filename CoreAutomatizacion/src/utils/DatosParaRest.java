package utils;


import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class DatosParaRest extends TestCase
{
	private WebDriver driver;

	public String m_nombre = this.getClass().getSimpleName();

	public String m_codigo = "REST_DATOS";

	private Resultados resultado;


	@Before
	public void setUp() throws Exception
	{
		resultado = new Resultados(m_nombre);
		driver = Utils.inicializarEjecucion(driver, m_codigo, m_nombre);
	}


	@Test
	public void testDatosParaRest() throws Exception
	{

		UtilsRest.borrarFicheroRest();

		try
		{

			new Constantes();

			Utils.loginPorDefecto(driver);

			TestProperties params = new TestProperties(UtilsRest.getProperties());
			params.cargarPropiedades();

			TestProperties params1 = new TestProperties(Utils.getProperties());
			params1.cargarPropiedades();
			String entorno = params1.getValorPropiedad("entorno");
			String rutaCSV = params1.getValorPropiedad("rutaDatosFijos");

			if(entorno.equalsIgnoreCase("Produccion"))
			{
				Log.write("La prueba de los servicios REST s�lo aplica en Entornos de Desarrollo e Integrado");
			}

			else
			{

				// El primero, al no existir a�n el restEjecuci�n usa como base el properties/rest.properties, pero el resto ya va con el de ejecuci�n
				params.updateProperties("properties/rest.properties", "codEmpresa", params1.getValorPropiedad("referenciaPorDefecto"));

				// Todos tiran del de ejecuci�n
				params.updateProperties(UtilsRest.getProperties(), "referencia", params1.getValorPropiedad("referenciaPorDefecto"));
				params.updateProperties(UtilsRest.getProperties(), "ref", params1.getValorPropiedad("referenciaPorDefecto"));
				params.updateProperties(UtilsRest.getProperties(), "codUsuario************", params1.getValorPropiedad("usuarioPorDefecto"));
				params.updateProperties(UtilsRest.getProperties(), "user", params1.getValorPropiedad("usuarioPorDefecto"));
				params.updateProperties(UtilsRest.getProperties(), "codUsuarioValidador", params1.getValorPropiedad("usuarioPorDefecto"));
				params.updateProperties(UtilsRest.getProperties(), "codUsuarioAdmin******", params1.getValorPropiedad("usuarioPorDefecto"));

				// Obtenemos la clave del usuario por defecto
				try
				{
					Usuario UsuarioValidar = utils.Usuario
							.obtenerUsuarioPorReferenciaCodigo(rutaCSV, params1.getValorPropiedad("referenciaPorDefecto"), params1.getValorPropiedad("usuarioPorDefecto"));
					params.updateProperties(UtilsRest.getProperties(), "valorPrimerFactorVal", UsuarioValidar.getClaveOp());
				}
				catch(Exception e1)
				{
					Log.write("No se pudo obtener la clave de operaciones del usuario/referencia por defecto. valorPrimerFactorVal");
				}

				// valorPrimerFactorVal* Distinto seg�n pa�s
				if(Utils.estamosEnLatam())
					params.updateProperties(UtilsRest.getProperties(), "valorPrimerFactorVal*", "qwerty99");
				else
					params.updateProperties(UtilsRest.getProperties(), "valorPrimerFactorVal*", "claveop01");

				// Propios de contexto
				// Cambiar� por pa�s - de momento su valor
				// params.updateProperties(UtilsRest.getProperties(), "iv-origen", params1.getValorPropiedad("variable"));
				// params.updateProperties(UtilsRest.getProperties(), "iv-id_sesion_ast", params1.getValorPropiedad("variable"));

				params.updateProperties(UtilsRest.getProperties(), "iv-cod_canal", UtilsRest.anadeCerosParams(params.getValorPropiedad("codCanal")));
				params.updateProperties(UtilsRest.getProperties(), "iv-cod_ban_int", UtilsRest.anadeCerosParams(params.getValorPropiedad("codBancoInterno")));
				params.updateProperties(UtilsRest.getProperties(), "iv-cod_emp", params1.getValorPropiedad("referenciaPorDefecto"));
				params.updateProperties(
						UtilsRest.getProperties(),
						"iv-user",
						UtilsRest.anadeCerosParams(params.getValorPropiedad("codCanal")) + UtilsRest.anadeCerosParams(params.getValorPropiedad("codBancoInterno"))
								+ params1.getValorPropiedad("referenciaPorDefecto") + params1.getValorPropiedad("usuarioPorDefecto"));
				params.updateProperties(UtilsRest.getProperties(), "iv-cod_usu", params1.getValorPropiedad("usuarioPorDefecto"));


				try
				{
					// ADM APODERADO

					Usuario AdmApoderado = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado == null)
					{
						AdmApoderado = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioAdmin*****", AdmApoderado.getCodUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioAdmin*****");
				}


				try
				{
					// ADM APODERADO

					Usuario AdmApoderado = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado == null)
					{
						AdmApoderado = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuario", AdmApoderado.getCodUsuario());
					params.updateProperties(UtilsRest.getProperties(), "nomUsuario", AdmApoderado.getNombreUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario");
				}


				try
				{
					// ADM SIN PODERES

					Usuario AdmSinPoderes = Funciones.devuelveUsuarioPorTipo("Sin poderes", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmSinPoderes == null)
					{
						AdmSinPoderes = Funciones.altaAdministradorTipo(driver, resultado, "Sin poderes", "Apoderado");
					}
					else
					{
						AdmSinPoderes.setEstado(Constantes.ESTADO_NO_USAR);
						AdmSinPoderes.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioValidador*****", AdmSinPoderes.getCodUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioValidador*****");
				}


				try
				{
					// ADM Manc2

					Usuario AdmSinFirma = Funciones.altaAdministrador(driver, resultado, "Mancomunado 2");


					params.updateProperties(UtilsRest.getProperties(), "codUsuarioValidador***", AdmSinFirma.getCodUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioValidador***");
				}


				try
				{
					// ADM APODERADO

					Usuario AdmApoderado = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado == null)
					{
						AdmApoderado = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuario", AdmApoderado.getCodUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario");
				}


				try
				{
					// ADM APODERADO 2

					Usuario AdmApoderado2 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado2 == null)
					{
						AdmApoderado2 = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado2.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado2.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioAdmin*", AdmApoderado2.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioAdmin*");
				}

				try
				{
					// ADM APODERADO 3

					Usuario AdmApoderado3 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado3 == null)
					{
						AdmApoderado3 = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado3.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado3.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioAdmin*", AdmApoderado3.getCodUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioAdmin*");
				}

				try
				{
					// ADM APODERADO 3

					Usuario AdmApoderado3 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado3 == null)
					{
						AdmApoderado3 = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado3.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado3.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioAdmin***", AdmApoderado3.getCodUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioAdmin***");
				}

				try
				{
					// NO ADM APODERADO 3

					Usuario UsuarioNoAdmin = Funciones.devuelveUsuarioPorTipo("NO", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(UsuarioNoAdmin == null)
					{
						UsuarioNoAdmin = Funciones.altaUsuarioFirmanteSinTokenTodoContratado(driver, resultado);
					}
					else
					{
						UsuarioNoAdmin.setEstado(Constantes.ESTADO_NO_USAR);
						UsuarioNoAdmin.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioAdmin**", UsuarioNoAdmin.getCodUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioAdmin**");
				}

//		  try {
//				//ADM SIN PODERES
//
//				Usuario UsuarioNoAdmin = Funciones.altaAdministrador(driver, resultado, "NO");
//					  
//				params.updateProperties(UtilsRest.getProperties(), "codUsuarioValidador*****", UsuarioNoAdmin.getCodUsuario());
//			
//			  } catch (Exception e) {
//				  Log.write("Error creando un usuario, codUsuarioValidador*****");
//			  }
//		  

				try
				{
					// ADM APODERADO

					Usuario AdmApoderado4 = Funciones.altaUsuarioTodoContratado(driver, resultado);

					AdmApoderado4.setEstado(Constantes.ESTADO_NO_USAR);
					AdmApoderado4.guardarUsuarioEnCSV();


					params.updateProperties(UtilsRest.getProperties(), "codUsuario*", AdmApoderado4.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario*");
				}

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "codUsuario**", AdmPendiente.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario**");
				}

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioValidado", AdmPendiente.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioValidado");
				}

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioValidado*", AdmPendiente.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioValidado*");
				}

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioValidado**", AdmPendiente.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioValidado**");
				}

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "codUsuario**********", AdmPendiente.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario**********");
				}

				try
				{
					// ADM BLOQUEADO

					Usuario AdmBloqueado = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "NO", Constantes.ESTADO_BLOQ);

					if(AdmBloqueado == null)
					{
						AdmBloqueado = Funciones.bloquearUsuario(driver, resultado);
					}
					else
					{
						AdmBloqueado.setEstado(Constantes.ESTADO_NO_USAR);
						AdmBloqueado.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuario***", AdmBloqueado.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario***");
				}

				try
				{
					// ADM BLOQUEADO CON ACCIONES PENDIENTES

					Usuario AdmBloqueado = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "NO", Constantes.ESTADO_BLOQ);

					if(AdmBloqueado == null)
					{
						AdmBloqueado = Funciones.bloquearUsuario(driver, resultado);
					}
					else
					{
						AdmBloqueado.setEstado(Constantes.ESTADO_NO_USAR);
						AdmBloqueado.guardarUsuarioEnCSV();
					}

					Utils.accederAdmUsuarios(driver);
					// Cambiar a frame
					WebElement frame = driver.findElement(By.xpath(Constantes.FramePrincipal));
					driver.switchTo().frame(frame);
					driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
					driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(AdmBloqueado.getCodUsuario());
					Utils.clickEnElVisible(driver, Constantes.BotonBuscar);

					Utils.esperarVisibleTiempo(driver, Constantes.ResultadosBusqueda, 20);
					// Seleccionar usuario filtrado
					Utils.clickEnElVisible(driver, Constantes.RadioSeleccionUsuarioFiltrado);
					Utils.clickEnElVisible(driver, Constantes.BotonEditar);
					// Validar t�tulo de la p�gina
					String titulo = Utils.devuelveTextoDelVisiblePrimero(driver, Constantes.TituloPantalla);
					Utils.vpWarning(driver, resultado, "Validar t�tulo p�gina modificaci�n usuario", "Modificaci�n de usuario " + AdmBloqueado.getNombreUsuario(), titulo, "FALLO");

					// Modificar nombre de usuario
					String NuevoNomUsuario = AdmBloqueado.getNombreUsuario() + "�������";
					driver.findElement(By.xpath(Constantes.CampoNombreUsuario)).clear();
					driver.findElement(By.xpath(Constantes.CampoNombreUsuario)).sendKeys(NuevoNomUsuario);

					Utils.capturaIntermedia(driver, resultado, "Editando usuario");

					// Guardar
					Utils.scrollAndClick(driver, By.xpath(Constantes.BotonContinuarModificar));

					params.updateProperties(UtilsRest.getProperties(), "codUsuario***********", AdmBloqueado.getCodUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario***********");
				}

				try
				{
					// ADM BLOQUEADO 2

					Usuario AdmBloqueado2 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "NO", Constantes.ESTADO_BLOQ);

					if(AdmBloqueado2 == null)
					{
						AdmBloqueado2 = Funciones.bloquearUsuario(driver, resultado);
					}
					else
					{
						AdmBloqueado2.setEstado(Constantes.ESTADO_NO_USAR);
						AdmBloqueado2.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuario****", AdmBloqueado2.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario****");
				}

				try
				{
					// ADM APODERADO

					Usuario AdmApoderado5 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado5 == null)
					{
						AdmApoderado5 = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado5.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado5.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuario*****", AdmApoderado5.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario*****");
				}

				try
				{
					// ADM APODERADO

					Usuario AdmApoderado6 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado6 == null)
					{
						AdmApoderado6 = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado6.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado6.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuario******", AdmApoderado6.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario******");
				}

				try
				{
					// ADM APODERADO

					Usuario AdmApoderado7 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado7 == null)
					{
						AdmApoderado7 = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado7.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado7.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioValidador*", AdmApoderado7.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioValidador*");
				}

				try
				{
					// ADM APODERADO

					Usuario AdmApoderado7 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "NO", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado7 == null)
					{
						AdmApoderado7 = Funciones.altaAdministrador(driver, resultado, "Solidario-Indistinto");
					}
					else
					{
						AdmApoderado7.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado7.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioValidador****", AdmApoderado7.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioValidador****");
				}

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente2 = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente2.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente2.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "listaUsuariosAValidar", AdmPendiente2.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, listaUsuariosAValidar");
				}

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente2 = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente2.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente2.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "listaUsuariosAValidar***", AdmPendiente2.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, listaUsuariosAValidar***");
				}

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente2 = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente2.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente2.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "listaUsuariosAValidar****", AdmPendiente2.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario listaUsuariosAValidar****");
				}

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente2 = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente2.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente2.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "listaUsuariosAValidar*****", AdmPendiente2.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, listaUsuariosAValidar*****");
				}


				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente2 = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente2.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente2.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "listaUsuariosAValidar**", AdmPendiente2.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, listaUsuariosAValidar**");
				}

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente3 = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente3.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente3.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "codUsuario*********", AdmPendiente3.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario*********");
				}

				try
				{
					// ADM APODERADO SIN ACCIONES PENDIENTES

					Usuario AdmPendiente2 = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente2.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente2.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "listaUsuariosAValidar*", AdmPendiente2.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, listaUsuariosAValidar*");
				}

				try
				{
					// ADM APODERADO SIN ACCIONES PENDIENTES

					Usuario AdmApoderado8 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado8 == null)
					{
						AdmApoderado8 = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado8.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado8.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuario*******", AdmApoderado8.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario*******");
				}

				try
				{
					// ADM APODERADO SIN ACCIONES PENDIENTES

					Usuario AdmApoderado8 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado8 == null)
					{
						AdmApoderado8 = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado8.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado8.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuario********", AdmApoderado8.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario codUsuario********");
				}


				try
				{
					// USUARIO CON TOKEN
					Token token = Token.obtenerTokenPruebas(params1.getValorPropiedad("referenciaPorDefecto"));
					Funciones.liberarToken(driver, token, resultado);
					token.setTipo(Constantes.TIPO_TOKEN_FISICO);
					Usuario UsuarioToken = Funciones.altaUsuarioNoAdminTokenFisicoAsignado(driver, resultado, token);

					params1.updateProperties(UtilsRest.getProperties(), "codUsuario", UsuarioToken.getCodUsuario());
					params1.updateProperties(UtilsRest.getProperties(), "codUsuarioValidador**", UsuarioToken.getCodUsuario());
					params1.updateProperties(UtilsRest.getProperties(), "nuSerieToken", "VC-01-" + token.getNumero().replace(" ", "").replace("-", ""));
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario con token, codUsuarioValidador**");
				}

				try
				{
					// TOKEN SIN ASIGNAR

					Token token = Token.obtenerTokenPruebas(params1.getValorPropiedad("referenciaPorDefecto"));
					Token tokenLibre = Funciones.obtenerTokenLibre(driver, resultado, token, params1.getValorPropiedad("referenciaPorDefecto"));
					Funciones.liberarToken(driver, tokenLibre, resultado);
					token.setTipo(Constantes.TIPO_TOKEN_FISICO);

					params1.updateProperties(UtilsRest.getProperties(), "nuSerieToken*", "VC-01-" + tokenLibre.getNumero().replace(" ", "").replace("-", ""));
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, nuSerieToken*. No hay token libres disponibles");
				}

				try
				{

					driver.switchTo().defaultContent();

					// Acceder a Personalizar cuentas
					driver.findElement(By.xpath(Constantes.EntrarMiPerfil)).click();
					driver.findElement(By.xpath(Constantes.EntrarPersonalizarCuentas)).click();

					Utils.cambiarFramePrincipal(driver);

					boolean existenCuentas = Utils.isDisplayed(driver, Constantes.Cuenta, 10);

					String numeroAsunto = "";

					if(existenCuentas)
					{
						numeroAsunto = driver.findElement(By.xpath(Constantes.Cuenta)).getText();
					}
					else
					{
						Log.write("ERROR - No hay cuentas para marcar / desmarcar como favoritas, numeroAsunto");
					}

					params1.updateProperties(UtilsRest.getProperties(), "numeroAsunto", numeroAsunto);

				}
				catch(Exception e)
				{
					Log.write("Error intentando acceder a Editar perfil, numeroAsunto");
				}


			}


		}
		catch(Exception e)
		{

		}
	}


	@After
	public void tearDown() throws Exception
	{
		Utils.guardarEjecucion(driver, resultado);
	}
}
