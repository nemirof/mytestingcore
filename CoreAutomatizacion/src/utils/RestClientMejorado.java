package utils;


import java.io.FileReader;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.csvreader.CsvReader;


public class RestClientMejorado extends TestCase
{
	public String m_nombre = this.getClass().getSimpleName();

	public String m_codigo = "REST_001";


	@Before
	public void setUp() throws Exception
	{}


	@Test
	public void testRest() throws Exception
	{

		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();

		TestProperties params1 = new TestProperties(Utils.getProperties());
		params1.cargarPropiedades();

		String entorno = params1.getValorPropiedad("entorno");
		String entorno2 = params.getValorPropiedad("entorno");

		String cargarContexto = params.getValorPropiedad("cargarContexto");

		if(entorno.equalsIgnoreCase("Produccion"))
		{
			Log.write("La prueba de los servicios REST s�lo aplica en Entornos de Desarrollo e Integrado");
		}

		else
		{

			if(!entorno.equalsIgnoreCase(entorno2))
				Log.write("El entorno donde ejecutar los servicios REST no coincide con el indicado en la obtenci�n de Datos");

			else
			{
				String maquina = params.getValorPropiedad("maquina");
				String puerto = params.getValorPropiedad("puerto");
				String URL = "";

				if(maquina.toLowerCase().indexOf("http") == -1)
				{
					URL = "http://" + maquina + ":" + puerto + "/";
				}
				else
					URL = maquina + ":" + puerto + "/";


				String archivo = params.getValorPropiedad("rutaLanzamiento");
				String outputFile = archivo;

				CsvReader reader = new CsvReader(new FileReader(outputFile));
				reader.setDelimiter(';');

				// Algunas cosas iniciales
				String parametrosIniciales = UtilsRest
						.procesarParametros("codCanal&codBancoInterno&codEmpresa&codBancoProd&codProducto&codSubproducto&codUsuario************&numeroAsunto&marcarFavorita=false");
				UtilsRest.devuelveRestRequest(URL, "kygu_mult_web_serviciosusuario_01/services/rest/InformacionUsuarios/marcarDesmarcarCuentaFavorita/", parametrosIniciales, cargarContexto);

				parametrosIniciales = UtilsRest.procesarParametros("codEmpresa&codUsuario************&registroInicial=0&registroFinal=10&codIdiomaISO=ES");
				String alertas = UtilsRest.devuelveRestRequest(URL, "kynf_mult_web_servicios_02/services/rest/MensajeriaAlertas/obtenerListadoAlertas/", parametrosIniciales, cargarContexto);

				String alerta = UtilsRest.devuelveAlerta(alertas);
				if(!alerta.equalsIgnoreCase(""))
					alertas = alertas.replace("id_alerta\":\"" + alerta, "");
				params1.updateProperties(UtilsRest.getProperties(), "listaAlertas", alerta);

				alerta = UtilsRest.devuelveAlerta(alertas);
				if(!alerta.equalsIgnoreCase(""))
					alertas = alertas.replace("id_alerta\":\"" + alerta, "");
				params1.updateProperties(UtilsRest.getProperties(), "listaAlertas*", alerta);

				String tipoAlerta = UtilsRest.devuelveTipoAlerta(alertas);
				params1.updateProperties(UtilsRest.getProperties(), "id_tip_alerta", tipoAlerta);

				// Obtenemos alg�n mensaje
				parametrosIniciales = UtilsRest.procesarParametros("codUsuario************&referencia");
				String mensajes = UtilsRest.devuelveRestRequest(URL, "kynf_mult_web_servicios_02/services/rest/MensajeriaAlertas/obtenerListadoMensajes/", parametrosIniciales, cargarContexto);

				String mensaje = UtilsRest.devuelveMensaje(mensajes);
				if(!mensaje.equalsIgnoreCase(""))
					mensajes = mensajes.replace("messageId\":\"" + mensaje, "");
				params1.updateProperties(UtilsRest.getProperties(), "listaMensajes", mensaje);

				mensaje = UtilsRest.devuelveMensaje(mensajes);
				if(!mensaje.equalsIgnoreCase(""))
					mensajes = mensajes.replace("messageId\":\"" + mensaje, "");
				params1.updateProperties(UtilsRest.getProperties(), "ids", mensaje);

				// EMPEZAMOS LA EJECUCI�N

				reader.readHeaders();

				String contexto = "";

				while(reader.readRecord())
				{

					String lanzar = reader.get(5);

					if(!lanzar.equalsIgnoreCase("NO"))
					{

						String codigoCaso = reader.get(0);

						String codigoRetorno = reader.get(1);
						String resultadoEsperado = reader.get(2);
						String servicio = reader.get(3);
						String parametros = reader.get(4);

						parametros = UtilsRest.procesarParametros(parametros);

						UtilsRest.CsvAppend(params.getValorPropiedad("rutaResultados"), codigoCaso, UtilsRest.devuelveNombreServicio(servicio), "ERROR", "", "");

						String respuesta = UtilsRest.devuelveRestRequest(URL, servicio, parametros, cargarContexto);


						if(cargarContexto.equalsIgnoreCase("SI"))
							contexto = "iv-origen=" + UtilsRest.iv_origen + " iv-id_sesion_ast=" + UtilsRest.iv_id_sesion_ast + " iv-cod_canal=" + UtilsRest.iv_cod_canal + " iv-cod_ban_int="
									+ UtilsRest.iv_cod_ban_int + " iv-cod_emp=" + UtilsRest.iv_cod_emp + " iv-user=" + UtilsRest.iv_user + " iv-cod_usu=" + UtilsRest.iv_cod_usu;

						UtilsRest.crearFichero(codigoCaso, servicio, respuesta, URL, parametros, contexto);

						if(respuesta.toLowerCase().indexOf(codigoRetorno.toLowerCase()) > -1 && respuesta.toLowerCase().indexOf(resultadoEsperado.toLowerCase()) > -1)
						{
							UtilsRest.CsvAppend(params.getValorPropiedad("rutaResultados"), codigoCaso, UtilsRest.devuelveNombreServicio(servicio), "OK",
									"La respuesta contiene tanto el c�digo de retorno como la descripci�n esperada.", parametros);
						}
						else
							UtilsRest.CsvAppend(params.getValorPropiedad("rutaResultados"), codigoCaso, UtilsRest.devuelveNombreServicio(servicio), "ERROR",
									"La respuesta no contiene el c�digo de retorno o la descripci�n esperada.", parametros);
					}
				}
			}
		}
	}


	@After
	public void tearDown() throws Exception
	{}


}
