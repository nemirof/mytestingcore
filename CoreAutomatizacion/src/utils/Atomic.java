package utils;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Atomic
{
	
	
	
	public static List<WebElement> findElements (WebDriver driver, String xpath){
		List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
		return listaElementos;
	}
	
	public static WebElement findElement (WebDriver driver, String xpath) {
		return driver.findElement(By.xpath(xpath));
	}
	
	public static void click (WebDriver driver, String xpath) {
		driver.findElement(By.xpath(xpath)).click();
	}
	
	
	public static void scrollTo (WebDriver driver, WebElement elemento) {
		int elementPosition = elemento.getLocation().getY();
		String js = String.format("window.scroll(0, %s)", elementPosition);
		((JavascriptExecutor)driver).executeScript(js);
	}
	
	public static void moveToElement(WebDriver driver, WebElement elemento) {
		Actions actions = new Actions(driver);
		actions.moveToElement(elemento).perform();
	}
	
	public static void click (WebDriver driver, WebElement element) {
		Utils.clickJS(driver, element);
		//		element.click();
	}

}
