package utils;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import junit.framework.TestCase;

public abstract class TestCaseCore extends TestCase {
	
	protected WebDriver driver;

	protected String m_nombre = this.getClass().getSimpleName();

	protected Resultados resultado;
	
	//Debe sobrescribirse
	private String m_codigo = this.getClass().getSimpleName();
	
	public TestCaseCore (String codigo) {
		m_codigo = codigo;
	}
	
	@Override
	@Before
	public void setUp() throws Exception
	{
		resultado = new Resultados(m_nombre);
		driver = Utils.inicializarCaso(driver, m_codigo, m_nombre);
	}

	
	@Override
	@After
	public void tearDown() throws Exception
	{
		Utils.guardarResultado(driver, resultado, m_codigo, m_nombre);
	}
}
