package utils;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import utils.TestProperties;
import utils.Utils;
 
public class DespliegueYEntrega extends JFrame implements ActionListener, ChangeListener{
 
	private JTabbedPane pesta�as;

	//Paneles
	
	private JPanel panelDespliegue;
	private JPanel panelEntrega;
	
	//Despliegue
	
	private JLabel seleccionarUUAA;	
	private JRadioButton desplegarKYGU;
	private JRadioButton desplegarKYOP;
	private JRadioButton desplegarKYHD;
	
	private JLabel seleccionarRama;
	private JTextField ramaDespliegue;
	
	private JLabel seleccionarVersionActual;
	private JTextField versionActual;
	
	private JLabel seleccionarDestinatarios;
	private JTextField destinatariosCorreo;
	
	private JButton ejecutarDespliegue;
	
	
	//Entregas
	
	private JLabel introduceVersionActualEntrega;
	private JTextField versionActualEntrega;
	
	private JLabel introduceVersionNuevaEntrega;
	private JTextField versionNuevaEntrega;
	
	private JLabel introduceRamas;
	
	private JLabel introduceRamaPaseKYGU;
	private JTextField ramaPaseKYGU;
	
	private JLabel introduceRamaPaseKYOP;
	private JTextField RamaPaseKYOP;
		
	private JLabel introduceRamaPaseKYHD;
	private JTextField RamaPaseKYHD;
	
	private JLabel seleccionarDestinatariosEntrega;
	private JTextField destinatariosCorreoEntrega;
	
	private JButton ejecutarEntrega;
	
    public DespliegueYEntrega(){
 
    	Color color = new java.awt.Color(240,248,255);
    	int cont=10;
 	
        //Parametros asociados a la ventana;
        setVisible(true);
 
        //Creamos el conjunto de pesta�as
        pesta�as=new JTabbedPane();
 
        
 ////PANEL DESPLIEGUE/////////////////////////////////////////////////       
        
        //Creamos el panel y lo a�adimos a las pesta�as
        panelDespliegue=new JPanel();
                
        panelDespliegue.setLayout(null);
        panelDespliegue.setBackground(color);
 
        //A�adimos un nombre de la pesta�a y el panel
        pesta�as.addTab("DESPLIEGUE", panelDespliegue);
 
        
     //Seleccionar UUAA
        
        seleccionarUUAA=new JLabel("1. Seleccione la UUAA a desplegar:");
        seleccionarUUAA.setSize(900, 900);
        seleccionarUUAA.setBounds(10,cont,400,30);
        panelDespliegue.add(seleccionarUUAA);
        cont+=30;
               
        desplegarKYGU=new JRadioButton("KYGU");
        desplegarKYGU.setSelected(true);
        desplegarKYGU.setBounds(10, cont, 60, 30);
        desplegarKYGU.setBackground(color);
               
        desplegarKYOP=new JRadioButton("KYOP");
        desplegarKYOP.setSelected(false);
        desplegarKYOP.setBounds(70+10, cont, 60, 30);
        desplegarKYOP.setBackground(color);
        
        desplegarKYHD=new JRadioButton("KYHD");
        desplegarKYHD.setSelected(false);
        desplegarKYHD.setBounds(70+70+10, cont, 60, 30);
        desplegarKYHD.setBackground(color);
        cont+=40;
        
        ButtonGroup grupoBotonesEntorno=new ButtonGroup();
        grupoBotonesEntorno.add(desplegarKYGU);
        grupoBotonesEntorno.add(desplegarKYOP);
        grupoBotonesEntorno.add(desplegarKYHD);
        
        panelDespliegue.add(desplegarKYGU);
        panelDespliegue.add(desplegarKYOP);
        panelDespliegue.add(desplegarKYHD);
        
        
     //Seleccionar Rama
        
        seleccionarRama=new JLabel("2. Introduzca la rama:");
        seleccionarRama.setSize(900, 900);
        seleccionarRama.setBounds(10,cont,400,30);
        panelDespliegue.add(seleccionarRama);
        cont+=30;
        
        ramaDespliegue=new JTextField();
        ramaDespliegue.setSize(900, 900);
        ramaDespliegue.setBounds(10, cont, 400, 25);
        panelDespliegue.add(ramaDespliegue);
        cont+=40;
        
        
      //Seleccionar Versi�n actual
        
        seleccionarVersionActual=new JLabel("3. Introduzca la versi�n actual:");
        seleccionarVersionActual.setSize(900,900);
        seleccionarVersionActual.setBounds(10, cont, 400, 30);
        panelDespliegue.add(seleccionarVersionActual);
        cont+=30;
              
        versionActual=new JTextField();
        versionActual.setSize(900, 900);
        versionActual.setBounds(10, cont, 400, 25);
        panelDespliegue.add(versionActual);
        cont+=40;
        
      //Introducir destinatarios correo
        
        seleccionarDestinatarios=new JLabel("4. Enviar correo de confirmaci�n a las siguientes direcciones:");
        seleccionarDestinatarios.setSize(900, 900);
        seleccionarDestinatarios.setBounds(10, cont, 400, 30);
        panelDespliegue.add(seleccionarDestinatarios);
        cont+=30;
        
        destinatariosCorreo=new JTextField();
        destinatariosCorreo.setSize(900, 900);
        destinatariosCorreo.setBounds(10, cont, 400, 25);
        panelDespliegue.add(destinatariosCorreo);
        cont+=40;
        
      //Bot�n ejecutar
        
        cont+=20;
        ejecutarDespliegue=new JButton("Ejecutar");
        ejecutarDespliegue.setSize(900, 900);
        ejecutarDespliegue.setBounds(310, cont, 100, 30);
        //ejecutarDespliegue.setEnabled(false);
        ejecutarDespliegue.addActionListener(this);
        panelDespliegue.add(ejecutarDespliegue);
       
        
        
        
        
        
////PANEL ENTREGA
 
       //Segundo JPanel para entrega
        panelEntrega=new JPanel();
        panelEntrega.setLayout(null);
        panelEntrega.setBackground(color);
        pesta�as.addTab("ENTREGA", panelEntrega);
        int contEntrega=10;
     
        
      //Introducir versi�n actual
        
    	introduceVersionActualEntrega=new JLabel("1. Introduce versi�n actual:");
    	introduceVersionActualEntrega.setSize(900, 900);
    	introduceVersionActualEntrega.setBounds(10, contEntrega, 400, 30);
        panelEntrega.add(introduceVersionActualEntrega);
        contEntrega+=30;
        
        versionActualEntrega=new JTextField();
        versionActualEntrega.setSize(900, 900);
        versionActualEntrega.setBounds(10, contEntrega, 400, 25);
        panelEntrega.add(versionActualEntrega);
        contEntrega+=40;
        
    	
      //Introducir nueva versi�n
        
        introduceVersionNuevaEntrega=new JLabel("2. Introduce nueva versi�n:");
        introduceVersionNuevaEntrega.setSize(900, 900);
        introduceVersionNuevaEntrega.setBounds(10, contEntrega, 400, 30);
    	panelEntrega.add(introduceVersionNuevaEntrega);
        contEntrega+=30;
        
        versionNuevaEntrega=new JTextField();
        versionNuevaEntrega.setSize(900, 900);
        versionNuevaEntrega.setBounds(10, contEntrega, 400, 25);
        panelEntrega.add(versionNuevaEntrega);
        contEntrega+=40;
    	
        
      //Introducir Ramas del pase
        
    	introduceRamas=new JLabel("3. Introduce las ramas del pase:");
    	introduceRamas.setSize(900, 900);
    	introduceRamas.setBounds(10, contEntrega, 400, 30);
    	panelEntrega.add(introduceRamas);
        contEntrega+=30;
        
        //KYGU
    	introduceRamaPaseKYGU=new JLabel("KYGU:");
    	introduceRamaPaseKYGU.setSize(900, 900);
    	introduceRamaPaseKYGU.setBounds(20, contEntrega, 400, 30);
    	panelEntrega.add(introduceRamaPaseKYGU);
    	
        ramaPaseKYGU=new JTextField();
        ramaPaseKYGU.setSize(900, 900);
        ramaPaseKYGU.setBounds(70, contEntrega+5, 300, 20);
        panelEntrega.add(ramaPaseKYGU);
    	
        contEntrega+=30;
        
        //KYOP
    	introduceRamaPaseKYOP=new JLabel("KYOP:");
    	introduceRamaPaseKYOP.setSize(900, 900);
    	introduceRamaPaseKYOP.setBounds(20, contEntrega, 400, 30);
    	panelEntrega.add(introduceRamaPaseKYOP);
        
        RamaPaseKYOP=new JTextField();
        RamaPaseKYOP.setSize(900, 900);
        RamaPaseKYOP.setBounds(70, contEntrega+5, 300, 20);
        panelEntrega.add(RamaPaseKYOP);
    	
        contEntrega+=30;
        
          
        //KYHD
    	introduceRamaPaseKYHD=new JLabel("KYHD:");
    	introduceRamaPaseKYHD.setSize(900, 900);
    	introduceRamaPaseKYHD.setBounds(20, contEntrega, 400, 30);
    	panelEntrega.add(introduceRamaPaseKYHD);


    	RamaPaseKYHD=new JTextField();
    	RamaPaseKYHD.setSize(900, 900);
    	RamaPaseKYHD.setBounds(70, contEntrega+5, 300, 20);
        panelEntrega.add(RamaPaseKYHD);
        contEntrega+=40;
        
      //Seleccionar destinatarios
        
        seleccionarDestinatariosEntrega=new JLabel("4. Enviar correo de confirmaci�n a las siguientes direcciones:");
        seleccionarDestinatariosEntrega.setSize(900, 900);
        seleccionarDestinatariosEntrega.setBounds(10, contEntrega, 400, 30);
        panelEntrega.add(seleccionarDestinatariosEntrega);
        contEntrega+=30;
        
        destinatariosCorreoEntrega=new JTextField();
        destinatariosCorreoEntrega.setSize(900, 900);
        destinatariosCorreoEntrega.setBounds(10, contEntrega, 400, 25);
        panelEntrega.add(destinatariosCorreoEntrega);
        contEntrega+=40;
        
      //Bot�n ejecutar
        
        contEntrega+=20;
        ejecutarEntrega=new JButton("Ejecutar");
        ejecutarEntrega.setSize(900, 900);
        ejecutarEntrega.setBounds(310, contEntrega, 100, 30);
        ejecutarEntrega.setEnabled(true);
        ejecutarEntrega.addActionListener(this);
        panelEntrega.add(ejecutarEntrega);
         
    	
    	//A�adimos pesta�as
        getContentPane().add(pesta�as);
    }
    
    
    public void stateChanged(ChangeEvent e) {
    	
//    	if(desplegarKYHD.isSelected())
//    		ejecutarDespliegue.setEnabled(true);
    	
    	
//    	boolean todoCompleto=false;
//    	
//    	if(ramaDespliegue.getText().equalsIgnoreCase("") || versionActual.getText().equalsIgnoreCase("")){
//    		todoCompleto=false;
//    	}
//    	else{
//    		todoCompleto=true;
//    	}
//    	
//    	
//    	if(todoCompleto){
//    		ejecutarDespliegue.setEnabled(false);
//    	}
//    	else{
//    		ejecutarDespliegue.setEnabled(true);
//    	}
    	    	
    }
    
    public void actionPerformed(ActionEvent e) {
    				
		try {
			if (e.getSource()==ejecutarDespliegue){
				
				if (ramaDespliegue.getText().isEmpty() ||
						versionActual.getText().isEmpty()){
						
						JOptionPane.showMessageDialog(this, "Indica el nombre de la rama y la versi�n actual para realizar el despliegue");
					}
				else{
				TestProperties params = new TestProperties(Utils.getProperties());
				params.cargarPropiedades();
				
				//Modificamos properties con los valores introducidos
				
				if (desplegarKYGU.isSelected()){
					params.updateSameProperties(Utils.getProperties(), "uuaa", desplegarKYGU.getText());
				}
				else if(desplegarKYOP.isSelected()){
					params.updateSameProperties(Utils.getProperties(), "uuaa", desplegarKYOP.getText());
				}
				else if(desplegarKYHD.isSelected()){
					params.updateSameProperties(Utils.getProperties(), "uuaa", desplegarKYHD.getText());
				}
				params.updateSameProperties(Utils.getProperties(), "nombreRama", ramaDespliegue.getText());
				params.updateSameProperties(Utils.getProperties(), "versionActual", versionActual.getText()+"L_Snapshot");
				params.updateSameProperties(Utils.getProperties(), "destinatarios", destinatariosCorreo.getText());
				
				Process p = Runtime.getRuntime().exec("cmd /c start matarProcesosChromeDriver.bat");
				p.waitFor();
				
				System.exit(0);
				}
				
			}
			
			
			if(e.getSource()==ejecutarEntrega){
				if (versionActualEntrega.getText().isEmpty() ||
					versionNuevaEntrega.getText().isEmpty() ||
					RamaPaseKYOP.getText().isEmpty() ||
					ramaPaseKYGU.getText().isEmpty() ||
					RamaPaseKYHD.getText().isEmpty() ){
					
					JOptionPane.showMessageDialog(this, "Completa los campos de las versiones y las ramas para realizar la entrega");
				}
				else{
				TestProperties params = new TestProperties(Utils.getProperties());
				params.cargarPropiedades();
				
				//Modificamos properties con los valores introducidos
				params.updateSameProperties(Utils.getProperties(), "versionActual", versionActualEntrega.getText()+"L");
				params.updateSameProperties(Utils.getProperties(), "versionNueva", versionNuevaEntrega.getText());
				params.updateSameProperties(Utils.getProperties(), "ramaEntregaKYOP", RamaPaseKYOP.getText());
				params.updateSameProperties(Utils.getProperties(), "ramaEntregaKYGU", ramaPaseKYGU.getText());
				params.updateSameProperties(Utils.getProperties(), "ramaEntregaKYHD", RamaPaseKYHD.getText());
				params.updateSameProperties(Utils.getProperties(), "destinatarios", destinatariosCorreoEntrega.getText());
				
				//Ejecutamos
				Process p = Runtime.getRuntime().exec("cmd /c start matarProcesosChromeDriver.bat");
				p.waitFor();
				
				System.exit(0);
				}
			}
			
		} catch (HeadlessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
    	
    	
    }
 
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
 
        DespliegueYEntrega ventana=new DespliegueYEntrega();
        
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());    

        UIManager.put("Button.disabledText", Color.gray);
        
        ventana.setAlwaysOnTop(true);
        ventana.setTitle("Herramienta Despliegues y Entregas");
        ventana.setResizable(true);
        ventana.setSize(450, 500);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        ventana.setLocation(dim.width/2-ventana.getSize().width/2, dim.height/2-ventana.getSize().height/2);
        ventana.setVisible(true);

        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
    }


 
}