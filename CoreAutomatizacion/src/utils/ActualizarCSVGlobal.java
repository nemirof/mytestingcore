package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import junit.framework.TestCase;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import utils.Resultados;
import utils.TestProperties;
import utils.Utils;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;


public class ActualizarCSVGlobal extends TestCase
{
	public String m_nombre = this.getClass().getSimpleName();

	public String m_codigo = "UTI_ACT";

	int contador = 0;

	boolean cadenaValida = false;

	String codigoFicheroHTML = "";

	String codigoFicheroCSV = "";

	public String codigoFicheroC204 = "";


	@Before
	public void setUp() throws Exception
	{
		new Resultados(m_nombre);
	}


	@Test
	public void testActualizarCSVGlobal() throws Exception
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		
		String esperaCSVGlobal = params.getValorPropiedad("esperaCSVGlobal");

		//Podr�a ni existir la propiedad
		if (esperaCSVGlobal==null) 
			esperaCSVGlobal = "";
		
		
		
		String nombreTempAns= System.getProperty("user.dir");//"Y:/ANS/tempANS";

		
		try
		{
			
			
			
			
		//Borramos la carpeta temp si existe	
			File directorio=new File(nombreTempAns); 
			
//			if (directorio.exists()) 
//			{		
//				Log.write("Directorio donde guardamos resultado global existe. Lo eliminamos:" + directorio.getAbsolutePath());
//
//				FileUtils.deleteDirectory(directorio);								
//			}
			
			
	
		//Buscamos todos los directorios
			
			File child = new File (System.getProperty("user.dir"));
			
			
			File file = new File (child.getParentFile().getAbsolutePath()); 
			Log.write("Directorio donde buscamos ejecuciones:" + file.getAbsolutePath());
			
			String[] directories = file.list(new FilenameFilter() { 
				
				public boolean accept(File current, String name) { 
				return new File(current, name).isDirectory(); 
			} 
			});
			
		//Creamos directorio temp
			directorio.mkdir(); 
		
		//Creamos dentro de temp, las carpetas de screenshots y results
			File directorioScreenshots = new File(nombreTempAns+"/screenshots"); 
//			directorioScreenshots.mkdir(); 
			
			File directorioResults = new File(nombreTempAns+"/results"); 
//			directorioResults.mkdir();
			
			File directorioLanzamiento = new File(nombreTempAns+"/lanzamiento"); 
			

			File rutaActual = new File (System.getProperty("user.dir"));	
			
			
			String horaActual = "";
			String horaMinima = "23:59:59";
			DateFormat datos = new SimpleDateFormat("HH:mm:ss");
			Date minima = datos.parse(horaMinima);
			
			
			
			//Posible espera global				
			if (esperaCSVGlobal.equalsIgnoreCase("SI")){
				
				String[] extensions = new String[] {"running"};
			
				for (int dir=0; dir<directories.length; dir++){
					
					if (!rutaActual.getName().equalsIgnoreCase(directories[dir])){
														
					   Log.write("Pillamos la carpeta: "+directories[dir]);
					   
					   int cont = 0;
					   
					   File carpetaResources = new File(rutaActual.getParentFile().getAbsolutePath()+ "/" +directories[dir]+"/resources");

					   if (carpetaResources.exists()) {
					   
						    List<File> files = (List<File>) FileUtils.listFiles(carpetaResources, extensions, true);
							
						    while (files.size()>0 && cont<=7200) {
						    	Log.write("Waiting to: "+ directories[dir] + " to finish...");
						    	Thread.sleep(1000);
						    	files = (List<File>) FileUtils.listFiles(carpetaResources, extensions, true);
						    	cont++;
						    }
						   
						    
						    if (cont>=7200)
						    	Log.write("FALLO : Se supera el timeOut para que termine el DIST: " + directories[dir]);
		
						   
					   }
						
					}
				        
				}
			}
			
			
	//Recorremos cada uno de los dist para actualizar los datos			
	for (int dir=0; dir<directories.length;dir++){
		
		if (!rutaActual.getName().equalsIgnoreCase(directories[dir])){
											
		   Log.write("Pillamos la carpeta: "+directories[dir]);
		   
		   File carpetaResultados = new File (rutaActual.getParentFile().getAbsolutePath()+ "/" +directories[dir]+"/results");

		   if (carpetaResultados.exists()) {
		   
			    //Guardamos las carpetas de screenshots y results de Origen
			   
				
				File origenScreenshots = new File(rutaActual.getParentFile().getAbsolutePath()+ "/" +directories[dir]+"/screenshots"); 
				File origenResults = new File(rutaActual.getParentFile().getAbsolutePath()+ "/" +directories[dir]+"/results"); 
				File origenLanzamiento = new File(rutaActual.getParentFile().getAbsolutePath()+ "/" +directories[dir]+"/lanzamiento"); 
	
			   
				//File origenScreenshots = new File(directories[dir]+"/screenshots");
				
				//File origenResults = new File(directories[dir]+"/results");
				
	
				//Copiamos ficheros de la carpeta Origen screenshots a la carpeta temporal, lo mismo con results
				Log.write("Origen screenshots: "+ origenScreenshots.getAbsolutePath());
				Log.write("Origen results: "+ origenResults.getAbsolutePath());
				
				horaActual = Utils.devuelveDatosDeEjecucionPasandoArchivo("Hora inicio",origenResults.getAbsolutePath()+"/ResumenEjecucionPruebas.html");
				Date actual = datos.parse(horaActual);
				
				if (actual.compareTo(minima)<0){
					minima = actual;
				}
	
				if (origenScreenshots.exists()){
					
					FileUtils.copyDirectory(origenScreenshots, directorioScreenshots);
				}
				if (origenResults.exists()){
					FileUtils.copyDirectory(origenResults, directorioResults);
				}
				
				if (origenLanzamiento.exists()){
					FileUtils.copyDirectory(origenLanzamiento, directorioLanzamiento);
				}
				
				
	
				File archivoResults = new File (rutaActual.getParentFile().getAbsolutePath()+ "/" +directories[dir]+"/results/results.csv");
				
				if (archivoResults.exists()){
					rellenaCSVGeneral(archivoResults.getAbsolutePath(), nombreTempAns+"/results/resultsGeneral.csv");
					Log.write("Origen result CSV: "+ archivoResults.getAbsolutePath());
					Log.write("Destino result CSV: "+  nombreTempAns+"/results/resultsGeneral.csv");
				}
				
		   }
		}
	        
	}
	
//	File fileOLD = new File(nombreTempAns+"/results/resultsGeneral.csv");
//
//	// File (or directory) with new name
//	File fileNEW = new File(nombreTempAns+"/results/results.csv");
//
//	if (fileNEW.exists()){
//	   fileNEW.delete();
//	}
//	// Rename file (or directory)
//	fileOLD.renameTo(fileNEW);

	
	
	
	
	
	
		
	        
	      	Log.write("FIN");  
	      
			java.util.Date hoy = new java.util.Date();
			SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
			String horaInicio = formatter2.format(minima);
			
			java.util.Date fin = new java.util.Date();
			String horaFin = formatter2.format(fin);
			
			
			Utils.CSVToTableNew(hoy, horaInicio, horaFin, null);
	

		
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
			e.printStackTrace();
		}

	}
	
	
	
	


	@After
	public void tearDown() throws Exception
	{
//	  Utils.guardarEjecucion(driver,resultado);
	}
	
	
    public static void rellenaCSVGeneral (String csvOrigen, String csvDestino) throws Exception {
        
		CsvReader csv = new CsvReader(new FileReader(csvOrigen));
	    csv.readHeaders();

	    
	     while (csv.readRecord()){

				String id = csv.get(0);
				String testName = csv.get(1);
				String resultado = csv.get(2);
				String comentario = csv.get(3);
				String captura = csv.get(4);
				
				Utils.CsvAppend(csvDestino, id, testName, resultado, comentario, captura);
	    	 
	    	 
	      }

	     csv.close();
		
    }
	
	
}
