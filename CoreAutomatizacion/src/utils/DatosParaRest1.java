package utils;


import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


public class DatosParaRest1 extends TestCase
{
	private WebDriver driver;

	public String m_nombre = this.getClass().getSimpleName();

	public String m_codigo = "REST_DATOS1";

	private Resultados resultado;


	@Before
	public void setUp() throws Exception
	{
		resultado = new Resultados(m_nombre);
		driver = Utils.inicializarEjecucion(driver, m_codigo, m_nombre);
	}


	@Test
	public void testDatosParaRest() throws Exception
	{

		UtilsRest.borrarFicheroRest();

		try
		{

			new Constantes();

			Utils.loginPorDefecto(driver);

			TestProperties params = new TestProperties(UtilsRest.getProperties());
			params.cargarPropiedades();

			TestProperties params1 = new TestProperties(Utils.getProperties());
			params1.cargarPropiedades();
			String entorno = params1.getValorPropiedad("entorno");

			if(entorno.equalsIgnoreCase("Produccion"))
			{
				Log.write("La prueba de los servicios REST s�lo aplica en Entornos de Desarrollo e Integrado");
			}

			else
			{

				try
				{

					// MIRAR PORQ NO COPIA EL RESTO BIEN
					params.updateProperties("properties/rest.properties", "codEmpresa", params1.getValorPropiedad("referenciaPorDefecto"));
					params.updateProperties(UtilsRest.getProperties(), "codUsuarioValidador", params1.getValorPropiedad("usuarioPorDefecto"));


					// ADM APODERADO

					Usuario AdmApoderado = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado == null)
					{
						AdmApoderado = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioAdmin*****", AdmApoderado.getCodUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioAdmin*****");
				}


				try
				{
					// ADM APODERADO

					Usuario AdmApoderado = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado == null)
					{
						AdmApoderado = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuario", AdmApoderado.getCodUsuario());
					params.updateProperties(UtilsRest.getProperties(), "nomUsuario", AdmApoderado.getNombreUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario");
				}


				try
				{
					// ADM SIN PODERES

					Usuario AdmSinPoderes = Funciones.devuelveUsuarioPorTipo("Sin poderes", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmSinPoderes == null)
					{
						AdmSinPoderes = Funciones.altaAdministradorTipo(driver, resultado, "Sin poderes", "Apoderado");
					}
					else
					{
						AdmSinPoderes.setEstado(Constantes.ESTADO_NO_USAR);
						AdmSinPoderes.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioValidador*****", AdmSinPoderes.getCodUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioValidador*****");
				}


				try
				{
					// ADM Manc2

					Usuario AdmSinFirma = Funciones.altaAdministrador(driver, resultado, "Mancomunado 2");


					params.updateProperties(UtilsRest.getProperties(), "codUsuarioValidador***", AdmSinFirma.getCodUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioValidador***");
				}


				try
				{
					// ADM APODERADO

					Usuario AdmApoderado = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado == null)
					{
						AdmApoderado = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuario", AdmApoderado.getCodUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario");
				}


				try
				{
					// ADM APODERADO 2

					Usuario AdmApoderado2 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado2 == null)
					{
						AdmApoderado2 = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado2.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado2.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioAdmin*", AdmApoderado2.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioAdmin*");
				}

				try
				{
					// ADM APODERADO 3

					Usuario AdmApoderado3 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado3 == null)
					{
						AdmApoderado3 = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado3.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado3.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioAdmin*", AdmApoderado3.getCodUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioAdmin*");
				}

			}


		}
		catch(Exception e)
		{

		}
	}


	@After
	public void tearDown() throws Exception
	{
		Utils.guardarEjecucion(driver, resultado);
	}
}
