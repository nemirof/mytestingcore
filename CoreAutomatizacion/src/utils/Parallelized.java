package utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.runners.Parameterized;
import org.junit.runners.model.RunnerScheduler;

public class Parallelized extends Parameterized {
	private static class ThreadPoolScheduler implements RunnerScheduler {
		private ExecutorService executor;

		public ThreadPoolScheduler () {
			
			TestProperties params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();
			String hilosParalelosMismoCaso = params.getValorPropiedad("hilosParalelosMismoCaso");
			
			if (hilosParalelosMismoCaso==null)
				hilosParalelosMismoCaso = "1";
			
			String threads = System.getProperty("junit.parallel.threads", hilosParalelosMismoCaso);
			int numThreads = Integer.parseInt(threads);
			executor = Executors.newFixedThreadPool(numThreads);
		}
		public void schedule(Runnable childStatement) {
			  executor.submit(childStatement);
		}

		public void finished() {
			   executor.shutdown();
	            try
	            {
	                executor.awaitTermination(3, TimeUnit.MINUTES);
	            }
	            catch (InterruptedException exc)
	            {
	                throw new RuntimeException(exc);
	            }

		}

	}

	public Parallelized(Class<?> klass) throws Throwable {
		super(klass);
		setScheduler(new ThreadPoolScheduler());
	}

}
