package utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Log {
private Logger logger;
private FileHandler fileHandler; 

public Log(String m_nombre) throws Exception {
	this.logger = Logger.getLogger(Long.toString(Thread.currentThread().getId()));
	fileHandler = new FileHandler("results/" + m_nombre + ".html", true); 
	fileHandler.setFormatter(new MyCustomFormatter());	
	logger.addHandler(fileHandler);
	logger.setLevel(Level.ALL);
	logger.setUseParentHandlers(false);
	
}

public void log(String texto) {
	logger.log(Level.INFO, texto);
}

public static void write(Object... text) {
	
	//Mejor inicializarlo a vac�o, pues el MyFormatter ya le mete un salto de l�nea
	String texto = "";
	
	if (text.length!=0)
		texto=text[0].toString();

	Logger.getLogger(Long.toString(Thread.currentThread().getId())).log(Level.INFO,texto);
}

public static void writeException(Exception e) {
	  StringWriter sWriter = new StringWriter();
      PrintWriter pWriter = new PrintWriter(sWriter);
      e.printStackTrace(pWriter);
      Log.write(sWriter.toString());
}

public void cerrarLog() {
	this.fileHandler.close();
	this.logger.removeHandler(fileHandler);
}

public static void cerrarTodosLog() {
	Handler[] arrayHandler = Logger.getLogger(Long.toString(Thread.currentThread().getId())).getHandlers();
	
	for (int i = 0; i < arrayHandler.length; i++) {
		arrayHandler[i].close();
		Logger.getLogger(Long.toString(Thread.currentThread().getId())).removeHandler(arrayHandler[i]);
	}
	
}

}