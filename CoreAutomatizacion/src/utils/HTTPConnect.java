package utils;


import java.net.*;
import java.io.*;


public class HTTPConnect
{

	public static void main(String args[])
	{

		Utils.setBBVAProxy();

		try
		{
			BufferedReader in = new BufferedReader(new InputStreamReader(openURLForInput(new URL("http://clouditarvi.ddns.net"))));
			String line;
			while((line = in.readLine()) != null)
			{
				System.out.println(line);
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}


	public static boolean comprobarRespuestaHTML(String url, String contenido) throws Exception
	{

		
		boolean resul = false;
		try
		{
			BufferedReader in = new BufferedReader(new InputStreamReader(openURLForInput(new URL(url))));
			String line;
			while((line = in.readLine()) != null && !resul)
			{
				if(line.toLowerCase().indexOf(contenido.toLowerCase()) > -1)
					resul = true;
			}
		}
		catch(IOException e)
		{
			System.out.println("INFO: "+ e.getMessage());

		}
		return resul;
	}

	public static Proxy setProxy () {
		
		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Constantes.proxy_hostname, Constantes.proxy_port));
		
		Authenticator authenticator = new Authenticator() {

	        public PasswordAuthentication getPasswordAuthentication() {
	            return (new PasswordAuthentication(Constantes.proxy_user, Utils.devuelveClaveCifrada().toCharArray()));
	        }
	    };
	    Authenticator.setDefault(authenticator);
	    
	    return proxy;
	}

	public static InputStream openURLForInput(URL url) throws IOException
	{

		// NOTA: Subida manual usr:selenium pass:qwerty99
		URLConnection conn;
		
		// Establecemos el proxy
		if (Utils.insideBBVA()) {
			Proxy proxy = setProxy();
			conn = url.openConnection(proxy);
		}
		
		else {
			conn = url.openConnection();
			conn.setDoInput(true);
		}
		
		
		
//    conn.setRequestProperty ("Authorization",
//       userNamePasswordBase64(uname,pword));
		conn.connect();
		return conn.getInputStream();
	}


	public static String userNamePasswordBase64(String username, String password)
	{
		return "Basic " + base64Encode(username + ":" + password);
	}

	private final static char base64Array[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
			'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};


	private static String base64Encode(String string)
	{
		String encodedString = "";
		byte bytes[] = string.getBytes();
		int i = 0;
		int pad = 0;
		while(i < bytes.length)
		{
			byte b1 = bytes[i++];
			byte b2;
			byte b3;
			if(i >= bytes.length)
			{
				b2 = 0;
				b3 = 0;
				pad = 2;
			}
			else
			{
				b2 = bytes[i++];
				if(i >= bytes.length)
				{
					b3 = 0;
					pad = 1;
				}
				else
					b3 = bytes[i++];
			}
			byte c1 = (byte)(b1 >> 2);
			byte c2 = (byte)(((b1 & 0x3) << 4) | (b2 >> 4));
			byte c3 = (byte)(((b2 & 0xf) << 2) | (b3 >> 6));
			byte c4 = (byte)(b3 & 0x3f);
			encodedString += base64Array[c1];
			encodedString += base64Array[c2];
			switch (pad)
			{
				case 0:
					encodedString += base64Array[c3];
					encodedString += base64Array[c4];
					break;
				case 1:
					encodedString += base64Array[c3];
					encodedString += "=";
					break;
				case 2:
					encodedString += "==";
					break;
			}
		}
		return encodedString;
	}
}
