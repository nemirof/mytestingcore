package utils;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.ProgressMonitor;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.commons.io.FileUtils;


public class SeleccionarUUAALanzamiento extends JFrame implements ActionListener, ChangeListener{
 
	private JLabel label1;
	private JLabel labelDestino;
	private JLabel labelCopiando;
	private JLabel copiarFichero;
	private JLabel informeResultante;
    private JButton boton1;
    private JButton guardar;
	private JRadioButton eiButton;
	private JRadioButton chromeButton;
	private JRadioButton firefoxButton;
	private JRadioButton ieButton;
	private JRadioButton proButton;
	private JRadioButton unicoInforme;
	private JRadioButton variosInformes;
	private JRadioButton tipoANS;
	private JRadioButton tipoCompletas;
	private JRadioButton tipoRapidas;
	static ProgressMonitor monitor;
	static int progress;
	static Timer timer;

    
	private JFileChooser chooser;
	 
    
	File child = new File (System.getProperty("user.dir"));
	
	File file = new File(child.getParentFile().getAbsolutePath()); 
	String[] directories = file.list(new FilenameFilter() { 
		
		public boolean accept(File current, String name) { 
		return new File(current, name).isDirectory(); 
	} 
	});
	
	JCheckBox[] boxes = new JCheckBox[directories.length];
	JCheckBox[] boxScheduler = new JCheckBox[1];
	JCheckBox[] boxGrid = new JCheckBox[1];
	JCheckBox[] boxGalatea = new JCheckBox[1];
	JPanel pane = new JPanel();
	private JLabel labelEntorno;
	private JLabel labelNavegador;
	private JLabel labelMails;	
	private JTextField textfieldMails;
	private JTextField textFieldGrid;
	private JTextField textfieldHilos;
	private JLabel labelHilos;	
	private JLabel labelTipoEjecucion;


    
    public SeleccionarUUAALanzamiento() throws IOException {
    	
//    	Object object= client.getForObject("http://15.30.170.147:4444"+"/grid/api/proxy?id="+ipaddress[i], Object.class);
    	
    	Color color = new java.awt.Color(240,248,255);
    	int cont=10;
    	   	    	
        setLayout(null);
        getContentPane().setBackground(color);
        
        labelTipoEjecucion=new JLabel("1 - Seleccione el tipo de ejecuci�n:");
        labelTipoEjecucion.setSize(900, 900);
        labelTipoEjecucion.setBounds(10,cont,400,30);
        add(labelTipoEjecucion);
        cont+=30;
        
        tipoANS = new JRadioButton("M�nimas");
        tipoANS.setSelected(true);
        tipoANS.setBounds(10,cont,100,30);
        tipoANS.setBackground(color);

        tipoCompletas = new JRadioButton("Completas");
        tipoCompletas.setBounds(100+10,cont,100,30);
        tipoCompletas.setBackground(color);

        
        tipoRapidas = new JRadioButton("R�pidas");
        tipoRapidas.setBounds(240,cont,100,30);
        tipoRapidas.setBackground(color);
        //cont+=40;

        // Group the radio buttons.
        ButtonGroup group = new ButtonGroup();
        group.add(tipoANS);
        group.add(tipoCompletas);
        group.add(tipoRapidas);

        this.add(tipoANS);
        this.add(tipoCompletas);
        this.add(tipoRapidas);

        
        boxGrid[0] = new JCheckBox();
        boxGrid[0].setText("Lanzar en GRID");
        boxGrid[0].setName("grid");
        boxGrid[0].setSelected(false);
        boxGrid[0].setVisible(true);
        boxGrid[0].setBackground(color);
        boxGrid[0].setBounds(410,cont,150,30);
        boxGrid[0].addChangeListener(this);
        add(boxGrid[0]);  
        
        textFieldGrid=new JTextField();
        textFieldGrid.setText("http://15.30.170.147:4444/wd/hub"); //http://15.30.170.147:4444/wd/hub
        textFieldGrid.setEnabled(false);
        textFieldGrid.setBounds(413,cont+30,200,30);
        add(textFieldGrid);
        
        boxGalatea[0] = new JCheckBox();
        boxGalatea[0].setText("Galatea");
        boxGalatea[0].setName("galatea");
        boxGalatea[0].setSelected(false);
        boxGalatea[0].setVisible(true);
        boxGalatea[0].setBackground(color);
        boxGalatea[0].setBounds(410,cont+60,150,30);
        boxGalatea[0].addChangeListener(this);
        add(boxGalatea[0]);  
        
        cont+=40;
        

        labelEntorno=new JLabel("2 - Seleccione el entorno:");
        labelEntorno.setSize(900, 900);
        labelEntorno.setBounds(10,cont,400,30);
        add(labelEntorno);
        cont+=30;
        
        eiButton = new JRadioButton("Integrado");
        eiButton.setSelected(true);
        eiButton.setBounds(10,cont,100,30);
        eiButton.setBackground(color);

        proButton = new JRadioButton("Producci�n");
        proButton.setBounds(100+10,cont,100,30);
        proButton.setBackground(color);
        cont+=40;

        // Group the radio buttons.
        ButtonGroup group2 = new ButtonGroup();
        group2.add(eiButton);
        group2.add(proButton);

        this.add(eiButton);
        this.add(proButton);
        
        
//        labelNavegador=new JLabel("2 - Seleccione navegador:");
//        labelNavegador.setSize(900, 900);
//        labelNavegador.setBounds(10,cont,400,30);
//        add(labelNavegador);
//        cont+=30;
//        
//        chromeButton = new JRadioButton("Chrome");
//        chromeButton.setSelected(true);
//        chromeButton.setBounds(10,cont,100,30);
//        chromeButton.setBackground(color);
//
//        firefoxButton = new JRadioButton("Firefox");
//        firefoxButton.setBounds(100+10,cont,100,30);
//        firefoxButton.setBackground(color);
//        cont+=40;
//
//        // Group the radio buttons.
//        ButtonGroup groupNavegador = new ButtonGroup();
//        groupNavegador.add(chromeButton);
//        groupNavegador.add(firefoxButton);
//
//        this.add(chromeButton);
//        this.add(firefoxButton);     
        
        labelMails=new JLabel("3 - Introduzca los mails a los que desea enviar el resumen:");
        labelMails.setSize(900, 900);
        labelMails.setBounds(10,cont,400,30);
        add(labelMails);
        cont+=30;
        textfieldMails=new JTextField();
        textfieldMails.setBounds(10,cont,400,30);
        add(textfieldMails);
        cont+=40;
        
        
        labelHilos=new JLabel("4 - �Cu�ntos hilos simult�neos?");
        labelHilos.setSize(900, 900);
        labelHilos.setBounds(10,cont,400,30);
        add(labelHilos);
        cont+=30;
        textfieldHilos=new JTextField("4");
        textfieldHilos.setColumns(2);
        textfieldHilos.setBounds(10,cont,30,30);
        add(textfieldHilos);
        
        textfieldHilos.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
              char c = e.getKeyChar();
              
              if (textfieldHilos.getText().length() >= 2 ) // limit to 3 characters
                  e.consume();
              
              if (!(Character.isDigit(c) ||
                 (c == KeyEvent.VK_BACK_SPACE) ||
                 (c == KeyEvent.VK_DELETE))) {
                   e.consume();
                 }
            }
          });
        
        cont = cont-30;
        
        labelNavegador=new JLabel("5 - Seleccione el navegador:");
        labelNavegador.setSize(900, 900);
        labelNavegador.setBounds(410,cont,400,30);
        add(labelNavegador);
        cont+=30;
        
        chromeButton = new JRadioButton("Chrome");
        chromeButton.setSelected(true);
        chromeButton.setBounds(410,cont,70,30);
        chromeButton.setBackground(color);

        firefoxButton = new JRadioButton("Firefox");
        firefoxButton.setBounds(480,cont,70,30);
        firefoxButton.setBackground(color);
        
        ieButton = new JRadioButton("IE10");
        ieButton.setBounds(550,cont,70,30);
        ieButton.setBackground(color);

        // Group the radio buttons.
        ButtonGroup groupBrowser = new ButtonGroup();
        groupBrowser.add(chromeButton);
        groupBrowser.add(firefoxButton);
        groupBrowser.add(ieButton);

        this.add(chromeButton);
        this.add(firefoxButton);
        this.add(ieButton);

        

        AutoSuggestor autoSuggestor = new AutoSuggestor(textfieldMails, this, null, Color.WHITE.brighter(), Color.BLACK, Color.RED, 0.85f) {
            @Override
            boolean wordTyped(String typedWord) {

                //create list for dictionary this in your case might be done via calling a method which queries db and returns results as arraylist
                ArrayList<String> words = new ArrayList<>();
                words.add("versionadocash@bbva.com");
                words.add("arquitectura-informacional.group@bbva.com");
                words.add("sabrina.bozzi.contractor@bbva.com");
                words.add("josemanuel.mancha.contractor@bbva.com");
                words.add("victorjose.fornas.contractor@bbva.com");
                words.add("antonioj.gomez@softtek.com");
                words.add("ivan.minsut.contractor@bbva.com");
                words.add("gestion-digital.group@bbva.com");    
                words.add("gestion-digital-2.group@bbva.com");
                words.add("pedro.redondo.gonzalez.contractor@bbva.com");
                words.add("monserrat.sanchez.sanchez.contractor@bbva.com");
                words.add("antonio.gomez.yunta.contractor@bbva.com");
                words.add("josevicente.moreno.contractor@bbva.com");
                words.add("indigo-administracin-de-usuarios-tse.group@bbva.com");
                words.add("dca-scrum.group@bbva.com");
                words.add("belen.gomez@bbva.com");

                setDictionary(words);
                //addToDictionary("bye");//adds a single word

                return super.wordTyped(typedWord);//now call super to check for any matches against newest dictionary
            }
        };
        
        cont+=40;
                
        label1=new JLabel("6 - Seleccione las aplicaciones que desea lanzar:");
        label1.setSize(900, 900);
        label1.setBounds(10,cont,400,30);
        add(label1);
        cont+=30;
       
        int altura = 0;      
        int columna = 0;
        
        for(int dir=0; dir<directories.length;dir++){
        	
        	if (!directories[dir].contains("LANZADOR_GLOBAL") && !directories[dir].contains(".git")) {
        		        		 
        		 altura = altura +35;
	        	 boxes[dir] = new JCheckBox(directories[dir]);
	        	 boxes[dir].setText(directories[dir]);
	             boxes[dir].setName(directories[dir]);
	             boxes[dir].setSelected(false);
	             boxes[dir].setVisible(true);
	             boxes[dir].setBackground(color);
	             
	             
	             if (columna % 3 == 0)
	            	 boxes[dir].setBounds(10,cont,200,30);
	             else if (columna % 3 == 1) {
	            	 boxes[dir].setBounds(210,cont,200,30);  
	             } 
	             else {
	            	 boxes[dir].setBounds(410,cont,200,30);
	            	 cont=cont+30; 
	             }
	             
	             //Para ponerlos en columnas.
//	             if (columna % 2 == 0)
//	            	 boxes[dir].setBounds(10,cont,200,30);
//	             else {
//	            	 boxes[dir].setBounds(210,cont,200,30);
//	            	 cont=cont+30;   
//	             }
	             //
	             
	             columna++;
	             boxes[dir].addChangeListener(this);
	             add(boxes[dir]);  
	             
	             setBounds(0,0,410,200+altura);
	//             pack();
	             
        	  
        	}
        }
        
        //Ajuste por si sale una fila mas
        if (columna % 3 == 0)
        	cont+=10;
        else
        	cont+=40;
        
        informeResultante=new JLabel("7 - �C�mo desea el informe resultante?");
        informeResultante.setSize(900, 900);
        informeResultante.setBounds(10,cont,400,30);
        add(informeResultante);
        cont+=30;
        
        unicoInforme = new JRadioButton("Un �nico informe para todas las aplicaciones seleccionadas");
        unicoInforme.setSelected(true);
        unicoInforme.setBounds(10,cont,400,30);
        unicoInforme.setBackground(color);

        cont+=30;
        variosInformes = new JRadioButton("Un informe por cada aplicaci�n seleccionada");
        variosInformes.setBounds(10,cont,400,30);
        variosInformes.setBackground(color);
        cont+=40;

        // Group the radio buttons.
        ButtonGroup group3 = new ButtonGroup();
        group3.add(unicoInforme);
        group3.add(variosInformes);
        
        this.add(unicoInforme);
        this.add(variosInformes);
        
        
        cont+=10;
        copiarFichero=new JLabel("8 - Seleccione la ruta donde se guardar�n los resultados:");
        copiarFichero.setSize(900, 900);
        copiarFichero.setBounds(10,cont,400,30);
        add(copiarFichero);
        cont+=35;
 

        guardar = new JButton("Examinar");
        guardar.setBounds(10, cont, 90, 30);
        guardar.addActionListener(this);
        add(guardar);
        cont=cont+30;
        
       labelDestino=new JLabel("Ruta: ");
       labelDestino.setSize(900, 900);
       labelDestino.setBounds(20,cont,400,30);
       labelDestino.setVisible(false);
       add(labelDestino);
       cont=cont+30;
       
       labelCopiando=new JLabel("Copiando archivos necesarios...");
       labelCopiando.setSize(900, 900);
       labelCopiando.setBounds(20,cont,400,30);
       labelCopiando.setVisible(false);
       add(labelCopiando);
       cont=cont+30;
       
       altura = cont+120;
       
       setBounds(0,0,640,altura);
       
       boxScheduler[0] = new JCheckBox();
       boxScheduler[0].setText("�Desea programar esta ejecuci�n?");
       boxScheduler[0].setName("schedule");
       boxScheduler[0].setSelected(false);
       boxScheduler[0].setVisible(true);
       boxScheduler[0].setBackground(color);
       boxScheduler[0].setBounds(10,cont+10,250,40);
       boxScheduler[0].addChangeListener(this);
       add(boxScheduler[0]);  
        

        boton1 = new JButton();

        boton1.setText("Ejecutar!");
        boton1.setForeground(Color.BLACK);
        boton1.setVerticalTextPosition(SwingConstants.BOTTOM);
        boton1.setHorizontalTextPosition(SwingConstants.CENTER);
        boton1.setBounds(500,cont+10,110,40);
        boton1.setHorizontalAlignment(SwingConstants.CENTER);
        add(boton1);
        boton1.addActionListener(this);
        boton1.setEnabled(false);
               
        
        

//        progressBar = new JProgressBar(0, 100);
//        progressBar.setValue(0);
//
//        progressBar.setStringPainted(true);
//        Border border = BorderFactory.createTitledBorder("Copiando ficheros necesarios...");
//        progressBar.setBorder(border);
//        progressBar.setBounds(10,cont,400,50);
//        progressBar.setVisible(true);
//        add(progressBar);
        
       

        
        
        
    }

    public int numeroChecksSeleccionados () {
    	int numeroSeleccionados = 0;
    	for(int dir=0; dir<directories.length;dir++){
   		 
    		if (!directories[dir].contains("LANZADOR_GLOBAL") && !directories[dir].contains(".git")) {
				 
    			if (boxes[dir].isSelected())
					 numeroSeleccionados++;
					 
				 }
   	 	}
    	
    	return numeroSeleccionados;
    }
    
    public void stateChanged(ChangeEvent e) {
    	
    	boolean todosFalse = true;
    	
    	if (boxGrid[0].isSelected()){
    		textFieldGrid.setEnabled(true);
    		textfieldHilos.setEnabled(false);
    	}
    	
    	if (!boxGrid[0].isSelected()){
    		textFieldGrid.setEnabled(false);
    		textfieldHilos.setEnabled(true);
    	}
    	
    	if (boxGalatea[0].isSelected()){
    		boxGrid[0].setSelected(true);
    		textFieldGrid.setEnabled(true);
//    		textFieldGrid.setText("https://a0c15c936c9aa11e9ae9306568a96ab9-d081d9726e806af8.elb.eu-west-1.amazonaws.com/galatea/wd/hub");
    		textFieldGrid.setText("https://globaldevtools.bbva.com/galatea/wd/hub");
    		
    	}
    	
    	 for(int dir=0; dir<directories.length;dir++){
    		 if (!directories[dir].contains("LANZADOR_GLOBAL") && !directories[dir].contains(".git")) {
				 if (boxes[dir].isSelected())
					 todosFalse = false;
					 
				 }
    	 }
    	 
   	     	 
    	 if (todosFalse)
    		 boton1.setEnabled(false);
    	 else{
	    	 if (labelDestino.isVisible())
	       		 boton1.setEnabled(true);
	       	 }
    	 
    	 
         

    	 
    }
    
    public void actionPerformed1(ActionEvent e) {
    	if (e.getSource()==boton1) {
    		labelCopiando.setVisible(true);
    	}
    }
    

    public void actionPerformed(ActionEvent e) {
        

    	try {
    		
    		if (e.getSource()==guardar) {
    			
    			
    		
    		 chooser = new JFileChooser(); 
    		 chooser.setCurrentDirectory(new File(System.getProperty("user.home")));
    		 chooser.setDialogTitle("Elige la carpeta donde desea guardar los resultados");
    		 chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    		    //
    		    // disable the "All files" option.
    		    //
    		 chooser.setAcceptAllFileFilterUsed(false);
    		    //    
    		 if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) { 
    		     labelDestino.setVisible(true); 
    		     labelDestino.setText("Ruta: " 
    		     +  chooser.getSelectedFile());  		        
    		      }
    		 else {
    		     System.out.println("No Selection ");
    		      }

    		}
    		
    		boolean todosFalse = true;
        	
	       	 for(int dir=0; dir<directories.length;dir++){
	       		 if (!directories[dir].contains("LANZADOR_GLOBAL") && !directories[dir].contains(".git")) {
	   				 if (boxes[dir].isSelected())
	   					 todosFalse = false;
	   					 
	   				 }
	       	 }

      	 
	       	 if (todosFalse)
	       		 boton1.setEnabled(false);
	       	 else{
	       		 if (labelDestino.isVisible())
	       			 boton1.setEnabled(true);
	       		
	       	 }
	       	 
	       	
	
			if (e.getSource()==boton1) {
							
				labelCopiando.setVisible(true);
				
				JOptionPane.showMessageDialog(this, "Antes de la ejecuci�n, se proceder� al copiado de los archivos necesarios en: \n" + chooser.getSelectedFile().getAbsolutePath()+ "\n");
				
				setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

				TestProperties params = new TestProperties(Utils.getProperties());
				params.cargarPropiedades();
				
				params.updateSameProperties(Utils.getProperties(), "destinatarios", textfieldMails.getText());
				
				
				if (tipoANS.isSelected()){
					params.updateSameProperties(Utils.getProperties(), "tipoEjecucion", "ANS");
				}
				
				else if (tipoCompletas.isSelected()){
					params.updateSameProperties(Utils.getProperties(), "tipoEjecucion", "");
				}
				
				File rutaActual = new File (System.getProperty("user.dir"));
				
				String ficheroPropertiesOLD = "";
				String ficheroLanzamiento = "";
				
				if (eiButton.isSelected()){
					ficheroPropertiesOLD = "EI_selenium.properties";
					params.updateSameProperties(Utils.getProperties(), "entorno", "Integrado");
				}
				else if (proButton.isSelected()){
					ficheroPropertiesOLD = "PRO_selenium.properties";
					params.updateSameProperties(Utils.getProperties(), "entorno", "Produccion");
				}
				

				String statText = new File("LanzamientoGlobal.bat").getAbsolutePath();
				FileOutputStream is = new FileOutputStream(statText);
				OutputStreamWriter osw = new OutputStreamWriter(is);
				Writer w = new BufferedWriter(osw);
				
				String ficheroBorrado = new File("Borrado.bat").getAbsolutePath();
				FileOutputStream is2 = new FileOutputStream(ficheroBorrado);
				OutputStreamWriter osw2 = new OutputStreamWriter(is2);
				Writer w2 = new BufferedWriter(osw2);
				
				String asuntoMail = "";
				
				//Para saber cuando estamos en el �ltimo
				int contador = 0;
				
				for(int dir=0; dir<directories.length;dir++){
					 
					 
					 if (!directories[dir].contains("LANZADOR_GLOBAL") && !directories[dir].contains(".git")) {

//						 w2.write("CALL cd.."+"\n");
//						 w2.write("CALL cd "+boxes[dir].getName()+"\n");
//						 w2.write("CALL cd Utilidades\n");
//						 w2.write("CALL BorrarResultados.bat"+"\n");
//						 w2.write("PING 1.1.1.13 -n 1 -w 3000 >NUL"+"\n");
						 						 
						 if (boxes[dir].isSelected()){		
							 
							 contador++;
							 
 
							File properties = new File (rutaActual.getParentFile().getAbsolutePath()+ "/" +directories[dir]+"/properties/selenium.properties"); 
							if (properties.exists())
								properties.delete();

							
							copyFile(rutaActual.getParentFile().getAbsolutePath()+ "/" +directories[dir]+"/properties/"+ficheroPropertiesOLD, rutaActual.getParentFile().getAbsolutePath()+ "/" +directories[dir]+"/properties/selenium.properties");

							//actualizamos tambien el properties de cada UUAA
							params.updateSameProperties(properties.getAbsolutePath(), "destinatarios", textfieldMails.getText());
							params.updateSameProperties(properties.getAbsolutePath(), "numeroEjecucionesSimultaneas", textfieldHilos.getText());
							
							//actualizamos la IP del Hub
							if (boxGrid[0].isSelected()){
//					    		String ipHub = "http://"+textFieldGrid.getText()+"/wd/hub";
								String ipHub = textFieldGrid.getText();
					    		params.updateSameProperties(properties.getAbsolutePath(), "ipHub", ipHub);
							}
							
							//Vamos a modificar el asuntoMail
							TestProperties paramsTemp = new TestProperties(rutaActual.getParentFile().getAbsolutePath()+ "/" +directories[dir]+"/properties/selenium.properties");
							paramsTemp.cargarPropiedades();
							String asuntoMailTemp = paramsTemp.getValorPropiedad("asuntoMail");
							
							if (chromeButton.isSelected())
								params.updateSameProperties(properties.getAbsolutePath(), "browser", "googlechrome");
							else if (firefoxButton.isSelected())
								params.updateSameProperties(properties.getAbsolutePath(), "browser", "firefox");
							else if (ieButton.isSelected())
								params.updateSameProperties(properties.getAbsolutePath(), "browser", "iexplore");
							
							
							if (asuntoMailTemp==null)
								asuntoMailTemp = "";
							
							if (!asuntoMail.equalsIgnoreCase(""))
								asuntoMail = "ANS";
							else
								asuntoMail = asuntoMailTemp;
							
							params.updateSameProperties(Utils.getProperties(), "asuntoMail", asuntoMail);
							
							//Lanzamiento CSV
							if (tipoANS.isSelected()){
								params.updateSameProperties(rutaActual.getParentFile().getAbsolutePath()+ "/" +directories[dir]+"/properties/selenium.properties", "rutaLanzamiento", "lanzamiento/lanzamientoMinimo.csv");										
							}
							else if (tipoCompletas.isSelected()){
								if (eiButton.isSelected())
									params.updateSameProperties(rutaActual.getParentFile().getAbsolutePath()+ "/" +directories[dir]+"/properties/selenium.properties", "rutaLanzamiento", "lanzamiento/lanzamientoCompletoEI.csv");
								else if (proButton.isSelected())
									params.updateSameProperties(rutaActual.getParentFile().getAbsolutePath()+ "/" +directories[dir]+"/properties/selenium.properties", "rutaLanzamiento", "lanzamiento/lanzamientoCompletoPRO.csv");
							}else if (tipoRapidas.isSelected()){
								params.updateSameProperties(rutaActual.getParentFile().getAbsolutePath()+ "/" +directories[dir]+"/properties/selenium.properties", "rutaLanzamiento", "lanzamiento/lanzamientoRapido.csv");										

							}
							
							
							
							File rutaPadre = new File(rutaActual.getParentFile().getAbsolutePath()+ "/" +directories[dir]);
							File rutaNueva = new File (chooser.getSelectedFile()+ "/"+directories[dir]);
			
							FileUtils.copyDirectory(rutaPadre, rutaNueva);		
							
							w.write("CALL cd.."+"\n");
							w.write("CALL cd "+boxes[dir].getName()+"\n");
						
							//Si no hay grid, lo de siempre
							if (!boxGrid[0].isSelected()){
								if (unicoInforme.isSelected()){
									w.write("java -jar Main.jar CrearFicheroLanzamientoSinMail"+"\n");
									w.write("CALL LanzamientoSimultaneoSinMail.bat"+"\n");
									
									if (contador==numeroChecksSeleccionados())
										w.write("CALL java -jar Main.jar CocheEscoba"+"\n" );
									
									else
										w.write("START java -jar Main.jar CocheEscoba"+"\n" );
									
									
								} else if (variosInformes.isSelected()) {
									w.write("java -jar Main.jar CrearFicheroLanzamiento"+"\n");
									w.write("CALL LanzamientoSimultaneo.bat"+"\n");
									w.write("START java -jar Main.jar CocheEscoba_Actualizar_Mail"+"\n" );
								}
							}
							
							//Si hay grid, ojito
							else {
								
								if (unicoInforme.isSelected()) {	
									if (boxGalatea[0].isSelected() && Utils.insideBBVA())
										w.write("START java -jar -Dhttp.proxyHost=localhost -Dhttp.proxyPort=3128 -Dhttps.proxyHost=localhost -Dhttps.proxyPort=3128 Main.jar Grid_SoloLanzar" + "\n");
									else
										w.write("START java -jar Main.jar Grid_SoloLanzar" + "\n");
								}
								else {
									if (boxGalatea[0].isSelected() && Utils.insideBBVA())
										w.write("START java -jar -Dhttp.proxyHost=localhost -Dhttp.proxyPort=3128 -Dhttps.proxyHost=localhost -Dhttps.proxyPort=3128 Main.jar" + "\n");
									else
										w.write("START java -jar Main.jar" + "\n");
										
								}
									
							}
							
							
	
						 }
					 }
	 
				 }
				 
				
				
				//Si se selecciona un unico iforme, el lanzador hace su magia
				if (unicoInforme.isSelected() && !boxGrid[0].isSelected()){
				 w.write("CALL cd.. \nCALL cd DIST_LANZADOR_GLOBAL\n" 
						 + "CALL java -jar Main.jar EsperarAlCocheEscoba\n"
						  +"CALL java -jar Main.jar ActualizarCSVGlobal>results\\ActualizarCSVGlobal.txt 2>&1 \n"
		    				+ "CALL java -jar Main.jar ActualizarCSVResultados\n"
		    				+ "CALL java -jar Main.jar MailSender >results\\MailSender.txt 2>&1\n"
						    + "if exist results\\InformeHTML\\resumenEjecucion.html START results\\InformeHTML\\resumenEjecucion.html"
		    				);
				
				} 
				

				if (unicoInforme.isSelected() && boxGrid[0].isSelected()){
					 w.write("CALL cd.. \nCALL cd DIST_LANZADOR_GLOBAL\n" 
							 + "CALL java -jar Main.jar EsperarAlCocheEscobaYRunning\n"
							  +"CALL java -jar Main.jar ActualizarCSVGlobal>results\\ActualizarCSVGlobal.txt 2>&1 \n"
			    				+ "CALL java -jar Main.jar ActualizarCSVResultados\n"
			    				+ "CALL java -jar Main.jar MailSender >results\\MailSender.txt 2>&1\n"
							    + "if exist results\\InformeHTML\\resumenEjecucion.html START results\\InformeHTML\\resumenEjecucion.html"
			    				);
					
					} 
				
				//Si se selecciona varios informes, el lanzador no hace nada
//				else if (variosInformes.isSelected()) {
//					 w.write("CALL cd.. \nCALL cd DIST_LANZADOR_GLOBAL\n" 
//							  +"CALL java -jar Main.jar ActualizarCSVGlobal>results\\ActualizarCSVGlobal.txt 2>&1 \n"
//			    				+ "CALL java -jar Main.jar ActualizarCSVResultados\n"
//			    				+ "CALL java -jar Main.jar MailSender >results\\MailSender.txt 2>&1\n"
//							    + "if exist results\\InformeHTML\\resumenEjecucion.html START results\\InformeHTML\\resumenEjecucion.html"
//			    				);
//				}
				
				
				w.close();
				w2.close();
				
				
				//Aqu� borramos
				borrarResultados(chooser.getSelectedFile().getAbsolutePath());
				

				File rutaPadre = new File(rutaActual.getParentFile().getAbsolutePath()+ "/DIST_LANZADOR_GLOBAL");
				File rutaNueva = new File (chooser.getSelectedFile()+ "/DIST_LANZADOR_GLOBAL");
				FileUtils.copyDirectory(rutaPadre,rutaNueva);
				
				
				
				
//				Process p = Runtime.getRuntime().exec("cmd /c start BorrarResultadosGlobal.bat");
//				p.waitFor();

				labelCopiando.setVisible(false);

				String unidad = chooser.getSelectedFile().getAbsolutePath().substring(0,1);
					
				if (!boxScheduler[0].isSelected()) {	
									
					//p = Runtime.getRuntime().exec("cmd /c "+unidad+": && cd \""+chooser.getSelectedFile().getAbsolutePath()+"\\DIST_LANZADOR_GLOBAL\" && start Borrado.bat");
					Runtime.getRuntime().exec("cmd /c "+unidad+": && cd \""+chooser.getSelectedFile().getAbsolutePath()+"\\DIST_LANZADOR_GLOBAL\" && start LanzamientoGlobal.bat");
				
				}
				
				else {
					
					JOptionPane.showInputDialog(this, "Programe con su Programador de Tareas favorito el siguiente bat: ",chooser.getSelectedFile().getAbsolutePath()+"\\DIST_LANZADOR_GLOBAL\\LanzamientoGlobal.bat");
					
				}

			    System.exit(0);
			    
			    
				
			}
		} catch (FileNotFoundException e1) {
			System.out.println("Excepcion 1");
			//e1.printStackTrace();
			
			if (e1.toString().contains("chromedriver.exe")) {
				labelCopiando.setVisible(false);
				int resul = JOptionPane.showConfirmDialog(this, "Existen procesos de chromedriver en uso. �Desea finalizarlos?");

				if (resul==0) {
					try {
						Runtime.getRuntime().exec("cmd /c taskkill /f /im chromedriver.exe");
						setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
						JOptionPane.showMessageDialog(this, "Procesos eliminados con �xito. Puede proceder a ejecutar de nuevo.");

					} catch (IOException e2) {
						
						e2.printStackTrace();
					}
				}
				
				else {
					setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}
			}
			
		} catch (IOException e1) {
			System.out.println("Excepcion 2");
			System.out.println(e1.toString());
			JOptionPane.showMessageDialog(this, "Se produjo un problema al copiar los archivos. Revise el log");
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			System.out.println("Excepcion 3");
			System.out.println(e1.toString());
			JOptionPane.showMessageDialog(this, "Se produjo un problema al copiar los archivos. Revise el log");

			e1.printStackTrace();
		} catch (Exception e1) {
			System.out.println("Excepcion 4");
			System.out.println(e1.toString());
			JOptionPane.showMessageDialog(this, "Se produjo un problema al copiar los archivos. Revise el log");

			e1.printStackTrace();
		}
    		

            
        
    }



    
    public static void main() throws IOException, ParseException, ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
    	
    	File carpetaApp = new File (System.getProperty("user.home") + "/ScriptNow");
    	
    	if (!carpetaApp.exists())
    		carpetaApp.mkdirs();
    	
    	FileOutputStream archivoSalida = new FileOutputStream(carpetaApp + "/logScriptNow.txt");
    	PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(archivoSalida), true);
    	System.setOut(ficheroSalida);
    	
    	Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println("INICIO ScriptNow:" +timestamp);
    	    	
        SeleccionarUUAALanzamiento formulario1=new SeleccionarUUAALanzamiento();
        
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());    

        UIManager.put("Button.disabledText", Color.gray);


        formulario1.setAlwaysOnTop(true);
        formulario1.setTitle("Ejecuci�n Global Autom�tica");
        formulario1.setResizable(true);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        formulario1.setLocation(dim.width/2-formulario1.getSize().width/2, dim.height/2-formulario1.getSize().height/2);
        formulario1.setVisible(true);

        formulario1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        
    }   
    
    
    
    public static void monitor() {
        JFrame frame = new JFrame("ProgressMonitor Sample");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridLayout(0, 1));

        JButton startButton = new JButton("Start");
        ActionListener startActionListener = new ActionListener() {
          public void actionPerformed(ActionEvent actionEvent) {
            Component parent = (Component) actionEvent.getSource();
            monitor = new ProgressMonitor(parent, "Loading Progress", "Getting Started...", 0, 200);
            progress = 0;
          }
        };
        startButton.addActionListener(startActionListener);
        frame.add(startButton);

 

        JButton autoIncreaseButton = new JButton("Automatic Increase");
        ActionListener autoIncreaseActionListener = new ActionListener() {
          public void actionPerformed(ActionEvent actionEvent) {
            if (monitor != null) {
              if (timer == null) {
                timer = new Timer(250, new ActionListener() {

                  public void actionPerformed(ActionEvent e) {
                    if (monitor == null)
                      return;
                    if (monitor.isCanceled()) {
                      System.out.println("Monitor canceled");
                      timer.stop();
                    } else {
                      progress += 3;
                      monitor.setProgress(progress);
                      monitor.setNote("Loaded " + progress + " files");
                    }
                  }
                });
              }
              timer.start();
            }
          }
        };
        autoIncreaseButton.addActionListener(autoIncreaseActionListener);
        frame.add(autoIncreaseButton);

        frame.setSize(300, 200);
        frame.setVisible(true);
      }
    

    public static void copyFile(String origin, String destination) throws IOException {
        Path FROM = Paths.get(origin);
        Path TO = Paths.get(destination);
        //overwrite the destination file if it exists, and copy
        // the file attributes, including the rwx permissions
        CopyOption[] options = new CopyOption[]{
          StandardCopyOption.REPLACE_EXISTING,
          StandardCopyOption.COPY_ATTRIBUTES
        }; 
        Files.copy(FROM, TO, options);
    }
    
public static void deleteFileWithExtension(String directory, String extension) throws IOException {
		
	File folder = new File(directory);
	File fList[] = folder.listFiles();
	// Searchs .lck
	for (int i = 0; i < fList.length; i++) {
	    String pes = fList[i].getName();
	    if (pes.endsWith(extension)) {
	        // and deletes
	        fList[i].delete();
	    }
	}
	}

public static void deleteAllFiles(String directory) throws IOException {
	
	File folder = new File(directory);
	File fList[] = folder.listFiles();
	// Searchs .lck
	for (int i = 0; i < fList.length; i++) {

	     fList[i].delete();
	    
	}
	}

public static void deleteFileStarting(String directory, String extension) throws IOException {
	
	File folder = new File(directory);
	File fList[] = folder.listFiles();
	// Searchs .lck
	for (int i = 0; i < fList.length; i++) {
	    String pes = fList[i].getName();
	    if (pes.startsWith(extension)) {
	        // and deletes
	        fList[i].delete();
	    }
	}
	}
    
    public static void borrarResultados(String ruta) {
    	
    	//Hay q borrar results sin informeHTML
    	
    	ruta = ruta + "/";
    	
    	try {
    		
    		File child = new File (ruta);
    		File file = new File(child.getAbsolutePath()); 
    		String[] directories = file.list(new FilenameFilter() { 
    			
    			public boolean accept(File current, String name) { 
    			return new File(current, name).isDirectory(); 
    		} 
    		});
    		
    		for(int dir=0; dir<directories.length;dir++){
    			
//    			File rutaResultados = new File (ruta + directories[dir] + "/results");
//    			File rutaResultadosRest = new File (ruta + directories[dir] + "/results/REST");
    			File rutaScreenshots = new File (ruta + directories[dir] + "/screenshots");
    			File rutaDownloads = new File (ruta + directories[dir] + "/downloads");
    			
    			deleteAllFiles(ruta + directories[dir] + "/results");
    			
    			if (new File(ruta + directories[dir] + "/results/REST").exists())
    				deleteAllFiles(ruta + directories[dir] + "/results/REST");
    			
    			FileUtils.cleanDirectory(rutaScreenshots);
    			FileUtils.cleanDirectory(rutaDownloads);
    			
    			File rutaDatosVariablesUsuarios = new File (ruta + directories[dir] + "/resources/DatosVariablesUsuarios.csv");   			
    			if (rutaDatosVariablesUsuarios.exists()) rutaDatosVariablesUsuarios.delete();
    			
    			File rutaDatosVariablesContactos = new File (ruta + directories[dir] + "/resources/DatosVariablesContactos.csv");   			
    			if (rutaDatosVariablesContactos.exists()) rutaDatosVariablesContactos.delete();
    			
    			File rutaDatosVariablesTokens = new File (ruta + directories[dir] + "/resources/DatosVariablesTokens.csv");   			
    			if (rutaDatosVariablesTokens.exists()) rutaDatosVariablesTokens.delete();
    			
    			File rutaTemp = new File (ruta + directories[dir] + "/resources/temp.txt");   			
    			if (rutaTemp.exists()) rutaTemp.delete();
    			
    			File rutapath = new File (ruta + directories[dir] + "/resources/path.txt");   			
    			if (rutapath.exists()) rutapath.delete();
    			
    			File rutatime = new File (ruta + directories[dir] + "/resources/time.txt");   			
    			if (rutatime.exists()) rutatime.delete();
    			
    			deleteFileWithExtension(ruta + directories[dir] + "/resources/", ".running");

    			deleteFileStarting(ruta + directories[dir] + "/resources/", "C-204_");
    			
    			File rutaInformeDetallado = new File (ruta + directories[dir] + "/results/InformeHTML/informeDetallado.html");   			
    			if (rutaInformeDetallado.exists()) rutaInformeDetallado.delete();
    			
    			File rutadatosEjecucion = new File (ruta + directories[dir] + "/results/InformeHTML/datosEjecucion.html");   			
    			if (rutadatosEjecucion.exists()) rutadatosEjecucion.delete();
    			
    			File rutaresumenEjecucion = new File (ruta + directories[dir] + "/results/InformeHTML/resumenEjecucion.html");   			
    			if (rutaresumenEjecucion.exists()) rutaresumenEjecucion.delete();
    			
    			
    		}
    		
    		
    	} catch (Exception x) {
    	} 
    	
    }
    
    
    
}