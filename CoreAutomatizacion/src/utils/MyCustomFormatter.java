package utils;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class MyCustomFormatter extends Formatter {

    @Override
    public String format(LogRecord record) {
        StringBuffer sb = new StringBuffer();
        sb.append(record.getMessage());
        sb.append(System.getProperty("line.separator"));
        return sb.toString();
    }

}
