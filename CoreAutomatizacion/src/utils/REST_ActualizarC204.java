package utils;


import java.io.File;

import junit.framework.TestCase;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class REST_ActualizarC204 extends TestCase
{
	public String m_nombre = this.getClass().getSimpleName();

	public String m_codigo = "UTI_0009";

	int contador = 0;

	boolean cadenaValida = false;

	String codigoFicheroHTML = "";

	String codigoFicheroCSV = "";

	public String codigoFicheroC204 = "";


	@Before
	public void setUp() throws Exception
	{
		new Resultados(m_nombre);
	}


	@Test
	public void testActualizarCSVResultados() throws Exception
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		TestProperties params1 = new TestProperties(UtilsRest.getProperties());
		params1.cargarPropiedades();

		String nombreC204 = params1.getValorPropiedad("nombreC204").toLowerCase();

		File fichero = UtilsRest.BajarC204(nombreC204);

		// Si no existe versi�n en la nube, empezamos uno en base a la plantilla
		if(fichero == null)
		{
			fichero = new File("resources/PlantillaREST.xls");
		}

		File c204 = new File("resources/" + nombreC204);

		try
		{

			Workbook w = Workbook.getWorkbook(fichero);
			WritableWorkbook copy = Workbook.createWorkbook(c204, w);

			WritableFont arial10ptBold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD);
			WritableCellFormat arial10BoldFormatBorder = new WritableCellFormat(arial10ptBold);
			arial10BoldFormatBorder.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
			arial10BoldFormatBorder.setAlignment(Alignment.CENTRE);

			WritableFont arial9pt = new WritableFont(WritableFont.ARIAL, 9, WritableFont.NO_BOLD);
			WritableCellFormat arial9Format = new WritableCellFormat(arial9pt);
			arial9Format.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
			arial9Format.setAlignment(Alignment.LEFT);

			WritableCellFormat arial9Format2 = new WritableCellFormat(arial9pt);
			arial9Format2.setBorder(jxl.format.Border.TOP, jxl.format.BorderLineStyle.THIN);
			arial9Format2.setBorder(jxl.format.Border.BOTTOM, jxl.format.BorderLineStyle.THIN);
			arial9Format2.setAlignment(Alignment.LEFT);

			WritableFont arial9ptCENTER = new WritableFont(WritableFont.ARIAL, 9, WritableFont.NO_BOLD);
			WritableCellFormat arial9CenterFormat = new WritableCellFormat(arial9ptCENTER);
			arial9CenterFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
			arial9CenterFormat.setAlignment(Alignment.CENTRE);

			WritableCellFormat arial10BoldFormatDownBorder = new WritableCellFormat(arial10ptBold);
			arial10BoldFormatDownBorder.setBorder(jxl.format.Border.BOTTOM, jxl.format.BorderLineStyle.MEDIUM);

			WritableCellFormat arial10BoldFormatDownBorder2 = new WritableCellFormat(arial10ptBold);
			arial10BoldFormatDownBorder2.setBorder(jxl.format.Border.BOTTOM, jxl.format.BorderLineStyle.MEDIUM);
			arial10BoldFormatDownBorder2.setBorder(jxl.format.Border.TOP, jxl.format.BorderLineStyle.THIN);

			WritableCellFormat arial10BoldFormatDownBorder3 = new WritableCellFormat(arial10ptBold);
			arial10BoldFormatDownBorder3.setBorder(jxl.format.Border.BOTTOM, jxl.format.BorderLineStyle.MEDIUM);
			arial10BoldFormatDownBorder3.setBorder(jxl.format.Border.TOP, jxl.format.BorderLineStyle.THIN);
			arial10BoldFormatDownBorder3.setBorder(jxl.format.Border.LEFT, jxl.format.BorderLineStyle.THIN);
			arial10BoldFormatDownBorder3.setBorder(jxl.format.Border.RIGHT, jxl.format.BorderLineStyle.THIN);
			arial10BoldFormatDownBorder3.setAlignment(Alignment.CENTRE);

			WritableFont arial10ptNegrita = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD, true);
			WritableCellFormat arial10NegritaFormatBorder = new WritableCellFormat(arial10ptNegrita);
			arial10NegritaFormatBorder.setWrap(true);
			arial10NegritaFormatBorder.setAlignment(Alignment.CENTRE);

//	    for (int k=0; k<copy.getSheets().length;k++){

			// Rellenar resultados
			WritableSheet sheet = copy.getSheet(1);

			if(!sheet.getSettings().isHidden())
			{

//		      for (int j = 0; j < sheet.getColumns(); j++) {
				for(int i = 0; i < sheet.getRows(); i++)
				{
					WritableCell cell = sheet.getWritableCell(0, i);
					WritableCell nextcell = sheet.getWritableCell(0, i + 1);

					if(UtilsRest.estadoCaso(cell.getContents()).equalsIgnoreCase("ERROR"))
					{
						if(!cell.getContents().equals(nextcell.getContents()))
						{
							Label label = new Label(8, i, "Insatisfactorio", arial10BoldFormatDownBorder3);
							sheet.addCell(label);
						}
						else
						{
							Label label = new Label(8, i, "Insatisfactorio", arial10BoldFormatBorder);
							sheet.addCell(label);
						}

						if(!cell.getContents().equals(nextcell.getContents()))
						{
							Label label = new Label(7, i, UtilsRest.ComentarioCaso(cell.getContents()), arial10BoldFormatDownBorder2);
							sheet.addCell(label);
						}
					}
					else if(UtilsRest.estadoCaso(cell.getContents()).equalsIgnoreCase("OK"))
					{

						if(!cell.getContents().equals(nextcell.getContents()))
						{
							Label label = new Label(8, i, "Satisfactorio", arial10BoldFormatDownBorder3);
							sheet.addCell(label);
						}
						else
						{

							Label label = new Label(8, i, "Satisfactorio", arial10BoldFormatBorder);
							sheet.addCell(label);
						}

						if(!cell.getContents().equals(nextcell.getContents()))
						{
							Label label = new Label(7, i, UtilsRest.ComentarioCaso(cell.getContents()), arial10BoldFormatDownBorder2);
							sheet.addCell(label);
						}
					}

					else if(UtilsRest.estadoCaso(cell.getContents()).equalsIgnoreCase("WARNING"))
					{
						if(!cell.getContents().equals(nextcell.getContents()))
						{
							Label label = new Label(8, i, "Satisfactorio", arial10BoldFormatDownBorder3);
							sheet.addCell(label);
						}
						else
						{
							Label label = new Label(8, i, "Satisfactorio", arial10BoldFormatBorder);
							sheet.addCell(label);
						}
						if(!cell.getContents().equals(nextcell.getContents()))
						{
							Label label = new Label(7, i, UtilsRest.ComentarioCaso(cell.getContents()), arial10BoldFormatDownBorder2);
							sheet.addCell(label);
						}
					}

					else if(UtilsRest.estadoCaso(cell.getContents()).equalsIgnoreCase("NO EJECUTADO"))
					{
						if(!cell.getContents().equals(nextcell.getContents()))
						{
							Label label = new Label(8, i, "Caso err�neo", arial10BoldFormatDownBorder3);
							sheet.addCell(label);
						}
						else
						{
							Label label = new Label(8, i, "Caso err�neo", arial10BoldFormatBorder);
							sheet.addCell(label);
						}

						if(!cell.getContents().equals(nextcell.getContents()))
						{
							Label label = new Label(7, i, "Este caso no aplica en este entorno", arial10BoldFormatDownBorder2);
							sheet.addCell(label);
						}
					}


				}
				// }
			}


			// Rellenar info adicional
			sheet = copy.getSheet(0);

			String FechaHoy = UtilsRest.GetTodayDate();

			// Fecha Actualizaci�n
			Label label = new Label(3, 7, FechaHoy, arial10BoldFormatDownBorder);
			sheet.addCell(label);

			// Versi�n actual
			WritableCell cell = sheet.getWritableCell(5, 7);
			String versionActual = cell.getContents();

			if(versionActual.equals(""))
				versionActual = "1.0";

			String versionNueva = String.valueOf(Double.parseDouble(versionActual) + 0.1).substring(0, 3);

			label = new Label(5, 7, versionNueva, arial10BoldFormatDownBorder);
			sheet.addCell(label);

			// Versi�n + Casos de prueba
			String crLf = "\n";
			String literalCasosVersion = "Casos de Prueba " + crLf + " V" + versionNueva + " (" + FechaHoy + ")";

			label = new Label(6, 0, literalCasosVersion, arial10NegritaFormatBorder);
			sheet.addCell(label);

			// Fecha inicio y fecha fin
			label = new Label(5, 15, FechaHoy, arial10BoldFormatBorder);
			sheet.addCell(label);

			label = new Label(9, 15, FechaHoy, arial10BoldFormatBorder);
			sheet.addCell(label);

			// Versi�n + casos en la pesta�a 1
			sheet = copy.getSheet(1);
			label = new Label(6, 0, literalCasosVersion, arial10NegritaFormatBorder);
			sheet.addCell(label);

			// Autor
			String autor = "Automatizaci�n";
			label = new Label(2, 8, autor, arial9Format);
			sheet.addCell(label);

			// Tablita
			sheet = copy.getSheet(0);
			int filaVersion = sheet.getRows();

			label = new Label(0, filaVersion, versionNueva, arial9CenterFormat);
			sheet.addCell(label);

			label = new Label(1, filaVersion, FechaHoy, arial9CenterFormat);
			sheet.addCell(label);

			label = new Label(2, filaVersion, autor, arial9Format2);
			sheet.addCell(label);

			label = new Label(3, filaVersion, "", arial9Format2);
			sheet.addCell(label);

			label = new Label(4, filaVersion, "", arial9Format2);
			sheet.addCell(label);

			sheet.mergeCells(2, filaVersion, 4, filaVersion);
			// sheet.mergeCells(2, filaVersion,4, filaVersion);

			label = new Label(5, filaVersion, "Ejecuci�n pruebas Autom�ticas REST", arial9Format);
			sheet.addCell(label);

			label = new Label(9, filaVersion, "", arial9Format);
			sheet.addCell(label);

			sheet.mergeCells(5, filaVersion, 9, filaVersion);

//	    }

			copy.write();
			copy.close();
			w.close();

			// SUBIDA DEL FICHERO C204

			String urlCloud = Constantes.dnsCloud;
			String dns = "http://146.148.15.138";

			if(HTTPConnect.comprobarRespuestaHTML(urlCloud + "/testing", "works"))
				dns = urlCloud;
			else
				dns = "http://146.148.15.138";


			TestApplet test = new TestApplet();
			test.SubirC204(c204.getAbsolutePath());

			String nombreC204Copia = nombreC204.replace(".xls", "") + UtilsRest.GetTodayDateAndTimeSFTP() + ".xls";
			File copia = new File("resources/" + nombreC204Copia);

			// Hacemos una copia para dejar en la nube el bueno y una copia
			FileUtils.copyFile(c204, copia);

			test.SubirC204(copia.getAbsolutePath());
			copia.delete();

			String ficheroC204 = dns + "/testing/" + nombreC204Copia;

			codigoFicheroC204 = "<a Style=\"color: #2AABDF;\" href=\""
					+ ficheroC204
					+ "\" target=\"_blank\" download=\"csv\"> <img alt=\"Abrir\" border=\"0\" src=\"https://sites.google.com/a/bbva.com/plataforma_banca_empresas/gestion-y-rendimiento/icGuardarT.gif\">Guardar</a>";


			Utils.crearFicheroTemporal("resources/temp.txt", nombreC204Copia);


		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}


	@After
	public void tearDown() throws Exception
	{
//	  UtilsRest.guardarEjecucion(driver,resultado);
	}
}
