package utils;


import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;


public class Token
{
	// Constantes nombres columnas
	private String Referencia;

	private String Tipo;

	private String Numero;

	private String Telefono;

	private String Estado;

	private String CodigoUsuario;

	private String NumeroFormateado;


	public Token(String referencia, String numero)
	{
		Referencia = referencia;
		Tipo = "";
		Numero = numero;
		Telefono = "";
		Estado = "";
		CodigoUsuario = "";
		setNumeroFormateado(numero.replace("-", "").replace(" ", ""));

	}


	public Token(CsvReader reader) throws IOException
	{
		Referencia = reader.get(0);
		Tipo = reader.get(1);
		Numero = reader.get(2);
		Telefono = reader.get(3);
		Estado = reader.get(4);
		CodigoUsuario = reader.get(5);
		setNumeroFormateado(Numero.replace("-", "").replace(" ", ""));
	}


	public static Token obtenerTokenPruebas(String vreferencia) throws Exception
	{
		Token TokenPruebas = null;
		String numeroToken = "";
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosTokenFijos");
		String ventorno = params.getValorPropiedad("entorno");
		// Buscamos un token para la referencia recibida y el entorno en el que se ejecuta la prueba
		CsvReader reader = new CsvReader(new FileReader(rutaCSV));
		boolean resul = false;
		while(reader.readRecord())
		{
			String entorno = reader.get(0);
			String referencia = reader.get(1);
			if((entorno.equalsIgnoreCase(ventorno)) && (referencia.equalsIgnoreCase(vreferencia)))
			{
				resul = true;
				numeroToken = reader.get(2);
				break;
			}
		}
		// Si lo encontramos, lo devolvemos
		if(resul)
			TokenPruebas = new Token(vreferencia, numeroToken);
		else
			Log.write("No se ha encontrado un token en el fichero " + rutaCSV + ", no se va a poder realizar la prueba");
		reader.close();
		return TokenPruebas;

	}


	public static Token obtenerTokenPorEstado(String vreferencia, String vestado) throws Exception
	{

		Token TokenExistente = null;
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosTokenVariables");
		if(new File(rutaCSV).exists())
		{
			CsvReader reader = new CsvReader(new FileReader(rutaCSV));
			boolean resul = false;
			// Buscamos si est� un token de esa referencia y con ese estado en el archivo
			while(reader.readRecord())
			{
				String referencia = reader.get(0);
				String estado = reader.get(4);
				if((estado.equalsIgnoreCase(vestado)) && (referencia.equalsIgnoreCase(vreferencia)))
				{
					resul = true;
					break;
				}
			}
			// Si lo encontramos, lo devolvemos
			if(resul)
				TokenExistente = new Token(reader);
			reader.close();
		}
		return TokenExistente;

	}


	// Elimina el token actual en el CSV
	public void eliminarTokenEnCSV() throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosTokenVariables");
		// Si existe el fichero, busca el token y lo elimina si existe
		if(new File(rutaCSV).exists())
		{
			CsvReader reader = new CsvReader(new FileReader(rutaCSV));
			boolean resul = false;
			int i = 0;
			// Buscamos si est� ese token en el archivo
			while(reader.readRecord())
			{
				i++;
				String referencia = reader.get(0);
				String numero = reader.get(2);
				if((referencia.equalsIgnoreCase(Referencia)) && numero.equalsIgnoreCase(Numero))
				{
					resul = true;
					break;
				}
			}
			// Si existe ya la entrada la borra
			if(resul)
				Utils.removeNthLine(rutaCSV, i - 1);
			reader.close();
		}
	}


	// Guarda o actualiza si existe el token en el csv
	public void guardarTokenEnCSV() throws Exception, IOException
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosTokenVariables");

		String outputFile = rutaCSV;
		// vemos si el fichero ya existe

		boolean alreadyExists = new File(outputFile).exists();

		try
		{
			// usamos FileWriter indicando que es para append
			CsvWriter writer = new CsvWriter(new FileWriter(outputFile, true), ',');
			CsvReader reader = new CsvReader(new FileReader(outputFile));

			// Si el fichero no existe, lo creamos con las cabeceras oportunas
			if(!alreadyExists)
			{
				writer.write("Referencia");
				writer.write("Tipo");
				writer.write("Numero");
				writer.write("Telefono");
				writer.write("Estado");
				writer.write("CodigoUsuario");
				writer.endRecord();
			}

			int i = 0;
			boolean resul = false;

			// Buscamos si est� ese token en el archivo
			while(reader.readRecord())
			{
				i++;
				// Buscamos por referencia y n�mero de dispositivo
				String referencia = reader.get(0);
				String numero = reader.get(2);
				if((referencia.equalsIgnoreCase(Referencia)) && (numero.equalsIgnoreCase(Numero)))
				{
					resul = true;
					break;
				}
			}
			// Si existe ya la entrada la borra
			if(resul)
				Utils.removeNthLine(outputFile, i - 1);

			writer.write(Referencia);
			writer.write(Tipo);
			writer.write(Numero);
			writer.write(Telefono);
			writer.write(Estado);
			writer.write(CodigoUsuario);
			writer.endRecord();
			writer.close();
			reader.close();

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	public String getReferencia()
	{
		return this.Referencia;
	}


	public void setReferencia(String valor)
	{
		Referencia = valor;
	}


	public String getEstado()
	{
		return this.Estado;
	}


	public void setEstado(String valor)
	{
		this.Estado = valor;
	}


	public String getCodigoUsuario()
	{
		return this.CodigoUsuario;
	}


	public void setCodigoUsuario(String valor)
	{
		CodigoUsuario = valor;
	}


	public String getNumero()
	{
		return this.Numero;
	}


	public void setNumero(String valor)
	{
		Numero = valor;
	}


	public String getTelefono()
	{
		return this.Telefono;
	}


	public void setTelefono(String valor)
	{
		Telefono = valor;
	}


	public String getTipo()
	{
		return this.Tipo;
	}


	public void setTipo(String valor)
	{
		Tipo = valor;
	}


	public String getNumeroFormateado()
	{
		return NumeroFormateado;
	}


	public void setNumeroFormateado(String numeroFormateado)
	{
		NumeroFormateado = numeroFormateado;
	}
}
