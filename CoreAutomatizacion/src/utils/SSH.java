package utils;


import java.io.InputStream;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;


public class SSH extends TestCase
{
	public String m_nombre = this.getClass().getSimpleName();

	public String m_codigo = "UTI_0005";

	int contador = 0;

	boolean cadenaValida = false;


	@Before
	public void setUp() throws Exception
	{}


	@Test
	public void testActualizarCSVResultados() throws Exception
	{


		String host = "15.30.174.150";
		String user = "";
		String password = Utils.devuelveClaveCifrada();
		String command1 = "cd C:/Compartido/Antonio/dist/ \n LanzamientoSimultaneo.bat";
		try
		{

			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch = new JSch();

			Session session = jsch.getSession(user, host, 22);
			session.setPassword(password);
			session.setConfig(config);
			session.connect();
			Log.write("Connected");

//          Channel channel= session.openChannel("shell");
//          
//          OutputStream toServer = channel.getOutputStream(); 
//          
//          BufferedReader fromServer = new java.io.BufferedReader(new 
//        		  java.io.InputStreamReader((channel.getInputStream())));
//        		              toServer = channel.getOutputStream(); 
//
//          channel.connect();
//          
//          toServer.write(command1.getBytes()); 
//          toServer.flush();

			Channel channel = session.openChannel("exec");
			((ChannelExec)channel).setCommand(command1);
			channel.setInputStream(null);
			((ChannelExec)channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			byte[] tmp = new byte[1024];
			while(true)
			{
				while(in.available() > 0)
				{
					int i = in.read(tmp, 0, 1024);
					if(i < 0)
						break;
					System.out.print(new String(tmp, 0, i));
				}
				if(channel.isClosed())
				{
					Log.write("exit-status: " + channel.getExitStatus());
					break;
				}
				try
				{
					Thread.sleep(1000);
				}
				catch(Exception ee)
				{}
			}
			channel.disconnect();
			session.disconnect();
			Log.write("DONE");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}


	}


	@After
	public void tearDown() throws Exception
	{

	}
}
