package utils;


import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintStream;
import java.util.Scanner;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ActualizarCSVResultados extends TestCase
{
	public String m_nombre = this.getClass().getSimpleName();

	public String m_codigo = "UTI_0002";

	private Resultados resultado;

	int contador = 0;

	boolean cadenaValida = false;

	boolean loginOK = false;

	String codigoFicheroHTML = "";

	String codigoFicheroCSV = "";

	String codigoFicheroC204 = "";


	@Before
	public void setUp() throws Exception
	{	
			
		resultado = new Resultados(m_nombre);
		
		Log.cerrarTodosLog();
		
		FileOutputStream fichero = new FileOutputStream("results/" + m_nombre + ".txt", true);
//		PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);

//		System.setOut(ficheroSalida);
//		System.setErr(ficheroSalida);
		
		//Solo para pruebas
//		List<String> lista = new ArrayList<String>();	
//		lista.add("administracion.PersonalizarCuentasModificarAliasTest");
//		lista.add("administracion.PersonalizarCuentasMarcarDesmarcarCuentaFavoritaTest");
//		lista.add("administracion.BuscarAuditoriaTest");
//		lista.add("portal.OpcionesMenuTest");
//		lista.add("portal.CabeceraTest");
//		lista.add("portal.PieTest");
//		lista.add("portal.ActivarAyuda");
//		lista.add("administracion.Desconectar");
//		lista.add("com.bbva.kynf.opr.AlertaC1_OPR_CanalMailSinMail");
//		lista.add("com.bbva.kynf.G02_ContratacionYAlertas");
//		lista.add("com.bbva.kynf.front.NuevoMensaje");
//		lista.add("com.bbva.kynf.front.MarcarComoLeido");
//		lista.add("com.bbva.kynf.front.FiltrosMensajes");
//		lista.add("com.bbva.kynf.confirming.ProcesoBatchConfirmingProveedores");
//		lista.add("com.bbva.kynf.contratacion.ContratacionServicio");
//		lista.add("com.bbva.kynf.vencimientoTransferencia.ProcesoBatchVencimientoTransferenciaPer");		
//		driver = Utils.inicializarEjecucion(m_codigo, m_nombre);
//		Utils.CSVToTableNew(new Date(), "", "", lista );
		
	}


	@Test
	public void testActualizarCSVResultados() throws Exception
	{

		Thread.sleep(3000);

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String destinoResultados = params.getValorPropiedad("destinoResultados");
		String generarFichero = params.getValorPropiedad("generarFichero").toLowerCase();
		String tipoEjecucion = params.getValorPropiedad("tipoEjecucion");

		//Podr�a ni existir la propiedad
		if (tipoEjecucion==null) 
			tipoEjecucion = "";

		String Pais = Utils.devuelveDatosDeEjecucion("Pa");
		String Entorno = Utils.devuelveDatosDeEjecucion("Entorno");
		String Navegador = Utils.devuelveDatosDeEjecucion("Navegador");
		String Fecha = Utils.devuelveDatosDeEjecucion("Fecha de Ejecuci");
		String HoraInicio = Utils.devuelveDatosDeEjecucion("Hora inicio");
		String HoraFin = Utils.devuelveDatosDeEjecucion("Hora fin");
		String CPTotal = Utils.devuelveDatosDeEjecucion("TOTAL");
		String CPOK = Utils.devuelveDatosDeEjecucion("Satisfactorios");
		String CPError = Utils.devuelveDatosDeEjecucion("Error");
		String CPWarning = Utils.devuelveDatosDeEjecucion("Avisos");
		String loggedUser = System.getProperty("user.name").toLowerCase();
		
		String UUAAs = Utils.devuelveUUAAsEjecutadas().trim();
		
		String casosEjecutados = Utils.devuelveCasosEjecutadosPorTipo(tipoEjecucion);

		String urlCloud = Constantes.dnsAzure;
		String dns = urlCloud;

		if(contador == 0)
		{

			// Subida a cloud
			if(destinoResultados.toLowerCase().indexOf("cloud") > -1)
			{

				TestApplet test = new TestApplet();
//				String nombreCarpeta = test.SubirEjecucionGoogleCloud("resultados.zip");
				String nombreCarpeta2 = test.SubirEjecucionAzure("resultados.zip");
				
				String ficheroHTML = dns + "/" + nombreCarpeta2 + "/results/InformeHTML/informeDetallado.html";
				String ficheroCSV = dns  + "/" + nombreCarpeta2 + "/results/results.csv";
				String ficheroC204 = "";

				codigoFicheroHTML = ficheroHTML;
				codigoFicheroCSV = ficheroCSV;

				
				System.out.println("[INFO]: Escribimos la ruta: "+ficheroHTML +", en el fichero resources/path.txt");
				File rutaPath = new File("resources/path.txt");
				if (rutaPath.exists()) rutaPath.delete();
				
				FileWriter fileHtm = new FileWriter(rutaPath.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fileHtm);
				bw.write(ficheroHTML);
				bw.close();
				
				
				//Si falla el mail, al menos dejar� aqu� la ruta del informe
				System.out.println("[INFO]: Escribimos la ruta: "+ficheroHTML +", en el fichero resources/pathInforme.txt");
				File rutaPathInforme = new File("resources/pathInforme.txt");
				if (rutaPathInforme.exists()) rutaPathInforme.delete();
				
				fileHtm = new FileWriter(rutaPathInforme.getAbsoluteFile());
				bw = new BufferedWriter(fileHtm);
				bw.write(ficheroHTML);
				bw.close();

				
				if(generarFichero.equalsIgnoreCase("SI"))
				{

					File temp = new File(System.getProperty("user.dir")+"/resources/temp.txt");

					if(temp.exists())
					{
						Scanner scanner = new Scanner(temp, "UTF-8");
						String text = scanner.useDelimiter("\\A").next();
						ficheroC204 = dns + "/testing/" + text;
						scanner.close();
					}


					codigoFicheroC204 = ficheroC204;

				}
				else
					codigoFicheroC204 = "N/A";
			}
			
		}

		try
		{			
			
			Utils.anadirResultadoAPIResultadosAzure(Pais, Entorno, Navegador, Fecha, HoraInicio, HoraFin, CPTotal, CPOK, CPError, CPWarning, codigoFicheroCSV.replace("\"", "\\\""), codigoFicheroHTML.replace("\"", "\\\""), codigoFicheroC204.replace("\"", "\\\""), UUAAs.replace("\"", "\\\""),loggedUser, tipoEjecucion,casosEjecutados);

		}
		catch(Exception e)
		{
			e.printStackTrace();
			resultado.appendError(e.getMessage());

		}
	}


	@After
	public void tearDown() throws Exception
	{
		//Utils.guardarEjecucion(driver, resultado);
	}
}
