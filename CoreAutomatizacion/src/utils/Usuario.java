package utils;


import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import jxl.Sheet;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;


public class Usuario
{
	// Constantes nombres columnas
	public final static String COLUMNAREFERENCIA = "Referencia";

	public final static String COLUMNACODUSUARIO = "CodUsuario";

	public final static String COLUMNAPASSWORD = "Password";

	public final static String COLUMNANOMBREUSUARIO = "NombreUsuario";

	public final static String COLUMNACLAVEOPERACIONES = "ClaveOp";

	public final static String COLUMNAMAIL = "Mail";

	public final static String COLUMNATIPODOCUMENTO = "TipoDoc";

	public final static String COLUMNADOCUMENTO = "NumDoc";

	public final static String COLUMNACODIGOPAIS = "CodPais";

	public final static String COLUMNAPAIS = "Pais";

	public final static String COLUMNATELEFONO = "Telefono";

	public final static String COLUMNACODIGOTIPOPODER = "CodTipoPoder";

	public final static String COLUMNATIPOPODER = "TipoPoder";

	public final static String COLUMNATIPOFIRMA = "TipoFirma";

	public final static int POSCOLUMNAREFERENCIA = 0;

	public final static int POSCOLUMNACODUSUARIO = 1;

	public final static int POSCOLUMNAPASSWORD = 2;

	public final static int POSCOLUMNANOMBREUSUARIO = 3;

	public final static int POSCOLUMNACLAVEOPERACIONES = 4;

	public final static int POSCOLUMNAMAIL = 5;

	public final static int POSCOLUMNATIPODOCUMENTO = 6;

	public final static int POSCOLUMNADOCUMENTO = 7;

	public final static int POSCOLUMNACODIGOPAIS = 8;

	public final static int POSCOLUMNAPAIS = 9;

	public final static int POSCOLUMNATELEFONO = 10;

	public final static int POSCOLUMNACODIGOTIPOPODER = 11;

	public final static int POSCOLUMNATIPOPODER = 12;

	public final static int POSCOLUMNATIPOFIRMA = 13;

	private String Referencia;

	private String CodUsuario;

	private String Password;

	private String NombreUsuario;

	private String ClaveOp;

	private String Mail;

	private String TipoDoc;

	private String NumDoc;

	private String CodPais;

	private String Pais;

	private String Telefono;

	private String CodTipoPoder;

	private String TipoPoder;

	private String TipoFirma;

	private String Estado;

	private String Dispositivo;


	public Usuario(String referencia)
	{
		Referencia = referencia;
		CodUsuario = "";
		Password = "";
		NombreUsuario = "";
		ClaveOp = "";
		Mail = "";
		TipoDoc = "";
		NumDoc = "";
		CodPais = "";
		Pais = "";
		Telefono = "";
		CodTipoPoder = "";
		TipoPoder = "";
		TipoFirma = "";
		Estado = "";
		Dispositivo = "";
	}


	public Usuario(Sheet hoja, int fila)
	{
		Referencia = hoja.getCell(POSCOLUMNAREFERENCIA, fila).getContents();
		CodUsuario = hoja.getCell(POSCOLUMNACODUSUARIO, fila).getContents();
		Password = hoja.getCell(POSCOLUMNAPASSWORD, fila).getContents();
		NombreUsuario = hoja.getCell(POSCOLUMNANOMBREUSUARIO, fila).getContents();
		ClaveOp = hoja.getCell(POSCOLUMNACLAVEOPERACIONES, fila).getContents();
		Mail = hoja.getCell(POSCOLUMNAMAIL, fila).getContents();
		TipoDoc = hoja.getCell(POSCOLUMNATIPODOCUMENTO, fila).getContents();
		NumDoc = hoja.getCell(POSCOLUMNADOCUMENTO, fila).getContents();
		CodPais = hoja.getCell(POSCOLUMNACODIGOPAIS, fila).getContents();
		Pais = hoja.getCell(POSCOLUMNAPAIS, fila).getContents();
		Telefono = hoja.getCell(POSCOLUMNATELEFONO, fila).getContents();
		CodTipoPoder = hoja.getCell(POSCOLUMNACODIGOTIPOPODER, fila).getContents();
		TipoPoder = hoja.getCell(POSCOLUMNATIPOPODER, fila).getContents();
		TipoFirma = hoja.getCell(POSCOLUMNATIPOFIRMA, fila).getContents();
	}


	public Usuario(CsvReader reader) throws IOException
	{
		Referencia = reader.get(0);
		CodUsuario = reader.get(1);
		Password = Utils.desencriptarConAES(reader.get(2));		
		NombreUsuario = reader.get(3);
		ClaveOp = reader.get(4);
		Mail = reader.get(5);
		TipoDoc = reader.get(6);
		NumDoc = reader.get(7);
		CodPais = reader.get(8);
		Pais = reader.get(9);
		Telefono = reader.get(10);
		CodTipoPoder = reader.get(11);
		TipoPoder = reader.get(12);
		TipoFirma = reader.get(13);
		Estado = reader.get(14);
		Dispositivo = "";
	}


	public static Usuario obtenerUsuarioPorReferenciaCodigo(Sheet hoja, String Referencia, String CodigoUsuario)
	{
		Usuario usuarioencontrado;
		int numfilas = hoja.getRows();
		int i = 1;
		while(i < numfilas && (!(hoja.getCell(POSCOLUMNAREFERENCIA, i).getContents().equals(Referencia) && hoja.getCell(POSCOLUMNACODUSUARIO, i).getContents().equals(CodigoUsuario))))
		{
			i++;
		}
		if(i < numfilas)
		{
			usuarioencontrado = new Usuario(hoja, i);
		}
		else
		{
			Log.write("Usuario " + CodigoUsuario + " de referencia " + Referencia + " no encontrado en excel de datos");
			usuarioencontrado = null;
		}

		return usuarioencontrado;
	}


	// Para los nuevos formatos en csv
	public static Usuario obtenerUsuarioPorReferenciaCodigo(String rutaCSV, String Referencia, String CodigoUsuario) throws Exception
	{
		Usuario usuarioEncontrado = null;

		String outputFile = rutaCSV;

		CsvReader reader = new CsvReader(new FileReader(outputFile));

		boolean resul = false;

		// Buscamos si est� ese usuario en el archivo
		while(reader.readRecord())
		{
			String referencia = reader.get(0);
			String codUsuario = reader.get(1);
			if(referencia.equalsIgnoreCase(Referencia) && codUsuario.equalsIgnoreCase(CodigoUsuario))
			{
				resul = true;
				break;
			}
		}

		if(resul)
		{
			usuarioEncontrado = new Usuario(reader);
		}

		reader.close();


		return usuarioEncontrado;
	}


	// Elimina el usuario actual en el CSV
	public void eliminarUsuarioEnCSV() throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosVariables");

		String outputFile = rutaCSV;

		CsvReader reader = new CsvReader(new FileReader(outputFile));

		boolean resul = false;
		int i = 0;

		// Buscamos si est� ese usuario en el archivo
		while(reader.readRecord())
		{
			i++;
			String id = reader.get(1);
			if(id.equalsIgnoreCase(CodUsuario))
			{
				resul = true;
				break;
			}
		}

		// Si existe ya la entrada la borra
		if(resul)
			Utils.removeNthLine(outputFile, i - 1);

		reader.close();

	}


	// Guarda o Actualiza si existe el usuario en el csv
	public void guardarUsuarioEnCSV() throws Exception, IOException
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosVariables");

		String outputFile = rutaCSV;

		// vemos si el fichero ya existe

		boolean alreadyExists = new File(outputFile).exists();

		try
		{
			// usamos FileWriter indicando que es para append
			CsvWriter writer = new CsvWriter(new FileWriter(outputFile, true), ',');
			CsvReader reader = new CsvReader(new FileReader(outputFile));

			// Si el fichero no existe, lo creamos con las cabeceras oportunas
			if(!alreadyExists)
			{
				writer.write("Referencia");
				writer.write("CodUsuario");
				writer.write("Password");
				writer.write("NombreUsuario");
				writer.write("ClaveOp");
				writer.write("Mail");
				writer.write("TipoDoc");
				writer.write("NumDoc");
				writer.write("CodPais");
				writer.write("Pais");
				writer.write("Telefono");
				writer.write("CodTipoPoder");
				writer.write("TipoPoder");
				writer.write("TipoFirma");
				writer.write("Estado");
				writer.endRecord();
			}

			int i = 0;
			boolean resul = false;

			// Buscamos si est� ese usuario en el archivo
			while(reader.readRecord())
			{
				i++;
				// Buscamos por N�mero de Documento
				String id = reader.get(7);
				if(id.equalsIgnoreCase(NumDoc))
				{
					resul = true;
					break;
				}
			}

			// Si existe ya la entrada la borra
			if(resul)
				Utils.removeNthLine(outputFile, i - 1);

			writer.write(Referencia);
			writer.write(CodUsuario);
			writer.write(Password);
			writer.write(NombreUsuario);
			writer.write(ClaveOp);
			writer.write(Mail);
			writer.write(TipoDoc);
			writer.write(NumDoc);
			writer.write(CodPais);
			writer.write(Pais);
			writer.write(Telefono);
			writer.write(CodTipoPoder);
			writer.write(TipoPoder);
			writer.write(TipoFirma);
			writer.write(Estado);

			writer.endRecord();
			writer.close();
			reader.close();

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}


	}


	public String getReferencia()
	{
		return this.Referencia;
	}


	public void setReferencia(String valor)
	{
		Referencia = valor;
	}


	public String getEstado()
	{
		return this.Estado;
	}


	public void setEstado(String valor)
	{
		this.Estado = valor;
	}


	public String getCodUsuario()
	{
		return this.CodUsuario;
	}


	public void setCodUsuario(String valor)
	{
		CodUsuario = valor;
	}


	public String getPassword()
	{
		return this.Password;
	}


	public void setPassword(String valor)
	{
		Password = valor;
	}


	public String getNombreUsuario()
	{
		return this.NombreUsuario;
	}


	public void setNombreUsuario(String valor)
	{
		NombreUsuario = valor;
	}


	public String getClaveOp()
	{
		return this.ClaveOp;
	}


	public void setClaveOp(String valor)
	{
		ClaveOp = valor;
	}


	public String getMail()
	{
		return this.Mail;
	}


	public void setMail(String valor)
	{
		Mail = valor;
	}


	public String getTipoDoc()
	{
		return this.TipoDoc;
	}


	public void setTipoDoc(String valor)
	{
		TipoDoc = valor;
	}


	public String getNumDoc()
	{
		return this.NumDoc;
	}


	public void setNumDoc(String valor)
	{
		NumDoc = valor;
	}


	public String getCodPais()
	{
		return this.CodPais;
	}


	public void setCodPais(String valor)
	{
		CodPais = valor;
	}


	public String getPais()
	{
		return this.Pais;
	}


	public void setPais(String valor)
	{
		Pais = valor;
	}


	public String getTelefono()
	{
		return this.Telefono;
	}


	public void setTelefono(String valor)
	{
		Telefono = valor;
	}


	public String getCodTipoPoder()
	{
		return this.CodTipoPoder;
	}


	public void setCodTipoPoder(String valor)
	{
		CodTipoPoder = valor;
	}


	public String getTipoPoder()
	{
		return this.TipoPoder;
	}


	public void setTipoPoder(String valor)
	{
		TipoPoder = valor;
	}


	public String getTipoFirma()
	{
		return this.TipoFirma;
	}


	public void setTipoFirma(String valor)
	{
		TipoFirma = valor;
	}


	public String getDispositivo()
	{
		return this.Dispositivo;
	}


	public void setDispositivo(String valor)
	{
		Dispositivo = valor;
	}
}
