package utils;


public class Resultados
{
	private String resultado;

	private String comentario;

	private String nombrecp;

	private StringBuffer verificationErrors = new StringBuffer();

	private StringBuffer verificationWarnings = new StringBuffer();


	public Resultados(String NombreCP)
	{
		resultado = "";
		comentario = "";
		nombrecp = NombreCP;
	}


	public String getNombreCP()
	{
		return this.nombrecp;
	}


	public String getResultado()
	{
		return this.resultado;
	}


	public String getComentario()
	{
		return this.comentario;
	}


	public StringBuffer getBufferErrores()
	{
		return this.verificationErrors;
	}


	public StringBuffer getBufferWarnings()
	{
		return this.verificationWarnings;
	}


	public String setResultado()
	{
		return this.resultado;
	}


	public void setResultado(String valor)
	{
		this.resultado = valor;
	}


	public void setComentario(String valor)
	{
		this.comentario = valor;
	}


	public void appendError(String valor)
	{
		this.verificationErrors.append(valor);
	}


	public void appendWarning(String valor)
	{
		this.verificationWarnings.append(valor);
	}


}
