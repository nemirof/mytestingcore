package utils;


import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.io.IOException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Authenticator;
import okhttp3.ConnectionPool;
import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

import org.openqa.selenium.remote.http.HttpClient;
import org.openqa.selenium.remote.http.HttpClient.Builder;
import org.openqa.selenium.remote.internal.OkHttpClient;

import com.google.common.base.Strings;

public class DefaultHttpFactory implements HttpClient.Factory {
	private SSLSocketFactory sslSocketFactory;

	private final TrustManager[] trustAllCerts;
	
	private final String usuarioGalatea = "bot-galatea-netcash";
	private final String passGalatea = "ZLv8W7ns3P2vEw7GB13tg8DZgZpSP4P7PTHgfyJ1";


	public DefaultHttpFactory() throws NoSuchAlgorithmException, KeyManagementException {

		/*Create a trust manager that does not validate certificate chains*/

        this.trustAllCerts = new TrustManager[] {

                new X509TrustManager() {

                    @Override

                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)

                            throws CertificateException {

                    }



                    @Override

                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)

                            throws CertificateException {

                    }



                    @Override

                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {

                        return new java.security.cert.X509Certificate[]{};

                    }

                }

        };

        final SSLContext sslContext = SSLContext.getInstance("SSL");

        sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

        /*Create an ssl socket factory with our all-trusting manager*/

        this.sslSocketFactory = sslContext.getSocketFactory();



	}



	private final ConnectionPool pool = new ConnectionPool();



    @Override

    public Builder builder() {

      return new Builder() {

        @Override

        public HttpClient createClient(URL url) {

          okhttp3.OkHttpClient.Builder client = new okhttp3.OkHttpClient.Builder()

              .connectionPool(pool)

              .followRedirects(true)

              .followSslRedirects(true)

              .proxy(proxy)

              .readTimeout(readTimeout.toMillis(), MILLISECONDS)

              .sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0])

              .connectTimeout(connectionTimeout.toMillis(), MILLISECONDS)

              .hostnameVerifier(new HostnameVerifier() {

                  @Override

                  public boolean verify(String hostname, SSLSession session) {

                      return true;

                  }

              });



//          String info = url.getUserInfo();

//          if (!Strings.isNullOrEmpty(info)) {

//            String[] parts = info.split(":", 2);
//
//            String user = parts[0];
//
//            String pass = parts.length > 1 ? parts[1] : null;



            final String credentials = Credentials.basic(usuarioGalatea, passGalatea);

            client.authenticator(new Authenticator() {
				
				@Override
				public Request authenticate(Route route, Response response) throws IOException {
		              if (response.request().header("Authorization") != null) {

		                return null; // Give up, we've already attempted to authenticate.

		              }



		              return response.request().newBuilder()

		                  .header("Authorization", credentials)

		                  .build();

		            }					
			});

//          }



          client.addNetworkInterceptor(
        		  new Interceptor() {
        				
        				@Override
        				public Response intercept(Chain chain) throws IOException {
        			            Request request = chain.request();

        			            Response response = chain.proceed(request);

        			            return response.code() == 408

        			                   ? response.newBuilder().code(500).message("Server-Side Timeout").build()

        			                   : response;

        			          }				
        				});        		  

          return new OkHttpClient(client.build(), url);

        }

      };

    }



    @Override

    public void cleanupIdleClients() {

      pool.evictAll();

    }

}