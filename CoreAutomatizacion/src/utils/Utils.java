package utils;



import io.qameta.allure.Allure;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.io.Writer;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Scanner;
import java.util.Vector;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipInputStream;

import javax.imageio.ImageIO;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import junit.framework.TestCase;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.CommandInfo;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.http.HttpClient.Factory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.UserInfo;
import com.rationaleemotions.GridApiAssistant;
import com.rationaleemotions.pojos.Host;
import com.rationaleemotions.pojos.HubConfiguration;
import com.rationaleemotions.pojos.NodeConfiguration;


/**
 * @author XE39619
 *
 */
/**
 * @author XE39619
 *
 */
/**
 * @author XE39619
 *
 */
/**
 * @author XE39619
 *
 */
public abstract class Utils extends TestCase
{
	
	private static String OS = System.getProperty("os.name").toLowerCase();

	public static String rutaProperties = "properties/selenium.properties";

	public static int timeOut = 40;

	static TestProperties params = new TestProperties(Utils.getProperties());

	// public staticl String[] Latam = new String[] {"Mexico","Chile","Argentina","Peru"};
	public static List<String> Latam = Arrays.asList("Mexico", "Chile", "Argentina", "Peru", "Compass");

	public static boolean reintento = true;

	public static boolean reintento2 = true;

	private static HashMap<String, Log> loggers = new HashMap<String, Log>();

	public static void setDefaultTimeOut(WebDriver driver)
	{
		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
	}


	public static void setTimeOut(WebDriver driver, int time)
	{
		driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
	}


	public static void login(WebDriver driver, Usuario usuario) throws Exception
	{
		if (usuario==null)
			throw new Exception ("No se encontr� el usuario necesario para logarse en la aplicaci�n.");

		//Escribimos el usuario con el que nos estamos logando
		Utils.pintarRefernciaUsuario(usuario.getReferencia(), usuario.getCodUsuario());

		params.cargarPropiedades();
		String idioma = params.getValorPropiedad("idioma").toLowerCase();
		String pais = params.getValorPropiedad("pais").toLowerCase();
		String entorno = params.getValorPropiedad("entorno").toLowerCase();
		String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
		String url = devuelveURLApartirDePaisYEntorno(rutaURLs, pais, entorno);

		if(Utils.estamosEnLatam())
		{

			if(Utils.isDisplayed(driver, Constantes.LoginLocal_ComboPais, 5))
			{
				loginLocal(driver, usuario);
			}
			else
			{
				loginTipoV7(driver, usuario);
			}

		}

		else
		{
			// Si no existe campo para login, se intenta recargar la aplicaci�n
			//	if(!Utils.isDisplayed(driver, Constantes.Login_Referencia, 2) && !Utils.isDisplayed(driver, Constantes.changeUserLabel, 1))
			//		driver.get(url);

			if (Utils.isDisplayed(driver, Constantes.ComboIdiomas, 2)) {
				String idiomaActual = driver.findElement(By.xpath(Constantes.ComboIdiomas)).getText();
				if(idiomaActual.toLowerCase().indexOf(idioma) < 0)
				{
					Utils.cambiarIdioma(driver, idioma);
				}
			}

			//RTS!!!!! 
			//Si estamos en Integrado, podemos modificar la fecha de ultimo acceso
			Utils.RTS_Update(usuario.getReferencia(), usuario.getCodUsuario());

			if (Utils.isDisplayedSinBefore(driver, Constantes.cerrarModalLogin, 0))
				Utils.clickEnElVisiblePrimero(driver, Constantes.cerrarModalLogin);


			//Utils.clickEnElVisiblePrimero(driver,"//button[contains(text(),'Aceptar')]");

			String currentUrlBrower=driver.getCurrentUrl();
			String auxBotonEntrar="";
			if (currentUrlBrower.contains("bbva.es/empresas.html")){

				utils.Log.write("Hacemos nuevo paso Login URL:  "+currentUrlBrower);
				Thread.sleep(2000);

				WebElement element  = driver.findElement( By.xpath("//header//*[contains(text(),'Acceso')]"));

				Utils.clickEnElVisiblePrimero(driver,"//header//*[contains(text(),'Acceso')]");
				Thread.sleep(2000);
				System.out.println("initSwitch:   "+java.time.LocalDateTime.now());  

				//		driver.switchTo().frame("tab-empresas-iframe");
				WebElement iframe = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("tab-empresas-iframe")));

				driver.switchTo().frame(iframe);

				System.out.println("endSwitch:   "+java.time.LocalDateTime.now());  
				auxBotonEntrar=Constantes.Login_BotonEntrar;

			}else{
				if(entorno.contains("integrado")){
					utils.Log.write("Login antiguo en Integrado:  "+currentUrlBrower);
				//	WebElement botonRedirect  = driver.findElement( By.xpath("//*[@id='kyop-redirect-button']"));
					Utils.clickEnElVisiblePrimero(driver,"//*[@id='kyop-redirect-button']");
					utils.Log.write("Hacemos nuevo paso Login URL:  "+currentUrlBrower);
					Thread.sleep(2000);

					WebElement element  = driver.findElement( By.xpath("//header//*[contains(text(),'Acceso')]"));

					Utils.clickEnElVisiblePrimero(driver,"//header//*[contains(text(),'Acceso')]");
					Thread.sleep(2000);
					System.out.println("initSwitch:   "+java.time.LocalDateTime.now());  

					//		driver.switchTo().frame("tab-empresas-iframe");
					WebElement iframe = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("tab-empresas-iframe")));

					driver.switchTo().frame(iframe);

					System.out.println("endSwitch:   "+java.time.LocalDateTime.now());  
					auxBotonEntrar=Constantes.Login_BotonEntrar;

				}else{
					utils.Log.write("Login antiguo en Produccion:  "+currentUrlBrower);
					auxBotonEntrar=Constantes.Login_BotonEntrar_Ant;

				}

			}



			//Si se guardo el usuario
			if (Utils.isDisplayedSinBefore(driver, Constantes.changeUserLabel, 0))
				Utils.clickEnElVisiblePrimero(driver, Constantes.changeUserLabel);

			Utils.introducirTextoEnElVisible(driver, Constantes.Login_Referencia, usuario.getReferencia());

			Utils.introducirTextoEnElVisible(driver, Constantes.Login_Usuario, usuario.getCodUsuario());

			Utils.introducirTextoEnElVisible(driver, Constantes.Login_Password, usuario.getPassword());

			Utils.clickEnElVisiblePrimero(driver, auxBotonEntrar);

			Thread.sleep(1500);

			if (Utils.isDisplayed(driver, Constantes.botonActivarDatosPersonales, 5))
				Utils.clickEnElVisiblePrimero(driver, Constantes.botonActivarDatosPersonales);

		}

		if(Utils.isDisplayed(driver, Constantes.BotonOKMessageDialog, 3)){
			Utils.clickEnElVisible(driver, Constantes.BotonOKMessageDialog);
			Thread.sleep(4000);
		}


		if(!Utils.isDisplayed(driver, Constantes.BotonDesconectar, 10))
		{
			if(Utils.isDisplayed(driver, Constantes.BotonOKMessageDialog, 5))
			{
				Utils.clickEnElVisiblePrimero(driver, Constantes.BotonOKMessageDialog);
			}
		}

		Utils.aceptarCampaniaSiSale(driver);

		//		if(Utils.isDisplayed(driver, Constantes.FrameCampana, 0))
		//		{
		//			Utils.cambiarFrameDadoVisible(driver, Constantes.FrameCampana);
		//			if(Utils.isDisplayed(driver, Constantes.continuarCampania, 1))
		//			{
		//				Utils.clickEnElVisible(driver, Constantes.continuarCampania);
		//				try
		//				{
		//					driver.switchTo().defaultContent();
		//					WebElement frame = driver.findElement(By.xpath(Constantes.FrameCampana));
		//					driver.switchTo().frame(frame);
		//					if(Utils.isDisplayed(driver, Constantes.ActivarTokenOtroMomento, 5))
		//					{
		//						Utils.clickEnElVisible(driver, Constantes.ActivarTokenOtroMomento);
		//						Thread.sleep(3500);
		//					}
		//					
		//					if (Utils.isDisplayed(driver, Constantes.divCerrarVentanaModal, 1))
		//						Utils.clickEnElVisiblePrimero(driver, Constantes.divCerrarVentanaModal);
		//				}
		//				catch(Exception e)
		//				{
		//
		//				}
		//			}
		//			driver.switchTo().defaultContent();
		//		}


		if (Utils.isDisplayedSinBefore(driver, Constantes.divCerrarVentanaModal, 0))
			Utils.clickEnElVisiblePrimero(driver, Constantes.divCerrarVentanaModal);

		if (Utils.isDisplayed(driver, Constantes.campanaDesplegableFlecha, 0)) {
			Atomic.click(driver, Constantes.campanaDesplegableFlecha);
		}

		Utils.esperaHastaApareceSinSalir(driver, Constantes.BotonDesconectar, 40);
		//		boolean esVisible = Utils.isDisplayed(driver, Constantes.BotonDesconectar, 40);
		// esperarVisibleTiempo(driver,Constantes.BotonDesconectar,20);

		if(!Utils.isDisplayed(driver, Constantes.BotonDesconectar, 0))
			throw new Exception("No se pudo realizar el login. Comprobar captura");

	}


	public static void loginTipoV7(WebDriver driver, Usuario usuario) throws Exception
	{

		String idioma = params.getValorPropiedad("idioma").toLowerCase();

		String pais = params.getValorPropiedad("pais").toLowerCase();
		String entorno = params.getValorPropiedad("entorno").toLowerCase();
		String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
		String url = devuelveURLApartirDePaisYEntorno(rutaURLs, pais, entorno);

		if(!existeElemento(driver, By.xpath(Constantes.Login_Referencia)))
			driver.get(url);

		String idiomaActual = "";

		if(Utils.isDisplayed(driver, Constantes.ComboIdiomas, 2))
		{
			idiomaActual = driver.findElement(By.xpath(Constantes.ComboIdiomas)).getText();
		}
		else if(Utils.isDisplayed(driver, Constantes.comboIdiomasCompass, 2))
		{
			idiomaActual = driver.findElement(By.xpath(Constantes.comboIdiomasCompass)).getText();
		}


		if(idiomaActual.toLowerCase().indexOf(idioma) < 0)
		{
			Utils.cambiarIdioma(driver, idioma);
		}

		//Si sale un modal raro
		if (Utils.isDisplayedSinBefore(driver, Constantes.cerrarModalLogin, 1))
			Utils.clickEnElVisiblePrimero(driver, Constantes.cerrarModalLogin);
		
		//Si se guardo el usuario
		if (Utils.isDisplayed(driver, Constantes.changeUserLabel, 1))
			Utils.clickEnElVisible(driver, Constantes.changeUserLabel);
		
		driver.findElement(By.xpath(Constantes.Login_Referencia)).clear();
		driver.findElement(By.xpath(Constantes.Login_Referencia)).sendKeys(usuario.getReferencia());
		driver.findElement(By.xpath(Constantes.Login_Usuario)).clear();
		driver.findElement(By.xpath(Constantes.Login_Usuario)).sendKeys(usuario.getCodUsuario());
		driver.findElement(By.xpath(Constantes.Login_Password)).clear();
		driver.findElement(By.xpath(Constantes.Login_Password)).sendKeys(usuario.getPassword());

		if(Utils.isDisplayed(driver, Constantes.Login_BotonEntrar, 1))
			Utils.clickEnElVisiblePrimero(driver, Constantes.Login_BotonEntrar);
		else if(Utils.isDisplayed(driver, Constantes.Login_BotonEntrarCompass, 1))
			Utils.clickEnElVisiblePrimero(driver, Constantes.Login_BotonEntrarCompass);

	}


	public static void loginLocal(WebDriver driver, Usuario usuario) throws Exception
	{

		String pais = params.getValorPropiedad("pais").toLowerCase();
		String entorno = params.getValorPropiedad("entorno").toLowerCase();
		String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
		String url = devuelveURLApartirDePaisYEntorno(rutaURLs, pais, entorno);

		if(!existeElemento(driver, By.xpath(Constantes.LoginLocal_Referencia)))
			driver.get(url);
		driver.findElement(By.xpath(Constantes.LoginLocal_Referencia)).clear();
		driver.findElement(By.xpath(Constantes.LoginLocal_Referencia)).sendKeys(usuario.getReferencia());
		driver.findElement(By.xpath(Constantes.LoginLocal_Usuario)).clear();
		driver.findElement(By.xpath(Constantes.LoginLocal_Usuario)).sendKeys(usuario.getCodUsuario());
		driver.findElement(By.xpath(Constantes.LoginLocal_Password)).clear();
		driver.findElement(By.xpath(Constantes.LoginLocal_Password)).sendKeys(usuario.getPassword());

		Utils.seleccionarComboContieneXPath(driver, Constantes.LoginLocal_ComboPais, pais);

		Utils.clickEnElVisiblePrimero(driver, Constantes.LoginLocal_BotonEntrar);
	}


	public static void loginUsuarioPassword(WebDriver driver, String referencia, String usuario, String password) throws Exception
	{
		
		Usuario user = new Usuario(referencia);
		user.setCodUsuario(usuario);
		user.setPassword(password);
		
		Utils.login(driver, user);

	}


	public static void loginUsuarioPasswordSinComprobar(WebDriver driver, String referencia, String usuario, String password) throws Exception
	{
		
		//Si sale un modal raro
		if (Utils.isDisplayedSinBefore(driver, Constantes.cerrarModalLogin, 1))
			Utils.clickEnElVisiblePrimero(driver, Constantes.cerrarModalLogin);
		
		//Si se guardo el usuario
		if (Utils.isDisplayed(driver, Constantes.changeUserLabel, 1))
			Utils.clickEnElVisible(driver, Constantes.changeUserLabel);

		driver.findElement(By.xpath(Constantes.Login_Referencia)).sendKeys(referencia);
		driver.findElement(By.xpath(Constantes.Login_Usuario)).sendKeys(usuario);
		driver.findElement(By.xpath(Constantes.Login_Password)).sendKeys(password);
		Utils.clickJS(driver, Constantes.Login_BotonEntrar);

		if(existeElementoTiempo(driver, By.xpath(Constantes.BotonOKMessageDialog), 10))
			Utils.clickJS(driver, Constantes.BotonOKMessageDialog);
		Thread.sleep(4000);
		Utils.isDisplayed(driver, Constantes.BotonDesconectar, 20);
		
		aceptarCampaniaSiSale(driver);

	}


	public static void loginCMP(WebDriver driver) throws Exception
	{
		params.cargarPropiedades();
		String referenciaSaltoCMP = params.getValorPropiedad("referenciaSaltoCMP").toLowerCase();
		String usuartioSaltoCMP = params.getValorPropiedad("usuartioSaltoCMP").toLowerCase();
		String passwordSaltoCMP = params.getValorPropiedad("passwordSaltoCMP").toLowerCase();
		String tokenSaltoCMP = params.getValorPropiedad("tokenSaltoCMP").toLowerCase();

		Utils.esperarVisibleTiempo(driver, Constantes.Login_Referencia, 10);

		Utils.introducirTextoEnElVisible(driver, Constantes.Login_Referencia, referenciaSaltoCMP);
		Utils.introducirTextoEnElVisible(driver, Constantes.Login_Usuario, usuartioSaltoCMP);
		Utils.introducirTextoEnElVisible(driver, Constantes.Login_Password, passwordSaltoCMP);

		Utils.clickEnElVisible(driver, Constantes.botonContinuarValue);

		// En producci�n e int lo va a pedir
		if(Utils.isDisplayed(driver, Constantes.tokenLogin, 10))
		{
			boolean validacion = false;
			int intentosValidacion = 0;
			while(!validacion && intentosValidacion < 3)
			{
				String mensaje = "Introduzca la clave de token: " + tokenSaltoCMP + " del usuario: " + usuartioSaltoCMP + " para el SALTO CMP";
				if(intentosValidacion > 0)
				{
					mensaje = mensaje + ". Tiene " + (3 - intentosValidacion) + " intento(s) m�s";
				}

				String resul = JOptionPane.showInputDialog(null, mensaje, "Informaci�n", JOptionPane.INFORMATION_MESSAGE);
				Utils.introducirTextoEnElVisible(driver, Constantes.tokenLogin, resul);
				Utils.clickEnElVisible(driver, Constantes.botonFinalizar);
				Thread.sleep(2000);
				if(!Utils.isDisplayed(driver, Constantes.botonAdministracionCMP, 10))
					intentosValidacion++;
				else
					validacion = true;
				if(intentosValidacion == 3)
					Log.write("Se han superado el n�mero de reintentos de acceso con token");
			}
		}

		if(existeElementoTiempo(driver, By.xpath(Constantes.BotonOKMessageDialog), 5))
			Utils.clickEnElVisiblePrimero(driver, Constantes.BotonOKMessageDialog);
		Thread.sleep(4000);
		boolean esVisible = Utils.isDisplayed(driver, Constantes.BotonDesconectar, 20);
		// esperarVisibleTiempo(driver,Constantes.BotonDesconectar,20);

		if(!esVisible)
			throw new Exception("No se pudo realizar el login en CMP. Comprobar captura");

		String idioma = params.getValorPropiedad("idioma").toLowerCase();

		String idiomaActual = driver.findElement(By.xpath(Constantes.idiomaActualCMP)).getText();
		if(idiomaActual.toLowerCase().indexOf(idioma) < 0)
		{
			Utils.cambiarIdiomaCMP(driver, idioma);
		}

		Utils.esperarProcesandoPeticionMejorado(driver);

	}


	public static void primerLogin(WebDriver driver, Usuario usuario, Resultados resultado) throws Exception
	{
		params.cargarPropiedades();
		String idioma = params.getValorPropiedad("idioma").toLowerCase();
		String pais = params.getValorPropiedad("pais").toLowerCase();
		String entorno = params.getValorPropiedad("entorno");
		String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
		String url = devuelveURLApartirDePaisYEntorno(rutaURLs, pais, entorno);

		if(url == null || url.equals(""))
		{
			throw new Exception("No se pudo obtener la URL correspondiente a los par�metros: Pa�s = " + pais + ", Entorno = " + entorno + ". Revisar el archivo properties y la excel con las URLs.");
		}

		// Compass diferente
		if(pais.equalsIgnoreCase("Compass") && !entorno.equalsIgnoreCase("Local"))
		{
			primerLoginCompass(driver, usuario, resultado, url);
		}
		

		else
		{

			if(pais.equalsIgnoreCase("Compass"))
				driver.get(url);

			// Si no existe campo para login, se intenta recargar la aplicaci�n
			if(!Utils.isDisplayedSinBefore(driver, Constantes.Login_Referencia, 1)||!Utils.isDisplayed(driver, Constantes.botonAcceso, 1))
				driver.get(url);

			if(!Utils.estamosEnLatam())
			{
				if (Utils.isDisplayed(driver, Constantes.ComboIdiomas, 2)) {
					String idiomaActual = driver.findElement(By.xpath(Constantes.ComboIdiomas)).getText();
					if(idiomaActual.toLowerCase().indexOf(idioma) < 0)
					{
						Utils.cambiarIdioma(driver, idioma);
					}
				}
			}
			
//			//Si sale un modal raro
//			if (Utils.isDisplayedSinBefore(driver, Constantes.cerrarModalLogin, 1))
//				Utils.clickEnElVisiblePrimero(driver, Constantes.cerrarModalLogin);
//			
//			//Si se guardo el usuario
//			if (Utils.isDisplayed(driver, Constantes.changeUserLabel, 1))
//				Utils.clickEnElVisible(driver, Constantes.changeUserLabel);
//
//			if(Utils.estamosEnLatam())
//			{
//				driver.findElement(By.xpath(Constantes.LoginLocal_Referencia)).clear();
//				driver.findElement(By.xpath(Constantes.LoginLocal_Usuario)).clear();
//				driver.findElement(By.xpath(Constantes.LoginLocal_Password)).clear();
//			}
//			
//			Utils.RTS_Update(usuario.getReferencia(), usuario.getCodUsuario());
//
//			Utils.introducirTextoEnElVisible2(driver, Constantes.Login_Referencia, usuario.getReferencia());
//			Utils.introducirTextoEnElVisible2(driver, Constantes.Login_Usuario, usuario.getCodUsuario());
//			Utils.introducirTextoEnElVisible2(driver, Constantes.Login_Password, usuario.getPassword());
//
//			if(Utils.estamosEnLatam() && Utils.isDisplayed(driver, Constantes.LoginLocal_ComboPais, 2))
//				Utils.seleccionarComboContieneXPath(driver, Constantes.LoginLocal_ComboPais, pais);
//
//			if(Utils.isDisplayed(driver, Constantes.Login_BotonEntrar, 2))
//				Utils.clickEnElVisiblePrimero(driver, Constantes.Login_BotonEntrar);
//			else if(Utils.isDisplayed(driver, Constantes.Login_BotonEntrarCompass, 2))
//				Utils.clickEnElVisiblePrimero(driver, Constantes.Login_BotonEntrarCompass);
//
//			if(existeElementoTiempo(driver, By.xpath(Constantes.BotonOKMessageDialog), 10))
//				Utils.clickEnElVisiblePrimero(driver, Constantes.BotonOKMessageDialog);
//
//			Utils.esperaHastaApareceSinSalir(driver, Constantes.DivMensajeInfo, 10);
			
			//RTS!!!!! 
			//Si estamos en Integrado, podemos modificar la fecha de ultimo acceso
			Utils.RTS_Update(usuario.getReferencia(), usuario.getCodUsuario());

			if (Utils.isDisplayedSinBefore(driver, Constantes.cerrarModalLogin, 0))
				Utils.clickEnElVisiblePrimero(driver, Constantes.cerrarModalLogin);


			//Utils.clickEnElVisiblePrimero(driver,"//button[contains(text(),'Aceptar')]");

			String currentUrlBrower=driver.getCurrentUrl();
			String auxBotonEntrar="";
			if (currentUrlBrower.contains("bbva.es/empresas.html")){

				utils.Log.write("Hacemos nuevo paso Login URL:  "+currentUrlBrower);
				Thread.sleep(2000);

				WebElement element  = driver.findElement( By.xpath("//header//*[contains(text(),'Acceso')]"));

				Utils.clickEnElVisiblePrimero(driver,"//header//*[contains(text(),'Acceso')]");
				Thread.sleep(2000);
				System.out.println("initSwitch:   "+java.time.LocalDateTime.now());  

				//		driver.switchTo().frame("tab-empresas-iframe");
				WebElement iframe = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("tab-empresas-iframe")));

				driver.switchTo().frame(iframe);

				System.out.println("endSwitch:   "+java.time.LocalDateTime.now());  
				auxBotonEntrar=Constantes.Login_BotonEntrar;

			}else{
				if(entorno.contains("integrado")){
					utils.Log.write("Login antiguo en Integrado:  "+currentUrlBrower);
				//	WebElement botonRedirect  = driver.findElement( By.xpath("//*[@id='kyop-redirect-button']"));
					Utils.clickEnElVisiblePrimero(driver,"//*[@id='kyop-redirect-button']");
					utils.Log.write("Hacemos nuevo paso Login URL:  "+currentUrlBrower);
					Thread.sleep(2000);

					WebElement element  = driver.findElement( By.xpath("//header//*[contains(text(),'Acceso')]"));

					Utils.clickEnElVisiblePrimero(driver,"//header//*[contains(text(),'Acceso')]");
					Thread.sleep(2000);
					System.out.println("initSwitch:   "+java.time.LocalDateTime.now());  

					//		driver.switchTo().frame("tab-empresas-iframe");
					WebElement iframe = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("tab-empresas-iframe")));

					driver.switchTo().frame(iframe);

					System.out.println("endSwitch:   "+java.time.LocalDateTime.now());  
					auxBotonEntrar=Constantes.Login_BotonEntrar;

				}else{
					utils.Log.write("Login antiguo en Produccion:  "+currentUrlBrower);
					auxBotonEntrar=Constantes.Login_BotonEntrar_Ant;

				}

			}



			//Si se guardo el usuario
			if (Utils.isDisplayedSinBefore(driver, Constantes.changeUserLabel, 0))
				Utils.clickEnElVisiblePrimero(driver, Constantes.changeUserLabel);

			Utils.introducirTextoEnElVisible(driver, Constantes.Login_Referencia, usuario.getReferencia());

			Utils.introducirTextoEnElVisible(driver, Constantes.Login_Usuario, usuario.getCodUsuario());

			Utils.introducirTextoEnElVisible(driver, Constantes.Login_Password, usuario.getPassword());

			Utils.clickEnElVisiblePrimero(driver, auxBotonEntrar);

			Thread.sleep(1500);

			if (Utils.isDisplayed(driver, Constantes.botonActivarDatosPersonales, 5))
				Utils.clickEnElVisiblePrimero(driver, Constantes.botonActivarDatosPersonales);

		}

		if(Utils.isDisplayed(driver, Constantes.BotonOKMessageDialog, 3)){
			Utils.clickEnElVisible(driver, Constantes.BotonOKMessageDialog);
			Thread.sleep(4000);
		}

//


		//		if(Utils.isDisplayed(driver, Constantes.FrameCampana, 0))
		//		{
		//			Utils.cambiarFrameDadoVisible(driver, Constantes.FrameCampana);
		//			if(Utils.isDisplayed(driver, Constantes.continuarCampania, 1))
		//			{
		//				Utils.clickEnElVisible(driver, Constantes.continuarCampania);
		//				try
		//				{
		//					driver.switchTo().defaultContent();
		//					WebElement frame = driver.findElement(By.xpath(Constantes.FrameCampana));
		//					driver.switchTo().frame(frame);
		//					if(Utils.isDisplayed(driver, Constantes.ActivarTokenOtroMomento, 5))
		//					{
		//						Utils.clickEnElVisible(driver, Constantes.ActivarTokenOtroMomento);
		//						Thread.sleep(3500);
		//					}
		//					
		//					if (Utils.isDisplayed(driver, Constantes.divCerrarVentanaModal, 1))
		//						Utils.clickEnElVisiblePrimero(driver, Constantes.divCerrarVentanaModal);
		//				}
		//				catch(Exception e)
		//				{
		//
		//				}
		//			}
		//			driver.switchTo().defaultContent();
		//		}



			
			

			// Verificar mensaje de aviso
			if(!Utils.estamosEnLatam())
			{
				String mensajeEsperado = "La contrase�a con la que ha accedido es de un solo uso. Defina su contrase�a para posteriores conexiones.";
				String mensajeActual = driver.findElement(By.xpath(Constantes.DivMensajeInfo)).getText();
				vpWarning(driver, resultado, "Validar texto cambio password. Esperado: "+mensajeActual, true, mensajeActual.contains(mensajeEsperado), "FALLO");
			}

			Utils.introducirTextoEnElVisible2(driver, Constantes.Login_AntiguaPassword, usuario.getPassword());

			// Cambio de password
			if(Utils.estamosEnLatam())
				usuario.setPassword("qwerty12");
			else
				usuario.setPassword("1111bbbb");
			
			Utils.introducirTextoEnElVisible2(driver, Constantes.Login_NuevaPassword, usuario.getPassword());
			Utils.introducirTextoEnElVisible2(driver, Constantes.Login_ConfirmarPassword, usuario.getPassword());

			Utils.capturaIntermedia(driver, resultado, "Cambio de password en primer acceso con el usuario");

			Utils.clickEnElVisiblePrimero(driver, Constantes.BotonContinuarAzulGrandote);

			if(Utils.isDisplayed(driver, Constantes.DivMensajeInfo, 5))
			{
				// Verificar mensaje de aviso
				// String mensajeEsperado="Utilizar� esta clave para firmar operaciones y validar cambios en la administraci�n de usuarios.";
				String mensajeEsperado = "En BBVA net cash, la contrase�a y la clave de firma son diferentes. Defina esta �ltima ahora para poder proceder a la firma de �rdenes y validaci�n";
				String mensaje = "la contrase�a y la clave de firma son diferentes. Defina esta �ltima";
				String mensajeAlternativo = "Utilizar� esta clave para firmar operaciones";
				String mensajeActual = driver.findElement(By.xpath(Constantes.DivMensajeInfo)).getText();
				vpWarning(driver, resultado, "Validar texto sobre clave para firmar operaciones. Mensaje esperado: " + mensajeEsperado, true,
						mensajeActual.toLowerCase().contains(mensaje.toLowerCase()) || mensajeActual.toLowerCase().contains(mensajeAlternativo.toLowerCase()), "FALLO");
			}
		}
	


	public static void primerLoginCompass(WebDriver driver, Usuario usuario, Resultados resultado, String url) throws Exception
	{

//		if (!existeElemento(driver,By.xpath(Constantes.Login_Referencia)))
		driver.get(url);
		
		//Si sale un modal raro
		if (Utils.isDisplayedSinBefore(driver, Constantes.cerrarModalLogin, 1))
			Utils.clickEnElVisiblePrimero(driver, Constantes.cerrarModalLogin);
		
		//Si se guardo el usuario
		if (Utils.isDisplayed(driver, Constantes.changeUserLabel, 1))
			Utils.clickEnElVisible(driver, Constantes.changeUserLabel);

		driver.findElement(By.xpath(Constantes.Login_Referencia)).clear();
		driver.findElement(By.xpath(Constantes.Login_Referencia)).sendKeys(usuario.getReferencia());
		driver.findElement(By.xpath(Constantes.Login_Usuario)).clear();
		driver.findElement(By.xpath(Constantes.Login_Usuario)).sendKeys(usuario.getCodUsuario());
		driver.findElement(By.xpath(Constantes.Login_Password)).clear();
		driver.findElement(By.xpath(Constantes.Login_Password)).sendKeys(usuario.getPassword());

		if(Utils.isDisplayed(driver, Constantes.Login_BotonEntrar, 2))
			Utils.clickJS(driver, Constantes.Login_BotonEntrar);
		else if(Utils.isDisplayed(driver, Constantes.Login_BotonEntrarCompass, 2))
			Utils.clickJS(driver, Constantes.Login_BotonEntrarCompass);

		if(existeElementoTiempo(driver, By.xpath(Constantes.BotonOKMessageDialog), 5))
			Utils.clickJS(driver, Constantes.BotonOKMessageDialog);

		// TOREMOVE QUITAR CUANDO NO SALGA USUARIO BLOQUEADO
		if(Utils.estamosEnLatam() && reintento)
		{
			reintento = false;
			if(Utils.isDisplayed(driver, Constantes.DivMensajeAviso, 10))
			{
				driver.get(url);
				primerLogin(driver, usuario, resultado);
				return;
			}
		}

		Utils.esperarVisibleTiempo(driver, Constantes.CompassUsuario, 20);


		// Cambiamos la password del usuario
		Utils.introducirTextoEnElVisible(driver, Constantes.CompassUsuario, usuario.getCodUsuario());
		Utils.introducirTextoEnElVisible(driver, Constantes.CompassOldPass, usuario.getPassword());

		usuario.setPassword("QWERTY12");
		Utils.introducirTextoEnElVisible(driver, Constantes.CompassNewPass, usuario.getPassword());
		Utils.introducirTextoEnElVisible(driver, Constantes.CompassConfirmPass, usuario.getPassword());

		Utils.clickEnElVisible(driver, Constantes.CompassConfirmar);


		// Nueva Clave de Operaciones
		if(Utils.isDisplayed(driver, Constantes.CompassClaveOpe, 20))
		{
			Utils.esperarVisibleTiempo(driver, Constantes.CompassClaveOpe, 20);
			usuario.setClaveOp("QWERTY99");
			Utils.introducirTextoEnElVisible(driver, Constantes.CompassClaveOpe, usuario.getClaveOp());
			Utils.introducirTextoEnElVisible(driver, Constantes.CompassConfirmarClaveOpe, usuario.getClaveOp());

			Utils.clickEnElVisible(driver, Constantes.CompassConfirmar);


			if(existeElementoTiempo(driver, By.xpath(Constantes.BotonOKMessageDialog), 5))
				Utils.clickEnElVisiblePrimero(driver, Constantes.BotonOKMessageDialog);
			// driver.findElement(By.xpath(Constantes.BotonCancelarMessageDialog)).click();

		}

		// TOREMOVE QUITAR CUANDO REDIRIJA TRAS CLAVE OP
		if(Utils.estamosEnLatam() && !Utils.isDisplayed(driver, Constantes.CompassClaveOpe, 10))
		{
			driver.get(url);
			Utils.esperarVisibleTiempoSinSalir(driver, Constantes.Login_Referencia, 10);
			driver.findElement(By.xpath(Constantes.Login_Referencia)).clear();
			driver.findElement(By.xpath(Constantes.Login_Referencia)).sendKeys(usuario.getReferencia());
			driver.findElement(By.xpath(Constantes.Login_Usuario)).clear();
			driver.findElement(By.xpath(Constantes.Login_Usuario)).sendKeys(usuario.getCodUsuario());
			driver.findElement(By.xpath(Constantes.Login_Password)).clear();
			driver.findElement(By.xpath(Constantes.Login_Password)).sendKeys(usuario.getPassword());

			if(Utils.isDisplayed(driver, Constantes.Login_BotonEntrar, 2))
				Utils.clickJS(driver, Constantes.Login_BotonEntrar);
			else if(Utils.isDisplayed(driver, Constantes.Login_BotonEntrarCompass, 2))
				Utils.clickJS(driver, Constantes.Login_BotonEntrarCompass);

			if(existeElementoTiempo(driver, By.xpath(Constantes.BotonOKMessageDialog), 5))
				Utils.clickJS(driver, Constantes.BotonOKMessageDialog);
		}

		Utils.esperarVisibleTiempoSinSalir(driver, Constantes.BotonDesconectar, 40);

	}


	public static void cambiarIdioma(WebDriver driver, String idioma) throws Exception
	{
		if(Utils.isDisplayed(driver, Constantes.ComboIdiomas, 2))
			Utils.clickEnElVisiblePrimero(driver, Constantes.ComboIdiomas);
		else if(Utils.isDisplayed(driver, Constantes.comboIdiomasCompass, 2))
			Utils.clickEnElVisiblePrimero(driver, Constantes.comboIdiomasCompass);
		if(idioma.equalsIgnoreCase("ES"))
			Utils.clickEnElVisiblePrimero(driver, Constantes.IdiomaEspanol);
		else if(idioma.equalsIgnoreCase("CAT"))
			Utils.clickEnElVisiblePrimero(driver, Constantes.IdiomaCatalan);
		else if(idioma.equalsIgnoreCase("PO"))
			Utils.clickEnElVisiblePrimero(driver, Constantes.IdiomaPortugues);
		else if(idioma.equalsIgnoreCase("EN"))
			Utils.clickEnElVisiblePrimero(driver, Constantes.IdiomaIngles);
	}


	public static void cambiarIdiomaCMP(WebDriver driver, String idioma) throws Exception
	{
		Utils.clickEnElVisiblePrimero(driver, Constantes.comboIdiomaCMP);
		if(idioma.equalsIgnoreCase("ES"))
			Utils.clickEnElVisiblePrimero(driver, Constantes.idiomaES_CMP);
	}


	public static void loginPorDefecto(WebDriver driver) throws Exception
	{

		// TOREMOVE cuando Compass redirija bien el logout
		if(Utils.estamosEnCompass())
		{
			String pais = params.getValorPropiedad("pais").toLowerCase();
			String entorno = params.getValorPropiedad("entorno");
			String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
			String url = devuelveURLApartirDePaisYEntorno(rutaURLs, pais, entorno);
			driver.get(url);
		}

		params.cargarPropiedades();
		// String rutaCSV = params.getValorPropiedad("rutaDatosFijos");
		String usuarioPorDefecto = params.getValorPropiedad("usuarioPorDefecto");
		String referenciaPorDefecto = params.getValorPropiedad("referenciaPorDefecto");
		String entorno = params.getValorPropiedad("entorno");

		// new Usuario();
		// Usuario UsuarioPrueba = utils.Usuario.obtenerUsuarioPorReferenciaCodigo(rutaCSV,params.getValorPropiedad("referenciaPorDefecto"), params.getValorPropiedad("usuarioPorDefecto"));
		Usuario UsuarioPrueba = Funciones.devuelveUsuarioFijo(referenciaPorDefecto, usuarioPorDefecto, entorno);

		if(UsuarioPrueba == null)
			throw new Exception("No hay usuario para realizar la prueba con nombre: " + usuarioPorDefecto + " referencia: " + referenciaPorDefecto + " para el entorno: " + entorno
					+ ". Revisar el archivo properties.");

		Utils.login(driver, UsuarioPrueba);
	}


	public static void loginAdmin(WebDriver driver) throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String ruta = params.getValorPropiedad("rutaDatosFijos");

		WorkbookSettings ws = new WorkbookSettings(); 
		ws.setEncoding("Cp1252");
		
		Workbook excelLogin = Workbook.getWorkbook(new File(ruta),ws);
		Sheet sheet1 = excelLogin.getSheet("Administrador");

		int fila = sheet1.findCell("administrador").getRow();

		String referencia = sheet1.getCell(sheet1.findCell("Referencia").getColumn(), fila).getContents();
		String usuario = sheet1.getCell(sheet1.findCell("Usuario").getColumn(), fila).getContents();
		String password = sheet1.getCell(sheet1.findCell("Password").getColumn(), fila).getContents();

		// Si no existe campo para login, se intenta recargar la aplicaci�n
		if(!existeElemento(driver, By.id("cod_emp")))
		{
			String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
			String pais = params.getValorPropiedad("pais");
			String entorno = params.getValorPropiedad("entorno");
			String url = devuelveURLApartirDePaisYEntorno(rutaURLs, pais, entorno);
			driver.get(url);
		}
		
		//Si se guardo el usuario
		if (Utils.isDisplayed(driver, Constantes.changeUserLabel, 1))
			Utils.clickEnElVisible(driver, Constantes.changeUserLabel);
		
		driver.findElement(By.id("cod_emp")).sendKeys(referencia);
		driver.findElement(By.id("cod_usu")).sendKeys(usuario);
		driver.findElement(By.id("eai_password")).sendKeys(password);
		Utils.clickEnElVisiblePrimero(driver, "//button[@type='submit']");

		esperarVisibleTiempo(driver, "//button[@id='botonDesconectar']", 20);

	}


	public static void aceptarCampaniaSiSale(WebDriver driver) throws Exception
	{
		Utils.setTimeOut(driver, 1);
		driver.switchTo().defaultContent();
		
		int contador = 0;
		
		//No quitar
		Thread.sleep(4000);
		
		while(Utils.isDisplayed(driver, Constantes.frameCampanaGenerico, 1) && contador < 4){
			
			Utils.cambiarFrameDadoVisible(driver, Constantes.frameCampanaGenerico);

			if(Utils.isDisplayed(driver, Constantes.continuarCampania, 1))
			{
				if (isDisplayedCSS(driver,Constantes.botonContinuarRTSDisabled,1))
//				if (driver.findElements(By.cssSelector(Constantes.botonContinuarRTSDisabled)).size()>0)
					Utils.ejecutarJS(driver, "document.querySelector(\""+Constantes.botonContinuarRTSDisabled+"\").removeAttribute(\"disabled\")");
							
				if (Utils.isDisplayed(driver, Constantes.continuarCampania, 1))
					Atomic.click(driver, Constantes.continuarCampania);
				
				contador++;
				
				try
				{
					driver.switchTo().defaultContent();
					Utils.cambiarFrameDado(driver, Constantes.frameCampanaGenerico);
					
					if(Utils.isDisplayed(driver, Constantes.ActivarTokenOtroMomento, 1))
					{
						Utils.clickEnElVisible(driver, Constantes.ActivarTokenOtroMomento);
						Thread.sleep(3500);
					}
					
					if (Utils.isDisplayed(driver, Constantes.divCerrarVentanaModal, 0))
						Utils.clickEnElVisiblePrimero(driver, Constantes.divCerrarVentanaModal);
				}
				catch(Exception e)
				{

				}
			}

			driver.switchTo().defaultContent();
			
			if (Utils.isDisplayedSinBefore(driver, Constantes.divCerrarVentanaModal, 0))
				Utils.clickEnElVisiblePrimero(driver, Constantes.divCerrarVentanaModal);
			
			Utils.setDefaultTimeOut(driver);
			
		}
		

//		if(Utils.isDisplayed(driver, Constantes.FrameCampana, 0))
//		{
//			Utils.cambiarFrameDadoVisible(driver, Constantes.FrameCampana);
//
//			if(Utils.isDisplayed(driver, Constantes.continuarCampania, 1))
//			{
//				Utils.clickEnElVisible(driver, Constantes.continuarCampania);
//				try
//				{
//					driver.switchTo().defaultContent();
//					WebElement frame = driver.findElement(By.xpath(Constantes.FrameCampana));
//					driver.switchTo().frame(frame);
//					if(Utils.isDisplayed(driver, Constantes.ActivarTokenOtroMomento, 5))
//					{
//						Utils.clickEnElVisible(driver, Constantes.ActivarTokenOtroMomento);
//						Thread.sleep(3500);
//					}
//					
//					if (Utils.isDisplayed(driver, Constantes.divCerrarVentanaModal, 1))
//						Utils.clickEnElVisiblePrimero(driver, Constantes.divCerrarVentanaModal);
//				}
//				catch(Exception e)
//				{
//
//				}
//			}
//
//			driver.switchTo().defaultContent();
//		} 
//		
//		else if(Utils.isDisplayed(driver, Constantes.FrameCampana2, 1))
//		
//		{
//			Utils.cambiarFrameDadoVisible(driver, Constantes.FrameCampana2);
//			if(Utils.isDisplayed(driver, Constantes.continuarCampania, 1))
//			{
//				Utils.clickEnElVisible(driver, Constantes.continuarCampania);
//				try
//				{
//					driver.switchTo().defaultContent();
//					WebElement frame = driver.findElement(By.xpath(Constantes.FrameCampana2));
//					driver.switchTo().frame(frame);
//					if(Utils.isDisplayed(driver, Constantes.ActivarTokenOtroMomento, 5))
//					{
//						Utils.clickEnElVisible(driver, Constantes.ActivarTokenOtroMomento);
//						Thread.sleep(3500);
//					}
//					
//					if (Utils.isDisplayed(driver, Constantes.divCerrarVentanaModal, 1))
//						Utils.clickEnElVisiblePrimero(driver, Constantes.divCerrarVentanaModal);
//				}
//				catch(Exception e)
//				{
//
//				}
//			}
//			
//			else if (Utils.isDisplayed(driver, Constantes.divCerrarVentanaModal, 1))
//				Utils.clickEnElVisiblePrimero(driver, Constantes.divCerrarVentanaModal);
//
//			driver.switchTo().defaultContent();
//		}
		
		
		if (Utils.isDisplayedSinBefore(driver, Constantes.divCerrarVentanaModal, 2))
			Atomic.click(driver, Constantes.divCerrarVentanaModal);
		
		if (Utils.isDisplayedSinBefore(driver, Constantes.campanaDesplegableFlecha , 0)) {
			Atomic.click(driver, Constantes.campanaDesplegableFlecha);
		}
		
	}


	public static void seleccionarCombo(WebDriver driver, String Combo, String Opcion) throws Exception
	{
		String xpath = "//select[@id='" + Combo + "'or @name='" + Combo + "']";
		
		Utils.seleccionarComboXPath(driver, xpath, Opcion);
		
//		Utils.esperaHastaApareceSinSalir(driver, xpath, 10);
//		
//		Select select=new Select(Utils.devuelveElementoVisibleYEnabled(driver, xpath));
//
//		select.selectByVisibleText(Opcion);
		
//		WebElement combo = driver.findElement(By.xpath(xpath));
//		List<WebElement> opcionesCombo = combo.findElements(By.tagName("option"));
//		for(WebElement opcion : opcionesCombo)
//		{
//			if(opcion.getText().equals(Opcion))
//				opcion.click();
//		}
	}


	public static void seleccionarComboFallando(WebDriver driver, String Combo, String Opcion) throws Exception
	{
		boolean encontrado = false;
		WebElement combo = driver.findElement(By.xpath("//select[@id='" + Combo + "'or@name='" + Combo + "']"));
		
		Select select=new Select(Utils.devuelveElementoVisibleYEnabled(driver,"//select[@id='" + Combo + "'or@name='" + Combo + "']"));
		
		for(WebElement opcion : select.getOptions())
		{
			if(opcion.getText().equals(Opcion))
			{
				opcion.click();
				encontrado = true;
			}

		}

		if(!encontrado)
		{
			throw new Exception("No se encontr� en el combo: " + Combo + " el valor: " + Opcion);
		}
	}


	public static void seleccionarComboXPath(WebDriver driver, String xpath, String Opcion) throws Exception
	{
		
		if (xpath.contains("//select")){
		
			Utils.esperaHastaApareceSinSalir(driver, xpath, 10);
			
			Select select=new Select(Utils.devuelveElementoVisibleYEnabled(driver, xpath));
			select.selectByVisibleText(Opcion);
		}
		
		else 
			seleccionarComboDiv(driver, xpath, Opcion);
		
//		WebElement combo = driver.findElement(By.xpath(xpath));
//		List<WebElement> opcionesCombo = combo.findElements(By.tagName("option"));
//		for(WebElement opcion : opcionesCombo)
//		{
//			if(opcion.getText().equalsIgnoreCase(Opcion)){
//				opcion.click();
//				break;
//			}
//		}
	}


	public static void seleccionarComboXPath(WebDriver driver, WebElement elementoCombo, String Opcion)
	{
		WebElement combo = elementoCombo;
		
		Select select=new Select(combo);
		
		for(WebElement opcion : select.getOptions())
		{
			if(opcion.getText().equalsIgnoreCase(Opcion))
				opcion.click();
		}
	}


	public static void seleccionarComboContieneXPath(WebDriver driver, String xpath, String Opcion) throws Exception
	{
		
		Utils.esperaHastaApareceSinSalir(driver, xpath, 10);
		
		Select select=new Select(Utils.devuelveElementoVisibleYEnabled(driver, xpath));
		
		for(WebElement opcion : select.getOptions())
		{
			String opcionX = opcion.getText().toLowerCase();
			if(opcionX.indexOf(Opcion.toLowerCase()) > -1){
				opcion.click();
				break;
			}
			if(opcionX.equals(opcion)){
				opcion.click();
				break;
			}
		}
	}
	
	public static void seleccionarComboContieneXPathFallando(WebDriver driver, String xpath, String Opcion) throws Exception
	{
		
		Utils.esperaHastaApareceSinSalir(driver, xpath, 10);
		
		Select select=new Select(Utils.devuelveElementoVisibleYEnabled(driver, xpath));
		
		boolean encontrado = false;
		
		for(WebElement opcion : select.getOptions())
		{
			String opcionX = opcion.getText().toLowerCase();
			if(opcionX.indexOf(Opcion.toLowerCase()) > -1){
				opcion.click();
				encontrado=true;
				break;
			}
			if(opcionX.equals(opcion)){
				opcion.click();
				encontrado=true;
				break;
			}
		}
		
		if (!encontrado)
			throw new Exception ("No se encontr� la opci�n: "+Opcion + " en el combo.");
		
	}


	public static String seleccionarComboDevuelveValue(WebDriver driver, String xpath) throws Exception
	{
		Select select=new Select(Utils.devuelveElementoVisibleYEnabled(driver, xpath));

		String value = "";

		for(WebElement opcion : select.getOptions())
		{
			String opcionX = opcion.getText().toLowerCase();
			opcion.click();
			value = opcion.getAttribute("value");
			break;
		}

		return value;
	}


	public static void seleccionarComboNOContieneXPath(WebDriver driver, String xpath, String Opcion) throws Exception
	{
		Select select=new Select(Utils.devuelveElementoVisibleYEnabled(driver, xpath));

		for(WebElement opcion : select.getOptions())
		{
			String opcionX = opcion.getText().toLowerCase();
			if(opcionX.indexOf(Opcion.toLowerCase()) == -1)
			{
				opcion.click();
				break;
			}
		}
	}


	// Validar valida con el usuario y referencia por defecto
	public static void validar(WebDriver driver) throws Exception
	{
		int intentosValidacion = 0;
		boolean validacion = false;
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosFijos");
		String entorno = params.getValorPropiedad("entorno");

		// Si el entorno es DESARROLLO, no valida los campos de claveOperacion y Token, pero aparecen

		if(entorno.equalsIgnoreCase("Desarrollo"))
		{
			
			if (Utils.isDisplayed(driver, Constantes.CampoClaveOperaciones, 2))
				Utils.introducirTextoEnElVisible2(driver, Constantes.CampoClaveOperaciones, "clavefalsa01");

			if (Utils.isDisplayed(driver, Constantes.CampoClaveToken, 2))
				Utils.introducirTextoEnElVisible2(driver, Constantes.CampoClaveToken, "clavefalsa01");
			
			Utils.crearBloqueo();
			Utils.clickEnElVisible(driver, Constantes.BotonValidarUsuario);
			Utils.esperarProcesandoPeticion(driver);
			Utils.liberarBloqueo();

			// En Integrado y Producci�n, la l�gica que ten�amos hasta ahora
		}
		else if(entorno.equalsIgnoreCase("Local") && Utils.estamosEnLatam() || entorno.equalsIgnoreCase("CDR") && Utils.estamosEnLatam())
		{

			if(Utils.isDisplayed(driver, Constantes.comboTokens, 3))
				Utils.seleccionarComboContieneXPath(driver, Constantes.comboTokens, "");

			if (Utils.isDisplayed(driver, Constantes.CampoClaveOperaciones, 2))
				Utils.introducirTextoEnElVisible2(driver, Constantes.CampoClaveOperaciones, "clavefalsa01");
			
			driver.findElement(By.xpath(Constantes.CampoClaveToken)).sendKeys("clavefalsa01");
			Utils.crearBloqueo();
			Utils.clickEnElVisible(driver, Constantes.BotonValidarUsuario);
			Utils.esperarProcesandoPeticion(driver);
			Utils.liberarBloqueo();
		}

		// INT y PRO
		else
		{

			Usuario UsuarioValidar = Usuario.obtenerUsuarioPorReferenciaCodigo(rutaCSV, params.getValorPropiedad("referenciaPorDefecto"), params.getValorPropiedad("usuarioPorDefecto"));
			if(UsuarioValidar == null)
				throw new Exception("No hay usuario para realizar la validaci�n");

			if (Utils.isDisplayed(driver, Constantes.CampoClaveOperaciones, 2))
				Utils.introducirTextoEnElVisible2(driver, Constantes.CampoClaveOperaciones, UsuarioValidar.getClaveOp());

			// Un INTEGRADO / QA que pide clave Token
			if((entorno.equalsIgnoreCase("Integrado") || entorno.equalsIgnoreCase("QA")) && (Utils.existeElementoTiempo(driver, By.xpath(Constantes.CampoClaveToken), 7)))
			{
				String mensaje = "Introduzca clave de token del usuario por defecto";
				Utils.crearVentanaParaToken(driver, mensaje);
				Utils.crearBloqueo();
				Utils.clickEnElVisible(driver, Constantes.BotonValidarUsuario);
				Utils.esperarProcesandoPeticion(driver);
				Utils.liberarBloqueo();
			}

			else if(Utils.existeElementoTiempo(driver, By.xpath(Constantes.CampoClaveToken), 4))
			{

				// Producci�n
				while(!validacion && intentosValidacion < 3)
				{
					String mensaje = "Introduzca clave de token del usuario por defecto";
					if(intentosValidacion > 0)
					{
						if (Utils.isDisplayed(driver, Constantes.CampoClaveOperaciones, 2))
							Utils.introducirTextoEnElVisible2(driver, Constantes.CampoClaveOperaciones, UsuarioValidar.getClaveOp());
						mensaje = mensaje + ". Tiene " + (3 - intentosValidacion) + " intento(s) m�s";
					}

					// driver.findElement(By.id("claveToken")).sendKeys(Main.computePin(Semilla,null));
					
					Utils.crearVentanaParaToken(driver, mensaje);
					
//					driver.findElement(By.xpath(Constantes.CampoClaveToken)).sendKeys(resul);
					// JOptionPane.showMessageDialog(null, mensaje,"Informaci�n",JOptionPane.INFORMATION_MESSAGE);
					Utils.clickEnElVisiblePrimero(driver, Constantes.BotonValidarUsuario);
					Thread.sleep(2000);
					if(driver.getPageSource().contains("La validaci�n ha sido incorrecta"))
						intentosValidacion++;
					else
						validacion = true;
					if(intentosValidacion == 3)
						Log.write("Se han superado el n�mero de reintentos de validaci�n permitidos");
				}
			}
			else
			{
				Utils.crearBloqueo();
				Utils.clickEnElVisible(driver, Constantes.BotonValidarUsuario);
				Utils.esperarProcesandoPeticion(driver);
				Utils.liberarBloqueo();
			}

		}
	}
	
	public static void crearVentanaParaToken(WebDriver driver, String mensaje) throws Exception {
		
		String sessionId = "";
		
		try {
			sessionId = ((RemoteWebDriver) driver).getSessionId().toString();
		} catch (Exception e) {
			sessionId = Long.toString(Thread.currentThread().getId());
		}
				
		//Algo mas de 8 minutos m�ximo de espera
		Utils.crearBloqueoToken(sessionId,500);
		
		//Si existe desafio lo pongo
		String mensajeDesafio = "";
		
		//CON TOKEN
		if (Utils.isDisplayed(driver, Constantes.desafio, 1)){
			String desafio = Utils.devuelveTextoDelVisiblePrimero(driver, Constantes.desafio);
			mensajeDesafio = "<br>Use el desaf�o: <font color='blue'>"+desafio+"</font>";
		}
		     
        String numerito;
		String botonEnviarMensaje="//a[@id='smsLink']";	
		boolean envioSms=Utils.isDisplayed(driver, botonEnviarMensaje, 3);
		
		//CONSMS
        if(envioSms==true && !Utils.isDisplayed(driver, Constantes.desafio, 1)){       	  
			Log.write("Entramos en sms");
			//String botonEnviarMensaje="//a[@id='smsLink']";	
			Utils.esperaHastaApareceSinSalir(driver, botonEnviarMensaje, 15);
			Utils.clickEnElVisible(driver, botonEnviarMensaje);
			Thread.sleep(8000);
			numerito=Utils.getClaveSms(sessionId);
			Log.write("Clave sms: "+numerito);
	
			Utils.liberarBloqueoToken(sessionId);
        }
		
        else{
        	
			JOptionPane optionPane = new JOptionPane("<html>" + mensaje + mensajeDesafio + "</html>", JOptionPane.INFORMATION_MESSAGE);
			optionPane.setWantsInput(true);
			JDialog dialog = optionPane.createDialog("Informaci�n");
			dialog.setAlwaysOnTop(true);
			dialog.requestFocus();
			dialog.setVisible(true);		
			dialog.dispose();
			numerito = optionPane.getInputValue().toString();
			
	//		String numerito = JOptionPane.showInputDialog(null, mensaje, "Informaci�n", JOptionPane.INFORMATION_MESSAGE);
			Utils.liberarBloqueoToken(sessionId);
        }
		
		
		driver.findElement(By.xpath(Constantes.CampoClaveToken)).sendKeys(numerito);
		
		
		

	}
	
	public static void crearVentanaParaTokenOtroUsuario(WebDriver driver, String mensaje) throws Exception {

		String sessionId = ((RemoteWebDriver) driver).getSessionId().toString();
		
		//9 minutos m�ximo de espera
		Utils.crearBloqueoToken(sessionId, 540);
		
		String numerito = JOptionPane.showInputDialog(null, mensaje, "Informaci�n", JOptionPane.INFORMATION_MESSAGE);
		driver.findElement(By.xpath(Constantes.CampoClaveTokenOtroUsuario)).sendKeys(numerito);
		
		Utils.liberarBloqueoToken(sessionId);
		

	}


	public static void validarOtroUsuario(WebDriver driver) throws Exception
	{
		int intentosValidacion = 0;
		boolean validacion = false;
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosFijos");
		String entorno = params.getValorPropiedad("entorno");

		// Si el entorno es DESARROLLO, no valida los campos de claveOperacion y Token, pero aparecen

		if(entorno.equalsIgnoreCase("Desarrollo"))
		{
			if (Utils.isDisplayed(driver, Constantes.CampoClaveOperacionesOtroUsuario, 5))
				Utils.introducirTextoEnElVisible2(driver, Constantes.CampoClaveOperacionesOtroUsuario, "clavefalsa01");		
			driver.findElement(By.xpath(Constantes.CampoClaveTokenOtroUsuario)).sendKeys("clavefalsa01");
			Utils.crearBloqueo();
			Utils.clickEnElVisible(driver, Constantes.BotonValidarUsuario);
			Utils.esperarProcesandoPeticion(driver);
			Utils.liberarBloqueo();

			// En Integrado y Producci�n, la l�gica que ten�amos hasta ahora
		}
		else if(entorno.equalsIgnoreCase("Local") && Utils.estamosEnLatam() || entorno.equalsIgnoreCase("CDR") && Utils.estamosEnLatam())
		{

			if(Utils.isDisplayed(driver, Constantes.comboTokens, 3))
				Utils.seleccionarComboContieneXPath(driver, Constantes.comboTokens, "");

			if (Utils.isDisplayed(driver, Constantes.CampoClaveOperacionesOtroUsuario, 5))
				Utils.introducirTextoEnElVisible2(driver, Constantes.CampoClaveOperacionesOtroUsuario, "clavefalsa01");

			driver.findElement(By.xpath(Constantes.CampoClaveTokenOtroUsuario)).sendKeys("clavefalsa01");
			Utils.crearBloqueo();
			Utils.clickEnElVisible(driver, Constantes.BotonValidarUsuario);
			Utils.esperarProcesandoPeticion(driver);
			Utils.liberarBloqueo();
		}

		// INT y PRO
		else
		{

			Usuario UsuarioValidar = Usuario.obtenerUsuarioPorReferenciaCodigo(rutaCSV, params.getValorPropiedad("referenciaPorDefecto"), params.getValorPropiedad("usuarioPorDefecto"));
			if(UsuarioValidar == null)
				throw new Exception("No hay usuario para realizar la validaci�n");

			if (Utils.isDisplayed(driver, Constantes.CampoClaveOperacionesOtroUsuario, 3))
				Utils.introducirTextoEnElVisible2(driver, Constantes.CampoClaveOperacionesOtroUsuario, UsuarioValidar.getClaveOp());

			// Un INTEGRADO / QA que pide clave Token
			if((entorno.equalsIgnoreCase("Integrado") || entorno.equalsIgnoreCase("QA")) && (Utils.existeElementoTiempo(driver, By.xpath(Constantes.CampoClaveToken), 7)))
			{
				driver.findElement(By.xpath(Constantes.CampoClaveTokenOtroUsuario)).sendKeys("clavefalsa01");
				Utils.crearBloqueo();
				Utils.clickEnElVisible(driver, Constantes.BotonValidarUsuario);
				Utils.esperarProcesandoPeticion(driver);
				Utils.liberarBloqueo();
			}

			else if(Utils.existeElementoTiempo(driver, By.xpath(Constantes.CampoClaveTokenOtroUsuario), 4))
			{

				// Producci�n
				while(!validacion && intentosValidacion < 3)
				{
					String mensaje = "Introduzca clave de token del usuario por defecto";
					if(intentosValidacion > 0)
					{
						if (Utils.isDisplayed(driver, Constantes.CampoClaveOperacionesOtroUsuario, 3))
							Utils.introducirTextoEnElVisible2(driver, Constantes.CampoClaveOperacionesOtroUsuario, UsuarioValidar.getClaveOp());
						
						mensaje = mensaje + ". Tiene " + (3 - intentosValidacion) + " intento(s) m�s";
					}
					
					crearVentanaParaTokenOtroUsuario(driver, mensaje);

//					String resul = JOptionPane.showInputDialog(null, mensaje, "Informaci�n", JOptionPane.INFORMATION_MESSAGE);
//					driver.findElement(By.xpath(Constantes.CampoClaveTokenOtroUsuario)).sendKeys(resul);
					
					// JOptionPane.showMessageDialog(null, mensaje,"Informaci�n",JOptionPane.INFORMATION_MESSAGE);
					Utils.clickEnElVisiblePrimero(driver, Constantes.BotonValidarUsuario);
					Thread.sleep(2000);
					if(driver.getPageSource().contains("La validaci�n ha sido incorrecta"))
						intentosValidacion++;
					else
						validacion = true;
					if(intentosValidacion == 3)
						Log.write("Se han superado el n�mero de reintentos de validaci�n permitidos");
				}
			}
			else
			{
				Utils.crearBloqueo();
				Utils.clickEnElVisible(driver, Constantes.BotonValidarUsuario);
				Utils.esperarProcesandoPeticion(driver);
				Utils.liberarBloqueo();
			}

		}
	}


	public static void validarUsuario(WebDriver driver, Usuario usuario, Token token) throws Exception
	{
		int intentosValidacion = 0;
		boolean validacion = false;
		params.cargarPropiedades();
		String entorno = params.getValorPropiedad("entorno");

		// Si el entorno es DESARROLLO, no valida los campos de claveOperacion y Token, pero aparecen

		if(entorno.equalsIgnoreCase("Desarrollo"))
		{
			if (Utils.isDisplayed(driver, Constantes.CampoClaveOperaciones, 3))
				Utils.introducirTextoEnElVisible2(driver, Constantes.CampoClaveOperaciones, "clavefalsa01");

			driver.findElement(By.xpath(Constantes.CampoClaveToken)).sendKeys("clavefalsa01");
			Utils.crearBloqueo();
			Utils.clickEnElVisible(driver, Constantes.BotonValidarUsuario);
			Utils.esperarProcesandoPeticion(driver);
			Utils.liberarBloqueo();


		}

		else if(entorno.equalsIgnoreCase("Local") && Utils.estamosEnLatam() || entorno.equalsIgnoreCase("CDR") && Utils.estamosEnLatam())
		{

			Utils.seleccionarComboContieneXPath(driver, Constantes.comboTokens, "");
			if (Utils.isDisplayed(driver, Constantes.CampoClaveOperaciones, 3))
				Utils.introducirTextoEnElVisible2(driver, Constantes.CampoClaveOperaciones, "clavefalsa01");
			driver.findElement(By.xpath(Constantes.CampoClaveToken)).sendKeys("clavefalsa01");
			Utils.crearBloqueo();
			Utils.clickEnElVisible(driver, Constantes.BotonValidarUsuario);
			Utils.esperarProcesandoPeticion(driver);
			Utils.liberarBloqueo();
		}


		// En Integrado y Producci�n, la l�gica que ten�amos hasta ahora
		else
		{

			if (Utils.isDisplayed(driver, Constantes.CampoClaveOperaciones, 3))
				Utils.introducirTextoEnElVisible2(driver, Constantes.CampoClaveOperaciones, usuario.getClaveOp());

			// Un INTEGRADO / QA que pide clave Token
			if((entorno.equalsIgnoreCase("Integrado") || entorno.equalsIgnoreCase("QA")) && (Utils.existeElementoTiempo(driver, By.xpath(Constantes.CampoClaveToken), 7)))
			{
				driver.findElement(By.xpath(Constantes.CampoClaveToken)).sendKeys("clavefalsa01");
				Utils.crearBloqueo();
				Utils.clickEnElVisible(driver, Constantes.BotonValidarUsuario);
				Utils.esperarProcesandoPeticion(driver);
				Utils.liberarBloqueo();
			}


			else if(Utils.existeElementoTiempo(driver, By.xpath(Constantes.CampoClaveToken), 7))
			{

				while(!validacion && intentosValidacion < 3)
				{
					String mensaje = "Introduzca clave de token del usuario " + usuario.getCodUsuario();
					if(token != null)
						mensaje = mensaje + " - n�mero de dispositivo " + token.getNumero();
					if(intentosValidacion > 0)
					{
						if (Utils.isDisplayed(driver, Constantes.CampoClaveOperaciones, 3))
							Utils.introducirTextoEnElVisible2(driver, Constantes.CampoClaveOperaciones, usuario.getClaveOp());
						mensaje = mensaje + ". Tiene " + (3 - intentosValidacion) + " intento(s) m�s";
					}

					crearVentanaParaToken(driver, mensaje);
					// driver.findElement(By.id("claveToken")).sendKeys(Main.computePin(Semilla,null));
//					String resul = JOptionPane.showInputDialog(null, mensaje, "Informaci�n", JOptionPane.INFORMATION_MESSAGE);
//					driver.findElement(By.xpath(Constantes.CampoClaveToken)).sendKeys(resul);
					// JOptionPane.showMessageDialog(null, mensaje,"Informaci�n",JOptionPane.INFORMATION_MESSAGE);
					Utils.clickEnElVisiblePrimero(driver, Constantes.BotonValidarUsuario);
					Thread.sleep(2000);
					if(driver.getPageSource().contains("La validaci�n ha sido incorrecta"))
						intentosValidacion++;
					else
						validacion = true;
					if(intentosValidacion == 3)
						Log.write("Se han superado el n�mero de reintentos de validaci�n permitidos");
				}
			}
			else
			{
				Utils.crearBloqueo();
				Utils.clickEnElVisible(driver, Constantes.BotonValidarUsuario);
				Utils.esperarProcesandoPeticion(driver);
				Utils.liberarBloqueo();
			}
		}
	}


	public static void validarOperacionOk(WebDriver driver)
	{
		// Validaci�n del literal mediante expresi�n regular (empieza con Operaci�n realizada correctamente)
		String mensajeOperacion = driver.findElement(By.xpath(Constantes.DivMensajeOk)).getText();
		Pattern patronMensajeOk = Pattern.compile(Constantes.patronOperacionRealizadaCorrectamente);
		if(!(patronMensajeOk.matcher(mensajeOperacion)).matches())
			Log.write("Error: Mensaje 'Operaci�n realizada correctamente' no aparece, se muestra: " + mensajeOperacion);
	}


	public static void scrollAndClick(WebDriver driver, By by) throws Exception
	{
		WebElement element = driver.findElement(by);
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", element);
		Thread.sleep(500);
		element.click();
	}


	public static void pulsarJS(WebDriver driver, By by)
	{
		WebElement boton = driver.findElement(by);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", boton);
	}


	public static void accederAdmUsuarios(WebDriver driver) throws Exception
	{
		// Acceder a administraci�n de usuarios
		driver.switchTo().defaultContent();
		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonAdministradorVisible);
//		Utils.esperaHastaApareceSinSalir(driver, Constantes.BotonAdministradorVisible, 10);
//		Atomic.click(driver, Constantes.BotonAdministradorVisible);
		Utils.esperaHastaApareceSinSalir(driver, Constantes.LiteralAdministracionUsuarios, 5);
		Utils.clickEnElVisiblePrimero(driver, Constantes.LiteralAdministracionUsuarios);
		Utils.esperarProcesandoPeticionMejorado(driver);

	}


	public static void accederAdmUsuariosV5(WebDriver driver) throws Exception
	{
		// Acceder a administraci�n de usuarios
		esperarVisibleTiempo(driver, Constantes.AdministracionYControl, 10);
		Utils.clickEnElVisible(driver, Constantes.AdministracionYControl);

		driver.switchTo().defaultContent();
		Utils.cambiarFramePrincipalV5(driver);

		esperarVisibleTiempo(driver, Constantes.LiteralAdministracionUsuariosV5, 5);
		Utils.clickEnElVisible(driver, Constantes.LiteralAdministracionUsuariosV5);

	}


	public static void cambiarFramePrincipalV5(WebDriver driver) throws Exception
	{
		driver.switchTo().defaultContent();
		Thread.sleep(500);
		WebElement frame = driver.findElement(By.xpath(Constantes.FramePrincipalV5));
		driver.switchTo().frame(frame);
	}


	public static void accederGestionDispositivos(WebDriver driver) throws Exception
	{
		// Acceder a gesti�n de dispositivos
		driver.switchTo().defaultContent();
		Atomic.click(driver, Constantes.BotonAdministradorVisible);
		try
		{
			Utils.clickEnElVisiblePrimero(driver, Constantes.GestionDeDispositivos);
		}
		catch(ElementNotVisibleException e)
		{
			Atomic.click(driver, Constantes.BotonAdministradorVisible);
			Utils.clickEnElVisiblePrimero(driver, Constantes.GestionDeDispositivos);
		}
		esperarProcesandoPeticionMejorado(driver);
	}


	public static void accederAuditoria(WebDriver driver) throws Exception
	{
		// Acceder a auditor�a
		driver.switchTo().defaultContent();
		Atomic.click(driver, Constantes.BotonAdministradorVisible);
		try
		{
			Utils.clickEnElVisiblePrimero(driver, Constantes.LiteralAuditoria);
			Utils.esperarProcesandoPeticionMejorado(driver);
		}
		catch(ElementNotVisibleException e)
		{
			Atomic.click(driver, Constantes.BotonAdministradorVisible);
			Utils.clickEnElVisiblePrimero(driver, Constantes.LiteralAuditoria);
			Utils.esperarProcesandoPeticionMejorado(driver);
		}

	}


	public static void accederAuditoriaIE(WebDriver driver) throws Exception
	{
		// Acceder a auditor�a
		driver.switchTo().defaultContent();
		esperarVisibleTiempo(driver, Constantes.BotonAdministradorVisible, 10);
		Atomic.click(driver, Constantes.BotonAdministradorVisible);
		esperarVisibleTiempo(driver, Constantes.LiteralAuditoria, 10);
		Utils.clickEnElVisiblePrimero(driver, Constantes.LiteralAuditoria);
		Utils.esperarProcesandoPeticionMejorado(driver);
	}


	public static String obtenerFecha()
	{
		// Devuelve fecha actual en formato ddmmaaaa
		Calendar fecha = new GregorianCalendar();
		String stdia = Integer.toString(fecha.get(Calendar.DAY_OF_MONTH));
		if(stdia.length() == 1)
			stdia = "0" + stdia;
		String stmes = Integer.toString(fecha.get(Calendar.MONTH) + 1);
		if(stmes.length() == 1)
			stmes = "0" + stmes;
		String stanyo = Integer.toString(fecha.get(Calendar.YEAR));
		return stdia + stmes + stanyo;
	}


	public static String obtenerFechaSeparador(String fecha)
	{
		// Devuelve fecha actual en formato dd/mm/aaaa despu�s de recibirla en formato ddmmaaa
		String FechaConSeparador = fecha.substring(0, 2) + "/" + fecha.substring(2, 4) + "/" + fecha.substring(4, 8);
		return FechaConSeparador;
	}


	public static boolean existeElemento(WebDriver driver, By by)
	{
		try
		{
			driver.findElement(by);
			return true;
		}
		catch(NoSuchElementException e)
		{
			return false;
		}
	}
	
	public static boolean existeElemento(WebDriver driver, String xpath, int tiempo)
	{
		
		driver.manage().timeouts().implicitlyWait(tiempo, TimeUnit.SECONDS);
		
		try
		{
			if (Atomic.findElements(driver, xpath).size()>0)
				return true;
			else 
				return false;
		}
		catch(NoSuchElementException e)
		{
			return false;
		}
		
		finally {
			driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
		}
	}


	public static boolean existeElementoTiempo(WebDriver driver, By by, int tiempo)
	{
		driver.manage().timeouts().implicitlyWait(tiempo, TimeUnit.SECONDS);

		try
		{
			driver.findElement(by);
			driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
			return true;
		}
		catch(NoSuchElementException e)
		{
			driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
			return false;
		}


	}


	public static void esperarVisibleTiempo(WebDriver driver, String XPath, int Tiempo) throws Exception
	{
		Utils.esperaHastaAparece(driver, XPath, Tiempo);
//		driver.manage().timeouts().implicitlyWait(Tiempo, TimeUnit.SECONDS);
//		WebDriverWait wait = new WebDriverWait(driver, Tiempo);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(XPath)));
//		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);

	}


	public static void esperarVisibleTiempoSinSalir(WebDriver driver, String XPath, int Tiempo) throws Exception
	{
		try
		{
			Utils.esperaHastaApareceSinSalir(driver, XPath, Tiempo);
//			driver.manage().timeouts().implicitlyWait(Tiempo, TimeUnit.SECONDS);
//			WebDriverWait wait = new WebDriverWait(driver, Tiempo);
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(XPath)));
//			driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
		}
		catch(Exception e)
		{

		}
	}


	public static void esperarElementoClicable(WebDriver driver, String xpath, int Tiempo) throws Exception
	{
		try
		{
			Utils.esperaHastaEnabled(driver, xpath, Tiempo);
//			driver.manage().timeouts().implicitlyWait(Tiempo, TimeUnit.SECONDS);
//			WebDriverWait wait = new WebDriverWait(driver, Tiempo);
//			wait.until(ExpectedConditions.elementToBeClickable(elemento));
//			driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
		}
		catch(Exception e)
		{

		}
	}


	public static void esperarTextoElemento(WebDriver driver, String XPath, String texto, int Tiempo) throws Exception
	{
		try
		{
			Utils.esperaHastaApareceSinSalir(driver, XPath, 10);
			
//			driver.manage().timeouts().implicitlyWait(Tiempo, TimeUnit.SECONDS);
//			WebDriverWait wait = new WebDriverWait(driver, Tiempo);
//			wait.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(XPath), texto));
//			driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
		}
		catch(Exception e)
		{

		}
	}


	// Espera mientras est� visible el elemento de "Procesando su petici�n"
	public static void esperarProcesandoPeticion(WebDriver driver) throws Exception
	{
		Utils.esperarProcesandoPeticionMejorado(driver);
		
//		driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
//		WebDriverWait wait = new WebDriverWait(driver, 1);
//		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(Constantes.ImagenCargando)));
//		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
	}


	// Espera por la ventana de espera que est� en Default Content y luego vuelve al frame principal
	// Para los mensajes de espera que aparecen mientras haces cosas del frame ppal
	public static void esperarProcesandoPeticionDefaultContentVolviendoAPPal(WebDriver driver) throws Exception
	{

		JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
		String currentFrame = (String)jsExecutor.executeScript("return self.name");

		try
		{
			driver.switchTo().defaultContent();
			int contador = 0;
			boolean desaparece = false;
			while(contador < 50)
			{
				if(Utils.isDisplayed(driver, Constantes.VentanaEspera, 1))
				{
					contador++;
					Thread.sleep(1000);
				}
				else
				{
					desaparece = true;
					break;
				}

			}
			if(currentFrame.equalsIgnoreCase("kyop-central-load-area"))
				Utils.cambiarFramePrincipal(driver);
			if(!desaparece)
				throw new Exception("TimeOut de 50 segundos esperando a procesar la petici�n");
		}
		catch(Exception e)
		{}
	}


	public static void esperarProcesandoPeticionMejorado(WebDriver driver) throws Exception
	{
		int contador = 0;
		boolean desaparece = false;
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		String currentFrame = (String) js.executeScript("return self.name");
		
		try {
			
			if (!currentFrame.equalsIgnoreCase(""))
				driver.switchTo().defaultContent();
			
			while(contador < 60)
			{
				if(Utils.isDisplayed(driver, Constantes.imagenCargandoCoronita, 1))
				{
					contador++;
					Thread.sleep(1000);
				}
				else
				{
					desaparece = true;
					break;
				}

			}
		} catch (Exception e) {
			
		}
		
		finally {
			
			if (!currentFrame.equalsIgnoreCase("")){
				WebElement frame = driver.findElement(By.name(currentFrame));
				driver.switchTo().frame(frame);	
			}

		}
		
		if(!desaparece)
			throw new Exception("TimeOut de 60 segundos esperando a procesar la petici�n");
		
	
		
	}


	public static void esperarProcesandoPeticionMejoradoSST(WebDriver driver) throws Exception
	{
		int contador = 0;
		boolean desaparece = false;
		while(contador < 50)
		{
			if(Utils.isDisplayed(driver, Constantes.ImagenCargandoSST, 1) || Utils.isDisplayed(driver, "//*[contains(text(),'Procesando su petici�n')]", 1))
			{
				contador++;
				Thread.sleep(1000);
			}
			else
			{
				desaparece = true;
				break;
			}

		}
		if(!desaparece)
			throw new Exception("TimeOut de 50 segundos esperando a procesar la petici�n");
	}


	public static void esperaHastaDesaparece(WebDriver driver, String xpath) throws Exception
	{
		int contador = 0;
		boolean desaparece = false;
		while(contador < 50)
		{
			if(Utils.isDisplayed(driver, xpath, 1))
			{
				contador++;
				Thread.sleep(1000);
			}
			else
			{
				desaparece = true;
				break;
			}
		}
		if(!desaparece)
			throw new Exception("TimeOut de 50 segundos esperando a " + xpath);
	}


	public static void esperarMasGenerico(WebDriver driver) throws Exception
	{
		
		Utils.esperaHastaDesapareceSinSalir(driver, Constantes.masGeneral);
		
//		driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
//		WebDriverWait wait = new WebDriverWait(driver, 1);
//		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(Constantes.masGeneral)));
//		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
	}


	public static boolean isPresentByXpath(WebDriver driver, String elementXpath)
	{
		boolean exists = false;
		try
		{
			WebElement e = driver.findElement(By.xpath(elementXpath));
			exists = e.isDisplayed();
		}
		catch(Exception e)
		{

		}
		return exists;

	}


	public static void setProperties(String ruta)
	{
		rutaProperties = ruta;
	}


	public static String getProperties()
	{
		return rutaProperties;
	}
	
	
	public static void removeFirstLine (String file, String id) throws Exception {
		
		File inputFile = new File(file);
		File tempFile = new File(file+"bobby");

		BufferedReader reader = new BufferedReader(new FileReader(inputFile));
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

		String lineToRemove = id;
		String currentLine;

		while((currentLine = reader.readLine()) != null) {
		    // trim newline when comparing with lineToRemove
		    String trimmedLine = currentLine.trim();
		    if(trimmedLine.equals(lineToRemove)) continue;
		    writer.write(currentLine + System.getProperty("line.separator"));
		}
		
		writer.close(); 
		reader.close(); 
		
		Path from = tempFile.toPath();
		Path to = Paths.get(file);
		Files.copy(from, to, StandardCopyOption.REPLACE_EXISTING);
		
	}


	public static void removeNthLine(String f, int toRemove) throws Exception
	{
		RandomAccessFile raf = new RandomAccessFile(f, "rw");

		// Leaf n first lines unchanged.
		for(int i = 0; i < toRemove; i++)
			raf.readLine();

		// Shift remaining lines upwards.
		long writePos = raf.getFilePointer();
		raf.readLine();
		long readPos = raf.getFilePointer();

		byte[] buf = new byte[1024];
		int n;
		while(-1 != (n = raf.read(buf)))
		{
			raf.seek(writePos);
			raf.write(buf, 0, n);
			readPos += n;
			writePos += n;
			raf.seek(readPos);
		}

		raf.setLength(writePos);
		raf.close();
		
	}
	
	public static String estamosEnASO () {
		
		String modoASO = "NO";
		
		try
		{
			params.cargarPropiedades();
			modoASO = params.getValorPropiedad("modoASO").toLowerCase();
		}
		catch(Exception e)
		{
			modoASO = "NO";
		}
		
		return modoASO;
	}


	public static void CsvAppend(String archivo, String testId, String testName, String resultado, String comentario, String captura) throws Exception
	{
		boolean omitirActualizacion = Utils.estamosEnASO().equalsIgnoreCase("SI") && testId.startsWith("BATCH_");
		
		Utils.waitRandomTime(1, 7);
		
		if (!omitirActualizacion) {

			String outputFile = archivo;
			String lockFile = archivo + ".lock";
			boolean resul = false;
	
			boolean existeBloqueo = new File(lockFile).exists();
			int contador = 0;
	
			// Esperamos si alg�n caso lo tiene bloqueado
			while(existeBloqueo && contador <= 60)
			{
				Thread.sleep(1000);
				contador++;
				existeBloqueo = new File(lockFile).exists();
			}
	
			// Creamos el bloqueo
	
			File archivoBloqueo = new File(lockFile);
			archivoBloqueo.createNewFile();
	
			// vemos si el fichero ya existe
			boolean alreadyExists = new File(outputFile).exists();
	
			try
			{
				// usamos FileWriter indicando que es para append
				CsvWriter writer = new CsvWriter(new FileWriter(outputFile, true), ',');
				CsvReader reader = new CsvReader(new FileReader(outputFile));
	
				// Si el fichero no existe, lo creamos con las cabeceras oportunas
				if(!alreadyExists)
				{
					writer.write("Test_ID");
					writer.write("Test_Name");
					writer.write("Resultado");
					writer.write("Comentario");
					writer.write("Captura");
					writer.endRecord();
				}
	
				int i = 0;
				while(reader.readRecord())
				{
					i++;
					String id = reader.get(0);
					if(id.equalsIgnoreCase(testId))
					{
						resul = true;
						break;
					}
	
				}
	
				// Si existe ya la entrada la borra
				if(resul)
					Utils.removeNthLine(outputFile, i - 1);
	
				writer.write(testId);
				writer.write(testName);
				writer.write(resultado);
				writer.write(comentario);
				writer.write(captura);
				writer.endRecord();
				writer.close();
	
			}
			catch(Exception e)
			{
				utils.Log.writeException(e);
			}
	
			archivoBloqueo.delete();
		}

	}


	public static WebDriver getWebDriverBrowser(String m_nombre, String... navegador)
	{
		WebDriver driver = null;

		try
		{
			
			TestProperties params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();
			String ipHub = params.getValorPropiedad("ipHub");
			
			try {
											
				driver = getRemoteDriver(ipHub, m_nombre, navegador);
	
			}
			
			
			catch (Exception exceptionGrid) {
				
				if(ipHub!=null && !ipHub.equals(""))	
					Log.writeException(exceptionGrid);
			

				// Leemos del properties el navegador que se nos ha indicado
				String pNavegador = params.getValorPropiedad("browser");
				
				if (navegador.length!=0)
					pNavegador = navegador[0];
				
				
				// Obtenemos el proxy si lo hubiera
				String proxy = params.getValorPropiedad("proxy");
	
				if(proxy == null)
					proxy = "";

				// Trusteer de momento s�lo en DEV
				if(Utils.estamosEnCompass())
				{
					proxy = "SI";
				}
	
				
				//FIREFOX
				if(pNavegador.indexOf("firefox") > -1)
				{
					
					// Obtenemos la ruta al binary de firefox (si la hubiera)
					String rutaFirefox = params.getValorPropiedad("rutaFirefox");
					String versionFirefox = params.getValorPropiedad("versionFirefox");
	
					if(rutaFirefox == null)
						rutaFirefox = "";
	
					String firefoxProfile = "";
	
					try
					{
						firefoxProfile = params.getValorPropiedad("firefoxProfilePath");
					}
					catch(Exception e)
					{
						firefoxProfile = "";
					}
	
					if(firefoxProfile == null)
						firefoxProfile = "";
					
					
					//SELENIUM 3
					System.setProperty("webdriver.gecko.driver", "lib/geckodriver.bat");
					
					File exeGecko = new File("lib/geckodriver.exe");
					File batGecko = new File("lib/geckodriver.bat");
					
					if (!exeGecko.exists())
						fail ("ERROR: No existe el .exe de GECKO");
					
					if (!batGecko.exists())
						fail ("ERROR: No existe el .bat de GECKO");
					
	
					// Obtenemos el driver espec�fico del navegador
					FirefoxOptions options = new FirefoxOptions();
							
					//Location where Firefox is installed
					if (!rutaFirefox.equals(""))
						options.setBinary(rutaFirefox);
					else
						fail ("ERROR: Debe especificar la ruta donde se encuentra Firefox instalado.");
	 
					//DesiredCapabilities capabilities = DesiredCapabilities.firefox();
					options.setCapability("moz:firefoxOptions", options);	
					
					if (!versionFirefox.equals(""))
						options.setCapability("version", versionFirefox);
					else
						fail ("ERROR: Debe especificar su versi�n de Firefox.");
					
					options.setCapability("browserName", "firefox");	
					options.setCapability("marionette", true);
					
					FirefoxProfile profile = new FirefoxProfile();
					
	
					//Tema proxy
					if(proxy.equalsIgnoreCase("SI"))
					{
						profile.setPreference("network.proxy.type", 1);
						profile.setPreference("network.proxy.http", "w9622746");
						profile.setPreference("network.proxy.http_port", 6588);
						profile.setPreference("network.proxy.ssl", "w9622746");
						profile.setPreference("network.proxy.ssl_port", 6588);
					}
					else if(proxy.equalsIgnoreCase("PAC"))
					{
						profile.setPreference("network.proxy.type", 2);
						try
						{
							profile.setPreference("network.proxy.autoconfig_url", "file:///" + new File("resources/proxy.pac").getCanonicalPath());
						}
						catch(IOException e)
						{
							utils.Log.writeException(e);
						}
		
					}
		
					else if(proxy.equalsIgnoreCase("DIRECT"))
					{
						profile.setPreference("network.proxy.type", 0);
					}
	
					profile.setAcceptUntrustedCertificates(true);
					profile.setPreference("browser.download.folderList", 2);
					String downloads = new File("downloads").getAbsolutePath();
					profile.setPreference("browser.download.dir", downloads);
					profile.setPreference(
							"browser.helperApps.neverAsk.saveToDisk",
							"image/jpg,text/csv,text/xml,application/xml,application/vnd.ms-excel,application/x-excel,application/x-msexcel,application/excel,application/pdf,application/octet-stream,application/rar,text/plain,application/text,application/zip");
	
					profile.setPreference("browser.helperApps.alwaysAsk.force", false);
					profile.setPreference("pdfjs.disabled", true);
					profile.setPreference("browser.startup.homepage", "");
					
					options.setCapability(FirefoxDriver.PROFILE, profile);
					
					//Navegador invisible
					//options.addArguments("--headless");
					
					
					driver = new FirefoxDriver(options);
					driver.manage().window().maximize();
					
				}
				
				//GOOGLE CHROME
				else if(pNavegador.equals("googlechrome"))
				{
	
					System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
					
					File exeChrome = new File("lib/chromedriver.exe");
					
					if (!exeChrome.exists())
						fail ("ERROR: No existe el .exe de CHROMEDRIVER");
	
	
					String downloads = new File("downloads").getAbsolutePath();
					
					HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
					chromePrefs.put("profile.default_content_settings.popups", 0);
					chromePrefs.put("download.default_directory", downloads);
					
					chromePrefs.put("download.prompt_for_download", false);
					chromePrefs.put("download.directory_upgrade", true);
					
					String descargarPDFExterno = params.getValorPropiedad("descargarPDFExterno");
	
					if(descargarPDFExterno != null){
						if (descargarPDFExterno.equalsIgnoreCase("SI"))	{
							chromePrefs.put("plugins.always_open_pdf_externally", true);
						}
					}
					
	//				Proxy En chrome hay que darle una vueltecita
	//				String PROXY = "localhost:8080";
	//
	//				org.openqa.selenium.Proxy proxy = new org.openqa.selenium.Proxy();
	//				proxy.setHttpProxy(PROXY)
	//				     .setFtpProxy(PROXY)
	//				     .setSslProxy(PROXY);
	//				DesiredCapabilities cap = new DesiredCapabilities();
	//				cap.setCapability(CapabilityType.PROXY, proxy);
	//
	//				WebDriver driver = new InternetExplorerDriver(cap);
					
					ChromeOptions options = new ChromeOptions();
					options.setExperimentalOption("prefs", chromePrefs);
					options.addArguments("enable-automation");
					options.addArguments("start-maximized");
	//				options.addArguments("--js-flags=--expose-gc");  
	//				options.addArguments("--enable-precise-memory-info"); 
					options.addArguments("disable-popup-blocking");
					options.addArguments("disable-default-apps"); 
	//				options.addArguments("disable-infobars");
					//options.addArguments("--no-sandbox");
	//				options.addArguments("--safebrowsing-disable-download-protection");
					options.addArguments("ignore-certificate-errors");
					options.setExperimentalOption("useAutomationExtension", false);
					
		            LoggingPreferences logPrefs = new LoggingPreferences();
		            logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
					
					options.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
					options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					
					driver = new ChromeDriver(options);
										
	//			ChromeOptions cOptions = new ChromeOptions();
	//		    cOptions.addArguments("test-type");
	//		    cOptions.addArguments("start-maximized");
	//		    cOptions.addArguments("--js-flags=--expose-gc");  
	//		    cOptions.addArguments("--enable-precise-memory-info"); 
	//		    cOptions.addArguments("--disable-popup-blocking");
	//		    cOptions.addArguments("--disable-default-apps"); 
	//		    cOptions.addArguments("disable-infobars");
	//		    driver = new ChromeDriver(cOptions);
	
				}
				else if(pNavegador.equals("*iehta") || pNavegador.indexOf("iexplore") > -1)
				{
					
	//				DesiredCapabilities capabilitiesIE = DesiredCapabilities.internetExplorer();
	//				capabilitiesIE.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
	//				capabilitiesIE.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "");
	//				capabilitiesIE.internetExplorer().setCapability("ignoreProtectedModeSettings", true);
	//				capabilitiesIE.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
	//
	//				System.setProperty("webdriver.ie.driver", "lib/IEDriverServer.exe");
	//				driver = new InternetExplorerDriver();
					
					System.setProperty("webdriver.ie.driver","lib\\IEDriverServer.exe");
					 
					// Initialise browser
					InternetExplorerOptions options = new InternetExplorerOptions();
					
					//DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
					options.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
	
					// Set ACCEPT_SSL_CERTS  variable to true
					options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
	
					// Set capability of IE driver to Ignore all zones browser protected mode settings.
					options.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
	
					// Initialize InternetExplorerDriver Instance using new capability.
					driver = new InternetExplorerDriver(options);
					driver.manage().window().maximize();
	
				}
			
			}
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			Utils.quit(driver);
		}

		return driver;
	}


	public static WebDriver getWebDriverBrowserWithProfile(String perfil)
	{
		WebDriver driver = null;
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		// Leemos del properties el navegador que se nos ha indicado
		String pNavegador = params.getValorPropiedad("browser");
		String pais = params.getValorPropiedad("pais");
		String entorno = params.getValorPropiedad("entorno");

		// Obtenemos la ruta al binary de firefox (si la hubiera)
		String rutaFirefox = params.getValorPropiedad("rutaFirefox");

		if(rutaFirefox == null)
			rutaFirefox = "";

		// Obtenemos el proxy si lo hubiera
		String proxy = params.getValorPropiedad("proxy");

		if(proxy == null)
			proxy = "";


		// TOREMOVE
		if(Utils.estamosEnLatam() && pais.equalsIgnoreCase("Compass") && !entorno.equalsIgnoreCase("Local"))
		{
			proxy = "SI";
		}


		// Trusteer de momento s�lo en DEV
		if(Utils.estamosEnCompass() && entorno.equalsIgnoreCase("Desarrollo"))
		{
			proxy = "PAC";
		}

		if(pNavegador.indexOf("firefox") > -1)
		{

			String versionFirefox = params.getValorPropiedad("versionFirefox");

			if(rutaFirefox == null)
				rutaFirefox = "";

			String firefoxProfile = perfil;
			FirefoxProfile profile = new FirefoxProfile();
			profile = new FirefoxProfile(new File(firefoxProfile));
			
			
			//SELENIUM 3
			System.setProperty("webdriver.gecko.driver", "lib/geckodriver.bat");
			
			File exeGecko = new File("lib/geckodriver.exe");
			File batGecko = new File("lib/geckodriver.bat");
			
			if (!exeGecko.exists())
				fail ("ERROR: No existe el .exe de GECKO");
			
			if (!batGecko.exists())
				fail ("ERROR: No existe el .bat de GECKO");
			

			// Obtenemos el driver espec�fico del navegador
			FirefoxOptions options = new FirefoxOptions();
					
			//Location where Firefox is installed
			if (!rutaFirefox.equals(""))
				options.setBinary(rutaFirefox);
			else
				fail ("ERROR: Debe especificar la ruta donde se encuentra Firefox instalado.");

//			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			options.setCapability("moz:firefoxOptions", options);	
			
			if (!versionFirefox.equals(""))
				options.setCapability("version", versionFirefox);
			else
				fail ("ERROR: Debe especificar su versi�n de Firefox.");
			
			options.setCapability("browserName", "firefox");	
			options.setCapability("marionette", true);
			
			options.setCapability(FirefoxDriver.PROFILE, profile);

			//Tema proxy
			if(proxy.equalsIgnoreCase("SI"))
			{
				profile.setPreference("network.proxy.type", 1);
				profile.setPreference("network.proxy.http", "w9622746");
				profile.setPreference("network.proxy.http_port", 6588);
				profile.setPreference("network.proxy.ssl", "w9622746");
				profile.setPreference("network.proxy.ssl_port", 6588);
			}
			else if(proxy.equalsIgnoreCase("PAC"))
			{
				profile.setPreference("network.proxy.type", 2);
				try
				{
					profile.setPreference("network.proxy.autoconfig_url", "file:///" + new File("resources/proxy.pac").getCanonicalPath());
				}
				catch(IOException e)
				{
					utils.Log.writeException(e);
				}

			}

			else if(proxy.equalsIgnoreCase("DIRECT"))
			{
				profile.setPreference("network.proxy.type", 0);
			}
			
			
			

			profile.setAcceptUntrustedCertificates(true);
			profile.setPreference("browser.download.folderList", 2);
			String downloads = new File("downloads").getAbsolutePath();
			profile.setPreference("browser.download.dir", downloads);
			profile.setPreference(
					"browser.helperApps.neverAsk.saveToDisk",
					"image/jpg,text/csv,text/xml,application/xml,application/vnd.ms-excel,application/x-excel,application/x-msexcel,application/excel,application/pdf,application/octet-stream,application/rar,text/plain,application/text,application/zip");

			profile.setPreference("browser.helperApps.alwaysAsk.force", false);
			profile.setPreference("pdfjs.disabled", true);
			profile.setPreference("browser.startup.homepage", "");
			

			driver = new FirefoxDriver(options);
			

//			FirefoxProfile profile = new FirefoxProfile();
//			FirefoxBinary firefoxbin = null;
//
//			try
//			{
//				if(!rutaFirefox.equalsIgnoreCase(""))
//				{
//					File pathToFirefoxBinary = new File(rutaFirefox);
//					firefoxbin = new FirefoxBinary(pathToFirefoxBinary);
//				}
//			}
//			catch(Exception e1)
//			{
//				Log.write("La ruta al .exe del firefox indicada en el archivo properties no es correcta. Revise el archivo properties");
//			}
//
//			// Si establecemos un perfil, lo cogemos del properties
//			if(!firefoxProfile.equalsIgnoreCase(""))
//			{
//				profile = new FirefoxProfile(new File(firefoxProfile));
//			}
//
//			// Le establecemos una serie de preferencias que nos interesan, como que los archivos indicados no nos pregunte al descargar
//			profile.setAcceptUntrustedCertificates(true);
//			profile.setPreference("browser.download.folderList", 2);
//			String downloads = new File("downloads").getAbsolutePath();
//			profile.setPreference("browser.download.dir", downloads);
//			profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
//					"image/jpg, text/csv,text/xml,application/xml,application/vnd.ms-excel,application/x-excel,application/x-msexcel,application/excel,application/pdf,application/octet-stream");
//
//			profile.setPreference("browser.helperApps.alwaysAsk.force", false);
//			profile.setPreference("pdfjs.disabled", true);
//			profile.setPreference("browser.startup.homepage", "");
//
//			if(proxy.equalsIgnoreCase("SI"))
//			{
//				profile.setPreference("network.proxy.type", 1);
//				profile.setPreference("network.proxy.http", "w9622746");
//				profile.setPreference("network.proxy.http_port", 6588);
//				profile.setPreference("network.proxy.ssl", "w9622746");
//				profile.setPreference("network.proxy.ssl_port", 6588);
//			}
//			else if(proxy.equalsIgnoreCase("PAC"))
//			{
//				profile.setPreference("network.proxy.type", 2);
//				try
//				{
//					profile.setPreference("network.proxy.autoconfig_url", "file:///" + new File("resources/proxy.pac").getCanonicalPath());
//				}
//				catch(IOException e)
//				{
//					utils.Log.writeException(e);
//				}
//
//			}
//
//			else if(proxy.equalsIgnoreCase("DIRECT"))
//			{
//				profile.setPreference("network.proxy.type", 0);
//			}
//
//			profile.setEnableNativeEvents(false);
//
//			if(!rutaFirefox.equalsIgnoreCase(""))
//			{
//				driver = new FirefoxDriver(firefoxbin, profile);
//			}
//			else
//			{
//				driver = new FirefoxDriver(profile);
//			}

		}
		else if(pNavegador.equals("googlechrome"))
		{
			System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
			
			File exeChrome = new File("lib/chromedriver.exe");
			
			if (!exeChrome.exists())
				fail ("ERROR: No existe el .exe de CHROMEDRIVER");


			String downloads = new File("downloads").getAbsolutePath();
			
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("download.default_directory", downloads);
			
//			Proxy En chrome hay que darle una vueltecita
//			System.setProperty("http.proxyHost", "w9622746");
//			System.setProperty("http.proxyPort", "6588");
//			System.setProperty("https.proxyHost", "w9622746");
//			System.setProperty("https.proxyPort", "6588");     
			
			
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", chromePrefs);
			options.addArguments("test-type");
			options.addArguments("start-maximized");
			options.addArguments("--js-flags=--expose-gc");  
			options.addArguments("--enable-precise-memory-info"); 
			options.addArguments("--disable-popup-blocking");
			options.addArguments("--disable-default-apps"); 
			options.addArguments("disable-infobars");
			options.addArguments("--safebrowsing-disable-download-protection");
			options.addArguments(System.getProperty("user.home") + "/AppData/Local/Google/Chrome/User Data");

			DesiredCapabilities cap = DesiredCapabilities.chrome();
			cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			cap.setCapability(ChromeOptions.CAPABILITY, options);
			driver = new ChromeDriver(cap);

		}
		else if(pNavegador.equals("*iehta") || pNavegador.indexOf("iexplore") > -1)
		{
			System.setProperty("webdriver.ie.driver", "lib/IEDriverServer.exe");
			driver = new InternetExplorerDriver();

		}

		return driver;
	}


	public static WebDriver getWebDriverBrowserWithProfileProxy(String perfil, String tipoProxy)
	{
		WebDriver driver = null;
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		// Leemos del properties el navegador que se nos ha indicado
		String pNavegador = params.getValorPropiedad("browser");

		// Obtenemos la ruta al binary de firefox (si la hubiera)
		String rutaFirefox = params.getValorPropiedad("rutaFirefox");

		if(rutaFirefox == null)
			rutaFirefox = "";

		// Obtenemos el proxy si lo hubiera
		String proxy = tipoProxy;

		String firefoxProfile = perfil;

		if(pNavegador.indexOf("firefox") > -1)
		{

			FirefoxProfile profile = new FirefoxProfile();
			FirefoxBinary firefoxbin = null;

			try
			{
				if(!rutaFirefox.equalsIgnoreCase(""))
				{
					File pathToFirefoxBinary = new File(rutaFirefox);
					firefoxbin = new FirefoxBinary(pathToFirefoxBinary);
				}
			}
			catch(Exception e1)
			{
				Log.write("La ruta al .exe del firefox indicada en el archivo properties no es correcta. Revise el archivo properties");
			}

			// Si establecemos un perfil, lo cogemos del properties
			if(!firefoxProfile.equalsIgnoreCase(""))
			{
				profile = new FirefoxProfile(new File(firefoxProfile));
			}

			// Le establecemos una serie de preferencias que nos interesan, como que los archivos indicados no nos pregunte al descargar
			profile.setAcceptUntrustedCertificates(true);
			profile.setPreference("browser.download.folderList", 2);
			String downloads = new File("downloads").getAbsolutePath();
			profile.setPreference("browser.download.dir", downloads);
			profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
					"image/jpg, text/csv,text/xml,application/xml,application/vnd.ms-excel,application/x-excel,application/x-msexcel,application/excel,application/pdf,application/octet-stream");

			profile.setPreference("browser.helperApps.alwaysAsk.force", false);
			profile.setPreference("pdfjs.disabled", true);
			profile.setPreference("browser.startup.homepage", "");

			if(proxy.equalsIgnoreCase("SI"))
			{
				profile.setPreference("network.proxy.type", 1);
				profile.setPreference("network.proxy.http", "w9622746");
				profile.setPreference("network.proxy.http_port", 6588);
				profile.setPreference("network.proxy.ssl", "w9622746");
				profile.setPreference("network.proxy.ssl_port", 6588);
			}
			else if(proxy.equalsIgnoreCase("PAC"))
			{
				profile.setPreference("network.proxy.type", 2);
				try
				{
					profile.setPreference("network.proxy.autoconfig_url", "file:///" + new File("resources/proxy.pac").getCanonicalPath());
				}
				catch(IOException e)
				{
					utils.Log.writeException(e);
				}

			}

			else if(proxy.equalsIgnoreCase("DIRECT"))
			{
				profile.setPreference("network.proxy.type", 0);
			}

			FirefoxOptions options = new FirefoxOptions();
			options.setProfile(profile);
//			profile.setEnableNativeEvents(false);

			if(!rutaFirefox.equalsIgnoreCase(""))
			{
				driver = new FirefoxDriver(options);
			}
			else
			{
				driver = new FirefoxDriver(options);
			}

		}
		else if(pNavegador.equals("googlechrome"))
		{
			System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
			driver = new ChromeDriver();

		}
		else if(pNavegador.equals("*iehta") || pNavegador.indexOf("iexplore") > -1)
		{
			System.setProperty("webdriver.ie.driver", "lib/IEDriverServer.exe");
			driver = new InternetExplorerDriver();

		}

		return driver;
	}
	
	
	public static WebDriver getWebDriverBrowserWithProxy(String tipoProxy)
	{
		WebDriver driver = null;
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		// Leemos del properties el navegador que se nos ha indicado
		String pNavegador = params.getValorPropiedad("browser");

		// Obtenemos la ruta al binary de firefox (si la hubiera)
		String rutaFirefox = params.getValorPropiedad("rutaFirefox");
		String versionFirefox = params.getValorPropiedad("versionFirefox");

		if(rutaFirefox == null)
			rutaFirefox = "";

		// Obtenemos el proxy si lo hubiera
		String proxy = tipoProxy;


		if(pNavegador.indexOf("firefox") > -1)
		{
			
			String firefoxProfile = "";

			try
			{
				firefoxProfile = params.getValorPropiedad("firefoxProfilePath");
			}
			catch(Exception e)
			{
				firefoxProfile = "";
			}

			if(firefoxProfile == null)
				firefoxProfile = "";
			
			
			//SELENIUM 3
			System.setProperty("webdriver.gecko.driver", "lib/geckodriver.bat");
			
			File exeGecko = new File("lib/geckodriver.exe");
			File batGecko = new File("lib/geckodriver.bat");
			
			if (!exeGecko.exists())
				fail ("ERROR: No existe el .exe de GECKO");
			
			if (!batGecko.exists())
				fail ("ERROR: No existe el .bat de GECKO");
			

			// Obtenemos el driver espec�fico del navegador
			FirefoxOptions options = new FirefoxOptions();
					
			//Location where Firefox is installed
			if (!rutaFirefox.equals(""))
				options.setBinary(rutaFirefox);
			else
				fail ("ERROR: Debe especificar la ruta donde se encuentra Firefox instalado.");

			//DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			options.setCapability("moz:firefoxOptions", options);	
			
			if (!versionFirefox.equals(""))
				options.setCapability("version", versionFirefox);
			else
				fail ("ERROR: Debe especificar su versi�n de Firefox.");
			
			options.setCapability("browserName", "firefox");	
			options.setCapability("marionette", true);
			
			FirefoxProfile profile = new FirefoxProfile();
			options.setCapability(FirefoxDriver.PROFILE, profile);

			//Tema proxy
			if(proxy.equalsIgnoreCase("SI"))
			{
				profile.setPreference("network.proxy.type", 1);
				profile.setPreference("network.proxy.http", "w9622746");
				profile.setPreference("network.proxy.http_port", 6588);
				profile.setPreference("network.proxy.ssl", "w9622746");
				profile.setPreference("network.proxy.ssl_port", 6588);
			}
			else if(proxy.equalsIgnoreCase("PAC"))
			{
				profile.setPreference("network.proxy.type", 2);
				try
				{
					profile.setPreference("network.proxy.autoconfig_url", "file:///" + new File("resources/proxy.pac").getCanonicalPath());
				}
				catch(IOException e)
				{
					utils.Log.writeException(e);
				}

			}

			else if(proxy.equalsIgnoreCase("DIRECT"))
			{
				profile.setPreference("network.proxy.type", 0);
			}
			
			
			

			profile.setAcceptUntrustedCertificates(true);
			profile.setPreference("browser.download.folderList", 2);
			String downloads = new File("downloads").getAbsolutePath();
			profile.setPreference("browser.download.dir", downloads);
			profile.setPreference(
					"browser.helperApps.neverAsk.saveToDisk",
					"image/jpg,text/csv,text/xml,application/xml,application/vnd.ms-excel,application/x-excel,application/x-msexcel,application/excel,application/pdf,application/octet-stream,application/rar,text/plain,application/text,application/zip");

			profile.setPreference("browser.helperApps.alwaysAsk.force", false);
			profile.setPreference("pdfjs.disabled", true);
			profile.setPreference("browser.startup.homepage", "");
			

			driver = new FirefoxDriver(options);
			
			
		}

			

		return driver;
	}


//	public static void guardarResultado(WebDriver driver, String m_codigo, String m_nombre,String resultado,String comentario) throws Exception {
//		
//		TestProperties params = new TestProperties(Utils.getProperties());
//		params.cargarPropiedades();
//		
//		if (comentario.indexOf(",")>-1) {
//			comentario="Se produjo un error inesperado. Revisar la traza";
//		}
//		
//		//Hacemos captura de pantalla
//		String captura = Utils.takeScreenShot(driver,m_nombre);
//
//		//Mandamos a la salida est�ndar informaci�n del mensaje
//		Log.write(comentario);
//		Log.write("Puede ver la captura en: "+captura);
//		
//		//A�adimos al csv la l�nea con la informaci�n
//		Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, resultado, comentario, captura);
//	}

	public static void desconectar(WebDriver driver)
	{
		try
		{
			if(Utils.isDisplayed(driver, Constantes.DesactivarAyuda, 2))
			{
				Utils.clickEnElVisible(driver, Constantes.DesactivarAyuda);
			}
			driver.switchTo().defaultContent();
			if(Utils.isDisplayed(driver, Constantes.BotonDesconectar, 4))
				Utils.clickEnElVisibleSinEspera(driver, Constantes.BotonDesconectar);
			else if(Utils.isDisplayed(driver, Constantes.FrameMenu, 1))
			{
				Utils.cambiarFrameMenuV5(driver);
				Utils.clickEnElVisibleSinEspera(driver, Constantes.BotonDesconectarV5);
			}

			if(Utils.isAlertPresent(driver))
				driver.switchTo().alert().accept();

			Thread.sleep(3000);
		}
		catch(Exception e)
		{

		}
	}


	public static void quit(WebDriver driver)
	{

		
		try
		{
			driver.close();
		}
		catch(Exception e1)
		{
			
		}

		try
		{
			Thread.sleep(2000);
			driver.quit();
		}
		catch(Exception e)
		{
			
		}

	}
	
	public static void quitDriver(WebDriver driver)
	{

		Utils.quit(driver);

	}


	public static void guardarResultado(WebDriver driver, Resultados resultado, String m_codigo, String m_nombre) throws Exception
	{
		try
		{
	
			TestProperties params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();
			
			String bufferErrores = resultado.getBufferErrores().toString();
			String bufferWarnings = resultado.getBufferWarnings().toString();
			
			if(!"".equals(bufferErrores))
				adivinarNetworkError(driver);

			if(Utils.isAlertPresent(driver))
				driver.switchTo().alert().accept();

			// Hacemos captura de pantalla
			String captura = Utils.takeScreenShot(driver, m_nombre, "simple");
			
			Utils.desconectar(driver);

			PrintStream ficheroMain = new PrintStream(new BufferedOutputStream(new FileOutputStream("results/Main.txt", true)), true);
			ficheroMain.append("[INFO] [FIN]" + Utils.GetTodayDateAndTime() + " Termina caso: " + m_nombre + ", codigo: " + m_codigo + "\n");
			ficheroMain.close();

			// L�gica de los resultados
			String comentarioWarning = resultado.getComentario() + ". Se han producido warnings. Revisar la traza";
			String comentarioOK = resultado.getComentario();
			String comentarioERRORControlado = resultado.getComentario();
			String comentarioERROR = "Se ha producido un error. Revisar la traza";
			String comentarioWARNINGgen = "Hay avisos en el log. Revisar la traza";
			String comentarioNOEJECUTADO = "No se ejecut� el caso. Revisar la traza";

			// Si resultado es Ok y alg�n buffer NO VAC�O
			if((resultado.getResultado() == "OK") && (!"".equals(bufferErrores) || (!"".equals(bufferWarnings))))
			{
							
				if(!"".equals(bufferErrores)){
					Log.write("<html><b style=\"color: #ff0000;\">ERRORES: " + bufferErrores + "</b></html>");
					Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "ERROR", comentarioERROR, captura);
					fail(bufferErrores);
				}
				
				if(!"".equals(bufferWarnings)) {
					Log.write("<html><b style=\"color: ##ffce00;\">WARNINGS: " + bufferWarnings + "</b></html>");
					Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "WARNING", comentarioWarning, captura);
				}
				
				Log.write("Puede ver la �ltima captura de la prueba en: " + captura);
			}

			// Si resultado es Ok y buffers vac�os
			else if((resultado.getResultado() == "OK") && ("".equals(bufferErrores) || ("".equals(bufferWarnings))))
			{
				Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "OK", comentarioOK, captura);
				Log.write(comentarioOK);
			}

			// Si resultado ERROR
			else if(resultado.getResultado() == "ERROR")
			{
				Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "ERROR", comentarioERRORControlado, captura);
				Log.write(comentarioERRORControlado);
				Log.write("Puede ver la �ltima captura de la prueba en: " + captura);
				fail(bufferErrores);
			}
			
			// Si resultado WARNING
			else if(resultado.getResultado() == "WARNING")
			{
				Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "WARNING", comentarioERRORControlado, captura);
				Log.write(comentarioERRORControlado);
				Log.write("Puede ver la �ltima captura de la prueba en: " + captura);
			}

			// Si resultado vac�o
			else if(resultado.getResultado() == "")
			{
				// Si hay errores
				if(!"".equals(bufferErrores))
				{
					Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "ERROR", Utils.adivinarError(resultado, comentarioERROR), captura);
					Log.write("<html><b style=\"color: #ff0000;\">ERRORES: " + bufferErrores+ "</b></html>");
					if(!"".equals(bufferWarnings))
						Log.write("<html><b style=\"color: ##ffce00;\">WARNINGS: " + bufferWarnings+ "</b></html>");
					Log.write("Puede ver la �ltima captura de la prueba en: " + captura);
					fail(bufferErrores);
				}
				// Si hay Warnings
				else if(!"".equals(bufferWarnings))
				{
					Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "WARNING", comentarioWARNINGgen, captura);
					Log.write("WARNINGS: " + bufferWarnings);
					Log.write("Puede ver la �ltima captura de la prueba en: " + captura);
					fail(bufferErrores);
				}
				// Algo raro ha pasado
				else
				{
					Log.write("Puede ver la �ltima captura de la prueba en: " + captura);
					Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "NO EJECUTADO", comentarioNOEJECUTADO, captura);
				}
				
			}
			
			
		}
		catch(Exception e)
		{
			Log.writeException(e);
		}
		
		finally {
			
			Utils.quit(driver);
			FileOutputStream fichero = new FileOutputStream("results/" + m_nombre + ".html", true);
			close(fichero);
			loggers.get(Long.toString(Thread.currentThread().getId())).cerrarLog();
			loggers.remove(Long.toString(Thread.currentThread().getId()));
		}
		

		
	}
	
	 public static void close(Closeable c) {
	     if (c == null) return; 
	     try {
	         c.close();
	     } catch (IOException e) {
	         //log the exception
	     }
	  }


	public static void guardarResultadoInstalacion(WebDriver driver, Resultados resultado, String m_codigo, String m_nombre) throws Exception
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		// Hacemos captura de pantalla
		String captura;
		try
		{
			captura = Utils.takeScreenShot(driver, m_nombre, "simple");
			Utils.desconectar(driver);
		}
		catch(Exception e)
		{
			captura = "";
		}

		Utils.quit(driver);

		// L�gica de los resultados
		String comentarioWarning = resultado.getComentario() + ". Se han producido warnings. Revisar la traza";
		String comentarioOK = resultado.getComentario();
		String comentarioERRORControlado = resultado.getComentario();
		String comentarioERROR = "Se ha producido un error. Revisar la traza";
		String comentarioWARNINGgen = "Hay avisos en el log. Revisar la traza";
		String comentarioNOEJECUTADO = "No se ejecut� el caso. Revisar la traza";

		String bufferErrores = resultado.getBufferErrores().toString();
		String bufferWarnings = resultado.getBufferWarnings().toString();

		// Si resultado es Ok y alg�n buffer NO VAC�O
		if((resultado.getResultado() == "OK") && (!"".equals(bufferErrores) || (!"".equals(bufferWarnings))))
		{
			Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "WARNING", comentarioWarning, captura);
			if(!"".equals(bufferErrores))
				Log.write("ERRORES: " + bufferErrores);
			if(!"".equals(bufferWarnings))
				Log.write("WARNINGS: " + bufferWarnings);
			Log.write("Puede ver la �ltima captura de la prueba en: " + captura);
		}

		// Si resultado es Ok y buffers vac�os
		else if((resultado.getResultado() == "OK") && ("".equals(bufferErrores) || ("".equals(bufferWarnings))))
		{
			Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "OK", comentarioOK, captura);
			Log.write(comentarioOK);
		}

		// Si resultado ERROR
		else if(resultado.getResultado() == "ERROR")
		{
			Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "ERROR", comentarioERRORControlado, captura);
			Log.write(comentarioERRORControlado);
			Log.write("Puede ver la �ltima captura de la prueba en: " + captura);
			fail(bufferErrores);
		}

		// Si resultado vac�o
		else if(resultado.getResultado() == "")
		{
			// Si hay errores
			if(!"".equals(bufferErrores))
			{
				Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "ERROR", Utils.adivinarError(resultado, comentarioERROR), captura);
				Log.write("ERRORES: " + bufferErrores);
				if(!"".equals(bufferWarnings))
					Log.write("WARNINGS: " + bufferWarnings);
				Log.write("Puede ver la �ltima captura de la prueba en: " + captura);
				fail(bufferErrores);
			}
			// Si hay Warnings
			else if(!"".equals(bufferWarnings))
			{
				Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "WARNING", comentarioWARNINGgen, captura);
				Log.write("WARNINGS: " + bufferWarnings);
				Log.write("Puede ver la �ltima captura de la prueba en: " + captura);
				fail(bufferWarnings);
			}
			// Algo raro ha pasado
			else
			{
				Log.write("Puede ver la �ltima captura de la prueba en: " + captura);
				Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "NO EJECUTADO", comentarioNOEJECUTADO, captura);
			}
		}
	}


	public static void guardarResultadoEscenia(WebDriver driver, boolean cadenaValida, Resultados resultado, String m_codigo, String m_nombre) throws Exception
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		PrintStream ficheroMain = new PrintStream(new BufferedOutputStream(new FileOutputStream("results/Main.txt", true)), true);
		ficheroMain.append("[INFO] [FIN]" + Utils.GetTodayDateAndTime() + " Termina caso: " + m_nombre + ", codigo: " + m_codigo + "\n");
		ficheroMain.close();

		// Hacemos captura de pantalla
		if(Utils.isAlertPresent(driver))
			driver.switchTo().alert().accept();
		String captura;
		try
		{
			captura = Utils.takeScreenShotChromeIncognito(driver, m_nombre);

		}
		catch(Exception e1)
		{

			captura = "";
		}

		Utils.quit(driver);

		try
		{
			Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe");
		}
		catch(Exception e)
		{}

		// L�gica de los resultados
		String comentarioWarning = resultado.getComentario() + ". Se han producido warnings. Revisar la traza";
		String comentarioOK = resultado.getComentario();
		String comentarioERRORControlado = resultado.getComentario();
		String comentarioERROR = "Se ha producido un error. Revisar la traza";
		String comentarioWARNINGgen = "Hay avisos en el log. Revisar la traza";
		String comentarioNOEJECUTADO = "No se ejecut� el caso. Revisar la traza";

		String bufferErrores = resultado.getBufferErrores().toString();
		String bufferWarnings = resultado.getBufferWarnings().toString();


		// Si resultado es Ok y cadena V�lida
		if((resultado.getResultado() == "OK") && (cadenaValida))
		{
			Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "OK", comentarioOK, captura);
			Log.write(comentarioOK);
		}

		// Si resultado es Ok y alg�n buffer NO VAC�O
		else if((resultado.getResultado() == "OK") && (!cadenaValida))
		{
			Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "WARNING", comentarioWarning, captura);
			if(!"".equals(bufferErrores))
				Log.write("ERRORES: " + bufferErrores);
			if(!"".equals(bufferWarnings))
				Log.write("WARNINGS: " + bufferWarnings);
			Log.write("Puede ver la �ltima captura de la prueba en: " + captura);
		}

		// Si resultado ERROR
		else if(resultado.getResultado() == "ERROR")
		{
			Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "ERROR", comentarioERRORControlado, captura);
			Log.write(comentarioERRORControlado);
			Log.write("Puede ver la �ltima captura de la prueba en: " + captura);
			fail(bufferErrores);
		}

		// Si resultado vac�o
		else if(resultado.getResultado() == "")
		{
			// Si hay errores
			if(!"".equals(bufferErrores))
			{
				Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "ERROR", Utils.adivinarError(resultado, comentarioERROR), captura);
				Log.write("ERRORES: " + bufferErrores);
				if(!"".equals(bufferWarnings))
					Log.write("WARNINGS: " + bufferWarnings);
				Log.write("Puede ver la �ltima captura de la prueba en: " + captura);
				fail(bufferErrores);
			}
			// Si hay Warnings
			else if(!"".equals(bufferWarnings))
			{
				Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "WARNING", comentarioWARNINGgen, captura);
				Log.write("WARNINGS: " + bufferWarnings);
				Log.write("Puede ver la �ltima captura de la prueba en: " + captura);
				fail(bufferWarnings);
			}
			// Algo raro ha pasado
			else
			{
				Log.write("Puede ver la �ltima captura de la prueba en: " + captura);
				Utils.CsvAppend(params.getValorPropiedad("rutaResultados"), m_codigo, m_nombre, "NO EJECUTADO", comentarioNOEJECUTADO, captura);
			}
		}
	}


	public static String adivinarError(Resultados resultado, String error)
	{

		String bufferErrores = resultado.getBufferErrores().toString();
		Scanner scanner = new Scanner(bufferErrores);

		String errorExtraido = "";

		if(bufferErrores.indexOf("Unable to locate element") > -1)
		{
			try
			{
				bufferErrores = scanner.nextLine() + scanner.nextLine();
				if(bufferErrores.indexOf("xpath") > -1)
				{
					errorExtraido = bufferErrores.substring(bufferErrores.indexOf("//"), bufferErrores.indexOf("}") - 1);
					errorExtraido = errorExtraido.replace(",", " ");
					errorExtraido = "No se encontr� el elemento: " + errorExtraido;
				}
			}
			catch(Exception e)
			{
				errorExtraido = "";
			}

		}

		else if(bufferErrores.indexOf("Timed out after") > -1)
		{
			try
			{
				bufferErrores = scanner.nextLine() + scanner.nextLine();
				if(bufferErrores.indexOf("xpath") > -1)
				{
					errorExtraido = bufferErrores.substring(bufferErrores.indexOf("//"), bufferErrores.indexOf("]") + 1);
					errorExtraido = errorExtraido.replace(",", " ");
					errorExtraido = "TimeOut esperando al elemento: " + errorExtraido;
				}
			}
			catch(Exception e)
			{
				errorExtraido = "";
			}

		}
		else if(bufferErrores.indexOf("No se pudo realizar el login.") > -1)
		{
			errorExtraido = bufferErrores;
		}


		if(errorExtraido.equalsIgnoreCase(""))
			return error;
		else
			return errorExtraido;


	}


	public static void guardarEjecucion(WebDriver driver, Resultados resultado)
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		Utils.quit(driver);
		String bufferErrores = resultado.getBufferErrores().toString();
		String bufferWarnings = resultado.getBufferWarnings().toString();
		if(!"".equals(bufferErrores))
		{
			Log.write("ERRORES: " + bufferErrores);
			fail(bufferErrores);
		}
		if(!"".equals(bufferWarnings))
			Log.write("WARNINGS: " + bufferWarnings);
	}


	public static void setResultadoOK(Resultados resultado, String comentario)
	{
		if("".equals(resultado.getBufferErrores().toString()))
		{
			resultado.setResultado("OK");
			resultado.setComentario(comentario);
		}
	}
	
	


	public static void setResultadoERROR(Resultados resultado, String comentario)
	{
		resultado.appendError(comentario);
		resultado.setResultado("ERROR");
		resultado.setComentario(comentario);
	}
	
	public static void setResultadoWarning(Resultados resultado, String comentario)
	{
		resultado.appendWarning(comentario);
		resultado.setResultado("WARNING");
		resultado.setComentario(comentario);
	}


//	public static void vp(WebDriver driver, Resultados resultado, String NombreVP, Object Esperado, Object Actual, String Pantallazo){
//		String cadenaAleatoria = Utils.CadenaAleatoria(5);
//		boolean ResultadoVP;
//		if (Esperado==Actual)ResultadoVP=true;
//		else if (Esperado.toString().equals(Actual.toString())) ResultadoVP=true;
//		else ResultadoVP=false;
//		String mensajeLog;
//		if (ResultadoVP)
//			Log.write("Validaci�n OK: "+NombreVP);
//		else
//		{
//			mensajeLog="Error en validaci�n: "+NombreVP+". Se esperaba "+Esperado.toString()+" y el valor actual es "+Actual.toString()+". ";
//			resultado.appendError(System.getProperty("line.separator")+mensajeLog);
//			Log.write(mensajeLog);
//		}
//		if (Pantallazo.equals("SI")||(Pantallazo.equals("FALLO")&&!ResultadoVP))
//		{
//			mensajeLog="Captura de la validaci�n en "+takeScreenShot(driver,resultado.getNombreCP()+"_"+cadenaAleatoria,"html");
//			resultado.appendError(mensajeLog);
//			Log.write(mensajeLog);
//		}
//	}

	public static void vp(WebDriver driver, Resultados resultado, String NombreVP, Object Esperado, Object Actual, String Pantallazo)
	{

		String cadenaAleatoria = Utils.CadenaAleatoria(5);

		boolean ResultadoVP;

		if(Esperado == Actual)
			ResultadoVP = true;
		else if(Esperado.toString().equals(Actual.toString()))
			ResultadoVP = true;
		else
			ResultadoVP = false;

		String mensajeLog;

		if(ResultadoVP)
			Log.write("Validaci�n OK: " + NombreVP);
		else

		{
			mensajeLog = "Error en validaci�n: " + NombreVP + ". Se esperaba " + Esperado.toString() + " y el valor actual es " + Actual.toString() + ". ";
			resultado.appendError(System.getProperty("line.separator") + mensajeLog);
			Log.write(mensajeLog);
		}

		if(Pantallazo.equals("SI"))
		{
			mensajeLog = "Captura de la validaci�n en " + takeScreenShot(driver, resultado.getNombreCP() + "_" + cadenaAleatoria, "html");
			Log.write("Validaci�n: " + NombreVP + "." + mensajeLog);

		}

		else if((Pantallazo.equals("FALLO") && !ResultadoVP))
		{
			mensajeLog = "Captura de la validaci�n en " + takeScreenShot(driver, resultado.getNombreCP() + "_" + cadenaAleatoria, "html");
			resultado.appendError(mensajeLog);
			Log.write(mensajeLog);
		}
	}


	public static void error(WebDriver driver, Resultados resultado, String comentario)
	{
		String captura = takeScreenShot(driver, resultado.getNombreCP(), "html");
		Log.write("Error: " + comentario + ". Puede ver la captura en: " + captura);
		resultado.appendError(comentario + ". Puede ver la captura en: " + captura);
	}


	public static void vpWarning(WebDriver driver, Resultados resultado, String NombreVP, Object Esperado, Object Actual, String Pantallazo)
	{
		String cadenaAleatoria = Utils.CadenaAleatoria(5);
		boolean ResultadoVP;
		if(Esperado == Actual)
			ResultadoVP = true;
		else if(Esperado.toString().equals(Actual))
			ResultadoVP = true;
		else
			ResultadoVP = false;
		String mensajeLog;
		if(ResultadoVP)
			Log.write("Validaci�n OK: " + NombreVP);
		else
		{
			mensajeLog = " en validaci�n: " + NombreVP + ". Se esperaba " + Esperado.toString() + " y el valor actual es " + Actual.toString() + ". ";
			resultado.appendWarning(System.getProperty("line.separator") + "Aviso" + mensajeLog);
			Log.write("Aviso " + mensajeLog);
		}
		if(Pantallazo.equals("SI"))
		{
			mensajeLog = "Captura de la validaci�n en " + takeScreenShot(driver, resultado.getNombreCP() + "_" + cadenaAleatoria, "html");
			Log.write(mensajeLog);
		}

		else if((Pantallazo.equals("FALLO") && !ResultadoVP))
		{
			mensajeLog = "Captura de la validaci�n en " + takeScreenShot(driver, resultado.getNombreCP() + "_" + cadenaAleatoria, "html");
			resultado.appendWarning(mensajeLog);
			Log.write(mensajeLog);
		}

	}


	public static String takeScreenShot(WebDriver driver, String fileName, String opcion)
	{
		
		// Obtenemos la fecha y hora de hoy
		java.util.Date hoy = new java.util.Date();
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd_MM_yyyy_H_m_s");
		String horaActual = formatter2.format(hoy);

		// Cogemos del properties la ruta donde se han de guardar las capturas
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();


		String ruta = params.getValorPropiedad("rutaScreenshots");
		
		String rutaScreenshots = ruta;

		try {

			TakesScreenshot scrShot =((TakesScreenshot)driver);
			
			File screenShot=scrShot.getScreenshotAs(OutputType.FILE);
			
			File ficheroDestino = new File(ruta + fileName + "_" + horaActual + ".jpg");
			FileUtils.copyFile(screenShot, ficheroDestino);
			// return ficheroDestino.getAbsolutePath();
			
			if(opcion.equalsIgnoreCase("html"))
				rutaScreenshots = "<html><a href='../" + ruta + ficheroDestino.getName() + "' target='_blank'>" + ruta + ficheroDestino.getName() + "</a></html>";
//  		return "<html><a href='../" + ruta + ficheroDestino.getName() + "' target='_blank'>" + ruta + ficheroDestino.getName() + "</a></html>";
			else
				rutaScreenshots = ruta + ficheroDestino.getName();
				//return ruta + ficheroDestino.getName();

		} catch (Exception e) {
			Log.writeException(e);
			return rutaScreenshots;
		}
		
		return rutaScreenshots;

		
	}


	public static String takeScreenShotChromeIncognito(WebDriver driver, String fileName) throws Exception
	{

		// Obtenemos la fecha y hora de hoy
		java.util.Date hoy = new java.util.Date();
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd_MM_yyyy_H_m_s");
		String horaActual = formatter2.format(hoy);

		// Cogemos del properties la ruta donde se han de guardar las capturas
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String ruta = params.getValorPropiedad("rutaScreenshots");


		BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
		ImageIO.write(image, "jpg", new File(ruta + fileName + "_" + horaActual + ".png"));

		return ruta + fileName + "_" + horaActual + ".png";
	}


	public static void CSVToTable(java.util.Date hoy, String horaInicio, String horaFin, List<String> lista) throws Exception
	{

		String urlLogo = "https://sites.google.com/a/bbva.com/plataforma_banca_empresas/home/logo.PNG";
		Random random = new Random();
		int segundosEspera = random.nextInt(5000);

		Thread.sleep(segundosEspera);

		// M�todo para pasar del CSV de resultados a una tabla HTML

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String contenido = "<!DOCTYPE html>\n" + "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"es\" lang=\"es\" dir=\"ltr\">\n" + "<head>\n" + "<style>* {font-family:Arial;}</style>\n"
				+ "<title>Resumen Pruebas Autom�ticas</title>\n" + "<meta http-equiv=\"Content-Type\" content=\"text/html;\" /> <meta http-equiv=\"Content-Type\" content=\"image/jpg;\" /> <meta http-equiv=\"Content-Type\" content=\"image/jpeg;\" />\n" + "<script type=\"teXt/javascript\"> \n"
				+ "function desplegar(tabla_a_desplegar,estadoT, estadoTfila,comentario) {\n" + "var tablA = document.getElementById(tabla_a_desplegar); \n"
				+ "var estadOt = document.getElementById(estadoT);\n" + "var fila = document.getElementById(estadoTfila);\n" + "switch(tablA.style.display) {\n" + "case \"none\": \n"
				+ "tablA.style.display = \"block\";\n" + "estadOt.innerHTML = \"Ocultar\";\n" + "fila.innerHTML = \"\";\n" + "fila.innerHTML = comentario;\n" + "break; default: \n"
				+ "tablA.style.display = \"none\"; \n" + "estadOt.innerHTML = \"Mostrar\"\n" + "break; \n" + "} \n" + "} \n" + "</script>\n"
				+ "<style>table,th,td{border:1px solid black;border-collapse:collapse;font-size:14px;border-color:#094fa4;}th,td{padding:5px;}th{text-align:left;}</style>\n" + "</head>\n"
				+ "<body>\n" + "<img src=\"" + urlLogo + "\" alt=\"BBVA netCash Pruebas Autom�ticas\">" + Utils.devuelveResumenHTML(hoy, horaInicio, horaFin, lista);

		contenido += Utils.devuelveInformeDetallado(lista);

		// Autor
		contenido += "<br><a>@usuario: " + System.getProperty("user.name").toLowerCase() + "</a>";

		// Cerramos el string con las etiquetas HTML correspondientes
		contenido += "\n" + "</body>" + "</html>";

		// Volcamos este string a un fichero nuevo
		File file = new File("results/ResumenEjecucionPruebas.html");

		FileWriter fileHtm = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fileHtm);

		bw.write(contenido);
		bw.close();

	}


	public static String devuelveResumenHTML(java.util.Date hoy, String horaInicio, String horaFin, List<String> lista) throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String archivoCSV = params.getValorPropiedad("rutaResultados");
		String navegador = params.getValorPropiedad("browser");
		String entorno = params.getValorPropiedad("entorno");
		String pais = params.getValorPropiedad("pais");

		CsvReader reader = new CsvReader(new FileReader(archivoCSV));

		// java.util.Date hoy = new java.util.Date();
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
		String diaActual = formatter2.format(hoy);

		int resultadosOK = 0;
		int resultadosERROR = 0;
		int resultadosWARNING = 0;
		int resultadosOK2 = 0;
		int resultadosERROR2 = 0;
		int resultadosWARNING2 = 0;

		String contenido = "";

		try
		{

			while(reader.readRecord())
			{
				String testName = reader.get(1);
				String resul = reader.get(2);
				
				if (!reader.get(0).contains("NO_ACTUALIZAR")){

					if(lista != null)
					{
						for(int k = 0; k < lista.size(); ++k)
						{
							if(lista.get(k).indexOf(testName) > -1)
								resul = resul + "*";
						}
					}
	
					if(resul.indexOf("OK") > -1)
						resultadosOK++;
					else if(resul.indexOf("ERROR") > -1)
						resultadosERROR++;
					else if(resul.indexOf("WARNING") > -1)
						resultadosWARNING++;
	
					if(resul.equalsIgnoreCase("OK*"))
						resultadosOK2++;
					else if(resul.equalsIgnoreCase("ERROR*"))
						resultadosERROR2++;
					else if(resul.equalsIgnoreCase("WARNING*"))
						resultadosWARNING2++;
				}

			}

			int totalesINT = resultadosOK + resultadosERROR + resultadosWARNING;

			double totales = resultadosOK + resultadosERROR + resultadosWARNING;
			double porcentajeOK = Math.round((resultadosOK * 1.0 / totales) * 100.0);
			double porcentajeERROR = Math.round((resultadosERROR * 1.0 / totales) * 100.0);
			double porcentajeWARNING = Math.round((resultadosWARNING * 1.0 / totales) * 100.0);

			try
			{
				porcentajeOK = Utils.roundTwoDecimals((resultadosOK * 1.0 / totales) * 100.0);
				porcentajeERROR = Utils.roundTwoDecimals((resultadosERROR * 1.0 / totales) * 100.0);
				porcentajeWARNING = Utils.roundTwoDecimals((resultadosWARNING * 1.0 / totales) * 100.0);
			}
			catch(Exception e)
			{

			}

			String literalSatisfactorios = "Satisfactorios";
			String literalError = "Error";
			String literalAvisos = "Avisos";

			if(resultadosOK2 > 0)
				literalSatisfactorios = "Satisfactorios*";
			if(resultadosERROR2 > 0)
				literalError = "Error*";
			if(resultadosWARNING2 > 0)
				literalAvisos = "Avisos*";

			String literalTrasReEjecucion = "";

			if(resultadosOK2 > 0 || resultadosERROR2 > 0 || resultadosWARNING2 > 0)
			{
				int totalesReejecutados = lista.size();
				literalTrasReEjecucion = "<br><a>*Tras reejecuci�n de " + totalesReejecutados + " casos por error o por dependencia de un caso de prueba err�neo:</a>";

				if(resultadosOK2 > 0)
					literalTrasReEjecucion = literalTrasReEjecucion + "<br><a>" + resultadosOK2 + " caso(s) Satisfactorios</a>";
				if(resultadosERROR2 > 0)
					literalTrasReEjecucion = literalTrasReEjecucion + "<br><a>" + resultadosERROR2 + " caso(s) Error</a>";
				if(resultadosWARNING2 > 0)
					literalTrasReEjecucion = literalTrasReEjecucion + "<br><a>" + resultadosWARNING2 + " caso(s) Avisos</a>";
			}


			contenido = "<h3>Datos de Ejecuci�n</h3>\n" + "<table style=\"width:300px\">\n" + "<tr><td>Pa�s</td><td>"
					+ pais
					+ "</td></tr>\n"
					+ "<tr><td>Navegador</td><td>"
					+ navegador
					+ "</td></tr>\n"
					+ "<tr><td>Entorno</td><td>"
					+ entorno
					+ "</td></tr>\n"
					+ "<tr><td>Fecha de Ejecuci�n</td><td>"
					+ diaActual
					+ "</td></tr>\n"
					+ "<tr><td>Hora inicio</td><td>"
					+ horaInicio
					+ "</td></tr>\n"
					+ "<tr><td>Hora fin</td><td>"
					+ horaFin
					+ "</td></tr>\n"
					+ "</table>\n"
					+ "<h3>Resumen Ejecuci�n</h3>\n"
					+ "<table style=\"width:300px\">\n"
					+ "<tr><th style=\"background-color:#b5e5f9\">Resumen Resultados</th><th style=\"background-color:#b5e5f9\">N�mero de casos</th><th style=\"background-color:#b5e5f9\">%</th></tr>\n"
					+ "<tr><td>" + literalSatisfactorios + "</td><td>" + resultadosOK + "</td><td>" + porcentajeOK + "%</td>\n" + "<tr><td>" + literalError + "</td><td>" + resultadosERROR
					+ "</td><td>" + porcentajeERROR + "%</td>\n" + "<tr><td>" + literalAvisos + "</td><td>" + resultadosWARNING + "</td><td>" + porcentajeWARNING + "%</td>\n"
					+ "<tr><th>TOTAL</th><th>" + totalesINT + "</th><th>100%</th>\n" + "</table>\n" + literalTrasReEjecucion + "<h3>Informe Detallado</h3>\n";


		}
		catch(IOException e)
		{
			utils.Log.writeException(e);
		}


		return contenido;

	}


	public static String devuelveResumenParaMail()
	{


		File archivoHTML = new File("results/ResumenEjecucionPruebas.html");
		
//		File archivoHTML = new File("results/InformeHTML/resumenEjecucion.html");

		StringBuilder contentBuilder = new StringBuilder();

		try
		{
			BufferedReader in = new BufferedReader(new FileReader(archivoHTML));
			String str;
			while((str = in.readLine()) != null)
			{
				contentBuilder.append(str);
			}
			in.close();
		}
		catch(IOException e)
		{}
		String content = contentBuilder.toString();
		
//		return content;

		int inicio = content.indexOf("<h3>Datos de Ejecuci�n</h3>");
		int fin = content.indexOf("<h3>Informe Detallado</h3>");


		if(inicio > -1 && fin > -1)
			return content.substring(inicio, fin);
		else
			return "";

	}


	public static String devuelveInformeDetallado(List<String> lista) throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String archivoCSV = params.getValorPropiedad("rutaResultados");


		CsvReader reader = new CsvReader(new FileReader(archivoCSV));
		boolean titulos = true;

		String contenido = "<table style=\"\"><tbody><tr>\n" + "<th style=\"background-color:#b5e5f9\">C�digo Caso</th>\n" + "<th style=\"background-color:#b5e5f9; width:800px\" >Caso</th>\n"
				+ "<th style=\"background-color:#b5e5f9\">Prioridad</th>\n" + "<th style=\"background-color:#b5e5f9\">Pasos</th>\n"
				+ "<th style=\"background-color:#b5e5f9\">Resultados esperados</th>\n" + "<th style=\"background-color:#b5e5f9\">Resultado</th>\n"
				+ "<th style=\"background-color:#b5e5f9; width:400px\">Comentario</th>\n" + "<th style=\"background-color:#b5e5f9\">Evidencia</th>\n"
				+ "<th style=\"background-color:#b5e5f9\">Detalle ejecuci�n</th></tr>\n";

		try
		{
			int i = 0;
			int j = 9999;
			while(reader.readRecord())
			{
				String id = reader.get(0);
				String testName = reader.get(1);
				String resultado = reader.get(2);
				String comentario = reader.get(3);
				String captura = reader.get(4);
				
				if (!id.contains("NO_ACTUALIZAR")){

				if(lista != null)
				{
					for(int k = 0; k < lista.size(); ++k)
					{
						if(lista.get(k).indexOf(testName) > -1)
						{
							resultado = resultado + "*";
						}
					}
				}

				// File ficheroDestino = new File("results/"+testName+".txt");
				// String txt = ficheroDestino.getAbsolutePath();

				String txt = testName + ".html";

				if(titulos)
					titulos = false;
				else
					contenido += "\n" + "<tr>" + "<td>" + id + "</td>" + "<td>" + Utils.devuelveDatoDocumentacion(id, "Caso", testName) + "</td>" + "<td>"
							+ Utils.devuelveDatoDocumentacion(id, "Prioridad", "") + "</td>" + "<td><div id=\"estadoT" + i + "\" onClick=\"desplegar('tabla_a_desplegar" + i + "','estadoT"
							+ i
							+ "', 'estadoTfila"
							+ i
							+ "', '"
							+ "Pasos: <br />"
							+ Utils.devuelveDatoDocumentacion(id, "Pasos", "")
							+ "')\" style=\"text-align:center; background: #E9FAD0; cursor: pointer;\">Mostrar</div></td>"
							// + "<td>"+ Utils.devuelveDatoDocumentacion(id, "Pasos", "") + "</td>"
							+ "<td><div id=\"estadoT" + j + "\" onClick=\"desplegar('tabla_a_desplegar" + j + "','estadoT" + j + "', 'estadoTfila"
							+ j
							+ "', '"
							+ "Resultado Esperado: "
							+ Utils.devuelveDatoDocumentacion(id, "ResultadoEsperado", "")
							+ "')\" style=\"text-align:center; background: #FDF158; cursor: pointer;\">Mostrar</div></td>"
							// + "<td>"+ Utils.devuelveDatoDocumentacion(id, "ResultadoEsperado", "") + "</td>"
							+ "<td" + devuelveFormatoResultado(resultado) + resultado + "</td>" + "<td>" + Utils.cortarComentario(comentario) + "</td>" + "<td>" + "<a href=\"" + "../screenshots/"
							+ Utils.nombreFichero(captura) + "\" target=\"_blank\">" + Utils.cortarPalabraLarga("/screenshots/" + Utils.nombreFichero(captura)) + "</a></td>" + "<td>" + "<a href=\""
							+ txt + "\" target=\"_blank\">" + "Ver Log" + "</a></td></tr>" + "<tr> <td colspan=\"9\" style=\"border-bottom: 1px solid #FFF;\"> <table id=\"tabla_a_desplegar" + i
							+ "\" style=\"display:none;\"> <tr> <td id=\"estadoTfila" + i + "\" style=\" background: #E9FAD0\">Pasos y Condiciones</td> </tr> </table> </td> </tr> "
							+ "<tr> <td colspan=\"9\"> <table id=\"tabla_a_desplegar" + j + "\" style=\"display:none;\"> <tr> <td id=\"estadoTfila" + j
							+ "\" style=\" background: #FDF158\">Resultado Esperado</td> </tr> </table> </td> </tr> ";
				i++;
				j--;
				}
			}

			contenido += "</tbody></table>";

		}
		catch(IOException e)
		{
			utils.Log.writeException(e);
		}


		return contenido;

	}


	public static String devuelveFormatoResultado(String resultado)
	{
		if(resultado.indexOf("OK") > -1)
			return " style=\"background-color: lightgreen;font-weight:bold;\">";
		else if(resultado.indexOf("ERROR") > -1)
			return " style=\"background-color: #F46355;font-weight:bold;\">";
		else if(resultado.indexOf("WARNING") > -1)
			return " style=\"background-color: lightyellow; font-weight:bold;\">";
		else
			return ">";

	}


	public static String nombreFichero(String rutaCompleta)
	{
		int opcion1 = rutaCompleta.lastIndexOf("/");
		int opcion2 = rutaCompleta.lastIndexOf("\\");

		if(opcion1 > -1)
			return rutaCompleta.substring(opcion1 + 1);
		else if(opcion2 > -1)
			return rutaCompleta.substring(opcion2 + 1);
		else
			return rutaCompleta;
	}


	public static String cortarPalabraLarga(String palabra)
	{
		int tamano = palabra.length();
		String nuevaPalabra = "";

		int numeroCorte = 30;
		int posicionInicio = 0;

		if(tamano > numeroCorte)
		{
			int numerobr = tamano / numeroCorte;
			for(int i = 0; i < numerobr; i++)
			{
				posicionInicio = i * numeroCorte;
				nuevaPalabra += palabra.subSequence(posicionInicio, posicionInicio + numeroCorte) + "<br>";
			}
			return nuevaPalabra + palabra.subSequence(posicionInicio + numeroCorte, palabra.length());
		}

		else
			return palabra;


	}
	
	public static String cortarPalabraLargaPorTamano(String palabra, int numeroCorte)
	{
		int tamano = palabra.length();
		String nuevaPalabra = "";

		int posicionInicio = 0;

		if(tamano > numeroCorte)
		{
			int numerobr = tamano / numeroCorte;
			for(int i = 0; i < numerobr; i++)
			{
				posicionInicio = i * numeroCorte;
				nuevaPalabra += palabra.subSequence(posicionInicio, posicionInicio + numeroCorte) + "<br>";
			}
			return nuevaPalabra + palabra.subSequence(posicionInicio + numeroCorte, palabra.length());
		}

		else
			return palabra;


	}
	
	public static String cortarPalabraLargaPorTamanoPuntosSuspensivos(String palabra, int numeroCorte)
	{
		int tamano = palabra.length();
		String nuevaPalabra = "";
		
		if (tamano>30 && !palabra.contains(" ")){
			palabra = palabra.replace("_", " ");
		}

		if(tamano > numeroCorte)
		{
			nuevaPalabra = palabra.substring(0, numeroCorte)+"...";
			return nuevaPalabra;
		}

		else
			return palabra;


	}


	public static String cortarComentario(String palabra)
	{

		try
		{
			Scanner scanner = new Scanner(palabra);
			String input = scanner.nextLine();
			String[] words = input.split(" ");

			int maxLength = 0;

			String longestWord = "";

			for(String word : words)
			{
				if(word.length() > maxLength)
				{
					maxLength = word.length();
					longestWord = word;
				}
			}
			scanner.close();
			return palabra.replace(longestWord, Utils.cortarPalabraLargaPorTamano(longestWord,42));

		}
		catch(Exception e)
		{
			return "";
		}


	}


	public static String devuelveDatoDocumentacion(String codigo, String campo, String original) throws Exception
	{

		String resultado = "";
		String ruta = "resources/Documentacion.xls";

		WorkbookSettings ws = new WorkbookSettings(); 
		ws.setEncoding("Cp1252");
		
		Workbook excelLogin = Workbook.getWorkbook(new File(ruta),ws);
		Sheet sheet1 = excelLogin.getSheet("Documentacion");

		// Obtenemos las columnas de Pa�s, de Entorno y de URL
		int columnaCodigo = sheet1.findCell("CodigoCaso").getColumn();
		int columnaDato = sheet1.findCell(campo).getColumn();

		boolean encontrado = false;

		for(int i = 0; (i < sheet1.getRows()) && !encontrado; i++)
		{
			if(sheet1.getCell(columnaCodigo, i).getContents().equalsIgnoreCase(codigo))
			{
				encontrado = true;
				resultado = sheet1.getCell(columnaDato, i).getContents();
			}
		}

		if(!encontrado)
			return original;
		else
		{
			String resultadoparcial = resultado.replaceAll("\"", "");
			resultadoparcial = resultadoparcial.replaceAll("'", "");
			resultadoparcial = resultadoparcial.replaceAll("(\r\n|\n)", "<br />");
			return resultadoparcial;
		}

	}


	public static String devuelveURLApartirDePaisYEntorno(String ruta, String pais, String entorno) throws Exception
	{

		String url = "";

		WorkbookSettings ws = new WorkbookSettings(); 
		ws.setEncoding("Cp1252");
		
		Workbook excelLogin = Workbook.getWorkbook(new File(ruta),ws);
		Sheet sheet1 = excelLogin.getSheet("URLs");

		// Obtenemos las columnas de Pa�s, de Entorno y de URL
		int columnaPais = sheet1.findCell("Pais").getColumn();
		int columnaEntorno = sheet1.findCell("Entorno").getColumn();
		int columnaURL = sheet1.findCell("URL").getColumn();

		boolean encontrado = false;

		for(int i = 0; (i < sheet1.getRows()) && !encontrado; i++)
		{
			if(sheet1.getCell(columnaPais, i).getContents().equalsIgnoreCase(pais) && sheet1.getCell(columnaEntorno, i).getContents().equalsIgnoreCase(entorno))
			{
				encontrado = true;
				url = sheet1.getCell(columnaURL, i).getContents();
			}
		}

		if(!encontrado)
			throw new Exception("No existe la combinaci�n de Pa�s/Entorno. Revisar archivo properties.");

		return url;

	}


	public static WebDriver inicializarCaso(WebDriver driver, String m_codigo, String m_nombre, String...cositas) throws Exception
	{

		try
		{
			// Establecemos que la salida est�ndar y la salida de errores sea el fichero que se crea por caso de prueba
			FileOutputStream fichero = new FileOutputStream("results/" + m_nombre + ".html", true);
			PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);
			PrintStream ficheroMain = new PrintStream(new BufferedOutputStream(new FileOutputStream("results/Main.txt", true)), true);

			ficheroMain.append("[INFO] [INICIO]" + Utils.GetTodayDateAndTime() + " Lanzando caso: " + m_nombre + ", codigo: " + m_codigo + "\n");
			ficheroMain.close();

//			System.setOut(ficheroSalida);
//			System.setErr(ficheroSalida);
			
			loggers.put(Long.toString(Thread.currentThread().getId()), new Log(m_nombre));

			// Pintamos el inicio de la traza
			Utils.pintarInicioTraza(m_codigo, m_nombre);

			// Cargamos las propiedades del properties
			TestProperties params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();
			String archivoCSV = params.getValorPropiedad("rutaResultados");
			String pais = params.getValorPropiedad("pais");
			String entorno = params.getValorPropiedad("entorno");
			String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
			String url = devuelveURLApartirDePaisYEntorno(rutaURLs, pais, entorno);

			if(url == null || url.equals(""))
			{
				throw new Exception("No se pudo obtener la URL correspondiente a los par�metros: Pa�s = " + pais + ", Entorno = " + entorno + ". Revisar el archivo properties y la excel con las URLs.");
			}


			// Inicializamos el caso a ERROR.
			Utils.CsvAppend(archivoCSV, m_codigo, m_nombre, "ERROR", "", "");

			// Obtenemos el driver espec�fico del navegador
			driver = Utils.getWebDriverBrowser(m_nombre,cositas);

			if(driver == null)
			{
				throw new Exception("No se estableci� correctamente la propiedad correspondiente al navegador. Revisar el archivo properties.");
			}


			// Navegamos a la url
			driver.get(url);

			driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
			
			if (!driver.toString().contains("ChromeDriver"))
				driver.manage().window().maximize();

			if("Certificate Error: Navigation Blocked".equals(driver.getTitle())) {
				driver.navigate().to("javascript:document.getElementById('overridelink').click()");
				Thread.sleep(3500);
				if (Utils.isAlertPresent(driver))
					driver.switchTo().alert().accept();
			}
			
			
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			Utils.quit(driver);
		}

		return driver;
	}


	public static WebDriver inicializarCasoOtroNavegador(WebDriver driver, String m_codigo, String m_nombre) throws Exception
	{


		try
		{
			// Cargamos las propiedades del properties
			TestProperties params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();
			String pais = params.getValorPropiedad("pais");
			String entorno = params.getValorPropiedad("entorno");
			String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
			String url = devuelveURLApartirDePaisYEntorno(rutaURLs, pais, entorno);

			if(url == null || url.equals(""))
			{
				throw new Exception("No se pudo obtener la URL correspondiente a los par�metros: Pa�s = " + pais + ", Entorno = " + entorno + ". Revisar el archivo properties y la excel con las URLs.");
			}

			// Obtenemos el driver espec�fico del navegador
			driver = Utils.getWebDriverBrowser(m_nombre);

			if(driver == null)
			{
				throw new Exception("No se estableci� correctamente la propiedad correspondiente al navegador. Revisar el archivo properties.");
			}

			// driver.get("");

			// Navegamos a la url
			driver.get(url);

			driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
			
			if (!driver.toString().contains("ChromeDriver"))
				driver.manage().window().maximize();
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			Utils.quit(driver);
		}

		return driver;
	}


	public static WebDriver inicializarCasoCMP(WebDriver driver, String m_codigo, String m_nombre) throws Exception
	{

		try
		{
			// Establecemos que la salida est�ndar y la salida de errores sea el fichero que se crea por caso de prueba
			FileOutputStream fichero = new FileOutputStream("results/" + m_nombre + ".html", true);
			PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);

//			System.setOut(ficheroSalida);
//			System.setErr(ficheroSalida);
			
			loggers.put(Long.toString(Thread.currentThread().getId()), new Log(m_nombre));

			// Pintamos el inicio de la traza
			Utils.pintarInicioTraza(m_codigo, m_nombre);

			// Cargamos las propiedades del properties
			TestProperties params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();
			String archivoCSV = params.getValorPropiedad("rutaResultados");
			String url = params.getValorPropiedad("URLSaltoCMP");

			if(url == null || url.equals(""))
			{
				throw new Exception("No se pudo obtener la URL del Salto CMP");
			}

			// Inicializamos el caso a ERROR.
			Utils.CsvAppend(archivoCSV, m_codigo, m_nombre, "ERROR", "", "");

			// Obtenemos el driver espec�fico del navegador
			driver = Utils.getWebDriverBrowser(m_nombre);

			if(driver == null)
			{
				throw new Exception("No se estableci� correctamente la propiedad correspondiente al navegador. Revisar el archivo properties.");
			}

			// Navegamos a la url
			driver.get(url);

			driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
			
			if (!driver.toString().contains("ChromeDriver"))
				driver.manage().window().maximize();
			
			Thread.sleep(3000);
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			Utils.quit(driver);
		}

		return driver;
	}


	public static WebDriver inicializarCasoAplicacion(WebDriver driver, String aplicacion, String m_codigo, String m_nombre) throws Exception
	{

		try
		{
			// Establecemos que la salida est�ndar y la salida de errores sea el fichero que se crea por caso de prueba
			FileOutputStream fichero = new FileOutputStream("results/" + m_nombre + ".html", true);
			PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);

			PrintStream ficheroMain = new PrintStream(new BufferedOutputStream(new FileOutputStream("results/Main.txt", true)), true);
			ficheroMain.append("[INFO] [INICIO]" + Utils.GetTodayDateAndTime() + " Lanzando caso: " + m_nombre + ", codigo: " + m_codigo + "\n");
			ficheroMain.close();


//			System.setOut(ficheroSalida);
//			System.setErr(ficheroSalida);
			
			loggers.put(Long.toString(Thread.currentThread().getId()), new Log(m_nombre));


			// Pintamos el inicio de la traza
			Utils.pintarInicioTraza(m_codigo, m_nombre);

			// Cargamos las propiedades del properties
			TestProperties params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();
			String archivoCSV = params.getValorPropiedad("rutaResultados");
			String entorno = params.getValorPropiedad("entorno");
			String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
			String url = devuelveURLApartirDePaisYEntorno(rutaURLs, aplicacion, entorno);


			if(url == null || url.equals(""))
			{
				throw new Exception("No se pudo obtener la URL de " + aplicacion);
			}

			// Inicializamos el caso a ERROR.
			Utils.CsvAppend(archivoCSV, m_codigo, m_nombre, "ERROR", "", "");

			// Obtenemos el driver espec�fico del navegador, si es Escenia CHROME
			if(aplicacion.equalsIgnoreCase("Escenia"))
			{

				System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");

//		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//		capabilities.setCapability("chrome.switches", Arrays.asList("--incognito"));

				ChromeOptions options = new ChromeOptions();
				options.addArguments("-incognito");
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);

				driver = new ChromeDriver(capabilities);
			}
			else
			{
				driver = Utils.getWebDriverBrowser(m_nombre);
			}

			if(driver == null)
			{
				throw new Exception("No se estableci� correctamente la propiedad correspondiente al navegador. Revisar el archivo properties.");
			}

			// Navegamos a la url
			driver.get(url);

			driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
			try
			{
				if (!driver.toString().contains("ChromeDriver"))
					driver.manage().window().maximize();
			}
			catch(Exception e)
			{}
			Thread.sleep(3000);
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			Utils.quit(driver);
		}

		return driver;
	}


	public static WebDriver inicializarCasoConCacheEstableciendoProxy(WebDriver driver, String aplicacion, String m_codigo, String m_nombre, String tipoProxy) throws Exception
	{

		try
		{
			// Establecemos que la salida est�ndar y la salida de errores sea el fichero que se crea por caso de prueba
			FileOutputStream fichero = new FileOutputStream("results/" + m_nombre + ".html", true);
			PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);

			PrintStream ficheroMain = new PrintStream(new BufferedOutputStream(new FileOutputStream("results/Main.txt", true)), true);
			ficheroMain.append("[INFO] [INICIO]" + Utils.GetTodayDateAndTime() + " Lanzando caso: " + m_nombre + ", codigo: " + m_codigo + "\n");
			ficheroMain.close();


//			System.setOut(ficheroSalida);
//			System.setErr(ficheroSalida);
			
			loggers.put(Long.toString(Thread.currentThread().getId()), new Log(m_nombre));


			// Pintamos el inicio de la traza
			Utils.pintarInicioTraza(m_codigo, m_nombre);

			// Cargamos las propiedades del properties
			TestProperties params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();
			String perfilFirefox = "";

			String rutaFirefox = System.getenv("APPDATA") + "/Mozilla/Firefox/Profiles/";
			File file = new File(rutaFirefox);

			String[] names = file.list();

			for(String name : names)
			{
				if(new File(rutaFirefox + name).isDirectory())
				{
					perfilFirefox = name;
					break;
				}
			}


			// Obtenemos el driver espec�fico del navegador, si es Escenia CHROME
			if(aplicacion.equalsIgnoreCase("Escenia"))
			{

				System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");

//		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//		capabilities.setCapability("chrome.switches", Arrays.asList("--incognito"));

				ChromeOptions options = new ChromeOptions();
				options.addArguments("-incognito");
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);

				driver = new ChromeDriver(capabilities);
			}
			else
			{
				driver = Utils.getWebDriverBrowserWithProfileProxy(rutaFirefox + perfilFirefox, tipoProxy);
			}

			if(driver == null)
			{
				throw new Exception("No se estableci� correctamente la propiedad correspondiente al navegador. Revisar el archivo properties.");
			}

			// Navegamos a la url
			driver.get("");

			driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
			try
			{
				if (!driver.toString().contains("ChromeDriver"))
					driver.manage().window().maximize();
			}
			catch(Exception e)
			{}
			Thread.sleep(3000);
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			Utils.quit(driver);
		}

		return driver;
	}


	public static WebDriver inicializarCasoAplicacionConCache(WebDriver driver, String aplicacion, String m_codigo, String m_nombre) throws Exception
	{

		try
		{
			// Establecemos que la salida est�ndar y la salida de errores sea el fichero que se crea por caso de prueba
			FileOutputStream fichero = new FileOutputStream("results/" + m_nombre + ".html", true);
			PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);

			PrintStream ficheroMain = new PrintStream(new BufferedOutputStream(new FileOutputStream("results/Main.txt", true)), true);
			ficheroMain.append("[INFO] [INICIO]" + Utils.GetTodayDateAndTime() + " Lanzando caso: " + m_nombre + ", codigo: " + m_codigo + "\n");
			ficheroMain.close();


//			System.setOut(ficheroSalida);
//			System.setErr(ficheroSalida);
			
			loggers.put(Long.toString(Thread.currentThread().getId()), new Log(m_nombre));


			// Pintamos el inicio de la traza
			Utils.pintarInicioTraza(m_codigo, m_nombre);

			// Cargamos las propiedades del properties
			TestProperties params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();
			String archivoCSV = params.getValorPropiedad("rutaResultados");
			String entorno = params.getValorPropiedad("entorno");
			String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
			String url = devuelveURLApartirDePaisYEntorno(rutaURLs, aplicacion, entorno);
			String perfilFirefox = "";

			String rutaFirefox = System.getenv("APPDATA") + "/Mozilla/Firefox/Profiles/";
			File file = new File(rutaFirefox);

			String[] names = file.list();

			for(String name : names)
			{
				if(new File(rutaFirefox + name).isDirectory())
				{
					perfilFirefox = name;
					break;
				}
			}


			if(url == null || url.equals(""))
			{
				throw new Exception("No se pudo obtener la URL de " + aplicacion);
			}

			// Inicializamos el caso a ERROR.
			Utils.CsvAppend(archivoCSV, m_codigo, m_nombre, "ERROR", "", "");

			// Obtenemos el driver espec�fico del navegador, si es Escenia CHROME
			if(aplicacion.equalsIgnoreCase("Escenia"))
			{

				System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");

//		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//		capabilities.setCapability("chrome.switches", Arrays.asList("--incognito"));

				ChromeOptions options = new ChromeOptions();
				options.addArguments("-incognito");
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);

				driver = new ChromeDriver(capabilities);
			}
			else
			{
				driver = Utils.getWebDriverBrowserWithProfile(rutaFirefox + perfilFirefox);
			}

			if(driver == null)
			{
				throw new Exception("No se estableci� correctamente la propiedad correspondiente al navegador. Revisar el archivo properties.");
			}

			// Navegamos a la url
			driver.get(url);

			driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
			try
			{
				if (!driver.toString().contains("ChromeDriver"))
					driver.manage().window().maximize();
			}
			catch(Exception e)
			{}
			Thread.sleep(3000);
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			Utils.quit(driver);
		}

		return driver;
	}


	public static WebDriver inicializarEjecucion(WebDriver driver, String m_codigo, String m_nombre) throws Exception
	{

		try
		{
			// Establecemos que la salida est�ndar y la salida de errores sea el fichero que se crea por caso de prueba
			PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(new FileOutputStream("results/" + m_nombre + ".txt")), true);
//			System.setOut(ficheroSalida);
//			System.setErr(ficheroSalida);
			
			loggers.put(Long.toString(Thread.currentThread().getId()), new Log(m_nombre));

			// Pintamos el inicio de la traza
			Utils.pintarInicioTraza(m_codigo, m_nombre);

			// Cargamos las propiedades del properties
			TestProperties params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();
			String pais = params.getValorPropiedad("pais");
			String entorno = params.getValorPropiedad("entorno");
			String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
			String url = devuelveURLApartirDePaisYEntorno(rutaURLs, pais, entorno);

			if(url == null || url.equals(""))
			{
				throw new Exception("No se pudo obtener la URL correspondiente a los par�metros: Pa�s = " + pais + ", Entorno = " + entorno + ". Revisar el archivo properties y la excel con las URLs.");
			}
			// Obtenemos el driver espec�fico del navegador
			driver = Utils.getWebDriverBrowser(m_nombre);

			if(driver == null)
			{
				throw new Exception("No se estableci� correctamente la propiedad correspondiente al navegador. Revisar el archivo properties.");
			}

			// Navegamos a la url
			driver.get(url);

			driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
			if (!driver.toString().contains("ChromeDriver"))
				driver.manage().window().maximize();
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			Utils.quit(driver);
		}

		return driver;
	}


	public static WebDriver inicializarEjecucion(String m_codigo, String m_nombre) throws Exception
	{

		// Establecemos que la salida est�ndar y la salida de errores sea el fichero que se crea por caso de prueba
		FileOutputStream fichero = new FileOutputStream("results/" + m_nombre + ".txt", true);
		PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);

//		System.setOut(ficheroSalida);
//		System.setErr(ficheroSalida);
		
		loggers.put(Long.toString(Thread.currentThread().getId()), new Log(m_nombre));


		// Pintamos el inicio de la traza
		Utils.pintarInicioTraza(m_codigo, m_nombre);

		// Inicializamos el driver de firefox

		FirefoxProfile profile = new FirefoxProfile();

		// Le establecemos una serie de preferencias que nos interesan, como que los archivos indicados no nos pregunte al descargar
		profile.setAcceptUntrustedCertificates(true);
		profile.setPreference("browser.download.folderList", 2);
		String downloads = new File("downloads").getAbsolutePath();
		profile.setPreference("browser.download.dir", downloads);
		profile.setPreference("browser.privatebrowsing.dont_prompt_on_enter", true);
		profile.setPreference("browser.privatebrowsing.autostart", true);
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
				"image/jpg, text/csv,text/xml,application/xml,application/vnd.ms-excel,application/x-excel,application/x-msexcel,application/excel,application/pdf");
//		profile.setEnableNativeEvents(false);
		FirefoxOptions options = new FirefoxOptions();
		options.setProfile(profile);
		
		WebDriver driver = new FirefoxDriver(options);
		return driver;
	}
	
	public static void inicializarUtilidadSinNavegador(String m_codigo, String m_nombre) throws Exception
	{

		// Establecemos que la salida est�ndar y la salida de errores sea el fichero que se crea por caso de prueba
		FileOutputStream fichero = new FileOutputStream("results/" + m_nombre + ".txt", true);
		PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);

//		System.setOut(ficheroSalida);
//		System.setErr(ficheroSalida);
		
		loggers.put(Long.toString(Thread.currentThread().getId()), new Log(m_nombre));


		// Pintamos el inicio de la traza
		Utils.pintarInicioTraza(m_codigo, m_nombre);
	}
	
	


	public static void pintarInicioTraza(String m_codigo, String m_nombre)
	{

		// Cargamos las propiedades que nos interesan del properties
		params.cargarPropiedades();
		String navegador = params.getValorPropiedad("browser");
		String pais = params.getValorPropiedad("pais");
		String entorno = params.getValorPropiedad("entorno");
		String ipHub = params.getValorPropiedad("ipHub");

		// Fecha actual
		java.util.Date hoy = new java.util.Date();
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String horaActual = formatter2.format(hoy);
		
		String charset = "windows-1252";
		
		if (Utils.isUnix())
			charset = "UTF-8";

//		Log.write("<html><head></head><body><pre style='word-wrap: break-word; white-space: pre-wrap;'>");
//		Log.write("--------------------------------------------------");
//		Log.write("	NOMBRE CASO DE PRUEBA: " + m_nombre);
//		Log.write("	PA�S :" + pais);
//		Log.write("	ENTORNO :" + entorno);
//		Log.write("	NAVEGADOR :" + navegador);
//		Log.write("	FECHA DE INICIO :" + horaActual);
//		Log.write("--------------------------------------------------");
//		Log.write();
		
		Log.write("<html><head><meta charset=\""+charset+"\"></head><body><pre style='word-wrap: break-word; white-space: pre-wrap;'>");
//		Log.write("--------------------------------------------------");
//		Log.write("	NOMBRE CASO DE PRUEBA: " + m_nombre);
//		Log.write("	PA�S :" + pais);
//		Log.write("	ENTORNO :" + entorno);
//		Log.write("	NAVEGADOR :" + navegador);
//		Log.write("	FECHA DE INICIO :" + horaActual);
//		Log.write("--------------------------------------------------");
		
		Log.write("<hr noshade>");
		Log.write("<h2 style='color:#069 ;padding-top: 20 px; margin: 0px'>"+m_codigo + "-" + m_nombre+"</h2>");
		Log.write("<style> table, th, td {border: 1px solid #094fa4;padding: 3px;}</style>");
		
		Log.write("<h3 style='margin: 0px'>Datos de ejecuci�n</h3>");
		
		if (ipHub==null || ipHub.equalsIgnoreCase("")){
			Log.write("<table style='width:300px; border-collapse: collapse;'> "+
				  "<tbody><tr><td>Pa�s</td><td>"+pais+"</td></tr>"+
                  "<tr><td>Navegador</td><td>"+navegador+"</td></tr>"+
				  "<tr><td>Entorno</td><td>"+entorno+"</td></tr>"+
				  "<tr><td>Fecha de Inicio</td><td>"+horaActual+"</td></tr>"+
				  "</tbody></table>");
		}
		
		else {
		
			Log.write("<table style='width:300px; border-collapse: collapse;'> "+
				  "<tbody><tr><td>Pa�s</td><td>"+pais+"</td></tr>"+
                "<tr><td>Navegador</td><td>"+navegador+"</td></tr>"+
				  "<tr><td>Entorno</td><td>"+entorno+"</td></tr>"+
				  "<tr><td>Fecha de Inicio</td><td>"+horaActual+"</td></tr>"+
				  "<tr><td>IP Remota</td><td>"+ipHub+"</td></tr>"+
				  "</tbody></table>");
		}
		
		


	}
	
	public static void pintarRefernciaUsuario(String referencia, String usuario)
	{
		Log.write("<h3 style='margin: 0px'>Datos de prueba</h3>");
		Log.write("<table style='width:300px; border-collapse: collapse;'> "+
				  "<tbody><tr><td>Referencia</td><td>"+referencia+"</td></tr>"+
                  "<tr><td>Usuario</td><td>"+usuario+"</td></tr>"+
				  "</tbody></table>");
		Log.write("Traza:");

	}


	// Devuelve una cadena aleatoria de n�meros del tama�o que se le indica
	public static String CadenaAleatoria(int numeroDigitos)
	{
		Random rnd = new Random();
		int x;
		String resul = "0";

		while(resul.charAt(0) == '0')
		{
			resul = "";
			for(int i = 0; i < numeroDigitos; i++)
			{
				x = (int)(rnd.nextDouble() * 10.0);
				resul = resul + String.valueOf(x);
			}
		}

		return resul;
	}


	public static String devuelveNIFAleatorio()
	{
		String NIF_STRING_ASOCIATION = "TRWAGMYFPDXBNJZSQVHLCKE";

		int numeros = Integer.parseInt(Utils.CadenaAleatoria(8));

		String dniAleatorio = String.valueOf(numeros) + NIF_STRING_ASOCIATION.charAt(numeros % 23);

		return dniAleatorio;

	}


	public static String devuelveRUTAleatorio()
	{
		int rut = Integer.parseInt(Utils.CadenaAleatoria(8));
		String cadena = String.valueOf(rut);
		int contador = 2;
		int acumulador = 0;
		int multiplo = 0;

		while(rut != 0)
		{
			multiplo = (rut % 10) * contador;
			acumulador = acumulador + multiplo;
			rut = rut / 10;
			contador++;
			if(contador > 7)
				contador = 2;
		}
		int digito = 11 - (acumulador % 11);
		String RutDigito = String.valueOf(digito);
		if(digito == 10)
			RutDigito = "K";
		if(digito == 11)
			RutDigito = "0";

		return cadena + "-" + RutDigito;
	}

	
	public static String devuelveNombreFicheroDescargado(WebDriver driver, String patron) throws Exception {
		
		String nombreFichero = null;
		
		
		try {
			
			String frameActual = Utils.devuelveFrameActual(driver);		
			Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		    String browserName = cap.getBrowserName().toLowerCase();
		    
		    if (browserName.toLowerCase().contains("chrome")) {
		    	
	
				Thread.sleep(3000);
				
				((JavascriptExecutor)driver).executeScript("window.open()");
				Utils.switchToTab(driver, 1);
				driver.get("chrome://downloads/");
	
				JavascriptExecutor js = (JavascriptExecutor)driver;
				String script = "return downloads.Manager.get().items_.filter(e => e.state === 'COMPLETE')  .map(e => e.filePath || e.file_path);";
				ArrayList archiveID =  (ArrayList) js.executeScript(script);
				
				int contador = 0;
				while (archiveID.size()==0 && contador < 20){
					Thread.sleep(1000);
					archiveID =  (ArrayList) js.executeScript(script);
					contador++;
				}
				
				String path = (String) archiveID.get(0);
				
				if (!path.contains(patron) && !path.contains(patron.toLowerCase()))
					return null;
				
				WebElement element = (WebElement)js.executeScript("var input = window.document.createElement('INPUT'); "+
						"input.setAttribute('type', 'file'); "+
						"input.setAttribute('id', 'paco'); "+
						"input.onchange = function (e) { e.stopPropagation() }; "+
						"return window.document.documentElement.appendChild(input); ");
				
				element.sendKeys(path);
				
				js.executeScript(""
						+ "function sleep(ms) {"+
						" return new Promise(resolve => setTimeout(resolve, ms));"+
						"}"+
						"async function final(){ var input=window.document.getElementById('paco');"+
						"   var reader = new FileReader();  "+
						"  reader.readAsDataURL(input.files[0]);"+
						"   await sleep(2000);console.error (reader.result); }"+
						"final();");
				
				Thread.sleep(7000);
				
				 String base64 = "";
				 for (LogEntry entry : driver.manage().logs().get(LogType.BROWSER)) {
				        String msg = entry.getMessage();
				        if (msg.contains("data:"))
				        	base64 = msg;
				    }
				 
				 int posicion = base64.indexOf("base64,");
				 base64 = base64.substring(posicion+7);
				 
				 byte[] byteArray = Base64.decodeBase64(base64.getBytes());
				 
				 File ficheroNuevo = new File(path);
				 
				 try (OutputStream stream = new FileOutputStream("downloads/"+ficheroNuevo.getName())) {
					    stream.write(byteArray);
				 }
	
				 nombreFichero = new File ("downloads/"+ficheroNuevo.getName()).getAbsolutePath();
				
				 driver.close();
				
				 Utils.switchToTab(driver, 0);
				 
				 Utils.cambiarFrameDadoVisibleName(driver, frameActual);
		    }
		    
		    else if (browserName.toLowerCase().contains("firefox")) {
		    	((JavascriptExecutor)driver).executeScript("window.open()");
		    	Utils.switchToTab(driver, 1);
		    	driver.get("file://"+ Constantes.carpetaDescargaGalatea);
		    	
		    	if (Utils.isDisplayed(driver, "//a[contains(@href,'"+patron+"')]", 2))
		    		nombreFichero = Utils.devuelveElementoVisible(driver, "//a[contains(@href,'"+patron+"')]").getAttribute("href");
				 
		    	driver.close();
					
				Utils.switchToTab(driver, 0);
				
				Utils.cambiarFrameDadoVisibleName(driver, frameActual);
		    }
			 
			
		} catch (Exception e) {
			
		}

		return nombreFichero;
	}
	
public static String devuelveNombreFicheroDescargadoFormato(WebDriver driver, String patron, String formato) throws Exception {
		
		String nombreFichero = null;
		
		try {
			
			String frameActual = Utils.devuelveFrameActual(driver);		
			Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		    String browserName = cap.getBrowserName().toLowerCase();
		    
		    if (browserName.toLowerCase().contains("chrome")) {
		
				Thread.sleep(10000);
				
				((JavascriptExecutor)driver).executeScript("window.open()");
				Utils.switchToTab(driver, 1);
				driver.get("chrome://downloads/");
	
				JavascriptExecutor js = (JavascriptExecutor)driver;
				String script = "return downloads.Manager.get().items_.filter(e => e.state === 'COMPLETE')  .map(e => e.filePath || e.file_path);";
				ArrayList archiveID =  (ArrayList) js.executeScript(script);
				
				int contador = 0;
				while (archiveID.size()==0 && contador < 20){
					Thread.sleep(1000);
					archiveID =  (ArrayList) js.executeScript(script);
					contador++;
				}
				
				String path = (String) archiveID.get(0);
				
				if (!path.contains(patron))
					return null;
				
				if (!path.contains(formato))
					return null;
				
				WebElement element = (WebElement)js.executeScript("var input = window.document.createElement('INPUT'); "+
						"input.setAttribute('type', 'file'); "+
						"input.setAttribute('id', 'paco'); "+
						"input.onchange = function (e) { e.stopPropagation() }; "+
						"return window.document.documentElement.appendChild(input); ");
				
				element.sendKeys(path);
				
				js.executeScript(""
						+ "function sleep(ms) {"+
						" return new Promise(resolve => setTimeout(resolve, ms));"+
						"}"+
						"async function final(){ var input=window.document.getElementById('paco');"+
						"   var reader = new FileReader();  "+
						"  reader.readAsDataURL(input.files[0]);"+
						"   await sleep(2000);console.error (reader.result); }"+
						"final();");
				
				Thread.sleep(7000);
				
				 String base64 = "";
				 for (LogEntry entry : driver.manage().logs().get(LogType.BROWSER)) {
				        String msg = entry.getMessage();
				        if (msg.contains("data:"))
				        	base64 = msg;
				    }
				 
				 int posicion = base64.indexOf("base64,");
				 base64 = base64.substring(posicion+7);
				 
				 byte[] byteArray = Base64.decodeBase64(base64.getBytes());
				 
				 File ficheroNuevo = new File(path);
				 
				 try (OutputStream stream = new FileOutputStream("downloads/"+ficheroNuevo.getName())) {
					    stream.write(byteArray);
				 }
	
				 nombreFichero = new File ("downloads/"+ficheroNuevo.getName()).getAbsolutePath();
				
				 driver.close();
				
				 Utils.switchToTab(driver, 0);
				 Utils.cambiarFrameDadoVisibleName(driver, frameActual);
			 
		    }
			 
			 
			else if (browserName.toLowerCase().contains("firefox")) {
				
			    	((JavascriptExecutor)driver).executeScript("window.open()");
			    	Utils.switchToTab(driver, 1);
			    	driver.get("file://"+ Constantes.carpetaDescargaGalatea);
			    	
			    	if (Utils.isDisplayed(driver, "//a[contains(@href,'"+patron+"')]", 2))
			    		nombreFichero = Utils.devuelveElementoVisible(driver, "//a[contains(@href,'"+patron+"')]").getAttribute("href");
					 
			    	driver.close();
						
					Utils.switchToTab(driver, 0);
					
					Utils.cambiarFrameDadoVisibleName(driver, frameActual);
			    }
			 
			
		} catch (Exception e) {
			
		}

		return nombreFichero;
	}
	
	
//	// Devuelve el nombre del fichero que nos acabamos de descargar
//	public static String devuelveNombreFicheroDescargado(Date fecha, String patron) throws Exception
//	{
//
//		String rutaArchivo = null;
//
//		// Obtenemos la ruta de la carpeta de downloads
//		String dirDownloads = new File("downloads").getAbsolutePath();
//
//		Thread.sleep(25000);
//		
//		rutaArchivo = buscarArchivoEnRuta(dirDownloads, patron, fecha);
//		
////		//Segundo intento por si fuera Grid		
////		if(rutaArchivo==null)
////			rutaArchivo = buscarArchivoEnRuta(Constantes.rutaCompartidaDescargas, patron, fecha);
//			
//			
//		return rutaArchivo;
//	}
	
public static String buscarArchivoEnRuta (String dirDownloads, String patron, Date fecha, String formato) {
		
		String rutaArchivo = null;
		
		File fl = new File(dirDownloads);

		// Todos los archivos de la carpeta
		File[] files = fl.listFiles(new FileFilter()
		{
			@Override
			public boolean accept(File file)
			{
				return file.isFile();
			}
		});

		// Obtenemos el �ltimo modificado
		long lastMod = Long.MIN_VALUE;
		File choise = null;
		for(File file : files)
		{
			if(file.lastModified() > lastMod && file.getName().indexOf(patron) > -1 && file.getName().indexOf(formato) > -1 && fecha.compareTo(new Date(file.lastModified())) < 0)
			{
				choise = file;
				lastMod = file.lastModified();
			}
		}

		if(choise != null)
			rutaArchivo = choise.getAbsolutePath();

		return rutaArchivo;
	}
	
	public static String buscarArchivoEnRuta (String dirDownloads, String patron, Date fecha) throws Exception {
		
		String rutaArchivo = null;
		
		File fl = new File(dirDownloads);

		// Todos los archivos de la carpeta
		File[] files = fl.listFiles(new FileFilter()
		{
			@Override
			public boolean accept(File file)
			{
				return file.isFile();
			}
		});
		
		if (files!=null) {

			// Obtenemos el �ltimo modificado
			long lastMod = Long.MIN_VALUE;
			File choise = null;
			for(File file : files)
			{
				
				if(file.lastModified() > lastMod && file.getName().toLowerCase().indexOf(patron.toLowerCase()) > -1 && fecha.compareTo(new Date(file.lastModified())) < 0)
				{
					choise = file;
					lastMod = file.lastModified();
				}
			}
	
			if(choise != null)
				rutaArchivo = choise.getAbsolutePath();
		}
		
		return rutaArchivo;
	}


	// Devuelve el nombre del fichero que nos acabamos de descargar pero ademas con el formato pasado, ya que puede ser a veces xls / pdf
//	public static String devuelveNombreFicheroDescargadoFormato(Date fecha, String patron, String formato) throws Exception
//	{
//		
//		String rutaArchivo = null;
//		
//		String dirDownloads = new File("downloads").getAbsolutePath();
//
//		Thread.sleep(25000);
//		
//		rutaArchivo = buscarArchivoEnRuta(dirDownloads, patron, fecha, formato);
//		
//		if(rutaArchivo==null)
//			Log.write("TimeOut esperando a que se descargue el fichero");
//
//
//		return rutaArchivo;
//	}
	
	public static boolean compruebaFicheroDescargadoContieneTexto (WebDriver driver, String patron, String textoABuscar) throws InterruptedException {
		
		boolean encontrado = false;
		
		try {
			
			Thread.sleep(10000);
			
			((JavascriptExecutor)driver).executeScript("window.open()");
			Utils.switchToTab(driver, 1);
			driver.get("chrome://downloads/");

			JavascriptExecutor js = (JavascriptExecutor)driver;
			String script = "return downloads.Manager.get().items_.filter(e => e.state === 'COMPLETE')  .map(e => e.filePath || e.file_path);";
			ArrayList archiveID =  (ArrayList) js.executeScript(script);
			
			int contador = 0;
			while (archiveID.size()==0 && contador < 20){
				Thread.sleep(1000);
				archiveID =  (ArrayList) js.executeScript(script);
				contador++;
			}
			
			String path = (String) archiveID.get(0);
			
			if (!path.contains(patron))
				return false;
			
			WebElement element = (WebElement)js.executeScript("var input = window.document.createElement('INPUT'); "+
					"input.setAttribute('type', 'file'); "+
					"input.setAttribute('id', 'paco'); "+
					"input.onchange = function (e) { e.stopPropagation() }; "+
					"return window.document.documentElement.appendChild(input); ");
			
			element.sendKeys(path);
			
			js.executeScript(""
					+ "function sleep(ms) {"+
					" return new Promise(resolve => setTimeout(resolve, ms));"+
					"}"+
					"async function final(){ var input=window.document.getElementById('paco');"+
					"   var reader = new FileReader();  "+
					"  reader.readAsDataURL(input.files[0]);"+
					"   await sleep(2000);console.error (reader.result); }"+
					"final();");
			
			Thread.sleep(7000);
			
			 String base64 = "";
			 for (LogEntry entry : driver.manage().logs().get(LogType.BROWSER)) {
			        String msg = entry.getMessage();
			        if (msg.contains("data:"))
			        	base64 = msg;
			    }
			 
			 int posicion = base64.indexOf("base64,");
			 base64 = base64.substring(posicion+7);
			 
			 byte[] byteArray = Base64.decodeBase64(base64.getBytes());
			 
			 File ficheroNuevo = new File(path);
			 
			 try (OutputStream stream = new FileOutputStream("downloads/"+ficheroNuevo.getName())) {
				    stream.write(byteArray);
			 }

			 
			 encontrado = Utils.checkPDFcontainsText("downloads/"+ficheroNuevo.getName(), textoABuscar);
			
			 driver.close();
			
			 Utils.switchToTab(driver, 0);
			 
			
		} catch (Exception e) {
			
		}

		return encontrado;
		
		
	}
	
	


	// Devuelve si est� visible o no un elemento. Si no est�, no provoca excepci�n.
	public static boolean isDisplayed(WebDriver driver, String xpath, int tiempo) throws Exception
	{

		Thread.sleep(500);
		driver.manage().timeouts().implicitlyWait(tiempo, TimeUnit.SECONDS);
		
		boolean resul = false;

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					resul = true;
			}
			
			
			if (!resul)
				for(int i = 0; i < listaElementos.size() && !resul; ++i)
				{
					if(Utils.checkBefore(driver, listaElementos.get(i)))
						resul = true;
				}
			

		}
		catch(Exception e)
		{

		}
		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
		return resul;
		
//		 WebDriverWait wait = new WebDriverWait(driver, tiempo);
//		    try{
//		        return wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath))) != null;
//
//		    }catch (Exception te){
//		        return false;
//		    }
		
	}
	
	public static boolean isDisplayedSinBefore(WebDriver driver, String xpath, int tiempo) throws Exception
	{

		driver.manage().timeouts().implicitlyWait(tiempo, TimeUnit.SECONDS);

		boolean resul = false;

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					resul = true;
			}
			

		}
		catch(Exception e)
		{

		}
		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
		
		return resul;
		
		
	}
	
	public static boolean isEnabled(WebDriver driver, String xpath, int tiempo) throws Exception
	{

		driver.manage().timeouts().implicitlyWait(tiempo, TimeUnit.SECONDS);

		boolean resul = false;

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isEnabled())
					resul = true;
			}

		}
		catch(Exception e)
		{

		}
		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
		return resul;
	}


	public static boolean isDisplayedElement(WebDriver driver, WebElement element, int tiempo) throws Exception
	{

		driver.manage().timeouts().implicitlyWait(tiempo, TimeUnit.SECONDS);

		boolean resul = false;

		try
		{

			if(element.isDisplayed())
				resul = true;


		}
		catch(Exception e)
		{

		}
		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
		return resul;
	}


	public static void clickEnElVisible(WebDriver driver, String xpath) throws Exception
	{
		
		Utils.esperaHastaApareceSinSalir(driver, xpath, 10);
		Thread.sleep(1000);
		
		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					elemento = listaElementos.get(i);
			}
			
			if (elemento==null){ 
				for(int i = 0; i < listaElementos.size(); ++i)
					{
						if(checkBefore(driver,listaElementos.get(i))){
							elemento = listaElementos.get(i);
							break;
						}
					}
			}
			
			if (elemento!=null)
				clickJS(driver, elemento);
			else
				throw new Exception("ERROR. No se encontr� el elemento: " + devuelveNombreConstante(xpath));
			
			Utils.esperarProcesandoPeticionDefaultContentVolviendoAPPal(driver);
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + devuelveNombreConstante(xpath));
		}

	}


	public static void clickEnElVisiblePrimero(WebDriver driver, String xpath) throws Exception
	{

		Utils.esperaHastaApareceSinSalir(driver, xpath, 20);
		Thread.sleep(500);
		
		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			boolean encontrado = false;

			for(int i = 0; i < listaElementos.size() && !encontrado; ++i)
			{
				if(listaElementos.get(i).isDisplayed())
				{
					elemento = listaElementos.get(i);
					encontrado = true;
				}
			}
			
			if (elemento==null){ 
				for(int i = 0; i < listaElementos.size(); ++i)
					{
						if(checkBefore(driver,listaElementos.get(i))){
							elemento = listaElementos.get(i);
							break;
						}
					}
			}

			if (elemento!=null)
				clickJS(driver, elemento);
			else
				throw new Exception("ERROR. No se encontr� el elemento: " + devuelveNombreConstante(xpath));
			
			Utils.esperarProcesandoPeticionDefaultContentVolviendoAPPal(driver);
		}
		catch(WebDriverException e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + devuelveNombreConstante(xpath));
		}

	}
	
	public static void clickSobreBotonReturn(WebDriver driver, String xpath) throws Exception
	{

		Utils.esperaHastaApareceSinSalir(driver, xpath, 20);
		
		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			boolean encontrado = false;

			for(int i = 0; i < listaElementos.size() && !encontrado; ++i)
			{
				if(listaElementos.get(i).isDisplayed())
				{
					elemento = listaElementos.get(i);
					encontrado = true;
				}
			}

			elemento.sendKeys(Keys.RETURN);
			Utils.esperarProcesandoPeticionDefaultContentVolviendoAPPal(driver);
		}
		catch(WebDriverException e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}

	}
	


	public static int numeroElementosVisibles(WebDriver driver, String xpath) throws Exception
	{

		int total = 0;

		Utils.setTimeOut(driver, 2);

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					total++;
			}
			
			if (total==0){
				for(int i = 0; i < listaElementos.size(); ++i)
				{
					if(Utils.checkBefore(driver, listaElementos.get(i)))
						total++;
				}
			
			}
			

		}
		catch(Exception e)
		{

		}

		Utils.setDefaultTimeOut(driver);
		return total;

	}


	public static void clickEnElVisibleEscenia(WebDriver driver, String xpath) throws Exception
	{

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					elemento = listaElementos.get(i);
			}

			elemento.click();
			if(Utils.isAlertPresent(driver))
				driver.switchTo().alert().accept();
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}

	}


	public static void clickEnElVisibleSinEspera(WebDriver driver, String xpath) throws Exception
	{

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					elemento = listaElementos.get(i);
			}

			elemento.click();
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}

	}


	public static void clickAndWait(WebDriver driver, String xpath) throws Exception
	{
		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					elemento = listaElementos.get(i);
			}

			elemento.click();
			Utils.esperarProcesandoPeticionDefaultContentVolviendoAPPal(driver);
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}

	}


	public static void introducirTextoEnElVisible(WebDriver driver, String xpath, String texto) throws Exception
	{
		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					elemento = listaElementos.get(i);
			}

			elemento.sendKeys(texto);
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}
	}


	public static void introducirTextoEnElVisible2(WebDriver driver, String xpath, String texto) throws Exception
	{
		
		Utils.esperaHastaApareceSinSalir(driver, xpath, 12);
		
		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;
			
			boolean encontrado = false;

			for(int i = 0; i < listaElementos.size() && !encontrado; ++i)
			{
				if(listaElementos.get(i).isDisplayed()){
					encontrado = true;
					elemento = listaElementos.get(i);
				}
			}
			elemento.clear();
			elemento.sendKeys(texto);
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}
	}


	public static void introducirTextoEnElVisibleKeys(WebDriver driver, String xpath, String texto) throws Exception
	{
		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					elemento = listaElementos.get(i);
			}

			for(int j = 0; j < texto.length(); j++)
			{
				elemento.sendKeys(String.valueOf(texto.charAt(j)));
			}

		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}
	}


	public static void clickAleatorioEnUnoDeLosVisibles(WebDriver driver, String xpath) throws Exception
	{
		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			List<WebElement> listaElementosVisibles = new ArrayList<WebElement>();

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
				{
					listaElementosVisibles.add(listaElementos.get(i));
				}
			}
			Random random = new Random();
			int randomNum = random.nextInt(listaElementosVisibles.size());

			Utils.clickJS(driver, listaElementosVisibles.get(randomNum));
			//listaElementosVisibles.get(randomNum).click();
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}
	}


	public static WebElement devuelveElementoVisible(WebDriver driver, String xpath) throws Exception
	{

		WebElement visible = null;

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
				{
					elemento = listaElementos.get(i);
					break;
				}
			}

			visible = elemento;

		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}

		if(visible == null)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}
		else
			return visible;
	}
	
	public static WebElement devuelveElementoVisibleYEnabled(WebDriver driver, String xpath) throws Exception
	{

		WebElement visible = null;

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed() && listaElementos.get(i).isEnabled())
				{
					elemento = listaElementos.get(i);
					break;
				}
			}

			visible = elemento;

		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}

		if(visible == null)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}
		else
			return visible;
	}


	// N�mero de filas de una tabla HTML
	public static int numeroFilasTabla(WebDriver driver, String xpathTabla) throws Exception
	{
		try
		{
			WebElement table = driver.findElement(By.xpath(xpathTabla));
			List<WebElement> allRows = table.findElements(By.tagName("tr"));
			return allRows.size();
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� la tabla: " + xpathTabla);
		}
	}


	public static int numeroFilasVisiblesTabla(WebDriver driver, String xpathTabla) throws Exception
	{
		try
		{

			WebElement table = driver.findElement(By.xpath(xpathTabla));
			List<WebElement> allRows = table.findElements(By.tagName("tr"));

			int filas = 0;

			for(int i = 0; i < allRows.size(); i++)
			{
				WebElement row = allRows.get(i);
				if(row.isDisplayed())
					filas++;
			}

			return filas;
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� la tabla: " + xpathTabla);
		}
	}


	public static int numeroFilasTablaDeUnaClase(WebDriver driver, String xpathTabla, String className) throws Exception
	{
		try
		{
			WebElement table = driver.findElement(By.xpath(xpathTabla));
			List<WebElement> allRows = table.findElements(By.xpath("//*[@class='" + className + "']"));
			return allRows.size();
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� la tabla: " + xpathTabla);
		}
	}


	public static int numeroFilasTabla(WebDriver driver, WebElement tabla) throws Exception
	{
		try
		{
			WebElement table = tabla;
			List<WebElement> allRows = table.findElements(By.tagName("tr"));
			return allRows.size();
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� la tabla: " + tabla.getTagName());
		}
	}


	// Devuelve la fila en la que est� el valor, si no -1. Empieza en 0
	public static int devuelveFilaContieneValor(WebDriver driver, String xpathTabla, String valor) throws Exception
	{
		try
		{
			WebElement table = driver.findElement(By.xpath(xpathTabla));
			List<WebElement> allRows = table.findElements(By.tagName("tr"));

			int encontrado = -1;

			for(int i = 0; i < allRows.size(); i++)
			{
				WebElement row = allRows.get(i);
				List<WebElement> cells = row.findElements(By.tagName("td"));

				for(WebElement cell : cells)
				{
					if(cell.getText().indexOf(valor) > -1)
					{
						encontrado = i;
						break;
					}
				}
			}

			return encontrado;
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� la tabla: " + xpathTabla);
		}
	}


	public static int devuelveFilaESValor(WebDriver driver, String xpathTabla, String valor) throws Exception
	{
		try
		{
			WebElement table = driver.findElement(By.xpath(xpathTabla));
			List<WebElement> allRows = table.findElements(By.tagName("tr"));

			int encontrado = -1;

			for(int i = 0; i < allRows.size(); i++)
			{
				WebElement row = allRows.get(i);
				List<WebElement> cells = row.findElements(By.tagName("td"));

				for(WebElement cell : cells)
				{
					if(cell.getText().equalsIgnoreCase(valor))
					{
						encontrado = i;
						break;
					}
				}
			}

			return encontrado;
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� la tabla: " + xpathTabla);
		}
	}


	public static int devuelveFilaContieneValor(WebDriver driver, WebElement tabla, String valor) throws Exception
	{
		try
		{
			WebElement table = tabla;
			List<WebElement> allRows = table.findElements(By.tagName("tr"));

			int encontrado = -1;

			for(int i = 0; i < allRows.size(); i++)
			{
				WebElement row = allRows.get(i);
				List<WebElement> cells = row.findElements(By.tagName("td"));

				for(WebElement cell : cells)
				{
					if(cell.getText().indexOf(valor) > -1)
					{
						encontrado = i;
						break;
					}
				}
			}

			return encontrado;
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� la tabla: " + tabla.getTagName());
		}
	}


	// Devuel el n�mero de ocurrencias que un string contiene a otro
	public static int cantidadOcurrencias(String cadena, String patron)
	{
		int cant = 0;
		while(cadena.indexOf(patron) > -1)
		{
			cadena = cadena.substring(cadena.indexOf(patron) + patron.length(), cadena.length());
			cant++;
		}
		return cant;
	}


	// Obtener la fecha de hoy
	public static String GetTodayDate() throws Exception
	{

		GregorianCalendar calendar = new GregorianCalendar();

		// date now:
		Date now = calendar.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		String formattedDate = fmt.format(now);

		Date today = calendar.getTime();
		formattedDate = fmt.format(today);

		return formattedDate;

	}


	public static String GetFechaParaEscenia() throws Exception
	{

		GregorianCalendar calendar = new GregorianCalendar();

		// date now:
		Date now = calendar.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHH");
		String formattedDate = fmt.format(now);

		Date today = calendar.getTime();
		formattedDate = fmt.format(today);

		return formattedDate;

	}


	public static String GetTodayYMD() throws Exception
	{

		GregorianCalendar calendar = new GregorianCalendar();

		// date now:
		Date now = calendar.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
		String formattedDate = fmt.format(now);

		Date today = calendar.getTime();
		formattedDate = fmt.format(today);

		return formattedDate;

	}


	public static String GetTodayFormat(String format) throws Exception
	{

		GregorianCalendar calendar = new GregorianCalendar();

		// date now:
		Date now = calendar.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat(format);
		String formattedDate = fmt.format(now);

		Date today = calendar.getTime();
		formattedDate = fmt.format(today);

		return formattedDate;

	}
	
	public static String GetDateMasNumeroDiasFormat(String format, int numeroDias) throws Exception
	{

		// date now:
		Date now = new Date();

		Calendar c = Calendar.getInstance();
		c.setTime(now);
		c.add(Calendar.DATE, numeroDias);
		now = c.getTime();

		SimpleDateFormat fmt = new SimpleDateFormat(format);
		String formattedDate = fmt.format(now);

		return formattedDate;

	}


	public static String GetTomorrowDate() throws Exception
	{

		// date now:
		Date now = new Date();

		Calendar c = Calendar.getInstance();
		c.setTime(now);
		c.add(Calendar.DATE, 1);
		now = c.getTime();

		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		String formattedDate = fmt.format(now);

		return formattedDate;

	}


	public static String GetDateMasNumeroDias(int numeroDias) throws Exception
	{

		// date now:
		Date now = new Date();

		Calendar c = Calendar.getInstance();
		c.setTime(now);
		c.add(Calendar.DATE, numeroDias);
		now = c.getTime();

		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		String formattedDate = fmt.format(now);

		return formattedDate;

	}


	public static String GetDateMasSegundos(int numeroSegundos) throws Exception
	{

		// date now:
		Date now = new Date();

		Calendar c = Calendar.getInstance();
		c.setTime(now);
		c.add(Calendar.SECOND, numeroSegundos);
		now = c.getTime();

		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String formattedDate = fmt.format(now);

		return formattedDate;

	}


	public static String GetTodayDateAndTime() throws Exception
	{

		GregorianCalendar calendar = new GregorianCalendar();

		// date now:
		Date now = calendar.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String formattedDate = fmt.format(now);

		Date today = calendar.getTime();
		formattedDate = fmt.format(today);

		return formattedDate;

	}


	public static String GetTodayDateAndTimeSFTP() throws Exception
	{

		GregorianCalendar calendar = new GregorianCalendar();

		// date now:
		Date now = calendar.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("ddMMyyyy_HHmmss");
		String formattedDate = fmt.format(now);

		Date today = calendar.getTime();
		formattedDate = fmt.format(today);

		return formattedDate;

	}


	public static String getTimeStamp() throws Exception
	{

		GregorianCalendar calendar = new GregorianCalendar();

		// date now:
		Date now = calendar.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("HHmmss");
		String formattedDate = fmt.format(now);

		Date today = calendar.getTime();
		formattedDate = fmt.format(today);

		return formattedDate;

	}


	// m�todo para realizar las capturas intermedias en funci�n del par�metro del properties
	public static void capturaIntermedia(WebDriver driver, Resultados resultado, String comentario)
	{
		params.cargarPropiedades();
		String capturasIntermedias = params.getValorPropiedad("evidencias");

		if(capturasIntermedias.equalsIgnoreCase("SI"))
		{
			String captura = takeScreenShot(driver, resultado.getNombreCP(), "html");
			Log.write("Evidencia: " + comentario + ". Puede ver la captura en: " + captura);
		}

	}
	
	// m�todo para realizar las capturas intermedias en funci�n del par�metro del properties
	public static void capturaBasica(WebDriver driver, String fileName, String comentario)
	{
			String captura = takeScreenShot(driver, fileName, "html");
			Log.write("Evidencia: " + comentario + ". Puede ver la captura en: " + captura);
	}


	// m�todo para obtener las opciones de men� configuradas para el entorno en el que se est� ejecutando y la referencia por defecto
	public static String obtenerOpcionesMenuDefecto() throws Exception
	{
		String opcionesMenu = "";
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosReferenciaFijos");
		String ventorno = params.getValorPropiedad("entorno");
		String vreferencia = params.getValorPropiedad("referenciaPorDefecto");
		// Buscamos las opciones de men� guardadas para la referencia por defecto y el entorno en el que se ejecuta la prueba
		CsvReader reader = new CsvReader(new FileReader(rutaCSV));
		while(reader.readRecord())
		{
			String entorno = reader.get(0);
			String referencia = reader.get(1);
			if((entorno.equalsIgnoreCase(ventorno)) && (referencia.equalsIgnoreCase(vreferencia)))
			{
				opcionesMenu = reader.get(7);
				break;
			}
		}
		if(opcionesMenu.equals(""))
			Log.write("No se han encontrado opciones de men� en el fichero " + rutaCSV + ", no se va a poder realizar la prueba");
		reader.close();
		return opcionesMenu;
	}


	public static String devuelveBloqueServicioGenerico(String tipoServicio) throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosReferenciaFijos");
		String vEntorno = params.getValorPropiedad("entorno");
		String vReferencia = params.getValorPropiedad("referenciaPorDefecto");

		String BloqueServicio = "";

		// Buscamos las opciones de men� guardadas para la referencia por defecto y el entorno en el que se ejecuta la prueba
		CsvReader reader = new CsvReader(new FileReader(rutaCSV));
		while(reader.readRecord())
		{
			String entorno = reader.get(0);
			String referencia = reader.get(1);
			if((entorno.equalsIgnoreCase(vEntorno)) && (referencia.equalsIgnoreCase(vReferencia)))
			{
				if(tipoServicio.equalsIgnoreCase("BloqueServicioSinFirma"))
				{
					BloqueServicio = reader.get(2);
					break;
				}
				else if(tipoServicio.equalsIgnoreCase("ServicioSinFirma"))
				{
					BloqueServicio = reader.get(3);
					break;
				}
				else if(tipoServicio.equalsIgnoreCase("BloqueServicioConFirma"))
				{
					BloqueServicio = reader.get(4);
					break;
				}
				else if(tipoServicio.equalsIgnoreCase("ServicioConFirma"))
				{
					BloqueServicio = reader.get(5);
					break;
				}
				else if(tipoServicio.equalsIgnoreCase("BloqueNServiciosConFirma"))
				{
					BloqueServicio = reader.get(6);
					break;
				}
				else if(tipoServicio.equalsIgnoreCase("BloqueOrdenanteBeneficiario"))
				{
					BloqueServicio = reader.get(8);
					break;
				}
				else if(tipoServicio.equalsIgnoreCase("ServicioOrdenanteBeneficiario"))
				{
					BloqueServicio = reader.get(9);
					break;
				}
				
				else if(tipoServicio.equalsIgnoreCase("BloqueDatosFijos"))
				{
					BloqueServicio = reader.get(10);
					break;
				}
				else if(tipoServicio.equalsIgnoreCase("ServicioDatosFijos"))
				{
					BloqueServicio = reader.get(11);
					break;
				}
				
				
			}
		}
//		if(BloqueServicio.equals(""))
//			Log.write("[INFO: No se ha encontrado " + tipoServicio + " en el fichero " + rutaCSV + ", no se va a poder realizar la prueba que aplique para este servicio]");
		reader.close();

		return BloqueServicio;
	}


	public static void CreaFicheroLanzamientoSimultaneo() throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		int numeroEjecucionesSimultaneas = Integer.parseInt(params.getValorPropiedad("numeroEjecucionesSimultaneas"));

		File statText = new File("resources/LanzamientoSimultaneo.bat");
		FileOutputStream is = new FileOutputStream(statText);
		OutputStreamWriter osw = new OutputStreamWriter(is);
		Writer w = new BufferedWriter(osw);

		for(int i = 1; i <= numeroEjecucionesSimultaneas; i++)
		{
			w.write("START java -jar Main.jar " + i + ">results\testMain" + i + ".txt 2>&1\n");
		}

		w.close();
	}


	public static void crearBloqueo() throws Exception
	{

		String lockFile = "resources/file.lock";
		int contador = 0;

		boolean existeBloqueo = new File(lockFile).exists();

		// Esperamos si alg�n caso lo tiene bloqueado
		while(existeBloqueo && contador <= 60)
		{
			Thread.sleep(1000);
			contador++;
			existeBloqueo = new File(lockFile).exists();
		}

		// Creamos el bloqueo
		File archivoBloqueo = new File(lockFile);
		archivoBloqueo.createNewFile();
	}
	
	public static void crearBloqueo(String nombreFichero, int timeOut) throws Exception
	{

		String lockFile = "resources/"+nombreFichero+".lock";
		int contador = 0;

		boolean existeBloqueo = new File(lockFile).exists();

		// Esperamos si alg�n caso lo tiene bloqueado
		while(existeBloqueo && contador <= timeOut)
		{
			Thread.sleep(1000);
			contador++;
			existeBloqueo = new File(lockFile).exists();
		}

		// Creamos el bloqueo
		File archivoBloqueo = new File(lockFile);
		archivoBloqueo.createNewFile();
	}
	
	public static void crearBloqueoToken(String id, int timeOut) throws Exception
	{
		
//		String id = Long.toString(Thread.currentThread().getId());
		
		String nombreFichero = "token";

		String idSaltoLinea = id+"\n";

		String lockFile = "resources/"+nombreFichero+".lock";
		int contador = 0;
		
		//Si no existe solo se crea
		File archivoBloqueo = new File(lockFile);
		
		if (!archivoBloqueo.exists())
			archivoBloqueo.createNewFile();
		
		//Solo a�adir si no esta
//		if (!searchString(lockFile, id))
		Files.write(Paths.get(lockFile), idSaltoLinea.getBytes(), StandardOpenOption.APPEND);
		
		boolean meTocaSalir = estaMiIdElPrimeroEnElFicheroONoEsta(id);

		// Esperamos si alg�n caso lo tiene bloqueado
		while(!meTocaSalir && contador <= timeOut)
		{
			Thread.sleep(1000);
			contador++;
			meTocaSalir = estaMiIdElPrimeroEnElFicheroONoEsta(id);
		}
		
		if(!estaMiIdElPrimeroEnElFicheroONoEsta(id) && contador>=timeOut ){
			//Quizas haya que hacer algo?
		}


		
	}
	
	public static int searchString(String fileName, String textoABuscar) throws IOException{
			
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		
		String currentLine;
		int lineaEncontrado = -1;
		int lineaActual = 0;
		
		while((currentLine = reader.readLine()) != null && lineaEncontrado==-1) {
		    
		    String trimmedLine = currentLine.trim();
		    if(trimmedLine.equals(textoABuscar)) 
		    	lineaEncontrado = lineaActual;
		    else lineaActual++;
		    
		}
		
		reader.close();
		System.out.println();
		
		return lineaEncontrado;
	}
	
	public static boolean estaMiIdElPrimeroEnElFicheroONoEsta(String id) throws Exception {
		
		String nombreFichero = "token";
		String lockFile = "resources/"+nombreFichero+".lock";
		boolean encontrado = false;
		int lineaEncontrado = -1;
		boolean resultado = false;
		
		File inputFile = new File(lockFile);
		BufferedReader reader = new BufferedReader(new FileReader(lockFile));
		
		if (inputFile.exists()) {
			
			String currentLine;
			int line = 0;
			
			while((currentLine = reader.readLine()) != null) {
			    
			    String trimmedLine = currentLine.trim();
			    
			    if(trimmedLine.equals(id)){ 
			    	encontrado = true;
			    	lineaEncontrado = line;
			    }
			    
			    line++;
			}
			
			
			resultado = encontrado && lineaEncontrado==0;
		
//			BufferedReader reader = new BufferedReader(new FileReader(inputFile));
//			
//			String textoLeido = reader.readLine();
//		
//			encontrado = textoLeido.trim().equals(id);
//	
//			reader.close(); 
			
		}
		
		reader.close();
			
		return resultado;
		
	}
	
	public static void liberarBloqueoToken(String id) throws Exception
	{
//		String id = Long.toString(Thread.currentThread().getId());
		
		String nombreFichero = "token";
		String lockFile = "resources/"+nombreFichero+".lock";	

		
		File inputFile = new File(lockFile);

		BufferedReader reader = new BufferedReader(new FileReader(inputFile));
		
		int numeroLinea=searchString(lockFile, id);

		removeNthLine(lockFile, numeroLinea);
		
//		removeFirstLine(lockFile, id);

		reader.close();
				
	}


	public static void crearFicheroRunning(String numero) throws Exception
	{
		try
		{
			String lockFile = "resources/Thread" + numero + ".running";
			File archivoBloqueo = new File(lockFile);
			archivoBloqueo.createNewFile();
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
		}

	}


	public static void crearFicheroTemporal(String ruta, String texto) throws Exception
	{
		try
		{
			
			File temp = new File(ruta);
			
			if (temp.exists())
				temp.delete();
			
			FileWriter fileHtm = new FileWriter(temp.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fileHtm);
			bw.write(texto);
			bw.close();
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
		}

	}
	
	public static void borrarFicheroTemporal(String ruta) throws Exception
	{
		try
		{
			File archivoBloqueo = new File(ruta);
			if(archivoBloqueo.exists()){
				archivoBloqueo.delete();
				if (archivoBloqueo.exists()) Thread.sleep(5000);
					
			}
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
		}
	}


	public static void borrarFicheroRunning(String numero) throws Exception
	{
		try
		{
			File archivoBloqueo = new File("resources/Thread" + numero + ".running");
			if(archivoBloqueo.exists()){
				archivoBloqueo.delete();
				if (archivoBloqueo.exists()) Thread.sleep(5000);
					
			}
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
		}
	}


	public static void liberarBloqueo() throws Exception
	{
		File archivoBloqueo = new File("resources/file.lock");
		archivoBloqueo.delete();
	}
	
	public static void liberarBloqueo(String nombreFichero) throws Exception
	{
		File archivoBloqueo = new File("resources/"+nombreFichero+".lock");
		archivoBloqueo.delete();
	}
	



	public static double roundTwoDecimals(double d)
	{
		DecimalFormat twoDForm = new DecimalFormat("#.#");
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		twoDForm.setDecimalFormatSymbols(dfs);
		return Double.valueOf(twoDForm.format(d));
	}


	public static String estadoCaso(String testID)
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String archivoResultados = params.getValorPropiedad("rutaResultados");

		String estadoCaso;

		try
		{
			CsvReader reader = new CsvReader(new FileReader(archivoResultados));

			estadoCaso = "";

			while(reader.readRecord())
			{
				String id = reader.get(0);
				if(id.equalsIgnoreCase(testID))
				{
					estadoCaso = reader.get(2);
					break;
				}

			}
		}
		catch(Exception e)
		{
			estadoCaso = "";
		}

		return estadoCaso;

	}
	
	public static String nombreCaso(String testID)
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String pathsLanzamiento[] = Utils.getPathsLanzamiento();
		String nombreCaso = "";

		for(String path : pathsLanzamiento)
		{
			
			try
			{
				CsvReader reader = new CsvReader(new FileReader(path));

				nombreCaso = "";

				while(reader.readRecord())
				{
					String id = reader.get(0);
					if(id.equalsIgnoreCase(testID))
					{
						nombreCaso = reader.get(1);
						return nombreCaso;
					}

				}

			}
			catch(Exception e)
			{
				nombreCaso = "";
			}
		}


		return nombreCaso;

	}


	public static String ComentarioCaso(String testID)
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String archivoResultados = params.getValorPropiedad("rutaResultados");

		String comentarioCaso;

		try
		{
			CsvReader reader = new CsvReader(new FileReader(archivoResultados));

			comentarioCaso = "";

			while(reader.readRecord())
			{
				String id = reader.get(0);
				if(id.equalsIgnoreCase(testID))
				{
					comentarioCaso = reader.get(3);
					break;
				}

			}
		}
		catch(Exception e)
		{
			comentarioCaso = "";
		}

		return comentarioCaso;

	}


	public static String devuelveCasoDelQueDepende(String testID)
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String pathsLanzamiento[] = Utils.getPathsLanzamiento();
		String nombreCaso = "";

		for(String path : pathsLanzamiento)
		{
			String codigoPadre = devuelveCodigoCasoDelQueDepende(testID);


			try
			{
				CsvReader reader = new CsvReader(new FileReader(path));

				nombreCaso = "";

				while(reader.readRecord())
				{
					String id = reader.get(0);
					if(id.equalsIgnoreCase(codigoPadre))
					{
						nombreCaso = reader.get(1);
						return nombreCaso;
					}

				}

			}
			catch(Exception e)
			{
				nombreCaso = "";
			}
		}


		return nombreCaso;

	}


	public static String devuelveCodigoCasoDelQueDepende(String testID)
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String nombreCaso = "";
		String pathsLanzamiento[] = Utils.getPathsLanzamiento();

		for(String path : pathsLanzamiento)
		{
			try
			{
				CsvReader reader = new CsvReader(new FileReader(path));

				nombreCaso = "";

				while(reader.readRecord())
				{
					String id = reader.get(0);
					if(id.equalsIgnoreCase(testID))
					{
						nombreCaso = reader.get(3);
						return nombreCaso;
					}

				}
			}
			catch(Exception e)
			{
				nombreCaso = "";
			}

		}

		return nombreCaso;

	}


	public static String tipoPoderPorIdioma(String tipoPoder)
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String idioma = params.getValorPropiedad("idioma");

		String tipoPoderTraducido = tipoPoder;

		if(idioma.equals("EN"))
		{
			if(tipoPoder == "Sin poderes")
				tipoPoderTraducido = "Without powers";
			if(tipoPoder == "Solidario-Indistinto")
				tipoPoderTraducido = "Joint-Several";
			if(tipoPoder == "Mancomunado 2")
				tipoPoderTraducido = "Joint 2";
			if(tipoPoder == "Mancomunado 3")
				tipoPoderTraducido = "Joint 3";
			if(tipoPoder == "Mancomunado 4")
				tipoPoderTraducido = "Joint 4";
		}

		return tipoPoderTraducido;

	}


	public static String tipoFirmaPorIdioma(String tipoFirma)
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String idioma = params.getValorPropiedad("idioma");

		String tipoFirmaTraducida = tipoFirma;

		if(idioma.equals("EN"))
		{
			if(tipoFirma == "Auditor")
				tipoFirmaTraducida = "Auditor";
			if(tipoFirma == "Apoderado")
				tipoFirmaTraducida = "Account holder";
		}

		return tipoFirmaTraducida;

	}


	public static String paisPorIdioma(String pais)
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String idioma = params.getValorPropiedad("idioma");

		String paisTraducido = pais;

		if(idioma.equals("ES"))
			paisTraducido = "Espa�a";
		if(idioma.equals("EN"))
			paisTraducido = "Spain";

		return paisTraducido;

	}


	public static String navegacionGoogleDrive(WebDriver driver, String url) throws Exception
	{
		try
		{

			String clave = devuelveClaveCifrada();

			if(clave.equalsIgnoreCase(""))
				throw new Exception ("Usuario Pass BBVA requerido. Revise Properties");

			driver = new FirefoxDriver();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			if (!driver.toString().contains("ChromeDriver"))
				driver.manage().window().maximize();

			if(Utils.isDisplayed(driver, "//input[@id='Email']", 10))
			{
				driver.findElement(By.xpath("//input[@id='Email']")).sendKeys("antonio.gomez.yunta.contractor@bbva.com");
				driver.findElement(By.xpath("//input[@id='Passwd']")).sendKeys(clave);
				Utils.clickEnElVisible(driver, "//input[@id='signIn']");
				Thread.sleep(7000);

				if(Utils.isDisplayed(driver, "//input[@id='ksni_user']", 10))
				{
					driver.findElement(By.xpath("//input[@id='ksni_user']")).sendKeys("");
					driver.findElement(By.xpath("//input[@id='ksni_password']")).sendKeys(clave);
					Utils.clickEnElVisible(driver, "//input[contains(@onclick,'comprueba')]");
					Thread.sleep(8000);
				}

			}

			WebDriverWait wait = new WebDriverWait(driver, 100);
			Utils.esperaHastaEnabled(driver, Constantes.submitApproveAccess, 10);
			Utils.clickEnElVisible(driver, Constantes.submitApproveAccess);
			String codigoAplicacion = driver.findElement(By.xpath("//input[@id='code']")).getAttribute("value");
			Log.write(codigoAplicacion);
			driver.close();
			return codigoAplicacion;


		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			Utils.quit(driver);
			return "";
		}
	}


	// Hay que mirar posibles mejoras como que a veces pide el m�vil
	public static boolean accederGoogleDrive(WebDriver driver) throws Exception
	{

		boolean resul = false;
		String clave = devuelveClaveCifrada();

		if(clave.equalsIgnoreCase(""))
			throw new Exception ("Usuario Pass BBVA requerido. Revise Properties");

		String idExcelGoogleDrive;

		idExcelGoogleDrive = params.getValorPropiedad("idExcelGoogleDrive");
		String usuarioBBVA = params.getValorPropiedad("usuarioBBVA");
		String mailBBVA = params.getValorPropiedad("mailBBVA");
		String passwordBBVA = params.getValorPropiedad("passwordBBVA");


		if(idExcelGoogleDrive == null || idExcelGoogleDrive.equalsIgnoreCase(""))
			idExcelGoogleDrive = "1TCAKvtgt9ahuWo_EJagI-1J3ytSJcWaqzvN5irXKySs";

		if(usuarioBBVA == null || usuarioBBVA.equalsIgnoreCase(""))
			throw new Exception ("ERROR : Debe introducir el usuario BBVA en el properties");

		if(mailBBVA == null || mailBBVA.equalsIgnoreCase(""))
			mailBBVA = "";

		if(passwordBBVA == null || passwordBBVA.equalsIgnoreCase(""))
			passwordBBVA = clave;

		String url = "https://docs.google.com/a/bbva.com/spreadsheets/d/" + idExcelGoogleDrive + "/edit#gid=0";
		driver.get(url);
//		driver.get("https://drive.google.com/a/bbva.com/?tab=mo#my-drive");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		if (!driver.toString().contains("ChromeDriver"))
			driver.manage().window().maximize();

		if(existeElementoTiempo(driver, By.xpath("//input[@id='Email']"), 10))
		{
			driver.findElement(By.xpath("//input[@id='Email']")).sendKeys(mailBBVA);

			if(Utils.isDisplayed(driver, "//input[@id='next']", 3))
				Utils.clickEnElVisible(driver, "//input[@id='next']");

			if(Utils.isDisplayed(driver, "//input[@id='Passwd']", 3))
				driver.findElement(By.xpath("//input[@id='Passwd']")).sendKeys(clave);

			if(Utils.isDisplayed(driver, "//input[@id='signIn']", 3))
				Utils.clickEnElVisible(driver, "//input[@id='signIn']");

			Thread.sleep(7000);
		}

		if(existeElementoTiempo(driver, By.xpath("//input[@id='ksni_user']"), 10))
		{
			driver.findElement(By.xpath("//input[@id='ksni_user']")).sendKeys(usuarioBBVA);
			driver.findElement(By.xpath("//input[@id='ksni_password']")).sendKeys(clave);
			Utils.clickEnElVisible(driver, "//button[@type='submit']");
			Thread.sleep(8000);
		}

		if(!Utils.isDisplayed(driver, "//*[contains(text(),'Error de login')]", 5))
			resul = true;

		if(!Utils.isDisplayed(driver, "//div[contains(@id,'docs')]", 3))
			driver.get(url);

		return resul;

	}


	// Devuelve los datos de la ejecuci�n del HTML creado
	public static String devuelveDatosDeEjecucion(String datoBuscar)
	{

		String valor = "";

		try
		{
			File archivoHTML = new File("results/ResumenEjecucionPruebas.html");
			Document doc = Jsoup.parse(archivoHTML, null);

			if(datoBuscar.equals("TOTAL"))
				valor = doc.select("th:contains(" + datoBuscar + ")").first().nextElementSibling().childNode(0).toString();
			else
				valor = doc.select("td:contains(" + datoBuscar + ")").first().nextElementSibling().childNode(0).toString();
			return valor;
		}
		catch(IndexOutOfBoundsException e)
		{
			return "";
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			return "";
		}

	}
	
	public static String devuelveDatosDeEjecucionPasandoArchivo(String datoBuscar, String rutaArchivo)
	{

		String valor = "";

		try
		{
			File archivoHTML = new File(rutaArchivo);
			Document doc = Jsoup.parse(archivoHTML, null);

			if(datoBuscar.equals("TOTAL"))
				valor = doc.select("th:contains(" + datoBuscar + ")").first().nextElementSibling().childNode(0).toString();
			else
				valor = doc.select("td:contains(" + datoBuscar + ")").first().nextElementSibling().childNode(0).toString();
			return valor;
		}
		catch(IndexOutOfBoundsException e)
		{
			return "";
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			return "";
		}

	}


	public static String devuelveUUAAsEjecutadas() throws IOException
	{
		String pathsLanzamiento[] = Utils.getPathsLanzamiento();


		String resul = "";
		boolean KYGU = false;
		boolean KYNF = false;
		boolean KYOP = false;
		boolean KYTA = false;
		boolean KYFX = false;
		boolean KGGT = false;
		boolean EBCF = false;
		boolean KBDM = false;
		boolean BOSS = false;
		boolean KYHD = false;
		boolean GMM = false;

		String htmlKYGU = "<img alt=\"KYGU\" title=\"KYGU\" border=\"0\" src=\"http://146.148.15.138/testing/icono_usuario.png\">";
		String htmlKYNF = "<img alt=\"KYNF\" title=\"KYNF\" border=\"0\" src=\"http://146.148.15.138/testing/icono_alerta.png\">";
		String htmlKYOP = "<img alt=\"KYOP\" title=\"KYOP\" border=\"0\" src=\"http://146.148.15.138/testing/icono_portal.png\">";
		String htmlKYTA = "<img alt=\"Transferencias\" title=\"Transferencias\" border=\"0\" src=\"http://146.148.15.138/testing/icono_transferencias.png\">";
		String htmlKYTAAdeudos = "<img alt=\"Adeudos\" title=\"Adeudos\" border=\"0\" src=\"http://146.148.15.138/testing/icono_transferencias.png\">";
		String htmlKYFX = "<img alt=\"KYFX\" title=\"KYFX\" border=\"0\" src=\"http://146.148.15.138/testing/icono_gitim.png\">";
		String htmlKGGT = "<img alt=\"KGGT\" title=\"KGGT\" border=\"0\" src=\"http://146.148.15.138/testing/icono_gitim.png\">";
		String htmlEBCF = "<img alt=\"EBCF\" title=\"EBCF\" border=\"0\" src=\"http://146.148.15.138/testing/icono_gitim.png\">";
		String htmlKBDM = "<img alt=\"KBDM\" title=\"KBDM\" border=\"0\" src=\"http://146.148.15.138/testing/icono_gitim.png\">";
		String htmlBOSS = "<img alt=\"BOSS\" title=\"BOSS\" border=\"0\" src=\"http://146.148.15.138/testing/icono_gitim.png\">";
		String htmlKYHD = "<img alt=\"KYHD\" title=\"KYHD\" border=\"0\" src=\"http://146.148.15.138/testing/icono_usuario.png\">";
		String htmlGMM = "<img alt=\"GMM\" title=\"GMM\" border=\"0\" src=\"http://146.148.15.138/testing/icono_usuario.png\">";

		
		String tipoEjecucion = params.getValorPropiedad("tipoEjecucion");

		//Podr�a ni existir la propiedad
		if (tipoEjecucion==null) 
			tipoEjecucion = "";
		
		if (tipoEjecucion.equalsIgnoreCase("ANS")){
			pathsLanzamiento = Utils.devuelveArchivosDeUnDirectorioString("lanzamiento/", ".csv");
			
			for (int j = 0; j < pathsLanzamiento.length; j++) 
				pathsLanzamiento[j] = "lanzamiento/"+pathsLanzamiento[j];
			
			
		}
		
		
		
		for(String path : pathsLanzamiento)
		{

			CsvReader reader = new CsvReader(new FileReader(path));


			while(reader.readRecord())
			{

				String nombre = reader.get(1);
				String lanzar = reader.get(2);
				if((nombre.contains("administracion") || nombre.contains("perfilado")) && lanzar.equalsIgnoreCase("SI") && !KYGU)
				{
					resul = resul + htmlKYGU + " ";
					KYGU = true;
				}
				else if(nombre.contains("com.bbva.kynf") && lanzar.equalsIgnoreCase("SI") && !KYNF)
				{
					resul = resul + htmlKYNF + " ";
					KYNF = true;
				}
				else if(nombre.contains("portal") && lanzar.equalsIgnoreCase("SI") && !KYOP)
				{
					resul = resul + htmlKYOP + " ";
					KYOP = true;
				}
				else if(nombre.contains("com.bbva.kyta.transferencias") && lanzar.equalsIgnoreCase("SI") && !KYTA)
				{
					resul = resul + htmlKYTA + " ";
					KYTA = true;
				}
				else if(nombre.contains("com.bbva.kyta.adeudos") && lanzar.equalsIgnoreCase("SI") && !KYTA)
				{
					resul = resul + htmlKYTAAdeudos + " ";
					KYTA = true;
				}
				else if(nombre.contains("com.bbva.kyfx") && lanzar.equalsIgnoreCase("SI") && !KYFX)
				{
					resul = resul + htmlKYFX + " ";
					KYFX = true;
				}
				else if(nombre.contains("com.bbva.kggt") && lanzar.equalsIgnoreCase("SI") && !KGGT)
				{
					resul = resul + htmlKGGT + " ";
					KGGT = true;
				}
				else if(nombre.contains("com.bbva.ebcf") && lanzar.equalsIgnoreCase("SI") && !EBCF)
				{
					resul = resul + htmlEBCF + " ";
					EBCF = true;
				}
				else if(nombre.contains("com.bbva.kbdm") && lanzar.equalsIgnoreCase("SI") && !KBDM)
				{
					resul = resul + htmlKBDM + " ";
					KBDM = true;
				}
				else if((nombre.contains("com.bbva.boss") || (nombre.contains("com.bbva.oneView"))) && lanzar.equalsIgnoreCase("SI") && !BOSS)
				{
					resul = resul + htmlBOSS + " ";
					BOSS = true;
				}
				
				else if(nombre.contains("com.bbva.kyhd") && lanzar.equalsIgnoreCase("SI") && !KYHD)
				{
					resul = resul + htmlKYHD + " ";
					KYHD = true;
				}
				
				else if(nombre.contains("com.bbva.healthcheck") && lanzar.equalsIgnoreCase("SI") && !GMM)
				{
					resul = resul + htmlGMM + " ";
					GMM = true;
				}

			}

		}
		

		return resul;


	}
	
	public static String devuelveUUAAParaMail() throws IOException
	{
		params.cargarPropiedades();
		String tipoEjecucion = params.getValorPropiedad("tipoEjecucion");

		//Podr�a ni existir la propiedad
		if (tipoEjecucion==null) 
			tipoEjecucion = "";
		
		if (!tipoEjecucion.equalsIgnoreCase("ANS")){
			String asuntoMail = params.getValorPropiedad("asuntoMail");
			
			if (asuntoMail!=null) {
				tipoEjecucion=asuntoMail;
			}
			
		}
		
		
		return tipoEjecucion;

		
//		String pathsLanzamiento[] = Utils.getPathsLanzamiento();
//
//
//		
//		boolean KYGU = false;
//		boolean KYTA = false;
//
//		String htmlKYGU = "de Regresi�n";
//		String htmlKYTA = "Transferencias";
//		String htmlKYTAAdeudos = "Adeudos";
//
//
//		for(String path : pathsLanzamiento)
//		{
//
//			CsvReader reader = new CsvReader(new FileReader(path));
//
//
//			while(reader.readRecord())
//			{
//
//				String nombre = reader.get(1);
//				String lanzar = reader.get(2);
//				
//				if(nombre.contains("administracion") && lanzar.equalsIgnoreCase("SI") && !KYGU)
//				{
//					resul = resul + htmlKYGU + " ";
//					KYGU = true;
//				}
//
//				else if(nombre.contains("com.bbva.kyta.transferencias") && lanzar.equalsIgnoreCase("SI") && !KYTA)
//				{
//					resul = resul + htmlKYTA + " ";
//					KYTA = true;
//				}
//				else if(nombre.contains("com.bbva.kyta.adeudos") && lanzar.equalsIgnoreCase("SI") && !KYTA)
//				{
//					resul = resul + htmlKYTAAdeudos + " ";
//					KYTA = true;
//				}
//
//			}
//
//		}
//
//		return resul;


	}


	public static boolean estamosEnLatam()
	{
		params.cargarPropiedades();
		String pais = params.getValorPropiedad("pais");

		return Latam.contains(pais);

	}


	public static String devuelveEntorno()
	{
		params.cargarPropiedades();
		return params.getValorPropiedad("entorno");
	}


	public static boolean estamosEnCompass()
	{
		params.cargarPropiedades();
		String pais = params.getValorPropiedad("pais");
		
		boolean resultado = false;
		
		if (pais!=null)
			resultado = pais.equalsIgnoreCase("Compass");

		return resultado;

	}


	public static boolean estamosEnFirefox()
	{
		params.cargarPropiedades();
		String navegador = params.getValorPropiedad("browser");

		return navegador.equalsIgnoreCase("firefox");

	}


	public static void cambioFramePrincipalMultiNavegador(WebDriver driver)
	{
		if(!estamosEnFirefox())
			Utils.cambiarFramePrincipal(driver);
	}


	public static boolean isAlertPresent(WebDriver driver)
	{
		try
		{
			driver.switchTo().alert();
			return true;
		}
		catch(Exception Ex)
		{
			return false;
		}
	}


	public static void handleAlert(WebDriver driver)
	{
		if(Utils.isAlertPresent(driver))
			driver.switchTo().alert().accept();
	}


	public static void cambiarFramePrincipal(WebDriver driver)
	{
		WebElement frame = driver.findElement(By.xpath(Constantes.FramePrincipal));
		driver.switchTo().frame(frame);
	}


	public static void cambiarFrameDado(WebDriver driver, String xpathframe)
	{
		WebElement frame = driver.findElement(By.xpath(xpathframe));
		driver.switchTo().frame(frame);
	}


	public static void cambiarFrameDadoVisible(WebDriver driver, String xpathframe) throws Exception
	{

		try
		{
			List<WebElement> listaFrames = driver.findElements(By.xpath(xpathframe));
			WebElement frame = null;

			for(int i = 0; i < listaFrames.size(); ++i)
			{
				if(listaFrames.get(i).isDisplayed())
					frame = listaFrames.get(i);
			}

			driver.switchTo().frame(frame);
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el frame: " + xpathframe);
		}
	}
	
	public static void cambiarFrameDadoVisibleName(WebDriver driver, String name) throws Exception
	{

		try
		{
			List<WebElement> listaFrames = driver.findElements(By.name(name));
			WebElement frame = null;

			for(int i = 0; i < listaFrames.size(); ++i)
			{
				if(listaFrames.get(i).isDisplayed())
					frame = listaFrames.get(i);
			}

			driver.switchTo().frame(frame);
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el frame: " + name);
		}
	}


	public static void setBBVAProxy()
	{
		// NOTA: Subida manual usr:selenium pass:qwerty99

		// Establecemos el proxy
//		if (Utils.insideBBVA())
//		{
//			final String clave = devuelveClaveCifrada();
			
//			System.setProperty("http.proxyHost", "cacheedi1.igrupobbva");
//			System.setProperty("http.proxyPort", "8080");
//			System.setProperty("http.proxyUser", "xe39619");
//			System.setProperty("http.proxyPassword", clave);
			
//			System.setProperty("http.proxyHost", Constantes.proxy_hostname);
//			System.setProperty("http.proxyPort", String.valueOf(Constantes.proxy_port));
//			System.setProperty("http.proxyUser", Constantes.proxy_user);
//			System.setProperty("http.proxyPassword", clave);
			
			
			
//			System.getProperties().put("http.proxyHost", "cacheedi1.igrupobbva");
//			System.getProperties().put("http.proxyPort", "8080");
//			System.getProperties().put("https.proxyHost", "cacheedi1.igrupobbva");
//			System.getProperties().put("https.proxyPort", "8080");
//			
//			Authenticator.setDefault(new Authenticator() {
//			    public PasswordAuthentication getPasswordAuthentication() {
//			       return new PasswordAuthentication("xe39619", clave.toCharArray());
//			    }
//			} );
//			
//			System.setProperty("http.proxyUser", "xe39619"); 
//			System.setProperty("http.proxyPassword", clave);
//			System.setProperty("https.proxyUser", "xe39619"); 
//			System.setProperty("https.proxyPassword", clave);
		
//		}
		
	}


	public static String extraerDigitosDeString(String cadena)
	{
		return cadena.replaceAll("\\D+", "");
	}


	public static void esperarProcesandoPeticionMejoradoCMP(WebDriver driver, Resultados resultado) throws Exception
	{
		int contador = 0;
		boolean desaparece = false;
		while(contador < 50)
		{
			if(Utils.isDisplayed(driver, Constantes.ImagenCargando, 1))
			{
				contador++;
				Thread.sleep(1000);
			}
			else
			{
				desaparece = true;
				break;
			}
		}
		if(!desaparece)
			Utils.setResultadoERROR(resultado, "TimeOut de 50 segundos esperando a procesar la petici�n");
	}


	public static void cambiarFrameMenuV5(WebDriver driver) throws Exception
	{
		driver.switchTo().defaultContent();
		Thread.sleep(500);
		WebElement frame = driver.findElement(By.xpath(Constantes.FrameMenu));
		driver.switchTo().frame(frame);
	}


	public static int getResponseCode(String urlString) throws Exception
	{
		URL u = new URL(urlString);
		HttpURLConnection huc = (HttpURLConnection)u.openConnection();
		huc.setRequestMethod("POST");
		huc.connect();
		return huc.getResponseCode();
	}


	public static File BajarC204(String fileName) throws IOException, Exception
	{

		// Establecemos el proxy
		Utils.setBBVAProxy();

		File fichero = null;

		try
		{

			String URL = "http://146.148.15.138/testing/" + fileName;

			if(getResponseCode(URL) != 404)
			{
				URL website = new URL(URL);
				fichero = new File("downloads/" + fileName);
				org.apache.commons.io.FileUtils.copyURLToFile(website, fichero);
			}


		}
		catch(Exception e)
		{
			fichero = null;
			Log.write("No existe una versi�n del C204: " + fileName + " subida en el cloud. Se parte del b�sico.");
		}

		return fichero;

	}


	public static void cambiarALaUltimaVentana(WebDriver driver) throws Exception
	{
		String lastWindow = (String)driver.getWindowHandles().toArray()[driver.getWindowHandles().toArray().length - 1];
		driver.switchTo().window(lastWindow);
	}


	public static void crearCSV(String outputFile, String numeroEscenia, String password, String telefono, String referenciaCreada, String usuario) throws Exception
	{
		// vemos si el fichero ya existe
		boolean alreadyExists = new File(outputFile).exists();
		boolean resul = false;

		try
		{
			// usamos FileWriter indicando que es para append
			CsvWriter writer = new CsvWriter(new FileWriter(outputFile, true), ',');
			CsvReader reader = new CsvReader(new FileReader(outputFile));

			// Si el fichero no existe, lo creamos con las cabeceras oportunas
			if(!alreadyExists)
			{
				writer.write("Fecha");
				writer.write("NumeroEscenia");
				writer.write("Password");
				writer.write("Telefono");
				writer.write("ReferenciaCreada");
				writer.write("Usuario");
				writer.endRecord();
			}

			int i = 0;
			while(reader.readRecord())
			{
				i++;
				String id = reader.get(0);
				if(id.equalsIgnoreCase(Utils.GetTodayDate()))
				{
					resul = true;
					break;
				}

			}

			// Si existe ya la entrada la borra
			if(resul)
				Utils.removeNthLine(outputFile, i - 1);

			writer.write(Utils.GetTodayDate());
			writer.write(numeroEscenia);
			writer.write(password);
			writer.write(telefono);
			writer.write(referenciaCreada);
			writer.write(usuario); 
			writer.endRecord();
			writer.close();

		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
		}
	}


	public static String obtenerValorDeCSV(String outputFile, String id, int columna, boolean borrarFila) throws Exception
	{

		boolean resul = false;
		String valor = "";

		try
		{

			CsvReader reader = new CsvReader(new FileReader(outputFile));

			int i = 0;
			while(reader.readRecord())
			{
				i++;
				String clave = reader.get(0);

				if(clave.equalsIgnoreCase(id))
				{
					resul = true;
					valor = reader.get(columna);
					if(borrarFila)
						Utils.removeNthLine(outputFile, i - 1);
				}
			}

			if(!resul)
				throw new Exception("No se encontr� ning�n registro en el fichero: " + outputFile + " del d�a de hoy.");

			return valor;

		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			return "";
		}
	}


	public static String ejecutarJS(WebDriver driver, String comando) throws Exception
	{

		try
		{
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript(comando);
			return "OK";
		}
		catch(Exception e)
		{
			return e.toString();
		}

	}


	public static String devuelveClaveCifrada()
	{

		try
		{
			params.cargarPropiedades();
			String passCifrada = params.getValorPropiedad("passwordCifrada");

			if(passCifrada == null || passCifrada.equalsIgnoreCase(""))
				return "";

			AES.setKey("paco");
			final String strToDecrypt = passCifrada;
			AES.decrypt(strToDecrypt.trim());

			String claveCifrada = AES.getDecryptedString();

			if(claveCifrada == null)
				return "";
			else
				return claveCifrada;

		}
		catch(Exception e)
		{
			return "";
		}
	}
	
	public static String desencriptarConAES(String strToDecrypt)
	{

		try
		{

			AES.setKey("paco");
			
			String claveDescifrada = AES.desencriptar(strToDecrypt);

			if(claveDescifrada == null)
				return strToDecrypt;
			else
				return claveDescifrada;

		}
		catch(Exception e)
		{
			return strToDecrypt;
		}
	}


	public static void navegarLogin(WebDriver driver) throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		
		String pais = params.getValorPropiedad("pais").toLowerCase();
		String entorno = params.getValorPropiedad("entorno");
		String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
		String url = devuelveURLApartirDePaisYEntorno(rutaURLs, pais, entorno);

		Thread.sleep(2500);
		Log.write(url);
		driver.get(url);

	}


	public static int getDigitosFromString(String cadena)
	{
		String digitos = cadena.replaceAll("\\D+", "");

		int numero;

		try
		{
			numero = Integer.valueOf(digitos);
		}

		catch(Exception e)
		{
			numero = -1;
		}

		return numero;
	}
	
	public static String getOnlyCaracteres(String cadena)
	{
		String caracteres = cadena.replaceAll("[^A-Za-z]+", "");

		return caracteres;
	}
	
	public static String getDigitosPuntosComasFromString(String cadena)
	{
		String caracteres = cadena.replaceAll("[^0-9,.]+", "");

		return caracteres;
	}
	
	
	
	


	public static String getDigitosCadenaFromString(String cadena)
	{
		String digitos = cadena.replaceAll("\\D+", "");

		String numero;

		try
		{
			numero = digitos;
			// numero = Integer.valueOf(digitos);
		}

		catch(Exception e)
		{
			numero = "-1";
		}

		return numero;
	}


	public static List<String> getAllMatches(String text, String regex)
	{
		List<String> matches = new ArrayList<String>();
		Matcher m = Pattern.compile("(?=(" + regex + "))").matcher(text);
		while(m.find())
		{
			matches.add(m.group(1));
		}
		return matches;
	}


	public static List<String> getAllMatchesWithoutStartAndEnd(String text, String regex, String start, String end)
	{
		List<String> matches = new ArrayList<String>();
		Matcher m = Pattern.compile("(?=(" + regex + "))").matcher(text);
		while(m.find())
		{
			matches.add(m.group(1).replace(start, "").replace(end, ""));
		}
		return matches;
	}


	public static void loginContratacion(WebDriver driver, Resultados resultado, String referencia) throws Exception
	{

		// Cargamos las propiedades del properties
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaURLs = params.getValorPropiedad("rutaDatosURLs");

		String url = devuelveURLApartirDePaisYEntorno(rutaURLs, "ContratacionSinLogin", "Integrado");

		// Navegamos a la url
		driver.get(url);

		Utils.esperarVisibleTiempo(driver, Constantes.desconectarSST, 35);

		// Validamos t�tulo
		Utils.vpWarning(driver, resultado, "T�tulo Contrataci�n SST", true, Utils.isDisplayed(driver, Constantes.TituloContratacion, 5), "FALLO");

		// Seleccionamos el tipo de Acceso
		Utils.seleccionarComboContieneXPath(driver, Constantes.comboTipoAcceso, "Referencia externa");
		Thread.sleep(1500);

		// Introducimos Ref. Externa
		Utils.introducirTextoEnElVisible(driver, Constantes.referenciaExterna, referencia);
		Utils.clickEnElVisible(driver, Constantes.botonAccederSST);
	}


	public static void navegaLogin(WebDriver driver, Resultados resultado) throws Exception
	{

		params.cargarPropiedades();

		String pais = params.getValorPropiedad("pais").toLowerCase();
		String entorno = params.getValorPropiedad("entorno");
		String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
		String url = Utils.devuelveURLApartirDePaisYEntorno(rutaURLs, pais, entorno);

		driver.get(url);
		Thread.sleep(1500);
	}


	public static void clickDesplegableremedy(WebDriver driver, String texto) throws Exception
	{
		new Actions(driver).sendKeys(Keys.ARROW_DOWN).perform();
		Thread.sleep(1500);
		Utils.clickEnElVisibleEscenia(driver, "//tr[@class='MenuTableRow']/td[contains(@class,'MenuEntryNoSub')][@arvalue='" + texto
				+ "']/preceding-sibling::td[contains(@class,'MenuEntry')][contains(text(),'" + texto + "')]");

		// Utils.introducirTextoEnElVisible2(driver, Constantes.aplicacion, texto);


	}


	public static void uploadFileSFTP(String rutaCarpeta, String archivo, String user, String pass, String maquina)
	{

		JSch jsch = new JSch();
		Session session = null;
		String fileName = "";

		try
		{
			session = jsch.getSession(user, maquina, 22);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pass);

			session.connect();

			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp)channel;


			ArrayList<String> list = new ArrayList<String>();
			Vector<LsEntry> entries = sftpChannel.ls(rutaCarpeta);

			for(LsEntry entry : entries)
			{
				if(!entry.getAttrs().isDir())
				{
					list.add(entry.getFilename());
				}
			}

			for(int i = 0; i < list.size(); i++)
			{
				fileName = list.get(i);
				if(fileName.contains("txt"))
					sftpChannel.rm(rutaCarpeta + "/" + fileName);
			}

			// Subimos
			sftpChannel.put(archivo, rutaCarpeta);

			sftpChannel.exit();
			session.disconnect();
		}
		catch(Exception e)
		{
			session.disconnect();
			fail("Hubo un problema al intentar subir el fichero: "+fileName+ " por FTP: "+ e.toString());
		}
		


	}


	public static void ejecutaComandoUnix(String comando, String user, String pass, String host)
	{
		try
		{
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, host, 22);
			UserInfo ui = new SUserInfo(pass, null);
			session.setUserInfo(ui);
			session.setPassword(pass);
			session.connect();

			ChannelExec channelExec = (ChannelExec)session.openChannel("exec");

			InputStream in = channelExec.getInputStream();

			channelExec.setCommand(comando);
			channelExec.connect();

			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			String line;
			int index = 0;

			while((line = reader.readLine()) != null)
			{
				Log.write(++index + " : " + line);
			}

			int exitStatus = channelExec.getExitStatus();
			channelExec.disconnect();
			session.disconnect();

			if(exitStatus < 0)
			{
				Log.write("Done, but exit status not set!");
			}
			else if(exitStatus > 0)
			{
				Log.write("Done, but with error!");
			}
			else
			{
				Log.write("Done!");
			}
		}
		catch(Exception e)
		{
			System.err.println("Error: " + e);
		}
	}


	public static void uploadFileSFTPSinBorrar(String rutaCarpeta, String archivo, String user, String pass, String maquina)
	{

		JSch jsch = new JSch();
		Session session = null;
		String fileName = "";

		try
		{
			session = jsch.getSession(user, maquina, 22);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pass);

			session.connect();

			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp)channel;


			ArrayList<String> list = new ArrayList<String>();
			Vector<LsEntry> entries = sftpChannel.ls(rutaCarpeta);

			for(LsEntry entry : entries)
			{
				if(!entry.getAttrs().isDir())
				{
					list.add(entry.getFilename());
				}
			}

			// Subimos
			sftpChannel.put(archivo, rutaCarpeta);

			sftpChannel.exit();
			session.disconnect();
		}
		catch(Exception e)
		{
			fail("Hubo un problema al intentar subir el fichero: "+fileName+ " por FTP: "+ e.toString());
		}



	}


	public static String downloadFileSFTP(String rutaCarpeta, String archivo, String user, String pass, String maquina, Date fecha)
	{

		JSch jsch = new JSch();
		Session session = null;

		String destPath = "";
		String fileName = "";

		try
		{
			session = jsch.getSession(user, maquina, 22);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pass);

			session.connect();

			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp)channel;

			// Descargamos

			ArrayList<String> list = new ArrayList<String>();
			Vector<LsEntry> entries = sftpChannel.ls(rutaCarpeta);
			for(LsEntry entry : entries)
			{
				if(entry.getFilename().toLowerCase().endsWith(".error"))
				{

					SftpATTRS attrs = entry.getAttrs();
					Date dateModify = new Date(attrs.getMTime() * 1000L);

					if(fecha.compareTo(dateModify) < 0)
					{
						list.add(entry.getFilename());
						break;
					}


				}
			}

			if(!list.isEmpty())
			{
				destPath = "results/";
				fileName = list.get(0);
				sftpChannel.get(rutaCarpeta + "/" + fileName, destPath + fileName);
			}

			sftpChannel.exit();
			session.disconnect();


		}
		catch(JSchException e)
		{
			utils.Log.writeException(e); // To change body of catch statement use File | Settings | File Templates.
		}
		catch(SftpException e)
		{
			utils.Log.writeException(e);
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
		}

		return destPath + fileName;


	}


	public static String downloadParticularFileSFTP(String rutaCarpeta, String archivo, String user, String pass, String maquina, Date fecha)
	{

		JSch jsch = new JSch();
		Session session = null;
		String destPath = "results/";

		try
		{
			session = jsch.getSession(user, maquina, 22);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(pass);

			session.connect();

			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp)channel;

			// Descargamos
			sftpChannel.get(rutaCarpeta + "/" + archivo, destPath + archivo);
			sftpChannel.exit();
			session.disconnect();


		}
		catch(JSchException e)
		{
			utils.Log.writeException(e); // To change body of catch statement use File | Settings | File Templates.
		}
		catch(SftpException e)
		{
			utils.Log.writeException(e);
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
		}

		return destPath + archivo;


	}


	public void getAllframes(final WebDriver driver, final String id)
	{
		final List<WebElement> iframes = driver.findElements(By.tagName("iframe"));
		for(WebElement iframe : iframes)
		{
			if(iframe.getAttribute("id").equals(id))
			{
				// TODO your stuff.
			}
		}
	}


	public static void esperarProcesandoKYOS(WebDriver driver) throws Exception
	{
		int contador = 0;
		boolean desaparece = false;
		while(contador < 20)
		{
			if(!Utils.isDisplayed(driver, Constantes.ImagenCargandoKYOS, 1))
			{
				contador++;
				Thread.sleep(1000);
			}
			else
			{
				break;
			}

		}

		contador = 0;

		while(contador < 50)
		{
			if(Utils.isDisplayed(driver, Constantes.ImagenCargandoKYOS, 1) || Utils.isDisplayed(driver, "//*[contains(text(),'Procesando su petici�n')]", 1))
			{
				contador++;
				Thread.sleep(1000);
			}
			else
			{
				desaparece = true;
				break;
			}

		}
		if(!desaparece)
			Log.write("TimeOut de 50 segundos esperando a procesar la petici�n");
	}


	public static void esperarProcesandoPeticionFirmas(WebDriver driver) throws Exception
	{
		
		int contador = 0;
		boolean desaparece = false;
		boolean aparece = false;

		while((contador < 10) && (!aparece))
		{
			if(!Utils.isDisplayed(driver, Constantes.ImagenCargandoFirmas, 1))
			{
				contador++;
				Thread.sleep(1000);
			}
			else
			{
				aparece = true;
				break;
			}

		}

		contador = 0;

		while((contador < 100) && (!desaparece))
		{
			if(Utils.isDisplayed(driver, Constantes.ImagenCargandoFirmas, 1))
			{
				contador++;
				Thread.sleep(1000);
			}
			else
			{
				desaparece = true;
				break;
			}

		}
		
	}


	public static String obtieneProximaFechaHabil()
	{
		Date today;
		String fecha;
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("dd" + "/" + "MM" + "/" + "yyyy");
		today = new Date();

		Calendar cal = Calendar.getInstance();
		cal.setTime(today);

		int numeroDia = cal.get(Calendar.DAY_OF_WEEK);

		if((numeroDia == 6) || (numeroDia == 7))
			cal.add(Calendar.DATE, 3);
		else
			cal.add(Calendar.DATE, 1);
		numeroDia = cal.get(Calendar.DAY_OF_WEEK);

		fecha = formatter.format(cal.getTime());

		return fecha;
	}


	public static String obtieneFechaHoy()
	{
		Date today;
		String fecha;
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("ddMMyyyy");
		today = new Date();
		fecha = formatter.format(today);

		return fecha;
	}


	public static void seleccionarComboXPathPorValue(WebDriver driver, String xpath, String Opcion) throws Exception
	{
		Select select=new Select(Utils.devuelveElementoVisibleYEnabled(driver, xpath));
		select.selectByValue(Opcion);
//		WebElement combo = driver.findElement(By.xpath(xpath));
//		List<WebElement> opcionesCombo = combo.findElements(By.tagName("option"));
//		for(WebElement opcion : opcionesCombo)
//		{
//			if(opcion.getAttribute("value").equalsIgnoreCase(Opcion))
//				opcion.click();
//		}
	}


	public static void esperaHastaDesapareceSinSalir(WebDriver driver, String xpath) throws Exception
	{
		int contador = 0;
		boolean desaparece = false;
		while(contador < 50)
		{
			if(Utils.isDisplayed(driver, xpath, 1))
			{
				contador++;
				Thread.sleep(1000);
			}
			else
			{
				desaparece = true;
				break;
			}
		}
		if(!desaparece)
			Log.write("TimeOut de 50 segundos esperando a " + xpath);
	}


	public static void loginEsperandoPor(WebDriver driver, Usuario usuario, String xpathEspera) throws Exception
	{
		
		Utils.login(driver, usuario);

		Utils.esperarVisibleTiempoSinSalir(driver, xpathEspera, 40);
		boolean esVisible = Utils.isDisplayed(driver, xpathEspera, 1);

		if(!esVisible)
			throw new Exception("No se pudo realizar el login esperando por " + xpathEspera + ". Comprobar captura");

	}


	public static void mouseSobreElVisible(WebDriver driver, String xpath) throws Exception
	{

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					elemento = listaElementos.get(i);
			}
			Actions action = new Actions(driver);
			action.moveToElement(elemento).build().perform();
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}

	}


	public static String obtieneFechaMasDias(int numDias)
	{
		Date today;
		String fecha;
		SimpleDateFormat formatter;
		formatter = new SimpleDateFormat("dd" + "/" + "MM" + "/" + "yyyy");
		today = new Date();

		Calendar cal = Calendar.getInstance();
		cal.setTime(today);

		int numeroDia = cal.get(Calendar.DAY_OF_WEEK);

		if((numeroDia == 6) || (numeroDia == 7))
		{
			numDias = numDias + 3;
			cal.add(Calendar.DATE, numDias);
		}
		else
			cal.add(Calendar.DATE, numDias);

		numeroDia = cal.get(Calendar.DAY_OF_WEEK);

		fecha = formatter.format(cal.getTime());

		return fecha;
	}


	public static void loginWebGestor(WebDriver driver, Resultados resultado, String tipoLogin, String refExterna, String oficina, String folio) throws Exception
	{

		Utils.esperarVisibleTiempo(driver, Constantes.desconectarSST, 35);

		// Validamos t�tulo
		Utils.vpWarning(driver, resultado, "T�tulo Contrataci�n SST", true, Utils.isDisplayed(driver, Constantes.TituloContratacion, 5), "FALLO");


		if(tipoLogin.equalsIgnoreCase("Referencia Externa"))
		{

			// Seleccionamos el tipo de Acceso
			Utils.seleccionarComboContieneXPath(driver, Constantes.comboTipoAcceso, "Referencia externa");
			Thread.sleep(1500);

			// Introducimos Ref. Externa
			Utils.introducirTextoEnElVisible(driver, Constantes.referenciaExterna, refExterna);


		}
		else if(tipoLogin.equalsIgnoreCase("Contrato"))
		{
			// Seleccionamos el tipo de Acceso
			Utils.seleccionarComboContieneXPath(driver, Constantes.comboTipoAcceso, "Contrato");
			Thread.sleep(1500);

			// Introducimos Contrato (oficina - folio)
			Utils.introducirTextoEnElVisible(driver, Constantes.oficina, oficina);
			Utils.introducirTextoEnElVisible(driver, Constantes.folio, folio);

		}

		Utils.clickEnElVisible(driver, Constantes.botonAccederSST);

		Utils.esperarProcesandoPeticionMejoradoSST(driver);
		Thread.sleep(1500);

	}


	public static void clickEnElVisibleSinoSeleccionado(WebDriver driver, String xpath) throws Exception
	{

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;
			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					if(!listaElementos.get(i).isSelected())
					{
						elemento = listaElementos.get(i);
						elemento.click();
						Utils.esperarProcesandoPeticionDefaultContentVolviendoAPPal(driver);
						break;
					}	
			}
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}

	}


	public static String[] getPathsLanzamiento()
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String archivosLanzamiento = params.getValorPropiedad("rutaLanzamiento");

		String pathsLanzamiento[] = archivosLanzamiento.split(";");

		return pathsLanzamiento;
	}


	public static void refresh(WebDriver driver)
	{

		driver.navigate().refresh();
		try
		{
			
			if (Utils.isDisplayed(driver, Constantes.frameCampanaGenerico, 2))
				Utils.cambiarFrameDadoVisible(driver, Constantes.frameCampanaGenerico);
			
			if (Utils.isDisplayed(driver, Constantes.continuarCampania, 0)){
				Atomic.click(driver, Constantes.continuarCampania);
				Utils.esperaHastaApareceSinSalir(driver, Constantes.BotonDesconectar, 1);
			}
			
			if (!Utils.isDisplayedSinBefore(driver, Constantes.BotonDesconectar, 1))
				Utils.aceptarCampaniaSiSale(driver);
		}
		catch(Exception e)
		{

		}

	}


	public static String seleccionarComboDevuelveTexto(WebDriver driver, String xpath) throws Exception
	{
		
		Select select=new Select(Utils.devuelveElementoVisibleYEnabled(driver, xpath));
		
		String value = "";

		for(WebElement opcion : select.getOptions())
		{
			if(!opcion.getText().toLowerCase().contains("selecciona"))
			{
				opcion.click();
				value = opcion.getText();
				break;
			}
		}

		return value;
	}


	public static String devuelveTextoDelVisible(WebDriver driver, String xpath) throws Exception
	{

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					elemento = listaElementos.get(i);
			}

			return elemento.getText();

		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}

	}


	public static String quitarLetrasDeFromString(String cadena)
	{
		String digitos = cadena.replaceAll("[^\\d,.-]", "");

		String numero;

		try
		{
			numero = digitos;
			// numero = Integer.valueOf(digitos);
		}

		catch(Exception e)
		{
			numero = "-1";
		}

		return numero;
	}


	public static void unZipFile(String pZipFile, String pFile) throws Exception
	{
		BufferedOutputStream bos = null;
		FileInputStream fis = null;
		ZipInputStream zipis = null;
		FileOutputStream fos = null;
		int BUFFER_SIZE = 1024;

		try
		{
			fis = new FileInputStream(pZipFile);
			zipis = new ZipInputStream(new BufferedInputStream(fis));
			if(zipis.getNextEntry() != null)
			{
				int len = 0;
				byte[] buffer = new byte[BUFFER_SIZE];
				fos = new FileOutputStream(pFile);
				bos = new BufferedOutputStream(fos, BUFFER_SIZE);

				while((len = zipis.read(buffer, 0, BUFFER_SIZE)) != -1)
					bos.write(buffer, 0, len);
				bos.flush();
			}
			else
			{
				throw new Exception("El zip no contenia fichero alguno");
			}
		}
		catch(Exception e)
		{
			throw e;
		}
		finally
		{
			bos.close();
			zipis.close();
			fos.close();
			fis.close();
		}
	}


	public static WebDriver inicializarCasoSinUrl(WebDriver driver, String m_codigo, String m_nombre, String...cositas) throws Exception
	{
		// Establecemos que la salida est�ndar y la salida de errores sea el fichero que se crea por caso de prueba
		FileOutputStream fichero = new FileOutputStream("results/" + m_nombre + ".html", true);
		PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);
		PrintStream ficheroMain = new PrintStream(new BufferedOutputStream(new FileOutputStream("results/Main.txt", true)), true);

		ficheroMain.append("[INFO] [INICIO]" + Utils.GetTodayDateAndTime() + " Lanzando caso: " + m_nombre + ", codigo: " + m_codigo + "\n");
		ficheroMain.close();

//  	  	PropertyConfigurator.configure("properties/log4j.properties");
//  	  	log.addAppender(new FileAppender(new PatternLayout(),"results/"+m_nombre+".txt", false));

//		System.setOut(ficheroSalida);
//		System.setErr(ficheroSalida);
		
		loggers.put(Long.toString(Thread.currentThread().getId()), new Log(m_nombre));

		// Pintamos el inicio de la traza
		Utils.pintarInicioTraza(m_codigo, m_nombre);

		// Cargamos las propiedades del properties
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String archivoCSV = params.getValorPropiedad("rutaResultados");

		// Inicializamos el caso a ERROR.
		Utils.CsvAppend(archivoCSV, m_codigo, m_nombre, "ERROR", "", "");

		// Obtenemos el driver espec�fico del navegador
		driver = Utils.getWebDriverBrowser(m_nombre,cositas);

		if(driver == null)
		{
			throw new Exception("No se estableci� correctamente la propiedad correspondiente al navegador. Revisar el archivo properties.");
		}

		// driver.get("");

		// Navegamos a la url
		//driver.get("");

		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
		if (!driver.toString().contains("ChromeDriver"))
			driver.manage().window().maximize();

		return driver;
	}
	
	public static WebDriver inicializarCasoChromeLeyendoOptions(WebDriver driver, String m_codigo, String m_nombre) throws Exception
	{
		
		FileOutputStream fichero = new FileOutputStream("results/" + m_nombre + ".html", true);
		PrintStream ficheroMain = new PrintStream(new BufferedOutputStream(new FileOutputStream("results/Main.txt", true)), true);

		ficheroMain.append("[INFO] [INICIO]" + Utils.GetTodayDateAndTime() + " Lanzando caso: " + m_nombre + ", codigo: " + m_codigo + "\n");
		ficheroMain.close();

		loggers.put(Long.toString(Thread.currentThread().getId()), new Log(m_nombre));

		// Pintamos el inicio de la traza
		Utils.pintarInicioTraza(m_codigo, m_nombre);
		
		
		
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		
		String rutaDriver = params.getValorPropiedad("rutaDriver");
		
		File exeChrome = new File(rutaDriver);
		
		if (!exeChrome.exists())
			Log.write("ERROR: No existe el .exe de CHROMEDRIVER");

		// Obtenemos el driver espec�fico del navegador
		System.setProperty("webdriver.chrome.driver", rutaDriver);
		


		String downloads = new File("downloads").getAbsolutePath();
		
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.default_directory", downloads);	
		chromePrefs.put("download.prompt_for_download", false);
		chromePrefs.put("download.directory_upgrade", true);
		
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePrefs);
		
		String[] chromeOptions = params.getValorPropiedad("chromeOptions").split(",");
		
		Log.write("Chrome Options:");
		
		for (int i = 0; i < chromeOptions.length; i++) {
			options.addArguments(chromeOptions[i]);
			Log.write(chromeOptions[i]);
		}
		
//		options.addArguments("enable-automation");
//		options.addArguments("start-maximized");
//		options.addArguments("disable-popup-blocking");
//		options.addArguments("disable-default-apps"); 
//		options.addArguments("headless");
//		options.addArguments("--no-sandbox"); 
//		options.addArguments("--disable-dev-shm-usage");
//		options.addArguments("ignore-certificate-errors");
		
		options.setExperimentalOption("useAutomationExtension", false);
		
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
		
		options.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
		options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		
		driver = new ChromeDriver(options);

		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
		
		Log.write("Window size:" + driver.manage().window().getSize());
		
		driver.manage().window().setSize(new Dimension(1440, 900));
		
		Log.write("Window RE-size:" + driver.manage().window().getSize());
		
		return driver;
	}


	public static String devuelveTextoDelVisiblePrimero(WebDriver driver, String xpath) throws Exception
	{

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			boolean encontrado = false;

			for(int i = 0; i < listaElementos.size() && !encontrado; ++i)
			{
				if(listaElementos.get(i).isDisplayed()){
					elemento = listaElementos.get(i);
					encontrado = true;
				}
			}

			return elemento.getText();

		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}

	}
	
	public static String devuelveValueDelVisible(WebDriver driver, String xpath) throws Exception
	{

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			boolean encontrado = false;

			for(int i = 0; i < listaElementos.size() && !encontrado; ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					elemento = listaElementos.get(i);
				encontrado = true;
			}

			return elemento.getAttribute("value");

		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}

	}


	public static void esperaMientrasNumeroElementosSeaUnValor(WebDriver driver, String xpath, int numeroElementos, int timeOutSegundos) throws Exception
	{

		int contador = 0;

		while((Utils.numeroElementosVisibles(driver, xpath) != numeroElementos) && contador < timeOutSegundos)
		{
			Thread.sleep(1000);
			contador++;
		}

		Thread.sleep(1500);

	}
	
	
	/**
	 * @return devuelve el numero de elementos que coinciden con un patron
	 */
	public static int numeroFicherosEnRutaConUnPatron (String ruta, final String patron) {
		File dir = new File(ruta);
		File[] files = dir.listFiles(new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		        return name.toLowerCase().endsWith(patron);
		    }
		});
		return files.length;
	}
	

	/**
	 * @param hoy
	 * @param horaInicio
	 * @param horaFin
	 * @param lista
	 * @throws Exception
	 * 
	 * @return devuelve el nuevo html de resultados
	 */
	@SuppressWarnings("unchecked")
	public static void CSVToTableNew(java.util.Date hoy, String horaInicio, String horaFin, List<String> lista) throws Exception
	{
		
		//Hacer solo el informe si no hay running
		
		Thread.sleep(10000);
		
		if (numeroFicherosEnRutaConUnPatron("resources/",".running")==0) {
			
			//Redirigimos la salida estandar a la consola para no interferir en los casos
//			System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
		
			Utils.crearBloqueo("informeNuevo", 300);
			
			String generarFichero = params.getValorPropiedad("generarFichero");
			
			if(generarFichero!=null && generarFichero.equalsIgnoreCase("SI"))
			{
				@SuppressWarnings("rawtypes")
				Class utilidad = Class.forName("utils.ActualizarC204");
				junit.textui.TestRunner.run(utilidad);
			}
			
			Utils.CSVToTable(hoy, horaInicio, horaFin, lista);
			generaDatosEjecucionHTML(hoy, horaInicio, horaFin);
			List<Integer> totales = generaResumenHTML(hoy, horaInicio, horaFin, lista);
			generaInformeDetallado(totales.get(0),totales.get(1),totales.get(2),totales.get(3),lista);
			
			Utils.liberarBloqueo("informeNuevo");
		
		}
	}
	
	/**
	 * @param hoy
	 * @param horaInicio
	 * @param horaFin
	 * @throws Exception
	 * 
	 * @return devuelve los datos de la ejecuci�n en HTML
	 */
	public static void generaDatosEjecucionHTML (java.util.Date hoy, String horaInicio, String horaFin) throws Exception{
		
		Random random = new Random();
		int segundosEspera = random.nextInt(5000);

		Thread.sleep(segundosEspera);
		
		String enlaceLogo = "javascript:void(0);";

		// M�todo para pasar del CSV de resultados a una tabla HTML

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		
		String navegador = params.getValorPropiedad("browser");
		String entorno = params.getValorPropiedad("entorno");
		String pais = params.getValorPropiedad("pais");
		
		// java.util.Date hoy = new java.util.Date();
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
		String diaActual = formatter2.format(hoy);
		
		String charset = "windows-1252";
		
		if (Utils.isUnix())
			charset = "UTF-8";

		String contenidoDatosEjecucion = "\r\n<!DOCTYPE html>\r\n<!-- Formateador HTML5 IE -->\r\n<!--[if IE]>\rn<script src=\"js/html5shiv.js\"></script>\r\n<![endif]-->\r\n<html  lang=\"es\" >\r\n<!--<![endif]-->\r\n<head>\r\n  <title>BBVA Pruebas Autom&aacute;ticas</title>\r\n  <meta charset=\""+charset+"\">\r\n  <meta name=\"robots\" content=\"noindex, nofollow\" />\r\n  <meta name=\"description\" content=\"\">\r\n  <meta name=\"HandheldFriendly\" content=\"True\">\r\n  <meta name=\"MobileOptimized\" content=\"320\">\r\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\" />\r\n  \r\n  \r\n  \r\n  <link rel=\"apple-touch-icon-precomposed\" sizes=\"100x100\" href=\"http://www.direction.es/clientes/bbva/directdebit/img/comun/icon_bbva.png\">\r\n  <link rel=\"stylesheet\" href=\"css/styles.css\" />\r\n  <link rel=\"stylesheet\" href=\"css/jquery-ui.min.css\" />\r\n  <script src=\"https://code.jquery.com/jquery-1.10.2.js\"  integrity=\"sha256-it5nQKHTz+34HijZJQkpNBIHsjpV8b6QzMJs9tmOBSo=\"  crossorigin=\"anonymous\"></script>\r\n  <script type=\"text/javascript\"  src=\"js/jquery-ui.min.js\"></script>\r\n  <script type=\"text/javascript\"  src=\"js/jquery.placeholder.min.js\"></script>\r\n  <script src=\"https://code.highcharts.com/highcharts.js\"></script>\r\n  <script src=\"js/rounded-corners.js\"></script>\r\n  <script type=\"text/javascript\"  src=\"js/plugins.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/jquery.hammer.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/hammer.min.js\"></script>\r\n  <script src=\"js/modernizr-2.7.2.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/jquery.easydropdown.min.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/jquery.offcanvas.min.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/prefixfree.min.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/codigo.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/grafico2.js\"></script>\r\n  <link rel=\"stylesheet\" href=\"/resources/demos/style.css\">\r\n  <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>\r\n\r\n  \r\n\r\n   <script>\r\n  $( function() {\r\n    $('.jqDialog').each(function(){\r\n\t\t$(this).dialog({\r\n\t\t autoOpen: false,\r\n\t\t});\r\n\t});\r\n  });\r\n  \r\n  function mostrarDialog(id){\r\n\t$( \"#\"+ id ).dialog( \"open\" );\r\n  }\r\n  \r\n  </script>\r\n  \r\n  \r\n</head>\r\n\r\n<body>\r\n\t<div id=\"principal\" class=\"layout-base\" >\r\n\t\t<!-- INICIO CABECERA AZUL -->\r\n\t\t<div class=\"header_fixed\">\r\n\t\t\t<header class=\"topBar\" id=\"logo_01\"  >\r\n\t\t\t\t<div class=\"elmarron\" >\r\n\t\t\t\t\t<div class=\"header_div\">\r\n\t\t\t\t\t\t<div class=\"help\">\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:window.history.back();\"><< Volver</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<h1 id=\"logo\"><a href=\""+enlaceLogo+"\"><em>BBVA Pruebas Autom\u00E1ticas</em></a></h1>\r\n\t\t\t\t\t\t<nav class=\"metanav\">\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t<li class=\"idioma\"></li>\r\n\t\t\t\t\t\t\t\t<li></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</nav>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"container\" id=\"operaciones_f\" >\r\n\t\t\t\t\t<div class=\"container_interior\">\r\n\t\t\t\t\t\t<a id=\"lupaFalsa\" title=\"buscador\" href=\"#\"></a>\r\n\t\t\t\t\t\t<a class=\"op_f\" href=\"datosEjecucion.html\">Datos ejecuci&oacute;n</a>\r\n\t\t\t\t\t\t<a class=\"op_f\" href=\"resumenEjecucion.html\">Resumen de ejecuci&oacute;n</a>\r\n\t\t\t\t\t\t<a class=\"op_f\" href=\"informeDetallado.html\">Informe detallado</a>\r\n\t\t\t\t\t\t<div class=\"hamb_icon\">\r\n\t\t\t\t\t\t\t<div id=\"nav-icon1Falso\">\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t\t<span class=\"mob_ile\"></span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<a href=\""+enlaceLogo+"\" class=\"connect\">Desconectar</a>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"container_search\" style=\"display:none\" >\r\n\t\t\t\t\t<div class=\"container_main\">\r\n\t\t\t\t\t\t<div class=\"searcher\">\r\n\t\t\t\t\t\t\t<a id=\"lupa_close\"  title=\"buscador\"></a>\r\n\t\t\t\t\t\t\t<input type=\"search\" class=\"search_hide\" value=\"Buscar movimientos, &oacute;rdenes, operativas, productos...\" onfocus=\"if(this.value == 'Buscar movimientos, &oacute;rdenes, operativas, productos...') { this.value = ''; }\"  />\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"hamb_icon\" id=\"search_close\">\r\n\t\t\t\t\t\t\t<div id=\"nav-icon2\" class=\"open\">\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div id=\"buscador_capa\" style=\"display:none\" >\r\n\t\t\t\t\t<div id=\"buscador_links\">\r\n\t\t\t\t\t\t<ul class=\"bl\">\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Quick links</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t<li><a href=\"#\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"#\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"#\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"#\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"#\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t<div class=\"no_find\">&iquest;No encuentras lo que buscas? llamanos al  <a href=\"#\">900 000 000</a> o m&aacute;ndanos un <a href=\"#\">mensaje</a>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<nav id=\"main_nav\" >\r\n\t\t\t\t\t<div id=\"menu_principal\">\r\n\t\t\t\t\t\t<div class=\"perfil_modulo\">\r\n\t\t\t\t\t\t\t<h4 class=\"clearfix\"><strong></strong><em>JuanPe</em></h4>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t<li class=\"idioma\"><a href=\"index_english.html\">Espa\u00F1ol</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div id=\"menu_principal_interior\">\r\n\t\t\t\t\t\t\t<div id=\"menu_principal_interior_blanco\">\r\n\t\t\t\t\t\t\t\t<div class=\"pestanas\">\r\n\t\t\t\t\t\t\t\t\t<nav class=\"tab-navigation fn-tabbednavigation\">\r\n\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\">\r\n\t\t\t\t\t\t\t\t\t\t\t<ul class=\"clearfix cl\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"selected fn-tabbednavigationitem\" href='#mis_productos' ><em>Mis productos</em></a></li>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"fn-tabbednavigationitem\" href='#operar'><em>Mis operaciones</em></a></li>\r\n\t\t\t\t\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t</nav>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>  \r\n\t\t\t\t\t\t</div>   \r\n\t\t\t\t\t\t<div class=\"menu_content\" >\r\n\t\t\t\t\t\t\t<ul class=\"mis_productos\" id=\"mis_productos\">\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Todos</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\""+enlaceLogo+"\">Situaci&oacute;n financiera</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Cuentas</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"index_cuenta.html\">*0000</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*2020</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*4040</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*6060</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"cuentas.html\">Ver todas</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Tarjetas</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*2222</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*5555</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*9999</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Ver todas</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Pr&eacute;stamos</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*1010</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*3030</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Ver todos</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Contratar</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Cuentas</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Tarjetas</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Pr&eacute;stamos</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Cuentas de cr&eacute;ditos</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Avales</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Leasing</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Ver todos</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t<ul class=\"operar\" id=\"operar\">\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Enviar dinero</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">N\u00F3minas</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"transferencia_ind.html\">Transferencias individuales</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"transferencia_mul.html\">Transferencias m\u00FAltiples</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Impuestos</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Cobrar dinero</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Domiciliaciones</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Recibos</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Ficheros</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"subida_ficheros.html\">Subida de ficheros</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Firmas</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Firma de \u00F3rdenes</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Contratar</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Cheques</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Confirming</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Informaci\u00F3n fiscal</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">TPVs</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Ver todos</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"botones_fixed\">\r\n\t\t\t\t\t\t<a href=\"#\"  class=\"ayuda\"><strong>Ayuda </strong></a>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</nav>\r\n\t\t\t</header>\r\n\t\t</div>\r\n\t\t<!-- FIN CABECERA AZUL -->\r\n\r\n\t\t\r\n\t\t<div class=\"scrollableArea\" id=\"mainScrollableArea\">\r\n\t\t\t<section class=\"main_section\"  id=\"cuenta_detail\">\r\n\t\t\t\t<section class=\"main_content\" id=\"time-now\" >\r\n\t\t\t\t\t<article class=\"with_table data_tab w_d_margin\" id=\"cuenta_detalle_tab02\">\r\n\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t<!-- END TAB NAVEGATION   -->\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t<div class=\"menu_content\" >\r\n\t\t\t\t\t\t<ul class=\"mis_productos\" id=\"datosEjecucion\">\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t <li class=\"li_element\">"
				+ "<strong>Pa&iacute;s</strong>\r\n\t\t\t\t\t\t <ul>\r\n\t\t\t\t\t\t <li>"+pais+"</li>\r\n\t\t\t\t\t\t </ul>\r\n\t\t\t\t\t\t </li>\r\n\t\t\t\t\t\t \r\n\t\t\t\t\t\t <li class=\"li_element\">"
				+ "<strong>Navegador</strong>\r\n\t\t\t\t\t\t <ul>\r\n\t\t\t\t\t\t <li>"+navegador+"</li>\r\n\t\t\t\t\t\t </ul>\r\n\t\t\t\t\t\t </li>\r\n\t\t\t\t\t\t \r\n\t\t\t\t\t\t <li class=\"li_element\">"
				+ "<strong>Entorno</strong>\r\n\t\t\t\t\t\t <ul>\r\n\t\t\t\t\t\t <li>"+entorno+"</li>\r\n\t\t\t\t\t\t </ul>\r\n\t\t\t\t\t\t </li>\r\n\t\t\t\t\t\t  \r\n\t\t\t\t\t\t <li class=\"li_element\">"
				+ "<strong>Fecha</strong>\r\n\t\t\t\t\t\t <ul>\r\n\t\t\t\t\t\t <li>"+diaActual+"</li>\r\n\t\t\t\t\t\t </ul>\r\n\t\t\t\t\t\t </li>\r\n\t\t\t\t\t\t  \r\n\t\t\t\t\t\t <li class=\"li_element\">"
				+ "<strong>Hora de inicio</strong>\r\n\t\t\t\t\t\t <ul>\r\n\t\t\t\t\t\t <li>"+horaInicio+"</li>\r\n\t\t\t\t\t\t </ul>\r\n\t\t\t\t\t\t </li>\r\n\t\t\t\t\t\t  \r\n\t\t\t\t\t\t <li class=\"li_element\">"
				+ "<strong>Hora de fin</strong>\r\n\t\t\t\t\t\t <ul>\r\n\t\t\t\t\t\t <li>"+horaFin+"</li>\r\n\t\t\t\t\t\t </ul>\r\n\t\t\t\t\t\t </li>\r\n\r\n\t\t\t\t\t\t<li class=\"li_element\">"
				+ "<strong>Usuario</strong>\r\n\t\t\t\t\t\t <ul>\r\n\t\t\t\t\t\t <li>"+System.getProperty("user.name").toLowerCase()+"</li>\r\n\t\t\t\t\t\t </ul>\r\n\t\t\t\t\t\t </li>\t\t\t\t\t\t \r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t</article>\r\n\t\t\t\t</section>\r\n\t\t\t</section>\r\n\t\t</div>\r\n\t</div>\r\n\t\r\n\t\r\n\r\n\r\n\t\r\n\r\n</body></html>";
		
		// Volcamos este string a un fichero nuevo
		File file = new File("results/InformeHTML/datosEjecucion.html");

		FileWriter fileHtm = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fileHtm);

		bw.write(contenidoDatosEjecucion);
		bw.close();
		
	}
	
	public static ArrayList<Integer> generaResumenHTML (java.util.Date hoy, String horaInicio, String horaFin, List<String> lista) throws Exception{
		
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String archivoCSV = params.getValorPropiedad("rutaResultados");


		CsvReader reader = new CsvReader(new FileReader(archivoCSV));
		
		String enlaceLogo = "javascript:void(0);";

		// java.util.Date hoy = new java.util.Date();

		int resultadosOK = 0;
		int resultadosERROR = 0;
		int resultadosWARNING = 0;
		int resultadosOK2 = 0;
		int resultadosERROR2 = 0;
		int resultadosWARNING2 = 0;
		int totalesINT = 0;

		try
		{

			while(reader.readRecord())
			{
				String testName = reader.get(1);
				String resul = reader.get(2);

				if(lista != null)
				{
					for(int k = 0; k < lista.size(); ++k)
					{
						if(lista.get(k).indexOf(testName) > -1)
							resul = resul + "*";
					}
				}

				if(resul.indexOf("OK") > -1)
					resultadosOK++;
				else if(resul.indexOf("ERROR") > -1)
					resultadosERROR++;
				else if(resul.indexOf("WARNING") > -1)
					resultadosWARNING++;

				if(resul.equalsIgnoreCase("OK*"))
					resultadosOK2++;
				else if(resul.equalsIgnoreCase("ERROR*"))
					resultadosERROR2++;
				else if(resul.equalsIgnoreCase("WARNING*"))
					resultadosWARNING2++;


			}

			totalesINT = resultadosOK + resultadosERROR + resultadosWARNING;

			double totales = resultadosOK + resultadosERROR + resultadosWARNING;
			double porcentajeOK = Math.round((resultadosOK * 1.0 / totales) * 100.0);
			double porcentajeERROR = Math.round((resultadosERROR * 1.0 / totales) * 100.0);
			double porcentajeWARNING = Math.round((resultadosWARNING * 1.0 / totales) * 100.0);

			try
			{
				porcentajeOK = Utils.roundTwoDecimals((resultadosOK * 1.0 / totales) * 100.0);
				porcentajeERROR = Utils.roundTwoDecimals((resultadosERROR * 1.0 / totales) * 100.0);
				porcentajeWARNING = Utils.roundTwoDecimals((resultadosWARNING * 1.0 / totales) * 100.0);
			}
			catch(Exception e)
			{

			}

			String literalSatisfactorios = "OK";
			String literalError = "Error";
			String literalAvisos = "Avisos";

			if(resultadosOK2 > 0)
				literalSatisfactorios = "OK*";
			if(resultadosERROR2 > 0)
				literalError = "Error*";
			if(resultadosWARNING2 > 0)
				literalAvisos = "Avisos*";

			String literalTrasReEjecucion = "";

			String charset = "windows-1252";
			
			if (Utils.isUnix())
				charset = "UTF-8";
			
			if(resultadosOK2 > 0 || resultadosERROR2 > 0 || resultadosWARNING2 > 0)
			{
				int totalesReejecutados = lista.size();
				literalTrasReEjecucion = "<br><a style=\"font-size: 15px;\">*Tras reejecuci�n de " + totalesReejecutados + " casos por error o por dependencia de un caso de prueba err�neo:</a>";

				if(resultadosOK2 > 0)
					literalTrasReEjecucion = literalTrasReEjecucion + "<br><a style=\"font-size: 15px;\">" + resultadosOK2 + " caso(s) Satisfactorios</a>";
				if(resultadosERROR2 > 0)
					literalTrasReEjecucion = literalTrasReEjecucion + "<br><a style=\"font-size: 15px;\">" + resultadosERROR2 + " caso(s) Error</a>";
				if(resultadosWARNING2 > 0)
					literalTrasReEjecucion = literalTrasReEjecucion + "<br><a style=\"font-size: 15px;\">" + resultadosWARNING2 + " caso(s) Avisos</a>";
			}

		String contenidoResumenEjecucion = "\r\n<!DOCTYPE html>\r\n<!-- Formateador HTML5 IE -->\r\n<!--[if IE]>\r\n<script src=\"js/html5shiv.js\"></script>\r\n<![endif]-->\r\n<html  lang=\"es\" >\r\n<!--<![endif]-->\r\n<head>\r\n  <title>BBVA Pruebas Autom&aacute;ticas</title>\r\n   <meta http-equiv=\"Content-Type\" content=\"text/html;\" /> <meta http-equiv=\"Content-Type\" content=\"image/jpg;\" /> <meta http-equiv=\"Content-Type\" content=\"image/jpeg;\" />  <meta charset=\""+charset+"\">  <meta name=\"robots\" content=\"noindex, nofollow\" />\r\n  <meta name=\"description\" content=\"\">\r\n  <meta name=\"HandheldFriendly\" content=\"True\">\r\n  <meta name=\"MobileOptimized\" content=\"320\">\r\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\" />\r\n  \r\n  \r\n  \r\n  <link rel=\"apple-touch-icon-precomposed\" sizes=\"100x100\" href=\"http://www.direction.es/clientes/bbva/directdebit/img/comun/icon_bbva.png\">\r\n  <link rel=\"stylesheet\" href=\"css/styles.css\" />\r\n <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script> <link rel=\"stylesheet\" href=\"css/jquery-ui.min.css\" />\r\n  <script src=\"https://code.jquery.com/jquery-1.10.2.js\"  integrity=\"sha256-it5nQKHTz+34HijZJQkpNBIHsjpV8b6QzMJs9tmOBSo=\"  crossorigin=\"anonymous\"></script>\r\n  <script type=\"text/javascript\"  src=\"js/jquery-ui.min.js\"></script>\r\n  <script type=\"text/javascript\"  src=\"js/jquery.placeholder.min.js\"></script>\r\n  <script src=\"https://code.highcharts.com/highcharts.js\"></script>\r\n  <script src=\"js/rounded-corners.js\"></script>\r\n  <script type=\"text/javascript\"  src=\"js/plugins.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/jquery.hammer.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/hammer.min.js\"></script>\r\n  <script src=\"js/modernizr-2.7.2.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/jquery.easydropdown.min.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/jquery.offcanvas.min.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/prefixfree.min.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/codigo.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/grafico2.js\"></script>\r\n  <link rel=\"stylesheet\" href=\"/resources/demos/style.css\">\r\n  <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>\r\n\r\n  \r\n\r\n   <script>\r\n  $( function() {\r\n    $('.jqDialog').each(function(){\r\n\t\t$(this).dialog({\r\n\t\t autoOpen: false,\r\n\t\t});\r\n\t});\r\n  });\r\n  \r\n  function mostrarDialog(id){\r\n\t$( \"#\"+ id ).dialog( \"open\" );\r\n  }\r\n  \r\n  </script>\r\n  \r\n  \r\n</head>\r\n\r\n<body>\r\n\t<div id=\"principal\" class=\"layout-base\" >\r\n\t\t<!-- INICIO CABECERA AZUL -->\r\n\t\t<div class=\"header_fixed\">\r\n\t\t\t<header class=\"topBar\" id=\"logo_01\"  >\r\n\t\t\t\t<div class=\"elmarron\" >\r\n\t\t\t\t\t<div class=\"header_div\">\r\n\t\t\t\t\t\t<div class=\"help\">\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:window.history.back();\"><< Volver</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<h1 id=\"logo\"><a href=\""+enlaceLogo+"\"><em>BBVA Pruebas Autom\u00E1ticas</em></a></h1>\r\n\t\t\t\t\t\t<nav class=\"metanav\">\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t<li class=\"idioma\"></li>\r\n\t\t\t\t\t\t\t\t<li></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</nav>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"container\" id=\"operaciones_f\" >\r\n\t\t\t\t\t<div class=\"container_interior\">\r\n\t\t\t\t\t\t<a id=\"lupaFalsa\" title=\"buscador\" href=\"#\"></a>\r\n\t\t\t\t\t\t<a class=\"op_f\" href=\"datosEjecucion.html\">Datos ejecuci&oacute;n</a>\r\n\t\t\t\t\t\t<a class=\"op_f\" href=\"resumenEjecucion.html\">Resumen de ejecuci&oacute;n</a>\r\n\t\t\t\t\t\t<a class=\"op_f\" href=\"informeDetallado.html\">Informe detallado</a>\r\n\t\t\t\t\t\t<div class=\"hamb_icon\">\r\n\t\t\t\t\t\t\t<div id=\"nav-icon1Falso\">\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t\t<span class=\"mob_ile\"></span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<a href=\""+enlaceLogo+"\" class=\"connect\">Desconectar</a>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"container_search\" style=\"display:none\" >\r\n\t\t\t\t\t<div class=\"container_main\">\r\n\t\t\t\t\t\t<div class=\"searcher\">\r\n\t\t\t\t\t\t\t<a id=\"lupa_close\"  title=\"buscador\"></a>\r\n\t\t\t\t\t\t\t<input type=\"search\" class=\"search_hide\" value=\"Buscar movimientos, &oacute;rdenes, operativas, productos...\" onfocus=\"if(this.value == 'Buscar movimientos, &oacute;rdenes, operativas, productos...') { this.value = ''; }\"  />\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"hamb_icon\" id=\"search_close\">\r\n\t\t\t\t\t\t\t<div id=\"nav-icon2\" class=\"open\">\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div id=\"buscador_capa\" style=\"display:none\" >\r\n\t\t\t\t\t<div id=\"buscador_links\">\r\n\t\t\t\t\t\t<ul class=\"bl\">\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Quick links</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t<li><a href=\"#\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"#\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"#\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"#\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"#\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t<div class=\"no_find\">&iquest;No encuentras lo que buscas? llamanos al  <a href=\"#\">900 000 000</a> o m&aacute;ndanos un <a href=\"#\">mensaje</a>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<nav id=\"main_nav\" >\r\n\t\t\t\t\t<div id=\"menu_principal\">\r\n\t\t\t\t\t\t<div class=\"perfil_modulo\">\r\n\t\t\t\t\t\t\t<h4 class=\"clearfix\"><strong></strong><em>JuanPe</em></h4>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t<li class=\"idioma\"><a href=\"index_english.html\">Espa\u00F1ol</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div id=\"menu_principal_interior\">\r\n\t\t\t\t\t\t\t<div id=\"menu_principal_interior_blanco\">\r\n\t\t\t\t\t\t\t\t<div class=\"pestanas\">\r\n\t\t\t\t\t\t\t\t\t<nav class=\"tab-navigation fn-tabbednavigation\">\r\n\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\">\r\n\t\t\t\t\t\t\t\t\t\t\t<ul class=\"clearfix cl\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"selected fn-tabbednavigationitem\" href='#mis_productos' ><em>Mis productos</em></a></li>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"fn-tabbednavigationitem\" href='#operar'><em>Mis operaciones</em></a></li>\r\n\t\t\t\t\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t</nav>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>  \r\n\t\t\t\t\t\t</div>   \r\n\t\t\t\t\t\t<div class=\"menu_content\" >\r\n\t\t\t\t\t\t\t<ul class=\"mis_productos\" id=\"mis_productos\">\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Todos</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\""+enlaceLogo+"\">Situaci&oacute;n financiera</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Cuentas</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"index_cuenta.html\">*0000</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*2020</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*4040</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*6060</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"cuentas.html\">Ver todas</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Tarjetas</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*2222</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*5555</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*9999</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Ver todas</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Pr&eacute;stamos</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*1010</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*3030</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Ver todos</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Contratar</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Cuentas</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Tarjetas</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Pr&eacute;stamos</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Cuentas de cr&eacute;ditos</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Avales</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Leasing</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Ver todos</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t<ul class=\"operar\" id=\"operar\">\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Enviar dinero</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">N\u00F3minas</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"transferencia_ind.html\">Transferencias individuales</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"transferencia_mul.html\">Transferencias m\u00FAltiples</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Impuestos</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Cobrar dinero</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Domiciliaciones</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Recibos</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Ficheros</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"subida_ficheros.html\">Subida de ficheros</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Firmas</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Firma de \u00F3rdenes</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Contratar</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Cheques</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Confirming</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Informaci\u00F3n fiscal</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">TPVs</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Ver todos</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"botones_fixed\">\r\n\t\t\t\t\t\t<a href=\"#\"  class=\"ayuda\"><strong>Ayuda </strong></a>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</nav>\r\n\t\t\t</header>\r\n\t\t</div>\r\n\t\t<!-- FIN CABECERA AZUL -->\r\n\r\n\r\n\t\t<div class=\"scrollableArea\" id=\"mainScrollableArea\">\r\n\t\t\t<section class=\"main_section\"  id=\"cuenta_detail\">\r\n\t\t\t\t<section class=\"main_content\" id=\"time-now\" style=\"border-bottom:0px\" >\r\n\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t<!-- END TAB NAVEGATION   -->\r\n\t\t\t\t\t\t<div class=\"tabla three_col\" >\t\r\n\t\t\t\t\t\t\t<table class=\"tbody\" style=\"width:100%;min-width:100%\" >\r\n\t\t\t\t\t\t\t\t<tr class=\"theader fila\">\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t     <th class=\"celda firstth\"><strong class=\"ts\"><em>Resumen</em></strong></th>\r\n\t\t\t\t\t\t\t\t     <th class=\"celda secondth\"><strong><em>N&uacute;mero de casos</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda thirdth\"><strong><em class=\"tipo\">Porcentaje</em></strong></th>\r\n\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t"
				+ "<tr class=\"fila highlander ok\">\r\n\t\t\t\t\t\t\t\t\t "
				+ "<td class=\"celda first\" >"+literalSatisfactorios+"</td>\r\n\t\t\t\t\t\t\t\t\t "
				+ "<td class=\"celda second\">"+resultadosOK+"</td>\r\n\t\t\t\t\t\t\t\t\t "
				+ "<td class=\"celda third\">"+porcentajeOK+"%</td>\r\n\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t"
				+ "<tr class=\"fila highlander ko\">\r\n\t\t\t\t\t\t\t\t\t "
				+ "<td class=\"celda first\" >"+literalError+"</td>\r\n\t\t\t\t\t\t\t\t\t "
				+ "<td class=\"celda second\">"+resultadosERROR+"</td>\r\n\t\t\t\t\t\t\t\t\t "
				+ "<td class=\"celda third\">"+porcentajeERROR+"%</td>\r\n\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t"
				+ "<tr class=\"fila highlander warning\">\r\n\t\t\t\t\t\t\t\t\t "
				+ "<td class=\"celda first\" >"+literalAvisos+"</td>\r\n\t\t\t\t\t\t\t\t\t "
				+ "<td class=\"celda second\">"+resultadosWARNING+"</td>\r\n\t\t\t\t\t\t\t\t\t "
				+ "<td class=\"celda third\">"+porcentajeWARNING+"%</td>\r\n\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t"
				+ "<tr class=\"fila highlander black\" style=\"background: aliceblue;\">"
				+ "\r\n\t\t\t\t\t\t\t\t\t "
				+ "<td class=\"celda first\" ><b>Total</b></td>\r\n\t\t\t\t\t\t\t\t\t "
				+ "<td class=\"celda second\"><b>"+totalesINT+"</b></td>\r\n\t\t\t\t\t\t\t\t\t "
				+ "<td class=\"celda third\"><b>100%</b></td>\r\n\r\n\t\t\t\t\t\t\t\t</tr>"
				+ "</table>"+literalTrasReEjecucion
				+ "\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t</article>\r\n\t\t\t\t</section>\r\n\t\t\t</section>\r\n\t\t</div>\r\n\t</div>\r\n\t\r\n\t\r\n\r\n\r\n\t\r\n</body>\r\n</html>";


		// Volcamos este string a un fichero nuevo
		File file = new File("results/InformeHTML/resumenEjecucion.html");

		FileWriter fileHtm = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fileHtm);

		bw.write(contenidoResumenEjecucion);
		bw.close();
		
		}

		catch(IOException e)
		{
			utils.Log.writeException(e);
		}
		
		
		ArrayList<Integer> listaResultados = new ArrayList<Integer>();
		
		listaResultados.add(totalesINT);
		listaResultados.add(resultadosOK);
		listaResultados.add(resultadosERROR);
		listaResultados.add(resultadosWARNING);
		
		return listaResultados;
	}
	
	
	
	public static void generaInformeDetallado(int totales, int totalesOK, int totalesKO, int totalesWarning, List<String> lista) throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String archivoCSV = params.getValorPropiedad("rutaResultados");


		CsvReader reader = new CsvReader(new FileReader(archivoCSV));
		boolean titulos = true;
		
		//Cogemos el fichero C204
		String ficheroC204 = "";
		String text = "";
//		String dns = "http://146.148.15.138";
//		String dns = Constantes.dnsAzure;
		
		String generarFichero = params.getValorPropiedad("generarFichero").toLowerCase();
		
		if(generarFichero!=null && generarFichero.equalsIgnoreCase("SI"))
		{

			File temp = new File(System.getProperty("user.dir")+"/resources/temp.txt");

			if(temp.exists())
			
			{
				Log.write("[INFO] - CSV FILE: '"+temp.getAbsoluteFile()+"' FOUND");
				Scanner scanner = new Scanner(temp, "UTF-8");
				text = scanner.useDelimiter("\\A").next();
				ficheroC204 = "..resources/"+text;
				scanner.close();
				Log.write("[INFO] - CSV READ: "+ficheroC204);
			}
			
			else {
				
				Log.write("[INFO] - "+temp.getAbsoluteFile()+" NOT FOUND");
				
			}
			
		}
		
		
		String enlaceLogo = "javascript:void(0);";
		
		String charset = "windows-1252";
		
		if (Utils.isUnix())
			charset = "UTF-8";

		String htmlPrincipal = "\r\n<!DOCTYPE html>\r\n<!-- Formateador HTML5 IE -->\r\n<!--[if IE]>\r\n<script src=\"js/html5shiv.js\"></script>\r\n<![endif]-->\r\n<html  lang=\"es\" >\r\n<!--<![endif]-->\r\n<head>\r\n  <title>BBVA Pruebas Autom&aacute;ticas</title>\r\n <meta http-equiv=\"Content-Type\" content=\"text/html;\" /> <meta http-equiv=\"Content-Type\" content=\"image/jpg;\" /> <meta http-equiv=\"Content-Type\" content=\"image/jpeg;\" />  <meta charset=\""+charset+"\">\r\n  <meta name=\"robots\" content=\"noindex, nofollow\" />\r\n  <meta name=\"description\" content=\"\">\r\n  <meta name=\"HandheldFriendly\" content=\"True\">\r\n  <meta name=\"MobileOptimized\" content=\"320\">\r\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\" />\r\n  \r\n  \r\n  \r\n  <link rel=\"apple-touch-icon-precomposed\" sizes=\"100x100\" href=\"http://www.direction.es/clientes/bbva/directdebit/img/comun/icon_bbva.png\">\r\n  <link rel=\"stylesheet\" href=\"css/styles.css\" />\r\n  <link rel=\"stylesheet\" href=\"css/jquery-ui.min.css\" />\r\n  <script src=\"https://code.jquery.com/jquery-1.10.2.js\"  integrity=\"sha256-it5nQKHTz+34HijZJQkpNBIHsjpV8b6QzMJs9tmOBSo=\"  crossorigin=\"anonymous\"></script>\r\n <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script> <script type=\"text/javascript\"  src=\"js/jquery-ui.min.js\"></script>\r\n  <script type=\"text/javascript\"  src=\"js/jquery.placeholder.min.js\"></script>\r\n  <script src=\"https://code.highcharts.com/highcharts.js\"></script>\r\n  <script src=\"js/rounded-corners.js\"></script>\r\n  <script type=\"text/javascript\"  src=\"js/plugins.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/jquery.hammer.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/hammer.min.js\"></script>\r\n  <script src=\"js/modernizr-2.7.2.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/jquery.easydropdown.min.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/jquery.offcanvas.min.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/prefixfree.min.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/codigo.js\"></script>\r\n  <script type=\"text/javascript\" src=\"js/grafico2.js\"></script>\r\n  <link rel=\"stylesheet\" href=\"/resources/demos/style.css\">\r\n  <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>\r\n\r\n  \r\n\r\n <script type=\"text/javascript\">\r\nfunction getParameterByName(name) {\r\n\t\t name = name.replace(/[\\[]/, \"\\[\").replace(/[\\]]/, \"\\]\");\r\n\t\t var regex = new RegExp(\"[\\?&]\" + name +\"=([^&#]*)\");\r\n\t\t results = regex.exec(location.search);\r\n\t\t    return results === null ? \"\" : decodeURIComponent(results[1].replace(/\\+/g, \" \"));\r\n\t\t}\r\n\t\t$(document).ready(function() {\r\n    $('#time-now').show();\r\n    $('#msg').hide();\r\n\tvar codigo = getParameterByName('codigo');"
				+ "\r\n\t$(this).val(codigo);\r\n\t$(\"#search\").val(codigo);\r\n\t if (codigo != undefined && codigo != \"\"){ \r\n\t\t$(\"#tablaDetalle tbody>tr\").hide();\r\n\t\t$(\"#tablaDetalle td:contains-ci('\" + codigo + \"')\").parent(\"tr\").show();\r\n\t\t$(\"#tablaDetalle th:contains-ci('\" + codigo + \"')\").parent(\"tr\").show();\r\n\t\t$(\"#tablaDetalleKO tbody>tr\").hide();\r\n\t\t$(\"#tablaDetalleKO td:contains-ci('\" + $(this).val() + \"')\").parent(\"tr\").show();\r\n\t\t$(\"#tablaDetalleKO th:contains-ci('\" + codigo + \"')\").parent(\"tr\").show();\r\n\t\t$(\"#tablaDetalleOK tbody>tr\").hide();\r\n\t\t$(\"#tablaDetalleOK td:contains-ci('\" + codigo + \"')\").parent(\"tr\").show();\r\n\t\t$(\"#tablaDetalleOK th:contains-ci('\" + codigo + \"')\").parent(\"tr\").show();\r\n\t\t$(\"#tablaDetalleWARNING tbody>tr\").hide();\r\n\t\t$(\"#tablaDetalleWARNING td:contains-ci('\" + codigo + \"')\").parent(\"tr\").show();\r\n\t\t$(\"#tablaDetalleWARNING th:contains-ci('\" + codigo + \"')\").parent(\"tr\").show();\r\n\t}\r\n\t// Write on keyup event of keyword input element\r\n\t$(\"#search\").keyup(function(){\r\n\t\t// When value of the input is not blank\r\n\t\tif( $(this).val() != \"\")\r\n\t\t{\r\n\t\t\t// Show only matching TR, hide rest of them\r\n\t\t\t$(\"#tablaDetalle tbody>tr\").hide();\r\n\t\t\t$(\"#tablaDetalle td:contains-ci('\" + $(this).val() + \"')\").parent(\"tr\").show();\r\n\t\t\t$(\"#tablaDetalle th:contains-ci('\" + $(this).val() + \"')\").parent(\"tr\").show();\r\n\t\t\t\r\n\t\t\t$(\"#tablaDetalleKO tbody>tr\").hide();\r\n\t\t\t$(\"#tablaDetalleKO td:contains-ci('\" + $(this).val() + \"')\").parent(\"tr\").show();\r\n\t\t\t$(\"#tablaDetalleKO th:contains-ci('\" + $(this).val() + \"')\").parent(\"tr\").show();\r\n\t\t\t\r\n\t\t\t$(\"#tablaDetalleOK tbody>tr\").hide();\r\n\t\t\t$(\"#tablaDetalleOK td:contains-ci('\" + $(this).val() + \"')\").parent(\"tr\").show();\r\n\t\t\t$(\"#tablaDetalleOK th:contains-ci('\" + $(this).val() + \"')\").parent(\"tr\").show();\r\n\t\t\t\r\n\t\t\t$(\"#tablaDetalleWARNING tbody>tr\").hide();\r\n\t\t\t$(\"#tablaDetalleWARNING td:contains-ci('\" + $(this).val() + \"')\").parent(\"tr\").show();\r\n\t\t\t$(\"#tablaDetalleWARNING th:contains-ci('\" + $(this).val() + \"')\").parent(\"tr\").show();\r\n\t\t}\r\n\t\telse\r\n\t\t{\r\n\t\t\t// When there is no input or clean again, show everything back\r\n\t\t\t$(\"#tablaDetalle tbody>tr\").show();\r\n\t\t\t$(\"#tablaDetalleKO tbody>tr\").show();\r\n\t\t\t$(\"#tablaDetalleOK tbody>tr\").show();\r\n\t\t\t$(\"#tablaDetalleWARNING tbody>tr\").show();\r\n\t\t}\r\n\t});\r\n\r\n});\r\n</script>  \r\n<script>\r\n  $( function() {\r\n    $('.jqDialog').each(function(){\r\n\t\t$(this).dialog({\r\n\t\t autoOpen: false,\r\n\t\t});\r\n\t});\r\n  });\r\n  \r\n  \r\n  function mostrarDialog(id){\r\n\t$( \"#\"+ id ).dialog( \"open\" );\r\n  }\r\n  </script>\r\n  \r\n  \r\n</head>\r\n\r\n<body>\r\n\t<div id=\"principal\" class=\"layout-base\" >\r\n\t\t<!-- INICIO CABECERA AZUL -->\r\n\t\t<div class=\"header_fixed\">\r\n\t\t\t<header class=\"topBar\" id=\"logo_01\"  >\r\n\t\t\t\t<div class=\"elmarron\" >\r\n\t\t\t\t\t<div class=\"header_div\">\r\n\t\t\t\t\t\t<div class=\"help\">\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t<li><a href=\"javascript:window.history.back();\"><< Volver</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<h1 id=\"logo\"><a href=\"javascript:void(0);\"><em>BBVA Pruebas Autom\u00E1ticas</em></a></h1>\r\n\t\t\t\t\t\t<nav class=\"metanav\">\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t<li class=\"idioma\"></li>\r\n\t\t\t\t\t\t\t\t<li></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</nav>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"container\" id=\"operaciones_f\" >\r\n\t\t\t\t\t<div class=\"container_interior\">\r\n\t\t\t\t\t\t<a id=\"lupaFalsa\" title=\"buscador\" href=\"#\"></a>\r\n\t\t\t\t\t\t<a class=\"op_f\" href=\"datosEjecucion.html\">Datos ejecuci&oacute;n</a>\r\n\t\t\t\t\t\t<a class=\"op_f\" href=\"resumenEjecucion.html\">Resumen de ejecuci&oacute;n</a>\r\n\t\t\t\t\t\t<a class=\"op_f\" href=\"informeDetallado.html\">Informe detallado</a>\r\n\t\t\t\t\t\t<div class=\"hamb_icon\">\r\n\t\t\t\t\t\t\t<div id=\"nav-icon1Falso\">\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t\t<span class=\"mob_ile\"></span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<a href=\"javascript:void(0);\" class=\"connect\">Desconectar</a>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"container_search\" style=\"display:none\" >\r\n\t\t\t\t\t<div class=\"container_main\">\r\n\t\t\t\t\t\t<div class=\"searcher\">\r\n\t\t\t\t\t\t\t<a id=\"lupa_close\"  title=\"buscador\"></a>\r\n\t\t\t\t\t\t\t<input type=\"search\" class=\"search_hide\" value=\"Buscar movimientos, &oacute;rdenes, operativas, productos...\" onfocus=\"if(this.value == 'Buscar movimientos, &oacute;rdenes, operativas, productos...') { this.value = ''; }\"  />\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"hamb_icon\" id=\"search_close\">\r\n\t\t\t\t\t\t\t<div id=\"nav-icon2\" class=\"open\">\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t\t<span></span>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div id=\"buscador_capa\" style=\"display:none\" >\r\n\t\t\t\t\t<div id=\"buscador_links\">\r\n\t\t\t\t\t\t<ul class=\"bl\">\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Quick links</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t<li><a href=\"#\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"#\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"#\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"#\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</a></li>\r\n\t\t\t\t\t\t\t\t<li><a href=\"#\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t<div class=\"no_find\">&iquest;No encuentras lo que buscas? llamanos al  <a href=\"#\">900 000 000</a> o m&aacute;ndanos un <a href=\"#\">mensaje</a>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<nav id=\"main_nav\" >\r\n\t\t\t\t\t<div id=\"menu_principal\">\r\n\t\t\t\t\t\t<div class=\"perfil_modulo\">\r\n\t\t\t\t\t\t\t<h4 class=\"clearfix\"><strong></strong><em>JuanPe</em></h4>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t\t<li class=\"idioma\"><a href=\"index_english.html\">Espa\u00F1ol</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div id=\"menu_principal_interior\">\r\n\t\t\t\t\t\t\t<div id=\"menu_principal_interior_blanco\">\r\n\t\t\t\t\t\t\t\t<div class=\"pestanas\">\r\n\t\t\t\t\t\t\t\t\t<nav class=\"tab-navigation fn-tabbednavigation\">\r\n\t\t\t\t\t\t\t\t\t\t<div class=\"clearfix\">\r\n\t\t\t\t\t\t\t\t\t\t\t<ul class=\"clearfix cl\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"selected fn-tabbednavigationitem\" href='#mis_productos' ><em>Mis productos</em></a></li>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<li><a class=\"fn-tabbednavigationitem\" href='#operar'><em>Mis operaciones</em></a></li>\r\n\t\t\t\t\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t</nav>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>  \r\n\t\t\t\t\t\t</div>   \r\n\t\t\t\t\t\t<div class=\"menu_content\" >\r\n\t\t\t\t\t\t\t<ul class=\"mis_productos\" id=\"mis_productos\">\r\n\t\t\t\t\t\t\t<li class=\"li_element\">"
				+ "<strong>Todos</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\""+enlaceLogo+"\">Situaci&oacute;n financiera</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Cuentas</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"index_cuenta.html\">*0000</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*2020</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*4040</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*6060</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"cuentas.html\">Ver todas</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Tarjetas</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*2222</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*5555</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*9999</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Ver todas</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Pr&eacute;stamos</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*1010</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">*3030</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Ver todos</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Contratar</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Cuentas</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Tarjetas</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Pr&eacute;stamos</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Cuentas de cr&eacute;ditos</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Avales</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Leasing</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Ver todos</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t<ul class=\"operar\" id=\"operar\">\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Enviar dinero</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">N\u00F3minas</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"transferencia_ind.html\">Transferencias individuales</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"transferencia_mul.html\">Transferencias m\u00FAltiples</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Impuestos</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Cobrar dinero</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Domiciliaciones</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Recibos</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Ficheros</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"subida_ficheros.html\">Subida de ficheros</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Firmas</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Firma de \u00F3rdenes</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t<li class=\"li_element\"><strong>Contratar</strong>\r\n\t\t\t\t\t\t\t<ul>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Cheques</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Confirming</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Informaci\u00F3n fiscal</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">TPVs</a></li>\r\n\t\t\t\t\t\t\t<li><a href=\"#\">Ver todos</a></li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t</li>\r\n\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"botones_fixed\">\r\n\t\t\t\t\t\t<a href=\"#\"  class=\"ayuda\"><strong>Ayuda </strong></a>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</nav>\r\n\t\t\t</header>\r\n\t\t</div>\r\n\t\t<!-- FIN CABECERA AZUL -->\r\n\r\n\r\n\t\t<div class=\"scrollableArea\" id=\"mainScrollableArea\">\r\n\t\t\t<section class=\"main_section\"  id=\"cuenta_detail\">\r\n\t\t\t\t<section class=\"main_content\" id=\"time-now\" style=\"display:none;\">\r\n\t\t\t\t\t<article class=\"with_table data_tab w_d_margin\" id=\"cuenta_detalle_tab02\">\r\n\t\t\t\t\t\t<header class=\"article_header\" id=\"mov_extracto\" >\r\n\t\t\t\t\t\t\t<h3 class=\"nob\"></h3>\r\n\t\t\t\t\t\t\t<div class=\"cuenta_sel_ah\"> \r\n\t\t\t\t\t\t\t\t<a href=\""+ficheroC204+"\" target=\"_blank\" download=\""+text+"\" class=\"button\" >Exportar C-204</a>\r\n\t\t\t\t\t\t\t\t<input id=\"search\" type=\"search\" class=\"search\" value=\"Buscar caso, prioridad, resultado...\" onfocus=\"if(this.value == 'Buscar caso, prioridad, resultado...') { this.value = ''; }\"/>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</header>\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t<!-- TAB NAVEGATION   -->\r\n\t\t\t\t\t\t<div class=\"pestanas\">\r\n\t\t\t\t\t\t\t<nav class=\"tab-navigation fn-tabbednavigation\">\r\n\t\t\t\t\t\t\t\t<div class=\"clearfix\">\r\n\t\t\t\t\t\t\t\t\t<ul class=\"clearfix\">\r\n\t\t\t\t\t\t\t\t\t\t<li><a href='#todos' class=\"selected fn-tabbednavigationitem\"><h3 class=\"nob\">Todos <span>("+totales+")</span></h3></a></li>\r\n\t\t\t\t\t\t\t\t\t\t<li><a  href='#OK' class=\"notselected fn-tabbednavigationitem\"><h3 class=\"nob\">OK <span>("+totalesOK+")</span></h3></a></li>\r\n\t\t\t\t\t\t\t\t\t\t<li><a  href='#KO' class=\"notselected fn-tabbednavigationitem\"><h3 class=\"nob\">KO <span>("+totalesKO+")</span></h3></a></li>\r\n\t\t\t\t\t\t\t\t\t\t<li><a  href='#WARNING' class=\"notselected fn-tabbednavigationitem\"><h3 class=\"nob\">Warning <span>("+totalesWarning+")</span></h3></a></li>\r\n\t\t\t\t\t\t\t\t\t</ul>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</nav>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t<!-- END TAB NAVEGATION   -->\r\n\t\t\t\t\t\t"
				+ "<div class=\"tabla six_col\" id=\"todos\">\t\r\n\t\t\t\t\t\t\t<table class=\"tbody\" id=\"tablaDetalle\">\r\n\t\t\t\t\t\t\t\t<tr class=\"theader fila\">\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t     <th class=\"celda firstth\"><strong class=\"ts\"><em>Codigo</em></strong></th>\r\n\t\t\t\t\t\t\t\t     <th class=\"celda secondth\"><strong><em>Caso</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda thirdth\" style= \"padding-left: 100px;\"><strong><em class=\"tipo\">Prioridad</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda fourth\"><strong><em class=\"estado\">Comentario</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda thirdth\"><strong><em>Captura</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda fiveth\"><strong><em>Log</em></strong></th>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t";
	//			+ "<tr class=\"fila highlander ok\">\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda first\" data-label=\"Creaci\u00F3n\">RPT_001</td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda second\" data-label=\"Identificador\" onclick=\"mostrarDialog('dialog_002')\"><a href=\"#\">Customer Login (contract ID + user ID + password)</a></td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda third\" data-label=\"Prioridad\"><a id=\"priority_normal\"  title=\"normal\"></a>Normal</td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda fourth\" data-label=\"Identificador\">Access successful to HelpDesk and select Company</td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda third third_none\" data-label=\"Captura\"><a id=\"screenshots\" title=\"screenshots\" href=\"http://146.148.15.138/testing/DISPRE_13032017_161219/screenshots/ComprobarLogin_13_03_2017_16_1_6.png\" target=\"_blank\"></a></td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda fiveth\" data-label=\"Log\"><a id=\"ver_informe\" title=\"ver_informe\" href=\"#\"></a></td>\r\n\t\t\t\t\t\t\t\t\t<div id=\"dialog_002\" class=\"jqDialog\" title=\"Basic dialog\" >\r\n\t\t\t\t\t\t\t\t\t  <p>222222222alog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\r\n\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t<div class=\"tabla six_col\" id=\"KO\">\t\r\n\t\t\t\t\t\t\t<table class=\"tbody\">\r\n\t\t\t\t\t\t\t\t<tr class=\"theader fila\">\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t     <th class=\"celda firstth\"><strong class=\"ts\"><em>Codigo</em></strong></th>\r\n\t\t\t\t\t\t\t\t     <th class=\"celda secondth\"><strong><em>Caso</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda thirdth\" style= \"padding-left: 100px;\"><strong><em class=\"tipo\">Prioridad</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda fourth\"><strong><em class=\"estado\">Comentario</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda thirdth\"><strong><em>Captura</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda fiveth\"><strong><em>Log</em></strong></th>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t<tr class=\"fila highlander ko\">\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda first\" data-label=\"Creaci\u00F3n\">RPT_001</td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda second\" data-label=\"Identificador\" onclick=\"mostrarDialog('dialog_002')\"><a href=\"#\">Customer Login (contract ID + user ID + password)</a></td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda third\" data-label=\"Prioridad\"><a id=\"priority_normal\"  title=\"normal\"></a>Normal</td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda fourth\" data-label=\"Identificador\">Access successful to HelpDesk and select Company</td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda third third_none\" data-label=\"Captura\"><a id=\"screenshots\" title=\"screenshots\" href=\"http://146.148.15.138/testing/DISPRE_13032017_161219/screenshots/ComprobarLogin_13_03_2017_16_1_6.png\" target=\"_blank\"></a></td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda fiveth\" data-label=\"Log\"><a id=\"ver_informe\" title=\"ver_informe\" href=\"#\"></a></td>\r\n\t\t\t\t\t\t\t\t\t<div id=\"dialog_002\" class=\"jqDialog\" title=\"Basic dialog\" >\r\n\t\t\t\t\t\t\t\t\t  <p>222222222alog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t<div class=\"tabla six_col\" id=\"OK\">\t\r\n\t\t\t\t\t\t\t<table class=\"tbody\">\r\n\t\t\t\t\t\t\t\t<tr class=\"theader fila\">\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t     <th class=\"celda firstth\"><strong class=\"ts\"><em>Codigo</em></strong></th>\r\n\t\t\t\t\t\t\t\t     <th class=\"celda secondth\"><strong><em>Caso</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda thirdth\" style= \"padding-left: 100px;\"><strong><em class=\"tipo\">Prioridad</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda fourth\"><strong><em class=\"estado\">Comentario</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda thirdth\"><strong><em>Captura</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda fiveth\"><strong><em>Log</em></strong></th>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t<tr class=\"fila highlander ok\">\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda first\" data-label=\"Creaci\u00F3n\">RPT_001</td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda second\" data-label=\"Identificador\" onclick=\"mostrarDialog('dialog_002')\"><a href=\"#\">Customer Login (contract ID + user ID + password)</a></td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda third\" data-label=\"Prioridad\"><a id=\"priority_normal\"  title=\"normal\"></a>Normal</td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda fourth\" data-label=\"Identificador\">Access successful to HelpDesk and select Company</td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda third third_none\" data-label=\"Captura\"><a id=\"screenshots\" title=\"screenshots\" href=\"http://146.148.15.138/testing/DISPRE_13032017_161219/screenshots/ComprobarLogin_13_03_2017_16_1_6.png\" target=\"_blank\"></a></td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda fiveth\" data-label=\"Log\"><a id=\"ver_informe\" title=\"ver_informe\" href=\"#\"></a></td>\r\n\t\t\t\t\t\t\t\t\t<div id=\"dialog_002\" class=\"jqDialog\" title=\"Basic dialog\" >\r\n\t\t\t\t\t\t\t\t\t  <p>222222222alog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t"
	//+ "<div class=\"tabla six_col\" id=\"WARNING\">\t\r\n\t\t\t\t\t\t\t<table class=\"tbody\">\r\n\t\t\t\t\t\t\t\t<tr class=\"theader fila\">\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t     <th class=\"celda firstth\"><strong class=\"ts\"><em>Codigo</em></strong></th>\r\n\t\t\t\t\t\t\t\t     <th class=\"celda secondth\"><strong><em>Caso</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda thirdth\" style= \"padding-left: 100px;\"><strong><em class=\"tipo\">Prioridad</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda fourth\"><strong><em class=\"estado\">Comentario</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda thirdth\"><strong><em>Captura</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda fiveth\"><strong><em>Log</em></strong></th>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t<tr class=\"fila highlander warning\">\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda first\" data-label=\"Creaci\u00F3n\">RPT_001</td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda second\" data-label=\"Identificador\" onclick=\"mostrarDialog('dialog_002')\"><a href=\"#\">Customer Login (contract ID + user ID + password)</a></td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda third\" data-label=\"Prioridad\"><a id=\"priority_normal\"  title=\"normal\"></a>Normal</td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda fourth\" data-label=\"Identificador\">Access successful to HelpDesk and select Company</td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda third third_none\" data-label=\"Captura\"><a id=\"screenshots\" title=\"screenshots\" href=\"http://146.148.15.138/testing/DISPRE_13032017_161219/screenshots/ComprobarLogin_13_03_2017_16_1_6.png\" target=\"_blank\"></a></td>\r\n\t\t\t\t\t\t\t\t\t <td class=\"celda fiveth\" data-label=\"Log\"><a id=\"ver_informe\" title=\"ver_informe\" href=\"#\"></a></td>\r\n\t\t\t\t\t\t\t\t\t<div id=\"dialog_002\" class=\"jqDialog\" title=\"Basic dialog\" >\r\n\t\t\t\t\t\t\t\t\t  <p>222222222alog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t"


		
		String tablaTotales = "";
		

		int i = 0;
		
		while(reader.readRecord())
			{
				String id = reader.get(0);
				String testName = reader.get(1);
				String resultado = reader.get(2);
				String comentario = reader.get(3);
				String captura = reader.get(4);

				if(lista != null)
				{
					for(int k = 0; k < lista.size(); ++k)
					{
						if(lista.get(k).indexOf(testName) > -1)
						{
							resultado = resultado + "*";
						}
					}
				}

				// File ficheroDestino = new File("results/"+testName+".txt");
				// String txt = ficheroDestino.getAbsolutePath();

				String txt = testName + ".html";

				if(titulos)
					titulos = false;
				else
					tablaTotales += "<tr class=\"fila highlander "+resultado.trim().toLowerCase().replace("*", "")+"\">"
								+ "<td class=\"celda first\" data-label=\"Creaci�n\">"+Utils.cortarPalabraLargaPorTamano(id,10)+"</td>"
								+ "<td class=\"celda second\" data-label=\"Identificador\" onclick=\"mostrarDialog('dialog_"+i+"')\"><a href=\"javascript:void(0);\">"+cortarPalabraLargaPorTamanoPuntosSuspensivos(Utils.devuelveDatoDocumentacion(id, "Caso", testName),133)+"</a></td>"
								+ "<td class=\"celda third\" data-label=\"Prioridad\"><a id=\"priority_high\"  title=\"High\"></a>"+Utils.devuelveDatoDocumentacion(id, "Prioridad", "")+"</td>"
								+ "<td class=\"celda fourth\" data-label=\"Identificador\">"+Utils.cortarComentario(comentario)+"</td>"
								+ "<td class=\"celda third third_none\" data-label=\"Captura\"><a id=\"screenshots\" title=\"screenshots\" onclick=\"mostrarDialog('dialogCaptura_"+i+"')\" href=\"#\"></a></td>"
								+ "<td class=\"celda fiveth\" data-label=\"Log\"><a id=\"ver_informe\" title=\"ver_informe\" href=\"../"+txt+"\" target=\"_blank\"></a></td>"
								+ "<div id=\"dialog_"+i+"\" class=\"jqDialog\" title=\"Basic dialog\" >"
								+ "<div id=\"descripcion\"><p style=\"line-height: normal;\"><b>"+Utils.devuelveDatoDocumentacion(id, "Caso", testName)+"</b></div>"
								+ "<div id=\"pasos\" style=\"background-color: #eef7fa;padding-bottom: 1px;border-radius: 7px;\"><p style=\"line-height: normal;padding-left: 17px;\"><b>Pasos:</b><br/><br/>"
								+ Utils.devuelveDatoDocumentacion(id, "Pasos", "")+"</p></div>"
								+ "<div id=\"resultados\" style=\"background-color: #eef7fa;padding-bottom: 1px;border-radius: 7px;\"><p style=\"line-height: normal;padding-left: 17px;\"><b>Resultado Esperado: </b><br/><br/>"
								+ Utils.devuelveDatoDocumentacion(id, "ResultadoEsperado", "")
								+ "</p></div></div>"
								+ "<div id=\"dialogCaptura_"+i+"\" class=\"jqDialog\" title=\"Basic dialog\" >"
								+ "<div id=\"imagenCaptura\" style=\"margin-bottom: 1%; text-align: center;\">"
								+ "<img src=\"../../screenshots/"+Utils.nombreFichero(captura)+"\" style=\" width: 100%;\"></div>"
								+ "</div></tr>";
												
				i++;
			}

			htmlPrincipal += tablaTotales+"\n</table>\n</div>";
			
			//Tabla de KO
			
			String tablaKOs = "";
			
			htmlPrincipal += "<div class=\"tabla six_col\" id=\"KO\">\t\r\n\t\t\t\t\t\t\t<table class=\"tbody\" id=\"tablaDetalleKO\">\r\n\t\t\t\t\t\t\t\t<tr class=\"theader fila\">\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t     <th class=\"celda firstth\"><strong class=\"ts\"><em>Codigo</em></strong></th>\r\n\t\t\t\t\t\t\t\t     <th class=\"celda secondth\"><strong><em>Caso</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda thirdth\" style= \"padding-left: 100px;\"><strong><em class=\"tipo\">Prioridad</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda fourth\"><strong><em class=\"estado\">Comentario</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda thirdth\"><strong><em>Captura</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda fiveth\"><strong><em>Log</em></strong></th>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t";
			
			reader = new CsvReader(new FileReader(archivoCSV));
			titulos = true;
			
						
			while(reader.readRecord())
			{
				String id = reader.get(0);
				String testName = reader.get(1);
				String resultado = reader.get(2);
				String comentario = reader.get(3);
				String captura = reader.get(4);

				if(lista != null)
				{
					for(int k = 0; k < lista.size(); ++k)
					{
						if(lista.get(k).indexOf(testName) > -1)
						{
							resultado = resultado + "*";
						}
					}
				}

				// File ficheroDestino = new File("results/"+testName+".txt");
				// String txt = ficheroDestino.getAbsolutePath();

				String txt = testName + ".html";

				if(titulos)
					titulos = false;
				else
					if (resultado.indexOf("ERROR")>-1)
						tablaKOs += "<tr class=\"fila highlander ko\">"
								+ "<td class=\"celda first\" data-label=\"Creaci�n\">"+Utils.cortarPalabraLargaPorTamano(id,8)+"</td>"
								+ "<td class=\"celda second\" data-label=\"Identificador\" onclick=\"mostrarDialog('dialog_"+i+"')\"><a href=\"javascript:void(0);\">"+cortarPalabraLargaPorTamanoPuntosSuspensivos(Utils.devuelveDatoDocumentacion(id, "Caso", testName),133)+"</a></td>"
								+ "<td class=\"celda third\" data-label=\"Prioridad\"><a id=\"priority_high\"  title=\"High\"></a>"+Utils.devuelveDatoDocumentacion(id, "Prioridad", "")+"</td>"
								+ "<td class=\"celda fourth\" data-label=\"Identificador\">"+Utils.cortarComentario(comentario)+"</td>"
								+ "<td class=\"celda third third_none\" data-label=\"Captura\"><a id=\"screenshots\" title=\"screenshots\" onclick=\"mostrarDialog('dialogCaptura_"+i+"')\" href=\"#\"></a></td>"
								+ "<td class=\"celda fiveth\" data-label=\"Log\"><a id=\"ver_informe\" title=\"ver_informe\" href=\"../"+txt+"\" target=\"_blank\"></a></td>"
								+ "<div id=\"dialog_"+i+"\" class=\"jqDialog\" title=\"Basic dialog\" >"
								+ "<div id=\"descripcion\"><p style=\"line-height: normal;\"><b>"+Utils.devuelveDatoDocumentacion(id, "Caso", testName)+"</b></div>"
								+ "<div id=\"pasos\" style=\"background-color: #eef7fa;padding-bottom: 1px;border-radius: 7px;\"><p style=\"line-height: normal;padding-left: 17px;\"><b>Pasos:</b><br/><br/>"
								+ Utils.devuelveDatoDocumentacion(id, "Pasos", "")+"</p></div>"
								+ "<div id=\"resultados\" style=\"background-color: #eef7fa;padding-bottom: 1px;border-radius: 7px;\"><p style=\"line-height: normal;padding-left: 17px;\"><b>Resultado Esperado: </b><br/><br/>"
								+ Utils.devuelveDatoDocumentacion(id, "ResultadoEsperado", "")
								+ "</p></div></div>"
								+ "<div id=\"dialogCaptura_"+i+"\" class=\"jqDialog\" title=\"Basic dialog\" >"
								+ "<div id=\"imagenCaptura\" style=\"margin-bottom: 1%; text-align: center;\">"
								+ "<img src=\"../../screenshots/"+Utils.nombreFichero(captura)+"\" style=\" width: 100%;\"></div>"
								+ "</div></tr>";
												
				i++;
			}

		htmlPrincipal += tablaKOs;
			
		htmlPrincipal += "</table></div>";
		
		
		
		
		//Tabla OKs
		
		String tablaOKs = "";
		
		htmlPrincipal += "<div class=\"tabla six_col\" id=\"OK\">\t\r\n\t\t\t\t\t\t\t<table class=\"tbody\"id=\"tablaDetalleOK\">\r\n\t\t\t\t\t\t\t\t<tr class=\"theader fila\">\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t     <th class=\"celda firstth\"><strong class=\"ts\"><em>Codigo</em></strong></th>\r\n\t\t\t\t\t\t\t\t     <th class=\"celda secondth\"><strong><em>Caso</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda thirdth\" style= \"padding-left: 100px;\"><strong><em class=\"tipo\">Prioridad</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda fourth\"><strong><em class=\"estado\">Comentario</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda thirdth\"><strong><em>Captura</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda fiveth\"><strong><em>Log</em></strong></th>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t";
		
		reader = new CsvReader(new FileReader(archivoCSV));
		titulos = true;
		
		while(reader.readRecord())
		{
			String id = reader.get(0);
			String testName = reader.get(1);
			String resultado = reader.get(2);
			String comentario = reader.get(3);
			String captura = reader.get(4);

			if(lista != null)
			{
				for(int k = 0; k < lista.size(); ++k)
				{
					if(lista.get(k).indexOf(testName) > -1)
					{
						resultado = resultado + "*";
					}
				}
			}


			String txt = testName + ".html";

			if(titulos)
				titulos = false;
			else
				if (resultado.indexOf("OK")>-1)
					tablaOKs += "<tr class=\"fila highlander ok\">"
							+ "<td class=\"celda first\" data-label=\"Creaci�n\">"+Utils.cortarPalabraLargaPorTamano(id,8)+"</td>"
							+ "<td class=\"celda second\" data-label=\"Identificador\" onclick=\"mostrarDialog('dialog_"+i+"')\"><a href=\"javascript:void(0);\">"+cortarPalabraLargaPorTamanoPuntosSuspensivos(Utils.devuelveDatoDocumentacion(id, "Caso", testName),133)+"</a></td>"
							+ "<td class=\"celda third\" data-label=\"Prioridad\"><a id=\"priority_high\"  title=\"High\"></a>"+Utils.devuelveDatoDocumentacion(id, "Prioridad", "")+"</td>"
							+ "<td class=\"celda fourth\" data-label=\"Identificador\">"+Utils.cortarComentario(comentario)+"</td>"
							+ "<td class=\"celda third third_none\" data-label=\"Captura\"><a id=\"screenshots\" title=\"screenshots\" onclick=\"mostrarDialog('dialogCaptura_"+i+"')\" href=\"#\"></a></td>"
							+ "<td class=\"celda fiveth\" data-label=\"Log\"><a id=\"ver_informe\" title=\"ver_informe\" href=\"../"+txt+"\" target=\"_blank\"></a></td>"
							+ "<div id=\"dialog_"+i+"\" class=\"jqDialog\" title=\"Basic dialog\" >"
							+ "<div id=\"descripcion\"><p style=\"line-height: normal;\"><b>"+Utils.devuelveDatoDocumentacion(id, "Caso", testName)+"</b></div>"
							+ "<div id=\"pasos\" style=\"background-color: #eef7fa;padding-bottom: 1px;border-radius: 7px;\"><p style=\"line-height: normal;padding-left: 17px;\"><b>Pasos:</b><br/><br/>"
							+ Utils.devuelveDatoDocumentacion(id, "Pasos", "")+"</p></div>"
							+ "<div id=\"resultados\" style=\"background-color: #eef7fa;padding-bottom: 1px;border-radius: 7px;\"><p style=\"line-height: normal;padding-left: 17px;\"><b>Resultado Esperado: </b><br/><br/>"
							+ Utils.devuelveDatoDocumentacion(id, "ResultadoEsperado", "")
							+ "</p></div></div>"
							+ "<div id=\"dialogCaptura_"+i+"\" class=\"jqDialog\" title=\"Basic dialog\" >"
							+ "<div id=\"imagenCaptura\" style=\"margin-bottom: 1%; text-align: center;\">"
							+ "<img src=\"../../screenshots/"+Utils.nombreFichero(captura)+"\" style=\" width: 100%;\"></div>"
							+ "</div></tr>";
											
			i++;
		}

		htmlPrincipal += tablaOKs;
		
		htmlPrincipal += "</table></div>";
		
		
		
		//Tabla de Warnings
		
		String tablaWarnings = "";
		
		htmlPrincipal += "<div class=\"tabla six_col\" id=\"WARNING\">\t\r\n\t\t\t\t\t\t\t<table class=\"tbody\" id=\"tablaDetalleWARNING\">\r\n\t\t\t\t\t\t\t\t<tr class=\"theader fila\">\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t     <th class=\"celda firstth\"><strong class=\"ts\"><em>Codigo</em></strong></th>\r\n\t\t\t\t\t\t\t\t     <th class=\"celda secondth\"><strong><em>Caso</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda thirdth\" style= \"padding-left: 100px;\"><strong><em class=\"tipo\">Prioridad</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda fourth\"><strong><em class=\"estado\">Comentario</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda thirdth\"><strong><em>Captura</em></strong></th>\r\n\t\t\t\t\t\t\t\t\t <th class=\"celda fiveth\"><strong><em>Log</em></strong></th>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t";
		
		reader = new CsvReader(new FileReader(archivoCSV));
		titulos = true;
		
		while(reader.readRecord())
		{
			String id = reader.get(0);
			String testName = reader.get(1);
			String resultado = reader.get(2);
			String comentario = reader.get(3);
			String captura = reader.get(4);

			if(lista != null)
			{
				for(int k = 0; k < lista.size(); ++k)
				{
					if(lista.get(k).indexOf(testName) > -1)
					{
						resultado = resultado + "*";
					}
				}
			}

			// File ficheroDestino = new File("results/"+testName+".txt");
			// String txt = ficheroDestino.getAbsolutePath();

			String txt = testName + ".html";

			if(titulos)
				titulos = false;
			else
				if (resultado.indexOf("WARNING")>-1)
					tablaWarnings += "<tr class=\"fila highlander warning\">"
							+ "<td class=\"celda first\" data-label=\"Creaci�n\">"+Utils.cortarPalabraLargaPorTamano(id,8)+"</td>"
							+ "<td class=\"celda second\" data-label=\"Identificador\" onclick=\"mostrarDialog('dialog_"+i+"')\"><a href=\"javascript:void(0);\">"+cortarPalabraLargaPorTamanoPuntosSuspensivos(Utils.devuelveDatoDocumentacion(id, "Caso", testName),133)+"</a></td>"
							+ "<td class=\"celda third\" data-label=\"Prioridad\"><a id=\"priority_high\"  title=\"High\"></a>"+Utils.devuelveDatoDocumentacion(id, "Prioridad", "")+"</td>"
							+ "<td class=\"celda fourth\" data-label=\"Identificador\">"+Utils.cortarComentario(comentario)+"</td>"
							+ "<td class=\"celda third third_none\" data-label=\"Captura\"><a id=\"screenshots\" title=\"screenshots\" onclick=\"mostrarDialog('dialogCaptura_"+i+"')\" href=\"#\"></a></td>"
							+ "<td class=\"celda fiveth\" data-label=\"Log\"><a id=\"ver_informe\" title=\"ver_informe\" href=\"../"+txt+"\" target=\"_blank\"></a></td>"
							+ "<div id=\"dialog_"+i+"\" class=\"jqDialog\" title=\"Basic dialog\" >"
							+ "<div id=\"descripcion\"><p style=\"line-height: normal;\"><b>"+Utils.devuelveDatoDocumentacion(id, "Caso", testName)+"</b></div>"
							+ "<div id=\"pasos\" style=\"background-color: #eef7fa;padding-bottom: 1px;border-radius: 7px;\"><p style=\"line-height: normal;padding-left: 17px;\"><b>Pasos:</b><br/><br/>"
							+ Utils.devuelveDatoDocumentacion(id, "Pasos", "")+"</p></div>"
							+ "<div id=\"resultados\" style=\"background-color: #eef7fa;padding-bottom: 1px;border-radius: 7px;\"><p style=\"line-height: normal;padding-left: 17px;\"><b>Resultado Esperado: </b><br/><br/>"
							+ Utils.devuelveDatoDocumentacion(id, "ResultadoEsperado", "")
							+ "</p></div></div>"
							+ "<div id=\"dialogCaptura_"+i+"\" class=\"jqDialog\" title=\"Basic dialog\" >"
							+ "<div id=\"imagenCaptura\" style=\"margin-bottom: 1%; text-align: center;\">"
							+ "<img src=\"../../screenshots/"+Utils.nombreFichero(captura)+"\" style=\" width: 100%;\"></div>"
							+ "</div></tr>";
											
			i++;
		}

		htmlPrincipal += tablaWarnings;
		
		htmlPrincipal += "</table></div>";
		
		

		
		htmlPrincipal += "</article>\r\n\t\t\t\t"
				+ "</section>\r\n\t\t\t</section>\r\n\t\t</div>\r\n\t</div>\r\n\t\r\n</body>\r\n</html>"
				+ "</tbody></table>";

		// Volcamos este string a un fichero nuevo
		File file = new File("results/InformeHTML/informeDetallado.html");

		FileWriter fileHtm = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fileHtm);

		bw.write(htmlPrincipal);
		bw.close();

	}

	
	public static void esperaHastaDesapareceSinSalirTiempo(WebDriver driver, String xpath, int timeOut) throws Exception
	{
		int contador = 0;
		boolean desaparece = false;
		while(contador < timeOut)
		{
			if(Utils.isDisplayed(driver, xpath, 1))
			{
				contador++;
				Thread.sleep(1000);
			}
			else
			{
				desaparece = true;
				break;
			}
		}
		if(!desaparece)
			Log.write("TimeOut de "+timeOut+" segundos esperando a " + xpath);
	}
	
	public static void InsertarEjecucionCSVCloud(String archivo, String Pais, String Entorno,String Navegador,String Fecha,String HoraInicio, String HoraFin, String CPTotal, String CPOK, String CPError, String CPWarning,String codigoFicheroCSV, String codigoFicheroHTML, String codigoFicheroC204,String UUAAs, String loggedUser) throws Exception
	{

		String outputFile = archivo;


		// vemos si el fichero ya existe
		boolean alreadyExists = new File(outputFile).exists();

		try
		{
			// usamos FileWriter indicando que es para append
			CsvWriter writer = new CsvWriter(new FileWriter(outputFile, true), ',');

			// Si el fichero no existe, lo creamos con las cabeceras oportunas
			if(!alreadyExists)
			{
				writer.write("Pais");
				writer.write("Entorno");
				writer.write("Navegador");
				writer.write("Fecha");
				writer.write("Hora inicio");
				writer.write("Hora fin");
				writer.write("Ejecutados");
				writer.write("OK");
				writer.write("Error");
				writer.write("Avisos");
				writer.write("CSV");
				writer.write("HTML completo");
				writer.write("C-204");
				writer.write("UUAAs");
				writer.write("Usuario");
				writer.write("Orden Fecha");
				writer.write("Orden Hora");
				writer.write("Duracion");
				writer.endRecord();
			}
			
			
			
			Path path = Paths.get(archivo);
			List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);

			int position = 1;
			String extraLine = Pais+","+ Entorno+","+ Navegador+","+ Fecha+","+ HoraInicio+","+ HoraFin+","+ CPTotal+","+ CPOK+","+ CPError+","+ CPWarning+","+ codigoFicheroCSV+","+ codigoFicheroHTML+","+ codigoFicheroC204+","+ UUAAs+","+loggedUser;  

			lines.add(position, extraLine);
			Files.write(path, lines, StandardCharsets.UTF_8);


			
//			reader.readHeaders();
//			
//			
//
//			writer.write(Pais);
//			writer.write(Entorno);
//			writer.write(Navegador); 
//			writer.write(Fecha);
//			writer.write(HoraInicio);
//			writer.write(HoraFin);
//			writer.write(CPTotal);
//			writer.write(CPOK);
//			writer.write(CPError); 
//			writer.write(CPWarning);
//			writer.write(codigoFicheroCSV);
//			writer.write(codigoFicheroHTML);
//			writer.write(codigoFicheroC204);
//			writer.write(UUAAs);
//			writer.write(loggedUser);
//			writer.endRecord();
//			writer.close();

		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
		}


	}
	
	
	public static void esperaHastaApareceSinSalir(WebDriver driver, String xpath, int tiempo) throws Exception
	{

		try
		{
			boolean encontrado = Utils.isDisplayed(driver, xpath, 1);
			int contador = 0;
			
			while (!encontrado && contador<tiempo) {
				Thread.sleep(1000);
				encontrado = Utils.isDisplayed(driver, xpath, 1);
				contador++;
			}

		}
		catch(Exception e)
		{

		}

	}

	public static void esperaHastaEnabled(WebDriver driver, String xpath, int tiempo) throws Exception
	{

		try
		{
			boolean encontrado = Utils.isDisplayed(driver, xpath, 1) && Utils.isEnabled(driver, xpath, 1);
			int contador = 0;
			
			while (!encontrado && contador<tiempo) {
				Thread.sleep(1000);
				encontrado = Utils.isDisplayed(driver, xpath, 1) && Utils.isEnabled(driver, xpath, 1);
				contador++;
			}

		}
		catch(Exception e)
		{

		}

	}

	public static void esperaHastaAparece(WebDriver driver, String xpath, int tiempo) throws Exception
	{

		boolean encontrado = Utils.isDisplayed(driver, xpath, 1);
		int contador = 0;
		
		while (!encontrado && contador<tiempo) {
			Thread.sleep(1000);
			encontrado = Utils.isDisplayed(driver, xpath, 1);
			contador++;
		}
		
		if (!encontrado)
			throw new Exception ("No apareci� el elemento: "+xpath+" durante los "+tiempo+ " segundos de espera");

	}

	public static void seleccionarUISelectorContieneXPath(WebDriver driver, String xpath, String Opcion) throws Exception
	{
		//clickEnElVisible(driver, xpath);
		Atomic.click(driver, xpath);
		List<WebElement> opcionesCombo = driver.findElements(By.xpath(xpath + "//li//select-option"));
		for(WebElement opcion : opcionesCombo)
		{
			Thread.sleep(500);
			String opcionX = opcion.getText().toLowerCase();
			if(opcionX.indexOf(Opcion.toLowerCase()) > -1){
				opcion.click();
				break;
			}
				
			if(opcionX.equals(opcion))
			{
				opcion.click();
				break;
			}
		}
	}
	
//	public static void seleccionarUISelectorIndigoContieneXPath(WebDriver driver, String xpath, String Opcion) throws Exception
//	{
//		//clickEnElVisible(driver, xpath);
//		Atomic.click(driver, xpath);
//		List<WebElement> opcionesCombo = driver.findElements(By.xpath(xpath + "/descendant::select-option"));
//		for(WebElement opcion : opcionesCombo)
//		{
//			Thread.sleep(500);
//			String opcionX = opcion.getText().toLowerCase();
//			if(opcionX.indexOf(Opcion.toLowerCase()) > -1){
//				opcion.click();
//				break;
//			}
//				
//			if(opcionX.equals(opcion))
//			{
//				opcion.click();
//				break;
//			}
//		}
//	}
	
	public static void seleccionarComboContieneXPathValorAleatorio(WebDriver driver, String xpath) throws Exception
	{
		Select select=new Select(Utils.devuelveElementoVisibleYEnabled(driver, xpath));
		List<WebElement> opcionesCombo = select.getOptions();
		
		Random random = new Random();
		int randomNum = random.nextInt(opcionesCombo.size());
		opcionesCombo.get(randomNum).click();
	}
	
	public static String seleccionarComboValorAleatorioDevuelveTexto(WebDriver driver, String xpath) throws Exception
	{
		Select select=new Select(Utils.devuelveElementoVisibleYEnabled(driver, xpath));
		List<WebElement> opcionesCombo = select.getOptions();
		
		Random random = new Random();
		int randomNum = random.nextInt(opcionesCombo.size());
		opcionesCombo.get(randomNum).click();
		return opcionesCombo.get(randomNum).getText();
	}
	
	public static String getColor (WebDriver driver, String xpath) throws Exception {
		
		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed()){
					elemento = listaElementos.get(i);
					break;
				}
				
			}

			String rgba = elemento.getCssValue("color");
			
			String[] numbers = null;
			
			if (rgba.contains("rgba"))
				numbers = rgba.replace("rgba(", "").replace(")", "").split(",");
			else if(rgba.contains("rgb"))
				numbers = rgba.replace("rgb(", "").replace(")", "").split(",");
			
			int r = Integer.parseInt(numbers[0].trim());
			int g = Integer.parseInt(numbers[1].trim());
			int b = Integer.parseInt(numbers[2].trim());
			String hex = String.format("#%02X%02X%02X", r, g, b); 
			return hex;
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}
		
	}
	
	/**
	 * @param driver
	 * @param xpath
	 * @return el color en formato hexadecimal
	 * @throws Exception
	 */
	public static String getBackgroundColor (WebDriver driver, String xpath) throws Exception {
		
		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed()){
					elemento = listaElementos.get(i);
					break;
				}
				
			}

			String rgba = elemento.getCssValue("background-color");
			
			String[] numbers = null;
			
			if (rgba.contains("rgba"))
				numbers = rgba.replace("rgba(", "").replace(")", "").split(",");
			else if(rgba.contains("rgb"))
				numbers = rgba.replace("rgb(", "").replace(")", "").split(",");
			
			int r = Integer.parseInt(numbers[0].trim());
			int g = Integer.parseInt(numbers[1].trim());
			int b = Integer.parseInt(numbers[2].trim());
			String hex = String.format("#%02X%02X%02X", r, g, b); 
			return hex;
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}
		
	}
	
	public static String fileToString(String path, Charset encoding) 
			  throws IOException 
			{
			  byte[] encoded = Files.readAllBytes(Paths.get(path));
			  return new String(encoded, encoding);
			}
	
	
	
	/**
	 * @param driver
	 * @param m_codigo
	 * @param m_nombre
	 * @return devuelve el driver de selenium
	 * @throws Exception
	 */
	public static WebDriver inicializarCasoS3(WebDriver driver, String m_codigo, String m_nombre) throws Exception
	{

		//System.setProperty("webdriver.gecko.driver","lib/geckodriver.exe");
		System.setProperty("webdriver.gecko.driver", "lib/geckodriver.bat");
		

		// Establecemos que la salida est�ndar y la salida de errores sea el fichero que se crea por caso de prueba
		FileOutputStream fichero = new FileOutputStream("results/" + m_nombre + ".html", true);
		PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);
		PrintStream ficheroMain = new PrintStream(new BufferedOutputStream(new FileOutputStream("results/Main.txt", true)), true);

		ficheroMain.append("[INFO] [INICIO]" + Utils.GetTodayDateAndTime() + " Lanzando caso: " + m_nombre + ", codigo: " + m_codigo + "\n");
		ficheroMain.close();


//		System.setOut(ficheroSalida);
//		System.setErr(ficheroSalida);
		
		loggers.put(Long.toString(Thread.currentThread().getId()), new Log(m_nombre));

		// Pintamos el inicio de la traza
		Utils.pintarInicioTraza(m_codigo, m_nombre);

		// Cargamos las propiedades del properties
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String archivoCSV = params.getValorPropiedad("rutaResultados");
		String pais = params.getValorPropiedad("pais");
		String entorno = params.getValorPropiedad("entorno");
		String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
		String url = Utils.devuelveURLApartirDePaisYEntorno(rutaURLs, pais, entorno);
		
		
		

		// Inicializamos el caso a ERROR.
		Utils.CsvAppend(archivoCSV, m_codigo, m_nombre, "ERROR", "", "");
		

		// Obtenemos el driver espec�fico del navegador
		FirefoxOptions options = new FirefoxOptions();
		
		options.setBinary("C:\\Program Files\\Mozilla Firefox53\\firefox.exe");
		//options.addPreference("log", "{level: none}");
		//options.setLogLevel(Level.SEVERE);
		 //Location where Firefox is installed
 
		//DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		options.setCapability("moz:firefoxOptions", options);
		options.setCapability("version", "53");
		options.setCapability("browserName", "firefox");		
		
		
		//options.addTo(capabilities);
		
		
		FirefoxProfile profile = new FirefoxProfile();
		options.setCapability(FirefoxDriver.PROFILE, profile);

		profile.setAcceptUntrustedCertificates(true);
		profile.setPreference("browser.download.folderList", 2);
		String downloads = new File("downloads").getAbsolutePath();
		profile.setPreference("browser.download.dir", downloads);
		profile.setPreference(
				"browser.helperApps.neverAsk.saveToDisk",
				"image/jpg,text/csv,text/xml,application/xml,application/vnd.ms-excel,application/x-excel,application/x-msexcel,application/excel,application/pdf,application/octet-stream,application/rar,text/plain,application/text,application/zip");

		profile.setPreference("browser.helperApps.alwaysAsk.force", false);
		profile.setPreference("pdfjs.disabled", true);
		profile.setPreference("browser.startup.homepage", "");
		
 
		driver = new FirefoxDriver(options);

		// Navegamos a la url
		driver.get(url);

		driver.manage().timeouts().implicitlyWait(timeOut , TimeUnit.SECONDS);
		if (!driver.toString().contains("ChromeDriver"))
			driver.manage().window().maximize();

		return driver;
	}
	
	
	/**
	 * @param driver
	 * @param xpath
	 * @return
	 */
	public static boolean isClickable(WebDriver driver, String xpath) 
    {
        try{
            WebDriverWait wait = new WebDriverWait(driver, 6);
            WebElement elemento = Atomic.findElement(driver, xpath);
            wait.until(ExpectedConditions.elementToBeClickable(elemento));
            return true;
        }
        catch (Exception e){
            return false;
        }
    }
	
	/**
	 * @param driver
	 * @param xpathExpression
	 */
	public static void clickJS (WebDriver driver, String xpathExpression) {
		
		try {
			WebElement element = driver.findElement(By.xpath(xpathExpression));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		} catch (Exception e) {

		}
		
	}
	
	/**
	 * @param driver
	 * @param element
	 */
	public static void clickJS (WebDriver driver,WebElement element){
		try {
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		} catch (Exception e) {

		}
	}
	
	public static void seleccionarComboContieneXPathValorAleatorioSinCero(WebDriver driver, String xpath)
	{
		WebElement combo = driver.findElement(By.xpath(xpath));
		List<WebElement> opcionesCombo = combo.findElements(By.tagName("option"));
		
		Random random = new Random();
		int randomNum = random.nextInt(opcionesCombo.size());
		if ((randomNum == 0) && (opcionesCombo.size()>1))
			randomNum = 1;
		opcionesCombo.get(randomNum).click();
	}
	
	
	public static String devuelveCasosEjecutadosPorTipo (String tipoEjecucion) throws Exception
	{
		
		String resul = "";
		List <String> listaCasos = new ArrayList<>();
		
		if (tipoEjecucion.equalsIgnoreCase("ANS")){
			
			File[] pathsLanzamiento = Utils.devuelveArchivosDeUnDirectorio("lanzamiento/", ".csv");
			
			for(File archivo : pathsLanzamiento)
			{
	
				CsvReader reader = new CsvReader(new FileReader(archivo));
			
				while(reader.readRecord())
				{
					String idCaso = reader.get(0);
					String nombreCaso = reader.get(1);
					String lanzar = reader.get(2);
//					String tipoCaso = reader.get(4);
					
					if (lanzar.equalsIgnoreCase("SI")) {
						String estadoCaso = Utils.estadoCaso(idCaso);
						listaCasos.add(nombreCaso + "#" +estadoCaso);
					}
						
				}
				
				reader.close();

			}
			
			
		}

		else if (!tipoEjecucion.equalsIgnoreCase("")){
			
			File[] pathsLanzamiento = Utils.devuelveArchivosDeUnDirectorio("lanzamiento/", ".csv");
			
			for(File archivo : pathsLanzamiento)
			{
	
				CsvReader reader = new CsvReader(new FileReader(archivo));
			
				while(reader.readRecord())
				{
					String idCaso = reader.get(0);
					String nombreCaso = reader.get(1);
					String lanzar = reader.get(2);
					String tipoCaso = reader.get(4);
					
					if (tipoCaso.equalsIgnoreCase(tipoEjecucion) & lanzar.equalsIgnoreCase("SI")) {
						String estadoCaso = Utils.estadoCaso(idCaso);
						listaCasos.add(nombreCaso + "#" +estadoCaso);
					}
						
				}
				
				reader.close();

			}

		}
		
		
		else {
			
			//Sin tipo de ejecucion

			String pathsLanzamiento[] = Utils.getPathsLanzamiento();
			
			for(String archivo : pathsLanzamiento)
			{
	
				CsvReader reader = new CsvReader(new FileReader(archivo));
			
				while(reader.readRecord())
				{
					String idCaso = reader.get(0);
					String nombreCaso = reader.get(1);
					String lanzar = reader.get(2);
					
					if (lanzar.equalsIgnoreCase("SI")) {
						String estadoCaso = Utils.estadoCaso(idCaso);
						listaCasos.add(nombreCaso + "#" +estadoCaso);
					}
						
				}
				
				reader.close();

			}
			
		}
		
		
		StringBuilder sb = new StringBuilder();
		for (String s : listaCasos)
		{
		    sb.append(s);
		    sb.append(";");
		}
		
		resul = sb.toString();

		return resul;
	}
	


	public static File[] devuelveArchivosDeUnDirectorio(String dirName, final String extension){
	        File dir = new File(dirName);

	        return dir.listFiles(new FilenameFilter() { 
	                 public boolean accept(File dir, String filename)
	                      { return filename.endsWith(extension); }
	        } );

	    }
	
	public static String[] devuelveArchivosDeUnDirectorioString(String dirName, final String extension){
        
		File dir = new File(dirName);

    	String[] directories = dir.list(new FilenameFilter() { 
    		
    		public boolean accept(File dir, String filename) { 
    		return filename.endsWith(extension); 
    	} 
    	});
    	
    	return directories;

    }

	
	public static void clickEspecialPrimero(WebDriver driver, String xpath) throws Exception
	{
		
		Utils.esperaHastaApareceSinSalir(driver, xpath, 10);

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			elemento = listaElementos.get(0);

			clickJS(driver, elemento);
			
			Utils.esperarProcesandoPeticionDefaultContentVolviendoAPPal(driver);
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}

	}

	public static void seleccionarUISelectorContieneXPath2(WebDriver driver, String xpath, String Opcion) throws Exception
	{
		clickEnElVisible(driver, xpath + "//i");
		Thread.sleep(3000);
		List<WebElement> opcionesCombo = driver.findElements(By.xpath(xpath + "//li//select-option"));
		for(WebElement opcion : opcionesCombo)
		{
			String opcionX = opcion.getText().toLowerCase();
			if(opcionX.indexOf(Opcion.toLowerCase()) > -1){
				opcion.click();
				break;
			}
				
			if(opcionX.equals(opcion))
			{
				opcion.click();
				break;
			}
		}
	}
	
	/**
	 * @param driver
	 * @param xpath
	 * @return devuelve la cadena del valor seleccionado del UISelector
	 * @throws Exception
	 */
	public static String seleccionarUISelectorDevuelveTexto(WebDriver driver, String xpath) throws Exception
	{
		Atomic.click(driver, xpath + "//i");
		Thread.sleep(1000);
		Utils.esperaHastaApareceSinSalir(driver, xpath + "//li//select-option", 10);
		List<WebElement> opcionesCombo = driver.findElements(By.xpath(xpath + "//li//select-option"));
		
		String textoADevolver = "";
		
		for(WebElement opcion : opcionesCombo)
		{
			Thread.sleep(2000);
			
			textoADevolver = opcion.getText();
			
			if (opcion.isDisplayed() && !textoADevolver.equalsIgnoreCase("")){
				opcion.click();
				break;
			}

		}
		
		return textoADevolver;
	}
	
	public static void openNewTab(WebDriver driver) throws Exception

	{
		if (driver.toString().contains("ChromeDriver"))
			ejecutarJS(driver, "window.open()");	
		else
			driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");

	}
	
	/**
	 * @param driver
	 * Cambia a la siguiente pesta�a
	 */
	public static void switchTab(WebDriver driver)

	{
	    driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.TAB);
	    ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(0));

	}
	
	/**
	 * @param driver
	 * @param tabNumber: n�mero de la pesta�a a la que queremos cambiar
	 */
	public static void switchToTab (WebDriver driver, int tabNumber) 
	{
	    ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs2.get(tabNumber));

	}
	
	public static String separarFormatoIban (String cuenta)
	{
		int i = 0;
		String cuentaAux = "";
		while ((i >=0 ) && (i<cuenta.length()))
		{
			int num = i+1;
            if ((i > 0) && ((num % 4) == 0) && (num < cuenta.length())){
            	cuentaAux = cuentaAux + cuenta.charAt(i) + " ";
            }else{
            	cuentaAux = cuentaAux + cuenta.charAt(i);
            }
            i = i +1;
		}
		
		return cuentaAux;
	}
	
	
	public static File crearC204 () {
		
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		
		String plantilla = "plantillaC204.xls";

		//En base a una plantilla inamovible
		File fichero = new File("resources/" + plantilla);

		try
		{
			
			//Se crea este fichero
			String nombreC204 = "C-204_"+ Utils.GetTodayDateAndTimeSFTP() + ".xls";
			File c204 = new File("resources/" + nombreC204);

			String rutaCSV = params.getValorPropiedad("rutaResultados");

			String outputFile = rutaCSV;

			CsvReader reader = new CsvReader(new FileReader(outputFile));
	        reader.readHeaders();
	        
	        int sumaLinea=0;
			WorkbookSettings ws = new WorkbookSettings(); 
			ws.setEncoding("Cp1252");
        	Workbook w = Workbook.getWorkbook(fichero,ws);
			WritableWorkbook copyR = Workbook.createWorkbook(c204, w);
	        
			
			WritableFont arial10ptBold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD);
			WritableCellFormat arial10BoldFormatBorder = new WritableCellFormat(arial10ptBold);
			arial10BoldFormatBorder.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
			arial10BoldFormatBorder.setAlignment(Alignment.CENTRE);
			arial10BoldFormatBorder.setWrap(true);

			WritableFont arial9pt = new WritableFont(WritableFont.ARIAL, 9, WritableFont.NO_BOLD);
			WritableCellFormat arial9Format = new WritableCellFormat(arial9pt);
			arial9Format.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
			arial9Format.setAlignment(Alignment.LEFT);

			WritableCellFormat arial9Format2 = new WritableCellFormat(arial9pt);
			arial9Format2.setBorder(jxl.format.Border.TOP, jxl.format.BorderLineStyle.THIN);
			arial9Format2.setBorder(jxl.format.Border.BOTTOM, jxl.format.BorderLineStyle.THIN);
			arial9Format2.setAlignment(Alignment.LEFT);

			WritableFont arial9ptCENTER = new WritableFont(WritableFont.ARIAL, 9, WritableFont.NO_BOLD);
			WritableCellFormat arial9CenterFormat = new WritableCellFormat(arial9ptCENTER);
			arial9CenterFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
			arial9CenterFormat.setAlignment(Alignment.CENTRE);

			WritableCellFormat arial10BoldFormatDownBorder = new WritableCellFormat(arial10ptBold);
			arial10BoldFormatDownBorder.setBorder(jxl.format.Border.BOTTOM, jxl.format.BorderLineStyle.MEDIUM);

			WritableCellFormat arial10BoldFormatDownBorder2 = new WritableCellFormat(arial10ptBold);
			arial10BoldFormatDownBorder2.setBorder(jxl.format.Border.BOTTOM, jxl.format.BorderLineStyle.MEDIUM);
			arial10BoldFormatDownBorder2.setBorder(jxl.format.Border.TOP, jxl.format.BorderLineStyle.THIN);

			WritableCellFormat arial10BoldFormatDownBorder3 = new WritableCellFormat(arial10ptBold);
			arial10BoldFormatDownBorder3.setBorder(jxl.format.Border.BOTTOM, jxl.format.BorderLineStyle.MEDIUM);
			arial10BoldFormatDownBorder3.setBorder(jxl.format.Border.TOP, jxl.format.BorderLineStyle.THIN);
			arial10BoldFormatDownBorder3.setBorder(jxl.format.Border.LEFT, jxl.format.BorderLineStyle.THIN);
			arial10BoldFormatDownBorder3.setBorder(jxl.format.Border.RIGHT, jxl.format.BorderLineStyle.THIN);
			arial10BoldFormatDownBorder3.setAlignment(Alignment.CENTRE);
			arial10BoldFormatDownBorder3.setWrap(true);
			
			WritableCellFormat arial10BoldFormatDownBorder3ID = new WritableCellFormat(arial10ptBold);
			arial10BoldFormatDownBorder3ID.setBorder(jxl.format.Border.BOTTOM, jxl.format.BorderLineStyle.MEDIUM);
			arial10BoldFormatDownBorder3ID.setBorder(jxl.format.Border.TOP, jxl.format.BorderLineStyle.THIN);
			arial10BoldFormatDownBorder3ID.setBorder(jxl.format.Border.LEFT, jxl.format.BorderLineStyle.THIN);
			arial10BoldFormatDownBorder3ID.setBorder(jxl.format.Border.RIGHT, jxl.format.BorderLineStyle.THIN);
			arial10BoldFormatDownBorder3ID.setAlignment(Alignment.JUSTIFY);
			arial10BoldFormatDownBorder3ID.setAlignment(Alignment.CENTRE);
			arial10BoldFormatDownBorder3ID.setWrap(true);
			
			WritableCellFormat arial10BoldFormatNoBorder3ID = new WritableCellFormat(arial10ptBold);
			arial10BoldFormatNoBorder3ID.setAlignment(Alignment.JUSTIFY);
			arial10BoldFormatNoBorder3ID.setAlignment(Alignment.CENTRE);
			arial10BoldFormatNoBorder3ID.setWrap(true);
			
			WritableCellFormat arial10BoldFormatDownBorderTop = new WritableCellFormat(arial10ptBold);
			//arial10BoldFormatDownBorderTop.setBorder(jxl.format.Border.BOTTOM, jxl.format.BorderLineStyle.THIN);
			arial10BoldFormatDownBorderTop.setBorder(jxl.format.Border.TOP, jxl.format.BorderLineStyle.MEDIUM);
			arial10BoldFormatDownBorderTop.setBorder(jxl.format.Border.LEFT, jxl.format.BorderLineStyle.THIN);
			arial10BoldFormatDownBorderTop.setBorder(jxl.format.Border.RIGHT, jxl.format.BorderLineStyle.THIN);
			arial10BoldFormatDownBorderTop.setAlignment(Alignment.CENTRE);
			arial10BoldFormatDownBorderTop.setWrap(true);
			
			WritableCellFormat arial10BoldFormatDownBorderBottom = new WritableCellFormat(arial10ptBold);
			arial10BoldFormatDownBorderBottom.setBorder(jxl.format.Border.BOTTOM, jxl.format.BorderLineStyle.MEDIUM);
			arial10BoldFormatDownBorderBottom.setBorder(jxl.format.Border.LEFT, jxl.format.BorderLineStyle.THIN);
			arial10BoldFormatDownBorderBottom.setBorder(jxl.format.Border.RIGHT, jxl.format.BorderLineStyle.THIN);
			arial10BoldFormatDownBorderBottom.setAlignment(Alignment.CENTRE);
			arial10BoldFormatDownBorderBottom.setWrap(true);

			WritableFont arial10ptNegrita = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD, true);
			WritableCellFormat arial10NegritaFormatBorder = new WritableCellFormat(arial10ptNegrita);
			arial10NegritaFormatBorder.setWrap(true);
			arial10NegritaFormatBorder.setAlignment(Alignment.CENTRE);
			
			
			WritableSheet sheet1 = copyR.getSheet(1);
			
	        while (reader.readRecord())
	        {
	        	String testName = reader.get(1);
				
				//Guardar en un string y contar los saltos de l�nea
				String pasos= Utils.devuelveDatoDocumentacion(reader.get("Test_ID"), "Pasos" , testName);
				int saltosDeLinea=0;
				int saltoDeLineaAnterior=0;
				int contador=0;
				
				String texto;
							
				while (saltosDeLinea!=-1){
					saltosDeLinea = pasos.indexOf("<br />", saltosDeLinea+1);
					contador++;
				}	
				
				
				//C�digo caso
				for (int i = 12+sumaLinea; i < contador+12+sumaLinea; ++i){
					
					if(i==contador+12+sumaLinea-1)
					{						
						if(i==12+sumaLinea)
						{
							Label label = new Label(0, i, reader.get("Test_ID"), arial10BoldFormatDownBorder3ID);
							sheet1.addCell(label);
						}
						else{
							Label label = new Label(0, i, "", arial10BoldFormatDownBorderBottom);
							sheet1.addCell(label);
						}
						
						
						
						//Label label = new Label(0, i, "", arial10BoldFormatDownBorder3);
						//sheet1.addCell(label);
					}
					
					else
					{
						if(i==12+sumaLinea)
						{
							Label label = new Label(0, i, reader.get("Test_ID"),arial10BoldFormatNoBorder3ID);
							sheet1.addCell(label);
						}
						else{
							Label label = new Label(0, i, "");
							sheet1.addCell(label);
						}
						
					}
				
					
				}
				
				//Nombre del caso
				String nombreCaso = Utils.devuelveDatoDocumentacion(reader.get("Test_ID"), "Caso" , testName);
				
				for (int i = 12+sumaLinea; i < contador+12+sumaLinea; ++i){
					//Label label = new Label(1, i, reader.get("Test_Name"));  
					//sheet1.addCell(label);		
									
					if(i==contador+12+sumaLinea-1)
					{
						Label label = new Label(1, i, nombreCaso, arial10BoldFormatDownBorder3);
						sheet1.addCell(label);
					}
					else
					{
						Label label = new Label(1, i, nombreCaso, arial10BoldFormatBorder);
						sheet1.addCell(label);
					}
					
				}
				
				//Prioridad
				for (int i = 12+sumaLinea; i < contador+12+sumaLinea; ++i){
					//Label label = new Label(2, i, "Alta");
					//sheet1.addCell(label);
					
					if(i==contador+12+sumaLinea-1)
					{
						Label label = new Label(2, i, "Alta", arial10BoldFormatDownBorder3);
						sheet1.addCell(label);
					}
					else
					{
						Label label = new Label(2, i, "Alta", arial10BoldFormatBorder);
						sheet1.addCell(label);
					}
					
					
				}
				
				//Resultados esperados
				String resultadoEsperado= Utils.devuelveDatoDocumentacion(reader.get("Test_ID"), "ResultadoEsperado" , testName);
				for (int i = 12+sumaLinea; i < contador+12+sumaLinea; ++i){
					if(i==contador+12+sumaLinea-1){						
						resultadoEsperado=resultadoEsperado.replace("<br />", "\r\n");
						Label label = new Label(5, i, resultadoEsperado, arial10BoldFormatDownBorder3);
						sheet1.addCell(label);
					}
					else{
						Label label = new Label(5, i, "La operaci�n se realiza correctamente", arial10BoldFormatBorder);
						sheet1.addCell(label);
					}
					

			
				}
				
				//C�digo condici�n
				for (int i = 12+sumaLinea; i < contador+12+sumaLinea; ++i){
					//Label label = new Label(2, i, "Alta");
					//sheet1.addCell(label);
					
					if(i==contador+12+sumaLinea-1)
					{
						Label label = new Label(3, i, "", arial10BoldFormatDownBorder3);
						sheet1.addCell(label);
					}
					else
					{
						Label label = new Label(3, i, "", arial10BoldFormatBorder);
						sheet1.addCell(label);
					}
					
					
				}	
				//Ciclo de prueba
				for (int i = 12+sumaLinea; i < contador+12+sumaLinea; ++i){
					//Label label = new Label(2, i, "Alta");
					//sheet1.addCell(label);
					
					if(i==contador+12+sumaLinea-1)
					{
						Label label = new Label(6, i, "", arial10BoldFormatDownBorder3);
						sheet1.addCell(label);
					}
					else
					{
						Label label = new Label(6, i, "", arial10BoldFormatBorder);
						sheet1.addCell(label);
					}
					
					
				}
				
				
				//Pasos					
				saltosDeLinea=0;
				int x=12;
					 
				while (saltosDeLinea!=-1){
					saltoDeLineaAnterior=saltosDeLinea;
					saltosDeLinea = pasos.indexOf("<br />", saltosDeLinea+1);
					if (saltosDeLinea==-1){
						texto=pasos.substring(saltoDeLineaAnterior);
						if(texto.contains("<br /")){
							saltoDeLineaAnterior=saltoDeLineaAnterior+6;
							texto=pasos.substring(saltoDeLineaAnterior);
						}
					}
					else{
						texto=pasos.substring(saltoDeLineaAnterior, saltosDeLinea);
						if(texto.contains("<br /")){
							saltoDeLineaAnterior=saltoDeLineaAnterior+6;
							texto=pasos.substring(saltoDeLineaAnterior, saltosDeLinea);
						}
					}
					
					if(saltosDeLinea==-1){
						Label label = new Label(4, x+sumaLinea, texto, arial10BoldFormatDownBorder3);
						sheet1.addCell(label);
						x++;	
					}
					
					else{
						Label label = new Label(4, x+sumaLinea, texto, arial10BoldFormatBorder);
						sheet1.addCell(label);
						x++;
					}
//					Label label = new Label(4, x+sumaLinea, texto);
//					sheet1.addCell(label);
//					x++;	

					}
				
				//Resultado obtenido
				for (int i = 12+sumaLinea; i < contador+12+sumaLinea; ++i){
					if(reader.get("Resultado").contains("OK")){
						//Label label = new Label(8, i, "Satisfactorio");
						//sheet1.addCell(label);
						if(i==contador+12+sumaLinea-1)
						{
							Label label = new Label(8, i, "Satisfactorio", arial10BoldFormatDownBorder3);
							sheet1.addCell(label);
						}
						else
						{
							Label label = new Label(8, i, "Satisfactorio", arial10BoldFormatBorder);
							sheet1.addCell(label);
						}
						
						
					}
					
					else if(reader.get("Resultado").contains("NO EJECUTADO")){
						//Label label = new Label(8, i, "Caso err�neo");
						//sheet1.addCell(label);
						if(i==contador+12+sumaLinea-1)
						{
							Label label = new Label(8, i, "Caso err�neo", arial10BoldFormatDownBorder3);
							sheet1.addCell(label);
						}
						else
						{
							Label label = new Label(8, i, "Caso err�neo", arial10BoldFormatBorder);
							sheet1.addCell(label);
						}
						
					}
					
					else if(reader.get("Resultado").contains("WARNING")){
						//Label label = new Label(8, i, "Satisfactorio");
						//sheet1.addCell(label);
						if(i==contador+12+sumaLinea-1)
						{
							Label label = new Label(8, i, "Satisfactorio", arial10BoldFormatDownBorder3);
							sheet1.addCell(label);
						}
						else
						{
							Label label = new Label(8, i, "Satisfactorio", arial10BoldFormatBorder);
							sheet1.addCell(label);
						}
					}
					
					else{
						//Label label = new Label(8, i, "Insatisfactorio");
						//sheet1.addCell(label);
						if(i==contador+12+sumaLinea-1)
						{
							Label label = new Label(8, i, "Insatisfactorio", arial10BoldFormatDownBorder3);
							sheet1.addCell(label);
						}
						else
						{
							Label label = new Label(8, i, "Insatisfactorio", arial10BoldFormatBorder);
							sheet1.addCell(label);
						
						}
					}
				}
				
				//Motivo de la prueba
				for (int i = 12+sumaLinea; i < contador+12+sumaLinea; ++i){
					if(!reader.get("Comentario").isEmpty()){
						if(i==contador+12+sumaLinea-1)
						{
							Label label = new Label(7, i, reader.get("Comentario"), arial10BoldFormatDownBorder3);
							sheet1.addCell(label);
						}
						else
						{
							Label label = new Label(7, i, "", arial10BoldFormatBorder);
							sheet1.addCell(label);
						
						}
					}
					else{
						if(i==contador+12+sumaLinea-1)
						{
							Label label = new Label(7, i, "", arial10BoldFormatDownBorder3);
							sheet1.addCell(label);
						}
						else
						{
							Label label = new Label(7, i, "", arial10BoldFormatBorder);
							sheet1.addCell(label);
						
						}
					}
				}
				
				
					sumaLinea=sumaLinea+contador;
				  
	        }
	         
	        reader.close();


			// Rellenar info adicional
			sheet1 = copyR.getSheet(0);

			String FechaHoy = Utils.GetTodayDate();

			// Fecha Actualizaci�n
			Label label = new Label(3, 7, FechaHoy, arial10BoldFormatDownBorder);
			sheet1.addCell(label);

			// Versi�n actual
			WritableCell cell = sheet1.getWritableCell(5, 7);
			String versionActual = cell.getContents();

			if(versionActual.equals(""))
				versionActual = "1.0";

			String versionNueva = String.valueOf(Double.parseDouble(versionActual) + 0.1).substring(0, 3);

			label = new Label(5, 7, versionNueva, arial10BoldFormatDownBorder);
			sheet1.addCell(label);

			// Versi�n + Casos de prueba
			String crLf = "\n";
			String literalCasosVersion = "Casos de Prueba " + crLf + " V" + versionNueva + " (" + FechaHoy + ")";

			label = new Label(6, 0, literalCasosVersion, arial10NegritaFormatBorder);
			sheet1.addCell(label);

			// Fecha inicio y fecha fin
			label = new Label(5, 15, FechaHoy, arial10BoldFormatBorder);
			sheet1.addCell(label);

			label = new Label(9, 15, FechaHoy, arial10BoldFormatBorder);
			sheet1.addCell(label);

			// Versi�n + casos en la pesta�a 1
			sheet1 = copyR.getSheet(1);
			label = new Label(6, 0, literalCasosVersion, arial10NegritaFormatBorder);
			sheet1.addCell(label);

			// Autor
			String autor = "Automatizaci�n";
			label = new Label(2, 8, autor, arial9Format);
			sheet1.addCell(label);

			// Tablita
			sheet1 = copyR.getSheet(0);
			int filaVersion = sheet1.getRows();

			label = new Label(0, filaVersion, versionNueva, arial9CenterFormat);
			sheet1.addCell(label);

			label = new Label(1, filaVersion, FechaHoy, arial9CenterFormat);
			sheet1.addCell(label);

			label = new Label(2, filaVersion, autor, arial9Format2);
			sheet1.addCell(label);

			label = new Label(3, filaVersion, "", arial9Format2);
			sheet1.addCell(label);

			label = new Label(4, filaVersion, "", arial9Format2);
			sheet1.addCell(label);

			sheet1.mergeCells(2, filaVersion, 4, filaVersion);
			// sheet.mergeCells(2, filaVersion,4, filaVersion);

			label = new Label(5, filaVersion, "Ejecuci�n pruebas Autom�ticas", arial9Format);
			sheet1.addCell(label);

			label = new Label(9, filaVersion, "", arial9Format);
			sheet1.addCell(label);

			sheet1.mergeCells(5, filaVersion, 9, filaVersion);

//	    }

			copyR.write();
			copyR.close();
		
			return c204;
		
		}
		
		catch(Exception e)
		{
			utils.Log.writeException(e);
			return null;
		}
		

		
		
		
	}
	
	
	public static boolean checkBefore (WebDriver driver, WebElement element) {
		
		
		String resul = ((JavascriptExecutor)driver)
		        .executeScript("return window.getComputedStyle(arguments[0], ':before').getPropertyValue('content');",element).toString();
		
		if (!resul.equalsIgnoreCase("") && !resul.equalsIgnoreCase("none"))
			return true;
		else
			return false;
		
	}
	
	public static void clickEnTodosLosVisibles(WebDriver driver, String xpath) throws Exception
	{
		
		Utils.esperaHastaApareceSinSalir(driver, xpath, 10);

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed()){
					elemento = listaElementos.get(i);
					clickJS(driver, elemento);
				}
			}
			
			if (elemento==null){ 
				for(int i = 0; i < listaElementos.size(); ++i)
					{
						if(checkBefore(driver,listaElementos.get(i))){
							elemento = listaElementos.get(i);
							clickJS(driver, elemento);
						}
					}
			}
		
			Utils.esperarProcesandoPeticionDefaultContentVolviendoAPPal(driver);
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}

	}
	
	
	public static boolean checkPDFcontainsText (String fileName, String texto) {
		
		PDFParser parser = null;
	    PDDocument pdDoc = null;
	    COSDocument cosDoc = null;
	    PDFTextStripper pdfStripper;
	    
	    boolean encontrado = false;

	    String parsedText;

	    File file = new File(fileName);
	    try {
	        parser = new PDFParser(new FileInputStream(file));
	        parser.parse();
	        cosDoc = parser.getDocument();
	        pdfStripper = new PDFTextStripper();
	        pdDoc = new PDDocument(cosDoc);
	        parsedText = pdfStripper.getText(pdDoc).replaceAll("[^A-Za-z0-9. ]+", "");
	        
	        if (parsedText.contains(texto))
	        	encontrado = true;
	        else 
	        	encontrado = false;
	        
	    } catch (Exception e) {
	        
	        try {
	            if (cosDoc != null)
	                cosDoc.close();
	            if (pdDoc != null)
	                pdDoc.close();
	        } catch (Exception e1) {
	            
	        }

	    }
		return encontrado;
		
		
	}
	
	public static String devuelveContenidoPDF (String fileName) {
		
		PDFParser parser = null;
	    PDDocument pdDoc = null;
	    COSDocument cosDoc = null;
	    PDFTextStripper pdfStripper;

	    String parsedText = "";

	    File file = new File(fileName);
	    try {
	        parser = new PDFParser(new FileInputStream(file));
	        parser.parse();
	        cosDoc = parser.getDocument();
	        pdfStripper = new PDFTextStripper();
	        pdDoc = new PDDocument(cosDoc);
	        parsedText = pdfStripper.getText(pdDoc).replaceAll("[^A-Za-z0-9. ]+", "");

	        
	    } catch (Exception e) {
	        
	        try {
	            if (cosDoc != null)
	                cosDoc.close();
	            if (pdDoc != null)
	                pdDoc.close();
	        } catch (Exception e1) {
	            
	        }

	    }
		return parsedText;
		
		
	}
	
	public static void borrarTodosFicherosConExtension(String directory, String extension) throws Exception
	{
		try
		{
			File dir = new File(directory);
			 
			for (File file : dir.listFiles()) {
				if (file.getName().endsWith(extension) && !file.delete()) {
					throw new IOException();
				}
			}
		}
		catch(Exception e)
		{
			
		}
	}
	
	public static void seleccionarComboDiv (WebDriver driver, String xpath, String Opcion) throws Exception {
		
		Utils.esperaHastaApareceSinSalir(driver, xpath, 5);
		
		Atomic.click(driver, xpath);
		
		Utils.introducirTextoEnElVisible2(driver, xpath + "/descendant::input", Opcion);
		
		List<WebElement> opcionesCombo = driver.findElements(By.xpath(xpath + "/descendant::li"));
		for(WebElement opcion : opcionesCombo)
		{
			Thread.sleep(3000);
			String opcionX = opcion.getText().toLowerCase();
			if(opcionX.indexOf(Opcion.toLowerCase()) > -1){
				opcion.click();
				break;
			}
				
			if(opcionX.equals(opcion))
			{
				opcion.click();
				break;
			}
		}
	}
	
	
	public static void loginNetAdvance(WebDriver driver, String referencia, String password) throws Exception
	{

		String idioma = params.getValorPropiedad("idioma").toLowerCase();

		String pais = params.getValorPropiedad("pais").toLowerCase();
		String entorno = params.getValorPropiedad("entorno").toLowerCase();
		String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
		String url = devuelveURLApartirDePaisYEntorno(rutaURLs, pais, entorno);

		if(!existeElemento(driver, By.xpath(Constantes.Login_Referencia)))
			driver.get(url);

		String idiomaActual = "";

		if(Utils.isDisplayed(driver, Constantes.ComboIdiomas, 2))
		{
			idiomaActual = driver.findElement(By.xpath(Constantes.ComboIdiomas)).getText();
		}
		else if(Utils.isDisplayed(driver, Constantes.comboIdiomasCompass, 2))
		{
			idiomaActual = driver.findElement(By.xpath(Constantes.comboIdiomasCompass)).getText();
		}


		if(idiomaActual.toLowerCase().indexOf(idioma) < 0)
		{
			Utils.cambiarIdioma(driver, idioma);
		}
		
		//Si sale un modal raro
		if (Utils.isDisplayedSinBefore(driver, Constantes.cerrarModalLogin, 1))
			Utils.clickEnElVisiblePrimero(driver, Constantes.cerrarModalLogin);

		//Si se guardo el usuario
		if (Utils.isDisplayed(driver, Constantes.changeUserLabel, 1))
			Utils.clickEnElVisible(driver, Constantes.changeUserLabel);
		
		Utils.introducirTextoEnElVisible2(driver, Constantes.Login_Referencia, referencia);
		Utils.introducirTextoEnElVisible2(driver, Constantes.Login_Password, password);


		if(Utils.isDisplayed(driver, Constantes.Login_BotonEntrar, 1))
			Utils.clickEnElVisiblePrimero(driver, Constantes.Login_BotonEntrar);
		else if(Utils.isDisplayed(driver, Constantes.Login_BotonEntrarCompass, 1))
			Utils.clickEnElVisiblePrimero(driver, Constantes.Login_BotonEntrarCompass);
		
		if(Utils.isDisplayed(driver, Constantes.BotonOKMessageDialog, 20))
			Utils.clickEnElVisible(driver, Constantes.BotonOKMessageDialog);

		Thread.sleep(4000);

		if(!Utils.isDisplayed(driver, Constantes.BotonDesconectar, 10))
		{
			if(Utils.isDisplayed(driver, Constantes.BotonOKMessageDialog, 5))
			{
				Utils.clickEnElVisiblePrimero(driver, Constantes.BotonOKMessageDialog);
			}
		}

		if(Utils.isDisplayed(driver, Constantes.FrameCampana, 1))
		{
			Utils.cambiarFrameDadoVisible(driver, Constantes.FrameCampana);
			if(Utils.isDisplayed(driver, Constantes.continuarCampania, 1))
			{
				Utils.clickEnElVisible(driver, Constantes.continuarCampania);
				try
				{
					driver.switchTo().defaultContent();
					WebElement frame = driver.findElement(By.xpath(Constantes.FrameCampana));
					driver.switchTo().frame(frame);
					if(Utils.isDisplayed(driver, Constantes.ActivarTokenOtroMomento, 5))
					{
						Utils.clickEnElVisible(driver, Constantes.ActivarTokenOtroMomento);
						Thread.sleep(3500);
					}
					
					if (Utils.isDisplayed(driver, Constantes.divCerrarVentanaModal, 1))
						Utils.clickEnElVisiblePrimero(driver, Constantes.divCerrarVentanaModal);
				}
				catch(Exception e)
				{

				}
			}
			driver.switchTo().defaultContent();
		}
		
		if (Utils.isDisplayed(driver, Constantes.divCerrarVentanaModal, 1))
			Utils.clickEnElVisiblePrimero(driver, Constantes.divCerrarVentanaModal);

		boolean esVisible = Utils.isDisplayed(driver, Constantes.BotonDesconectar, 40);
		// esperarVisibleTiempo(driver,Constantes.BotonDesconectar,20);

		if(!esVisible)
			throw new Exception("No se pudo realizar el login. Comprobar captura");

	}
	
	
	public static String devuelveFrameActual (WebDriver driver) throws Exception {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		String currentFrame = (String) js.executeScript("return self.name");
		return currentFrame;
	}
	
	
	public static List<String> getBrowserVersion (WebDriver driver) {
		
		String uAgent = (String) ((JavascriptExecutor) driver).executeScript("return navigator.userAgent;");
		Log.write(uAgent);
		
		List <String> array = new ArrayList<>();
		
		String browser = "";
		String browser_version = "";
		
		if (uAgent.contains("MSIE") && uAgent.contains("Windows")) {
								
			browser = "Internet Explorer";
			browser_version  = uAgent.substring(uAgent.indexOf("MSIE")+5, uAgent.indexOf("Windows")-2);
		} 
		
		else if (uAgent.contains("Trident/7.0")) {
			browser = "Internet Explorer";
			browser_version = "11.0";
			}
		

		
		else if (uAgent.contains("Firefox")) {
			browser = "Firefox";
			browser_version = uAgent.substring(uAgent.indexOf("Firefox")+8);
			
		}
		
		else if (uAgent.contains("Chrome")) {
			browser = "Chrome";
			browser_version = uAgent.substring(uAgent.indexOf("Chrome")+7, uAgent.indexOf("Chrome")+9);
			
		}
		
		else {
			
			browser = "Unknown";
			browser_version = "Unknown";
			
		}
		
		array.add(browser);
		array.add(browser_version);
		
		return array;
		
	}
	
	public static Double numeroAleatorioEntreDos(Double rangeMin, Double rangeMax)
	{
		Random r = new Random();
		Double randomValue = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
		return randomValue;
		
	}
	
	public static int numeroAleatorioEntreDos(int Low, int High)
	{
		int randomNum = ThreadLocalRandom.current().nextInt(Low, High + 1);
		return randomNum;
	}
	
	
	public static void seleccionarUISelectorIndigoContieneXPath(WebDriver driver, String xpath, String Opcion) throws Exception
	{
		Utils.esperaHastaApareceSinSalir(driver, xpath, 5);
		Thread.sleep(1500);
		Atomic.click(driver, xpath);
		
		if (Utils.isDisplayed(driver, xpath + "/descendant::input[contains(@placeholder,'uscar')]", 2)){
			Utils.introducirTextoEnElVisible2(driver, xpath + "/descendant::input[contains(@placeholder,'uscar')]", Opcion);
			Utils.clickEnElVisible(driver, xpath + "/descendant::select-option");
		}
		
		else {
			
			List<WebElement> opcionesCombo = driver.findElements(By.xpath(xpath + "/descendant::select-option"));
			for(WebElement opcion : opcionesCombo)
			{
				Thread.sleep(500);
				String opcionX = opcion.getText().toLowerCase();
				if(opcionX.indexOf(Opcion.toLowerCase()) > -1){
					opcion.click();
					break;
					
				}
					
				if(opcionX.equals(opcion))
				{
					opcion.click();
					break;
				}
			}
		}
	}
	
	
	
	public static void adivinarNetworkError (WebDriver driver) {
		 
		
		 try {
			 
			 
			 
			 LogEntries logs = driver.manage().logs().get("performance");

			 int status = -1;
			 
			 String mensajeEscrito = "";

			 for (Iterator<LogEntry> it = logs.iterator(); it.hasNext();)
			 {
			     LogEntry entry = it.next();

			     JSONObject json = new JSONObject(entry.getMessage());

			     //Log.write(json.toString());

			     JSONObject message = json.getJSONObject("message");
			     String method = message.getString("method");

			     if (method != null
			             && "Network.responseReceived".equals(method))
			     {
			         JSONObject paramsJSON = message.getJSONObject("params");
			        
			         JSONObject response = paramsJSON.getJSONObject("response");
			         String messageUrl = response.getString("url");
			         status = response.getInt("status");
			         
			         if (status>=400 && status<=599 && noEsImagenNiCss(messageUrl)) {
			        	 
			        	 String customerRequest = readStringFromURL(driver,messageUrl, status);

			        	 if (!mensajeEscrito.equalsIgnoreCase(messageUrl)) {
			              	Log.write("<html><b style=\"color: #1500ff;\">- [NETWORK ERROR RESPONSE] for "+ messageUrl + ": " + status +"</b></html>");
			              	if (!customerRequest.equalsIgnoreCase(""))
			              		Log.write("<html><b style=\"color: #1500ff;\">"+customerRequest+"</b></html>");
			              	mensajeEscrito = messageUrl;
			        	 }
			        	 
			          //Log.write("<html><b style=\"color: #ff0000;\">Headers del Network Error: " + response.get("headers") + "</b></html>");
			         }
			         


			     }

			 }
			 
			 

		} catch (Exception e) {

		}
	}
	
	
	public static String readStringFromURL(WebDriver driver, String requestURL, int status) throws Exception 
	{
		
		String resul = "";
		List<String> resultado = new ArrayList<>();

		if ((status==409 || status==500) && requestURL.contains("restASO"))
		{
		
			Utils.openNewTab(driver);
			Utils.switchToTab(driver, 1);
			driver.get(requestURL);
			Thread.sleep(1000);
			resul = Utils.devuelveTextoDelVisible(driver, "//pre");
			String respuesta[] = resul.split(",");
			
			for (int i = 0; i < respuesta.length; i++) {
				
				if (respuesta[i].contains("consumerRequestId")){
					resultado.add(respuesta[i]);
				}
				
				else if (respuesta[i].contains("systemErrorDescription")){
					resultado.add(respuesta[i]);
				}
				
				else if (respuesta[i].contains("systemErrorCode")){
					resultado.add(respuesta[i]);
				}
					
				
			}
			
			Utils.switchToTab(driver, 0);
		}
		
		return resultado.toString().replace("[", "").replace("]", "");
	}
	
	
	public static boolean noEsImagenNiCss (String message) {
		return 
				message.indexOf(".jpeg")==-1 && 
				message.indexOf(".jpg")==-1 &&
				message.indexOf(".png")==-1 &&
				message.indexOf(".css")==-1 &&
				message.indexOf("common/styles/")==-1 &&
				message.indexOf(".js")==-1 &&	
				message.indexOf(".json")==-1 &&
				message.indexOf("min.js")==-1;
	}
	
	
	public static String devuelveNombreConstante(String xpath) {

		String nombreConstante = xpath;

		try {

			Constantes t = new Constantes();
			for (Field f : t.getClass().getFields()) {
				if (f.get(t).toString().equalsIgnoreCase(xpath)) {
					nombreConstante = t.getClass().getSimpleName() +"." + f.getName();
					break;
				}
			}
		} catch (Exception e) {
			return xpath;
		}
		
		return nombreConstante;

	}
	
	public static int getDayOfWeek () {
		
	 	Calendar calendar = Calendar.getInstance();

        // Get the weekday and print it - starting on Sunday
        int weekday = calendar.get(Calendar.DAY_OF_WEEK);
        
        return weekday;

	}
	
public static void introducirTextoEnElVisibleShadow (WebDriver driver, List<String> listaShadows, String css, String texto) throws Exception{
		
		WebElement shadowRoot = devuelveElementoShadow(driver, listaShadows);
		
		try
		{
			List<WebElement> listaElementos = shadowRoot.findElements(By.cssSelector(css));
			WebElement elemento = null;
			
			boolean encontrado = false;

			for(int i = 0; i < listaElementos.size() && !encontrado; ++i)
			{
				if(listaElementos.get(i).isDisplayed() && listaElementos.get(i).getLocation().getX()>0 && listaElementos.get(i).getLocation().getY()>0){
					elemento = listaElementos.get(i);
					encontrado = true;
				}
			}
			
			elemento.clear();
			elemento.sendKeys(texto);
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + css);
		}
		
	}
	
	
	public static void clickEnElVisiblePorTextoShadow (WebDriver driver, List<String> listaShadows, String css, String texto) throws Exception {
		
		WebElement shadowRoot = devuelveElementoShadowPorTexto(driver, listaShadows, texto);
		
		try
		{
			List<WebElement> listaElementos = shadowRoot.findElements(By.cssSelector(css));
			WebElement elemento = null;
			
			boolean encontrado = false;

			for(int i = 0; i < listaElementos.size() && !encontrado; ++i)
			{
				if(listaElementos.get(i).isDisplayed() && listaElementos.get(i).getLocation().getX()>0 && listaElementos.get(i).getLocation().getY()>0){
					elemento = listaElementos.get(i);
					encontrado = true;
//					if (listaElementos.get(i).getText().contains(texto))	{				
//						encontrado = true;
//					}
				}
			}
			
			if (!encontrado)
				throw new Exception ("No se encontr� el elemento: "+ css + " con texto: "+ texto);
			
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", elemento);
			Thread.sleep(500);
			elemento.click();
			
		}
		catch(Exception e)
		{
			throw new Exception("ERROR tratando de clickar el elemento: " + css + " - Excepci�n: " + e.toString());
		}
	}
	
	public static void clickEnElVisibleShadow (WebDriver driver, List<String> listaShadows, String css) throws Exception {
		
		WebElement shadowRoot = devuelveElementoShadow(driver, listaShadows);
		
		try
		{
			Thread.sleep(1000);
			List<WebElement> listaElementos = shadowRoot.findElements(By.cssSelector(css));
			WebElement elemento = null;
			
			boolean encontrado = false;

			for(int i = 0; i < listaElementos.size() && !encontrado; ++i)
			{
				if(listaElementos.get(i).isDisplayed() && listaElementos.get(i).getLocation().getX()>0 && listaElementos.get(i).getLocation().getY()>0){
					elemento = listaElementos.get(i);
					encontrado = true;
				}
			}
			
			if (!encontrado)
				throw new Exception("No se encontr� el elemento: " + css);
			
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", elemento);
			Thread.sleep(500);
			elemento.click();
			
		}
		catch(Exception e)
		{
			throw new Exception("ERROR tratando de clickar el elemento: " + css + " - Excepci�n: " + e.toString());
		}
	}
	
	
	
	public static boolean isDisplayedCSS(WebDriver driver, String css, int tiempo) throws Exception
	{

		Thread.sleep(1000);
		driver.manage().timeouts().implicitlyWait(tiempo, TimeUnit.SECONDS);
		
		boolean resul = false;

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.cssSelector(css));

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					resul = true;
			}

		}
		catch(Exception e)
		{

		}
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		return resul;
		
//		 WebDriverWait wait = new WebDriverWait(driver, tiempo);
//		    try{
//		        return wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath))) != null;
//
//		    }catch (Exception te){
//		        return false;
//		    }
		
	}
	
	public static void esperaHastaApareceSinSalirCSS(WebDriver driver, String css, int tiempo) throws Exception
	{

		try
		{
			boolean encontrado = isDisplayedCSS(driver, css, 1);
			int contador = 0;
			
			while (!encontrado && contador<tiempo) {
				Thread.sleep(1000);
				encontrado = isDisplayedCSS(driver, css, 1);
				contador++;
			}

		}
		catch(Exception e)
		{

		}

	}
	
	public static WebElement devuelveElementoShadowPorTexto (WebDriver driver, List<String> listaShadows, String texto ) throws Exception {
		
		WebElement shadowRoot = null;
		List<WebElement> listaElementos;
		boolean encontrado = false;
		
		for(int i = 0; i < listaShadows.size(); ++i) {

			//Primera vez
			if (shadowRoot==null){
				esperaHastaApareceSinSalirCSS(driver, listaShadows.get(i), 5);
				listaElementos = driver.findElements(By.cssSelector(listaShadows.get(i)));
			}
			else
				listaElementos = shadowRoot.findElements(By.cssSelector(listaShadows.get(i)));
			
			WebElement elemento = null;
			
			
			if (listaElementos.size()==1) {
				if (i==listaShadows.size()-1){
					if (listaElementos.get(0).getText().contains(texto)){
						elemento = listaElementos.get(0);
						encontrado = true;
					}
		
				}
			
			else
				elemento = listaElementos.get(0);

			}
			
			else
			{
			
				for(int j = 0; j < listaElementos.size() && !encontrado; ++j)
				{
					
					if(listaElementos.get(j).isDisplayed() && listaElementos.get(j).getLocation().getX()>=0 && listaElementos.get(j).getLocation().getY()>=0){
						
						if (i==listaShadows.size()-1){
							if (listaElementos.get(j).getText().contains(texto)){
								elemento = listaElementos.get(j);
								encontrado = true;
							}
						}
						
						else
							elemento = listaElementos.get(j);
						
					}
						
				}
			}
			
			if (elemento!=null) {
				
				WebElement root = elemento;
				shadowRoot = expandRootElement(root, driver);
				
				
			}

		}
		
		return shadowRoot;
		
	}
	
	public static WebElement devuelveElementoShadow (WebDriver driver, List<String> listaShadows ) throws Exception {
		
		WebElement shadowRoot = null;
		List<WebElement> listaElementos;
		
		for(int i = 0; i < listaShadows.size(); ++i) {

			//Primera vez
			if (shadowRoot==null){
				esperaHastaApareceSinSalirCSS(driver, listaShadows.get(i), 5);
				listaElementos = driver.findElements(By.cssSelector(listaShadows.get(i)));
			}
			else
				listaElementos = shadowRoot.findElements(By.cssSelector(listaShadows.get(i)));
			
			WebElement elemento = null;
			
			for(int j = 0; j < listaElementos.size(); ++j)
			{
				if(listaElementos.get(j).isDisplayed() && listaElementos.get(j).getLocation().getX()>=0 && listaElementos.get(j).getLocation().getY()>=0)
					elemento = listaElementos.get(j);
			}
			
			if (elemento!=null) {
				
				WebElement root = elemento;
				shadowRoot = expandRootElement(root, driver);
				
				
			}

		}
		
		return shadowRoot;
		
	}

	
	public static WebElement expandRootElement(WebElement element, WebDriver driver) {
        WebElement ele = (WebElement) ((JavascriptExecutor) driver).executeScript("return arguments[0].shadowRoot",element);
		return ele;
	}
	
	public static String seleccionarUISelectorDevuelveTexto2(WebDriver driver, String xpath) throws Exception
	{
		Utils.clickEnElVisiblePrimero(driver, xpath + "//i");
//		Atomic.click(driver, xpath + "//i");
		Thread.sleep(1000);
		Utils.esperaHastaApareceSinSalir(driver, xpath + "/descendant::select-option/descendant::span", 10);
		List<WebElement> opcionesCombo = driver.findElements(By.xpath(xpath + "/descendant::select-option/descendant::span"));
		
		String textoADevolver = "";
		
		for(WebElement opcion : opcionesCombo)
		{
			Thread.sleep(2000);
			
			textoADevolver = opcion.getText();
			
			if (opcion.isDisplayed() && !textoADevolver.equalsIgnoreCase("")){
				opcion.click();
				break;
			}

		}
		
		return textoADevolver;
	}
	
	
	public static void CSVToTableCompatibilidad(java.util.Date hoy, String horaInicio, String horaFin, List<String> lista) throws Exception
	{
		
		//Hacer solo el informe si no hay running
		
		Thread.sleep(30000);
		
		if (numeroFicherosEnRutaConUnPatron("resources/",".running")==0) {
			
			//Redirigimos la salida estandar a la consola para no interferir en los casos
//			System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
		
			Utils.crearBloqueo("informeNuevo", 300);
			
			String generarFichero = params.getValorPropiedad("generarFichero");
			
			if(generarFichero!=null && generarFichero.equalsIgnoreCase("SI"))
			{
				@SuppressWarnings("rawtypes")
				Class utilidad = Class.forName("utils.ActualizarC204");
				junit.textui.TestRunner.run(utilidad);
			}
			
			Utils.CSVToTable(hoy, horaInicio, horaFin, lista);
			generaDatosEjecucionHTML(hoy, horaInicio, horaFin);
			List<Integer> totales = generaResumenHTML(hoy, horaInicio, horaFin, lista);
			generaInformeDetallado(totales.get(0),totales.get(1),totales.get(2),totales.get(3),lista);
			
			Utils.liberarBloqueo("informeNuevo");
		
		}
	}
	
	public static Double getOnlyDigitosPuntosComasFromString(String cadena)
	{
		String caracteres = cadena.replaceAll("[^0-9&,.]", "");

		Double numero = 0.0;
		
	
		
		try
		{
			 //separador de miles es una coma - 234,456.33
			if ((caracteres.indexOf(",")<caracteres.indexOf("."))){
				
				  NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
				  ParsePosition parsePosition = new ParsePosition(0);
				  Number number = numberFormat.parse(caracteres, parsePosition);

				  if(parsePosition.getIndex() != caracteres.length()){
				    throw new ParseException("Invalid input", parsePosition.getIndex());
				  }

				  numero = number.doubleValue();
			
				  //separador de miles es un punto pero no hay comas
				
				
			} else if ((caracteres.indexOf(",")>caracteres.indexOf("."))){
				
				 
				  NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.GERMAN);
				  ParsePosition parsePosition = new ParsePosition(0);
				  Number number = numberFormat.parse(caracteres, parsePosition);

				  if(parsePosition.getIndex() != caracteres.length()){
				    throw new ParseException("Invalid input", parsePosition.getIndex());
				  }

				  numero = number.doubleValue();
			}
			
			else {
				NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.GERMAN);
				  ParsePosition parsePosition = new ParsePosition(0);
				  Number number = numberFormat.parse(caracteres, parsePosition);

				  if(parsePosition.getIndex() != caracteres.length()){
				    throw new ParseException("Invalid input", parsePosition.getIndex());
				  }

				  numero = number.doubleValue();
			}
			
		}

		catch(Exception e)
		{
			numero = 0.0;
		}
		
		return numero;

		
	}
	
	public static void EjecutarSSH(String pUser, String pPass, String pHost, int pPort, String pComando) throws Exception {

		Log.write("<html><h3 style=\"color: #0000FF;\">"+pComando+" </h3></html>");
		Log.write("[INICIO]: Se procede a ejecutar el siguiente comando por SSH: "+ pComando);
		int timeOut = 1500;
				
		JSch ssh = new JSch();
		// Instancio el objeto session para la transferencia
		Session session = null;
		// instancio el canal sftp
		ChannelExec channelssh = null;
		try {
			// Inciciamos el JSch con el usuario, host y puerto
			session = ssh.getSession(pUser, pHost, pPort);
			// Seteamos el password
			session.setPassword(pPass);
			// El SFTP requiere un intercambio de claves
			// con esta propiedad le decimos que acepte la clave
			// sin pedir confirmaci�n
			Properties prop = new Properties();
			prop.put("StrictHostKeyChecking", "no");
			prop.put("PreferredAuthentications", "publickey,keyboard-interactive,password");
			session.setConfig(prop);
			session.connect();
 
			// Abrimos el canal de sftp y conectamos
			channelssh = (ChannelExec) session.openChannel("exec");
	
			// seteamos el comando a ejecutar
			channelssh.setCommand("cd /decdr/IRelease_CDR/;" +pComando);
			// conectar y ejecutar
			channelssh.connect();
			
			//TODO Loop until channel.isClosed = true	
			InputStream in = channelssh.getInputStream();
			int contador = 0;
			 byte[] tmp = new byte[1024];
			  while (true && contador<timeOut)
			  {
			    while (in.available() > 0)
			    {
			      int i = in.read(tmp, 0, 1024);
			      if (i < 0)
			        break;
			      System.out.print(new String(tmp, 0, i));
			    }
			    if (channelssh.isClosed())
			    {
			    	
			      if (channelssh.getExitStatus()==0) {
			    	  Log.write("<html><b style=\"color: #329932;\"> [FIN]: exit-status: " + channelssh.getExitStatus() + " </b></html>");
			    	  Log.write("<html><b style=\"color: #329932;\"> [OK]: Finished correctly </b></html>");
			      }
			     
			      else {
			    	  
			    	  Log.write("<html><b style=\"color: #ff0000;\"> [ERROR]: exit-status: " + channelssh.getExitStatus() + " </b></html>");
			    	  throw new Exception ("<html><b style=\"color: #ff0000;\"> Revisar error y reintentar. Disk full? (o no existe carpeta en Servidor)</b></html>");
			      }
			      
			      break;
			    }
			    try
			    {
			      Thread.sleep(1000);
			      contador++;
			    }
			    catch (Exception ee)
			    {
			    	
			    }
			  }
			  
			  if (contador>=timeOut) throw new Exception ("<html><b style=\"color: #ff0000;\"> [ERROR]: No se complet� el comando correctamente("+pComando+"). TimeOut </b></html>");
			
			
		} catch (Exception e) {
		
			throw new Exception(e);
			
			
		} finally {
			// Cerramos el canal y session
			if (channelssh.isConnected())
				channelssh.disconnect();
			if (session.isConnected())
				session.disconnect();
		}
	}
	
	public static String getFirstDayNextMonth () {
		Calendar calendar = Calendar.getInstance();         
		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		Date nextMonthFirstDay = calendar.getTime();

		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		String formattedDate = fmt.format(nextMonthFirstDay);
		return formattedDate;
	}
	
	public static void getGridData (String ipHUB, String sessionId) throws Exception {
	    
		Host hub = new Host(ipHUB);
	    GridApiAssistant assistant = new GridApiAssistant(hub);
	    HubConfiguration hubConfig = assistant.getHubConfiguration();
	    hubConfig.getCapabilityMatcher();
	    
        //Now lets query the Hub to figure out to which node did the hub route our test to.
        Host node = assistant.getNodeDetailsForSession(sessionId);

        System.out.println("Test routed to " + node.toString());
        //Lets check what does the node configuration look like.
        NodeConfiguration nodeConfig = assistant.getNodeConfigForSession(node);
        //Here's how we get hold of the capabilities that are supported by this node.
        
        File fileToUpload = new File("C:\\Users\\xe39619\\Desktop\\temp\\aa.txt");
        String fileLocation = assistant.uploadFileToNode(sessionId, fileToUpload);
        
        FileReader fr = new FileReader(fileLocation);
        
        int i; 
        while ((i=fr.read()) != -1) 
          System.out.print((char) i); 
        
        fr.close();
        
        System.out.println("aaa");
        
	}
	
	
	
	public static void esperarSiGridSobrecargado (String ipHUB){
			
		try {
			Host hub = new Host(ipHUB);
			GridApiAssistant assistant = new GridApiAssistant(hub);
			HubConfiguration hubConfig = assistant.getHubConfiguration();
			hubConfig.getCapabilityMatcher();
			
			if (hubConfig.getSlotCount().getFree() <= 3)
				Thread.sleep(8000);
		} catch (Exception e) {
			
		}

		
	}
	
	
public static RemoteWebDriver connectViaProxy(Capabilities caps, String ipHub) {
        
//		String proxyHost = "cacheedi1.igrupobbva";
//        int proxyPort = 8080;
//        String proxyUserDomain = "ADBBVA";
//        String proxyUser = "xe39619";
//        String proxyPassword = devuelveClaveCifrada();
	
	String proxyHost = "proxyvip.igrupobbva";
    int proxyPort = 8080;
    String proxyUserDomain = "";
    String proxyUser = "xe39619";
    String proxyPassword = "atleti25";

        URL url;

        try {
            url = new URL(ipHub);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }

        
//        HttpClientBuilder builder = HttpClientBuilder.create();
//
//        HttpHost proxy = new HttpHost(proxyHost, proxyPort);
//
//        CredentialsProvider credsProvider = new BasicCredentialsProvider();
//
//        credsProvider.setCredentials(new AuthScope(proxyHost, proxyPort), new NTCredentials(proxyUser, proxyPassword, getWorkstation(), proxyUserDomain));
//
//        if (url.getUserInfo() != null && !url.getUserInfo().isEmpty()) {
//            credsProvider.setCredentials(new AuthScope(url.getHost(), (url.getPort() > 0 ? url.getPort() : url.getDefaultPort())), new UsernamePasswordCredentials(url.getUserInfo()));
//        }
//
//        builder.setProxy(proxy);
//        builder.setDefaultCredentialsProvider(credsProvider);
//
//        Factory factory = new MyHttpClientFactory(builder);
//
//        HttpCommandExecutor executor = new HttpCommandExecutor(new HashMap<String, CommandInfo>(), url, factory);

        
        HttpClientBuilder builder = HttpClientBuilder.create();

        HttpHost proxy = new HttpHost(proxyHost, proxyPort);

        CredentialsProvider credsProvider = new BasicCredentialsProvider();

        credsProvider.setCredentials(new AuthScope(proxyHost, proxyPort), new NTCredentials(proxyUser, proxyPassword, getWorkstation(), proxyUserDomain));

        if (url.getUserInfo() != null && !url.getUserInfo().isEmpty()) {
            credsProvider.setCredentials(new AuthScope(url.getHost(), (url.getPort() > 0 ? url.getPort() : url.getDefaultPort())), new UsernamePasswordCredentials(url.getUserInfo()));
        }

        builder.setProxy(proxy);
        builder.setDefaultCredentialsProvider(credsProvider);

        Factory factory = new MyHttpClientFactory(builder);

        HttpCommandExecutor executor = new HttpCommandExecutor(new HashMap<String, CommandInfo>(), url, factory);

        return new RemoteWebDriver(executor, caps);
        
        
        //return new RemoteWebDriver(url,caps);
    }

	private static  String getWorkstation() {
	    Map<String, String> env = System.getenv();
	
	    if (env.containsKey("COMPUTERNAME")) {
	        // Windows
	        return env.get("COMPUTERNAME");         
	    } else if (env.containsKey("HOSTNAME")) {
	        // Unix/Linux/MacOS
	        return env.get("HOSTNAME");
	    } else {
	        // From DNS
	        try
	        {
	            return InetAddress.getLocalHost().getHostName();
	        }
	        catch (UnknownHostException ex)
	        {
	            return "Unknown";
	        }
	    }
	}
	
	
	
	public static WebDriver getRemoteDriver(String ipHUB, String m_nombre, String... navegador) throws Exception
	{
								
			RemoteWebDriver driver = null;
			Capabilities options = null;
		
			TestProperties params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();

			// Leemos del properties el navegador que se nos ha indicado
			String pNavegador = params.getValorPropiedad("browser");
			
			if (navegador.length!=0)
				pNavegador = navegador[0];
			
			String entorno = params.getValorPropiedad("entorno");			
			
			// Obtenemos el proxy si lo hubiera
			String proxy = params.getValorPropiedad("proxy");

			if(proxy == null)
				proxy = "";


			//FIREFOX
			if(pNavegador.indexOf("firefox") > -1)
			{

				FirefoxOptions optionsFirefox = new FirefoxOptions();
				FirefoxProfile profile = new FirefoxProfile();
				 
				profile.setAcceptUntrustedCertificates(true);
				profile.setPreference("browser.download.folderList", 2);
				profile.setPreference("browser.download.dir", Constantes.carpetaDescargaGalatea);
				profile.setPreference(
						"browser.helperApps.neverAsk.saveToDisk",
						"image/jpg,text/csv,text/xml,application/xml,application/vnd.ms-excel,application/x-excel,application/x-msexcel,application/excel,application/pdf,application/octet-stream,application/rar,text/plain,application/text,application/zip");

				profile.setPreference("browser.helperApps.alwaysAsk.force", false);
				profile.setPreference("pdfjs.disabled", true);
				profile.setPreference("browser.startup.homepage", "");

			     
//			     profile.setPreference("browser.helperApps.alwaysAsk.force", false);
//			     profile.setPreference("browser.download.manager.showWhenStarting",false);

				 optionsFirefox.setCapability(FirefoxDriver.PROFILE, profile);
				
				//En duda
//				optionsFirefox.setCapability("useAutomationExtension", false);
//				optionsFirefox.setCapability("w3c", false); //
				
	            //Solo en integrado
	            if (entorno.equalsIgnoreCase("Integrado"))
	            	optionsFirefox.setCapability("tunnelId", "netcash");
	            
	            LoggingPreferences logPrefs = new LoggingPreferences();
	            logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
				
	            optionsFirefox.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
				optionsFirefox.setCapability("idleTimeout", 240);
				optionsFirefox.setCapability("noTunnelDomains", "*.com");
				optionsFirefox.setCapability("noTunnelDomains", "*.es");
				
				options = optionsFirefox;
				

			}
			
			//GOOGLE CHROME
			else if(pNavegador.equals("googlechrome"))
			{
				
//				System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");

				HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
				chromePrefs.put("profile.default_content_settings.popups", 0);
				
				chromePrefs.put("download.prompt_for_download", false);
				chromePrefs.put("download.directory_upgrade", true);
	
				ChromeOptions optionsChrome = new ChromeOptions();
				optionsChrome.setExperimentalOption("prefs", chromePrefs);
				optionsChrome.addArguments("enable-automation");
				optionsChrome.addArguments("start-maximized");
				optionsChrome.addArguments("disable-popup-blocking");
				optionsChrome.addArguments("disable-default-apps"); 
				optionsChrome.setExperimentalOption("useAutomationExtension", false);
				optionsChrome.setExperimentalOption("w3c", false);
				optionsChrome.addArguments("ignore-certificate-errors");
				
	            LoggingPreferences logPrefs = new LoggingPreferences();
	            logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
				
	            optionsChrome.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
	            optionsChrome.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
	            optionsChrome.setCapability("name", m_nombre);
	            
				String descargarPDFExterno = params.getValorPropiedad("descargarPDFExterno");
				
				if(descargarPDFExterno != null){
					if (descargarPDFExterno.equalsIgnoreCase("SI"))	{
						chromePrefs.put("plugins.always_open_pdf_externally", true);
					}
				}
	            
	            //Solo en integrado
	            if (entorno.equalsIgnoreCase("Integrado"))
	            	optionsChrome.setCapability("tunnelId", "netcash");
	            
	            optionsChrome.setCapability("idleTimeout", 240);
	            optionsChrome.setCapability("noTunnelDomains", "*.com");
	            optionsChrome.setCapability("noTunnelDomains", "*.es");

				options = optionsChrome;


			}
			
			else if(pNavegador.equals("*iehta") || pNavegador.indexOf("iexplore") > -1)
			{
								 
//				// Initialise browser
//				InternetExplorerOptions optionsIE = new InternetExplorerOptions();
//				
//				optionsIE.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING,false);
//				optionsIE.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, false);
//				optionsIE.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, true);
//				
//				//DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
//				optionsIE.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
//
//				// Set ACCEPT_SSL_CERTS  variable to true
//				optionsIE.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
//				optionsIE.setCapability("idleTimeout", 240);
//				optionsIE.setCapability("noTunnelDomains", "*.com");
//				optionsIE.setCapability("name", m_nombre);
//
//				// Set capability of IE driver to Ignore all zones browser protected mode settings.
//				optionsIE.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
//				
				MutableCapabilities sauceOptions = new MutableCapabilities();
				InternetExplorerOptions browserOptions = new InternetExplorerOptions();
				browserOptions.setCapability("platformName", "Windows 7");
				browserOptions.setCapability("browserVersion", "11.0");
				browserOptions.setCapability("noTunnelDomains", "*.com");
				browserOptions.setCapability("noTunnelDomains", "*.es");
				browserOptions.setCapability("sauce:options", sauceOptions);
				
				
	            //Solo en integrado
	            if (entorno.equalsIgnoreCase("Integrado"))
	            	browserOptions.setCapability("tunnelId", "netcash");
				
				options= browserOptions;

			}

			//1 Reintento
			try {
				
				HttpCommandExecutor cmd = new HttpCommandExecutor(
						new HashMap(), new URL(ipHUB) , new DefaultHttpFactory());
				driver = new RemoteWebDriver(cmd, options);


			} catch (WebDriverException e) {
				HttpCommandExecutor cmd = new HttpCommandExecutor(
						new HashMap(), new URL(ipHUB) , new DefaultHttpFactory());
				driver = new RemoteWebDriver(cmd, options);
			}
			
			driver.setFileDetector(new LocalFileDetector());
			
//			driver.manage().window().maximize();
			
			if (Allure.getLifecycle().getCurrentTestCase().isPresent()) {
			
				String sessionId = ((RemoteWebDriver) driver).getSessionId().toString();
				
	            final String browserName = ((RemoteWebDriver) driver).getCapabilities().getBrowserName();
	            final String browserVersion = ((RemoteWebDriver) driver).getCapabilities().getVersion();
	            final String platform = ((RemoteWebDriver) driver).getCapabilities().getPlatform().toString();
	            String sessionParameter = java.text.MessageFormat.format("'{'\"sessionId\":\"{0}\", \"testId\":\"{1}\"'}'",
	                    sessionId, sessionId);
	            String capabilitiesParameter = java.text.MessageFormat.format("'{'\"deviceName\":\"{0}\", \"platformName\":\"{1}\", \"browserName\":\"{2}\", \"version\":\"{3}\", \"platformName\":\"{4}\"'}'",
	                    "", "", browserName, browserVersion, platform);
	
	            Allure.parameter("_session",sessionParameter);
	            Allure.parameter("_capabilities",capabilitiesParameter);
			
			}
		
			return driver;

		
	}

	
	public static WebDriver inicializarCasoRemote(WebDriver driver, String m_codigo, String m_nombre, String browser) throws Exception
	{

		try
		{
			
			PrintStream ficheroMain = new PrintStream(new BufferedOutputStream(new FileOutputStream("results/Main.txt", true)), true);

			ficheroMain.append("[INFO] [INICIO]" + Utils.GetTodayDateAndTime() + " Lanzando caso: " + m_nombre + ", codigo: " + m_codigo + "\n");
			ficheroMain.close();

//			System.setOut(ficheroSalida);
//			System.setErr(ficheroSalida);
			
			loggers.put(Long.toString(Thread.currentThread().getId()), new Log(m_nombre));
			
			TestProperties params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();
			String Node = params.getValorPropiedad("ipHub");//"http://15.30.172.157:4444/wd/hub";
			
			if (Node==null){
				String ipLocal = Inet4Address.getLocalHost().getHostAddress();
				Node = "http://"+ipLocal+":4444/wd/hub";
			}
			
			String archivoCSV = params.getValorPropiedad("rutaResultados");
			String pais = params.getValorPropiedad("pais");
			String entorno = params.getValorPropiedad("entorno");
			String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
			String url = Utils.devuelveURLApartirDePaisYEntorno(rutaURLs, pais, entorno);

			// Pintamos el inicio de la traza
			 Utils.pintarInicioTraza(m_codigo, browser);
			 Utils.CsvAppend(archivoCSV, m_codigo, m_nombre, "ERROR", "", "");
			 driver = Utils.getRemoteDriver(Node, browser);
			 driver.get(url);
			
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			Utils.quit(driver);
		}

		return driver;
	}
	
	public static Collection<String[]> devuelveDataRemote () {
		TestProperties params = new TestProperties(Utils.getProperties());
	    params.cargarPropiedades();

		Collection<String[]> datos = new ArrayList<String[]>();

	    String navegadores = params.getValorPropiedad("browser");
	    ArrayList<String> myList = new ArrayList<String>(Arrays.asList(navegadores.split(",")));

	    for (int i = 0; i < myList.size(); i++) {
	      datos.add(new String[] { (String)myList.get(i) });
	    }
	    
	    return datos;
	}
	
	public static Collection<String[]> devuelveDataCollection (String property) {
		TestProperties params = new TestProperties(Utils.getProperties());
	    params.cargarPropiedades();

		Collection<String[]> datos = new ArrayList<String[]>();

	    String properties = params.getValorPropiedad(property);
	    ArrayList<String> myList = new ArrayList<String>(Arrays.asList(properties.split(",")));

	    for (int i = 0; i < myList.size(); i++) {
	      datos.add(new String[] { (String)myList.get(i) });
	    }
	    
	    return datos;
	}
	
	public static List<String> devuelveDataRemote2 () {
		
		TestProperties params = new TestProperties(Utils.getProperties());
	    params.cargarPropiedades();

	    String navegadores = params.getValorPropiedad("browser");

	    return Arrays.asList(navegadores.split(","));
	}
	
	public static WebElement devuelveElementoContieneUnTexto (WebDriver driver, String xpath, String texto) {
		
		List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
		WebElement elemento = null;
		boolean encontrado = false;		
		
		for(int i = 0; i < listaElementos.size(); ++i)
		{
			if(listaElementos.get(i).isDisplayed() && !encontrado){
				elemento = listaElementos.get(i);
				
				if (elemento.getText().contains(texto))
					encontrado = true;
				
			}
			
		}
		
		return elemento;

	}
	
	 public static boolean compruebaFormatoDecimal(String string, String separador) {
		    return string.matches("[0-9]+\\"+separador+"[0-9]+");
		  }
	 
		public static boolean isWindows() {

			return (OS.indexOf("win") >= 0);

		}

		public static boolean isMac() {

			return (OS.indexOf("mac") >= 0);

		}

		public static boolean isUnix() {

			return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
			
		}

		public static boolean isSolaris() {

			return (OS.indexOf("sunos") >= 0);

		}
		
		
		public static Double parseDouble (String cadena) throws ParseException {
			
			String caracteres = cadena.replaceAll("[^0-9&,.]", "");

			Double numero = 0.0;
			
		
			
			try
			{
				 //separador de miles es una coma - 234,456.33
				if ((caracteres.indexOf(",")<caracteres.indexOf("."))){
					
					  NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
					  ParsePosition parsePosition = new ParsePosition(0);
					  Number number = numberFormat.parse(caracteres, parsePosition);

					  if(parsePosition.getIndex() != caracteres.length()){
					    throw new ParseException("Invalid input", parsePosition.getIndex());
					  }

					  numero = number.doubleValue();
				
					  //separador de miles es un punto pero no hay comas
					
					
				} else if ((caracteres.indexOf(",")>caracteres.indexOf("."))){
					
					 
					  NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.GERMAN);
					  ParsePosition parsePosition = new ParsePosition(0);
					  Number number = numberFormat.parse(caracteres, parsePosition);

					  if(parsePosition.getIndex() != caracteres.length()){
					    throw new ParseException("Invalid input", parsePosition.getIndex());
					  }

					  numero = number.doubleValue();
				}
				
				else {
					NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.GERMAN);
					  ParsePosition parsePosition = new ParsePosition(0);
					  Number number = numberFormat.parse(caracteres, parsePosition);

					  if(parsePosition.getIndex() != caracteres.length()){
					    throw new ParseException("Invalid input", parsePosition.getIndex());
					  }

					  numero = number.doubleValue();
				}
				
			}

			catch(Exception e)
			{
				numero = 0.0;
			}
			
			return numero;
		}
		
		public static double round(double value, int places) {
		    if (places < 0) throw new IllegalArgumentException();

		    BigDecimal bd = new BigDecimal(value);
		    bd = bd.setScale(places, RoundingMode.HALF_UP);
		    return bd.doubleValue();
		}
		
		public static boolean insideBBVA() {
		    try {
		        final URL url = new URL("https://eup-grupobbva2.igrupobbva/");
		        final URLConnection conn = url.openConnection();
		        conn.connect();
		        conn.getInputStream().close();
		        return true;
		    } catch (MalformedURLException e) {
		        throw new RuntimeException(e);
		    } catch (SSLHandshakeException e) {
		        return true;
		    } catch (IOException e) {
				return false;
			}
		}

		public static boolean existeElementoShadowPorTexto (WebDriver driver, List<String> listaShadows, String texto ) throws Exception {
			
			WebElement shadowRoot = null;
			List<WebElement> listaElementos;
			boolean encontrado = false;
			
			for(int i = 0; i < listaShadows.size(); ++i) {

				//Primera vez
				if (shadowRoot==null){
					Utils.esperaHastaApareceSinSalirCSS(driver, listaShadows.get(i), 5);
					listaElementos = driver.findElements(By.cssSelector(listaShadows.get(i)));
				}
				else
					listaElementos = shadowRoot.findElements(By.cssSelector(listaShadows.get(i)));
				
				WebElement elemento = null;
				
				
				if (listaElementos.size()==1) {
					if (i==listaShadows.size()-1){
						if (listaElementos.get(0).getText().contains(texto)){
							elemento = listaElementos.get(0);
							encontrado = true;
						}
			
					}
				
				else
					elemento = listaElementos.get(0);

				}
				
				else
				{
				
					for(int j = 0; j < listaElementos.size() && !encontrado; ++j)
					{
						
						if(listaElementos.get(j).isDisplayed() && listaElementos.get(j).getLocation().getX()>=0 && listaElementos.get(j).getLocation().getY()>=0){
							
							if (i==listaShadows.size()-1){
								if (listaElementos.get(j).getText().contains(texto)){
									elemento = listaElementos.get(j);
									encontrado = true;
								}
							}
							
							else
								elemento = listaElementos.get(j);
							
						}
							
					}
				}
				
				if (elemento!=null) {
					
					WebElement root = elemento;
					shadowRoot = Utils.expandRootElement(root, driver);
					
					
				}

			}
			
			return encontrado;
			
		}

	
		public static void esperaHastaShadowContieneTexto (WebDriver driver, List<String> listaShadows, String texto, int timeout) throws Exception {
			
			Utils.setTimeOut(driver, 1);
			
			int contador = 0;
			
			while (!existeElementoShadowPorTexto(driver, listaShadows, texto) && contador<timeout){
				Thread.sleep(1000);
				contador++;
			}
			
			Utils.setDefaultTimeOut(driver);
		}
		
		public static void listf(String directoryName, List<File> files) {
		    File directory = new File(directoryName);

		    // Get all files from a directory.
		    File[] fList = directory.listFiles();
		    if(fList != null)
		        for (File file : fList) {      
		            if (file.isFile()) {
		                files.add(file);
		            } else if (file.isDirectory()) {
		                listf(file.getAbsolutePath(), files);
		            }
		        }
		    }
		
		
		public static WebElement buscarShadowPorAtributoChrome (WebDriver driver, List<String> listaShadows, String atributoABuscar ) throws Exception {
			
			WebElement shadowRoot = null;
			List<WebElement> listaElementos;
			WebElement elemento = null;
			
			for(int i = 0; i < listaShadows.size(); ++i) {

				//Primera vez
				if (shadowRoot==null){
					esperaHastaApareceSinSalirEnChrome(driver, listaShadows.get(i), 5);
					listaElementos = driver.findElements(By.cssSelector(listaShadows.get(i)));
				}
				else
				{
					if(i==1) //Si se llega al final de la lista de shadows, entonces se busca el css
					{
						
						listaElementos = shadowRoot.findElements(By.cssSelector(atributoABuscar));
					}
					else
						listaElementos = shadowRoot.findElements(By.cssSelector(listaShadows.get(i)));
				}
				
				
				
				for(int j = 0; j < listaElementos.size(); ++j)
				{
					if(listaElementos.get(j).isDisplayed() && listaElementos.get(j).getLocation().getX()>=0 && listaElementos.get(j).getLocation().getY()>=0)
					{
						if(i==listaShadows.size()-1) //Se buscan los shadows que correspondan al �ltimo ingresado.
						{
							elemento = listaElementos.get(j);
							break;
						}
						else
							elemento = listaElementos.get(j);
					}
				}
				
				if (elemento!=null) {
					
					WebElement root = elemento;
					shadowRoot = expandRootElement(root, driver);
				}

			}
			
			return elemento;		
		}
		
		//m�todo nuevo
		public static void esperaHastaApareceSinSalirEnChrome(WebDriver driver, String css, int tiempo) throws Exception
		{

			try
			{
				boolean encontrado = isDisplayed(driver, css, 1);
				int contador = 0;
				
				while (!encontrado && contador<tiempo) {
					Thread.sleep(1000);
					encontrado = isDisplayed(driver, css, 1);
					contador++;
				}

			}
			catch(Exception e)
			{

			}

		}
		
		/**
		 * Pulsa un elemento contenido dentro de uno o varios shadow-root. El array css
		 * enviado como parametro debe corresponder a una �nica ramificaci�n del DOM.
		 * 
		 * @author Softtek-Automatizaci�n bbva
		 * @author Sergio Olmedo
		 * @version 1.0
		 * @param listOfCss.  Array con el css por el cual se llega a cada nodo cuyo
		 *                    shadow-root se debe desplegar.
		 * @param elementCss. Css del elemento que se quiere pulsar.
		 */
		public static void clickEnElElementoVisibleShadowJs(WebDriver driver, String[] listOfCss, String elementCss) throws Exception {

			JavascriptExecutor js = (JavascriptExecutor) driver;
			String script = "return document.querySelector(\"" + listOfCss[0] + "\")";
			for (int i = 1; i < listOfCss.length; i++) {
				script = script + ".shadowRoot.querySelector(\"" + listOfCss[i] + "\")";
			}
			script = script + ".shadowRoot.querySelector(\"" + elementCss + "\")";
			WebElement element = (WebElement) js.executeScript(script);
			if (element.isDisplayed() && element.getLocation().getX() >= 0 && element.getLocation().getY() >= 0) {
				element.click();
			} else {
				throw new Exception("El elemento no est� visible");
			}

		}

		/**
		 * Devuelve un elemento contenido en un shadow-root haciendo uso de su css. El
		 * array CSS enviado como parametro debe corresponderse a una �nica ramificaci�n
		 * del DOM. Dentro de esta ramificaci�n, el css del elemento buscado tambien
		 * deber� corresponder a un �nico elemento.
		 * 
		 * @author Softtek-Automatizaci�n bbva
		 * @author Sergio Olmedo
		 * @version 1.0
		 * @param listOfCss.  Array con el css por el cual se llega a cada nodo cuyo
		 *                    shadow-root se debe desplegar.
		 * @param elementCss. Css de los elementos que se quieren obtener.
		 */
		private WebElement devuelveElementoUnicoShadowJs(WebDriver driver, String[] listOfCss, String elementCss) {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			String script = "return document.querySelector(\"" + listOfCss[0] + "\")";
			for (int i = 1; i < listOfCss.length; i++) {
				script = script + ".shadowRoot.querySelector(\"" + listOfCss[i] + "\")";
			}
			script = script + ".shadowRoot.querySelector(\"" + elementCss + "\")";
			return (WebElement) js.executeScript(script);

		}

		/**
		 * Devuelve los nodos contenidos en un shadow-root que cumplen con el css
		 * enviado. El array CSS enviado como parametro debe corresponderse a una �nica
		 * ramificaci�n del DOM, siendo los nodos correspondientes al elemento buscado
		 * los que puedan tener m�s de una correspondencia.
		 * 
		 * @author Softtek-Automatizaci�n bbva
		 * @author Sergio Olmedo
		 * @version 1.0
		 * @param listOfCss.  Array con el css por el cual se llega a cada nodo cuyo
		 *                    shadow-root se debe desplegar.
		 * @param elementCss. Css de los elementos que se quieren obtener.
		 */
		public static ArrayList<WebElement> devuelveElementosShadowJs(WebDriver driver, String[] listOfCss, String elementCss) {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			String script = "return [].slice.call(document.querySelector(\"" + listOfCss[0] + "\")";
			for (int i = 1; i < listOfCss.length; i++) {
				script = script + ".shadowRoot.querySelector(\"" + listOfCss[i] + "\")";
			}
			script = script + ".shadowRoot.querySelectorAll(\"" + elementCss + "\"))";
			return (ArrayList<WebElement>) js.executeScript(script);

		}

		/**
		 * Devuelve los nodos contenidos en un shadow-root que cumplen con el css
		 * enviado. El array CSS enviado como parametro debe corresponderse a una �nica
		 * ramificaci�n del DOM, siendo los nodos correspondientes al elemento buscado
		 * los que puedan tener m�s de una correspondencia.
		 * 
		 * @author Softtek-Automatizaci�n bbva
		 * @author Sergio Olmedo
		 * @version 1.0
		 * @param listOfCss.  Array con el css por el cual se llega a cada nodo cuyo
		 *                    shadow-root se debe desplegar.
		 * @param elementCss. Css de los elementos que se quieren obtener.
		 * @param texto.      Texto que se desea introducir
		 * @throws Exception
		 */
		public static void introducirTextoEnElVisibleShadowJS(WebDriver driver, String[] listOfCss, String elementCss, String texto)
				throws Exception {
			try {
				List<WebElement> listaElementos = Utils.devuelveElementosShadowJs(driver, listOfCss, elementCss);
				WebElement elemento = null;

				boolean encontrado = false;

				for (int i = 0; i < listaElementos.size() && !encontrado; ++i) {
					if (listaElementos.get(i).isDisplayed() && listaElementos.get(i).getLocation().getX() > 0
							&& listaElementos.get(i).getLocation().getY() > 0) {
						elemento = listaElementos.get(i);
						encontrado = true;
					}
				}

				elemento.clear();
				elemento.sendKeys(texto);
			} catch (Exception e) {
				throw new Exception("ERROR. No se encontr� el elemento: " + elementCss);
			}
		}

		
		public static int devuelvePosicionPrimerDigito (String string) {
			int i = 0;
			while (i < string.length() && !Character.isDigit(string.charAt(i))) i++;

			return i;
		}
		
		public static void crearInformeHTML (List<String> listaCasosARELanzar, String... horaIniArray) throws Exception {
						
			// Fecha de inicio
			java.util.Date hoy = new java.util.Date();
			SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
			String horaInicio = formatter2.format(hoy);
			
			if (horaIniArray.length>0)
				horaInicio = horaIniArray[0];
				
			
			String formatoResultados = params.getValorPropiedad("formatoResultados");

			java.util.Date fin = new java.util.Date();
			String horaFin = formatter2.format(fin);
			
			if(formatoResultados.indexOf("html") > -1)
			{
				// Leemos la hora de inicio si alg�n hilo ya la dej�
				Utils.crearBloqueo("timeLock", 60);
				File time = new File("resources/time.txt");
				if(time.exists())
				{
					Scanner scanner = new Scanner(time, "UTF-8");
					String text = scanner.useDelimiter("\\A").next();
					horaInicio = text;
					scanner.close();
					time.delete();
				}
				Utils.liberarBloqueo("timeLock");
				Utils.CSVToTableNew(hoy, horaInicio, horaFin, listaCasosARELanzar);
			}
			
		}
		
	public static String clickAleatorioEnUnoDeLosVisiblesDevuelveTexto(WebDriver driver, String xpath) throws Exception {
		try {
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			List<WebElement> listaElementosVisibles = new ArrayList<WebElement>();

			for (int i = 0; i < listaElementos.size(); ++i) {
				if (listaElementos.get(i).isDisplayed()) {
					listaElementosVisibles.add(listaElementos.get(i));
				}
			}
			Random random = new Random();
			int randomNum = random.nextInt(listaElementosVisibles.size());

			Utils.clickJS(driver, listaElementosVisibles.get(randomNum));

			return listaElementosVisibles.get(randomNum).getText();
		} catch (Exception e) {
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}
	}
	
	public static void waitRandomTime (int min, int max) throws Exception {
		Random random = new Random();
		int myRandomNumber = random.nextInt(max*1000 - min*1000) + min*1000;
		Thread.sleep(myRandomNumber);
	}
	
    public static boolean isIpReachable(String targetIp) {
    	 
        boolean result = false;
        try {
            InetAddress target = InetAddress.getByName(targetIp);
            result = target.isReachable(2000);  
       
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return result;
    }
    
    public static boolean serverListening(String host, int port)
    {
        Socket s = null;
        try
        {
            s = new Socket(host, port);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
        finally
        {
            if(s != null)
                try {s.close();}
                catch(Exception e){}
        }
    }
    
    public static boolean RTS_Update (String referencia, String usuario) {
    	
    	try {
    		
    		usuario = usuario.toUpperCase();
    		
    		params.cargarPropiedades();
    		String entorno = params.getValorPropiedad("entorno");
    		
    		if (entorno.equalsIgnoreCase("Integrado") && Utils.insideBBVA()){
				    		
	    		String fechaAyer = Utils.GetDateMasNumeroDiasFormat("yyyy-MM-dd",-1);
				
				String updateFechaRTS = "update bdue.tbdueusa set TIM_CAMBSITU = '"+fechaAyer+" 00:00:00' where cod_logonapl = '"+referencia+"' and cod_usuario = '"+usuario+"'";
	
				String updateSMS = "UPDATE BDUE.TBDUEUSA SET XSN_TELCERTI='3' where cod_logonapl = '"+referencia+"' and cod_usuario = '"+usuario+"'";

				if (DB2Connection.update(updateFechaRTS,"KYGU_EI_DB2"))
					Log.write("Se actualiza la fecha del �ltimo acceso del usuario: "+usuario);
				
				if (DB2Connection.update(updateSMS,"KYGU_EI_DB2"))
					Log.write("Se actualiza la validaci�n por SMS/MAIL del usuario: "+usuario);
    		
    		}
			
			return true;
			
		} catch (Exception e) {
			return false;
		}
    }
    
    public static void setPropertyChromeVersion (WebDriver driver) {
    	//Idea. La versi�n se lee del properties. Si no coincide, el primer caso modificar�a el properties y
    	//avisar�a. Ir�a al cocheEscoba
    	
    }
    
    public static String getBrowserAndVersion(WebDriver driver) {

    	String browser_version = null;
    	Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
    	String browsername = cap.getBrowserName();
    	
    	// This block to find out IE Version number
    	if ("internet explorer".equalsIgnoreCase(browsername)) {
    		String uAgent = (String) ((JavascriptExecutor) driver).executeScript("return navigator.userAgent;");
    		System.out.println(uAgent);
    	
    		//uAgent return as �MSIE 8.0 Windows� for IE8
    		if (uAgent.contains("MSIE") && uAgent.contains("Windows")) {
    			browser_version = uAgent.substring(uAgent.indexOf("MSIE")+5, uAgent.indexOf("Windows")-2);
    		} else if (uAgent.contains("Trident/7.0")) {
    			browser_version = "11.0";
    		} else {
    			browser_version = "0.0";
    		}
    		} 
    	else
    	{
    		//Browser version for Firefox and Chrome
    		browser_version = cap.getVersion();// .split(�.�)[0];
    	}
    		String browserversion = browser_version.substring(0, browser_version.indexOf("."));
    	
    		return browsername + " " + browserversion;
    }
    
    public static void loginGoogleBBVA (WebDriver driver, String correo, String user, String pass) {
    	try
		{
    		
    		driver.get(Constantes.intranetBBVA);
    		
    		Utils.capturaBasica(driver, "LoginBBVA" , "Captura login Google/BBVA");
    		
    		if(Utils.isDisplayed(driver, Constantes.inputMail, 2))
			{
				Utils.introducirTextoEnElVisible2(driver, Constantes.inputMail, correo);
				Utils.clickEnElVisiblePrimero(driver, Constantes.botonSiguiente);
			}
    		
    		if(Utils.isDisplayed(driver, Constantes.loginUserBBVA, 10))
			{
				Utils.introducirTextoEnElVisible2(driver, Constantes.loginUserBBVA, user);
				Utils.introducirTextoEnElVisible2(driver, Constantes.passUserBBVA, pass);
				Utils.capturaBasica(driver, "LoginBBVA" , "Captura tras introducir user/pass BBVA");
				
				Utils.clickEnElVisiblePrimero(driver, Constantes.botonLoginBBVA);
				Utils.esperaHastaDesapareceSinSalir(driver, Constantes.loginUserBBVA);
				Utils.capturaBasica(driver, "LoginBBVA" , "Captura tras click en login BBVA");
				
			}
    		
    		if (Utils.isDisplayed(driver, Constantes.continuarGoogleBBVA, 5)){
				Utils.clickEnElVisiblePrimero(driver, Constantes.continuarGoogleBBVA);
				Utils.capturaBasica(driver, "LoginBBVA" , "Captura despu�s de continuar");
			}
    		
    		if(Utils.isDisplayed(driver, Constantes.inputMail, 2))
			{
				Utils.introducirTextoEnElVisible2(driver, Constantes.inputMail, correo);
				Utils.capturaBasica(driver, "LoginBBVA" , "Captura tras introducir mail");
				Utils.clickEnElVisiblePrimero(driver, Constantes.botonSiguiente);
			}
    		
    		
    		
//    		Utils.esperaHastaApareceSinSalir(driver, Constantes.inputMail, 10);
//    		
//    		Utils.capturaBasica(driver, "LoginBBVA" , "Captura login Google/BBVA");
//
//			if(Utils.isDisplayed(driver, Constantes.inputMail, 2))
//			{
//				Log.write("Aparece input mail");
//				Utils.introducirTextoEnElVisible2(driver, Constantes.inputMail, correo);
//				Utils.capturaBasica(driver, "LoginBBVA" , "Captura tras introducir mail");
//				Utils.clickEnElVisiblePrimero(driver, Constantes.botonSiguiente);
//				
//				
//
//				if(Utils.isDisplayed(driver, Constantes.loginUserBBVA, 10))
//				{
//					Log.write("Aparece login BBVA");
//					Utils.introducirTextoEnElVisible2(driver, Constantes.loginUserBBVA, user);
//					Utils.introducirTextoEnElVisible2(driver, Constantes.passUserBBVA, pass);
//					Utils.capturaBasica(driver, "LoginBBVA" , "Captura tras introducir user/pass BBVA");
//					Utils.clickEnElVisiblePrimero(driver, Constantes.botonLoginBBVA);
//					
//				}
//
//			}
//			
//			if (Utils.isDisplayed(driver, Constantes.continuarGoogleBBVA, 5)){
//				Utils.capturaBasica(driver, "LoginBBVA" , "Captura antes de continuar");
//				Utils.clickEnElVisiblePrimero(driver, Constantes.continuarGoogleBBVA);
//				Utils.capturaBasica(driver, "LoginBBVA" , "Captura despu�s de continuar");
//			}
			
			
//			////////////////////////////////
//			//Reintento////////////////////
//			////////////////////////////////
//			
//			Log.write("Posible reintento.");
//			
//			String codigoFuente = driver.getPageSource();
//			
//			Log.write("[INFO] - PageSource ------------------------------------------");
//			Log.write(codigoFuente);
//			Log.write("--------------------------------------------------------------");
//			
//			if(Utils.isDisplayed(driver, Constantes.inputMail, 5))
//			{
//				Log.write("[INFO] - Reintentamos el Login Google/BBVA");
//				Log.write("Aparece input mail");
//				Utils.introducirTextoEnElVisible2(driver, Constantes.inputMail, correo);
//				Utils.capturaBasica(driver, "LoginBBVA" , "Captura tras introducir mail");
//				Utils.clickEnElVisiblePrimero(driver, Constantes.botonSiguiente);
//				
//				
//
//				if(Utils.isDisplayed(driver, Constantes.loginUserBBVA, 10))
//				{
//					Log.write("Aparece login BBVA");
//					Utils.introducirTextoEnElVisible2(driver, Constantes.loginUserBBVA, user);
//					Utils.introducirTextoEnElVisible2(driver, Constantes.passUserBBVA, pass);
//					Utils.capturaBasica(driver, "LoginBBVA" , "Captura tras introducir user/pass BBVA");
//					Utils.clickEnElVisiblePrimero(driver, Constantes.botonLoginBBVA);
//					
//				}
//
//			}
//			
//			if (Utils.isDisplayed(driver, Constantes.continuarGoogleBBVA, 5)){
//				Utils.capturaBasica(driver, "LoginBBVA" , "Captura antes de continuar");
//				Utils.clickEnElVisiblePrimero(driver, Constantes.continuarGoogleBBVA);
//				Utils.capturaBasica(driver, "LoginBBVA" , "Captura despu�s de continuar");
//			}
			


		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
		}
    }
    
    public static String getSubstring (String cadenaOriginal, String lodeAntes, String lodeDespues) {
    	
    	String encontrado = "";
 
    	Pattern pattern = Pattern.compile(lodeAntes+"(.*?)"+lodeDespues);
    	Matcher matcher = pattern.matcher(cadenaOriginal);
    	if (matcher.find())
    	{
    	    encontrado = (matcher.group(1));
    	}
    	
    	return encontrado;
    	
    }
    
	public static void introducirTextoEnElVisible3(WebDriver driver, String xpath, String texto)
	{
	    WebElement element = Atomic.findElement(driver, xpath);
	    Actions navigator = new Actions(driver);
	    navigator.click(element)
	        .sendKeys(Keys.END)
	        .keyDown(Keys.SHIFT)
	        .sendKeys(Keys.HOME)
	        .keyUp(Keys.SHIFT)
	        .sendKeys(Keys.BACK_SPACE)
	        .sendKeys(texto)
	        .perform();
	}
	
	
	
	public static Proxy setProxy () {
		
		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Constantes.proxy_hostname, Constantes.proxy_port));
		
		Authenticator authenticator = new Authenticator() {

	        public PasswordAuthentication getPasswordAuthentication() {
	            return (new PasswordAuthentication(Constantes.proxy_user, Utils.devuelveClaveCifrada().toCharArray()));
	        }
	    };
	    Authenticator.setDefault(authenticator);
	    
	    return proxy;
	}
	
	private static class DefaultTrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }
	
	
	public static String anadirResultadoAPIResultadosAzure(String pais, String entorno, String navegador, String fecha, String horaInicio, String horaFin, String ejecutados, String ok, String error, String avisos, String csv, String html, String c204, String uuaas, String usuario, String tipoEjecucion, String casosEjecutados) throws Exception
	{
		
		String contentType = "application/json";

		TestApplet peticion = new TestApplet();


//		String uri = "https://resultados-testing.herokuapp.com/api/resultados";
		String uri = "https://app-resultsauto.northeurope.cloudapp.azure.com/api-qa/resultados";
		
		Proxy proxy = null;
		
		// Establecemos el proxy
		if (Utils.insideBBVA()) {
			proxy = setProxy();
		}
		
		 // configure the SSLContext with a TrustManager
        SSLContext ctx = SSLContext.getInstance("TLSv1.2");
        ctx.init(new KeyManager[0], new TrustManager[] {new DefaultTrustManager()}, new SecureRandom());
        SSLContext.setDefault(ctx);

        URL url = new URL(uri);
        
        HttpURLConnection conn;
        
		if (proxy==null)
        	conn = (HttpURLConnection) url.openConnection();
        else
        	conn = (HttpURLConnection) url.openConnection(proxy);
        
        
        conn.setRequestMethod("POST");
        
        if (!contentType.equals(""))
        	conn.setRequestProperty("Content-Type", contentType);
        
        
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestProperty("Authorization", peticion.getTokenAzure());

        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
        
//        Date ahora = new Date();
        
        GregorianCalendar calendar = new GregorianCalendar();

		// date now:
		Date now = calendar.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		String formattedDate = fmt.format(now);

		Date today = calendar.getTime();
		formattedDate = fmt.format(today);

        writer.write("{\n    \"pais\": \""+pais+"\",\n    "
        		+ "\"entorno\": \""+entorno+"\",\n    "
        		+ "\"navegador\": \""+navegador+"\",\n    "
        		+ "\"fecha\": \""+fecha+"\",\n    "
        		+ "\"horaInicio\": \""+horaInicio+"\",\n    "
        		+ "\"horaFin\": \""+horaFin+"\",\n    "
        		+ "\"ejecutados\": \""+ejecutados+"\",\n    "
        		+ "\"ok\": \""+ok+"\",\n    "
        		+ "\"error\": \""+error+"\",\n    "
        		+ "\"avisos\": \""+avisos+"\",\n    "
        		+ "\"csv\": \""+csv+"\",\n    "
        		+ "\"html\": \""+html+"\",\n    "
        		+ "\"c204\": \""+c204+"\",\n    "
        		+ "\"uuaas\": \""+uuaas+"\",\n    "
        		+ "\"usuario\": \""+usuario+"\", \n "
        		+ "\"tipoEjecucion\": \""+tipoEjecucion+"\", \n "
        		+ "\"casosEjecutados\": \""+casosEjecutados+"\", \n "
        		+ "\"sysdate\": \""+formattedDate+"\"  }");
        writer.flush();

        String line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        

        while ((line = reader.readLine()) != null) {
            Log.write(line);
        }

        
        
        writer.close();
        reader.close();         

       
        int status = conn.getResponseCode();

        
        conn.disconnect();

			
		return "";
	}
	
    public static String getClaveSms(String sessionId){
        Process process = null;
        String commandString="resources/adb shell cd sdcard; ls ClaveBBVA*";
        try {
            process = Runtime.getRuntime().exec(commandString);
        } catch (IOException e) {
        	System.out.println("Revisar configuracion Adb");
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String clave;
        String numerito = "";
        try {
        	while ((clave = reader.readLine()) != null) {
        		clave = clave.replaceAll("\\D+","");
        		numerito = clave;
        	}

        	System.out.println("Clave utilizada: " +numerito);
        	return numerito;
        } catch (IOException e) {
        	e.printStackTrace();
        	return "FALLO";
        }
  
    }
    
	public static void loginUsuarioPasswordRecocumentacion(WebDriver driver, String referencia, String usuario, String password) throws Exception
	{
		
		Usuario user = new Usuario(referencia);
		user.setCodUsuario(usuario);
		user.setPassword(password);
		
		Utils.loginRedocumentacion(driver, user);

	}
    
	public static void loginRedocumentacion(WebDriver driver, Usuario usuario) throws Exception
	{
		if (usuario==null)
			throw new Exception ("No se encontr� el usuario necesario para logarse en la aplicaci�n.");

		//Escribimos el usuario con el que nos estamos logando
		Utils.pintarRefernciaUsuario(usuario.getReferencia(), usuario.getCodUsuario());

		params.cargarPropiedades();
		String idioma = params.getValorPropiedad("idioma").toLowerCase();
		String pais = params.getValorPropiedad("pais").toLowerCase();
		String entorno = params.getValorPropiedad("entorno").toLowerCase();
		String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
		String url = devuelveURLApartirDePaisYEntorno(rutaURLs, pais, entorno);

		if(Utils.estamosEnLatam())
		{

			if(Utils.isDisplayed(driver, Constantes.LoginLocal_ComboPais, 5))
			{
				loginLocal(driver, usuario);
			}
			else
			{
				loginTipoV7(driver, usuario);
			}

		}

		else
		{

			if (Utils.isDisplayed(driver, Constantes.ComboIdiomas, 2)) {
				String idiomaActual = driver.findElement(By.xpath(Constantes.ComboIdiomas)).getText();
				if(idiomaActual.toLowerCase().indexOf(idioma) < 0)
				{
					Utils.cambiarIdioma(driver, idioma);
				}
			}

			//RTS!!!!! 
			//Si estamos en Integrado, podemos modificar la fecha de ultimo acceso
			Utils.RTS_Update(usuario.getReferencia(), usuario.getCodUsuario());

			if (Utils.isDisplayedSinBefore(driver, Constantes.cerrarModalLogin, 0))
				Utils.clickEnElVisiblePrimero(driver, Constantes.cerrarModalLogin);

			String currentUrlBrower=driver.getCurrentUrl();
			String auxBotonEntrar="";
			if (currentUrlBrower.contains("bbva.es/empresas.html")){

				utils.Log.write("Hacemos nuevo paso Login URL:  "+currentUrlBrower);
				Thread.sleep(2000);

				WebElement element  = driver.findElement( By.xpath("//header//*[contains(text(),'Acceso')]"));

				Utils.clickEnElVisiblePrimero(driver,"//header//*[contains(text(),'Acceso')]");
				Thread.sleep(2000);
				System.out.println("initSwitch:   "+java.time.LocalDateTime.now());  

				WebElement iframe = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("tab-empresas-iframe")));

				driver.switchTo().frame(iframe);

				System.out.println("endSwitch:   "+java.time.LocalDateTime.now());  
				auxBotonEntrar=Constantes.Login_BotonEntrar;

			}else{
				if(entorno.contains("integrado")){
					utils.Log.write("Login antiguo en Integrado:  "+currentUrlBrower);
					Utils.clickEnElVisiblePrimero(driver,"//*[@id='kyop-redirect-button']");
					utils.Log.write("Hacemos nuevo paso Login URL:  "+currentUrlBrower);
					Thread.sleep(2000);

					WebElement element  = driver.findElement( By.xpath("//header//*[contains(text(),'Acceso')]"));

					Utils.clickEnElVisiblePrimero(driver,"//header//*[contains(text(),'Acceso')]");
					Thread.sleep(2000);
					System.out.println("initSwitch:   "+java.time.LocalDateTime.now());  

					WebElement iframe = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.id("tab-empresas-iframe")));

					driver.switchTo().frame(iframe);

					System.out.println("endSwitch:   "+java.time.LocalDateTime.now());  
					auxBotonEntrar=Constantes.Login_BotonEntrar;

				}else{
					utils.Log.write("Login antiguo en Produccion:  "+currentUrlBrower);
					auxBotonEntrar=Constantes.Login_BotonEntrar_Ant;

				}

			}

			//Si se guardo el usuario
			if (Utils.isDisplayedSinBefore(driver, Constantes.changeUserLabel, 0))
				Utils.clickEnElVisiblePrimero(driver, Constantes.changeUserLabel);

			Utils.introducirTextoEnElVisible(driver, Constantes.Login_Referencia, usuario.getReferencia());

			Utils.introducirTextoEnElVisible(driver, Constantes.Login_Usuario, usuario.getCodUsuario());

			Utils.introducirTextoEnElVisible(driver, Constantes.Login_Password, usuario.getPassword());

			Utils.clickEnElVisiblePrimero(driver, auxBotonEntrar);

			Thread.sleep(1500);

			if (Utils.isDisplayed(driver, Constantes.botonActivarDatosPersonales, 5))
				Utils.clickEnElVisiblePrimero(driver, Constantes.botonActivarDatosPersonales);

		}

		if(Utils.isDisplayed(driver, Constantes.BotonOKMessageDialog, 3)){
			Utils.clickEnElVisible(driver, Constantes.BotonOKMessageDialog);
			Thread.sleep(4000);
		}
		
		if(Utils.isDisplayed(driver, Constantes.frameCampanaGenerico, 6))
		Utils.cambiarFrameDadoVisible(driver, Constantes.frameCampanaGenerico);

	}
	
    
	
}
