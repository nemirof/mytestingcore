package utils;


import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class DatosParaRest2 extends TestCase
{
	private WebDriver driver;

	public String m_nombre = this.getClass().getSimpleName();

	public String m_codigo = "REST_DATOS";

	private Resultados resultado;


	@Before
	public void setUp() throws Exception
	{
		resultado = new Resultados(m_nombre);
		driver = Utils.inicializarEjecucion(driver, m_codigo, m_nombre);
	}


	@Test
	public void testDatosParaRest() throws Exception
	{

		UtilsRest.borrarFicheroRest();

		try
		{

			new Constantes();

			Utils.loginPorDefecto(driver);

			TestProperties params = new TestProperties(UtilsRest.getProperties());
			params.cargarPropiedades();

			TestProperties params1 = new TestProperties(Utils.getProperties());
			params1.cargarPropiedades();
			String entorno = params1.getValorPropiedad("entorno");

			if(entorno.equalsIgnoreCase("Produccion"))
			{
				Log.write("La prueba de los servicios REST s�lo aplica en Entornos de Desarrollo e Integrado");
			}

			else
			{

				try
				{
					// ADM APODERADO 3

					Usuario AdmApoderado3 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado3 == null)
					{
						AdmApoderado3 = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado3.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado3.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioAdmin***", AdmApoderado3.getCodUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioAdmin***");
				}

				try
				{
					// NO ADM APODERADO 3

					Usuario UsuarioNoAdmin = Funciones.devuelveUsuarioPorTipo("NO", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(UsuarioNoAdmin == null)
					{
						UsuarioNoAdmin = Funciones.altaUsuarioFirmanteSinTokenTodoContratado(driver, resultado);
					}
					else
					{
						UsuarioNoAdmin.setEstado(Constantes.ESTADO_NO_USAR);
						UsuarioNoAdmin.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuarioAdmin**", UsuarioNoAdmin.getCodUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuarioAdmin**");
				}

//			  try {
//					//ADM SIN PODERES
				//
//					Usuario UsuarioNoAdmin = Funciones.altaAdministrador(driver, resultado, "NO");
//						  
//					params.updateProperties(UtilsRest.getProperties(), "codUsuarioValidador*****", UsuarioNoAdmin.getCodUsuario());
//				
//				  } catch (Exception e) {
//					  Log.write("Error creando un usuario, codUsuarioValidador*****");
//				  }
//			  

				try
				{
					// ADM APODERADO

					Usuario AdmApoderado4 = Funciones.altaUsuarioTodoContratado(driver, resultado);

					AdmApoderado4.setEstado(Constantes.ESTADO_NO_USAR);
					AdmApoderado4.guardarUsuarioEnCSV();


					params.updateProperties(UtilsRest.getProperties(), "codUsuario*", AdmApoderado4.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario*");
				}

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "codUsuario**", AdmPendiente.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario**");
				}

				try
				{
					// ADM BLOQUEADO CON ACCIONES PENDIENTES

					Usuario AdmBloqueado = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "NO", Constantes.ESTADO_BLOQ);

					if(AdmBloqueado == null)
					{
						AdmBloqueado = Funciones.bloquearUsuario(driver, resultado);
					}
					else
					{
						AdmBloqueado.setEstado(Constantes.ESTADO_NO_USAR);
						AdmBloqueado.guardarUsuarioEnCSV();
					}

					Utils.accederAdmUsuarios(driver);
					// Cambiar a frame
					WebElement frame = driver.findElement(By.xpath(Constantes.FramePrincipal));
					driver.switchTo().frame(frame);
					driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
					driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(AdmBloqueado.getCodUsuario());
					Utils.clickEnElVisible(driver, Constantes.BotonBuscar);

					Utils.esperarVisibleTiempo(driver, Constantes.ResultadosBusqueda, 20);
					// Seleccionar usuario filtrado
					Utils.clickEnElVisible(driver, Constantes.RadioSeleccionUsuarioFiltrado);
					Utils.clickEnElVisible(driver, Constantes.BotonEditar);
					// Validar t�tulo de la p�gina
					String titulo = Utils.devuelveTextoDelVisiblePrimero(driver, Constantes.TituloPantalla);
					Utils.vpWarning(driver, resultado, "Validar t�tulo p�gina modificaci�n usuario", "Modificaci�n de usuario " + AdmBloqueado.getNombreUsuario(), titulo, "FALLO");

					// Modificar nombre de usuario
					String NuevoNomUsuario = AdmBloqueado.getNombreUsuario() + "�������";
					driver.findElement(By.xpath(Constantes.CampoNombreUsuario)).clear();
					driver.findElement(By.xpath(Constantes.CampoNombreUsuario)).sendKeys(NuevoNomUsuario);

					Utils.capturaIntermedia(driver, resultado, "Editando usuario");

					// Guardar
					Utils.scrollAndClick(driver, By.xpath(Constantes.BotonContinuarModificar));

					params.updateProperties(UtilsRest.getProperties(), "codUsuario***********", AdmBloqueado.getCodUsuario());

				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario***********");
				}

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "codUsuario**********", AdmPendiente.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario**********");
				}

				try
				{
					// ADM BLOQUEADO

					Usuario AdmBloqueado = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "NO", Constantes.ESTADO_BLOQ);

					if(AdmBloqueado == null)
					{
						AdmBloqueado = Funciones.bloquearUsuario(driver, resultado);
					}
					else
					{
						AdmBloqueado.setEstado(Constantes.ESTADO_NO_USAR);
						AdmBloqueado.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuario***", AdmBloqueado.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario***");
				}

				try
				{
					// ADM BLOQUEADO 2

					Usuario AdmBloqueado2 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "NO", Constantes.ESTADO_BLOQ);

					if(AdmBloqueado2 == null)
					{
						AdmBloqueado2 = Funciones.bloquearUsuario(driver, resultado);
					}
					else
					{
						AdmBloqueado2.setEstado(Constantes.ESTADO_NO_USAR);
						AdmBloqueado2.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuario****", AdmBloqueado2.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario****");
				}


			}


		}
		catch(Exception e)
		{

		}
	}


	@After
	public void tearDown() throws Exception
	{
		Utils.guardarEjecucion(driver, resultado);
	}
}
