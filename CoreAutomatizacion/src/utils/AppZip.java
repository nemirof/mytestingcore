package utils;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;


public class AppZip
{
	List<String> fileList;

	private static final String OUTPUT_ZIP_FILE = "C:\\MyFile2.zip";


	AppZip()
	{
		fileList = new ArrayList<String>();
	}


	public static void main(String[] args) throws Exception
	{
		AppZip appZip = new AppZip();

		String SOURCE_FOLDER = "";

		String Pais = Utils.devuelveDatosDeEjecucion("Pa");
		String Entorno = Utils.devuelveDatosDeEjecucion("Entorno");
		String pais = Pais.substring(0, 3).toUpperCase();
		String entorno = Entorno.substring(0, 3).toUpperCase();
		String fecha = Utils.GetTodayDateAndTimeSFTP();

		String nombreCarpeta = pais + entorno + "_" + fecha;


		boolean success = (new File(nombreCarpeta + "/" + nombreCarpeta)).mkdirs();

		if(!success)
			throw new Exception("No se pudo crear la carpeta para hacer el zip");

		AppZip.copyFolder(new File(new File("results/").getAbsolutePath()), new File(nombreCarpeta + "/" + nombreCarpeta + "/results/"));
		AppZip.copyFolder(new File(new File("resources/").getAbsolutePath()), new File(nombreCarpeta + "/" + nombreCarpeta + "/resources/"));
		AppZip.copyFolder(new File(new File("screenshots/").getAbsolutePath()), new File(nombreCarpeta + "/" + nombreCarpeta + "/screenshots/"));

		SOURCE_FOLDER = new File(nombreCarpeta).getAbsolutePath();

		appZip.generateFileList(new File(SOURCE_FOLDER), SOURCE_FOLDER);
		appZip.zipIt(OUTPUT_ZIP_FILE, SOURCE_FOLDER);

		FileUtils.deleteDirectory((new File(nombreCarpeta)));

	}


	public static void copyFolder(File src, File dest) throws IOException
	{

		if(src.isDirectory())
		{

			// if directory not exists, create it
			if(!dest.exists())
			{
				dest.mkdir();
				System.out.println("Directory copied from " + src + "  to " + dest);
			}

			// list all the directory contents
			String files[] = src.list();

			for(String file : files)
			{
				// construct the src and dest file structure
				File srcFile = new File(src, file);
				File destFile = new File(dest, file);
				// recursive copy
				copyFolder(srcFile, destFile);
			}

		}
		else
		{
			// if file, then copy it
			// Use bytes stream to support all file types
			InputStream in = new FileInputStream(src);
			OutputStream out = new FileOutputStream(dest);

			byte[] buffer = new byte[1024];

			int length;
			// copy the file content in bytes
			while((length = in.read(buffer)) > 0)
			{
				out.write(buffer, 0, length);
			}

			in.close();
			out.close();
			System.out.println("File copied from " + src + " to " + dest);
		}
	}


	/**
	 * Zip it
	 * 
	 * @param zipFile output ZIP file location
	 * @param SOURCE_FOLDER
	 */
	public void zipIt(String zipFile, String SOURCE_FOLDER)
	{

		byte[] buffer = new byte[1024];

		try
		{

			FileOutputStream fos = new FileOutputStream(zipFile);
			ZipOutputStream zos = new ZipOutputStream(fos);

			System.out.println("Output to Zip : " + zipFile);

			for(String file : this.fileList)
			{

				System.out.println("File Added : " + file);
				ZipEntry ze = new ZipEntry(file);
				zos.putNextEntry(ze);

				FileInputStream in = new FileInputStream(SOURCE_FOLDER + File.separator + file);

				int len;
				while((len = in.read(buffer)) > 0)
				{
					zos.write(buffer, 0, len);
				}

				in.close();
			}

			zos.closeEntry();
			// remember close it
			zos.close();

			System.out.println("Done");
		}
		catch(IOException ex)
		{
			ex.printStackTrace();
		}
	}


	public void zipItWithEmptyFolders(String zipFile, String SOURCE_FOLDER)
	{

		String[] emptyDirs = {"downloads", "results", "screenshots"};

		File child = null;

		byte[] buffer = new byte[1024];

		try
		{

			FileOutputStream fos = new FileOutputStream(zipFile);
			ZipOutputStream zos = new ZipOutputStream(fos);

			System.out.println("Output to Zip : " + zipFile);

			for(int i = 0; i < emptyDirs.length; i++)
			{

				child = new File(emptyDirs[i]);


				System.out.println("Adding : " + child.getAbsolutePath());

				zos.putNextEntry(new ZipEntry(child + "/"));
				zos.closeEntry();

			}

			for(String file : this.fileList)
			{


				System.out.println("File Added : " + file);
				ZipEntry ze = new ZipEntry(file);

				zos.putNextEntry(ze);

				FileInputStream in = new FileInputStream(SOURCE_FOLDER + File.separator + file);

				int len;
				while((len = in.read(buffer)) > 0)
				{
					zos.write(buffer, 0, len);
				}

				in.close();
			}

			zos.closeEntry();
			// remember close it
			zos.close();

			System.out.println("Done");
		}
		catch(IOException ex)
		{
			ex.printStackTrace();
		}
	}


	/**
	 * Traverse a directory and get all files,
	 * and add the file into fileList
	 * 
	 * @param node file or directory
	 */
	public void generateFileList(File node, String SOURCE_FOLDER)
	{

		// add file only
		if(node.isFile())
		{
			fileList.add(generateZipEntry(node.getAbsoluteFile().toString(), SOURCE_FOLDER));
		}

		if(node.isDirectory())
		{
			String[] subNote = node.list();
			for(String filename : subNote)
			{
				generateFileList(new File(node, filename), SOURCE_FOLDER);
			}
		}

	}


	public void generateFileListWithEmptyFolders(File node, String SOURCE_FOLDER)
	{

		// add file only
		if(node.isFile() || node.list().length == 0)
		{
			fileList.add(generateZipEntry(node.getAbsoluteFile().toString(), SOURCE_FOLDER));
		}

		else if(node.isDirectory())
		{
			String[] subNote = node.list();
			for(String filename : subNote)
			{
				generateFileListWithEmptyFolders(new File(node, filename), SOURCE_FOLDER);
			}
		}

	}


	/**
	 * Format the file path for zip
	 * 
	 * @param file file path
	 * @return Formatted file path
	 */
	private String generateZipEntry(String file, String SOURCE_FOLDER)
	{
		return file.substring(SOURCE_FOLDER.length() + 1, file.length());
	}
}
