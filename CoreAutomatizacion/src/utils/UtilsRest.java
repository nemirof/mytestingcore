package utils;


import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import junit.framework.TestCase;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;


@SuppressWarnings("deprecation")
public abstract class UtilsRest extends TestCase
{

	public static String rutaProperties = "properties/rest.properties";

	public static String rutaPropertiesEjecucion = "resources/restEjecucion.properties";

	public static int timeOut = 40;

	// public static String[] Latam = new String[] {"Mexico","Chile","Argentina","Peru"};
	public static List<String> Latam = Arrays.asList("Mexico", "Chile", "Argentina", "Peru", "Compass");

	public static String iv_origen = "";

	public static String iv_id_sesion_ast = "";

	public static String iv_cod_canal = "";

	public static String iv_cod_ban_int = "";

	public static String iv_cod_emp = "";

	public static String iv_user = "";

	public static String iv_cod_usu = "";
	
	private static  HashMap<String, Log> loggers = new HashMap<String, Log>();



	public static void setDefaultTimeOut(WebDriver driver)
	{
		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
	}


	public static Date StringToDate(String string, String formato) throws ParseException
	{
		// Ejemplo formato: HH:mm:ss,SSS
		SimpleDateFormat formatterhora = new SimpleDateFormat(formato);
		Date fecha = formatterhora.parse(string);
		return fecha;
	}


	public static String obtenerFecha()
	{
		// Devuelve fecha actual en formato ddmmaaaa
		Calendar fecha = new GregorianCalendar();
		String stdia = Integer.toString(fecha.get(Calendar.DAY_OF_MONTH));
		if(stdia.length() == 1)
			stdia = "0" + stdia;
		String stmes = Integer.toString(fecha.get(Calendar.MONTH) + 1);
		if(stmes.length() == 1)
			stmes = "0" + stmes;
		String stanio = Integer.toString(fecha.get(Calendar.YEAR));
		return stdia + stmes + stanio;
	}


	public static String obtenerFechaSeparador(String fecha)
	{
		// Devuelve fecha actual en formato dd/mm/aaaa despu�s de recibirla en formato ddmmaaa
		String FechaConSeparador = fecha.substring(0, 2) + "/" + fecha.substring(2, 4) + "/" + fecha.substring(4, 8);
		return FechaConSeparador;
	}


	public static boolean existeElemento(WebDriver driver, By by)
	{
		try
		{
			driver.findElement(by);
			return true;
		}
		catch(NoSuchElementException e)
		{
			return false;
		}
	}


	public static void setProperties(String ruta)
	{
		rutaProperties = ruta;
	}


	public static void borrarFicheroRest()
	{
		File file = new File(rutaPropertiesEjecucion);
		boolean alreadyExists = file.exists();

		if(alreadyExists)
			file.delete();

	}


	public static String getProperties()
	{

		File file = new File(rutaPropertiesEjecucion);

		boolean alreadyExists = file.exists();
		Date fecha = new Date(System.currentTimeMillis() - (1 * 60 * 60 * 1000));

		if(!alreadyExists)
			return rutaProperties;

		if(fecha.compareTo(new Date(file.lastModified())) > 0)
		{
			return rutaProperties;
		}
		else
			return rutaPropertiesEjecucion;
	}


	public static void removeNthLine(String f, int toRemove) throws Exception
	{
		RandomAccessFile raf = new RandomAccessFile(f, "rw");

		// Leaf n first lines unchanged.
		for(int i = 0; i < toRemove; i++)
			raf.readLine();

		// Shift remaining lines upwards.
		long writePos = raf.getFilePointer();
		raf.readLine();
		long readPos = raf.getFilePointer();

		byte[] buf = new byte[1024];
		int n;
		while(-1 != (n = raf.read(buf)))
		{
			raf.seek(writePos);
			raf.write(buf, 0, n);
			readPos += n;
			writePos += n;
			raf.seek(readPos);
		}

		raf.setLength(writePos);
		raf.close();
	}


	public static void CsvAppend(String archivo, String testId, String testName, String resultado, String comentario, String captura) throws Exception
	{

		String outputFile = archivo;
		String lockFile = archivo + ".lock";
		boolean resul = false;

		boolean existeBloqueo = new File(lockFile).exists();
		int contador = 0;

		// Esperamos si alg�n caso lo tiene bloqueado
		while(existeBloqueo && contador <= 60)
		{
			Thread.sleep(1000);
			contador++;
			existeBloqueo = new File(lockFile).exists();
		}

		// Creamos el bloqueo

		File archivoBloqueo = new File(lockFile);

		archivoBloqueo.createNewFile();

		// vemos si el fichero ya existe
		boolean alreadyExists = new File(outputFile).exists();

		try
		{
			// usamos FileWriter indicando que es para append
			CsvWriter writer = new CsvWriter(new FileWriter(outputFile, true), ',');
			CsvReader reader = new CsvReader(new FileReader(outputFile));

			// Si el fichero no existe, lo creamos con las cabeceras oportunas
			if(!alreadyExists)
			{
				writer.write("Test_ID");
				writer.write("Test_Name");
				writer.write("Resultado");
				writer.write("Comentario");
				writer.write("Captura");
				writer.endRecord();
			}

			int i = 0;
			while(reader.readRecord())
			{
				i++;
				String id = reader.get(0);
				if(id.equalsIgnoreCase(testId))
				{
					resul = true;
					break;
				}

			}

			// Si existe ya la entrada la borra
			if(resul)
				UtilsRest.removeNthLine(outputFile, i - 1);

			writer.write(testId);
			writer.write(testName);
			writer.write(resultado);
			writer.write(comentario);
			writer.write(captura);
			writer.endRecord();
			writer.close();

		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
		}

		archivoBloqueo.delete();

	}


	public static WebDriver getWebDriverBrowser()
	{
		WebDriver driver = null;
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();

		File profileDirectory = new File("resources/Profiles");
		FirefoxProfile profile = new FirefoxProfile(profileDirectory);
		
		FirefoxOptions options = new FirefoxOptions();
		options.setProfile(profile);
		
		driver = new FirefoxDriver(options);


		return driver;
	}


	public static void setResultadoOK(Resultados resultado, String comentario)
	{
		if("".equals(resultado.getBufferErrores().toString()))
		{
			resultado.setResultado("OK");
			resultado.setComentario(comentario);
		}
	}


	public static void setResultadoERROR(Resultados resultado, String comentario)
	{
		resultado.setResultado("ERROR");
		resultado.setComentario(comentario);
	}


	public static void vp(WebDriver driver, Resultados resultado, String NombreVP, Object Esperado, Object Actual, String Pantallazo)
	{
		String cadenaAleatoria = UtilsRest.CadenaAleatoria(5);
		boolean ResultadoVP;
		if(Esperado == Actual)
			ResultadoVP = true;
		else if(Esperado.toString().equals(Actual.toString()))
			ResultadoVP = true;
		else
			ResultadoVP = false;
		String mensajeLog;
		if(ResultadoVP)
			Log.write("Validaci�n OK: " + NombreVP);
		else
		{
			mensajeLog = "Error en validaci�n: " + NombreVP + ". Se esperaba " + Esperado.toString() + " y el valor actual es " + Actual.toString() + ". ";
			resultado.appendError(System.getProperty("line.separator") + mensajeLog);
			Log.write(mensajeLog);
		}
		if(Pantallazo.equals("SI") || (Pantallazo.equals("FALLO") && !ResultadoVP))
		{
			mensajeLog = "Captura de la validaci�n en " + takeScreenShot(driver, resultado.getNombreCP() + "_" + cadenaAleatoria);
			resultado.appendError(mensajeLog);
			Log.write(mensajeLog);
		}
	}


	public static void error(WebDriver driver, Resultados resultado, String comentario)
	{
		String captura = takeScreenShot(driver, resultado.getNombreCP());
		Log.write("Error: " + comentario + ". Puede ver la captura en: " + captura);
		resultado.appendError(comentario + ". Puede ver la captura en: " + captura);
	}


	public static void vpWarning(WebDriver driver, Resultados resultado, String NombreVP, Object Esperado, Object Actual, String Pantallazo)
	{
		String cadenaAleatoria = UtilsRest.CadenaAleatoria(5);
		boolean ResultadoVP;
		if(Esperado == Actual)
			ResultadoVP = true;
		else if(Esperado.toString().equals(Actual))
			ResultadoVP = true;
		else
			ResultadoVP = false;
		String mensajeLog;
		if(ResultadoVP)
			Log.write("Validaci�n OK: " + NombreVP);
		else
		{
			mensajeLog = " en validaci�n: " + NombreVP + ". Se esperaba " + Esperado.toString() + " y el valor actual es " + Actual.toString() + ". ";
			resultado.appendWarning(System.getProperty("line.separator") + "Error" + mensajeLog);
			Log.write("Aviso " + mensajeLog);
		}
		if(Pantallazo.equals("SI") || (Pantallazo.equals("FALLO") && !ResultadoVP))
		{
			mensajeLog = "Captura de la validaci�n en " + takeScreenShot(driver, resultado.getNombreCP() + "_" + cadenaAleatoria);
			resultado.appendWarning(mensajeLog);
			Log.write(mensajeLog);
		}
	}


	public static String takeScreenShot(WebDriver driver, String fileName)
	{

		// Obtenemos la fecha y hora de hoy
		java.util.Date hoy = new java.util.Date();
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd_MM_yyyy_H_m_s");
		String horaActual = formatter2.format(hoy);

		// Cogemos del properties la ruta donde se han de guardar las capturas
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();

		String ruta = params.getValorPropiedad("rutaScreenshots");
		File screenShot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try
		{
			File ficheroDestino = new File(ruta + fileName + "_" + horaActual + ".png");
			FileUtils.copyFile(screenShot, ficheroDestino);
			// return ficheroDestino.getAbsolutePath();
			return ruta + ficheroDestino.getName();

		}
		catch(IOException ioe)
		{
			throw new RuntimeException(ioe.getMessage(), ioe);
		}
	}


	public static void CSVToTableRest(java.util.Date hoy, String horaInicio, String horaFin, List<String> lista) throws Exception
	{

		String urlLogo = "https://sites.google.com/a/bbva.com/plataforma_banca_empresas/piezas-estructurales/REST.PNG";
		Random random = new Random();
		int segundosEspera = random.nextInt(5000);

		Thread.sleep(segundosEspera);

		// M�todo para pasar del CSV de resultados a una tabla HTML

		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();

		String contenido = "<!DOCTYPE html>\n" + "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"es\" lang=\"es\" dir=\"ltr\">\n" + "<head>\n" + "<style>* {font-family:Arial;}</style>\n"
				+ "<title>Resumen Pruebas REST</title>\n" + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1252\" />\n" + "<script type=\"teXt/javascript\"> \n"
				+ "function desplegar(tabla_a_desplegar,estadoT, estadoTfila,comentario) {\n" + "var tablA = document.getElementById(tabla_a_desplegar); \n"
				+ "var estadOt = document.getElementById(estadoT);\n" + "var fila = document.getElementById(estadoTfila);\n" + "switch(tablA.style.display) {\n" + "case \"none\": \n"
				+ "tablA.style.display = \"block\";\n" + "estadOt.innerHTML = \"Ocultar\";\n" + "fila.innerHTML = \"\";\n" + "fila.innerHTML = comentario;\n" + "break; default: \n"
				+ "tablA.style.display = \"none\"; \n" + "estadOt.innerHTML = \"Mostrar\"\n" + "break; \n" + "} \n" + "} \n" + "</script>\n"
				+ "<style>table,th,td{border:1px solid black;border-collapse:collapse;font-size:14px;border-color:#094fa4;}th,td{padding:5px;}th{text-align:left;}</style>\n" + "</head>\n"
				+ "<body>\n" + "<img src=\"" + urlLogo + "\" alt=\"BBVA netCash Pruebas Autom�ticas\">" + UtilsRest.devuelveResumenHTML(hoy, horaInicio, horaFin, lista);

		contenido += UtilsRest.devuelveInformeDetallado(lista);

		// Cerramos el string con las etiquetas HTML correspondientes
		contenido += "\n" + "</body>" + "</html>";

		// Volcamos este string a un fichero nuevo
		File file = new File("results/REST/InformeServiciosREST.html");

		FileWriter fileHtm = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fileHtm);

		bw.write(contenido);
		bw.close();

	}


	public static String devuelveResumenHTML(java.util.Date hoy, String horaInicio, String horaFin, List<String> lista) throws Exception
	{
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();

		TestProperties params1 = new TestProperties(Utils.getProperties());
		params1.cargarPropiedades();

		String archivoCSV = params.getValorPropiedad("rutaResultados");
		String entorno = params1.getValorPropiedad("entorno");

		CsvReader reader = new CsvReader(new FileReader(archivoCSV));

		// java.util.Date hoy = new java.util.Date();
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
		String diaActual = formatter2.format(hoy);

		int resultadosOK = 0;
		int resultadosERROR = 0;
		int resultadosWARNING = 0;
		int resultadosOK2 = 0;
		int resultadosERROR2 = 0;
		int resultadosWARNING2 = 0;

		String contenido = "";

		try
		{

			while(reader.readRecord())
			{
				String testName = reader.get(1);
				String resul = reader.get(2);

				if(lista != null)
				{
					for(int k = 0; k < lista.size(); ++k)
					{
						if(lista.get(k).indexOf(testName) > -1)
							resul = resul + "*";
					}
				}

				if(resul.indexOf("OK") > -1)
					resultadosOK++;
				else if(resul.indexOf("ERROR") > -1)
					resultadosERROR++;
				else if(resul.indexOf("WARNING") > -1)
					resultadosWARNING++;

				if(resul.equalsIgnoreCase("OK*"))
					resultadosOK2++;
				else if(resul.equalsIgnoreCase("ERROR*"))
					resultadosERROR2++;
				else if(resul.equalsIgnoreCase("WARNING*"))
					resultadosWARNING2++;


			}

			int totalesINT = resultadosOK + resultadosERROR + resultadosWARNING;

			double totales = resultadosOK + resultadosERROR + resultadosWARNING;
			double porcentajeOK = Math.round((resultadosOK * 1.0 / totales) * 100.0);
			double porcentajeERROR = Math.round((resultadosERROR * 1.0 / totales) * 100.0);
			double porcentajeWARNING = Math.round((resultadosWARNING * 1.0 / totales) * 100.0);

			try
			{
				porcentajeOK = UtilsRest.roundTwoDecimals((resultadosOK * 1.0 / totales) * 100.0);
				porcentajeERROR = UtilsRest.roundTwoDecimals((resultadosERROR * 1.0 / totales) * 100.0);
				porcentajeWARNING = UtilsRest.roundTwoDecimals((resultadosWARNING * 1.0 / totales) * 100.0);
			}
			catch(Exception e)
			{

			}

			String literalSatisfactorios = "Satisfactorios";
			String literalError = "Error";
			String literalAvisos = "Avisos";

			if(resultadosOK2 > 0)
				literalSatisfactorios = "Satisfactorios*";
			if(resultadosERROR2 > 0)
				literalError = "Error*";
			if(resultadosWARNING2 > 0)
				literalAvisos = "Avisos*";

			String literalTrasReEjecucion = "";

			if(resultadosOK2 > 0 || resultadosERROR2 > 0 || resultadosWARNING2 > 0)
			{
				int totalesReejecutados = lista.size();
				literalTrasReEjecucion = "<br><a>*Tras reejecuci�n de " + totalesReejecutados + " casos por error o por dependencia de un caso de prueba err�neo:</a>";

				if(resultadosOK2 > 0)
					literalTrasReEjecucion = literalTrasReEjecucion + "<br><a>" + resultadosOK2 + " caso(s) Satisfactorios</a>";
				if(resultadosERROR2 > 0)
					literalTrasReEjecucion = literalTrasReEjecucion + "<br><a>" + resultadosERROR2 + " caso(s) Error</a>";
				if(resultadosWARNING2 > 0)
					literalTrasReEjecucion = literalTrasReEjecucion + "<br><a>" + resultadosWARNING2 + " caso(s) Avisos</a>";
			}


			contenido = "<h3>Datos de Ejecuci�n</h3>\n" + "<table style=\"width:300px\">\n" + "<tr><td>Entorno</td><td>"
					+ entorno
					+ "</td></tr>\n"
					+ "<tr><td>Fecha de Ejecuci�n</td><td>"
					+ diaActual
					+ "</td></tr>\n"
					+ "<tr><td>Hora inicio</td><td>"
					+ horaInicio
					+ "</td></tr>\n"
					+ "<tr><td>Hora fin</td><td>"
					+ horaFin
					+ "</td></tr>\n"
					+ "</table>\n"
					+ "<h3>Resumen Ejecuci�n</h3>\n"
					+ "<table style=\"width:300px\">\n"
					+ "<tr><th style=\"background-color:#b5e5f9\">Resumen Resultados</th><th style=\"background-color:#b5e5f9\">N�mero de casos</th><th style=\"background-color:#b5e5f9\">%</th></tr>\n"
					+ "<tr><td>" + literalSatisfactorios + "</td><td>" + resultadosOK + "</td><td>" + porcentajeOK + "%</td>\n" + "<tr><td>" + literalError + "</td><td>" + resultadosERROR
					+ "</td><td>" + porcentajeERROR + "%</td>\n" + "<tr><td>" + literalAvisos + "</td><td>" + resultadosWARNING + "</td><td>" + porcentajeWARNING + "%</td>\n"
					+ "<tr><th>TOTAL</th><th>" + totalesINT + "</th><th>100%</th>\n" + "</table>\n" + literalTrasReEjecucion + "<h3>Informe Detallado</h3>\n";


		}
		catch(IOException e)
		{
			utils.Log.writeException(e);
		}


		return contenido;

	}


	public static String devuelveResumenParaMail()
	{


		File archivoHTML = new File("results/ResumenEjecucionPruebas.html");

		StringBuilder contentBuilder = new StringBuilder();

		try
		{
			BufferedReader in = new BufferedReader(new FileReader(archivoHTML));
			String str;
			while((str = in.readLine()) != null)
			{
				contentBuilder.append(str);
			}
			in.close();
		}
		catch(IOException e)
		{}
		String content = contentBuilder.toString();

		int inicio = content.indexOf("<h3>Datos de Ejecuci�n</h3>");
		int fin = content.indexOf("<h3>Informe Detallado</h3>");


		if(inicio > -1 && fin > -1)
			return content.substring(inicio, fin);
		else
			return "";

	}


	public static String devuelveInformeDetallado(List<String> lista) throws Exception
	{

		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();

		String archivoCSV = params.getValorPropiedad("rutaResultados");


		CsvReader reader = new CsvReader(new FileReader(archivoCSV));
		boolean titulos = true;

		String contenido = "<table style=\"\"><tbody><tr>\n" + "<th style=\"background-color:#b5e5f9\">C�digo Caso</th>\n" + "<th style=\"background-color:#b5e5f9; width:800px\" >Caso</th>\n"
				+ "<th style=\"background-color:#b5e5f9\">Prioridad</th>\n" + "<th style=\"background-color:#b5e5f9\">Pasos</th>\n"
				+ "<th style=\"background-color:#b5e5f9\">Resultados esperados</th>\n" + "<th style=\"background-color:#b5e5f9\">Resultado</th>\n"
				+ "<th style=\"background-color:#b5e5f9; width:400px\">Comentario</th>\n" + "<th style=\"background-color:#b5e5f9\">Par�metros</th>\n"
				+ "<th style=\"background-color:#b5e5f9\">Respuesta REST</th></tr>\n";

		try
		{
			int i = 0;
			int j = 9999;
			while(reader.readRecord())
			{
				String id = reader.get(0);
				String testName = reader.get(1);
				String resultado = reader.get(2);
				String comentario = reader.get(3);
				String parametros = reader.get(4);

				if(lista != null)
				{
					for(int k = 0; k < lista.size(); ++k)
					{
						if(lista.get(k).indexOf(testName) > -1)
						{
							resultado = resultado + "*";
						}
					}
				}

				// File ficheroDestino = new File("results/"+testName+".txt");
				// String txt = ficheroDestino.getAbsolutePath();

				String txt = id + ".txt";
				String ficheroParams = id + "_params.txt";

				if(titulos)
					titulos = false;
				else
					contenido += "\n" + "<tr>" + "<td>"
							+ id
							+ "</td>"
							+ "<td>"
							+ UtilsRest.devuelveDatoDocumentacion(id, "Caso", testName)
							+ "</td>"
							+ "<td>"
							+ UtilsRest.devuelveDatoDocumentacion(id, "Prioridad", "")
							+ "</td>"
							// + "<td><div id=\"estadoT"+i+"\" onClick=\"desplegar('tabla_a_desplegar"+i+"','estadoT"+i+"', 'estadoTfila"+i+"', '"+"Datos de entrada: <br />"+UtilsRest.devuelveDatoDocumentacion(id, "Pasos",
							// "")+"')\" style=\"text-align:center; background: #E9FAD0; cursor: pointer;\">Mostrar</div></td>"
							+ "<td><div id=\"estadoT"
							+ i
							+ "\" onClick=\"desplegar('tabla_a_desplegar"
							+ i
							+ "','estadoT"
							+ i
							+ "', 'estadoTfila"
							+ i
							+ "', '"
							+ "Datos de entrada: <br />"
							+ UtilsRest.devuelveResultadoEsperadoCSVRest(id)
							+ "')\" style=\"text-align:center; background: #E9FAD0; cursor: pointer;\">Mostrar</div></td>"


							// + "<td>"+ Utils.devuelveDatoDocumentacion(id, "Pasos", "") + "</td>"
							// + "<td><div id=\"estadoT"+j+"\" onClick=\"desplegar('tabla_a_desplegar"+j+"','estadoT"+j+"', 'estadoTfila"+j+"', '"+"Resultado Esperado: "+UtilsRest.devuelveDatoDocumentacion(id, "ResultadoEsperado",
							// "")+"')\" style=\"text-align:center; background: #FDF158; cursor: pointer;\">Mostrar</div></td>"
							+ "<td><div id=\"estadoT" + j + "\" onClick=\"desplegar('tabla_a_desplegar" + j + "','estadoT" + j + "', 'estadoTfila" + j
							+ "', '"
							+ "Resultado Esperado: "
							+ UtilsRest.devuelveResultadoEsperadoCSVRest(id)
							+ "')\" style=\"text-align:center; background: #FDF158; cursor: pointer;\">Mostrar</div></td>"

							// + "<td>"+ Utils.devuelveDatoDocumentacion(id, "ResultadoEsperado", "") + "</td>"
							+ "<td" + devuelveFormatoResultado(resultado) + resultado + "</td>"
							+ "<td>"
							+ UtilsRest.cortarComentario(comentario)
							+ "</td>"
							+ "<td>"
							+ UtilsRest.cortarComentario(parametros)
							+ "</td>"
//						+ "<td>"+"<a href=\""+ficheroParams+"\" target=\"_blank\">"+ "Ver Par�metros" + "</a></td></tr>"
//						+ "<td>"+"<a href=\""+"../screenshots/"+Utils.nombreFichero(captura)+"\" target=\"_blank\">"+ Utils.cortarPalabraLarga("/screenshots/"+Utils.nombreFichero(captura)) + "</a></td>"
							+ "<td>" + "<a href=\"" + txt + "\" target=\"_blank\">" + "Ver Respuesta" + "</a></td></tr>"
							+ "<tr> <td colspan=\"9\" style=\"border-bottom: 1px solid #FFF;\"> <table id=\"tabla_a_desplegar" + i + "\" style=\"display:none;\"> <tr> <td id=\"estadoTfila" + i
							+ "\" style=\" background: #E9FAD0\">Pasos y Condiciones</td> </tr> </table> </td> </tr> " + "<tr> <td colspan=\"9\"> <table id=\"tabla_a_desplegar" + j
							+ "\" style=\"display:none;\"> <tr> <td id=\"estadoTfila" + j + "\" style=\" background: #FDF158\">Resultado Esperado</td> </tr> </table> </td> </tr> ";
				i++;
				j--;
			}

			contenido += "</tbody></table>";

		}
		catch(IOException e)
		{
			utils.Log.writeException(e);
		}


		return contenido;

	}


	public static String devuelveResultadoEsperadoCSVRest(String numeroCaso) throws Exception
	{
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();
		String archivoCSV = params.getValorPropiedad("rutaLanzamiento");

		CsvReader reader = new CsvReader(new FileReader(archivoCSV), ';');

		boolean encontrado = false;

		String codigoRetorno = "";
		String resultadoEsperado = "";

		while(reader.readRecord() && !encontrado)
		{
			String id = reader.get(0);
			if(id.equalsIgnoreCase(numeroCaso))
			{
				codigoRetorno = reader.get(1);
				resultadoEsperado = reader.get(2);
				encontrado = true;
			}

		}


		return "Resultado Esperado: Codigo de retorno: " + codigoRetorno + ". Descripcion: " + resultadoEsperado;

	}


	public static String devuelveFormatoResultado(String resultado)
	{
		if(resultado.indexOf("OK") > -1)
			return " style=\"background-color: lightgreen;font-weight:bold;\">";
		else if(resultado.indexOf("ERROR") > -1)
			return " style=\"background-color: #F46355;font-weight:bold;\">";
		else if(resultado.indexOf("WARNING") > -1)
			return " style=\"background-color: lightyellow; font-weight:bold;\">";
		else
			return ">";

	}


	public static String nombreFichero(String rutaCompleta)
	{
		int opcion1 = rutaCompleta.lastIndexOf("/");
		int opcion2 = rutaCompleta.lastIndexOf("\\");

		if(opcion1 > -1)
			return rutaCompleta.substring(opcion1 + 1);
		else if(opcion2 > -1)
			return rutaCompleta.substring(opcion2 + 1);
		else
			return rutaCompleta;
	}


	public static String cortarPalabraLarga(String palabra)
	{
		int tamano = palabra.length();
		String nuevaPalabra = "";

		int numeroCorte = 30;
		int posicionInicio = 0;

		if(tamano > numeroCorte)
		{
			int numerobr = tamano / numeroCorte;
			for(int i = 0; i < numerobr; i++)
			{
				posicionInicio = i * numeroCorte;
				nuevaPalabra += palabra.subSequence(posicionInicio, posicionInicio + numeroCorte) + "<br>";
			}
			return nuevaPalabra + palabra.subSequence(posicionInicio + numeroCorte, palabra.length());
		}

		else
			return palabra;


	}


	public static String cortarComentario(String palabra)
	{

		try
		{
			Scanner scanner = new Scanner(palabra);
			String input = scanner.nextLine();
			String[] words = input.split(" ");

			int maxLength = 0;

			String longestWord = "";

			for(String word : words)
			{
				if(word.length() > maxLength)
				{
					maxLength = word.length();
					longestWord = word;
				}
			}
			return palabra.replace(longestWord, UtilsRest.cortarPalabraLarga(longestWord));

		}
		catch(Exception e)
		{
			return "";
		}


	}


	public static String devuelveDatoDocumentacion(String codigo, String campo, String original) throws Exception
	{

		String resultado = "";
		String ruta = "resources/DocumentacionRest.xls";

		WorkbookSettings ws = new WorkbookSettings(); 
		ws.setEncoding("Cp1252");
		Workbook excelLogin = Workbook.getWorkbook(new File(ruta),ws);
		Sheet sheet1 = excelLogin.getSheet("Documentacion");

		// Obtenemos las columnas de Pa�s, de Entorno y de URL
		int columnaCodigo = sheet1.findCell("CodigoCaso").getColumn();
		int columnaDato = sheet1.findCell(campo).getColumn();

		boolean encontrado = false;

		for(int i = 0; (i < sheet1.getRows()) && !encontrado; i++)
		{
			if(sheet1.getCell(columnaCodigo, i).getContents().equalsIgnoreCase(codigo))
			{
				encontrado = true;
				resultado = sheet1.getCell(columnaDato, i).getContents();
			}
		}

		if(!encontrado)
			return original;
		else
		{
			String resultadoparcial = resultado.replaceAll("\"", "");
			resultadoparcial = resultadoparcial.replaceAll("'", "");
			resultadoparcial = resultadoparcial.replaceAll("(\r\n|\n)", "<br />");
			return resultadoparcial;
		}

	}


	public static String devuelveURLApartirDePaisYEntorno(String ruta, String pais, String entorno) throws Exception
	{

		String url = "";
		WorkbookSettings ws = new WorkbookSettings(); 
		ws.setEncoding("Cp1252");
		Workbook excelLogin = Workbook.getWorkbook(new File(ruta),ws);
		Sheet sheet1 = excelLogin.getSheet("URLs");

		// Obtenemos las columnas de Pa�s, de Entorno y de URL
		int columnaPais = sheet1.findCell("Pais").getColumn();
		int columnaEntorno = sheet1.findCell("Entorno").getColumn();
		int columnaURL = sheet1.findCell("URL").getColumn();

		boolean encontrado = false;

		for(int i = 0; (i < sheet1.getRows()) && !encontrado; i++)
		{
			if(sheet1.getCell(columnaPais, i).getContents().equalsIgnoreCase(pais) && sheet1.getCell(columnaEntorno, i).getContents().equalsIgnoreCase(entorno))
			{
				encontrado = true;
				url = sheet1.getCell(columnaURL, i).getContents();
			}
		}

		if(!encontrado)
			throw new Exception("No existe la combinaci�n de Pa�s/Entorno. Revisar archivo properties.");

		return url;

	}


	public static WebDriver inicializarCaso(WebDriver driver, String m_codigo, String m_nombre) throws Exception
	{

//    	Logger log = Logger.getLogger(Class.forName(m_nombre));


		// Establecemos que la salida est�ndar y la salida de errores sea el fichero que se crea por caso de prueba
		FileOutputStream fichero = new FileOutputStream("results/" + m_nombre + ".txt", true);
		PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);

//  	  	PropertyConfigurator.configure("properties/log4j.properties");
//  	  	log.addAppender(new FileAppender(new PatternLayout(),"results/"+m_nombre+".txt", false));

//		System.setOut(ficheroSalida);
//		System.setErr(ficheroSalida);
		
		loggers.put(Long.toString(Thread.currentThread().getId()), new Log(m_nombre));

		// Pintamos el inicio de la traza
		UtilsRest.pintarInicioTraza(m_codigo, m_nombre);

		// Cargamos las propiedades del properties
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();
		String archivoCSV = params.getValorPropiedad("rutaResultados");
		String pais = params.getValorPropiedad("pais");
		String entorno = params.getValorPropiedad("entorno");
		String rutaURLs = params.getValorPropiedad("rutaDatosURLs");
		String url = devuelveURLApartirDePaisYEntorno(rutaURLs, pais, entorno);

		if(url == null || url.equals(""))
		{
			throw new Exception("No se pudo obtener la URL correspondiente a los par�metros: Pa�s = " + pais + ", Entorno = " + entorno + ". Revisar el archivo properties y la excel con las URLs.");
		}


		// Inicializamos el caso a ERROR.
		UtilsRest.CsvAppend(archivoCSV, m_codigo, m_nombre, "ERROR", "", "");

		// Obtenemos el driver espec�fico del navegador
		driver = UtilsRest.getWebDriverBrowser();

		if(driver == null)
		{
			throw new Exception("No se estableci� correctamente la propiedad correspondiente al navegador. Revisar el archivo properties.");
		}

		driver.get("");

		// Navegamos a la url
		driver.get(url);

		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
		if (!driver.toString().contains("ChromeDriver"))
			driver.manage().window().maximize();

		return driver;
	}


	public static void pintarInicioTraza(String m_codigo, String m_nombre)
	{

		TestProperties params = new TestProperties(UtilsRest.getProperties());
		// Cargamos las propiedades que nos interesan del properties
		params.cargarPropiedades();
		String navegador = params.getValorPropiedad("browser");
		String pais = params.getValorPropiedad("pais");
		String entorno = params.getValorPropiedad("entorno");

		// Fecha actual
		java.util.Date hoy = new java.util.Date();
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String horaActual = formatter2.format(hoy);


		Log.write("--------------------------------------------------");
		Log.write("	NOMBRE CASO DE PRUEBA: " + m_nombre);
		Log.write("	PA�S :" + pais);
		Log.write("	ENTORNO :" + entorno);
		Log.write("	NAVEGADOR :" + navegador);
		Log.write("	FECHA DE INICIO :" + horaActual);
		Log.write("--------------------------------------------------");
		Log.write();


	}


	// Devuelve una cadena aleatoria de n�meros del tama�o que se le indica
	public static String CadenaAleatoria(int numeroDigitos)
	{
		Random rnd = new Random();
		int x;
		String resul = "";
		for(int i = 0; i < numeroDigitos; i++)
		{
			x = (int)(rnd.nextDouble() * 10.0);
			resul = resul + String.valueOf(x);
		}
		return resul;
	}


	public static String devuelveNIFAleatorio()
	{
		String NIF_STRING_ASOCIATION = "TRWAGMYFPDXBNJZSQVHLCKE";

		int numeros = Integer.parseInt(UtilsRest.CadenaAleatoria(8));

		String dniAleatorio = String.valueOf(numeros) + NIF_STRING_ASOCIATION.charAt(numeros % 23);

		return dniAleatorio;

	}


	public static String devuelveRUTAleatorio()
	{
		int rut = Integer.parseInt(UtilsRest.CadenaAleatoria(8));
		String cadena = String.valueOf(rut);
		int contador = 2;
		int acumulador = 0;
		int multiplo = 0;

		while(rut != 0)
		{
			multiplo = (rut % 10) * contador;
			acumulador = acumulador + multiplo;
			rut = rut / 10;
			contador++;
			if(contador > 7)
				contador = 2;
		}
		int digito = 11 - (acumulador % 11);
		String RutDigito = String.valueOf(digito);
		if(digito == 10)
			RutDigito = "K";
		if(digito == 11)
			RutDigito = "0";

		return cadena + "-" + RutDigito;
	}


	// Devuelve el nombre del fichero que nos acabamos de descargar
	public static String devuelveNombreFicheroDescargado(Date fecha, String patron) throws Exception
	{

		String rutaArchivo = null;

		// Obtenemos la ruta de la carpeta de downloads
		String dirDownloads = new File("downloads").getAbsolutePath();

		Thread.sleep(30000);

		File fl = new File(dirDownloads);

		// Todos los archivos de la carpeta
		File[] files = fl.listFiles(new FileFilter()
		{
			@Override
			public boolean accept(File file)
			{
				return file.isFile();
			}
		});

		// Obtenemos el �ltimo modificado
		long lastMod = Long.MIN_VALUE;
		File choise = null;
		for(File file : files)
		{
			if(file.lastModified() > lastMod && file.getName().indexOf(patron) > -1 && fecha.compareTo(new Date(file.lastModified())) < 0)
			{
				choise = file;
				lastMod = file.lastModified();
			}
		}

		if(choise != null)
			rutaArchivo = choise.getAbsolutePath();

		return rutaArchivo;
	}


	// Devuelve el nombre del fichero que nos acabamos de descargar pero ademas con el formato pasado, ya que puede ser a veces xls / pdf
	public static String devuelveNombreFicheroDescargadoFormato(Date fecha, String patron, String formato) throws Exception
	{

		String rutaArchivo = null;

		// Obtenemos la ruta de la carpeta de downloads
		String dirDownloads = new File("downloads").getAbsolutePath();

		Thread.sleep(12000);

		File fl = new File(dirDownloads);

		// Todos los archivos de la carpeta
		File[] files = fl.listFiles(new FileFilter()
		{
			@Override
			public boolean accept(File file)
			{
				return file.isFile();
			}
		});

		// Obtenemos el �ltimo modificado
		long lastMod = Long.MIN_VALUE;
		File choise = null;
		for(File file : files)
		{
			if(file.lastModified() > lastMod && file.getName().indexOf(patron) > -1 && file.getName().indexOf(formato) > -1 && fecha.compareTo(new Date(file.lastModified())) < 0)
			{
				choise = file;
				lastMod = file.lastModified();
			}
		}

		if(choise != null)
			rutaArchivo = choise.getAbsolutePath();

		return rutaArchivo;
	}


	// Devuelve si est� visible o no un elemento. Si no est�, no provoca excepci�n.
	public static boolean isDisplayed(WebDriver driver, String xpath, int tiempo) throws Exception
	{

		driver.manage().timeouts().implicitlyWait(tiempo, TimeUnit.SECONDS);

		boolean resul = false;

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					resul = true;
			}

		}
		catch(Exception e)
		{

		}
		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
		return resul;
	}


	public static void introducirTextoEnElVisible(WebDriver driver, String xpath, String texto) throws Exception
	{
		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					elemento = listaElementos.get(i);
			}

			elemento.sendKeys(texto);
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}
	}


	public static void clickAleatorioEnUnoDeLosVisibles(WebDriver driver, String xpath) throws Exception
	{
		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			List<WebElement> listaElementosVisibles = new ArrayList<WebElement>();

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
				{
					listaElementosVisibles.add(listaElementos.get(i));
				}
			}
			Random random = new Random();
			int randomNum = random.nextInt(listaElementosVisibles.size());

			listaElementosVisibles.get(randomNum).click();
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}
	}


	public static int numeroElementosVisibles(WebDriver driver, String xpath) throws Exception
	{
		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			List<WebElement> listaElementosVisibles = new ArrayList<WebElement>();

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
				{
					listaElementosVisibles.add(listaElementos.get(i));
				}
			}

			return listaElementosVisibles.size();

		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}
	}


	public static WebElement devuelveElementoVisible(WebDriver driver, String xpath) throws Exception
	{

		WebElement visible = null;

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			WebElement elemento = null;

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
				{
					elemento = listaElementos.get(i);
					break;
				}
			}

			visible = elemento;

		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}

		if(visible == null)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}
		else
			return visible;
	}


	// N�mero de filas de una tabla HTML
	public static int numeroFilasTabla(WebDriver driver, String xpathTabla) throws Exception
	{
		try
		{
			WebElement table = driver.findElement(By.xpath(xpathTabla));
			List<WebElement> allRows = table.findElements(By.tagName("tr"));
			return allRows.size();
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� la tabla: " + xpathTabla);
		}
	}


	public static int numeroFilasVisiblesTabla(WebDriver driver, String xpathTabla) throws Exception
	{
		try
		{

			WebElement table = driver.findElement(By.xpath(xpathTabla));
			List<WebElement> allRows = table.findElements(By.tagName("tr"));

			int filas = 0;

			for(int i = 0; i < allRows.size(); i++)
			{
				WebElement row = allRows.get(i);
				if(row.isDisplayed())
					filas++;
			}

			return filas;
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� la tabla: " + xpathTabla);
		}
	}


	public static int numeroFilasTablaDeUnaClase(WebDriver driver, String xpathTabla, String className) throws Exception
	{
		try
		{
			WebElement table = driver.findElement(By.xpath(xpathTabla));
			List<WebElement> allRows = table.findElements(By.xpath("//*[@class='" + className + "']"));
			return allRows.size();
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� la tabla: " + xpathTabla);
		}
	}


	public static int numeroFilasTabla(WebDriver driver, WebElement tabla) throws Exception
	{
		try
		{
			WebElement table = tabla;
			List<WebElement> allRows = table.findElements(By.tagName("tr"));
			return allRows.size();
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� la tabla: " + tabla.getTagName());
		}
	}


	// Devuelve la fila en la que est� el valor, si no -1. Empieza en 0
	public static int devuelveFilaContieneValor(WebDriver driver, String xpathTabla, String valor) throws Exception
	{
		try
		{
			WebElement table = driver.findElement(By.xpath(xpathTabla));
			List<WebElement> allRows = table.findElements(By.tagName("tr"));

			int encontrado = -1;

			for(int i = 0; i < allRows.size(); i++)
			{
				WebElement row = allRows.get(i);
				List<WebElement> cells = row.findElements(By.tagName("td"));

				for(WebElement cell : cells)
				{
					if(cell.getText().indexOf(valor) > -1)
					{
						encontrado = i;
						break;
					}
				}
			}

			return encontrado;
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� la tabla: " + xpathTabla);
		}
	}


	public static int devuelveFilaContieneValor(WebDriver driver, WebElement tabla, String valor) throws Exception
	{
		try
		{
			WebElement table = tabla;
			List<WebElement> allRows = table.findElements(By.tagName("tr"));

			int encontrado = -1;

			for(int i = 0; i < allRows.size(); i++)
			{
				WebElement row = allRows.get(i);
				List<WebElement> cells = row.findElements(By.tagName("td"));

				for(WebElement cell : cells)
				{
					if(cell.getText().indexOf(valor) > -1)
					{
						encontrado = i;
						break;
					}
				}
			}

			return encontrado;
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� la tabla: " + tabla.getTagName());
		}
	}


	// Devuel el n�mero de ocurrencias que un string contiene a otro
	public static int cantidadOcurrencias(String cadena, String patron)
	{
		int cant = 0;
		while(cadena.indexOf(patron) > -1)
		{
			cadena = cadena.substring(cadena.indexOf(patron) + patron.length(), cadena.length());
			cant++;
		}
		return cant;
	}


	// Obtener la fecha de hoy
	public static String GetTodayDate() throws Exception
	{

		GregorianCalendar calendar = new GregorianCalendar();

		// date now:
		Date now = calendar.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		String formattedDate = fmt.format(now);

		Date today = calendar.getTime();
		formattedDate = fmt.format(today);

		return formattedDate;

	}


	public static String GetTodayDateAndTime() throws Exception
	{

		GregorianCalendar calendar = new GregorianCalendar();

		// date now:
		Date now = calendar.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String formattedDate = fmt.format(now);

		Date today = calendar.getTime();
		formattedDate = fmt.format(today);

		return formattedDate;

	}


	public static String GetTodayDateAndTimeSFTP() throws Exception
	{

		GregorianCalendar calendar = new GregorianCalendar();

		// date now:
		Date now = calendar.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("ddMMyyyy_HHmmss");
		String formattedDate = fmt.format(now);

		Date today = calendar.getTime();
		formattedDate = fmt.format(today);

		return formattedDate;

	}


	// m�todo para realizar las capturas intermedias en funci�n del par�metro del properties
	public static void capturaIntermedia(WebDriver driver, Resultados resultado, String comentario)
	{
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();
		String capturasIntermedias = params.getValorPropiedad("evidencias");

		if(capturasIntermedias.equalsIgnoreCase("SI"))
		{
			String captura = takeScreenShot(driver, resultado.getNombreCP());
			Log.write("Evidencia: " + comentario + ". Puede ver la captura en: " + captura);
		}

	}


	// m�todo para obtener las opciones de men� configuradas para el entorno en el que se est� ejecutando y la referencia por defecto
	public static String obtenerOpcionesMenuDefecto() throws Exception
	{
		String opcionesMenu = "";
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosReferenciaFijos");
		String ventorno = params.getValorPropiedad("entorno");
		String vreferencia = params.getValorPropiedad("referenciaPorDefecto");
		// Buscamos las opciones de men� guardadas para la referencia por defecto y el entorno en el que se ejecuta la prueba
		CsvReader reader = new CsvReader(new FileReader(rutaCSV));
		while(reader.readRecord())
		{
			String entorno = reader.get(0);
			String referencia = reader.get(1);
			if((entorno.equalsIgnoreCase(ventorno)) && (referencia.equalsIgnoreCase(vreferencia)))
			{
				opcionesMenu = reader.get(7);
				break;
			}
		}
		if(opcionesMenu.equals(""))
			Log.write("No se han encontrado opciones de men� en el fichero " + rutaCSV + ", no se va a poder realizar la prueba");
		reader.close();
		return opcionesMenu;
	}


	public static String devuelveBloqueServicioGenerico(String tipoServicio) throws Exception
	{
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosReferenciaFijos");
		String vEntorno = params.getValorPropiedad("entorno");
		String vReferencia = params.getValorPropiedad("referenciaPorDefecto");

		String BloqueServicio = "";

		// Buscamos las opciones de men� guardadas para la referencia por defecto y el entorno en el que se ejecuta la prueba
		CsvReader reader = new CsvReader(new FileReader(rutaCSV));
		while(reader.readRecord())
		{
			String entorno = reader.get(0);
			String referencia = reader.get(1);
			if((entorno.equalsIgnoreCase(vEntorno)) && (referencia.equalsIgnoreCase(vReferencia)))
			{
				if(tipoServicio.equalsIgnoreCase("BloqueServicioSinFirma"))
				{
					BloqueServicio = reader.get(2);
					break;
				}
				else if(tipoServicio.equalsIgnoreCase("ServicioSinFirma"))
				{
					BloqueServicio = reader.get(3);
					break;
				}
				else if(tipoServicio.equalsIgnoreCase("BloqueServicioConFirma"))
				{
					BloqueServicio = reader.get(4);
					break;
				}
				else if(tipoServicio.equalsIgnoreCase("ServicioConFirma"))
				{
					BloqueServicio = reader.get(5);
					break;
				}
				else if(tipoServicio.equalsIgnoreCase("BloqueNServiciosConFirma"))
				{
					BloqueServicio = reader.get(6);
					break;
				}
			}
		}
		if(BloqueServicio.equals(""))
			Log.write("No se ha encontrado BloqueServicioSinFirma en el fichero " + rutaCSV + ", no se va a poder realizar la prueba");
		reader.close();

		return BloqueServicio;
	}


	public static void CreaFicheroLanzamientoSimultaneo() throws Exception
	{
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();

		int numeroEjecucionesSimultaneas = Integer.parseInt(params.getValorPropiedad("numeroEjecucionesSimultaneas"));

		File statText = new File("resources/LanzamientoSimultaneo.bat");
		FileOutputStream is = new FileOutputStream(statText);
		OutputStreamWriter osw = new OutputStreamWriter(is);
		Writer w = new BufferedWriter(osw);

		for(int i = 1; i <= numeroEjecucionesSimultaneas; i++)
		{
			w.write("START java -jar Main.jar " + i + ">results\testMain" + i + ".txt 2>&1\n");
		}

		w.close();
	}


	public static void crearBloqueo() throws Exception
	{

		String lockFile = "resources/file.lock";
		int contador = 0;

		boolean existeBloqueo = new File(lockFile).exists();

		// Esperamos si alg�n caso lo tiene bloqueado
		while(existeBloqueo && contador <= 60)
		{
			Thread.sleep(1000);
			contador++;
			existeBloqueo = new File(lockFile).exists();
		}

		// Creamos el bloqueo
		File archivoBloqueo = new File(lockFile);
		archivoBloqueo.createNewFile();
	}


	public static void crearFicheroRunning(String numero) throws Exception
	{
		try
		{
			String lockFile = "resources/Thread" + numero + ".running";
			File archivoBloqueo = new File(lockFile);
			archivoBloqueo.createNewFile();
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
		}

	}


	public static void borrarFicheroRunning(String numero) throws Exception
	{
		try
		{
			File archivoBloqueo = new File("resources/Thread" + numero + ".running");
			if(archivoBloqueo.exists())
				archivoBloqueo.delete();
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
		}
	}


	public static void liberarBloqueo() throws Exception
	{
		File archivoBloqueo = new File("resources/file.lock");
		archivoBloqueo.delete();
	}


	public static double roundTwoDecimals(double d)
	{
		DecimalFormat twoDForm = new DecimalFormat("#.#");
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		twoDForm.setDecimalFormatSymbols(dfs);
		return Double.valueOf(twoDForm.format(d));
	}


	public static String estadoCaso(String testID)
	{
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();

		String archivoResultados = params.getValorPropiedad("rutaResultados");

		String estadoCaso;

		try
		{
			CsvReader reader = new CsvReader(new FileReader(archivoResultados));

			estadoCaso = "";

			while(reader.readRecord())
			{
				String id = reader.get(0);
				if(id.equalsIgnoreCase(testID))
				{
					estadoCaso = reader.get(2);
					break;
				}

			}
		}
		catch(Exception e)
		{
			estadoCaso = "";
		}

		return estadoCaso;

	}


	public static String devuelveCasoDelQueDepende(String testID)
	{
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();

		String archivoLanzamiento = params.getValorPropiedad("rutaLanzamiento");

		String nombreCaso;

		try
		{
			CsvReader reader = new CsvReader(new FileReader(archivoLanzamiento));

			nombreCaso = "";

			while(reader.readRecord())
			{
				String id = reader.get(0);
				if(id.equalsIgnoreCase(testID))
				{
					nombreCaso = reader.get(1);
					break;
				}

			}
		}
		catch(Exception e)
		{
			nombreCaso = "";
		}

		return nombreCaso;

	}


	public static String tipoPoderPorIdioma(String tipoPoder)
	{

		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();

		String idioma = params.getValorPropiedad("idioma");

		String tipoPoderTraducido = tipoPoder;

		if(idioma.equals("EN"))
		{
			if(tipoPoder == "Sin poderes")
				tipoPoderTraducido = "Without powers";
			if(tipoPoder == "Solidario-Indistinto")
				tipoPoderTraducido = "Joint-Several";
			if(tipoPoder == "Mancomunado 2")
				tipoPoderTraducido = "Joint 2";
			if(tipoPoder == "Mancomunado 3")
				tipoPoderTraducido = "Joint 3";
			if(tipoPoder == "Mancomunado 4")
				tipoPoderTraducido = "Joint 4";
		}

		return tipoPoderTraducido;

	}


	public static String tipoFirmaPorIdioma(String tipoFirma)
	{

		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();
		String idioma = params.getValorPropiedad("idioma");

		String tipoFirmaTraducida = tipoFirma;

		if(idioma.equals("EN"))
		{
			if(tipoFirma == "Auditor")
				tipoFirmaTraducida = "Auditor";
			if(tipoFirma == "Apoderado")
				tipoFirmaTraducida = "Account holder";
		}

		return tipoFirmaTraducida;

	}


	public static String paisPorIdioma(String pais)
	{

		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();
		String idioma = params.getValorPropiedad("idioma");

		String paisTraducido = pais;

		if(idioma.equals("ES"))
			paisTraducido = "Espa�a";
		if(idioma.equals("EN"))
			paisTraducido = "Spain";

		return paisTraducido;

	}


	// Devuelve los datos de la ejecuci�n del HTML creado
	public static String devuelveDatosDeEjecucion(String datoBuscar)
	{

		String valor = "";

		try
		{
			File archivoHTML = new File("results/REST/InformeServiciosREST.html");
			Document doc = Jsoup.parse(archivoHTML, null);

			if(datoBuscar.equals("TOTAL"))
				valor = doc.select("th:contains(" + datoBuscar + ")").first().nextElementSibling().childNode(0).toString();
			else
				valor = doc.select("td:contains(" + datoBuscar + ")").first().nextElementSibling().childNode(0).toString();
			return valor;
		}
		catch(IndexOutOfBoundsException e)
		{
			return "";
		}
		catch(Exception e)
		{
			utils.Log.writeException(e);
			return "";
		}

	}


	public static boolean estamosEnLatam()
	{
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();
		String pais = params.getValorPropiedad("pais");

		return Latam.contains(pais);

	}


	public static boolean estamosEnFirefox()
	{
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();
		String navegador = params.getValorPropiedad("browser");

		return navegador.equalsIgnoreCase("firefox");

	}


	public static void cambioFramePrincipalMultiNavegador(WebDriver driver)
	{
		if(!estamosEnFirefox())
			UtilsRest.cambiarFramePrincipal(driver);
	}


	public static boolean isAlertPresent(WebDriver driver)
	{
		try
		{
			driver.switchTo().alert();
			return true;
		}
		catch(NoAlertPresentException Ex)
		{
			return false;
		}
	}


	public static void cambiarFramePrincipal(WebDriver driver)
	{
		WebElement frame = driver.findElement(By.xpath(Constantes.FramePrincipal));
		driver.switchTo().frame(frame);
	}


	public static void setBBVAProxy()
	{
//		TestProperties params = new TestProperties(Utils.getProperties());
//		params.cargarPropiedades();
//
//		// Establecemos el proxy
//		if (Utils.insideBBVA()) {
//
//			String clave = Utils.devuelveClaveCifrada();
//	
//			System.setProperty("http.proxyHost", Constantes.proxy_hostname);
//			System.setProperty("http.proxyPort", String.valueOf(Constantes.proxy_port));
//			System.setProperty("http.proxyUser", "xe39619");
//			System.setProperty("http.proxyPassword", clave);
//		}
	}


	public static String extraerDigitosDeUnString(String mensaje)
	{
		return mensaje.replaceAll("\\D+", "");
	}


	public static String cadenaAlfaNumerica(int len)
	{
		final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder(len);
		for(int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}


	public static File BajarC204(String fileName) throws IOException, Exception
	{

		// Establecemos el proxy
		UtilsRest.setBBVAProxy();

		File fichero = null;

		try
		{
			URL website = new URL("http://146.148.15.138/testing/" + fileName);
			fichero = new File("downloads/" + fileName);
			org.apache.commons.io.FileUtils.copyURLToFile(website, fichero);

		}
		catch(Exception e)
		{
			fichero = null;
			Log.write("No existe una versi�n del C204: " + fileName + " subida en el cloud. Se parte del b�sico.");
		}

		return fichero;

	}


	public static String ComentarioCaso(String testID)
	{
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();

		String archivoResultados = params.getValorPropiedad("rutaResultados");

		String comentarioCaso;

		try
		{
			CsvReader reader = new CsvReader(new FileReader(archivoResultados));

			comentarioCaso = "";

			while(reader.readRecord())
			{
				String id = reader.get(0);
				if(id.equalsIgnoreCase(testID))
				{
					comentarioCaso = reader.get(3);
					break;
				}

			}
		}
		catch(Exception e)
		{
			comentarioCaso = "";
		}

		return comentarioCaso;

	}


	public void SubirC204(String OUTPUT_ZIP_FILE) throws IOException, Exception
	{

			Utils.setBBVAProxy();

//          AppZip appZip = new AppZip();
//      	
//          //Cogemos datos para crear el nombre de la carpeta
//      	String SOURCE_FOLDER = ""; 			
//  		String nombreCarpeta = "Pruebas_Automaticas";
//      	
//      	//Creamos la carpeta para guardar la movida
//  		boolean success = (new File(nombreCarpeta+"/")).mkdirs();
//      	
//      	if (!success) throw new Exception("No se pudo crear la carpeta para hacer el zip");
//      	
//      	AppZip.copyFolder(new File(new File("dist/").getAbsolutePath()), new File(nombreCarpeta+"/"));
//      	
//      	SOURCE_FOLDER = new File(nombreCarpeta).getAbsolutePath();
//      	
//      	appZip.generateFileList(new File(SOURCE_FOLDER),SOURCE_FOLDER);
//      	appZip.zipItWithEmptyFolders(OUTPUT_ZIP_FILE,SOURCE_FOLDER);
//      	
//      	//Borramos la carpeta local porque ya tenemos el zip
//      	FileUtils.deleteDirectory((new File(nombreCarpeta)));

		// Subimos el ZIP
		TestApplet peticion = new TestApplet();

		String Fichero = OUTPUT_ZIP_FILE;
		peticion.multipartRequest("http://146.148.15.138/servletFileUpload/UploadDownloadFileServlet", "", Fichero, Fichero);


	}


	public static String devuelveNombreServicio(String nombre)
	{
		return nombre.substring(nombre.substring(0, nombre.length() - 1).lastIndexOf("/"));
	}


	public static String devuelveRestRequest(String URL, String servicio, String parametros, String contexto) throws Exception
	{
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(URL + servicio + "?" + parametros);
		post.addHeader("Content-Type", "application/x-www-form-urlencoded");

		if(contexto.equalsIgnoreCase("SI"))
		{

			post.addHeader("iv-origen", UtilsRest.devuelveParametroCabecera(params.getValorPropiedad("iv-origen"), "iv-origen", parametros, servicio));
			iv_origen = UtilsRest.devuelveParametroCabecera(params.getValorPropiedad("iv-origen"), "iv-origen", parametros, servicio);

			post.addHeader("iv-id_sesion_ast", UtilsRest.devuelveParametroCabecera(params.getValorPropiedad("iv-id_sesion_ast"), "iv-id_sesion_ast", parametros, servicio));
			iv_id_sesion_ast = UtilsRest.devuelveParametroCabecera(params.getValorPropiedad("iv-id_sesion_ast"), "iv-id_sesion_ast", parametros, servicio);

			post.addHeader("iv-cod_canal", UtilsRest.devuelveParametroCabecera(params.getValorPropiedad("iv-cod_canal"), "iv-cod_canal", parametros, servicio));
			iv_cod_canal = UtilsRest.devuelveParametroCabecera(params.getValorPropiedad("iv-cod_canal"), "iv-cod_canal", parametros, servicio);

			post.addHeader("iv-cod_ban_int", UtilsRest.devuelveParametroCabecera(params.getValorPropiedad("iv-cod_ban_int"), "iv-cod_ban_int", parametros, servicio));
			iv_cod_ban_int = UtilsRest.devuelveParametroCabecera(params.getValorPropiedad("iv-cod_ban_int"), "iv-cod_ban_int", parametros, servicio);

			post.addHeader("iv-cod_emp", UtilsRest.devuelveParametroCabecera(params.getValorPropiedad("iv-cod_emp"), "iv-cod_emp", parametros, servicio));
			iv_cod_emp = UtilsRest.devuelveParametroCabecera(params.getValorPropiedad("iv-cod_emp"), "iv-cod_emp", parametros, servicio);

			post.addHeader("iv-user", UtilsRest.devuelveParametroCabecera(params.getValorPropiedad("iv-user"), "iv-user", parametros, servicio));
			iv_user = UtilsRest.devuelveParametroCabecera(params.getValorPropiedad("iv-user"), "iv-user", parametros, servicio);

			post.addHeader("iv-cod_usu", UtilsRest.devuelveParametroCabecera(params.getValorPropiedad("iv-cod_usu"), "iv-cod_usu", parametros, servicio));
			iv_cod_usu = UtilsRest.devuelveParametroCabecera(params.getValorPropiedad("iv-cod_usu"), "iv-cod_usu", parametros, servicio);

		}

		StringEntity input = new StringEntity("product");
		post.setEntity(input);
		HttpResponse response = client.execute(post);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		String mensaje = "";
		while((line = rd.readLine()) != null)
		{
			mensaje += line;
		}

		if(mensaje == "")
			mensaje = response.getStatusLine().toString();

		return mensaje;
	}


	public static String makeRequest(String uri, String json)
	{
		try
		{
			HttpPost httpPost = new HttpPost(uri);
			httpPost.setEntity(new StringEntity(json));
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");
			HttpResponse response = new DefaultHttpClient().execute(httpPost);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String line = "";
			String mensaje = "";
			while((line = rd.readLine()) != null)
			{
				mensaje += line;
			}

			if(mensaje == "")
				mensaje = response.getStatusLine().toString();

			return mensaje;

		}
		catch(UnsupportedEncodingException e)
		{
			utils.Log.writeException(e);
		}
		catch(ClientProtocolException e)
		{
			utils.Log.writeException(e);
		}
		catch(IOException e)
		{
			utils.Log.writeException(e);
		}
		return "";
	}


	public static String procesarParametros(String parametros)
	{

		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();
		String nombre = "";
		String valor = "";
		String cadenaResultante = "";

		// codCanal&codBancoInterno&codEmpresa&codBancoProd&codProducto&codSubproducto&codUsuario

		List<String> listaParametros = new ArrayList<String>();
		String palabra = "";

		for(int i = 0; i < parametros.length(); i++)
		{
			char c = parametros.charAt(i);
			if(c == '&')
			{
				listaParametros.add(palabra);
				palabra = "";
			}
			else
			{
				palabra = palabra + c;
			}
		}

		// A�adimos la �ltima palabra que no acaba con &
		listaParametros.add(palabra);

		List<String> listaParametrosFinal = new ArrayList<String>();

		for(int i = 0; i < listaParametros.size(); ++i)
		{
			if(listaParametros.get(i).indexOf("=") == -1)
			{
				nombre = listaParametros.get(i);
				valor = params.getValorPropiedad(listaParametros.get(i));
				if(valor == null)
					valor = "";
				listaParametrosFinal.add(nombre + "=" + valor);
			}
			else
			{
				listaParametrosFinal.add(listaParametros.get(i));
			}
		}

		// Y quitamos los que tienen asterisco
		for(int i = 0; i < listaParametrosFinal.size(); ++i)
		{
			cadenaResultante = cadenaResultante + "&" + listaParametrosFinal.get(i).replace("*", "");
		}

		return cadenaResultante.substring(1);
	}


	public static void crearFichero(String nombre, String servicio, String resultado, String URL, String parametros, String contexto) throws Exception
	{
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();
		String entorno = params.getValorPropiedad("entorno");

		File time = new File("results/REST/" + nombre + ".txt");
		FileWriter fileHtm = new FileWriter(time.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fileHtm);
		bw.write("--------------------------------------------------");
		bw.newLine();
		bw.write("SERVICIO: " + devuelveNombreServicio(servicio).replace("/", ""));
		bw.newLine();
		bw.write("ENTORNO :" + entorno);
		bw.newLine();
		bw.write("URL : " + URL + servicio);
		bw.newLine();
		bw.write("PARAMETROS : " + parametros);
		bw.newLine();
		bw.write("CONTEXTO : " + contexto);
		bw.newLine();
		bw.write("FECHA DE INICIO :" + UtilsRest.GetTodayDateAndTime());
		bw.newLine();
		bw.write("--------------------------------------------------");
		bw.newLine();
		bw.newLine();
		bw.write(resultado);
		bw.close();
	}


	public static void crearFicheroParams(String nombre, String servicio, String resultado, String URL) throws Exception
	{
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();
		String entorno = params.getValorPropiedad("entorno");

		File time = new File("results/" + nombre + "_params.txt");
		FileWriter fileHtm = new FileWriter(time.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fileHtm);
		bw.write("--------------------------------------------------");
		bw.newLine();
		bw.write("	SERVICIO: " + servicio);
		bw.newLine();
		bw.write("	ENTORNO : " + entorno);
		bw.newLine();
		bw.write("	URL : " + URL);
		bw.newLine();
		bw.write("--------------------------------------------------");
		bw.newLine();
		bw.newLine();
		bw.write(resultado);
		bw.close();
	}


	public static boolean accederGoogleDrive(WebDriver driver) throws Exception
	{

		boolean resul = false;
		String clave = Utils.devuelveClaveCifrada();

		if(clave.equalsIgnoreCase(""))
			throw new Exception ("Usuario Pass BBVA requerido. Revise Properties");

		driver.get("https://docs.google.com/a/bbva.com/spreadsheets/d/1zu6iwEhICaegBOTV7GzVbar0ZVP1s12S6PD5RhUmbtY/edit#gid=0");
//		driver.get("https://drive.google.com/a/bbva.com/?tab=mo#my-drive");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		if (!driver.toString().contains("ChromeDriver"))
			driver.manage().window().maximize();

		if(Utils.existeElementoTiempo(driver, By.xpath("//input[@id='Email']"), 10))
		{
			driver.findElement(By.xpath("//input[@id='Email']")).sendKeys("antonio.gomez.yunta.contractor@bbva.com");
			driver.findElement(By.xpath("//input[@id='Passwd']")).sendKeys(clave);
			Utils.clickEnElVisible(driver, "//input[@id='signIn']");
			Thread.sleep(7000);
		}
		if(Utils.existeElementoTiempo(driver, By.xpath("//input[@id='ksni_user']"), 10))
		{
			driver.findElement(By.xpath("//input[@id='ksni_user']")).sendKeys("xe39619");
			driver.findElement(By.xpath("//input[@id='ksni_password']")).sendKeys(clave);
			Utils.clickEnElVisible(driver, "//input[contains(@onclick,'comprueba')]");
			Thread.sleep(8000);
		}

		if(!Utils.isDisplayed(driver, "//span[contains(text(),'Error de login')]", 5))
			resul = true;

		return resul;

	}


	public static String devuelveAlerta(String respuesta)
	{
		String alerta;

		try
		{
			int posicionAlerta = respuesta.indexOf("id_alerta\":");
			alerta = respuesta.substring(posicionAlerta, respuesta.indexOf(",", posicionAlerta));
			alerta = alerta.replace("id_alerta\":", "");
			alerta = alerta.replace("\"", "");

		}
		catch(Exception e)
		{
			alerta = "";
		}

		return alerta;
	}


	public static String devuelveTipoAlerta(String respuesta)
	{
		String alerta;

		try
		{
			int posicionAlerta = respuesta.indexOf("codTipo\":");
			alerta = respuesta.substring(posicionAlerta, respuesta.indexOf(",", posicionAlerta));
			alerta = alerta.replace("codTipo\":", "");
			alerta = alerta.replace("\"", "");

		}
		catch(Exception e)
		{
			alerta = "";
		}

		return alerta;
	}


	public static String devuelveMensaje(String respuesta)
	{
		String alerta;

		try
		{
			int posicionAlerta = respuesta.indexOf("messageId\":");
			alerta = respuesta.substring(posicionAlerta, respuesta.indexOf(",", posicionAlerta));
			alerta = alerta.replace("messageId\":", "");
			alerta = alerta.replace("\"", "");

		}
		catch(Exception e)
		{
			alerta = "";
		}

		return alerta;
	}


	public static String anadeCerosParams(String valor)
	{
		int numeroDigitos = valor.length();

		String respuesta = "";

		for(int i = numeroDigitos; i < 4; i++)
		{
			respuesta = "0" + respuesta;
		}

		return respuesta + valor;

	}


	public static String devuelveParametroCabecera(String defecto, String nombreParam, String parametros, String servicio)
	{

		String valor = "";

		if(nombreParam.equalsIgnoreCase("iv-cod_canal"))
		{
			valor = devuelveValorParametro("codCanal", parametros);
			if(!valor.equalsIgnoreCase(""))
				valor = UtilsRest.anadeCerosParams(valor);
		}

		else if(nombreParam.equalsIgnoreCase("iv-cod_ban_int"))
		{
			valor = devuelveValorParametro("codBancoInterno", parametros);
			if(!valor.equalsIgnoreCase(""))
				valor = UtilsRest.anadeCerosParams(valor);
		}

		else if(nombreParam.equalsIgnoreCase("iv-cod_emp"))
		{
			valor = devuelveValorParametro("codEmpresa", parametros);

			if(valor.equalsIgnoreCase(""))
				valor = devuelveValorParametro("referencia", parametros);

			if(valor.equalsIgnoreCase(""))
				valor = devuelveValorParametro("ref", parametros);

		}

		else if(nombreParam.equalsIgnoreCase("iv-user"))
		{

			String empresa = devuelveValorParametro("codEmpresa", parametros);

			if(empresa.equalsIgnoreCase(""))
				empresa = devuelveValorParametro("referencia", parametros);

			if(empresa.equalsIgnoreCase(""))
				empresa = devuelveValorParametro("ref", parametros);

			valor = devuelveValorParametro("codUsuarioAdmin", parametros);

			if(valor.equalsIgnoreCase(""))
				valor = devuelveValorParametro("codUsuarioValidador", parametros);

			if(valor.equalsIgnoreCase("") && servicio.indexOf("bloquearUsuario") == -1 && servicio.indexOf("eliminarUsuario") == -1 && servicio.indexOf("bloquearDispositivoPorReintentos") == -1)
				valor = devuelveValorParametro("codUsuario", parametros);

			if(valor.equalsIgnoreCase(""))
				valor = devuelveValorParametro("user", parametros);

			if(!valor.equalsIgnoreCase(""))
				valor = UtilsRest.anadeCerosParams(UtilsRest.anadeCerosParams(devuelveValorParametro("codCanal", parametros))
						+ UtilsRest.anadeCerosParams(devuelveValorParametro("codBancoInterno", parametros)) + empresa + valor);
		}

		else if(nombreParam.equalsIgnoreCase("iv-cod_usu"))
		{
			valor = devuelveValorParametro("codUsuarioAdmin", parametros);

			if(valor.equalsIgnoreCase(""))
				valor = devuelveValorParametro("codUsuarioValidador", parametros);

			if(valor.equalsIgnoreCase("") && servicio.indexOf("bloquearUsuario") == -1 && servicio.indexOf("eliminarUsuario") == -1 && servicio.indexOf("bloquearDispositivoPorReintentos") == -1)
				valor = devuelveValorParametro("codUsuario", parametros);

			if(valor.equalsIgnoreCase(""))
				valor = devuelveValorParametro("user", parametros);

		}


		if(valor.equalsIgnoreCase("") && nombreParam.equalsIgnoreCase("iv-user"))
			return "";
		else if(valor.equalsIgnoreCase("") && nombreParam.equalsIgnoreCase("iv-cod_usu"))
			return "";
		else if(valor.equalsIgnoreCase("") && nombreParam.equalsIgnoreCase("iv-cod_emp"))
			return "";
		else if(valor.equalsIgnoreCase(""))
			return defecto;
		else
			return valor;
	}


	public static String devuelveValorParametro(String nombreParam, String parametros)
	{

		String resultado = "";

		try
		{
			int posicion = parametros.indexOf(nombreParam);

			if(posicion == -1)
				return "";

			int posicion2 = parametros.indexOf("&", posicion);
			if(posicion2 == -1)
				posicion2 = parametros.length();
			resultado = parametros.substring(posicion + nombreParam.length() + 1, posicion2);
			return resultado;
		}
		catch(Exception e)
		{
			return "";
		}
	}
	
	private static class DefaultTrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }
	
	
	public static Proxy setProxy () {
		
		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Constantes.proxy_hostname, Constantes.proxy_port));
		
		Authenticator authenticator = new Authenticator() {

	        public PasswordAuthentication getPasswordAuthentication() {
	            return (new PasswordAuthentication(Constantes.proxy_user, Utils.devuelveClaveCifrada().toCharArray()));
	        }
	    };
	    Authenticator.setDefault(authenticator);
	    
	    return proxy;
	}
	
	
	public static String anadirResultadoAPIResultadosAzure(String pais, String entorno, String navegador, String fecha, String horaInicio, String horaFin, String ejecutados, String ok, String error, String avisos, String csv, String html, String c204, String uuaas, String usuario, String tipoEjecucion, String casosEjecutados) throws Exception
	{
		
		String contentType = "application/json";

		TestApplet peticion = new TestApplet();


//		String uri = "https://resultados-testing.herokuapp.com/api/resultados";
		String uri = "https://app-resultsauto.northeurope.cloudapp.azure.com/api-qa/resultados";
		
		Proxy proxy = null;
		
		// Establecemos el proxy
		if (Utils.insideBBVA()) {
			proxy = setProxy();
		}
		
		 // configure the SSLContext with a TrustManager
        SSLContext ctx = SSLContext.getInstance("TLSv1.2");
        ctx.init(new KeyManager[0], new TrustManager[] {new DefaultTrustManager()}, new SecureRandom());
        SSLContext.setDefault(ctx);

        URL url = new URL(uri);
        
        HttpURLConnection conn;
        
		if (proxy==null)
        	conn = (HttpURLConnection) url.openConnection();
        else
        	conn = (HttpURLConnection) url.openConnection(proxy);
        
        
        conn.setRequestMethod("POST");
        
        if (!contentType.equals(""))
        	conn.setRequestProperty("Content-Type", contentType);
        
        
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestProperty("Authorization", peticion.getTokenAzure());

        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
        
//        Date ahora = new Date();
        
        GregorianCalendar calendar = new GregorianCalendar();

		// date now:
		Date now = calendar.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		String formattedDate = fmt.format(now);

		Date today = calendar.getTime();
		formattedDate = fmt.format(today);

        writer.write("{\n    \"pais\": \""+pais+"\",\n    "
        		+ "\"entorno\": \""+entorno+"\",\n    "
        		+ "\"navegador\": \""+navegador+"\",\n    "
        		+ "\"fecha\": \""+fecha+"\",\n    "
        		+ "\"horaInicio\": \""+horaInicio+"\",\n    "
        		+ "\"horaFin\": \""+horaFin+"\",\n    "
        		+ "\"ejecutados\": \""+ejecutados+"\",\n    "
        		+ "\"ok\": \""+ok+"\",\n    "
        		+ "\"error\": \""+error+"\",\n    "
        		+ "\"avisos\": \""+avisos+"\",\n    "
        		+ "\"csv\": \""+csv+"\",\n    "
        		+ "\"html\": \""+html+"\",\n    "
        		+ "\"c204\": \""+c204+"\",\n    "
        		+ "\"uuaas\": \""+uuaas+"\",\n    "
        		+ "\"usuario\": \""+usuario+"\", \n "
        		+ "\"tipoEjecucion\": \""+tipoEjecucion+"\", \n "
        		+ "\"casosEjecutados\": \""+casosEjecutados+"\", \n "
        		+ "\"sysdate\": \""+formattedDate+"\"  }");
        writer.flush();

        String line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        

        while ((line = reader.readLine()) != null) {
            Log.write(line);
        }

        
        
        writer.close();
        reader.close();         

       
        int status = conn.getResponseCode();

        
        conn.disconnect();

			
		return "";
	}
	
	
//	public static String anadirResultadoAPIResultados(String pais, String entorno, String navegador, String fecha, String horaInicio, String horaFin, String ejecutados, String ok, String error, String avisos, String csv, String html, String c204, String uuaas, String usuario, String tipoEjecucion, String casosEjecutados) throws Exception
//	{
//		
//		String contentType = "application/json";
//		
//		String uri = "https://resultados-testing.herokuapp.com/api/resultados";
//		
//		//Proxy instance, proxy ip = 10.0.0.1 with port 8080
//		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("cacheedi1.igrupobbva", 8080));
//		
//		 // configure the SSLContext with a TrustManager
//        SSLContext ctx = SSLContext.getInstance("TLS");
//        ctx.init(new KeyManager[0], new TrustManager[] {new DefaultTrustManager()}, new SecureRandom());
//        SSLContext.setDefault(ctx);
//
//        URL url = new URL(uri);
//        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection(proxy);
//        conn.setRequestMethod("POST");
//        
//        if (!contentType.equals(""))
//        	conn.setRequestProperty("Content-Type", contentType);
//        
//        conn.setHostnameVerifier(new HostnameVerifier() {
//            @Override
//            public boolean verify(String arg0, SSLSession arg1) {
//                return true;
//            }
//        });
//        
//        conn.setDoInput(true);
//        conn.setDoOutput(true);
//
//        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
//        
////        Date ahora = new Date();
//        
//        GregorianCalendar calendar = new GregorianCalendar();
//
//		// date now:
//		Date now = calendar.getTime();
//		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//		String formattedDate = fmt.format(now);
//
//		Date today = calendar.getTime();
//		formattedDate = fmt.format(today);
//
//        writer.write("{\n    \"pais\": \""+pais+"\",\n    "
//        		+ "\"entorno\": \""+entorno+"\",\n    "
//        		+ "\"navegador\": \""+navegador+"\",\n    "
//        		+ "\"fecha\": \""+fecha+"\",\n    "
//        		+ "\"horaInicio\": \""+horaInicio+"\",\n    "
//        		+ "\"horaFin\": \""+horaFin+"\",\n    "
//        		+ "\"ejecutados\": \""+ejecutados+"\",\n    "
//        		+ "\"ok\": \""+ok+"\",\n    "
//        		+ "\"error\": \""+error+"\",\n    "
//        		+ "\"avisos\": \""+avisos+"\",\n    "
//        		+ "\"csv\": \""+csv+"\",\n    "
//        		+ "\"html\": \""+html+"\",\n    "
//        		+ "\"c204\": \""+c204+"\",\n    "
//        		+ "\"uuaas\": \""+uuaas+"\",\n    "
//        		+ "\"usuario\": \""+usuario+"\", \n "
//        		+ "\"tipoEjecucion\": \""+tipoEjecucion+"\", \n "
//        		+ "\"casosEjecutados\": \""+casosEjecutados+"\", \n "
//        		+ "\"sysdate\": \""+formattedDate+"\"  }");
//        writer.flush();
//
//        String line;
//        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//
//        while ((line = reader.readLine()) != null) {
//            Log.write(line);
//        }
//
//        
//        
//        writer.close();
//        reader.close();         
//
//       
//        int status = conn.getResponseCode();
//
//        
//        conn.disconnect();
//
//			
//		return "";
//	}
	
	
	public static String makeRequestKYNF(String uri, String json) throws Exception
	{
		
		String contentType = "application/json";
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		
		//Proxy instance, proxy ip = 10.0.0.1 with port 8080
		Proxy proxy = null;
		
		// Establecemos el proxy
		if (Utils.insideBBVA()) {
			proxy = setProxy();
		}
		 // configure the SSLContext with a TrustManager
        SSLContext ctx = SSLContext.getInstance("TLS");
        ctx.init(new KeyManager[0], new TrustManager[] {new DefaultTrustManager()}, new SecureRandom());
        SSLContext.setDefault(ctx);

        URL url = new URL(uri);
        HttpURLConnection conn;
        
        if (proxy==null)
        	conn = (HttpURLConnection) url.openConnection();
        else
        	conn = (HttpURLConnection) url.openConnection(proxy);
        
        conn.setRequestMethod("POST");
        
        conn.setRequestProperty("aap","00000016");
        conn.setRequestProperty("iv-user", "0023000120072852ADMIN001");
        conn.setRequestProperty("iv-ticket", "68hckIH+c+8lg1sBdyScpQ3gASYOeji47KzvmzB8jnDMxdGNrL76T+ShTOTl9Ze48ywB6dzeBQM=");
        
        if (!contentType.equals(""))
        	conn.setRequestProperty("Content-Type", contentType);
        
//        conn.setHostnameVerifier(new HostnameVerifier() {
//            @Override
//            public boolean verify(String arg0, SSLSession arg1) {
//                return true;
//            }
//        });
        
        conn.setDoInput(true);
        conn.setDoOutput(true);

        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
        
//        Date ahora = new Date();
        
        GregorianCalendar calendar = new GregorianCalendar();

		// date now:
		Date now = calendar.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		String formattedDate = fmt.format(now);

		Date today = calendar.getTime();
		formattedDate = fmt.format(today);

        writer.write(json);
        writer.flush();

        String line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

        while ((line = reader.readLine()) != null) {
            Log.write(line);
        }

        
        
        writer.close();
        reader.close();         

       
        int status = conn.getResponseCode();

        
        conn.disconnect();

			
		return "";
	}
	
	
	public static String anadirResultadoIncis(String grupo, String estado, String baja, String media, String alta, String critica) throws Exception
	{
		
		// NOTA: Subida manual usr:selenium pass:qwerty99
		
		Proxy proxy = null;
		
		// Establecemos el proxy
		
		
		String contentType = "application/json";
		String texto = "{\"grupo\": \""+grupo+"\","
				+ "\"estado\": \""+estado+"\","
				+ "\"baja\": \""+baja+"\","
				+ "\"media\": \""+media+"\","
				+ "\"alta\": \""+alta+"\","
				+ "\"critica\": \""+critica+"\"  }";
	
		if (Utils.insideBBVA()) {
			proxy = setProxy();
		}

		
		
		 SSLContext ctx = SSLContext.getInstance("TLS");
	        ctx.init(new KeyManager[0], new TrustManager[] {new DefaultTrustManager()}, new SecureRandom());
	        SSLContext.setDefault(ctx);

	        URL url = new URL("https://app-resultsauto.northeurope.cloudapp.azure.com/api/incidencias");
	        
	        HttpsURLConnection conn;
	        
	        if (proxy==null)
	        	conn = (HttpsURLConnection) url.openConnection();
	        else
	        	conn = (HttpsURLConnection) url.openConnection(proxy);
	        
	        conn.setRequestMethod("PUT");
	        
	        conn.setRequestProperty("Accept-Charset", "UTF-8");
	        conn.setRequestProperty("charset", "UTF-8");
	        
	        if (!contentType.equals(""))
	        	conn.setRequestProperty("Content-Type", contentType);
	        
	        conn.setHostnameVerifier(new HostnameVerifier() {
	            public boolean verify(String arg0, SSLSession arg1) {
	                return true;
	            }
	        });
	        
	        conn.setDoInput(true);
	        conn.setDoOutput(true);

	        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream(),"UTF-8");
	        writer.write(texto);
	        
	        writer.flush();

	        String line;
	        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));

	        while ((line = reader.readLine()) != null) {
	            Log.write(line);
	        }

	        
	        
	        writer.close();
	        reader.close();         

	       
	        int status = conn.getResponseCode();

	        
	        conn.disconnect();
		
//		//URL url = new URL("https://resultados-testing.herokuapp.com/api/incidencias");
//		URL url = new URL("https://app-resultsauto.northeurope.cloudapp.azure.com/api/incidencias");
//		HttpURLConnection httpCon = (HttpURLConnection) url.openConnection(proxy);
//		httpCon.setDoOutput(true);
//		httpCon.setRequestMethod("PUT");
//		httpCon.setRequestProperty("Content-Type", contentType);
//		OutputStreamWriter out = new OutputStreamWriter(
//		    httpCon.getOutputStream());
//		out.write(texto);
//		out.close();
//		httpCon.getInputStream();
//		httpCon.disconnect();

			
		return "";
	}
	

}
