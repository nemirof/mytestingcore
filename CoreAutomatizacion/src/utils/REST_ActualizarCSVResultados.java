package utils;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Scanner;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;


public class REST_ActualizarCSVResultados extends TestCase
{
	private WebDriver driver;

	public String m_nombre = this.getClass().getSimpleName();

	public String m_codigo = "REST_0002";

	private Resultados resultado;

	int contador = 0;

	boolean cadenaValida = false;

	boolean loginOK = false;

	String codigoFicheroHTML = "";

	String codigoFicheroCSV = "";

	String codigoFicheroC204 = "";


	@Before
	public void setUp() throws Exception
	{
		resultado = new Resultados(m_nombre);
		driver = Utils.inicializarEjecucion(m_codigo, m_nombre);
	}


	@Test
	public void testActualizarCSVResultados() throws Exception
	{

		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();
		String destinoResultados = params.getValorPropiedad("destinoResultados");
		String nombreC204 = params.getValorPropiedad("nombreC204").toLowerCase();
		String generarFichero = params.getValorPropiedad("generarFichero").toLowerCase();
		String Entorno = UtilsRest.devuelveDatosDeEjecucion("Entorno");
		String Fecha = UtilsRest.devuelveDatosDeEjecucion("Fecha de Ejecuci");
		String HoraInicio = UtilsRest.devuelveDatosDeEjecucion("Hora inicio");
		String HoraFin = UtilsRest.devuelveDatosDeEjecucion("Hora fin");
		String CPTotal = UtilsRest.devuelveDatosDeEjecucion("TOTAL");
		String CPOK = UtilsRest.devuelveDatosDeEjecucion("Satisfactorios");
		String CPError = UtilsRest.devuelveDatosDeEjecucion("Error");
		String CPWarning = UtilsRest.devuelveDatosDeEjecucion("Avisos");

		String urlCloud = Constantes.dnsCloud;
		String dns = "http://146.148.15.138";

		if(contador == 0)
		{

			// Subida a cloud
			if(destinoResultados.toLowerCase().indexOf("cloud") > -1)
			{

				if(HTTPConnect.comprobarRespuestaHTML(urlCloud, "works"))
					dns = urlCloud;
				else
					dns = "http://146.148.15.138";


				TestApplet test = new TestApplet();
				String nombreCarpeta = test.SubirEjecucionGoogleCloudREST("resultadosREST.zip");

				String ficheroHTML = dns + "/testing/" + nombreCarpeta + "/results/InformeServiciosREST.html";
				String ficheroCSV = dns + "/testing/" + nombreCarpeta + "/results/resultsRest.csv";
				String ficheroC204 = dns + "/testing/" + nombreC204;

				codigoFicheroHTML = "<a Style=\"color: #2AABDF;\" href=\"" + ficheroHTML
						+ "\" target=\"_blank\"> <img alt=\"Abrir\" border=\"0\" src=\"https://sites.google.com/a/bbva.com/plataforma_banca_empresas/gestion-y-rendimiento/icOpenT.gif\">Abrir</a>";
				codigoFicheroCSV = "<a Style=\"color: #2AABDF;\" href=\""
						+ ficheroCSV
						+ "\" target=\"_blank\" download=\"csv\"> <img alt=\"Abrir\" border=\"0\" src=\"https://sites.google.com/a/bbva.com/plataforma_banca_empresas/gestion-y-rendimiento/icGuardarT.gif\">Guardar</a>";
				codigoFicheroC204 = "<a Style=\"color: #2AABDF;\" href=\""
						+ ficheroC204
						+ "\" target=\"_blank\" download=\"csv\"> <img alt=\"Abrir\" border=\"0\" src=\"https://sites.google.com/a/bbva.com/plataforma_banca_empresas/gestion-y-rendimiento/icGuardarT.gif\">Guardar</a>";

				if(generarFichero.equalsIgnoreCase("SI"))
				{

					File temp = new File("resources/temp.txt");

					if(temp.exists())
					{
						String text = new Scanner(temp, "UTF-8").useDelimiter("\\A").next();
						ficheroC204 = dns + "/testing/" + text;
						temp.deleteOnExit();
					}

					codigoFicheroC204 = "<a Style=\"color: #2AABDF;\" href=\""
							+ ficheroC204
							+ "\" target=\"_blank\" download=\"csv\"> <img alt=\"Abrir\" border=\"0\" src=\"https://sites.google.com/a/bbva.com/plataforma_banca_empresas/gestion-y-rendimiento/icGuardarT.gif\">Guardar</a>";

				}
				else
					codigoFicheroC204 = "N/A";


			}

			// Subida a ad003
			if(destinoResultados.toLowerCase().indexOf("ad003") > -1)
			{

				String entorno = Entorno.substring(0, 3).toUpperCase();

				String fecha = Utils.GetTodayDateAndTimeSFTP();

				String nombreCarpeta = "REST_" + entorno + "_" + fecha;
				String rutaCarpeta = "/de/kygu/online/multipais/web/logs/di/" + nombreCarpeta;

				JSch jsch = new JSch();
				Session session = null;

				try
				{
					session = jsch.getSession("xakygu1d", "ad003", 22);
					session.setConfig("StrictHostKeyChecking", "no");
					session.setPassword("1dxakygu");

					session.connect();

					String logo = "resources/logo.PNG";

					Channel channel = session.openChannel("sftp");
					channel.connect();
					ChannelSftp sftpChannel = (ChannelSftp)channel;
					sftpChannel.mkdir(rutaCarpeta);
					sftpChannel.mkdir(rutaCarpeta + "/resources");
					sftpChannel.mkdir(rutaCarpeta + "/results");
					sftpChannel.mkdir(rutaCarpeta + "/screenshots");

					// Logo
					sftpChannel.put(logo, rutaCarpeta + "/resources");

					// results
					File folder = new File(new File("results/").getAbsolutePath());
					File[] listOfFiles = folder.listFiles();

					for(File file : listOfFiles)
					{
						if(file.isFile())
						{
							sftpChannel.put("results/" + file.getName(), rutaCarpeta + "/results");
						}
					}

					// screenshots
					folder = new File(new File("screenshots/").getAbsolutePath());
					listOfFiles = folder.listFiles();

					for(File file : listOfFiles)
					{
						if(file.isFile())
						{
							sftpChannel.put("screenshots/" + file.getName(), rutaCarpeta + "/screenshots");
						}
					}

					sftpChannel.exit();
					session.disconnect();
				}
				catch(JSchException e)
				{
					e.printStackTrace(); // To change body of catch statement use File | Settings | File Templates.
				}
				catch(SftpException e)
				{
					e.printStackTrace();
				}


				String ficheroHTML = "http://ad003.igrupobbva:6824/kygu_mult_web_testing/" + nombreCarpeta + "/results/ResumenEjecucionPruebas.html";
				String ficheroCSV = "http://ad003.igrupobbva:6824/kygu_mult_web_testing/" + nombreCarpeta + "/results/results.csv";

				codigoFicheroHTML = "<a Style=\"color: #2AABDF;\" href=\"" + ficheroHTML
						+ "\" target=\"_blank\"> <img alt=\"Abrir\" border=\"0\" src=\"https://sites.google.com/a/bbva.com/plataforma_banca_empresas/gestion-y-rendimiento/icOpenT.gif\">Abrir</a>";
				codigoFicheroCSV = "<a Style=\"color: #2AABDF;\" href=\""
						+ ficheroCSV
						+ "\" target=\"_blank\" download=\"csv\"> <img alt=\"Abrir\" border=\"0\" src=\"https://sites.google.com/a/bbva.com/plataforma_banca_empresas/gestion-y-rendimiento/icGuardarT.gif\">Guardar</a>";

			}

		}

		try
		{
			// Controlamos las reejuciones
			contador++;

			// Accedemos al drive
			loginOK = UtilsRest.accederGoogleDrive(driver);
			String nombreFichero = "Resultados pruebas automatizadas";

			if(driver.getTitle().indexOf(nombreFichero) == -1)

			{
				if(driver.getTitle().indexOf("Mi unidad") > -1)
				{

					// Vamos a mi unidad
					String miUnidad = "//span[contains(text(),'Mi unidad')]";
					String myDrive = "//span[contains(text(),'My Drive')]";

					driver.switchTo().defaultContent();
					// driver.switchTo().frame(0);

					if(Utils.isDisplayed(driver, miUnidad, 7))
						Utils.clickEnElVisible(driver, miUnidad);
					else if(Utils.isDisplayed(driver, myDrive, 7))
						Utils.clickEnElVisible(driver, myDrive);

					// Pulsamos en el nombre del fichero
					driver.findElement(By.xpath("//span[@title='" + nombreFichero + "']")).click();
					Thread.sleep(3000);


				}
			}

			String lastWindow = (String)driver.getWindowHandles().toArray()[driver.getWindowHandles().toArray().length - 1];
			this.driver.switchTo().window(lastWindow);

			if(Utils.isDisplayed(driver, "//*[contains(text(),'intentando')]", 10))
			{
				if(contador >= 10)
				{
					fail("Se reintenta 10 veces sin �xito");
				}
				else
					testActualizarCSVResultados();
			}

			// Introducimos una l�nea con los datos de la ejecuci�n
			Utils.esperarVisibleTiempoSinSalir(driver, "//div[@id='docs-insert-menu']", 10);
			Thread.sleep(5000);
			driver.findElement(By.xpath("//div[@id='docs-insert-menu']")).click();
			Thread.sleep(1000);

			// CLICK EN INSERTAR DEBAJO
			String xpath1 = "//div[@id=':6o']";
			String xpath2 = "//strong[contains(text(),'debajo')]";
			String xpath3 = "//strong[contains(text(),'below')]";

			if(Utils.isDisplayed(driver, xpath2, 7))
				Utils.clickEnElVisible(driver, xpath2);
			else if(Utils.isDisplayed(driver, xpath3, 7))
				Utils.clickEnElVisible(driver, xpath3);
			else if(Utils.isDisplayed(driver, xpath1, 7))
				Utils.clickEnElVisible(driver, xpath1);

			Thread.sleep(5000);

			// driver.findElement(By.xpath("//div[@class='goog-inline-block grid4-inner-container']")).click();

			if(Entorno.equals("") || Fecha.equals("") || CPTotal.equals("") || CPOK.equals("") || CPError.equals("") || CPWarning.equals(""))
			{
				if(contador < 10 && !cadenaValida)
					testActualizarCSVResultados();

			}

			else
			{

				driver.switchTo().defaultContent();
				Thread.sleep(500);
				new Actions(driver).sendKeys(Entorno).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(Keys.TAB).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(Fecha).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(Keys.TAB).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(HoraInicio).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(Keys.TAB).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(HoraFin).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(Keys.TAB).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(CPTotal).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(Keys.TAB).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(CPOK).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(Keys.TAB).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(CPError).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(Keys.TAB).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(CPWarning).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(Keys.TAB).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(codigoFicheroCSV).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(Keys.TAB).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(codigoFicheroHTML).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(Keys.TAB).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(codigoFicheroC204).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(Keys.TAB).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(Fecha).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(Keys.TAB).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(HoraInicio).perform();
				Thread.sleep(500);
				new Actions(driver).sendKeys(Keys.TAB).perform();
				Thread.sleep(5000);

				Utils.esperarVisibleTiempoSinSalir(driver, "//div[@id='docs-notice'][contains(text(),'cambios guardados')]", 10);

				cadenaValida = true;
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
			resultado.appendError(e.getMessage());
			// A veces falla porque tarda mucho en responder el drive o por otros motivos
			if(contador < 10 && !cadenaValida && loginOK)
				testActualizarCSVResultados();
		}
	}


	@After
	public void tearDown() throws Exception
	{
		Utils.guardarEjecucion(driver, resultado);
	}
}
