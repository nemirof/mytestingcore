package utils;


import junit.framework.TestCase;


@SuppressWarnings("unused")
public class MailSender extends TestCase
{

	@Override
	public void setUp() throws Exception
	{

	}


	public void testEnviaMail() throws Exception
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		
		//String archivoHTML = "results/ResumenEjecucionPruebas.html";
		
		String uuaa = Utils.devuelveUUAAParaMail();

		Mail.enviaMail("Resumen pruebas Autom�ticas "+uuaa, "Hola, os adjuntamos los resultados de la ejecuci�n autom�tica. El resumen de �sta, ser�a el siguiente:", "","automatizacion-bbva.group@bbva.com");

	}


	@Override
	public void tearDown() throws Exception
	{

	}
}
