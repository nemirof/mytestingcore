package utils;


import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


public class DatosParaRest4 extends TestCase
{
	private WebDriver driver;

	public String m_nombre = this.getClass().getSimpleName();

	public String m_codigo = "REST_DATOS";

	private Resultados resultado;


	@Before
	public void setUp() throws Exception
	{
		resultado = new Resultados(m_nombre);
		driver = Utils.inicializarEjecucion(driver, m_codigo, m_nombre);
	}


	@Test
	public void testDatosParaRest() throws Exception
	{

		UtilsRest.borrarFicheroRest();

		try
		{

			new Constantes();

			Utils.loginPorDefecto(driver);

			TestProperties params = new TestProperties(UtilsRest.getProperties());
			params.cargarPropiedades();

			TestProperties params1 = new TestProperties(Utils.getProperties());
			params1.cargarPropiedades();
			String entorno = params1.getValorPropiedad("entorno");

			if(entorno.equalsIgnoreCase("Produccion"))
			{
				Log.write("La prueba de los servicios REST s�lo aplica en Entornos de Desarrollo e Integrado");
			}

			else
			{

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente2 = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente2.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente2.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "listaUsuariosAValidar*****", AdmPendiente2.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, listaUsuariosAValidar*****");
				}


				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente2 = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente2.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente2.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "listaUsuariosAValidar**", AdmPendiente2.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, listaUsuariosAValidar**");
				}

				try
				{
					// ADM APODERADO CON ACCIONES PENDIENTES

					Usuario AdmPendiente3 = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente3.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente3.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "codUsuario*********", AdmPendiente3.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario*********");
				}

				try
				{
					// ADM APODERADO SIN ACCIONES PENDIENTES

					Usuario AdmPendiente2 = Funciones.altaUsuarioPteActivar(driver, resultado);

					AdmPendiente2.setEstado(Constantes.ESTADO_NO_USAR);
					AdmPendiente2.guardarUsuarioEnCSV();

					params.updateProperties(UtilsRest.getProperties(), "listaUsuariosAValidar*", AdmPendiente2.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, listaUsuariosAValidar*");
				}

				try
				{
					// ADM APODERADO SIN ACCIONES PENDIENTES

					Usuario AdmApoderado8 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado8 == null)
					{
						AdmApoderado8 = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado8.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado8.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuario*******", AdmApoderado8.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, codUsuario*******");
				}

				try
				{
					// ADM APODERADO SIN ACCIONES PENDIENTES

					Usuario AdmApoderado8 = Funciones.devuelveUsuarioPorTipo("Solidario-Indistinto", "Apoderado", Constantes.ESTADO_ACTIVO);

					if(AdmApoderado8 == null)
					{
						AdmApoderado8 = Funciones.altaUsuarioTodoContratado(driver, resultado);
					}
					else
					{
						AdmApoderado8.setEstado(Constantes.ESTADO_NO_USAR);
						AdmApoderado8.guardarUsuarioEnCSV();
					}

					params.updateProperties(UtilsRest.getProperties(), "codUsuario********", AdmApoderado8.getCodUsuario());
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario codUsuario********");
				}


				try
				{
					// USUARIO CON TOKEN
					Token token = Token.obtenerTokenPruebas(params1.getValorPropiedad("referenciaPorDefecto"));
					Funciones.liberarToken(driver, token, resultado);
					token.setTipo(Constantes.TIPO_TOKEN_FISICO);
					Usuario UsuarioToken = Funciones.altaUsuarioNoAdminTokenFisicoAsignado(driver, resultado, token);

					params1.updateProperties(UtilsRest.getProperties(), "codUsuario", UsuarioToken.getCodUsuario());
					params1.updateProperties(UtilsRest.getProperties(), "codUsuarioValidador**", UsuarioToken.getCodUsuario());
					params1.updateProperties(UtilsRest.getProperties(), "nuSerieToken", "VC-01-" + token.getNumero().replace(" ", "").replace("-", ""));
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario con token");
				}

				try
				{
					// TOKEN SIN ASIGNAR

					Token token = Token.obtenerTokenPruebas(params1.getValorPropiedad("referenciaPorDefecto"));
					Token tokenLibre = Funciones.obtenerTokenLibre(driver, resultado, token, params1.getValorPropiedad("referenciaPorDefecto"));
					Funciones.liberarToken(driver, tokenLibre, resultado);
					token.setTipo(Constantes.TIPO_TOKEN_FISICO);

					params1.updateProperties(UtilsRest.getProperties(), "nuSerieToken*", "VC-01-" + tokenLibre.getNumero().replace(" ", "").replace("-", ""));
				}
				catch(Exception e)
				{
					Log.write("Error creando un usuario, nuSerieToken*");
				}


			}


		}
		catch(Exception e)
		{

		}
	}


	@After
	public void tearDown() throws Exception
	{
		Utils.guardarEjecucion(driver, resultado);
	}
}
