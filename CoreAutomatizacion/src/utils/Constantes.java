package utils;


public class Constantes
{
	
	//GALATEA
	public static String carpetaDescargaGalatea = "/home/seluser/downloads";
	
	//INTRANET
	public static String intranetBBVA = "https://bbva-intranet.appspot.com";
	
	//TEMA PROXY
	
	public static String proxy_hostname = "cacheedi1.igrupobbva";//"proxyvip.igrupobbva";//"cacheedi1.igrupobbva";
	
	public static int proxy_port = 8080;
	
	public static String proxy_user = "xe39619";
	
	
	public static String usuarioSFT_PEI = "xtpru01";
	
	public static String passSFTP_EI = "gdepssd1";

	// Estados usuario
	public static String ESTADO_ACTIVO = "Activo";

	public static String ESTADO_BLOQ = "Bloqueado";

	public static String ESTADO_DESBLOQ = "Desbloqueado";

	public static String ESTADO_PDTEACTIVAR = "Pendiente de activar";

	public static String ESTADO_PDTEBLOQ = "Pendiente de bloquear";

	public static String ESTADO_PDTEDESBLOQ = "Pendiente de desbloquear";

	public static String ESTADO_PDTEMODIFICAR = "Pendiente de modificar";

	public static String ESTADO_PDTEBAJA = "Pendiente de baja";

	public static String ESTADO_BAJA = "Baja";

	public static String ESTADO_NO_USAR = "NO_USAR";

	public static String ESTADO_ACTIVO_ACCEDIDO_LATAM = "Activo";

	// Estados dispositivo
	public static String ESTADO_DISP_ACTIVO = "Activo";

	public static String ESTADO_DISP_PDTEACTIVAR = "Pendiente activaci�n";

	public static String ESTADO_DISP_DISPONIBLE = "Disponible";

	public static String ESTADO_DISP_BLOQUEADO = "Bloqueado";

	public static String ESTADO_DISP_DESBLOQUEADO = "Desbloqueado";

	public static String ESTADO_DISP_NO_USAR = "NO_USAR";

	// Tipos dispositivo
	public static String TIPO_TOKEN_MOVIL = "Token M�vil";

	public static String TIPO_TOKEN_FISICO = "Token Plus";

	// BOT�N ADMINISTRADOR VISIBLE
	public static String BotonAdministradorVisible = "//*[contains(@id,'kyop-header-button')][contains(@value,'Administraci�n')][not(contains(@style,'none'))]/span";

	public static String LiteralAdministracionUsuarios = "//li/span[contains(text(),'Administraci�n de usuarios') or contains(text(),'Administraci�n de Usuarios')]";

	// ELEMENTOS COMUNES
	public static String loadingLogin = "//*[contains(@src,'resources-new/images/common/loadingNBbva.gif')]";
	
	public static String FramePrincipal = "//iframe[@id='kyop-central-load-area']";

	public static String FrameLightbox = "//iframe[@id='kyop-lightbox-iframe-div']";

	public static String FrameAvisoLegal = "//iframe[@id='kyop-ads-iframe']";
	
	public static String frameCampanaGenerico = "//iframe[@id='kyop-campaigns-lightbox-iframe-div' or @id='kyop-lightbox-iframe-div']";

	public static String FrameCampana = "//iframe[@id='kyop-campaigns-lightbox-iframe-div']";

	public static String FrameCampana2 = "//iframe[@id='kyop-lightbox-iframe-div']";
	
	public static String OpcionesMenu = "//a[starts-with(@id,'kyop-menuOption-')and contains(@id,'-menuLeft')]";

	public static String OpcionesMenuLATAM = "//span[@class='flechaAbajoMenuGris']/preceding-sibling::a";

	public static String OpcionesMenuTerminalesLATAM = "//a[contains(text(),'')][contains(@onclick,'selectMenuHoverOption')]";

	public static String OpcionesMenuGenerico = "//a[starts-with(@id,'kyop-menuOption-')]";

	public static String OpcionesMenuHijas = "//*[contains(@id,'kyop-opcionMenuHija') and contains(@id,'-menuLeft')]";

	public static String AnadirFavoritos = "//*[@id='kyop-add-favorites']";
	
	public static String tooltipTelefono = "//*[@id='kyop-telephone-body' or @id='kyop-header-telefone' or @class='ventanaModal kyop-internal-container-ventanaModal']";

	public static String MenuNivel3 = "//*[@id='kyop-menu-title-0']";

	public static String MenuTambienLePuedeInteresar = "//*[@id='kyop-menu-relatedservices-title']";

	public static String ActivarAyuda = "//*[contains(@id,'kyop-link-process-help-directy')]";

	public static String DesactivarAyuda = "//*[@*='kyop-help-link-close']";

	public static String DenosSuOpinion = "//*[@id='kyop-assess-netcash']";

	public static String EliminarFavoritos = "//*[@id='kyop-delete-favorites']";

	public static String SpanFavoritos = "//div[contains(@onclick,'kyop-header-arrow-down-favorite')]";

	public static String SpanImprimir = "//span[@class='pointer'][text()='Imprimir']";

	public static String TelefonoContacto = "//*[@onclick='com.bbva.kyop.controller.MenuController.openContactLink()']";

	public static String TelefonoContactoLogin = "//a[contains(@onclick,'kyop-modal-0')]";
	
	public static String literalAtencionCliente = "//a[contains(text(),'Atenci�n al cliente')]";

	public static String TelefonoContactoLogin2 = "//a[@rapage='telefonos']";

	public static String SpanAyuda = "//span[text()='Ayuda' or @value='Ayuda']";

	public static String SpanInformacionProductos = "//span[@class='kyop-pointer kyop-control-click'][text()='Informaci�n de productos']";

	public static String AdminFavoritos = "//*[@id='kyop-admin-favorites']";

	public static String CabeceraAyuda = "//span[contains(text(),'Ayuda')]";

	public static String TituloAyuda = "//h1[contains(text(),'Ayuda')]";

	public static String TituloInformacionProductos = "//h1[contains(text(),'Informaci�n de productos')]";

	public static String BotonVerProductosContratados = "//input[@class='submit'][@value='Ver productos contratados']";

	public static String CabeceraTextoAyuda = "//h2[starts-with(text(),'Consulte los v�deotutoriales')]";

	public static String CabeceraInformacionProductos = "//span[text()='Informaci�n de productos']";

	public static String CabeceraAtencionCliente = "//span[contains(text(),'902 33 53 73') or contains(text(),'91 224 98 02')]";
	
	public static String cabeceraAtencionClienteNuevo = "//span[@id='telefoneTranslate']";
	
	public static String ventanitaTelefonosAtCliente = "//div[@id='kyop-header-telefone']";

	public static String CabeceraTextoServicioAtencionCliente = "//div[starts-with(text(),'Servicio de atenci�n al cliente')]";

	public static String CabeceraIdiomaActual = "//span[@id='kyop-current-language']";

	public static String CabeceraEnlaceCatalan = "//a[text()='Catal�']";

	public static String CabeceraEnlaceIngles = "//a[text()='English']";

	public static String CabeceraEnlacePortugues = "//a[text()='Portugu�s']";

	public static String h1 = "//h1";

	public static String ImagenCerrarVentana = "//img[@id='kyop-lightbox-close-img']";

	public static String ImagenCerrarVentanaModal = "//img[contains(@onclick,'closeModalWindow')]";
	
	public static String divCerrarTooltip = "//*[contains(@onclick,'mostrarOcerrarTooltip') or contains(@onclick,'closeModalWindow')]";
	
	public static String divCerrarVentanaModal = "//*[contains(@onclick,'closeLightBoxKyop')]";

	public static String TituloContenido = "//h2[@id='tituloContenido']";

	public static String TituloAdminFavoritos = "//*[@key='kyop.ntcsh.favorites.adminfavorites']";

	public static String DivTitle = "//div[@class='title']";

	public static String DivOpinion = "//div[contains(@class,'opi-data')]";

	public static String DivClassVentanaModal = "//div[@class='ventanaModal']";

	public static String DivClassVentanaModalGenerico = "//div[contains(@class,'ventanaModal')]";
	
	public static String cerrarVentanaModalGenerico = DivClassVentanaModalGenerico + "/descendant::*[contains(@class,'closeTooltip')]";

	public static String PieEnlaceSeguridad = "//a[text()='Seguridad']";

	public static String PieEnlaceTarifasAvisos = "//a[contains(text(),'Tarifas y otros avisos')]";
	
	public static String tituloTarifas = "//h1[contains(text(),'Tarifas')]";

	public static String PieEnlaceAvisoLegal = "//a[text()='Aviso legal']";
	
	public static String piePoliticaCookies = "//a[@id='kyop-footer-link-cookies']";

	public static String ImagenCargando = "//*[contains(@src,'loadingBbva.gif') or @id='loadderGiftNewHome' or @id='ventanaCapaEspera']";

	public static String ImagenCargando2 = "//*[@id='imgCargando' or @id='contenedorLoader']";
	
	public static String imagenCargandoCoronita = "//*[@id='ventanaCapaEspera'][contains(@style,'display: block')]";

	public static String VentanaEspera = "//div[@id='ventanaCapaEspera']";

	public static String BotonOKMessageDialog = "//div[contains(@id,'messagebox')]/descendant::button[contains(@onclick,'executeCallback()')]";

	public static String ComboIdiomas = "//*[@onclick='showLanguageSelect()' or @id='cboIdioma']";

	public static String IdiomaEspanol = "//a[contains(@href,'es_ES')]";

	public static String IdiomaCatalan = "//a[contains(@href,'ca_ES')]";

	public static String IdiomaPortugues = "//a[contains(@href,'pt_PT')]";

	public static String IdiomaIngles = "//a[contains(@href,'en_EN')]";

	public static String BotonDesconectar = "//*[@id='botonDesconectar' or @id='kyop.ntcsh.header.exitNew']";

	public static String campanaDesplegable = "//div[contains(@class,'bbvaCCBannerEmergente')][contains(@class,'show-emergente')][contains(@class,'open')]";
	
	public static String campanaDesplegableFlecha = "//div[contains(@class,'bbvaCCBannerEmergente')][contains(@class,'show-emergente')][contains(@class,'open')]/descendant::div[@class='bbvaCCBannerEmergente-boton']";
	
	public static String botonAcceso="//header//*[contains(text(),'Acceso')]";
	
	public static String Login_Referencia = "//input[@id='cod_emp' or @id='empresa']";

	public static String Login_Usuario = "//input[@id='cod_usu' or @id='usuario']";

	public static String Login_Password = "//input[@id='eai_password' or @id='clave_acceso']";

	public static String Login_AntiguaPassword = "//input[@name='eai_password']";

	public static String Login_NuevaPassword = "//input[@name='eai_newpassword']";

	public static String Login_ConfirmarPassword = "//input[@name='eai_confpassword']";

	public static String Login_BotonEntrar_Ant = "//button[@type='submit'][not(@id='button_redirect')][contains(@onclick,'validateForm')]";
	public static String Login_BotonEntrar = "//button[@id='cms-auth-business__submit']";

	public static String cerrarModalLogin = "//div[contains(@onclick,'closeReditectModal()') or contains(@onclick,'closeRedirectModal()') or contains(@class,'close_modal_redirect')]";
	
	public static String CampoClaveOperaciones = "//input[@id='claveOperacion']";

	public static String CampoClaveToken = "//input[@id='claveToken']";
	
	public static String campoClaveAcceso = "//input[@id='claveAcceso']";
	
	public static String desafio = "//*[@id='Desafio']";
	
	public static String botonContinuarAcceso = "//button[@id='bContinuarAcceso']";

	public static String CampoClaveOperacionesOtroUsuario = "//input[@id='claveOperacionOtroUsuario']";

	public static String CampoClaveTokenOtroUsuario = "//input[@id='claveTokenOtroUsuario']";

	public static String BotonValidarUsuario = "//button[@id='bValidar']";

	public static String BotonRechazar = "//button[@id='bRechazar_1']";

	public static String BotonRechazarGenerico = "//button[contains(@id,'bRechazar')]";

	public static String DivBienvenida = "//div[@class='bienvenidaNetCash']";

	public static String LinkHome = "//a[@id='botonContinuar']";

	public static String botonContinuarGenerico = "//button[text()='Continuar']";

	public static String BotonValidarGenerico = "//button[contains(@id,'bValidar')]";

	public static String BotonValidarTokenGenerico = "//button[contains(@id,'btnValidarInf')]";

	public static String LinkValidarPendientes = "//*[@id='linkValidarPendientes']";

	public static String validarOtroUsuario = "//label[@for='usuarioSeleccionadoDistinto2'][@class='inputAI']";

	public static String OlvidoSuContrasena = "//a[@id='forgotPasswordEurasia']";

	public static String TituloOlvidoSuContrasena = "//h1[contains(text(),'restablecimiento de contrase�a') or contains(text(),'reestablecimiento de contrase�a')]";

	public static String FormularioOlvidoClave = "//form[@id='olvidoClave']";

	public static String VolverLogin = "//a[contains(text(),'Volver')]";

	public static String catalogoServiciosGenerico = "//a[@rapage='catalogo']";

	public static String AvisoLegal = "//a[@rapage='Aviso legal']";

	public static String TituloAvisoLegal = "//div[@class='avisoLegal']/descendant::*[contains(text(),'Aviso Legal') or contains(text(),'AVISO LEGAL')]";//"//div[contains(text(),'Aviso Legal')]";

	public static String GestionDeDispositivos = "//span[contains(text(),'Gesti�n')][contains(text(),'ispositivos')]";//"//span[contains(text(),'Gesti�n de dispositivos')]";

	public static String eliminarFiltros = "//span[@id='spanEliminaFiltros']";

	public static String flechaArribaTabla = "//a[contains(@href,'formularioPrincipal')]//descendant::p[(@class='icon-paginacionSubir')]";//"//a[contains(@href,'formularioPrincipal')]/img[contains(@src,'flechaArriba')]";


	// MENU
	public static String InformacionDeCuentas = "//a[contains(@id,'kyop-menuOption')][contains(text(),'Informaci�n de cuentas')]";

	public static String SaldosYMovimientos = "//a[contains(@id,'kyop-opcionMenuHija')][contains(text(),'Saldos y movimientos')]";

	// SALDOS Y MOVIMIENTOS
	public static String cuentaSaldosMovimientos = "//span[@class='cuentaLarga']";

	// CUENTA DE PERSONALIZAR CUENTAS
	public static String cuentaPersonalizarCuentas = "//td[@headers='cuenta']";


	public static String usuarioBloqueadoLogin = "//div[@id='alertError']/*[contains(text(),'bloqueado') or contains(text(),'locked')]";


	public static String DivMensajeOk = "//div[@class='alerta bordeOk']";

	public static String DivMensajeAviso = "//div[@class='alerta bordeWarning']";
	
	public static String DivMensajeAvisoGenerico = "//div[contains(@class,'alerta bordeWarning')]";

	public static String DivMensajeInfo = "//div[@class='alerta bordeInfo']";

	// PORTAL
	public static String ResultadosProductosContratados = "//span[@class='title']";

	public static String CountryJumper = "//*[@id='kyop-countryjump-select'][@onclick]";

	public static String TablaCountryJumper = "//table[@id='kyop-countryjump-table']";

	public static String entradaDeMenu = "//*[@class='kyop-li-search ui-menu-item']";
	
	public static String entradaDeMenuTerminal = "//br/preceding-sibling::*[1]/self::strong/ancestor::a";
	
	public static String resultadoNoExistenResultados = "//*[text()='No existen resultados']";
	
	public static String enlaceEntradaDeMenu = "//ul/descendant::li[contains(@class,'ui-menu')]/a";


	// public static String iconoMensajes ="//*[@id='kynf_iconoMensaje']";

	public static String tresRayitasMenu = "//div[contains(@class,'app-header')][contains(@class,'menu')][not(contains(@class,'menuClosed'))]/descendant::span[@class='icon-menu']";

	public static String buscarMenu = "//input[@id='kyop-searchCombo']";

	public static String MigaActual = "//div[@class='miga actual']";

	public static String rastroMigaActual = "//div[@id='migas-wrapper']";

	public static String moduloOperacionesFrecuentes = "//div[@id='kyop-slice-middle-left']";

	public static String ultimaConexionPie = "//div[@id='kyop-footer-info-container-last-connection']";

	public static String VentanaDesconexion = "//div[@id='ventanaFinSesion']";

	public static String botonContinuarSesion = "//button[@id='continuarVentanaFinSesion']";


	// LOGIN LOCAL
	public static String LoginLocal_Referencia = "//input[@id='empresa' or @id='cod_emp']";

	public static String LoginLocal_Usuario = "//input[@id='usuario' or @id='cod_usu']";

	public static String LoginLocal_Password = "//input[@id='clave_acceso' or @id='eai_password']";

	public static String LoginLocal_ComboPais = "//select[@name='lista']";

	public static String LoginLocal_BotonEntrar = "//input[@id='aceptar' or @type='submit']";


	// PANTALLA M�TODO DE FIRMA
	public static String RadioFirmaClaveOperaciones = "//input[@id='eai_firma_opkey']";

	public static String RadioFirmaFormula = "//input[@id='eai_firma_formula']";

	public static String CampoNuevaClaveOperaciones = "//input[@name='eai_claveop']";

	public static String CampoConfirmarNuevaClaveOperaciones = "//input[@name='eai_confclaveop']";

	public static String ComboConstante = "eai_constante";

	public static String ComboOperador = "eai_operador";

	public static String ComboDigito = "eai_digito";

	public static String BotonContinuarAzulGrandote = "//button[contains(@class,'grandote')]"; // [contains(@onclick,'javascript:comprobar')]; //img[contains(@src,'continuar_blanca.png')]

	public static String botonContinuarValue = "//*[contains(@value,'Continuar')]";

	// public static String DivNumeroCalcularFormula="//div[contains(text(),'N�mero')]/following-sibling::div";
	// public static String DivNumeroCalcularFormula="//div[@id='formulaOper']/div[@class='row']/descendant::div[contains(@class,'left')]/h5[not(contains(text(),'o'))]";
	public static String DivNumeroCalcularFormula = "//*[contains(text(),'N�mero')]/ancestor::div[contains(@class,'col-xs-12 col-sm-12 col-md-2')]/following-sibling::div/descendant::h5";

	public static String CampoCalculoFirma = "//input[@id='eai_resultado_firma']";

	// PANTALLA ADMINISTRACI�N USUARIOS
	public static String EntrarMiPerfil = "//span[contains(text(),'Mi perfil') or @class='kyop-long-name azul' or contains(@class,'icon-profile')]";

	public static String EntrarEditarPerfil = "//span[contains(text(),'Editar perfil')]";

	public static String EntrarPersonalizarCuentas = "//span[contains(text(),'Personalizar cuentas') or contains(text(),'Configurar servicios') or contains(text(),'Personalizar productos')]";
	
	public static String TablaUsuarios = "//table[@id='tablaDatosUsuario']";

	public static String TablaUsuariosNombres = "//table[@id='tablaDatosUsuario']/tbody/tr/td[@headers='col1']";

	public static String TablaUsuarios_CodigoPrimerUsuario = "//table[@id='tablaDatosUsuario']/tbody/tr[1]/td[2]";

	public static String checkDeLaTabla = "//label[contains(@for,'seleccionarUsuario')]";//"//input[contains(@id,'seleccionarUsuario')]";

	public static String TablaUsuarios_NombrePrimerUsuario = "//table[@id='tablaDatosUsuario']/tbody/tr[1]/td[3]";

	public static String TablaUsuarios_InputPrimerUsuario = "//table[@id='tablaDatosUsuario']/tbody/tr[1]/td[1]/label";

	// public static String TablaUsuarios_EstadoPrimerUsuario="//table[@id='tablaDatosUsuario']/tbody/tr[1]/td[5]";
	public static String TablaUsuarios_EstadoPrimerUsuario = "//table[@id='tablaDatosUsuario']/tbody/tr[1]/td[@headers='col4']";

	// public static String TablaUsuarios_FirmantePrimerUsuario="//table[@id='tablaDatosUsuario']/tbody/tr[1]/td[8]";
	public static String TablaUsuarios_FirmantePrimerUsuario = "//table[@id='tablaDatosUsuario']/tbody/tr[1]/td[@headers='col7']";

	public static String ColumnaCodigoUsuario = "//th[@id='columnaCodigo']";

	public static String ColumnaNombreUsuario = "//th[@id='columnaNombre']";

	public static String ColumnaTipoUsuario = "//th[@id='col3']";

	public static String ColumnaEstado = "//th[@id='col4']";

	public static String ColumnaPoderValidacion = "//th[@id='col5']";

	public static String ColumnaClaveFirma = "//th[@id='col6']";

	public static String ColumnaFirmante = "//th[@id='col7']";
	
	public static String columnaAdminUsuarios = "//th[@id='col10']";

	public static String LinkVerMasUsuarios = "//a[@id='verMasUsuarios']";

	public static String RadioSeleccionUsuarioFiltrado = "//label[contains(@for,'seleccionarUsuario')]";//"//input[contains(@id,'seleccionarUsuario')]";

	public static String radioSeleccionUsuarioValidador = "//label[contains(@for,'indiceUsuarioValidador')]";

	public static String CampoBusqueda = "//input[@id='entradaBuscar']";

	public static String BotonBuscar = "//button[@id='bBuscar']";

	public static String ResultadosBusqueda = "//div[@class='resultado right']";

	public static String BotonAccederFiltrar = "//*[contains(@src,'botonFiltrar.png') or contains(@class,'botonIconoFiltrar')]";

	// public static String BotonAccederFiltrar="//button[@value='filtrar']";
	public static String CheckFiltroEstadoTodos = "//label[@for='checkFiltroTodosEstado']";

	public static String CheckFiltroEstadoActivo = "//label[@for='checkActivo']";

	public static String CheckFiltroEstadoPdteActivar = "//label[@for='checkPendiente']";

	public static String CheckFiltroTipoUsuarioTodos = "//label[@for='checkFiltroTodosTipo']";

	public static String CheckFiltroTipoAdministrador = "//label[@for='checkAdministrador']";

	public static String BotonFiltrar = "//button[@id='bFiltrar']";

	public static String BotonAccederAltaUsuario = "//button[@id='bNuevoUsuario']";

	public static String BotonEliminar = "//button[@id='bEliminar']";

	public static String BotonEliminarGenerico = "//button[contains(@id,'bEliminar')]";

	public static String BotonEliminarConf = "//button[@id='bEliminarConf']";

	public static String BotonEliminarConfGenerico = "//button[contains(@id,'bEliminarConf')]";
	
	public static String BotonEliminarOrdenPendiente="//div[@id='kyfb_ventanaModalEliminarFP']/descendant::button[contains(text(),'Eliminar')]";

	public static String BotonBloquear = "//button[@id='bBloquear']";

	public static String BotonBloquearConf = "//button[@id='bBloquearConf']";

	public static String BotonDesbloquear = "//button[@id='bDesbloquear']";

	public static String BotonDesbloquearGenerico = "//button[contains(@id,'bDesbloquear')]";

	public static String BotonDesbloquearConf = "//button[@id='bDesbloquearConf']";

	public static String BotonConsultar = "//button[@id='bConsultar']";

	public static String BotonEditar = "//button[@id='bEditar']";

	public static String BotonEditarGenerico = "//button[contains(@id,'bEditar')]";

	public static String BotonDescargarPerfil = "//button[@id='bDescargarPerfil'and@value='Descarga el perfil del usuario']";

	public static String BotonDescargarPerfilConf = "//button[@id='bDescargarPerfil'and@value='']";

	public static String BotonDescargarPerfilConfGenerico = "//button[contains(@id,'bDescargarPerfil')]";

	// public static String BotonDescargarPDF="//button[contains(text(),'Descargar PDF')]";
	public static String BotonDescargarPDF = "//*[contains(@onclick,'descargarListaUsuario')][contains(@onclick,'pdf')]";

	public static String BotonDescargarPDFDispositivos = "//*[contains(@onclick,'exportarPDF')]";

	public static String pestaniaModificarPerfilado = "//a[@id='pestaniaModificarPerfilado']";

	public static String imagenAlerta = "//*[contains(@class,'CeldaVariasValidaciones')]";//"//*[contains(@src,'iconoAlertaTabla') or contains(@class,'icon-alert')][not(contains(@class,'interior'))]";

	public static String BotonDescargarPerfilExcel = "//button[@id='bDescargarPerfilExcel']";

	public static String BotonDescargarPerfilExcelConf = "//button[@id='bDescargarPerfilXLS']";

	public static String verCambiosPendientes = "//a[@id='consultarCambiosPendientes' or @id='consultarCambiosPendientesPerfilado']";

	public static String verCambiosPendientesDatosPersonales = "//a[contains(@href,'cargarCambiosPendientesValidar') or @id='consultarCambiosPendientesPerfilado']";

	public static String cerrarVentanaCambiosPendientes = "//*[@id='cerrarVentanaModalDP']";

	public static String datosPersonalesDesplegados = "//div[@id='cambiosDatosPersonalesDesplegada']";

	public static String serviciosSinCambios = "//p[contains(@class,'SinCambios')][contains(@style,'block')]";

	public static String serviciosConCambios = "//p[contains(@class,'ConCambios')][contains(@style,'block')]";

	public static String servicioNotChecked = "//input[contains(@id,'check')][not(@checked)]";

	public static String servicioChecked = "//input[contains(@id,'check')][@checked]/following-sibling::label";//"//input[contains(@id,'check')][@checked]";

	public static String servicioCheckedDisabled = "//input[contains(@class,'radioCheck')][@value='true']";


	public static String LinkValidarOperacion = "//b[(text()='Validar operaci�n')]";

	public static String LinkPendienteActivar = "//a[(text()='Pendiente de alta')]";

	public static String TextoNoHayUsuarios = "//span[(text()='No hay usuarios')]";

	// PANTALLA PERSONALIZAR CUENTAS
	public static String TextoAlias = "//span[@id='jqTextoAlias_1']";

	public static String TextoAliasGenerico = "//span[contains(@id,'jqTextoAlias')]";

	public static String IconoEditarAlias = "//*[contains(@class,'jqAccionLapizGris')]";

	public static String IconoEditarAlias1 = "//span[@id='jqTextoAlias_1']/following-sibling::*[contains(@class,'jqAccionLapizGris')]";

	public static String CampoAlias = "//input[@id='alias_1']";

	public static String BotonGuardarAlias = "//button[contains(@class,'jqAccionStickAceptar')]";

	public static String ImagenCuentaFavorita = "//*[contains(@class,'jqCambiarImagen')]";

	public static String Cuenta = "//td[@headers='cuenta']";

	public static String CampoAliasGenerico = "//input[contains(@id,'alias_')]";

	// PANTALLA EDITAR SERVICIOS PERFIL
	public static String PestanaServiciosFirmas = "//a[text()='Servicios y firmas']";

	// PANTALLA VALIDAR OPERACI�N
	public static String DivMensajeOperacionPendiente = "//div[contains(@class,' jqElementoDesplegable')]/descendant::p[@class]"; //"//div[starts-with(@class,'infoDesplegar')]";

	public static String LinkDesplegar = "//*[contains(@class,'icon-deMas') or @class='icon-aniadir']";

	public static String LinkDesplegarMas = "//*[contains(@src,'mas16x16_gris.png') or @class='icon-aniadir']";

	public static String LinkDesplegarMenos = "//*[@class='enlaceDespliegue' and @id='0' or @class='icon-menos']";

	public static String DivTextoDesplegado = "//div[contains(@class,'contenidoDetalleResumenValidacion')]/ancestor::div[contains(@class,'jqNodoPadre')]";//"//div[starts-with(@class,'contenidoDesplegado')]";

	public static String BotonValidar = "//button[@value='Validar']";

	public static String BotonValidarTareas = "//button[contains(@value,'Validar tareas')]";

	public static String LinkValidarOtroMomento = "//span[contains(text(),'Validar en otro momento')]";

	public static String DescargarTareaValidarAltaPDF = "//*[@id='bDescargarPDF']";

	public static String DescargarTareaValidarAltaXLS = "//*[@id='bDescargarXLS']";

	public static String MasDesplegar = LinkDesplegar;


	// PANTALLA ALTA USUARIO
	public static String TituloPantalla = "//h1[@class='jqRaTitulo']";

	public static String BotonOn = "//*[@alt='Enmascarar ON' or contains(@class,'cambioTextoSwitchOn')]";

	public static String DivEnmascarar = "//div[@id='bEnmascarar']/img";

	public static String CampoCodigoUsuario = "//input[@id='txCodUsuario']";

	public static String CampoNombreUsuario = "//input[@id='txDesUsuario']";

	public static String CampoMailUsuario = "//input[@id='txEmail']";

	public static String CampoMail2Usuario = "//input[@id='txEmail2']";

	public static String CampoPasswordUsuario = "//input[@id='txPassword']";

	public static String ComboTipoDocumentoUsuario = "//select[@id='txTipoDoc']";
	
	public static String ComboTipoDocumentoUsuarioDIV = "//div[@id='txTipoDoc_chosen']";

	public static String CampoDocumentoUsuario = "//input[@id='txNumDoc']";

	public static String ComboPais = "//select[@id='txNomPais']";
	
	public static String ComboPaisDIV = "//div[@id='txNomPais_chosen']";

	public static String CampoTelefonoUsuario = "//input[@id='txTelefono']";

	public static String CampoEstadoUsuario = "//input[@id='txEstadoUsuario']";

	public static String RadioTipoUsuarioAdmin = "//label[@for='tipoUsuarioAdmin']";//"//input[@id='tipoUsuarioAdmin']";

	public static String ComboTipoAdmin = "//select[@id='tipoPoder']";
	
	public static String ComboTipoAdminDIV = "//div[@id='tipoPoder_chosen']";

	public static String RadioFirmante = "//label[@for='checkFirmante']";//"//input[@id='checkFirmante']";

	public static String RadioNoFirmante = "//label[@for='checkNoFirmante']";

	public static String ComboTipoFirmante = "//select[@id='tipoFirma']";
	
	public static String ComboTipoFirmanteDIV = "//div[@id='tipoFirma_chosen']";

	public static String RadioDispositivoFisico = "//label[@for='tipoDispositivoFisico'][@class='inputAI']";

	public static String RadioDispositivoMovil = "//label[@for='tipoDispositivoMovil'][@class='inputAI']";

	public static String ComboDispositivo = "//select[@id='txDisp']";//"//div[@id='txDisp_chosen']";

	public static String ComboDispositivoLATAM = "//select[@id='dispositivo']";

	// Cambios Pendientes de Validar
	public static String divCambiosPendientes = "//div[@id='alertaCambiosPendientesEditarPerfil']";

	public static String linkVerCambiosPendientes = "//a[contains(@href,'cargarCambiosPendientesValidar')]";

	public static String ventanaCambiosPendientes = "//div[@id='VMConsultaCambiosPendientes']";

	public static String AceptarCustodia = "//label[@for='aceptaCustodia']";

	public static String AceptarCondiciones = "//label[@for='aceptaCond'][@class='inputAI']";

	public static String BotonAltaUsuario = "//button[@id='botonAlta']";

	public static String DivAltaSinToken = "//div[@id='altaSinToken'][contains(@style,'block')]";

	public static String BotonContinuar = "//button[@id='bContinuar']";

	public static String BotonContinuarModificar = "//button[@id='bContinuarModificar' or @id='bContinuarModificarUsuario']";

	public static String LabelOpcionesPerfilado = "//label[@for='opcionPerfilado']";

	public static String TextoResumenValidarUsuario = "//*[@id='form.errors']";

	// SMS
	public static String LinkSMS = "//a[@id='smsLink']";

	public static String MensajeSMSEnviadoOK = "//div[@id='alerta'][@class='alerta bordeOk']";

	// PANTALLA EDITAR PERFIL
	public static String CampoContrasenaUsuario = "//input[@id='txtAntPassword']";

	public static String CampoNuevaContrasenaUsuario = "//input[@id='txPassword']";

	public static String CampoRepetirNuevaContrasenaUsuario = "//input[@id='txtRepPassword']";

	public static String CampoClaveOperacionesUsuario = "//input[@id='txClaveOp']";

	public static String RadioClaveOperacionesUsuario = "//input[@name='tipoClaveOperaciones'][@value='O']/following-sibling::label[contains(@class,'input')]";

	public static String CampoNuevaClaveOperacionesUsuario = "//input[@name='claveOperaciones']";

	public static String CampoRepetirNuevaClaveOperacionesUsuario = "//input[@name='campo2']";

	// PANTALLA CONFIGURACI�N PERFIL
	public static String CheckCopiarPerfil = "//label[@for='copiarPerfil'][@class='inputAI']";

	public static String CheckTodoContratado = "//label[@for='todoContratado'][@class='inputAI']";

	public static String CheckConfigurarPerfil = "//label[@for='configurarDetalle'][@class='inputAI']";

	public static String BotonContinuarAzul = "//button[@id='botonContinuar']";

	public static String BotonGuardar = "//button[starts-with(text(),'Guardar')]";

	public static String botonGuardarAzul = "//button[contains(@onclick,'divConfGuardar')][contains(text(),'Guardar')]";

	public static String BotonPropagarServicios = "//button[@id='bPropagarAServicios']";

	public static String TituloTodoContratado = "//h2[contains(text(),'todo lo contratado')]";

	public static String ComboServiciosFirmas = "//select[@id='copiarServiciosFirmas' or @id='cb_copiarServiciosFirmas']";//"//div[@id='copiarServiciosFirmas_chosen' or @id='cb_copiarServiciosFirmas_chosen']";

	public static String ComboServiciosFirmasOpciones = "//select[@id='cb_copiarServiciosFirmas' or @id='copiarServiciosFirmas']/option";//"//div[@id='copiarServiciosFirmas_chosen' or @id='cb_copiarServiciosFirmas_chosen']/descendant::li";

	public static String ComboServiciosFirmasOpcionServicios = "//select[@id='copiarServiciosFirmas']/option[text()='Servicios']";

	public static String botonQuitarPermisos = "//button[contains(@onclick,'borrarConfig')]";

	// PANTALLA CONFIRMACION ALTA USUARIO
	public static String DivColumnaIzquierda = "//div[@class='dobleColumnaIzquierda']";

	public static String DivColumnaDerecha = "//div[@class='dobleColumnaDerecha']";

	public static String DivDesplegado = DivTextoDesplegado;//"//div[contains(@class,'contenidoDesplegado')]";

	// PANTALLA GESTI�N DISPOSITIVOS
	public static String ColumnaTipoDispositivo = "//th[@id='tipDispositivo']";

	public static String ColumnaNumeroDispositivo = "//th[@id='numDispositivo']";

	public static String ColumnaTelefonoDispositivo = "//th[@id='codTelefono']";

	public static String ColumnaEstadoDispositivo = "//th[@id='estado']";

	public static String ColumnaNombreUsuarioDispositivo = "//th[@id='desUsuario']";

	public static String ColumnaCodigoUsuarioDispositivo = "//th[@id='codUsuario']";

	public static String TablaDispositivos = "//table[@id='tablaDatosDispositivo']";

	public static String TablaDispositivos_TipoPrimerDispositivo = "//table[@id='tablaDatosDispositivo']/tbody/tr[1]/td[2]";

	public static String TablaDispositivos_NumeroPrimerDispositivo = "//table[@id='tablaDatosDispositivo']/tbody/tr[1]/td[3]";

	public static String TablaDispositivos_TelefonoPrimerDispositivo = "//table[@id='tablaDatosDispositivo']/tbody/tr[1]/td[4]";

	public static String TablaDispositivos_EstadoPrimerDispositivo = "//table[@id='tablaDatosDispositivo']/tbody/tr[1]/td[@headers='estado']";

	// public static String TablaDispositivos_CodigoUsuarioPrimerDispositivo="//table[@id='tablaDatosDispositivo']/tbody/tr[1]/td[6]";
	public static String TablaDispositivos_CodigoUsuarioPrimerDispositivo = "//table[@id='tablaDatosDispositivo']/tbody/tr[1]/td[@headers='codUsuario']";

	// public static String TablaDispositivos_EstadoPrimerDispositivo="//table[@id='tablaDatosDispositivo']/tbody/tr[1]/td[@headers='estado']";
	// public static String TablaDispositivos_SeleccionarPrimerDispositivo="//input[@id='rbFila1']";
	public static String TablaDispositivos_SeleccionarPrimerDispositivo = "//label[contains(@for,'rbFila1')]";

	public static String LinkVerMasDispositivos = "//a[@id='verMasDispositivos']";


	public static String BotonBajaDispositivo = "//button[@id='btnBajaSup']";

	public static String BotonBajaDispositivo2 = "//button[@id='btnBajaAccionSup']";

	public static String BotonDesasignarDispositivo = "//button[@id='btnDesasignarSup']";

	public static String BotonDesasignarDispositivo2 = "//button[@id='btnDesasignarAccionSup']";

	public static String BotonAsignarDispositivo = "//button[@id='btnAsignarSup']";

	public static String BotonAsignarDispositivo2 = "//button[@id='btnAsignarListadoSup']";

	public static String BotonBloquearDispositivo = "//button[@id='btnBloquearSup']";

	public static String BotonBloquearDispositivoGenerico = "//button[contains(@id,'btnBloquearSup')]";

	public static String BotonBloquearDispositivo2 = "//button[@id='btnBloquearAccionSup']";

	public static String BotonBloquearDispositivo2Generico = "//button[contains(@id,'btnBloquearAccionSup')]";

	public static String BotonDesbloquearDispositivo = "//button[@id='btnDesbloquearSup']";

	public static String BotonDesbloquearDispositivoGenerico = "//button[contains(@id,'btnDesbloquearSup')]";

	public static String BotonDesbloquearDispositivo2 = "//button[@id='btnDesbloquearAccionSup']";

	public static String BotonDesbloquearDispositivo2Generico = "//button[contains(@id,'btnDesbloquearAccionSup')]";

	public static String BotonBuscarDispositivo = "//button[@id='btnBuscar']";

	public static String CheckFiltroBajaSinAsignar = "//input[@id='checkBloqueadoHD']";

	public static String CheckFiltroTokenPlus = "//label[@for='filtroTipos1']";

	public static String CheckFiltroTokenSoftware = "//input[contains(@id,'filtroTipos')][@value='TS']";

	public static String CheckFiltroBaja = "//label[@for='checkPendiente']";

	public static String BotonSolicitarDispositivo = "//button[@id='bNuevoDispositivo']";

	public static String BotonBuscarUsuarioDispositivo = "//button[@id='MbBuscar']";

	public static String TablaUsuarioDispositivo_NombrePrimerUsuario = "//table[@id='tablaNumero']/tbody/tr[1]/td[2]";

	public static String TablaUsuarioDispositivo_SeleccionarPrimerUsuario = "//label[@for='indiceUsuario1']";//"//input[@id='indiceUsuario1']";

	public static String BotonGenerar = "//button[@id='btGenerar']";

	// ASIGNACION DISPOSITIVOS
	public static String CampoBuscarUsuariosSinDispositivo = "//input[@id='entradaBuscarUsuariosAsignacion']";

	public static String BotonBuscarUsuariosSinDispositivo = "//button[@id='btnBuscarUsuarioAsignacion']";

	public static String RadioSeleccionUsuarioAsignarDisp = "//label[contains(@for,'radioAsignarUsuario')]";


	// PANTALLA AUDITOR�A
	public static String LiteralAuditoria = "//span[text()='Auditor�a']";

	public static String FiltroFechaDesde = "//input[@id='lbFechaDesde']";

	public static String FiltroFechaHasta = "//input[@id='lbFechaHasta']";

	public static String BotonFiltrarAuditoria = "//button[@id='filtraLista']";

	public static String TablaAuditoriaPrimeraOperacion = "//table[@id='tablaDatosAuditoria']/tbody/tr[1]/td[4]";

	public static String TablaAuditoriaSegundaOperacion = "//table[@id='tablaDatosAuditoria']/tbody/tr[2]/td[4]";

	public static String TablaAuditoriaUsuarioPrimeraOperacion = "//table[@id='tablaDatosAuditoria']/tbody/tr[1]/td[3]";

	public static String TablaAuditoriaUsuarioSegundaOperacion = "//table[@id='tablaDatosAuditoria']/tbody/tr[2]/td[3]";

	public static String TablaAuditoriaCodigoUsuarioPrimeraOperacion = "//table[@id='tablaDatosAuditoria']/tbody/tr[1]/td[2]";

	public static String DivVentanaModal = "//div[@id='ventanaModal']";

	// ALTA DETALLADA
	public static String checkInformacionCuentas = "//*[contains(text(),'INFORMACI�N DE CUENTAS')]/parent::*/preceding-sibling::*/descendant::label[@class='inputAI']";

	public static String checkInformacionCuentasLiteral = "//*[contains(text(),'INFORMACI�N DE CUENTAS')]/parent::*/preceding-sibling::*/descendant::label[@class='inputAI']";

	public static String checkPagos = "//*[contains(text(),'PAGOS')]/parent::*/preceding-sibling::*/descendant::label[@class='inputAI']";

	public static String checkCobros = "//*[contains(text(),'COBROS')]/parent::*/preceding-sibling::*/descendant::label[@class='inputAI']";

	public static String checkFicheros = "//*[contains(text(),'FICHEROS')]/parent::*/preceding-sibling::*/descendant::label[@class='inputAI']";

	public static String checkFirmas = "//*[contains(text(),'FIRMAS')]/parent::*/preceding-sibling::*/descendant::label[@class='inputAI']";

	public static String enlaceConfigurarVisible = "//*[contains(@id,'enlaceEditar')][contains(@style,'block')]";

	public static String enlaceConfigurarInformacionCuentas = "//*[contains(text(),'INFORMACI�N DE CUENTAS')]/following::*[contains(@id,'enlaceEditar')][not(contains(@id,'Configurada'))][contains(@style,'block')]";

	public static String enlaceRevisarInformacionCuentas = "//*[contains(text(),'INFORMACI�N DE CUENTAS')]/ancestor::li/descendant::*[contains(@id,'enlaceEditar')][(contains(@id,'Configurada'))][contains(@style,'block')]";

	public static String enlaceConfigurarPagos = "//*[contains(text(),'PAGOS')]/following::*[contains(@id,'enlaceEditar')][not(contains(@id,'Configurada'))][contains(@style,'block')]";

	public static String enlaceConfigurarCobros = "//*[contains(text(),'COBROS')]/following::*[contains(@id,'enlaceEditar')][not(contains(@id,'Configurada'))][contains(@style,'block')]";

	public static String enlaceConfigurarFicheros = "//*[contains(text(),'FICHEROS')]/following::*[contains(@id,'enlaceEditar')][not(contains(@id,'Configurada'))][contains(@style,'block')]";

	public static String enlaceConfigurarFirmas = "//*[contains(text(),'FIRMAS')]/following::*[contains(@id,'enlaceEditar')][not(contains(@id,'Configurada'))][contains(@style,'block')]";

	public static String masGeneral = "//div[contains(@class,'cabeceraDesplegable')][contains(@style,'block')]/descendant::*[@id='mas_general']";//"//*[@id='mas_general']";

	public static String masEspecificoLiteral = "//*[contains(text(),'ESPEC�FICA')]/preceding::*[@id='mas_por_servicio']";

	// Si se confirma que s�lo hay masGeneral y masEspec�fico, utilizar este:
	public static String masEspecifico = "//div[contains(@class,'cabeceraDesplegable')][contains(@style,'block')]/descendant::*[@id='mas_por_servicio']";

	// public static String masHistorico = "//*[contains(text(),'Hist�rico')]/preceding::*[contains(@id,'mas_0')]";
	public static String masHistorico = "//*[contains(text(),'Hist�rico')]/parent::*/parent::*/descendant::*[contains(@id,'mas')]";

	public static String masHistoricoLiteral = "//*[contains(text(),'Hist�rico')]/parent::*/parent::*/descendant::*[contains(@id,'mas')]";

	// public static String masRecibos = "//*[contains(text(),'Recibos')]/preceding::*[contains(@id,'mas_0')]";
	public static String masRecibos = "//*[contains(text(),'Recibos')]/parent::*/parent::*/descendant::*[contains(@id,'mas')]";

	public static String masNominas = "//*[contains(text(),'N�minas')]/parent::*/parent::*/descendant::*[contains(@id,'mas')]";

	public static String accederATodasLasCuentasEspecifico = "//*[contains(text(),'Acceder a todas las cuentas')]/preceding-sibling::label[contains(@for,'Especifico')]";

	public static String accederATodasLasCuentasGenerico = "//*[contains(text(),'Acceder a todas las cuentas')]/preceding-sibling::label[@for='radioTodo']";

	public static String especificarPermisoCuentaEspecifico = "//*[contains(text(),'Especificar permisos sobre las cuentas')]/preceding-sibling::label[contains(@for,'Especifico')]";

	public static String especificarPermisoCuentaExcepciones = "//*[contains(text(),'Especificar permisos sobre las cuentas')]/preceding-sibling::label[contains(@for,'Excepciones')]";

	public static String accederYFirmarEnTodasLasCuentasEspecifico = "//*[contains(text(),'Acceder y firmar en todas las cuentas')]/preceding-sibling::label[contains(@for,'Especifico')]";

	public static String accederYFirmarEnTodasLasCuentasGenerico = "//*[contains(text(),'Acceder y firmar en todas las cuentas')]/preceding-sibling::label[@for='radioTodo']";

	public static String botonGuardar = "//button[text()='Guardar']";

	public static String comboPermisos = "//select[@id='tipoPermisoSelect_0_0']";

	public static String comboPermisosPagos = "//select[@id='tipoPermisoSelectAgrupacion']";

	public static String comboPoderFirma = "//select[@id='tipoPoderSelectServicio_0_0']";

	public static String comboPoderFirmaPagos = "//select[@id='tipoPoderSelectAgrupacion']";

	public static String inputLimiteFirma = "//input[@id='limiteFirmaGlobalInput_0_0']";

	public static String inputLimiteFirmaGenerica = "//input[@id='limiteFirmaGlobalInputGenerica']";

	public static String inputLimiteFirmaGeneralBueno = "//input[contains(@id,'limiteFirmaGlobalInput')]";

	public static String botonPropagarATodosLosServicios = "//button[@id='bPropagarAServicios']";

	public static String continuarAvisoImportante = "//button[contains(@id,'bConfirm')]";

	public static String limiteFirmaGeneral = "//input[@id='limiteGeneralFirma']";

	public static String limiteFirmaCuentas = "//input[contains(@id,'limiteListaAsuntosGenerica')]";

	public static String propagarLimiteFirma = "//button[@id='propagarLimiteFirma']";

	public static String comboTipoPoderGenerico = "//select[contains(@id,'tipoPoderSelect')]";

	public static String comboTipoPermisoGenerico = "//select[contains(@id,'tipoPermisoSelect')]";

	public static String spanConfigurado = "//span[text()='Configurado']";

	public static String revisarConfiguracion = "//a[contains(@id,'enlaceEditarConfigurada')]";

	public static String tipoPermisoPadreGenerico = "//select[contains(@id,'tipoPermisoSelectTH')]";

	public static String tipoPermisoAgrupacionPadreGenerico = "//select[contains(@id,'tipoPermisoSelectAgrupacionTH')]";

	public static String radioExcepcionesEspecifico = "//label[contains(@for,'radioExcepcionesEspecifico')]";

	// Tabla de Cuentas
	public static String tablaDatosAgrupacion = "//table[@id='tablaDatosAgrupacion']";

	public static String tablaDatosAgrupacionGenerico = "//table[contains(@id,'tablaDatosAgrupacion')]";

	public static String tablaCuentasGlobales = "//table[@id='tablaDatosServicio_0_0']";

	public static String tablaCuentasGlobalesGenerico = "//table[contains(@id,'tablaDatosServicio')]";

	public static String primerRegistroTablaCuentasAgrupacion = "//input[@id='seleccionarCuentaGlobal_0']";

	public static String primerRegistroTablaCuentasGlobales = "//input[@id='seleccionarCuentaGlobal_0_0_0']";

	public static String primerRegistroSeleccionaCuentaGenerico = "//label[contains(@for,'seleccionarCuentaGlobal')]";

	public static String primerRegistroConComboDeFirma = "//select[contains(@id,'tipoPermisoSelect')]/ancestor::td[contains(@id,'tdcol')]/preceding-sibling::*/descendant::label[contains(@for,'seleccionarCuentaGlobal')]";

	public static String primerRegistroSeleccionaCuentaGenericoConCombo = "//label[contains(@for,'seleccionarCuentaGlobal')]";

	public static String RegistroSeleccionaCuentaNotDisabled = "//select[contains(@id,'tipoPermisoSelect')][not(@disabled)]";

	public static String primerComboTipoPermiso = "//select[@id='tipoPermisoSelectTD_0_0_1']";

	public static String combotipoPoderTH = "//select[@id='tipoPoderSelectServicioTH_0_0']";

	public static String checkMarcarTodosAgrupacion = "//input[contains(@onclick,'checkTodosAgrupacion')]/following-sibling::label";

	public static String tablaCuentasAlias = "//div[contains(@id,'aliasToolTip')]";

	public static String tablaCuentasSinAlias = "//td[@headers='col3']";

	// Literales varios
	public static String recuerdeAsignarDispositivo = "Recuerde que debe asignar un dispositivo";

	public static String esteCambioAfectaraAtodosLosPermisos = "Este cambio afectara a todos los permisos";

	public static String conExito = "con �xito";

	public static String Exito = "�xito";

	public static String PendienteDeAlta = "Pendiente de alta";

	public static String literalAdministrador = "Administrador";

	public static String literalUsuario = "Usuario";

	public static String MensajeServiciosFirmasAsignados = "Servicios y firmas asignados al usuario";

	public static String UnUsuarioTotales = "1 usuarios totales";

	public static String patronOperacionRealizadaCorrectamente = "Operaci�n realizada correctamente.*";

	public static String literalServicios = "servicios";

	public static String literalFirmas = "firmas";

	public static String literalServiciosYFirmas = "servicios y firmas";

	public static String haSidoEliminadoCorrectamente = "ha sido eliminado correctamente";

	public static String MensajePendienteDeBaja = "Pendiente de baja";

	public static String MensajePendienteBaja = "Pendiente baja";

	public static String PreAsignado = "Preasignado";

	public static String literalSinPoderes = "sin poderes";

	public static String literalValidacionOperacion = "Validacion de operaci�n";

	public static String literalBajaUsuario = "Baja Usuario";

	public static String literalDetallesSeleccion = "Detalle de la selecci�n";


	// Propios LATAM
	public static String comboTokens = "//select[@id='comboMFA']";

	public static String TokenHardwareLATAM = "//input[contains(@id,'TH')]";

	public static String TokenSoftwareLATAM = "//input[contains(@id,'TS')]";

	public static String comboDispositivoHardware = "//select[@id='dispositivo']";

	public static String comboDispositivoSoftware = "//select[@id='dispSoft']";

	public static String botonAsignarDispositivoLATAM = "//button[@id='btnAsignarSupCompass']";

	public static String comboSeleccionarToken = "//select[@id='selectD_1']";

	public static String botonAsignarLATAM = "//button[@id='btnAsignarCompassInf']";

	public static String checkAsignarToken = "//input[contains(@id,'checkAsignar')]";

	public static String dispositivosSinNadaPendiente = "//*[contains(@class,'SinOperacionPendiente')]/parent::*/parent::tr[contains(@id,'fila')]/descendant::input[contains(@id,'rbFila')]";

	public static String botonValidacionMultiple = "//button[@id='bValidarMultiple']";

	public static String checkFiltroPendienteValidar = "//label[@for='checkFiltroPendienteValidar']";

	public static String checkBloqueOrdenanteBeneficiario = "";

	public static String enlaceConfigurarBloqueOrdenanteBeneficiario = "";

	public static String masServicioOrdenanteBeneficiario = "";

	public static String comboIdiomasCompass = "//a[@id='lang_change_select']";

	public static String Login_BotonEntrarCompass = "//button[@type='submit']";

	// SALTO CMP
	public static String tokenLogin = "//input[@id='eai_tokenVasco']";

	public static String botonFinalizar = "//button[@id='btnAcceder']";

	public static String botonAdministracionCMP = "//button[@id='botonAdministrador' or contains(@value,'Administraci�n y control') or contains(@value,'Administraci�n')]";

	public static String botonSalto = "//*[@id='saltoplegado']";

	public static String comboIdiomaCMP = "//*[@id='idplegado']";

	public static String idiomaES_CMP = "//a[contains(@onclick,\"cambiarIdiomaUsuario('es')\")]";

	public static String idiomaActualCMP = "//span[@id='idiomaActual' or @id='kyop-current-language']";

	public static String SaltoAEspana = "//a[contains(@onclick,'saltaPais') or contains(@onclick,'countryJump')][contains(text(),'Espa�a')]";

	public static String SaltoAChile = "//a[contains(@onclick,'saltaPais') or contains(@onclick,'countryJump')][contains(text(),'Chile')]";

	public static String SaltoAGlobal = "//a[contains(@id,'countryjump')][contains(@onclick,'es_GL')]";

	public static String logoGNC = "//img[contains(@src,'logo_bbva-global-net-cash')]";

	// V5
	public static String FrameMenu = "//iframe[@id='kdpoMenu']";

	public static String AdministracionYControl = "//a[contains(@id,'cab1_Nivel_1_Opcion_1')][not(contains(@onclick,'quitaEstiloSegundoNivel'))]";

	public static String ImagenCargandoCMP = "//span[@id='loadBoxText']";

	public static String LiteralConsulta = "//a[contains(text(),'Consulta')][not(contains(text(),'Audit'))]";

	public static String tablaResultados = "//input[@id='btrUsuario']/ancestor::table";

	public static String LiteralAdministracionUsuariosV5 = "//a[contains(text(),'Administra')][contains(text(),'Usuarios')]";

	public static String BotonDesconectarV5 = "//a[contains(@onclick,'javascript:desconexion()') or contains(@href,'javascript:desconexion')]";

	public static String FramePrincipalV5 = "//iframe[@id='kdpoArea']";

	public static String BotonBuscarV5 = "//input[@name='BTBuscar']";

	// Beneficiarios
	public static String gestionBeneficiarios = "//a[contains(@onclick,'lanzarGestionBeneficiarios')]";

	public static String tituloGestionBeneficiarios = "//h1[contains(text(),'Gesti�n de beneficiarios')]";

	public static String botonNuevaRelacion = "//button[@id='bNuevaRelacion']";

	public static String tituloNuevaRelacionBeneficiarios = "//h1[contains(text(),'Nueva relaci�n ordenante beneficiario')]";

	public static String CuentasOrdenantes = "//*[contains(text(),'Ordenante')]/ancestor::td[contains(@id,'tdcol')]/preceding-sibling::*/descendant::input[contains(@id,'seleccionarCuentaGlobal')]";

	public static String CuentasBeneficiarias = "//*[contains(text(),'Beneficiario')]/ancestor::td[contains(@id,'tdcol')]/preceding-sibling::*/descendant::input[contains(@id,'seleccionarCuentaGlobal')]";

	public static String SeleccionRelacionOrdenante = "//*[contains(@id,'seleccioRelacion')][contains(@class,'Ordenante')]";

	public static String SeleccionRelacionBeneficiario = "//*[contains(@id,'seleccioRelacion')][contains(@class,'Beneficiario')]";

	public static String LimiteFirmaRelacion = "//input[@id='limiteF']";

	public static String LimiteoperacionRelacion = "//input[@id='limiteOp']";

	public static String tablaGestionBeneficiarios = "//table[@id='tablaDatosGestionBeneficiarios']";

	public static String SeleccionRelacion = "//*[contains(@id,'seleccionRelacion')][contains(@class,'radioGestion')]";

	public static String SeleccionRelacionGenerico = "//*[contains(@class,'radioGestion')]";

	public static String botonEditarRelacion = "//button[@id='bConfigurar']";

	public static String ModificarLimiteFirmaRelacion = "//input[@id='mLimiteFirma']";

	public static String ModificarLimiteoperacionRelacion = "//input[@id='mLimiteOperacion']";

	public static String tituloEditarRelacionBeneficiarios = "//h1[contains(text(),'Editar relaci�n')]";

	// INSTALACI�N - Escenia

	// Login
	public static String userEscenia = "//input[@name='username']";

	public static String passEscenia = "//input[@name='password']";

	public static String botonAceptarLoginEscenia = "//button[@id='accept' or 'aceptar']";//"//input[@name='aceptar']";

	public static String Flechitas = "//*[@id='botonDespMenus']";

	public static String ListaDesplegable = "//div[@id='listaDesplegable']";

	public static String Sistemas = "//td[@id='ESIST']";

	public static String SalirEscenia = "//span[@id='multiidiomaSalir']";

	public static String GestionDeTokens = "//div[@id='hm_m_main1_I2']";

	public static String GestionDeSolicitudes = "//div[@id='hm_m_Gesti�n de Tokens_I1']";

	public static String CambioEstado = "//div[@id='hm_m_Gesti�n de Tokens_I2']";

	public static String InputNumeroToken = "//input[@id='numserie']";

	public static String campoFechaDesde = "//input[@name='fecha_desde']";

	public static String campoFechaHasta = "//input[@name='fecha_hasta']";

	public static String comboEstadoEscenia = "//select[@id='cbo_Estado']";

	public static String botonBuscarEscenia = "//input[@name='buscar']";

	public static String botonGuardarEscenia = "//input[@name='guardar']";

	public static String campoTokenEscenia = "//input[contains(@id,'numSerie')]";

	public static String comboTokenEscenia = "//select[contains(@id,'tipo_token')]";

	public static String comboTokenEscenia2 = "//select[contains(@name,'tipo_token')]";

	public static String checkRecordarDatos = "//*[@id='checkRecData' or @for='checkRecData']";

	public static String changeUserLabel = "//a[@id='changeLabel']";

	public static String userRecorded = "//*[@id='cod_usu_Rec']";

	public static String companyRecorded = "//*[@id='cod_emp_Rec']";


	// Contrataci�n
	public static String TituloContratacion = "//*[contains(text(),'contrataci�n SSTT')]";

	public static String desconectarSST = "//a[contains(@onclick,'desconectar')]";

	public static String comboTipoAcceso = "//select[@id='tipo-acceso']";

	public static String referenciaExterna = "//input[@id='ref-externa']";

	public static String oficina = "//input[@id='oficina']";

	public static String folio = "//input[@id='folio']";

	public static String botonAccederSST = "//button[contains(@class,'btnAceptar')]";

	public static String ImagenCargandoSST = "//img[contains(@src,'ico-modal-cargando')]";

	public static String TituloContratarProducto = "//h2[contains(text(),'Contratar Producto')]";

	public static String cuentaCargo1 = "//input[@id='cuentaActual1']";

	public static String cuentaCargo2 = "//input[@id='cuentaActual2']";

	public static String cuentaCargo3 = "//input[@id='cuentaActual3']";

	public static String cuentaCargo4 = "//input[@id='cuentaActual4']";

	public static String validarCuenta = "//button[contains(@onclick,'validarCuentaAsociada')]";

	public static String titularCuentaActual = "//span[@id='cuentaActual-titular']";

	public static String comisionApertura = "//input[@id='comisionApertura']";

	public static String comisionFijaMensual = "//input[@id='comisionMensual']";

	public static String comboLiquidacion = "//select[@id='periodicidadLiquidacion']";

	public static String AdministracionAvanzada = "//input[@id='tipoAdminAvanzada']";

	public static String tipoTokenMovil = "//input[@id='tipoTokenMovil']";

	public static String tipoTokenFisico = "//input[@id='tipoTokenFisico']";

	public static String numeroMovil = "//input[@id='num1']";

	public static String nombreEmpresaSST = "//input[@id='nombreEmpresa']";

	public static String nombreContactoSST = "//input[@id='personaContacto']";

	public static String comboActivacionServicios = "//select[@id='activacionServicios']";

	public static String alertaBordeOK = "//div[contains(@class,'alerta bordeOk')]";

	public static String alertaBordeOK2 = "//div[contains(@class,'bordeOk')]";

	public static String ContratarTodasCuentas = "//button[@id='btnSeleccionarCuentasAsuntos']";

	public static String ventanaModalSST = "//div[contains(@class,'ventanaModal')]";

	public static String nuevaPalabraPaso = "//input[@id='nuevaPassword']";

	public static String aceptarSST = "//button[contains(@class,'botonAceptar')]";

	public static String tituloGestionSolicitudToken = "//h2[contains(text(),'Gesti�n de solicitud de tokens')]";

	public static String fechaSolicitudSST = "//input[@id='fechaSolicitud']";

	public static String horaSolicitudSST = "//input[@id='horaSolicitud']";

	public static String TituloCancelarProducto = "//h2[contains(text(),'Cancelar producto')]";

	public static String tokenEsceniaINT = "2829840072";

	public static String cerrarVentanaModal = "//*[contains(@class,'ftr botonCerrar')]";


	public static String ACtivarServicioTokenMovil = "//input[@id='activarServicioTokensMoviles']";

	public static String siToolTip = "//button[@id='siTooltip']";

	public static String comboPaisTelefono = "//select[@id='extPais']";

	public static String telefonoSMS = "//input[@id='telefonoSMS']";

	public static String idiomaSMS = "//select[@id='idiomaSMS']";

	public static String inputBuscarServicio = "//input[contains(@class,'servicioBuscado')]";

	public static String cerrarBusquedaServicio = "//button[contains(@onclick,'javascript:Desplegable.cerrarBusqueda')]";

	public static String labelBanco = "//label[contains(text(),'Banco')]/following-sibling::input";

	public static String labelOficina = "//label[contains(text(),'Oficina')]/following-sibling::input";

	public static String labelCon = "//label[contains(text(),'Con')]/following-sibling::input";

	public static String labelFolio = "//label[contains(text(),'Folio')]/following-sibling::input";

	public static String labelCh = "//label[contains(text(),'Ch')]/following-sibling::input";

	public static String validarUnSoloServicio = "//button[contains(@class,'btnValidar')][contains(@class,'UnSoloServicio')]";

	public static String asuntoContratacion = "//select[contains(@id,'asuntos-origen')]/option";
	
	public static String botonMover = "//button[@class='mover']";
	
	// Activaci�n
	public static String TituloActivacion = "//*[contains(text(),'Activaci�n de BBVA')]";

	public static String checkTodoContratado = "//input[@id='copiarPerfilTodoLoContratado']";

	public static String checkTransparenciaCanales = "//input[@id='transparenciaCanales']";

	public static String checkAceptaCustod = "//input[@id='aceptaCustodia']";

	public static String checkAceptaCond = "//input[@id='aceptaCond']";

	public static String botonContinuarActivacion = "//button[@id='btContinuarTooltip']";

	public static String AltaCorrecta = "//p[contains(text(),'dado de alta')]";

	public static String botonIrAlPortal = "//button[@id='irAlPortalID']";

	public static String ActivarTokenOtroMomento = "//a[@id='botonContinuar']";

	public static String TituloActivacionToken = "//*[contains(text(),'Asignar dispositivo de seguridad')]";

	public static String CampoToken00 = "//input[@name='numeroSerieAcuse[0][0]']";

	public static String CampoToken01 = "//input[@name='numeroSerieAcuse[0][1]']";

	public static String CampoToken02 = "//input[@name='numeroSerieAcuse[0][2]']";

	public static String AceptaCondicionesToken = "//input[@id='idAceptaCondiciones']";

	public static String botonContinuar2 = "//button[@id='btContinuar2']";

	public static String ContinuarTooltip = "//button[@id='siTooltip']";
	
	public static String submitApproveAccess = "//button[@id='submit_approve_access']";

	// Continuar Campa�a
	public static String continuarCampania = "//*[@class='continuarCampania' or @class='botonAceptar' or @id='botonContinuar' or contains(@ng-click,'closeCampaign') or contains(text(),'Continuar')  or contains(@on-action,'closeCampaign')]";
	
	public static String botonContinuarRTSDisabled = "button[ng-click*='onAction'][disabled='true']";
	
	// COMPASS
	public static String CompassUsuario = "//input[@id='eai_user']";

	public static String CompassOldPass = "//input[@id='eai_password']";

	public static String CompassNewPass = "//input[@id='eai_newpassword']";

	public static String CompassConfirmPass = "//input[@id='eai_confpassword']";

	public static String CompassConfirmar = "//button[@id='dr_grabar']";

	public static String inputUsuarioUsuario = "//label[@for='tipoUsuarioUsuario']";

	public static String CompassClaveOpe = "//input[@id='eai_claveop']";

	public static String CompassConfirmarClaveOpe = "//input[@id='eai_confclaveop']";

	public static String BotonCancelarMessageDialog = "//div[contains(@id,'messagebox')]/descendant::button[contains(@onclick,'executeCallbackCancel()')]";

	public static String inputDispositivoConLista = "//select[contains(@id,'selectD')]/ancestor::tr/descendant-or-self::input[contains(@id,'checkAsignar')]";

	// GAPS
	public static String botonReiniciarClaveDeFirma = "//button[@id='bReiniciarClave']";

	public static String botonResetClaveFirma = "//button[@id='bReiniciarClaveConf']";

	// IDIOMAS
	public static String botonEntrarIngles = "//button[contains(@class,'grandote') or contains(@type,'submit')][contains(text(),'Enter')]";

	public static String botonEntrarEspanol = "//button[contains(@class,'grandote') or contains(@type,'submit')][contains(text(),'Entrar')]";

	public static String CatalogoDeServiciosES = "//a[contains(text(),'Cat�logo de servicios')]";

	public static String CatalogoDeServiciosEN = "//a[contains(text(),'Services catalogue')]";

	// DNS
	// public static String dnsCloud = "http://clouditarvi.ddns.net";
	public static String dnsCloud = "http://146.148.15.138";
	public static String dnsAzure = "https://reportsautotesting.z16.web.core.windows.net";
	
	public static String urlSubidaAzure = "https://app-resultsauto.northeurope.cloudapp.azure.com/api-qa/uploadFile";


	// Usuario Password Cloud Itarvi
	public static String usuarioCloud = "selenium";

	public static String passwordCloud = "qwerty99";
	
	public static String usuarioAzure = "selenium";

	public static String passwordAzure = "S0fttek@2018";

	// Usuario Password Escenia
	public static String usuarioEscenia = "xe03497";

	public static String passwordEscenia = "itarvi25";

	// KYOS
	public static String ImagenCargandoKYOS = "//*[contains(@src,'loader_vermas.gif') or contains(text(),'espere') or contains(@src,'common/images/loadingBbva.gif')]";
	
	//Firmas
	public static String firmasCampoResultadoFormula ="//input[@id='kyfb_claveOperacionesFPF']";
	
	public static String firmasCampoDispositivoSeguridad = "//input[@id='kyfb_dispositivoSeguridadFPF' or @id='kyfb_tokenValue' or @id='kyfb_otpSmsValue']";
	
	public static String firmasBotonContinuarFirma = "//button[@id='kyfb_botonContinuarFirma']";
	
	public static String firmasLinkSMS = "//a[contains(@onclick,'kyfb_enviarSMS()')]";
	
	public static String firmasLinkDispositivoFisico = "//*[contains(text(),'Firmar con') or contains(text(),'Introduzca en su token m�vil')]";
	//public static String firmasLinkDispositivoFisico = "//a[contains(text(),'Introduzca en su token m�vil')]";
	
	public static String firmasErrorCrendenciales = "//div[@id='kyfb_alertasFirmarPdteError']//span";

	public static String ImagenCargandoFirmas = "//img[contains(@src,'firmas/img/loader_tap_tabla_lightbox.gif')]";
	
	public static String botonAtrasAdmUsuarios = "//a[@id='enlaceAdminUsuarios']";

	public static String contenidoPestanaVisible = "//div[contains(@class,'contenidoPestana visible')]";

	//Nueva ayuda
	public static String nuevaAyuda = "//div[contains(@class,'enjoyhint')]";
	
	public static String botonSiguienteAyuda = "//div[contains(@class,'bbvaNext')][contains(@class,'enjoyhint')]";

	public static String botonCerrarEnjoyHint = "//div[@class='enjoyhint_close_btn']";
	
	//Input KYTA
	public static String inputrecuadroSubirfichero = "//input[@ngf-select][@ng-model='files']";
	
	public static String inputrecuadroSubirficheroCSS ="body > input[type=file]";
	
	public static String comboGenericoSubidaFichero = "//div[@class='ui-selector']";
	
	//ARQ INFORMACIONAL
	public static String xpathArqInformacional = "//span[contains(@class,'icon-profile')][not(contains(text(),'Mi perfil'))]";
	
	public static String logoCabeceraDegradadoAntiguo = "//img[contains(@src,'barraDegradadaCabecera')]";
	
	public static String hideLightBox = "//div[contains(@onclick,'hideLightBox')]";
	
	public static String inputBuscadorGenerico = "//ui-search/descendant::input";
	
	//NUEVA POLITICA DATOS USUARIOS
	
	public static String BotonContinuarModificarUsuario = "//button[@id='bContinuarModificarUsuario']";

	public static String iframeTratamientoDatosUsuario = "//iframe[(@id='kygu-lightbox-iframe-modal-div-usuario')]";

	public static String tituloTratamientoDatosUsuario = "//*[contains(text(),'POL�TICA DE PROTECCI�N DE DATOS DE USUARIOS')]";

	public static String botonAceptarTratamientoDatosUsuario ="//button[(@id='aceptarPolitica')]";

	public static String botonCancelarTratamientoDatosUsuario ="//button[(@id='rechazarPolitica')]";
	
	public static String bodyTratamientoDatosPersonales =  "//body/descendant::div[(@id='wrapper')]";
	
	//Confirming
	public static String botonNuevaOperacion = "//*[@principal-name='Nueva operaci�n']/descendant::button";

	public static String ficheroTablaPendientes = "//table-pending/descendant::a[contains(@ng-click,'file')]";

	public static String buscarTablaPendientes = "//table-pending/descendant::input[contains(@placeholder,'Buscar')]";

	public static String inputsTablaPendientes = "//table-pending/descendant::td[contains(@class,'uiCheck')]";

	public static String tablaPendientes = "//table[@id='table_pending']";

	public static String mostrarMasTablaPendientes = "//table-pending/descendant::a[contains(@class,'show-more')]";

	public static String loadingShowMore = "//div[contains(@class,'loading-show-more')]";

	public static String botonFiltrosPending = "//table-pending/descendant::button[contains(@uib-popover-template,'filter')]";

	public static String botonFiltrosContracts = "//table-contracts/descendant::button[contains(@uib-popover-template,'filter')]";

	public static String nuevoConfirmingManual = "//a[@id='creation']";
	
	public static String tituloNuevoConfirming = "//*[contains(text(),'Nueva remesa de confirming')]";
	
	public static String nombreRemesa = "//*[@id='name']";
	
	public static String nombreRemesaEditar = "//*[@id='name']/descendant::input";
	
	public static String selectorDocumentos = "//ui-selector[contains(@data,'documents')]";
	
	public static String selectorContratos = "//ui-selector[contains(@data,'contracts')]";
	
	public static String botonContinuarConfirming = "//button[contains(@ng-click,'newRemittance')]";
	
	public static String iconoAgendaConfirming = "//ui-agenda[contains(@ng-if,'Agenda')]/descendant::a";
	
	public static String buscadorAgendaConfirming = "//ui-search/descendant::input[@placeholder]"; //"//ui-search/descendant::input[@placeholder='Buscar']";
	
	public static String inputAgendaConfirming = "//input[contains(@ng-model,'contact.inputCheck')]/ancestor::td";
	
	public static String literalResultadosAgendaConf = "//span[contains(@class,'table-head-label')]/descendant::b[contains(text(),'mostrado')]";//"//*[contains(@ng-bind-html,'getContactsLabel')]/descendant::b";
	
	public static String importeConfirming = "//input[@id='amount']";
	
	public static String codigoFactura = "//input[@id='billCode']";
	
	public static String fechaFactura = "//input[@name='billDate']";
	
	public static String fechaPago = "//input[@name='payDate']";
	
	public static String guardarYContinuar = "//button[contains(@ng-click,'addOrder')][not(contains(@ng-if,'edit'))]";
	
	public static String filaOrdenResumenConf = "//tr[contains(@ng-form,'inlineOrderForm')]";
	
	public static String inputFilaOrdenesConf = "//input[contains(@ng-model,'order.active')]";
	
	public static String filaOrdenesConf = "//tr[@ng-form='inlineOrderForm']";
	
	public static String cerrarVentanaModalConfirming = "//a[contains(@ng-click,'closeModal')]";
	
	public static String botonEliminarOrdenConf = "//button[contains(@class,'btnEliminar')]";
	
	public static String inputImporteInLine = "//input[@id='import']";
	
	public static String confimarEdicionInLine = "//a[contains(@class,'confirmar')]";
	
	public static String botonDejarPdteFirma = "//button[contains(@ng-click,'finalizarFichero')]";
	
	public static String hideMessage = "//a[contains(@ng-click,'hideMessage')]";
	
	
	//PERFILADO PARA ADEUDOS
	
	public static String configurarCobros = "//label[contains(text(),'COBROS')]//parent::div//following-sibling::div//a[contains(@id,'enlaceEditarConfigurada')]";
	
	public static String desplegarServicioAdeudosCORE = "//span[text()='C19.14 PRESENTACIONES SEPA' or text()='Adeudos directos SEPA']/ancestor::div[contains(@class,'cabeceraDesplegable')]/descendant::*[contains(@id,'mas')]"; 
	
	public static String desplegarServicioAdeudosB2B = "//span[text()='Adeudos Directos C19.44']/ancestor::div[contains(@class,'cabeceraDesplegable')]/descendant::*[contains(@id,'mas')]";
	
	public static String desplegarServicioAdeudosCOREFin = "//span[text()='Adeudos financiados 19.14']/ancestor::div[contains(@class,'cabeceraDesplegable')]/descendant::*[contains(@id,'mas')]"; 
	
	public static String desplegarServicioAdeudosB2BFin = "//span[text()='Adeudos financiados 19.44']/ancestor::div[contains(@class,'cabeceraDesplegable')]/descendant::*[contains(@id,'mas')]";
	
	public static String asuntosServicios = "//table[contains(@id,'tablaDatosServicio')]//td[@headers='col3']";
	
	public static String checkAsuntoServicio = asuntosServicios + "/preceding-sibling::td//input[contains(@id,'seleccionarCuentaGlobal')]";
	
	public static String tituloNetCash = "//img[@title='BBVA net cash']";
	
	public static String tablaCuentas = "//table[@id='tablaCuentas']";
	
	public static String loadingAlternativo = "//div[@id='cargaContenedor' or @id='contenedorLoader']";
	
	public static String logoBBVA = "//span[@id='logoBBVAcontainer']";

	//AGENDA
	public static String botonAgendaVisible = "//*[contains(@class,'kyop-control-click')][contains(text(),'Agenda')][not(contains(@style,'none'))]";

	public static String nuevoContactoAgenda = "//div[contains(@ng-click,'redirectToNewCustomer')]/descendant::button";

	public static String iconoAgenda = "//div[@id='btnAgenda']/descendant::a";
	
	public static String iconoAgendaAdeudos = "//a[contains(@ng-if,'showAgenda') or contains(@ng-show,'AperturaAgenda')]";
	
	public static String tituloAgenda = "//h1[contains(text(),'Nuevo contacto')]";
	
	public static String nombreContacto = "//input[@name='name']";
	
	public static String inputAdeudosContacto = "//label[contains(text(),'Adeudos')]/descendant::span";
	
	public static String inputFavoritosContacto = "//label[contains(text(),'Favoritos')]/descendant::span";
	
	public static String inputConfirmingContacto = "//label[contains(text(),'Confirming')]/descendant::span";
	
	public static String inputTransferenciasContacto = "//label[contains(text(),'Transferencias')]/descendant::span";
	
	public static String numeroCuentaContacto = "//input[@name='account0.ibanAccount']";
	
	public static String otraCuentaContacto = "//input[@name='unknownAccount0']";		
	
	public static String otraCuenta = "//label[contains(@for,'tipoCuentaOtros')]";
	
	public static String codigoBIC = "//input[@name='account0.biccode']";
	
	public static String aliasCuenta = "//input[@name='account0.alias']";
	
	public static String botonContinuar = "//button[contains(@type,'submit')]";
	
	public static String comboTipoDocumento = "//ui-selector[contains(@data,'documentCodes')]";
	
	public static String numeroDocumento = "//input[@name='documentRecipient'][contains(@ng-model,'personalData')]";
	
	public static String telefono = "//input[@name='telephone']";
	
	public static String emailBeneficiario = "//input[@name='emailBeneficiario']";
	
	public static String fax = "//input[@name='fax']";

	public static String isEmpleadoNO = "//label[@for='isEmployeeRadioNo']/descendant::span";
	
	public static String isEmpleadoYES = "//label[@for='isEmployeeRadioYes']/descendant::span";
	
	public static String tipoBeneficiario = "//ui-selector[contains(@data,'documentTypes')]";
	
	public static String tipoDocumentoBeneficiario = "//ui-selector[contains(@data,'lastBeneficiaryCodes')]";
	
	public static String inputIDBeneficiario = "//input[@name='documentRecipient'][contains(@ng-model,'finalRecipient')]";
	
	public static String inputNombreBeneficiario = "//input[@name='lastBenName']";
	
	public static String pais = "//ui-selector[contains(@data,'countries')]";
	
	public static String provincia = "//input[@name='state']";
	
	public static String poblacion = "//input[@name='legalAddressCity']";
	
	public static String inputCP = "//input[@name='zipCode']";
	
	public static String domicilio = "//textarea[contains(@ng-model,'streetName')]";
	
	public static String domicilioFiscal = "//textarea[contains(@ng-model,'legalAddress.name')]";
	
	public static String filaCuenta = "//td[contains(@ng-if,'account')]/ancestor::tr";
	
	public static String buscadorAgenda = "//div[contains(@class,'simpleSearch')]/descendant::input";
	
	public static String literalResultadosAgenda = "//*[contains(@class,'table-head-label ng-binding') or contains(@translate,'agenda.PAGINACION_TOTAL')]/descendant::b";//"//*[contains(@translate,'agenda.PAGINACION_TOTAL')]/descendant::b";
	
	public static String inputAgenda = "//label[contains(@for,'beneficiary')]/descendant::span";
	
	public static String botonActivarDatosPersonales = "//button[(@id='bActivar')]";
	
	public static String tituloPedirCita = "//h1[contains(text(),'Pedir cita')]";
	
	public static String ventanaModalExcelCM = "//div[@uib-modal-window='modal-window']";

	public static String botonAceptarVentanaModalExcelCM = "//span[text()='Entendido']/parent::button";
	
	public static String ventanaMedidasSeguridadFrame = "//iframe[@id='kyop-campaigns-lightbox-iframe-div']";
	
	public static String continuarMedidasSeguridad = "//button[text()=' Continuar ']";
	
	
	//Login Google
	
	public static String inputMail = "//input[@type='email']";
	
	public static String botonSiguiente = "//div[@id='identifierNext']";
	
	public static String loginUserBBVA = "//input[@id='ksni_user']";
	
	public static String passUserBBVA = "//input[@id='ksni_password']";
	
	public static String botonLoginBBVA = "//button[@id='accept']";
	
	public static String continuarGoogleBBVA = "//div[@role='button']/descendant::span[contains(text(),'Continuar') or contains(text(),'ontinu') ]";

	public Constantes() throws Exception
	{

		perfilado();

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String pais = params.getValorPropiedad("pais").toLowerCase();

		if(Utils.estamosEnLatam())
		{

			ESTADO_ACTIVO = "Pendiente de activar";
			ESTADO_PDTEACTIVAR = "Pendiente de activar el alta";
			Login_Referencia = LoginLocal_Referencia;
			Login_Usuario = LoginLocal_Usuario;
			Login_Password = LoginLocal_Password;
			Login_BotonEntrar = LoginLocal_BotonEntrar;
			literalValidacionOperacion = "Validaci�n de baja";
			EntrarEditarPerfil = "//span[contains(text(),'Editar perfil') or contains(text(),'Edite su perfil')]";
			EntrarPersonalizarCuentas = "//span[contains(text(),'Personalizar cuentas') or contains(text(),'Personalizaci�n de cuentas')]";

			// TOREMOVE cuando se redirija bien el desconectar
			Utils.reintento = true;

			if(pais.equalsIgnoreCase("Compass"))
			{
				GestionDeDispositivos = "//span[contains(text(),'Gesti�n')][contains(text(),'Dispositivos')]";
				;
			}

		}

		String idioma = params.getValorPropiedad("idioma");

		if(pais.equalsIgnoreCase("Spain") && idioma.equals("EN"))
			cargarConstantesENGLISH();

		// if (idioma.equals("CAT")|| idioma.equals("PO") || (!pais.equalsIgnoreCase("Spain") && idioma.equals("ES"))) cargarConstantesMultiIdioma(); Que tendr� los mismos literales que ENGLISH pero gen�ricos
		// Si son solo dos idiomas mas, podr�a hacer los literales de cada uno

	}


	public void cargarConstantesMultiIdioma() throws Exception
	{
		LiteralAuditoria = "//li[contains(@onclick,'MenuController.launchProcessFromHeaderButton')][contains(@onclick,'7877778614')]";
	}


	public void cargarConstantesENGLISH() throws Exception
	{
		BotonAdministradorVisible = "//button[contains(@id,'kyop-header-button')][contains(@value,'Administration')][not(contains(@style,'none'))]";
		LiteralAdministracionUsuarios = "//li/span[text()='User administration']";
		recuerdeAsignarDispositivo = "Remember to assign a safety device";
		esteCambioAfectaraAtodosLosPermisos = "This change will affect all";
		conExito = "Successfully";
		LinkValidarOtroMomento = "//span[contains(text(),'Validate later')]";
		literalAdministrador = "Administrator";
		MensajeServiciosFirmasAsignados = "Operation Completed Successfully";
		UnUsuarioTotales = "1 total users";
		patronOperacionRealizadaCorrectamente = "Operation Completed Successfully*";
		literalServicios = "services";
		literalFirmas = "firms";
		Exito = "Successfully";
		literalServiciosYFirmas = "configured the profile";
		TituloTodoContratado = "//h2[contains(text(),'everything contracted')]";
		haSidoEliminadoCorrectamente = "has been successfully deleted";
		LinkValidarOperacion = "//b[(text()='Validate operation')]";
		MensajePendienteDeBaja = "Low slope";
		TextoNoHayUsuarios = "//span[(text()='No users')]";
		LiteralAuditoria = "//span[text()='Audit']";
		;
		literalUsuario = "User";
		literalSinPoderes = "without powers";
		literalValidacionOperacion = "Validate operation";
		literalBajaUsuario = "Delete user";
		literalDetallesSeleccion = "Details of the selection";

		// Estados USUARIO
		ESTADO_ACTIVO = "Active";
		ESTADO_BLOQ = "Blocked";
		ESTADO_DESBLOQ = "Unblocked";
		ESTADO_PDTEACTIVAR = "Awating activation";

		// Estados dispositivo
		ESTADO_DISP_ACTIVO = "Active";
		ESTADO_DISP_PDTEACTIVAR = "Pending activation";
		ESTADO_DISP_DISPONIBLE = "Available";
		ESTADO_DISP_BLOQUEADO = "Blocked";
		ESTADO_DISP_DESBLOQUEADO = "Unblocked";
	}


	public void perfilado() throws Exception
	{
		checkInformacionCuentas = "//*[contains(text(),'" + Utils.devuelveBloqueServicioGenerico("BloqueServicioSinFirma").toUpperCase() + "') or contains(text(),'"
				+ Utils.devuelveBloqueServicioGenerico("BloqueServicioSinFirma") + "')]/parent::*/preceding-sibling::*/descendant::label[@class='inputAI']";
		checkPagos = "//*[contains(text(),'" + Utils.devuelveBloqueServicioGenerico("BloqueNServiciosConFirma").toUpperCase() + "') or contains(text(),'"
				+ Utils.devuelveBloqueServicioGenerico("BloqueNServiciosConFirma") + "')]/parent::*/preceding-sibling::*/descendant::label[@class='inputAI']";
		checkCobros = "//*[contains(text(),'" + Utils.devuelveBloqueServicioGenerico("BloqueServicioConFirma").toUpperCase() + "') or contains(text(),'"
				+ Utils.devuelveBloqueServicioGenerico("BloqueServicioConFirma") + "')]/parent::*/preceding-sibling::*/descendant::label[@class='inputAI']";

		enlaceConfigurarInformacionCuentas = "//*[contains(text(),'" + Utils.devuelveBloqueServicioGenerico("BloqueServicioSinFirma").toUpperCase() + "') or contains(text(),'"
				+ Utils.devuelveBloqueServicioGenerico("BloqueServicioSinFirma") + "')]/following::*[contains(@id,'enlaceEditar')][not(contains(@id,'Configurada'))][contains(@style,'block')]";
		enlaceConfigurarPagos = "//*[contains(text(),'" + Utils.devuelveBloqueServicioGenerico("BloqueNServiciosConFirma").toUpperCase() + "') or contains(text(),'"
				+ Utils.devuelveBloqueServicioGenerico("BloqueNServiciosConFirma") + "')]/following::*[contains(@id,'enlaceEditar')][not(contains(@id,'Configurada'))][contains(@style,'block')]";
		enlaceConfigurarCobros = "//*[contains(text(),'" + Utils.devuelveBloqueServicioGenerico("BloqueServicioConFirma").toUpperCase() + "') or contains(text(),'"
				+ Utils.devuelveBloqueServicioGenerico("BloqueServicioConFirma") + "')]/following::*[contains(@id,'enlaceEditar')][not(contains(@id,'Configurada'))][contains(@style,'block')]";

		masHistorico = "//*[contains(text(),'" + Utils.devuelveBloqueServicioGenerico("ServicioSinFirma") + "') or contains(text(),'"
				+ Utils.devuelveBloqueServicioGenerico("ServicioSinFirma").toUpperCase() + "') or contains(text(),'" + Utils.devuelveBloqueServicioGenerico("ServicioSinFirma")
				+ "') or contains(text(),'" + Utils.devuelveBloqueServicioGenerico("ServicioSinFirma") + "')]/parent::*/parent::*/descendant::*[contains(@id,'mas')]";
		masRecibos = "//*[contains(text(),'" + Utils.devuelveBloqueServicioGenerico("ServicioConFirma") + "') or contains(text(),'"
				+ Utils.devuelveBloqueServicioGenerico("ServicioConFirma").toUpperCase() + "')]/parent::*/parent::*/descendant::*[contains(@id,'mas')]";

		if(Utils.estamosEnLatam())
		{
			checkBloqueOrdenanteBeneficiario = "//*[contains(text(),'" + Utils.devuelveBloqueServicioGenerico("BloqueOrdenanteBeneficiario").toUpperCase()
					+ "')]/parent::*/preceding-sibling::*/descendant::label[@class='inputAI']";
			enlaceConfigurarBloqueOrdenanteBeneficiario = "//*[contains(text(),'" + Utils.devuelveBloqueServicioGenerico("BloqueOrdenanteBeneficiario").toUpperCase()
					+ "')]/following::*[contains(@id,'enlaceEditar')][not(contains(@id,'Configurada'))][contains(@style,'block')]";
			masServicioOrdenanteBeneficiario = "//*[contains(text(),'" + Utils.devuelveBloqueServicioGenerico("ServicioOrdenanteBeneficiario") + "') or contains(text(),'"
					+ Utils.devuelveBloqueServicioGenerico("ServicioOrdenanteBeneficiario").toUpperCase() + "')]/parent::*/parent::*/descendant::*[contains(@id,'mas')]";

		}

	}
}
