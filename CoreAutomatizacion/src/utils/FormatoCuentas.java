package utils;


import java.util.ArrayList;
import java.util.List;



/**
 * Formato de cuentas: Convierte BOCF a IBAN
 */
public class FormatoCuentas
{

	/**
	 * Constante con el valor dos.
	 */
	private static final int TWO = 2;

	/**
	 * Constante con el valor seis.
	 */
	private static final int SIX = 6;

	/**
	 * Constante con el valor diez.
	 */
	private static final int TEN = 10;

	/**
	 * Constante con el valor once.
	 */
	private static final int ELEVEN = 11;

	/**
	 * Pesos del primer d�gito banco
	 */
	private static final int[] PESOSPRIMERDIGITOBANCO = {4, 8, 5, 10};

	/**
	 * Pesos del primer d�gito oficina
	 */
	private static final int[] PESOSPRIMERDIGITOOFICINA = {9, 7, 3, 6};

	/**
	 * Pesos del segundo d�gito cuenta
	 */
	private static final int[] PESOSSEGUNGODIGITOCUENTA = {1, 2, 4, 8, 5, 10, 9, 7, 3, 6};

	/**
	 * Secuencia inicial
	 */
	private static final int[] SECUENCIAINICIAL = {1, 3, 9, 7, 1, 3, 9, 7, 1, 3, 9, 7};

	/**
	 * Cadena de ceros.
	 */
	private static final String ZEROES = "00";

	/**
	 * N�mero de paso
	 */
	private static final int STEP = 18;

	/**
	 * Divisor
	 */
	private static final long DIVISOR = 97L;

	/**
	 * Resto
	 */
	private static final long SUBSTRACT = 98L;

	/**
	 * ES
	 */
	private static final String ES = "ES";

	/** The log. */


	/**
	 * Convierte un bocf a IBAN
	 * 
	 * @param String bocf 19 posiciones 4 banco, 4 oficina, 4 contrapartida, 7 folio
	 * @return String con el IBAN
	 */
	public String convertirBocfToIban(final String bocf)
	{
		final String bocfAux = bocf;
		try
		{
			final Integer banco = Integer.valueOf(bocfAux.substring(0, 4));
			final Integer oficina = Integer.valueOf(bocfAux.substring(4, 8));
			final Integer contrapartida = Integer.valueOf(bocfAux.substring(8, 12));
			final Integer folio = Integer.valueOf(bocfAux.substring(12, 19));

			return bocfToIban(String.format("%04d", banco), String.format("%04d", oficina), String.format("%03d", contrapartida), String.format("%06d", folio));

		}
		catch(final Exception e)
		{
			Log.write("Error convirtiendo BOC a IBAN");
			return bocfAux;
		}


	}


	/**
	 * Calcula el �ltimo d�gito de control para una oficina, contrapartida y
	 * folio
	 * 
	 * @param oficina
	 * @param contrapartida
	 * @param folio
	 * @return d�gito de control
	 */
	private String calcularUltimoDigitoControl(final String oficina, final String contrapartida, final String folio)
	{

		int digito = 0;
		final String stAux = oficina + contrapartida.substring(contrapartida.length() - TWO) + folio.substring(folio.length() - SIX);

		for(int i = 0; i < SECUENCIAINICIAL.length; i++)
		{
			digito += Integer.valueOf(String.valueOf(stAux.charAt(i))) * SECUENCIAINICIAL[i];
		}

		digito %= TEN;
		return String.valueOf(digito);
	}


	/**
	 * Calula los d�gitos de control de un CCC
	 * 
	 * @param banco
	 *            banco del CCC
	 * @param oficina
	 *            oficinal del CCC
	 * @param cuenta
	 *            cuenta del CCC
	 * @return D�gito de control para el banco, oficinal, cuenta aportados
	 */
	private String calcularDigitosControl(final String banco, final String oficina, final String cuenta)
	{

		final StringBuilder result = new StringBuilder();
		int primerDigito = 0;
		int segundoDigito = 0;

		for(int i = 0; i < PESOSPRIMERDIGITOBANCO.length; i++)
		{
			primerDigito += Integer.valueOf(String.valueOf(banco.charAt(i))) * PESOSPRIMERDIGITOBANCO[i];
		}

		for(int i = 0; i < PESOSPRIMERDIGITOOFICINA.length; i++)
		{
			primerDigito += Integer.valueOf(String.valueOf(oficina.charAt(i))) * PESOSPRIMERDIGITOOFICINA[i];
		}
		primerDigito %= ELEVEN;
		primerDigito = ELEVEN - primerDigito;

		if(primerDigito == TEN)
		{
			primerDigito = 1;
		}
		else if(primerDigito == ELEVEN)
		{
			primerDigito = 0;
		}

		for(int i = 0; i < PESOSSEGUNGODIGITOCUENTA.length; i++)
		{
			segundoDigito += Integer.valueOf(String.valueOf(cuenta.charAt(i))) * PESOSSEGUNGODIGITOCUENTA[i];
		}

		segundoDigito %= ELEVEN;
		segundoDigito = ELEVEN - segundoDigito;
		if(segundoDigito == TEN)
		{
			segundoDigito = 1;
		}
		else if(segundoDigito == ELEVEN)
		{
			segundoDigito = 0;
		}

		result.append(primerDigito);
		result.append(segundoDigito);
		return result.toString();
	}


	/**
	 * Nos da la cuenta en formato IBAN partiendo de un bocf(banco/oficina/contrapartida/folio).
	 * 
	 * @param banco
	 * @param oficina
	 * @param contrapartida
	 * @param folio
	 * @return String Cuenta Iban
	 */
	private String bocfToIban(final String banco, final String oficina, final String contrapartida, final String folio)
	{

		final String ccc = bocfToCCC(banco, oficina, contrapartida, folio, null);

		return ES + calculateControlDigit(ES, ccc) + ccc;

	}


	public String ibanToBocf(final String iban)
	{
		// Eliminamos los tres primeros digitos
		String bancoOficinaDCCuenta = iban.substring(4, iban.length());

		// Se elimina el digito control - posiciones 9 y 10

		final String firstSplit = bancoOficinaDCCuenta.substring(0, 8);
		final String secondSplit = bancoOficinaDCCuenta.substring(10, bancoOficinaDCCuenta.length());

		bancoOficinaDCCuenta = firstSplit.concat(secondSplit);

		final String bocf = bancoOficinaDCCuenta.substring(0, bancoOficinaDCCuenta.length() - 1);

		return bocf;
	}
	
	public String ibanToBocf2(final String iban)
	{
		// Eliminamos los cuatro primeros digitos
		String bancoOficinaDCCuenta = iban.substring(4);

		// Se elimina el digito control y la contraPartida y el folio se completan con ceros
		
		String banco = bancoOficinaDCCuenta.substring(0, 4);
		String oficina = bancoOficinaDCCuenta.substring(4, 8);
		//String digitoControl = bancoOficinaDCCuenta.substring(8,10);
		String contraPartida = bancoOficinaDCCuenta.substring(10,13);
		String folio = bancoOficinaDCCuenta.substring(13, 19);
		
		final String bocf = banco + oficina + "0" + contraPartida + "0" + folio;

		return bocf;
	}


//	public String obtenerListaIbanSeparadaPorComas()
//	{
//		String listaIban = null;
//
//		try
//		{
//			final List<Object> listaAsuntos = asuntoUsuarioDAO.obtenerListaAsuntos();
//
//			if(listaAsuntos != null)
//			{
//				for(int i = 0; i < listaAsuntos.size(); i++)
//				{
//					listaIban = listaIban.concat(listaAsuntos.get(i) + ",");
//				}
//			}
//
//		}
//
//		catch(final Exception e)
//		{
//			Log.write("Error al construir la lista de iban separada por comas");
//			return null;
//		}
//
//		return listaIban;
//	}


	public List<String> listaIbanToBocf(final String listaIban)
	{
		final List<String> listaBocf = new ArrayList<String>();

		final String[] listaIbanArray = listaIban.split(",");

		for(int i = 0; i < listaIbanArray.length; i++)
		{
			listaBocf.add(ibanToBocf(listaIbanArray[i]));
		}

		return listaBocf;
	}


	/**
	 * Convierte un asunto de formato bocf a CCC
	 * 
	 * @param banco
	 * @param oficina
	 * @param contrapartida
	 * @param folio
	 * @param separador
	 * @return String CCC
	 */
	private String bocfToCCC(final String banco, final String oficina, final String contrapartida, final String folio, String separador)
	{

		if(separador == null)
		{
			separador = "";
		}

		final String lastControlDigit = calcularUltimoDigitoControl(oficina, contrapartida, folio);

		final String cuenta = contrapartida + folio + lastControlDigit;

		final String controlDigit = calcularDigitosControl(banco, oficina, cuenta);

		final String ccc = banco + separador + oficina + separador + controlDigit + separador + cuenta;

		return ccc;
	}


	/**
	 * Nos da el D�gito de control de un IBAN
	 * 
	 * @param bban
	 * @return long
	 */
	private long getIBANControlDigit(final String bban)
	{
		Long result;
		if(bban.length() <= STEP)
		{
			result = Long.parseLong(bban) % DIVISOR;

		}
		else
		{
			result = getIBANControlDigit(Long.parseLong(bban.substring(0, STEP)) % DIVISOR + bban.substring(STEP));
		}
		return result;
	}


	/**
	 * Nos da el digito de control
	 * 
	 * @param country
	 * @param bban
	 * @return String
	 */
	private String calculateControlDigit(final String country, final String bban)
	{
		final String internalAccount = bban + country + ZEROES;
		final StringBuilder buf = new StringBuilder();
		for(int i = 0; i < internalAccount.length(); i++)
		{
			final char character = Character.toUpperCase(internalAccount.charAt(i));
			buf.append(asInt(character));

		}
		final Long control = SUBSTRACT - getIBANControlDigit(buf.toString());
		return String.format("%02d", control);
	}


	private static String asInt(final char c)
	{
		return String.format("%s", Character.getNumericValue(Character.toUpperCase(c)));
	}


}
