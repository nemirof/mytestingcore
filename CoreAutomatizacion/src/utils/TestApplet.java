package utils;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;


public class TestApplet
{

	List<String> fileList;


	// private static final String OUTPUT_ZIP_FILE = "C:\\resultados.zip";
	
	public String SubirEjecucionAzure(String OUTPUT_ZIP_FILE) throws IOException, Exception
	{
		
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		// Establecemos el proxy
		Utils.setBBVAProxy();
		
		String UUAAs = Utils.devuelveUUAAsEjecutadas().trim();

		AppZip appZip = new AppZip();

		// Cogemos datos para crear el nombre de la carpeta
		String SOURCE_FOLDER = "";
		String Pais = Utils.devuelveDatosDeEjecucion("Pa");
		String Entorno = Utils.devuelveDatosDeEjecucion("Entorno");
		String pais;
		try
		{
			pais = Pais.substring(0, 3).toUpperCase();
		}
		catch(Exception e1)
		{
			pais = "SPA";
		}
		String entorno;

		// Entorno QA solo tiene 2 letras
		try
		{
			entorno = Entorno.substring(0, 3).toUpperCase();
		}
		catch(Exception e)
		{
			entorno = Entorno.substring(0, 2).toUpperCase();
		}

		String fecha = Utils.GetTodayDateAndTimeSFTP();

		String nombreCarpetaCloud = params.getValorPropiedad("nombreCarpetaCloud");
		
		if (nombreCarpetaCloud!=null && !nombreCarpetaCloud.equalsIgnoreCase(""))
			pais = nombreCarpetaCloud;
			
		String nombreCarpeta = pais + entorno + "_" + fecha;

		// Creamos la carpeta para guardar la movida
		boolean success = (new File(nombreCarpeta + "/" + nombreCarpeta)).mkdirs();

		if(!success)
			throw new Exception("No se pudo crear la carpeta para hacer el zip");


		AppZip.copyFolder(new File(new File("results/").getAbsolutePath()), new File(nombreCarpeta + "/" + nombreCarpeta + "/results/"));
		
		//Si estamos en adeudos subo tambi�n los ficheros
		if (UUAAs.contains("Adeudos")){
			String rutaFicheros = params.getValorPropiedad("rutaficheros");
			
			if (rutaFicheros!=null) {
				File dest = new File(nombreCarpeta+"/"+nombreCarpeta+ "/" +rutaFicheros + "/");
				dest.mkdirs();
				//AppZip.copyFolder(new File(new File("resources/FicherosActualizados/").getAbsolutePath()), new File(nombreCarpeta + "/" + nombreCarpeta + "/resources/FicherosActualizados/"));
				AppZip.copyFolder(new File(new File(rutaFicheros).getAbsolutePath()), new File(nombreCarpeta+"/"+nombreCarpeta+ "/" +rutaFicheros + "/"));
			}
		}
		AppZip.copyFolder(new File(new File("screenshots/").getAbsolutePath()), new File(nombreCarpeta + "/" + nombreCarpeta + "/screenshots/"));

		SOURCE_FOLDER = new File(nombreCarpeta).getAbsolutePath();

		appZip.generateFileList(new File(SOURCE_FOLDER), SOURCE_FOLDER);
		appZip.zipIt(OUTPUT_ZIP_FILE, SOURCE_FOLDER);
		System.out.println("Comprimiendo...");

		// Borramos la carpeta local porque ya tenemos el zip
		FileUtils.deleteDirectory((new File(nombreCarpeta)));

		// Subimos el ZIP
		TestApplet peticion = new TestApplet();

		String Fichero = OUTPUT_ZIP_FILE;
		//peticion.multipartRequest("http://146.148.15.138/servletFileUpload/UploadDownloadFileServlet", "", Fichero, Fichero);
//		peticion.multipartRequestAzure("https://app-resultsauto.northeurope.cloudapp.azure.com/api/uploadFile", "", Fichero, Fichero);
		
		peticion.multipartRequestAzure(Constantes.urlSubidaAzure, "", Fichero, Fichero);

		// Borramos el zip local porque ya est� subido
		new File(OUTPUT_ZIP_FILE).delete();

		return nombreCarpeta;

	}


	private void ignoreCertificates() {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkClientTrusted(final X509Certificate[] certs, final String authType) {
			}

			@Override
			public void checkServerTrusted(final X509Certificate[] certs, final String authType) {
			}
		} };

		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static class DefaultTrustManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }


	public String getTokenAzure () throws Exception {
		
		
		ignoreCertificates();
				
		HttpsURLConnection connection = null;
		
		String contentType = "application/json";
		
		String uri = "https://app-resultsauto.northeurope.cloudapp.azure.com/api-qa/login";
		
		Proxy proxy = null;
		
		// Establecemos el proxy
		if (Utils.insideBBVA()) {
			proxy = setProxy();
		}
		
		URL url = new URL(uri);
		
		if (proxy==null)
			connection = (HttpsURLConnection) url.openConnection();
		else
			connection = (HttpsURLConnection) url.openConnection(proxy);
        
        
		connection.setRequestMethod("POST");		
		connection.setRequestProperty("Connection", "Keep-Alive");
        
        if (!contentType.equals(""))
        	connection.setRequestProperty("Content-Type", contentType);

        connection.setDoInput(true);
        connection.setDoOutput(true);
        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
       
        writer.write("{\"user\": \"selenium\",\"password\": \"S0fttek@2018\"}");
        writer.flush();

        String line;
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        String respuesta = "";
        
        while ((line = reader.readLine()) != null) {
        	respuesta = respuesta + line;
        }

        JSONObject tokenJSon = new JSONObject(respuesta);
        String token = (String) tokenJSon.get("token");
        
        
        writer.close();
        reader.close();         

       
        int status = connection.getResponseCode();
        
        if (status!=200)
        	return "";
   
        connection.disconnect();
        
        return token;
	}
	
	public Proxy setProxy () {
		
		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(Constantes.proxy_hostname, Constantes.proxy_port));
		
		Authenticator authenticator = new Authenticator() {

	        public PasswordAuthentication getPasswordAuthentication() {
	            return (new PasswordAuthentication(Constantes.proxy_user, Utils.devuelveClaveCifrada().toCharArray()));
	        }
	    };
	    Authenticator.setDefault(authenticator);
	    
	    return proxy;
	}

	public String multipartRequestAzure(String urlTo, String post, String filepath, String filefield) throws Exception, IOException
	{
		
		ignoreCertificates();
		
		System.out.println("Subiendo a Azure...");
		
		HttpsURLConnection connection = null;
		DataOutputStream outputStream = null;
		InputStream inputStream = null;

		String twoHyphens = "--";
		String boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
		String lineEnd = "\r\n";

		String result = "";

		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;

		String[] q = filepath.split("\\" + File.separator);
		int idx = q.length - 1;

		try
		{
			TestProperties params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();

			// NOTA: Subida manual usr:selenium pass:qwerty99
			
			Proxy proxy = null;
			
			// Establecemos el proxy
			if (Utils.insideBBVA())			
				proxy = setProxy();

			File file = new File(filepath);
			FileInputStream fileInputStream = new FileInputStream(file);

			URL url = new URL(urlTo);
			
			if (proxy==null)
				connection = (HttpsURLConnection) url.openConnection();
			else
				connection = (HttpsURLConnection) url.openConnection(proxy);
			
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);

			connection.setRequestMethod("POST");
			connection.setRequestProperty("Connection", "Keep-Alive");
//			connection.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
//			connection.setRequestProperty("Authorization", TestAuth.userNamePasswordBase64(Constantes.usuarioAzure, Constantes.passwordAzure));
			
			connection.setRequestProperty("Authorization", getTokenAzure());

//                 connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
			connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
//			connection.setRequestProperty("Content-Type", "application/x-zip-compressed");

			outputStream = new DataOutputStream(connection.getOutputStream());
			outputStream.writeBytes(twoHyphens + boundary + lineEnd);
//			outputStream.writeBytes("Content-Disposition: form-data; name=\"" + filefield + "\"; fileName=\"" + q[idx] + "\"" + lineEnd);
			outputStream.writeBytes("Content-Disposition: form-data; name=\"" + "fileName" + "\"; filename=\"" + q[idx] + "\"" + lineEnd);
//                 outputStream.writeBytes("Content-Type: image/jpeg" + lineEnd);
			outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);
			outputStream.writeBytes(lineEnd);

			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			while(bytesRead > 0)
			{
				outputStream.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			outputStream.writeBytes(lineEnd);

			// Upload POST Data
//                 String[] posts = post.split("&");
//                 int max = posts.length;
//                 for(int i=0; i<max;i++) {
//                         outputStream.writeBytes(twoHyphens + boundary + lineEnd);
//                         String[] kv = posts[i].split("=");
//                         outputStream.writeBytes("Content-Disposition: form-data; name=\"" + kv[0] + "\"" + lineEnd);
//                         outputStream.writeBytes("Content-Type: text/plain"+lineEnd);
//                         outputStream.writeBytes(lineEnd);
//                         outputStream.writeBytes(kv[1]);
//                         outputStream.writeBytes(lineEnd);
//                 }

			outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			inputStream = connection.getInputStream();
			result = this.convertStreamToString(inputStream);

			fileInputStream.close();
			inputStream.close();
			outputStream.flush();
			outputStream.close();

			return result;
		

		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "error";
		}
	}


	public String SubirEjecucionGoogleCloud(String OUTPUT_ZIP_FILE) throws IOException, Exception
	{
		
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		// NOTA: Subida manual usr:selenium pass:qwerty99

		// Establecemos el proxy
		Utils.setBBVAProxy();
		
		String UUAAs = Utils.devuelveUUAAsEjecutadas().trim();

		AppZip appZip = new AppZip();

		// Cogemos datos para crear el nombre de la carpeta
		String SOURCE_FOLDER = "";
		String Pais = Utils.devuelveDatosDeEjecucion("Pa");
		String Entorno = Utils.devuelveDatosDeEjecucion("Entorno");
		String pais;
		try
		{
			pais = Pais.substring(0, 3).toUpperCase();
		}
		catch(Exception e1)
		{
			pais = "SPA";
		}
		String entorno;

		// Entorno QA solo tiene 2 letras
		try
		{
			entorno = Entorno.substring(0, 3).toUpperCase();
		}
		catch(Exception e)
		{
			entorno = Entorno.substring(0, 2).toUpperCase();
		}

		String fecha = Utils.GetTodayDateAndTimeSFTP();

		String nombreCarpetaCloud = params.getValorPropiedad("nombreCarpetaCloud");
		
		if (nombreCarpetaCloud!=null && !nombreCarpetaCloud.equalsIgnoreCase(""))
			pais = nombreCarpetaCloud;
			
		String nombreCarpeta = pais + entorno + "_" + fecha;

		// Creamos la carpeta para guardar la movida
		boolean success = (new File(nombreCarpeta + "/" + nombreCarpeta)).mkdirs();

		if(!success)
			throw new Exception("No se pudo crear la carpeta para hacer el zip");


		AppZip.copyFolder(new File(new File("results/").getAbsolutePath()), new File(nombreCarpeta + "/" + nombreCarpeta + "/results/"));
		
		//Si estamos en adeudos subo tambi�n los ficheros
		if (UUAAs.contains("Adeudos")){
			String rutaFicheros = params.getValorPropiedad("rutaficheros");
			
			if (rutaFicheros!=null) {
				File dest = new File(nombreCarpeta+"/"+nombreCarpeta+ "/" +rutaFicheros + "/");
				dest.mkdirs();
				//AppZip.copyFolder(new File(new File("resources/FicherosActualizados/").getAbsolutePath()), new File(nombreCarpeta + "/" + nombreCarpeta + "/resources/FicherosActualizados/"));
				AppZip.copyFolder(new File(new File(rutaFicheros).getAbsolutePath()), new File(nombreCarpeta+"/"+nombreCarpeta+ "/" +rutaFicheros + "/"));
			}
		}
		AppZip.copyFolder(new File(new File("screenshots/").getAbsolutePath()), new File(nombreCarpeta + "/" + nombreCarpeta + "/screenshots/"));

		SOURCE_FOLDER = new File(nombreCarpeta).getAbsolutePath();

		appZip.generateFileList(new File(SOURCE_FOLDER), SOURCE_FOLDER);
		appZip.zipIt(OUTPUT_ZIP_FILE, SOURCE_FOLDER);

		// Borramos la carpeta local porque ya tenemos el zip
		FileUtils.deleteDirectory((new File(nombreCarpeta)));

		// Subimos el ZIP
		TestApplet peticion = new TestApplet();

		String Fichero = OUTPUT_ZIP_FILE;
		peticion.multipartRequest("http://146.148.15.138/servletFileUpload/UploadDownloadFileServlet", "", Fichero, Fichero);

		// Borramos el zip local porque ya est� subido
		new File(OUTPUT_ZIP_FILE).delete();

		return nombreCarpeta;

	}


	public String SubirEjecucionGoogleCloudREST(String OUTPUT_ZIP_FILE) throws IOException, Exception
	{

		// Establecemos el proxy
		Utils.setBBVAProxy();

		AppZip appZip = new AppZip();

		// Cogemos datos para crear el nombre de la carpeta
		String SOURCE_FOLDER = "";
		String Entorno = UtilsRest.devuelveDatosDeEjecucion("Entorno");

		String entorno;
		try
		{
			entorno = Entorno.substring(0, 3).toUpperCase();
		}
		catch(Exception e)
		{
			entorno = Entorno.substring(0, 2).toUpperCase();
		}
		String fecha = Utils.GetTodayDateAndTimeSFTP();

		String nombreCarpeta = "REST_" + entorno + "_" + fecha;

		// Creamos la carpeta para guardar la movida
		boolean success = (new File(nombreCarpeta + "/" + nombreCarpeta)).mkdirs();

		if(!success)
			throw new Exception("No se pudo crear la carpeta para hacer el zip");

		AppZip.copyFolder(new File(new File("results/REST/").getAbsolutePath()), new File(nombreCarpeta + "/" + nombreCarpeta + "/results/"));
		AppZip.copyFolder(new File(new File("resources/").getAbsolutePath()), new File(nombreCarpeta + "/" + nombreCarpeta + "/resources/"));

		
		SOURCE_FOLDER = new File(nombreCarpeta).getAbsolutePath();

		appZip.generateFileList(new File(SOURCE_FOLDER), SOURCE_FOLDER);
		appZip.zipIt(OUTPUT_ZIP_FILE, SOURCE_FOLDER);

		// Borramos la carpeta local porque ya tenemos el zip
		FileUtils.deleteDirectory((new File(nombreCarpeta)));

		// Subimos el ZIP
		TestApplet peticion = new TestApplet();

		String Fichero = OUTPUT_ZIP_FILE;
		peticion.multipartRequest("http://146.148.15.138/servletFileUpload/UploadDownloadFileServlet", "", Fichero, Fichero);

		// Borramos el zip local porque ya est� subido
		new File(OUTPUT_ZIP_FILE).delete();

		return nombreCarpeta;

	}


	public String SubirVersionScripts(String OUTPUT_ZIP_FILE) throws IOException, Exception
	{

		// Establecemos el proxy
		Utils.setBBVAProxy();

		AppZip appZip = new AppZip();

		// Cogemos datos para crear el nombre de la carpeta
		String SOURCE_FOLDER = "";
		String nombreCarpeta = "Pruebas_Automaticas";

		// Creamos la carpeta para guardar la movida
		boolean success = (new File(nombreCarpeta + "/")).mkdirs();

		if(!success)
			throw new Exception("No se pudo crear la carpeta para hacer el zip");

		AppZip.copyFolder(new File(new File("dist/").getAbsolutePath()), new File(nombreCarpeta + "/"));

		SOURCE_FOLDER = new File(nombreCarpeta).getAbsolutePath();

		appZip.generateFileList(new File(SOURCE_FOLDER), SOURCE_FOLDER);
		appZip.zipItWithEmptyFolders(OUTPUT_ZIP_FILE, SOURCE_FOLDER);

		// Borramos la carpeta local porque ya tenemos el zip
		FileUtils.deleteDirectory((new File(nombreCarpeta)));

		// Subimos el ZIP
		TestApplet peticion = new TestApplet();

		String Fichero = OUTPUT_ZIP_FILE;
		peticion.multipartRequest("http://146.148.15.138/servletFileUpload/UploadDownloadFileServlet", "", Fichero, Fichero);

		// Borramos el zip local porque ya est� subido
		new File(OUTPUT_ZIP_FILE).delete();

		return nombreCarpeta;

	}


	public void SubirC204(String OUTPUT_ZIP_FILE) throws IOException, Exception
	{

		// Establecemos el proxy
		Utils.setBBVAProxy();

//          AppZip appZip = new AppZip();
//      	
//          //Cogemos datos para crear el nombre de la carpeta
//      	String SOURCE_FOLDER = ""; 			
//  		String nombreCarpeta = "Pruebas_Automaticas";
//      	
//      	//Creamos la carpeta para guardar la movida
//  		boolean success = (new File(nombreCarpeta+"/")).mkdirs();
//      	
//      	if (!success) throw new Exception("No se pudo crear la carpeta para hacer el zip");
//      	
//      	AppZip.copyFolder(new File(new File("dist/").getAbsolutePath()), new File(nombreCarpeta+"/"));
//      	
//      	SOURCE_FOLDER = new File(nombreCarpeta).getAbsolutePath();
//      	
//      	appZip.generateFileList(new File(SOURCE_FOLDER),SOURCE_FOLDER);
//      	appZip.zipItWithEmptyFolders(OUTPUT_ZIP_FILE,SOURCE_FOLDER);
//      	
//      	//Borramos la carpeta local porque ya tenemos el zip
//      	FileUtils.deleteDirectory((new File(nombreCarpeta)));

		// Subimos el ZIP
		TestApplet peticion = new TestApplet();

		String Fichero = OUTPUT_ZIP_FILE;
		peticion.multipartRequest("http://146.148.15.138/servletFileUpload/UploadDownloadFileServlet", "", Fichero, Fichero);


	}
	
	public void SubirC204Azure(String OUTPUT_ZIP_FILE) throws IOException, Exception
	{

		// Establecemos el proxy
		Utils.setBBVAProxy();

		// Subimos el ZIP
		TestApplet peticion = new TestApplet();

		String Fichero = OUTPUT_ZIP_FILE;
		peticion.multipartRequestAzure(Constantes.urlSubidaAzure, "", Fichero, Fichero);


	}

	public void SubirFicheroAzure(String OUTPUT_ZIP_FILE) throws IOException, Exception
	{

//		// Establecemos el proxy
//		Utils.setBBVAProxy();
//
		// Subimos el ZIP
		TestApplet peticion = new TestApplet();

		String Fichero = OUTPUT_ZIP_FILE;
		peticion.multipartRequestAzure(Constantes.urlSubidaAzure, "", Fichero, Fichero);


	}
	
	
	public String multipartRequest(String urlTo, String post, String filepath, String filefield) throws Exception, IOException
	{
		HttpURLConnection connection = null;
		DataOutputStream outputStream = null;
		InputStream inputStream = null;

		String twoHyphens = "--";
		String boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
		String lineEnd = "\r\n";

		String result = "";

		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;

		String[] q = filepath.split("\\" + File.separator);
		int idx = q.length - 1;

		try
		{
			File file = new File(filepath);
			FileInputStream fileInputStream = new FileInputStream(file);

			URL url = new URL(urlTo);
			connection = (HttpURLConnection)url.openConnection();

			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);

			connection.setRequestMethod("POST");
			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Authorization", TestAuth.userNamePasswordBase64(Constantes.usuarioCloud, Constantes.passwordCloud));
//                 connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
			connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

			outputStream = new DataOutputStream(connection.getOutputStream());
			outputStream.writeBytes(twoHyphens + boundary + lineEnd);
			outputStream.writeBytes("Content-Disposition: form-data; name=\"" + filefield + "\"; filename=\"" + q[idx] + "\"" + lineEnd);
//                 outputStream.writeBytes("Content-Type: image/jpeg" + lineEnd);
			outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);
			outputStream.writeBytes(lineEnd);

			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			while(bytesRead > 0)
			{
				outputStream.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			}

			outputStream.writeBytes(lineEnd);

			// Upload POST Data
//                 String[] posts = post.split("&");
//                 int max = posts.length;
//                 for(int i=0; i<max;i++) {
//                         outputStream.writeBytes(twoHyphens + boundary + lineEnd);
//                         String[] kv = posts[i].split("=");
//                         outputStream.writeBytes("Content-Disposition: form-data; name=\"" + kv[0] + "\"" + lineEnd);
//                         outputStream.writeBytes("Content-Type: text/plain"+lineEnd);
//                         outputStream.writeBytes(lineEnd);
//                         outputStream.writeBytes(kv[1]);
//                         outputStream.writeBytes(lineEnd);
//                 }

			outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			inputStream = connection.getInputStream();
			result = this.convertStreamToString(inputStream);

			fileInputStream.close();
			inputStream.close();
			outputStream.flush();
			outputStream.close();

			return result;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "error";
		}
	}


	private String convertStreamToString(InputStream is)
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try
		{
			while((line = reader.readLine()) != null)
			{
				sb.append(line);
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				is.close();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
	
	public String SubidaBasicaGoogleCloud (String OUTPUT_ZIP_FILE, String literal) throws IOException, Exception{
		  
		TestProperties params = new TestProperties(UtilsRest.getProperties());
		params.cargarPropiedades();
		String entorno = params.getValorPropiedad("entorno").substring(0, 3);

		// Establecemos el proxy
		Utils.setBBVAProxy();

		AppZip appZip = new AppZip();

		// Cogemos datos para crear el nombre de la carpeta
		String SOURCE_FOLDER = "";
		String pais = literal;

		String fecha = Utils.GetTodayDateAndTimeSFTP();

		String nombreCarpeta = pais + entorno + "_" + fecha;

		// Creamos la carpeta para guardar la movida
		boolean success = (new File(nombreCarpeta + "/" + nombreCarpeta)).mkdirs();

		if(!success)
			throw new Exception("No se pudo crear la carpeta para hacer el zip");

		AppZip.copyFolder(new File(new File("results/").getAbsolutePath()), new File(nombreCarpeta + "/" + nombreCarpeta + "/results/"));
		// AppZip.copyFolder(new File(new File("resources/").getAbsolutePath()), new File(nombreCarpeta+"/"+nombreCarpeta+"/resources/"));
     	AppZip.copyFolder(new File(new File("screenshots/").getAbsolutePath()), new File(nombreCarpeta+"/"+nombreCarpeta+"/screenshots/"));
     	AppZip.copyFolder(new File(new File("downloads/").getAbsolutePath()), new File(nombreCarpeta+"/"+nombreCarpeta+"/downloads/"));

		SOURCE_FOLDER = new File(nombreCarpeta).getAbsolutePath();

		appZip.generateFileList(new File(SOURCE_FOLDER), SOURCE_FOLDER);
		appZip.zipIt(OUTPUT_ZIP_FILE, SOURCE_FOLDER);

		// Borramos la carpeta local porque ya tenemos el zip
		FileUtils.deleteDirectory((new File(nombreCarpeta)));

		// Subimos el ZIP
		TestApplet peticion = new TestApplet();

		String Fichero = OUTPUT_ZIP_FILE;
		peticion.multipartRequest("http://146.148.15.138/servletFileUpload/UploadDownloadFileServlet", "", Fichero, Fichero);

		// Borramos el zip local porque ya est� subido
		new File(OUTPUT_ZIP_FILE).delete();

		return nombreCarpeta;
     	
      }
	
	public String SubirReporteEstabilidadGoogleCloud (String OUTPUT_ZIP_FILE) throws IOException, Exception{
		  
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String entorno = params.getValorPropiedad("entorno").substring(0, 3);

		// Establecemos el proxy
		Utils.setBBVAProxy();

		AppZip appZip = new AppZip();

		// Cogemos datos para crear el nombre de la carpeta
		String SOURCE_FOLDER = "";
		String pais = "DIS";

		String fecha = Utils.GetTodayDateAndTimeSFTP();

		String nombreCarpeta = pais + entorno + "_" + fecha;

		// Creamos la carpeta para guardar la movida
		boolean success = (new File(nombreCarpeta + "/" + nombreCarpeta)).mkdirs();

		if(!success)
			throw new Exception("No se pudo crear la carpeta para hacer el zip");

		AppZip.copyFolder(new File(new File("results/").getAbsolutePath()), new File(nombreCarpeta + "/" + nombreCarpeta + "/results/"));
		// AppZip.copyFolder(new File(new File("resources/").getAbsolutePath()), new File(nombreCarpeta+"/"+nombreCarpeta+"/resources/"));
     	AppZip.copyFolder(new File(new File("screenshots/").getAbsolutePath()), new File(nombreCarpeta+"/"+nombreCarpeta+"/screenshots/"));

		SOURCE_FOLDER = new File(nombreCarpeta).getAbsolutePath();

		appZip.generateFileList(new File(SOURCE_FOLDER), SOURCE_FOLDER);
		appZip.zipIt(OUTPUT_ZIP_FILE, SOURCE_FOLDER);

		// Borramos la carpeta local porque ya tenemos el zip
		FileUtils.deleteDirectory((new File(nombreCarpeta)));

		// Subimos el ZIP
		TestApplet peticion = new TestApplet();

		String Fichero = OUTPUT_ZIP_FILE;
		peticion.multipartRequest("http://146.148.15.138/servletFileUpload/UploadDownloadFileServlet", "", Fichero, Fichero);

		// Borramos el zip local porque ya est� subido
		new File(OUTPUT_ZIP_FILE).delete();

		return nombreCarpeta;
     	
      }

}
