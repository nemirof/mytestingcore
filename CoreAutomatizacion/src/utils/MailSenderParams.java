package utils;


import junit.framework.TestCase;


public class MailSenderParams extends TestCase
{

	@Override
	public void setUp() throws Exception
	{

	}


	public void testEnviaMail() throws Exception
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String ficheroHTML = params.getValorPropiedad("archivoMail");

		if (ficheroHTML==null || ficheroHTML.equalsIgnoreCase("")){
			ficheroHTML = "";
			
		}
		
		else {
			
			String dns = "http://146.148.15.138";
			
			TestApplet test = new TestApplet();
			String nombreCarpeta = test.SubidaBasicaGoogleCloud("resultados.zip","DPL_KYNF_");

			ficheroHTML = dns + "/testing/" + nombreCarpeta + "/" + ficheroHTML;

		}
		
		Mail.enviaMailBasico("Resumen Despliegue Automático", "Hola, os adjuntamos los resultados de la ejecución automática.", ficheroHTML);
	}


	@Override
	public void tearDown() throws Exception
	{

	}
}
