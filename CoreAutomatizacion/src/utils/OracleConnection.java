package utils;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.SQLException;


public class OracleConnection
{

	public static Connection ORAconnect()
	{
		String jdbcClassName = "oracle.jdbc.driver.OracleDriver";
		String url = "jdbc:oracle:thin:@oraei_nacar01b:1568/BKDPO001";
		String user = "xakdpo1i";
		String password = "gmessdd0";

		Connection connection = null;
		try
		{
			// Load class into memory
			Class.forName(jdbcClassName);
			// Establish connection
			connection = DriverManager.getConnection(url, user, password);

		}
		catch(ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			System.out.println(e.toString());
		}
		finally
		{
			if(connection != null)
			{
				System.out.println("Conexi�n correcta con ORACLE.");
//                try {
//                    connection.close();
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
			}
		}
		return connection;

	}


	public static String select(String query, String nombreColumna) throws SQLException
	{
		Connection connection = ORAconnect();

		Statement st = null;
		String valor = "";

		try
		{
			st = connection.createStatement();
			ResultSet rs = st.executeQuery(query);

			while(rs.next())
			{
				valor = rs.getString(nombreColumna);

			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(st != null)
			{
				st.close();
			}

			if(connection != null)
			{
				connection.close();
			}
		}

		System.out.println("Se ejecuta correctamente la query");
		return valor;

	}


	public static boolean update(String query) throws SQLException
	{
		Connection connection = ORAconnect();

		Statement st = null;
		boolean resul = false;

		try
		{
			st = connection.createStatement();
			st.executeUpdate(query);
			connection.commit();

			System.out.println("Se ejecuta correctamente el update en ORACLE");
			resul = true;

		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if(st != null)
			{
				st.close();
			}

			if(connection != null)
			{
				connection.close();
			}
		}
		return resul;


	}

//    public static void main (String[] args){
//    	
//    	String query = "SELECT * FROM TLSB.TTLSBCAB AS TTLSBCAB, TLSB.TTLSBPOR AS TTLSBPOR WHERE TTLSBCAB.COD_CLASEORD IN ('S9R','S9D') AND TTLSBCAB.COD_ESTACASH = '005' AND (TTLSBPOR.COD_CLIECASH = TTLSBCAB.COD_CLIECASH AND TTLSBPOR.COD_CLASEORD = TTLSBCAB.COD_CLASEORD AND TTLSBPOR.COD_IDORDEN = TTLSBCAB.COD_IDORDEN)";
//    	String updateDevoluciones = "UPDATE TLSB.TTLSBCAB SET FEC_PROCESCA = '20151119' WHERE COD_CLIECASH = '0023000120072852' AND COD_CLASEORD = 'S9D' AND COD_IDORDEN = 'hszujkvg'";
//    	String updateRechazo = "UPDATE TLSB.TTLSBCAB SET FEC_PROCESCA = '20151118' WHERE COD_CLIECASH = '0023000120072852' AND COD_CLASEORD = 'S9R' AND COD_IDORDEN = 'hszujyiv'";
//    	
//    	try {
//    		update(updateDevoluciones);
//    		String valor = select(query,"COD_IDORDEN");
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//    }

}
