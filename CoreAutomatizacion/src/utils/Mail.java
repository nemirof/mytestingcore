package utils;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Properties;
import java.util.Scanner;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class Mail
{
	
	private static void enviarConGMail(String destinatario, String asunto) throws FileNotFoundException {
		
		
		File ruta = new File("resources/path.txt");
  		
	  	String path = "";
	  	
		if (ruta.exists()){				
			Scanner scanner2 = new Scanner( ruta, "UTF-8" );
			path = scanner2.useDelimiter("\\A").next();
			System.out.println("[INFO]: Leemos la ruta: "+path +", en el fichero resources/path.txt");
			scanner2.close();
			ruta.delete();
		}
		
		//BORRAR
		String pathCloud = "";
		File rutaCloud = new File("resources/pathCloud.txt");
		if (rutaCloud.exists()){				
			Scanner scanner3 = new Scanner(rutaCloud, "UTF-8" );
			path = scanner3.useDelimiter("\\A").next();
			System.out.println("[INFO]: Leemos la ruta: "+path +", en el fichero resources/pathCloud.txt");
			scanner3.close();
			rutaCloud.delete();
		}



		
		String texto = "Hola, os adjuntamos los resultados de la ejecuci�n autom�tica. El resumen de �sta, ser�a el siguiente:";

				

		
	    // Esto es lo que va delante de @gmail.com en tu cuenta de correo. Es el remitente tambi�n.
	    String remitente = "automatizacion.bbva.group@gmail.com";  //Para la direcci�n nomcuenta@gmail.com
	    String ccEmail = "automatizacion.bbva.group@gmail.com"; 
	    Properties props = System.getProperties();
	    props.put("mail.smtp.host", "smtp.gmail.com");  //El servidor SMTP de Google
	    props.put("mail.smtp.user", remitente);
	    props.put("mail.smtp.clave", "atleti14");    //La clave de la cuenta
	    props.put("mail.smtp.auth", "true");    //Usar autenticaci�n mediante usuario y clave
	    props.put("mail.smtp.starttls.enable", "true"); //Para conectar de manera segura al servidor SMTP
	    props.put("mail.smtp.port", "587"); //El puerto SMTP seguro de Google

	    Session session = Session.getDefaultInstance(props);
	    MimeMessage message = new MimeMessage(session);

	    try {
	        message.setFrom(new InternetAddress(remitente));
	        message.addRecipients(Message.RecipientType.TO, destinatario); 
	        message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
	        //Se podr�an a�adir varios de la misma manera
//	        message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
	        message.setSubject(asunto);
	        message.setText(
					"<a>"+texto+" </a>"
							+ "\n"
							+ Utils.devuelveResumenParaMail()			
							+ "<br><br><a> El informe completo de esta ejecuci�n se encuentra en: <br> <a href=\""+path+"\">Informe HTML</a></a>",
					"utf-8", "html");
	        Transport transport = session.getTransport("smtp");
	        transport.connect("smtp.gmail.com", remitente, "atleti14");
	        transport.sendMessage(message, message.getAllRecipients());
	        transport.close();
	    }
	    catch (MessagingException me) {
	        me.printStackTrace();   //Si se produce un error
	    }
	}


	public static void enviaMail(String subject, String texto, String file1, String...remitente) throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String fueraBBVA = params.getValorPropiedad("fueraBBVA");
		
		String host = "devsmtptc-ipfromlock.igrupobbva";
		String usernameTemp = "";
		String passwordTemp = "";
		
		// Remitente (NO SE PUEDE CAMBIAR)
		String from = "itarvi-pnm@bbva.com";
		
		// Destinatarios
		String to = params.getValorPropiedad("destinatarios");
		String ccEmail = "automatizacion-bbva.group@bbva.com, automatizacion.bbva.group@gmail.com";
		String bccEmail = params.getValorPropiedad("bcc");
		
		
		if (!Utils.insideBBVA()) {
			
			enviarConGMail(to, subject);
//			host = "smtp.sendgrid.net";
//			usernameTemp = "azure_b7fb14ceaaa350f5d764f7145fc0b517@azure.com";
//			passwordTemp = "S0fttek@2019";
//			from="noreply@softtek.com";
			
//			host = "mail.gmail.com";
//			usernameTemp = "autonemiroff@gmail.com";
//			passwordTemp = "S0fttek@2019";
//			from="noreply@softtek.com";
			
		}
		
		else {
		
		final String username = usernameTemp;
		final String password = passwordTemp;

		System.out.println("Env�o de mail. Asunto: "+subject);

		if (remitente.length>0)
			from = remitente[0];

		// Assuming you are sending email through relay.jangosmtp.net
		

		Properties props = new Properties();
		props.put("mail.smtp.auth", "false");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.debug", "true");


		// Get the Session object.
		Session session = Session.getInstance(props, new javax.mail.Authenticator()
		{
			protected PasswordAuthentication getPasswordAuthentication()
			{
				return new PasswordAuthentication(username, password);
			}
		});

		try
		{
			
    	  	File ruta = new File("resources/path.txt");
	  		
    	  	String path = "";
    	  	
  			if (ruta.exists()){				
  				Scanner scanner2 = new Scanner( ruta, "UTF-8" );
  				path = scanner2.useDelimiter("\\A").next();
  				System.out.println("[INFO]: Leemos la ruta: "+path +", en el fichero resources/path.txt");
  				scanner2.close();
  				ruta.delete();
  			}
  			
  			//BORRAR
  			String pathCloud = "";
  			File rutaCloud = new File("resources/pathCloud.txt");
  			if (rutaCloud.exists()){				
  				Scanner scanner3 = new Scanner(rutaCloud, "UTF-8" );
  				pathCloud = scanner3.useDelimiter("\\A").next();
  				System.out.println("[INFO]: Leemos la ruta: "+pathCloud +", en el fichero resources/pathCloud.txt");
  				scanner3.close();
  				rutaCloud.delete();
  			}

			params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();

			String filename = new File(file1).getAbsolutePath();
			
			boolean fileExists = new File(filename).exists();

			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));

			// Se establece el subject
			message.setSubject(subject + " " + Utils.GetTodayDateAndTime());

			// create and fill the first message part
			MimeBodyPart mbp1 = new MimeBodyPart();
			
			
			if (file1.equalsIgnoreCase("")){
				
				if(HTTPConnect.comprobarRespuestaHTML(path, "Pruebas"))
					mbp1.setText(
							"<a>"+texto+" </a>"
									+ "\n"
									+ Utils.devuelveResumenParaMail()			
									+ "<br><br><a> El informe completo de esta ejecuci�n se encuentra en: <br> <a href=\""+path+"\">Informe HTML</a></a>",
							"utf-8", "html");
				
				else if(HTTPConnect.comprobarRespuestaHTML(pathCloud, "Pruebas"))
					mbp1.setText(
							"<a>"+texto+" </a>"
									+ "\n"
									+ Utils.devuelveResumenParaMail()			
									+ "<br><br><a> El informe completo de esta ejecuci�n se encuentra en: <br> <a href=\""+pathCloud+"\">Informe HTML</a></a>",
							"utf-8", "html");
				
				else 
					mbp1.setText("<a>"+texto+" </a>"
									+ "\n"+ Utils.devuelveResumenParaMail(),"utf-8", "html");
			

				Multipart mp = new MimeMultipart();
				mp.addBodyPart(mbp1);

				// add the Multipart to the message
				message.setContent(mp);

				if(ccEmail.length() > 0)
				{
					message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
				}
				
//				if(bccEmail.length() > 0)
//				{
//					message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bccEmail, false));
//				}

			
			
			}
			

			// Si existe el HTML de resultados
			else if(fileExists)
			{
				
				if(HTTPConnect.comprobarRespuestaHTML(path, "Datos de Ejecuci�n"))
					mbp1.setText(
							"<a>"+texto+" </a>"
									+ "\n"
									+ Utils.devuelveResumenParaMail()
									+ "<br><br> <a>Para m�s informaci�n puede ver el informe adjunto o visitar nuestro site:<br> <a href=\"https://sites.google.com/a/bbva.com/plataforma_banca_empresas/piezas-estructurales/gestion-instalacion-release/acceso_gestion_\">Gesti�n de Pruebas</a></a>"					
									+ "<br><br><a> El informe completo de esta ejecuci�n se encuentra en: <br> <a href=\""+path+"\">Informe HTML</a></a>",
							"utf-8", "html");
				
				else if(HTTPConnect.comprobarRespuestaHTML(pathCloud, "Datos de Ejecuci�n"))
					mbp1.setText(
							"<a>"+texto+" </a>"
									+ "\n"
									+ Utils.devuelveResumenParaMail()
									+ "<br><br> <a>Para m�s informaci�n puede ver el informe adjunto o visitar nuestro site:<br> <a href=\"https://sites.google.com/a/bbva.com/plataforma_banca_empresas/piezas-estructurales/gestion-instalacion-release/acceso_gestion_\">Gesti�n de Pruebas</a></a>"					
									+ "<br><br><a> El informe completo de esta ejecuci�n se encuentra en: <br> <a href=\""+pathCloud+"\">Informe HTML</a></a>",
							"utf-8", "html");
				else {
					mbp1.setText(
							"<a>"+texto+" </a>"
									+ "\n"
									+ Utils.devuelveResumenParaMail()
									+ "<br><br> <a>Para m�s informaci�n puede ver el informe adjunto o visitar nuestro site:<br> <a href=\"https://sites.google.com/a/bbva.com/plataforma_banca_empresas/piezas-estructurales/gestion-instalacion-release/acceso_gestion_\">Gesti�n de Pruebas</a></a>",
									"utf-8", "html");
				}


				// attach the file to the message
				MimeBodyPart mbp2 = new MimeBodyPart();
				FileDataSource fds = new FileDataSource(filename);
				mbp2.setDataHandler(new DataHandler(fds));
				mbp2.setFileName(fds.getName());
				
//				// Handle attachment 1
//		        MimeBodyPart messageBodyPart1 = new MimeBodyPart();
//		        messageBodyPart1.attachFile(filename);
//
//		        // Handle attachment 2
//		        MimeBodyPart messageBodyPart2 = new MimeBodyPart();
//		        messageBodyPart2.attachFile(filename2);


				Multipart mp = new MimeMultipart();
				mp.addBodyPart(mbp1);
				mp.addBodyPart(mbp2);

				// add the Multipart to the message
				message.setContent(mp);

				if(ccEmail.length() > 0)
				{
					message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
				}
				
				if(bccEmail.length() > 0)
				{
					message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bccEmail, false));
				}
			}

			else
			{
				mbp1.setText("Hola, os informamos que ha terminado el despliegue autom�tico. Sucedi� alg�n problema en la generaci�n del archivo HTML, con lo que no os podemos adjuntar el mismo. " + "\nP�ngase en contacto con los responsables.");
				Multipart mp = new MimeMultipart();
				mp.addBodyPart(mbp1);
				message.setContent(mp);
				
				if(ccEmail.length() > 0)
				{
					message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
				}
				
				if(bccEmail.length() > 0)
				{
					message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bccEmail, false));
				}
			}


			// Send message
			Transport.send(message);

			System.out.println("Sent message successfully....");

		}
		catch(MessagingException e)
		{
			throw new RuntimeException(e);
		}
		}
	}
	
	
	public static void enviaMailBasico(String subject, String texto, String file1) throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		// Destinatarios
		String to = params.getValorPropiedad("destinatarios");
		String ccEmail = "automatizacion-bbva.group@bbva.com";//"jorge.munoz.bautista.contractor@bbva.com," + params.getValorPropiedad("destinatarios");


		// Remitente (NO SE PUEDE CAMBIAR)
		String from = "itarvi-pnm@bbva.com";
		final String username = "";
		final String password = "";

		// Assuming you are sending email through relay.jangosmtp.net
		String host = "devsmtptc-ipfromlock.igrupobbva";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "false");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.debug", "true");


		// Get the Session object.
		Session session = Session.getInstance(props, new javax.mail.Authenticator()
		{
			protected PasswordAuthentication getPasswordAuthentication()
			{
				return new PasswordAuthentication(username, password);
			}
		});

		try
		{
				
			
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));

			// Se establece el subject
			message.setSubject(subject + " " + Utils.GetTodayDateAndTime());

			// create and fill the first message part
			MimeBodyPart mbp1 = new MimeBodyPart();
			
			mbp1.setText(
					"<a>"+texto+" </a>"
							+ "\n"
							+ "<br><br><a> El informe completo de esta ejecuci�n se puede consultar en: <br><br> <a href=\""+file1+"\">Informe HTML</a></a>",
					"utf-8", "html");
			Multipart mp = new MimeMultipart();
			mp.addBodyPart(mbp1);
			message.setContent(mp);
			if(ccEmail.length() > 0)
			{
				message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
			}
			

			// Send message
			Transport.send(message);

			System.out.println("Sent message successfully....");

		}
		catch(MessagingException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	public static void enviaMailTexto(String subject, String texto, String file1, String ccEmail, String...remitente) throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		// Destinatarios
		String to = "automatizacion-bbva.group@bbva.com";

		// Remitente (NO SE PUEDE CAMBIAR)
		String from = "itarvi-pnm@bbva.com";
		
		if (remitente.length>0)
			from = remitente[0];
		
		final String username = "";
		final String password = "";

		// Assuming you are sending email through relay.jangosmtp.net
		String host = "devsmtptc-ipfromlock.igrupobbva";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "false");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.debug", "true");
		


		// Get the Session object.
		Session session = Session.getInstance(props, new javax.mail.Authenticator()
		{
			protected PasswordAuthentication getPasswordAuthentication()
			{
				return new PasswordAuthentication(username, password);
			}
		});

		try
		{
				
			
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));

			// Se establece el subject
			message.setSubject(subject + " " + Utils.GetTodayDateAndTime());

			// create and fill the first message part
			MimeBodyPart mbp1 = new MimeBodyPart();
			
			mbp1.setText(
					"<a>"+texto+" </a>"+ "\n",
					"utf-8", "html");

			

			String filename = new File(file1).getAbsolutePath();
			
			MimeBodyPart mbp2 = new MimeBodyPart();
			FileDataSource fds = new FileDataSource(filename);
			mbp2.setDataHandler(new DataHandler(fds));
			mbp2.setFileName(fds.getName());
			
			Multipart mp = new MimeMultipart();
			mp.addBodyPart(mbp1);
			mp.addBodyPart(mbp2);

			// add the Multipart to the message
			message.setContent(mp);

			if(ccEmail.length() > 0)
			{
				message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
			}
			
			

			// Send message
			Transport.send(message);

			System.out.println("Sent message successfully....");

		}
		catch(MessagingException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	public static void enviaMailInfoServicios(String subject, String texto, String file1, String ccEmail, String...remitente) throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		// Destinatarios
		String to = "mario.pulido.leno.contractor@bbva.com";

		// Remitente (NO SE PUEDE CAMBIAR)
		String from = "info-servicios.group@bbva.com";
		
		if (remitente.length>0)
			from = remitente[0];
		
		final String username = "";
		final String password = "";

		// Assuming you are sending email through relay.jangosmtp.net
		String host = "devsmtptc-ipfromlock.igrupobbva";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "false");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.debug", "true");
		


		// Get the Session object.
		Session session = Session.getInstance(props, new javax.mail.Authenticator()
		{
			protected PasswordAuthentication getPasswordAuthentication()
			{
				return new PasswordAuthentication(username, password);
			}
		});

		try
		{
				
			
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));

			// Se establece el subject
			message.setSubject(subject);

			// create and fill the first message part
			MimeBodyPart mbp1 = new MimeBodyPart();
			
			mbp1.setText(
					"<a>"+texto+" </a>"+ "\n\n"
							+ "<img src=\"https://cdn3.imggmi.com/uploads/2019/3/29/ef4d2a554f6e8823e8cf34a2e7be11a4-full.png\" alt=\"Smiley face\">"
							+ "\n",
					"utf-8", "html");

			

			String filename = new File(file1).getAbsolutePath();
			
			MimeBodyPart mbp2 = new MimeBodyPart();
			FileDataSource fds = new FileDataSource(filename);
			mbp2.setDataHandler(new DataHandler(fds));
			mbp2.setFileName(fds.getName());
			
			Multipart mp = new MimeMultipart();
			mp.addBodyPart(mbp1);
			mp.addBodyPart(mbp2);

			// add the Multipart to the message
			message.setContent(mp);

			if(ccEmail.length() > 0)
			{
				message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
			}
			
			

			// Send message
			Transport.send(message);

			System.out.println("Sent message successfully....");

		}
		catch(MessagingException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	public static void enviaMailBasicoEquipoAuto(String subject, String texto, String file1) throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		// Destinatarios
		String to = params.getValorPropiedad("destinatarios");
		String ccEmail = "automatizacion-bbva.group@bbva.com";//"jorge.munoz.bautista.contractor@bbva.com," + params.getValorPropiedad("destinatarios");


		// Remitente (NO SE PUEDE CAMBIAR)
		String from = "automatizacion-bbva.group@bbva.com";
		final String username = "";
		final String password = "";

		// Assuming you are sending email through relay.jangosmtp.net
		String host = "devsmtptc-ipfromlock.igrupobbva";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "false");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.debug", "true");


		// Get the Session object.
		Session session = Session.getInstance(props, new javax.mail.Authenticator()
		{
			protected PasswordAuthentication getPasswordAuthentication()
			{
				return new PasswordAuthentication(username, password);
			}
		});

		try
		{
				
			
			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));

			// Se establece el subject
			message.setSubject(subject + " " + Utils.GetTodayDateAndTime());

			// create and fill the first message part
			MimeBodyPart mbp1 = new MimeBodyPart();
			
			mbp1.setText(
					"<a>"+texto+" </a>"
							+ "\n"
							+ "<br><br><a> El informe completo de esta ejecuci�n se puede consultar en: <br><br> <a href=\""+file1+"\">Informe HTML</a></a>",
					"utf-8", "html");
			Multipart mp = new MimeMultipart();
			mp.addBodyPart(mbp1);
			message.setContent(mp);
			if(ccEmail.length() > 0)
			{
				message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
			}
			

			// Send message
			Transport.send(message);

			System.out.println("Sent message successfully....");

		}
		catch(MessagingException e)
		{
			throw new RuntimeException(e);
		}
	}

	
	
	public static void enviaMailAdjunto(String subject, String texto, String captura1) throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		// Destinatarios
		String to = params.getValorPropiedad("destinatarios");
		String ccEmail = "automatizacion-bbva.group@bbva.com";//"jorge.munoz.bautista.contractor@bbva.com," + params.getValorPropiedad("destinatarios");

		// Remitente (NO SE PUEDE CAMBIAR)
		String from = "automatizacion-bbva.group@bbva.com";
		final String username = "";
		final String password = "";

		// Assuming you are sending email through relay.jangosmtp.net
		String host = "devsmtptc-ipfromlock.igrupobbva";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "false");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.debug", "true");


		// Get the Session object.
		Session session = Session.getInstance(props, new javax.mail.Authenticator()
		{
			protected PasswordAuthentication getPasswordAuthentication()
			{
				return new PasswordAuthentication(username, password);
			}
		});

		try
		{

			params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();

			String filename = new File(captura1).getAbsolutePath();
			
			boolean fileExists = new File(filename).exists();

			// Create a default MimeMessage object.
			Message message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));

			// Se establece el subject
			message.setSubject(subject + " - " +Utils.GetTodayDateAndTime());

			// create and fill the first message part
			MimeBodyPart mbp1 = new MimeBodyPart();

			// Si existe el HTML de resultados
			if(fileExists)
			{

				// Enviamos un resumen que extraemos del HTML de resultados.
				mbp1.setText(
						"<a>"+texto+" </a>"
								+ "\n"
								+ "<br><br> <a>Compruebe los ficheros adjuntos."
								+ "</a>",
						"utf-8", "html");


				// attach the file to the message
				MimeBodyPart mbp2 = new MimeBodyPart();
				FileDataSource fds = new FileDataSource(filename);
				mbp2.setDataHandler(new DataHandler(fds));
				mbp2.setFileName(fds.getName());
				
//				// Handle attachment 1
//		        MimeBodyPart messageBodyPart1 = new MimeBodyPart();
//		        messageBodyPart1.attachFile(filename);
//
//		        // Handle attachment 2
//		        MimeBodyPart messageBodyPart2 = new MimeBodyPart();
//		        messageBodyPart2.attachFile(filename2);


				Multipart mp = new MimeMultipart();
				mp.addBodyPart(mbp1);
				mp.addBodyPart(mbp2);

				// add the Multipart to the message
				message.setContent(mp);

				if(ccEmail.length() > 0)
				{
					message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
				}
			}
			else
			{
				mbp1.setText("Hola, os informamos que ha terminado la ejecuci�n. Sucedi� alg�n problema en la generaci�n del archivo, con lo que no os podemos adjuntar el mismo. " + "\nP�ngase en contacto con los responsables.");
				Multipart mp = new MimeMultipart();
				mp.addBodyPart(mbp1);
				message.setContent(mp);
				if(ccEmail.length() > 0)
				{
					message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
				}
			}


			// Send message
			Transport.send(message);

			System.out.println("Sent message successfully....");

		}
		catch(MessagingException e)
		{
			throw new RuntimeException(e);
		}
	}
	
}
