package utils;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import junit.framework.TestCase;


public class CrearFicheroLanzamiento extends TestCase
{

	public void setUp() throws Exception
	{}


	public void testCrearFichero() throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		int numeroEjecucionesSimultaneas = Integer.parseInt(params.getValorPropiedad("numeroEjecucionesSimultaneas"));

		String statText = new File("LanzamientoSimultaneo.bat").getAbsolutePath();
		System.out.println(statText);
		FileOutputStream is = new FileOutputStream(statText);
		OutputStreamWriter osw = new OutputStreamWriter(is);
		Writer w = new BufferedWriter(osw);

		 if (Utils.isWindows()) {

			for(int i = 0; i < numeroEjecucionesSimultaneas; i++)
			{
				w.write("START java -jar Main.jar " + i + "\n");
				w.write("timeout 8\n");
				// w.write("START CMD.EXE /C ^(java -jar Main.jar "+i+"^> results\\testMain"+i+".txt^)\n");
			}
	
			w.write("java -jar Main.jar SoloEspera");
		 }
		 
		 else {
			 
			 	w.write("#!/bin/bash");
			 
				for(int i = 0; i < numeroEjecucionesSimultaneas; i++)
				{
					w.write("java -jar Main.jar " + i + "&\n");
					w.write("sleep 8&\n");
					// w.write("START CMD.EXE /C ^(java -jar Main.jar "+i+"^> results\\testMain"+i+".txt^)\n");
				}
		
				w.write("java -jar Main.jar SoloEspera");
			 
			 
		 }

		w.close();

	}


	public void tearDown() throws Exception
	{

	}

}
