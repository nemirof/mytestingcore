package utils;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import junit.framework.TestCase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.csvreader.CsvReader;


public abstract class Funciones extends TestCase
{

	public static void informarAlta(WebDriver driver, Usuario usuario) throws Exception
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String pais = params.getValorPropiedad("pais");

		if(pais.equalsIgnoreCase("Spain"))
			Funciones.informarAltaSpain(driver, usuario);
		else if(pais.equalsIgnoreCase("Mexico"))
			Funciones.informarAltaMexico(driver, usuario);
		else if(pais.equalsIgnoreCase("Compass"))
			Funciones.informarAltaCompass(driver, usuario);
		else if(pais.equalsIgnoreCase("Chile"))
			Funciones.informarAltaChile(driver, usuario);
		else if(pais.equalsIgnoreCase("Argentina"))
			Funciones.informarAltaArgentina(driver, usuario);
		else
			Funciones.informarAltaDefecto(driver, usuario);


	}


	public static void seleccionarTipoTelefono(WebDriver driver, Usuario usuario) throws Exception
	{
		String dispositivo = usuario.getDispositivo();

		if(dispositivo == null)
			dispositivo = "";

		if(!dispositivo.equalsIgnoreCase(""))
		{
			if(dispositivo.equalsIgnoreCase("F�sico"))
			{
				if(Utils.estamosEnLatam())
				{
					Utils.seleccionarCombo(driver, "txType", "Fijo");
				}
			}
			else if(dispositivo.equalsIgnoreCase("M�vil"))
			{
				if(Utils.estamosEnLatam())
				{
					Utils.seleccionarCombo(driver, "txType", "M�vil");
				}
			}
			else if(Utils.estamosEnLatam() && Utils.isDisplayed(driver, "//select[@id='txType'or@name='txType']", 1))
			{
				Utils.seleccionarCombo(driver, "txType", "Fijo");
			}
		}
		else if(Utils.estamosEnLatam() && Utils.isDisplayed(driver, "//select[@id='txType'or@name='txType']", 1))
		{
			Utils.seleccionarCombo(driver, "txType", "Fijo");
		}
	}


	public static void informarAltaMexico(WebDriver driver, Usuario usuario) throws Exception
	{

		usuario.setTipoDoc("Pasaporte");
		Utils.esperarProcesandoPeticionMejorado(driver);
		Utils.esperarVisibleTiempo(driver, Constantes.CampoCodigoUsuario, 20);
		driver.findElement(By.xpath(Constantes.CampoCodigoUsuario)).sendKeys(usuario.getCodUsuario());
		driver.findElement(By.xpath(Constantes.CampoNombreUsuario)).sendKeys(usuario.getNombreUsuario());
		driver.findElement(By.xpath(Constantes.CampoMailUsuario)).sendKeys(usuario.getMail());
		driver.findElement(By.xpath(Constantes.CampoMail2Usuario)).sendKeys(usuario.getMail());
		driver.findElement(By.xpath(Constantes.CampoPasswordUsuario)).sendKeys(usuario.getPassword());

		Utils.seleccionarCombo(driver, "txTipoDoc", usuario.getTipoDoc());
		driver.findElement(By.xpath(Constantes.CampoDocumentoUsuario)).sendKeys(usuario.getNumDoc());

		Funciones.seleccionarTipoTelefono(driver, usuario);

		Utils.seleccionarCombo(driver, "txNomPais", usuario.getPais());


		driver.findElement(By.xpath(Constantes.CampoTelefonoUsuario)).sendKeys(usuario.getTelefono());

		if(!usuario.getTipoPoder().equals("NO"))
		{
			Utils.clickEnElVisible(driver, Constantes.RadioTipoUsuarioAdmin);
			Utils.seleccionarCombo(driver, "tipoPoder", usuario.getTipoPoder());
		}
		if(!usuario.getTipoFirma().equals("NO"))
		{
			Utils.clickEnElVisible(driver, Constantes.RadioFirmante);
			Utils.seleccionarCombo(driver, "tipoFirma", usuario.getTipoFirma());
		}
	}


	public static void informarAltaCompass(WebDriver driver, Usuario usuario) throws Exception
	{

		usuario.setTipoDoc("Pasaporte");
		Utils.esperarProcesandoPeticionMejorado(driver);
		Utils.esperarVisibleTiempo(driver, Constantes.CampoCodigoUsuario, 20);
		driver.findElement(By.xpath(Constantes.CampoCodigoUsuario)).sendKeys(usuario.getCodUsuario());
		driver.findElement(By.xpath(Constantes.CampoNombreUsuario)).sendKeys(usuario.getNombreUsuario());
		driver.findElement(By.xpath(Constantes.CampoMailUsuario)).sendKeys(usuario.getMail());
		driver.findElement(By.xpath(Constantes.CampoMail2Usuario)).sendKeys(usuario.getMail());
		driver.findElement(By.xpath(Constantes.CampoPasswordUsuario)).sendKeys(usuario.getPassword());

		Utils.seleccionarCombo(driver, "txTipoDoc", usuario.getTipoDoc());
		driver.findElement(By.xpath(Constantes.CampoDocumentoUsuario)).sendKeys(usuario.getNumDoc());

		Funciones.seleccionarTipoTelefono(driver, usuario);

		Utils.seleccionarCombo(driver, "txNomPais", usuario.getPais());


		driver.findElement(By.xpath(Constantes.CampoTelefonoUsuario)).sendKeys(usuario.getTelefono());

		if(!usuario.getTipoPoder().equals("NO"))
		{
			Utils.clickEnElVisible(driver, Constantes.RadioTipoUsuarioAdmin);
			Utils.seleccionarComboFallando(driver, "tipoPoder", usuario.getTipoPoder());
		}
		if(!usuario.getTipoFirma().equals("NO"))
		{
			Utils.clickEnElVisible(driver, Constantes.RadioFirmante);
			Utils.seleccionarComboFallando(driver, "tipoFirma", usuario.getTipoFirma());
		}
	}


	public static void informarAltaArgentina(WebDriver driver, Usuario usuario) throws Exception
	{

		usuario.setTipoDoc("Pasaporte");
		Utils.esperarProcesandoPeticionMejorado(driver);
		Utils.esperarVisibleTiempo(driver, Constantes.CampoCodigoUsuario, 20);
		driver.findElement(By.xpath(Constantes.CampoCodigoUsuario)).sendKeys(usuario.getCodUsuario());
		driver.findElement(By.xpath(Constantes.CampoNombreUsuario)).sendKeys(usuario.getNombreUsuario());
		driver.findElement(By.xpath(Constantes.CampoMailUsuario)).sendKeys(usuario.getMail());
		driver.findElement(By.xpath(Constantes.CampoMail2Usuario)).sendKeys(usuario.getMail());
		driver.findElement(By.xpath(Constantes.CampoPasswordUsuario)).sendKeys(usuario.getPassword());

		Utils.seleccionarCombo(driver, "txTipoDoc", usuario.getTipoDoc());
		driver.findElement(By.xpath(Constantes.CampoDocumentoUsuario)).sendKeys(usuario.getNumDoc());

		Utils.seleccionarCombo(driver, "txNomPais", usuario.getPais());
		Funciones.seleccionarTipoTelefono(driver, usuario);

		driver.findElement(By.xpath(Constantes.CampoTelefonoUsuario)).sendKeys(usuario.getTelefono());

		if(!usuario.getTipoPoder().equals("NO"))
		{
			Utils.clickEnElVisible(driver, Constantes.RadioTipoUsuarioAdmin);
			Utils.seleccionarCombo(driver, "tipoPoder", usuario.getTipoPoder());
		}
		if(!usuario.getTipoFirma().equals("NO"))
		{
			Utils.clickEnElVisible(driver, Constantes.RadioFirmante);
			Utils.seleccionarCombo(driver, "tipoFirma", usuario.getTipoFirma());
		}
	}


	public static void informarAltaChile(WebDriver driver, Usuario usuario) throws Exception
	{

		usuario.setTipoDoc("RUT");
		usuario.setNumDoc(Utils.devuelveRUTAleatorio());

		Utils.esperarProcesandoPeticionMejorado(driver);
		Utils.esperarVisibleTiempo(driver, Constantes.CampoNombreUsuario, 20);
		driver.findElement(By.xpath(Constantes.CampoNombreUsuario)).sendKeys(usuario.getNombreUsuario());
		driver.findElement(By.xpath(Constantes.CampoMailUsuario)).sendKeys(usuario.getMail());
		driver.findElement(By.xpath(Constantes.CampoMail2Usuario)).sendKeys(usuario.getMail());
		driver.findElement(By.xpath(Constantes.CampoPasswordUsuario)).sendKeys(usuario.getPassword());

		Utils.seleccionarCombo(driver, "txTipoDoc", usuario.getTipoDoc());
		driver.findElement(By.xpath(Constantes.CampoDocumentoUsuario)).sendKeys(usuario.getNumDoc());

		Utils.seleccionarCombo(driver, "txNomPais", usuario.getPais());
		Funciones.seleccionarTipoTelefono(driver, usuario);
		driver.findElement(By.xpath(Constantes.CampoTelefonoUsuario)).sendKeys(usuario.getTelefono());

		if(!usuario.getTipoPoder().equals("NO"))
		{
//			driver.findElement(By.xpath(Constantes.RadioTipoUsuarioAdmin)).click();
//			Utils.seleccionarCombo (driver,"tipoPoder",usuario.getTipoPoder());
		}
		if(!usuario.getTipoFirma().equals("NO"))
		{
			Utils.clickEnElVisible(driver, Constantes.RadioFirmante);
			Utils.seleccionarCombo(driver, "tipoFirma", usuario.getTipoFirma());
		}
	}


	public static void informarAltaDefecto(WebDriver driver, Usuario usuario) throws Exception
	{
		Utils.esperarProcesandoPeticionMejorado(driver);
		Utils.esperarVisibleTiempo(driver, Constantes.CampoCodigoUsuario, 20);
		driver.findElement(By.xpath(Constantes.CampoCodigoUsuario)).sendKeys(usuario.getCodUsuario());
		driver.findElement(By.xpath(Constantes.CampoNombreUsuario)).sendKeys(usuario.getNombreUsuario());
		driver.findElement(By.xpath(Constantes.CampoMailUsuario)).sendKeys(usuario.getMail());
		driver.findElement(By.xpath(Constantes.CampoMail2Usuario)).sendKeys(usuario.getMail());
		driver.findElement(By.xpath(Constantes.CampoPasswordUsuario)).sendKeys(usuario.getPassword());
		Utils.seleccionarCombo(driver, "txTipoDoc", usuario.getTipoDoc());
		driver.findElement(By.xpath(Constantes.CampoDocumentoUsuario)).sendKeys(usuario.getNumDoc());
		Utils.seleccionarCombo(driver, "txNomPais", usuario.getPais());
		driver.findElement(By.xpath(Constantes.CampoTelefonoUsuario)).sendKeys(usuario.getTelefono());
		if(!usuario.getTipoPoder().equals("NO"))
		{
			Utils.clickEnElVisible(driver, Constantes.RadioTipoUsuarioAdmin);
			Utils.seleccionarCombo(driver, "tipoPoder", usuario.getTipoPoder());
		}
		if(!usuario.getTipoFirma().equals("NO"))
		{
			Utils.clickEnElVisible(driver, Constantes.RadioFirmante);
			Utils.seleccionarCombo(driver, "tipoFirma", usuario.getTipoFirma());
		}
	}


	public static void informarAltaSpain(WebDriver driver, Usuario usuario) throws Exception
	{
		Utils.esperarProcesandoPeticionMejorado(driver);
		Utils.esperarVisibleTiempoSinSalir(driver, Constantes.CampoCodigoUsuario, 10);
		
		Utils.introducirTextoEnElVisible(driver, Constantes.CampoCodigoUsuario, usuario.getCodUsuario());
		Utils.introducirTextoEnElVisible(driver, Constantes.CampoNombreUsuario, usuario.getNombreUsuario());
		Utils.introducirTextoEnElVisible(driver, Constantes.CampoMailUsuario, usuario.getMail());
		Utils.introducirTextoEnElVisible(driver, Constantes.CampoMail2Usuario, usuario.getMail());
		Utils.introducirTextoEnElVisible(driver, Constantes.CampoPasswordUsuario, usuario.getPassword());
		Utils.seleccionarComboXPath(driver, Constantes.ComboTipoDocumentoUsuario, usuario.getTipoDoc());
		Utils.introducirTextoEnElVisible(driver, Constantes.CampoDocumentoUsuario, usuario.getNumDoc());
		Utils.seleccionarComboXPath(driver, Constantes.ComboPais, usuario.getPais());
		
		Utils.introducirTextoEnElVisible(driver, Constantes.CampoTelefonoUsuario, usuario.getTelefono());
		
		//Utils.seleccionarCombo(driver, "txTipoDoc", usuario.getTipoDoc());
		//Utils.seleccionarCombo(driver, "txNomPais", usuario.getPais());
		
		if(!usuario.getTipoPoder().equals("NO"))
		{
			Utils.clickEnElVisiblePrimero(driver, Constantes.RadioTipoUsuarioAdmin);
			Utils.seleccionarComboXPath(driver, Constantes.ComboTipoAdmin, usuario.getTipoPoder());
//			Utils.seleccionarCombo(driver, "tipoPoder", usuario.getTipoPoder());
		}
		if(!usuario.getTipoFirma().equals("NO"))
		{
			Utils.clickEnElVisiblePrimero(driver, Constantes.RadioFirmante);
			Utils.seleccionarComboXPath(driver, Constantes.ComboTipoFirmante, usuario.getTipoFirma());
//			Utils.seleccionarCombo(driver, "tipoFirma", usuario.getTipoFirma());
		}
	}


	public static void informarAltaSpainSinMail(WebDriver driver, Usuario usuario) throws Exception
	{
		usuario.setTipoDoc("Pasaporte");
		Utils.esperarProcesandoPeticionDefaultContentVolviendoAPPal(driver);
		Utils.esperarVisibleTiempo(driver, Constantes.CampoCodigoUsuario, 20);
		driver.findElement(By.xpath(Constantes.CampoCodigoUsuario)).sendKeys(usuario.getCodUsuario());
		driver.findElement(By.xpath(Constantes.CampoNombreUsuario)).sendKeys(usuario.getNombreUsuario());
		try
		{
			if(driver.findElement(By.xpath(Constantes.CampoMailUsuario)).getAttribute("disabled").equalsIgnoreCase("null"))
			{
				driver.findElement(By.xpath(Constantes.CampoMailUsuario)).sendKeys(usuario.getMail());
				driver.findElement(By.xpath(Constantes.CampoMail2Usuario)).sendKeys(usuario.getMail());
			}
		}
		catch(Exception e)
		{

		}
		driver.findElement(By.xpath(Constantes.CampoPasswordUsuario)).sendKeys(usuario.getPassword());
		Utils.seleccionarCombo(driver, "txTipoDoc", usuario.getTipoDoc());
		driver.findElement(By.xpath(Constantes.CampoDocumentoUsuario)).sendKeys(usuario.getNumDoc());
		Utils.seleccionarCombo(driver, "txNomPais", usuario.getPais());
		driver.findElement(By.xpath(Constantes.CampoTelefonoUsuario)).sendKeys(usuario.getTelefono());
		if(!usuario.getTipoPoder().equals("NO"))
		{
			Utils.clickEnElVisible(driver, Constantes.RadioTipoUsuarioAdmin);
			Utils.seleccionarCombo(driver, "tipoPoder", usuario.getTipoPoder());
		}
		if(!usuario.getTipoFirma().equals("NO"))
		{
			Utils.clickEnElVisible(driver, Constantes.RadioFirmante);
			Utils.seleccionarCombo(driver, "tipoFirma", usuario.getTipoFirma());
		}
	}


	// crea un objeto de la clase Usuario con datos aleatorios
	public static Usuario crearUsuarioAleatorio(String referencia, String tipoPoder, String tipoFirma) throws Exception
	{
		if(referencia.equalsIgnoreCase("default"))
		{
			TestProperties params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();
			referencia = params.getValorPropiedad("referenciaPorDefecto");
		}

		Usuario nuevoCliente = new Usuario(referencia);

		// L�gica para CodTIpoPoder en funci�n de TipoPoder

		if(tipoPoder == "Solidario-Indistinto")
			nuevoCliente.setCodTipoPoder("S");
		if(tipoPoder == "Mancomunado 2")
			nuevoCliente.setCodTipoPoder("M2");
		if(tipoPoder == "Mancomunado 3")
			nuevoCliente.setCodTipoPoder("M3");
		if(tipoPoder == "Mancomunado 4")
			nuevoCliente.setCodTipoPoder("M4");
		if(tipoPoder == "Sin Poderes")
			nuevoCliente.setCodTipoPoder("SP");

		tipoPoder = Utils.tipoPoderPorIdioma(tipoPoder);
		tipoFirma = Utils.tipoFirmaPorIdioma(tipoFirma);

		String codigoUsuario = "TC" + Utils.CadenaAleatoria(6);
		nuevoCliente.setCodUsuario(codigoUsuario);
		nuevoCliente.setNombreUsuario("AUTO" + codigoUsuario);
		nuevoCliente.setMail("auto" + codigoUsuario + "@mail.com");
		nuevoCliente.setPassword("1111aaaa");


		nuevoCliente.setNumDoc(Utils.devuelveNIFAleatorio());
		nuevoCliente.setTelefono("91" + Utils.CadenaAleatoria(7));
		nuevoCliente.setTipoPoder(tipoPoder);
		nuevoCliente.setTipoFirma(tipoFirma);
		nuevoCliente.setTipoDoc("NIF");
		nuevoCliente.setPais(Utils.paisPorIdioma("Espa�a"));
		nuevoCliente.setCodPais("0034");


		return nuevoCliente;

	}


	// Este m�todo devuelve el primer usuario que coincida con las entradas TipoPoder, TipoFirma y Estado.
	// Si alg�n par�metro est� vac�o, ignora el valor de dicho par�metro
	public static Usuario devuelveUsuarioPorTipo(String vTipoPoder, String vTipoFirma, String vEstado) throws Exception
	{

		Usuario usuarioExistente = null;

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosVariables");

		String outputFile = rutaCSV;

		boolean alreadyExists = new File(outputFile).exists();

		if(!alreadyExists)
			return null;

		CsvReader reader = new CsvReader(new FileReader(outputFile));


		boolean resul = false;

		// Buscamos si est� un usuario con esas caracter�sticas en el archivo
		while(reader.readRecord())
		{
			String tipoPoder = reader.get(12);
			String tipoFirma = reader.get(13);
			String estado = reader.get(14);
			if((tipoPoder.equalsIgnoreCase(vTipoPoder) || vTipoPoder.equals("")) && (tipoFirma.equalsIgnoreCase(vTipoFirma) || vTipoFirma.equals("")) && (estado.equalsIgnoreCase(vEstado))
					|| vEstado.equals(""))
			{
				resul = true;
				break;
			}
		}

		// Si lo encontramos, lo devolvemos
		if(resul)
			usuarioExistente = new Usuario(reader);

		reader.close();

		return usuarioExistente;

	}


	// Devuelve un usuario del csv de los datos fijos de prueba. Un fichero por pa�s
	public static Usuario devuelveUsuarioFijo(String vReferencia, String vCodUsuario, String vEntorno) throws Exception
	{

		Usuario usuarioExistente = null;

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosFijos");

		String outputFile = rutaCSV;

		CsvReader reader = new CsvReader(new FileReader(outputFile));

		boolean resul = false;

		// Buscamos si est� un usuario con esas caracter�sticas en el archivo
		while(reader.readRecord())
		{
			String referencia = reader.get(0);
			String codUsuario = reader.get(1);
			String entorno = reader.get(15);
			if((referencia.equalsIgnoreCase(vReferencia)) && (codUsuario.equalsIgnoreCase(vCodUsuario)) && (entorno.equalsIgnoreCase(vEntorno)))
			{
				resul = true;
				break;
			}
		}

		// Si lo encontramos, lo devolvemos
		if(resul)
			usuarioExistente = new Usuario(reader);

		reader.close();

		return usuarioExistente;

	}


	public static Usuario generarUsuarioPdteModificarServiciosFirmas(WebDriver driver, Resultados resultado) throws Exception
	{

		Usuario UsuarioPdteModificarServicios = Funciones.altaUsuarioTodoContratado(driver, resultado);

		// Buscamos el usuario
		Utils.accederAdmUsuarios(driver);
		Funciones.buscarUsuarioYSeleccionarlo(driver, resultado, UsuarioPdteModificarServicios);

		// Bot�n Editar
		Utils.clickEnElVisible(driver, Constantes.BotonEditar);
		Utils.esperarProcesandoPeticion(driver);

		// Validar t�tulo de la p�gina
		String titulo = Utils.devuelveTextoDelVisiblePrimero(driver, Constantes.TituloPantalla);
		Utils.vpWarning(driver, resultado, "Validar t�tulo p�gina modificaci�n usuario", "Modificaci�n de usuario " + UsuarioPdteModificarServicios.getNombreUsuario(), titulo, "FALLO");

		// Pesta�a Servicios Y Firmas
		Utils.clickEnElVisible(driver, Constantes.pestaniaModificarPerfilado);
		Utils.esperarProcesandoPeticionMejorado(driver);

		Utils.capturaIntermedia(driver, resultado, "Editar servicios y firmas de usuario");

		// Deseleccionamos todos los checks
		String xpathChecksMarcados = "//*[@type='checkbox'][@value='true']";
		if(Utils.estamosEnLatam())
			xpathChecksMarcados = "//*[@type='checkbox'][@checked]";

		List<WebElement> listaElementos = driver.findElements(By.xpath(xpathChecksMarcados));
		WebElement elemento = null;

		for(int i = 0; i < listaElementos.size(); ++i)
		{
			if(listaElementos.get(i).isDisplayed())
			{
				elemento = listaElementos.get(i);
				Utils.clickJS(driver, elemento);
			}
		}

		// Seleccionamos el de PAGOS
		Utils.clickEnElVisible(driver, Constantes.checkPagos);

		// Bot�n Continuar
		Utils.clickEnElVisible(driver, Constantes.botonContinuarGenerico);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Se valida el texto
		String textoEsperado = Constantes.conExito;
		String textoActual = driver.findElement(By.xpath(Constantes.TextoResumenValidarUsuario)).getText();
		Utils.vp(driver, resultado, "Mensaje servicios y firmas seleccionados", true, textoActual.indexOf(textoEsperado) > -1, "FALLO");

		return UsuarioPdteModificarServicios;
	}


	public static Usuario generarUsuarioPdteModificarServiciosFirmasConDosServicios(WebDriver driver, Resultados resultado) throws Exception
	{

		Usuario UsuarioPdteModificarServicios = Funciones.altaUsuarioTodoContratado(driver, resultado);

		// Buscamos el usuario
		Utils.accederAdmUsuarios(driver);
		Funciones.buscarUsuarioYSeleccionarlo(driver, resultado, UsuarioPdteModificarServicios);

		// Bot�n Editar
		Utils.clickEnElVisible(driver, Constantes.BotonEditar);
		Utils.esperarProcesandoPeticion(driver);

		// Validar t�tulo de la p�gina
		String titulo = Utils.devuelveTextoDelVisiblePrimero(driver, Constantes.TituloPantalla);
		Utils.vpWarning(driver, resultado, "Validar t�tulo p�gina modificaci�n usuario", "Modificaci�n de usuario " + UsuarioPdteModificarServicios.getNombreUsuario(), titulo, "FALLO");

		// Pesta�a Servicios Y Firmas
		Utils.clickEnElVisible(driver, Constantes.pestaniaModificarPerfilado);
		Utils.esperarProcesandoPeticionMejorado(driver);

		Utils.capturaIntermedia(driver, resultado, "Editar servicios y firmas de usuario");

		// Deseleccionamos todos los checks
		String xpathChecksMarcados = "//*[@type='checkbox'][@value='true']//following-sibling::label";//"//*[@type='checkbox'][@value='true']";
//		if(Utils.estamosEnLatam())
//			xpathChecksMarcados = "//*[@type='checkbox'][@checked]";

		List<WebElement> listaElementos = driver.findElements(By.xpath(xpathChecksMarcados));
		WebElement elemento = null;

		for(int i = 0; i < listaElementos.size(); ++i)
		{
//			if(listaElementos.get(i).isDisplayed())
//			{
				elemento = listaElementos.get(i);
				Utils.clickJS(driver, elemento);
//			}
		}

		// Seleccionamos el de PAGOS
		Utils.clickEnElVisible(driver, Constantes.checkPagos);

		// Seleccionamos el de INFORMACION DE CUENTAS
		Utils.clickEnElVisible(driver, Constantes.checkInformacionCuentas);

		// Bot�n Continuar
		Utils.clickEnElVisible(driver, Constantes.botonContinuarGenerico);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Se valida el texto
		String textoEsperado = Constantes.conExito;
		String textoActual = driver.findElement(By.xpath(Constantes.TextoResumenValidarUsuario)).getText();
		Utils.vp(driver, resultado, "Mensaje servicios y firmas seleccionados", true, textoActual.indexOf(textoEsperado) > -1, "FALLO");

		return UsuarioPdteModificarServicios;
	}


	// Lleva un usuario activo a pendiente de modificar cambi�ndole el nombre. Si es necesario, se da de alta un usuario
	public static Usuario generarUsuarioPdteModificar(WebDriver driver, Resultados resultado) throws Exception
	{

		// Buscamos si existe un usuario no administrador activo. Si no, lo damos de alta
		Usuario usuarioPrueba = Funciones.devuelveUsuarioPorTipo("NO", "", Constantes.ESTADO_ACTIVO);

		if(usuarioPrueba == null)
		{
			usuarioPrueba = Funciones.altaUsuarioFirmanteSinTokenTodoContratado(driver, resultado);
		}
		else
		{
			Log.write("Se coge el usuario " + usuarioPrueba.getCodUsuario() + " para esta prueba");
			usuarioPrueba.setEstado(Constantes.ESTADO_NO_USAR);
			usuarioPrueba.guardarUsuarioEnCSV();
		}
		// Acceder a administraci�n de usuarios
		Utils.accederAdmUsuarios(driver);
		// Cambiar a frame
		WebElement frame = driver.findElement(By.xpath(Constantes.FramePrincipal));
		driver.switchTo().frame(frame);

		driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
		driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(usuarioPrueba.getCodUsuario());
		
		Utils.clickEnElVisible(driver, Constantes.BotonBuscar);
		Utils.esperarProcesandoPeticionMejorado(driver);
		
		Utils.esperaHastaAparece(driver, Constantes.ResultadosBusqueda, 20);

		// Validar que solo hay un elemento en la tabla
		String mensajeResultado = driver.findElement(By.xpath(Constantes.ResultadosBusqueda)).getText();
		Utils.vp(driver, resultado, "Resultado de b�squeda", Constantes.UnUsuarioTotales, mensajeResultado, "FALLO");
		// Validar estado usuario filtrado
		Utils.vp(driver, resultado, "Validar estado " + Constantes.ESTADO_ACTIVO, Constantes.ESTADO_ACTIVO, driver.findElement(By.xpath(Constantes.TablaUsuarios_EstadoPrimerUsuario)).getText(),
				"FALLO");
		// Seleccionar usuario filtrado
		Utils.clickEnElVisible(driver, Constantes.RadioSeleccionUsuarioFiltrado);
		Utils.clickEnElVisible(driver, Constantes.BotonEditar);
		// Validar t�tulo de la p�gina
		String titulo = Utils.devuelveTextoDelVisiblePrimero(driver, Constantes.TituloPantalla);
		Utils.vp(driver, resultado, "Validar titulo pagina modificaci�n usuario", "Modificaci�n de usuario " + usuarioPrueba.getNombreUsuario(), titulo, "FALLO");

		// Modificar nombre de usuario
		String NuevoNomUsuario = usuarioPrueba.getNombreUsuario() + "�������";
		driver.findElement(By.xpath(Constantes.CampoNombreUsuario)).clear();
		driver.findElement(By.xpath(Constantes.CampoNombreUsuario)).sendKeys(NuevoNomUsuario);
		// Guardar
		Utils.scrollAndClick(driver, By.xpath(Constantes.BotonContinuarModificar));

		if(Utils.estamosEnLatam())
		{
			if(Utils.isDisplayed(driver, Constantes.BotonContinuar, 3))
				Utils.clickEnElVisible(driver, Constantes.BotonContinuar);
		}
		
		aceptarTratamientoDeDatosDeUsuario(driver, resultado, true);

		// Validar el mensaje de Modificar datos del usuario
		Utils.vp(driver, resultado, "Mensaje Modificar usuario", true, driver.findElement(By.xpath(Constantes.DivMensajeOperacionPendiente)).getText().contains("Modificar datos del usuario"), "FALLO");
		
		// Expandir y validar el c�digo de usuario y el nuevo nombre a modificar
		if (Utils.isDisplayed(driver, Constantes.LinkDesplegar, 1))
			Utils.clickEnElVisible(driver, Constantes.LinkDesplegar);
		
		String textoDesplegado = Utils.devuelveTextoDelVisiblePrimero(driver, Constantes.DivTextoDesplegado);
		Utils.vp(driver, resultado, "Texto desplegable contiene el c�digo del usuario", true, textoDesplegado.indexOf(usuarioPrueba.getCodUsuario()) > -1, "FALLO");
		Utils.vp(driver, resultado, "Texto desplegable de tarea de validaci�n contiene el nuevo nombre del usuario", true, textoDesplegado.toUpperCase().indexOf(NuevoNomUsuario.toUpperCase()) > -1,
				"FALLO");
		// Acceder a consulta de usuario
		Utils.accederAdmUsuarios(driver);
		
		// cambiar a frame
		Utils.cambiarFramePrincipal(driver);
		driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
		driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(usuarioPrueba.getCodUsuario());
		Utils.clickEnElVisible(driver, Constantes.BotonBuscar);
		Utils.esperarProcesandoPeticionMejorado(driver);
		
		// Se accede a la consulta
		Utils.clickEnElVisible(driver, Constantes.RadioSeleccionUsuarioFiltrado);
		Utils.clickEnElVisible(driver, Constantes.BotonConsultar);
		
		// Se valida t�tulo - 02/12/15 - Aparece el nombre del perfil en vuelo
		titulo = Utils.devuelveTextoDelVisiblePrimero(driver, Constantes.TituloPantalla);
		Utils.vp(driver, resultado, "Validar titulo pagina consulta usuario. Usuario en vuelo: "+NuevoNomUsuario, true, titulo.contains(NuevoNomUsuario), "FALLO");
		
		// Se comprueba el nombre del perfil en vuelo y que el campo est� deshabilitado
		Utils.vp(driver, resultado, "Nombre usuario", NuevoNomUsuario, driver.findElement(By.xpath(Constantes.CampoNombreUsuario)).getAttribute("value"), "FALLO");
		Pattern patronClass = Pattern.compile(".*desactivado");
		String Campo = driver.findElement(By.xpath(Constantes.CampoNombreUsuario)).getAttribute("class");
		Utils.vp(driver, resultado, "Campo nombre usuario deshabilitado por pdte modificaci�n", true, (patronClass.matcher(Campo)).matches(), "FALLO");

		return usuarioPrueba;
	}


	// Crea un usuario en la aplicaci�n de tipo administrador solidario no firmante en estado Activo
	public static Usuario altaUsuarioActivo(WebDriver driver, Resultados resultado) throws Exception
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String usuarioPorDefecto = params.getValorPropiedad("usuarioPorDefecto");


		// Creamos un usuario Aleatorio para el alta
		Usuario UsuarioX = Funciones.crearUsuarioAleatorio("default", "Solidario-Indistinto", "NO");

		// acceder a administraci�n de usuarios
		Utils.accederAdmUsuarios(driver);

		// cambiar a frame
		Utils.esperarProcesandoPeticionMejorado(driver);
		WebElement frame = driver.findElement(By.xpath(Constantes.FramePrincipal));
		driver.switchTo().frame(frame);
		Utils.clickEnElVisible(driver, Constantes.BotonAccederAltaUsuario);
		Utils.esperarProcesandoPeticionMejorado(driver);
		
		Funciones.informarAlta(driver, UsuarioX);

//		Utils.clickEnElVisible(driver, Constantes.AceptarCondiciones);

		Utils.capturaIntermedia(driver, resultado, "Se informan datos en el alta de usuario");

		Utils.clickEnElVisible(driver, Constantes.BotonAltaUsuario);
		Utils.esperarProcesandoPeticionMejorado(driver);
		
		// Se valida el texto de aviso sobre dispositivo al finalizar el paso 1 del alta
		String textoDiv = driver.findElement(By.xpath(Constantes.DivAltaSinToken)).getText();
		String textoEsperado = Constantes.recuerdeAsignarDispositivo;
		Utils.vp(driver, resultado, "Texto del div sobre dispositivo", true, textoDiv.indexOf(textoEsperado) > -1, "FALLO");
		Utils.clickEnElVisible(driver, Constantes.BotonContinuar);
		
		aceptarTratamientoDeDatosDeUsuario(driver, resultado, true);

		Utils.esperarProcesandoPeticionDefaultContentVolviendoAPPal(driver);

		// Se valida que hay 3 opciones de perfilado
		List<WebElement> opcionesPerfilado = driver.findElements(By.xpath(Constantes.LabelOpcionesPerfilado));
		Utils.vp(driver, resultado, "Validar n�mero opciones perfilado", 3, opcionesPerfilado.size(), "FALLO");
		
		// Seleccionar copiar perfil, filtrar por el usuario ADM1, seleccionarlo y pulsar continuar
		Utils.clickEnElVisible(driver, Constantes.CheckCopiarPerfil);

		Utils.esperarProcesandoPeticionMejorado(driver);

		Funciones.buscarUsuarioYSeleccionarlo(driver, resultado, usuarioPorDefecto);

		Utils.esperarProcesandoPeticionMejorado(driver);

		Utils.capturaIntermedia(driver, resultado, "Se selecciona usuario por defecto para copiar perfil");

		Utils.clickEnElVisible(driver, Constantes.BotonContinuarAzul);

		Utils.esperarProcesandoPeticionMejorado(driver);

		Utils.validar(driver);

		// Se valida mensaje de operaci�n realizada correctamente
		Utils.validarOperacionOk(driver);

		// Se validan divs de izquierda y derecha con datos de alta de usuario
		Funciones.comprobarDIVSTrasElAlta(driver, resultado, UsuarioX, Constantes.literalAdministrador, UsuarioX.getTipoPoder());

		// Se valida literal sobre servicios y firmas en la p�gina
		Utils.vp(driver, resultado, "Aparece mensaje de Servicios y firmas asignados al usuario", true,
				driver.getPageSource().toLowerCase().contains(Constantes.MensajeServiciosFirmasAsignados.toLowerCase()), "FALLO");

		// Guardamos el usuario dado de alta y validado
		UsuarioX.setEstado(Constantes.ESTADO_NO_USAR);
		UsuarioX.guardarUsuarioEnCSV();
		Log.write("Se ha dado de alta el usuario " + UsuarioX.getCodUsuario() + " con estado " + Constantes.ESTADO_ACTIVO);
		return UsuarioX;

	}


	// Busca un usuario y selecciona el radio button. Sin importar el frame en el que te encuentres
	public static void buscarUsuarioYSeleccionarlo(WebDriver driver, Resultados resultado, Usuario usuarioPrueba) throws Exception
	{
		
		Utils.esperaHastaApareceSinSalir(driver, Constantes.CampoBusqueda, 7);

		if(!Utils.isDisplayed(driver, Constantes.CampoBusqueda, 7))
		{
			Utils.cambiarFramePrincipal(driver);
		}
		
		Utils.introducirTextoEnElVisible2(driver, Constantes.CampoBusqueda, usuarioPrueba.getCodUsuario());

		Utils.clickEnElVisible(driver, Constantes.BotonBuscar);
		
		Utils.esperarProcesandoPeticionMejorado(driver);

		Utils.esperarVisibleTiempo(driver, Constantes.ResultadosBusqueda, 20);

		// Validar que solo hay un elemento en la tabla
		
		//Validar que solo hay un elemento en la tabla
		String mensajeResultado=Utils.getDigitosCadenaFromString(driver.findElement(By.xpath(Constantes.ResultadosBusqueda)).getText());	  
		Utils.vp(driver, resultado, "Resultado de b�squeda", "1", mensajeResultado, "FALLO"); 
		if (!mensajeResultado.equalsIgnoreCase("1")){
			  throw new Exception("La b�squeda de usuarios no funciona correctamente. Se para el caso por precauci�n");
		}
		
//		String mensajeResultado = driver.findElement(By.xpath(Constantes.ResultadosBusqueda)).getText();
//		Utils.vp(driver, resultado, "Resultado de b�squeda contiene 1 usuarios totales", true, mensajeResultado.contains(Constantes.UnUsuarioTotales), "FALLO");
//		if(!mensajeResultado.contains(Constantes.UnUsuarioTotales))
//		{
//			throw new Exception("La b�squeda de usuarios no funciona correctamente. Se para el caso por precauci�n");
//		}

		

		// Seleccionar usuario filtrado
		Utils.clickEnElVisible(driver, Constantes.RadioSeleccionUsuarioFiltrado);
	}


	public static void buscarUsuarioYSeleccionarloValidar(WebDriver driver, Resultados resultado, Usuario usuarioPrueba) throws Exception
	{
		
		Utils.esperaHastaApareceSinSalir(driver, Constantes.CampoBusqueda, 7);

		if(!Utils.isDisplayed(driver, Constantes.CampoBusqueda, 7))
		{
			Utils.cambiarFramePrincipal(driver);
		}

		driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
		driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(usuarioPrueba.getCodUsuario());
		Utils.clickEnElVisible(driver, Constantes.BotonBuscar);

		// Seleccionar usuario filtrado
		int usuariosEncontrados = driver.findElements(By.xpath(Constantes.radioSeleccionUsuarioValidador)).size();

		if(usuariosEncontrados > 1)
			throw new Exception("La b�squeda de usuarios no funciona correctamente. Se para el caso por precauci�n");

		Utils.esperarProcesandoPeticionMejorado(driver);
		
		Utils.clickEnElVisible(driver, Constantes.radioSeleccionUsuarioValidador);
	}


	public static void buscarDosUsuariosYSeleccionarlos(WebDriver driver, Resultados resultado, Usuario usuarioPrueba, Usuario usuarioPrueba2) throws Exception
	{
		
		Utils.esperaHastaApareceSinSalir(driver, Constantes.CampoBusqueda, 7);
		
		if(!Utils.isDisplayed(driver, Constantes.CampoBusqueda, 7))
		{
			Utils.cambiarFramePrincipal(driver);
		}

		driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();

		Utils.clickEnElVisible(driver, Constantes.BotonAccederFiltrar);

		driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys("TC");
		Utils.clickEnElVisible(driver, Constantes.CheckFiltroTipoUsuarioTodos);
		Utils.clickEnElVisible(driver, Constantes.CheckFiltroTipoAdministrador);
		Thread.sleep(500);
		Utils.clickEnElVisible(driver, Constantes.checkFiltroPendienteValidar);
		Utils.clickEnElVisible(driver, Constantes.BotonFiltrar);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Utils.clickEnElVisible(driver,Constantes.BotonBuscar);

		Utils.esperarVisibleTiempo(driver, Constantes.ResultadosBusqueda, 20);

		String usuario1 = "//input[contains(@id,'seleccionarUsuario')][contains(@value,'" + usuarioPrueba.getCodUsuario() + "')]/following-sibling::label";
		String usuario2 = "//input[contains(@id,'seleccionarUsuario')][contains(@value,'" + usuarioPrueba2.getCodUsuario() + "')]/following-sibling::label";

		int contador = 0;
		boolean encontrados = false;

		while(!encontrados && contador < 250)
		{

			if(Utils.isDisplayed(driver, usuario1, 1) && Utils.isDisplayed(driver, usuario2, 1))
				encontrados = true;
			else
			{
				Utils.clickEnElVisible(driver, Constantes.LinkVerMasUsuarios);
				contador++;
			}
		}

		if(!encontrados)
			throw new Exception("No se encontraron los usuarios: " + usuarioPrueba.getCodUsuario() + " y: " + usuarioPrueba2.getCodUsuario());


		Utils.clickEnElVisible(driver, usuario1);
		Utils.clickEnElVisible(driver, usuario2);

	}


	public static void buscarUsuarioYSeleccionarloParaToken(WebDriver driver, Resultados resultado, Usuario usuarioPrueba) throws Exception
	{

		if(!Utils.isDisplayed(driver, Constantes.CampoBusqueda, 7))
		{
			Utils.cambiarFramePrincipal(driver);
		}

		driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
		driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(usuarioPrueba.getCodUsuario());
		Utils.clickEnElVisible(driver, Constantes.BotonBuscarDispositivo);
		
		Utils.esperarProcesandoPeticionMejorado(driver);

		Utils.esperarVisibleTiempo(driver, Constantes.ResultadosBusqueda, 20);
		Thread.sleep(1000);

		int numeroResultados = driver.findElements(By.xpath(Constantes.inputDispositivoConLista)).size();

		Utils.vp(driver, resultado, "Resultados al filtrar por usuario asignando token", 1, numeroResultados, "FALLO");

		if(numeroResultados > 1)
			throw new Exception("La b�squeda de usuarios para asignarles token no funciona correctamente. Se para el caso por precauci�n");

		// Seleccionar usuario filtrado
		Utils.clickEnElVisible(driver, Constantes.inputDispositivoConLista);

	}


	// Busca un usuario y selecciona el radio button. Sin importar el frame en el que te encuentres
	public static void buscarUsuarioYSeleccionarlo(WebDriver driver, Resultados resultado, String usuarioPrueba) throws Exception
	{


		if(!Utils.isDisplayed(driver, Constantes.CampoBusqueda, 7))
		{
			Utils.cambiarFramePrincipal(driver);
		}

		Utils.introducirTextoEnElVisible2(driver, Constantes.CampoBusqueda, usuarioPrueba);
		Utils.clickEnElVisible(driver, Constantes.BotonBuscar);
		
		Utils.esperarProcesandoPeticionMejorado(driver);

		Utils.esperarVisibleTiempo(driver, Constantes.ResultadosBusqueda, 20);

		// Validar que solo hay un elemento en la tabla
		String mensajeResultado = driver.findElement(By.xpath(Constantes.ResultadosBusqueda)).getText();
		Utils.vp(driver, resultado, "Resultado de b�squeda", Constantes.UnUsuarioTotales, mensajeResultado, "FALLO");

		// Seleccionar usuario filtrado
		Utils.clickEnElVisible(driver, Constantes.RadioSeleccionUsuarioFiltrado);
	}


	// Crea un usuario en la aplicaci�n de tipo administrador solidario no firmante y lo deja pendiente de activar
	public static Usuario altaUsuarioPteActivar(WebDriver driver, Resultados resultado) throws Exception
	{

		// Creamos un usuario Aleatorio para el alta
		Usuario UsuarioX = Funciones.crearUsuarioAleatorio("default", "Solidario-Indistinto", "NO");

		// acceder a administraci�n de usuarios
		Utils.accederAdmUsuarios(driver);

		// cambiar a frame
		WebElement frame = driver.findElement(By.xpath(Constantes.FramePrincipal));
		driver.switchTo().frame(frame);
		Utils.clickEnElVisible(driver, Constantes.BotonAccederAltaUsuario);

		Funciones.informarAlta(driver, UsuarioX);
//		Utils.clickEnElVisible(driver, Constantes.AceptarCondiciones);

		Utils.capturaIntermedia(driver, resultado, "Se informan datos en el alta de usuario");

		Utils.clickEnElVisible(driver, Constantes.BotonAltaUsuario);
		
		// Se valida el texto de aviso sobre dispositivo al finalizar el paso 1 del alta
		String textoDiv = driver.findElement(By.xpath(Constantes.DivAltaSinToken)).getText();
		String textoEsperado = Constantes.recuerdeAsignarDispositivo;
		Utils.vp(driver, resultado, "Texto del div sobre dispositivo", true, textoDiv.indexOf(textoEsperado) > -1, "FALLO");
		Utils.clickEnElVisible(driver, Constantes.BotonContinuar);
		
		aceptarTratamientoDeDatosDeUsuario(driver, resultado, true);
		
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Se valida que hay 3 opciones de perfilado
		List<WebElement> opcionesPerfilado = driver.findElements(By.xpath(Constantes.LabelOpcionesPerfilado));
		Utils.vp(driver, resultado, "Opciones de perfilado", 3, opcionesPerfilado.size(), "FALLO");

		// Seleccionar copiar perfil, filtrar por el usuario por defecto, seleccionarlo y pulsar continuar
		Utils.clickEnElVisible(driver, Constantes.CheckCopiarPerfil);
		Utils.esperarProcesandoPeticionMejorado(driver);
		
		driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosFijos");
		Usuario UsuarioPrueba = Usuario.obtenerUsuarioPorReferenciaCodigo(rutaCSV, params.getValorPropiedad("referenciaPorDefecto"), params.getValorPropiedad("usuarioPorDefecto"));
		driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(UsuarioPrueba.getCodUsuario());
		Utils.clickEnElVisible(driver, Constantes.BotonBuscar);
		Utils.esperarProcesandoPeticionMejorado(driver);
		Utils.clickEnElVisible(driver, Constantes.TablaUsuarios_InputPrimerUsuario);
		
		if(!driver.findElement(By.xpath(Constantes.TablaUsuarios_InputPrimerUsuario)).isSelected())
			Utils.clickEnElVisiblePrimero(driver, Constantes.TablaUsuarios_InputPrimerUsuario);

		Utils.capturaIntermedia(driver, resultado, "Se selecciona usuario por defecto para copiar perfil");

		Utils.clickEnElVisible(driver, Constantes.BotonContinuarAzul);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Se valida el texto
		textoEsperado = Constantes.conExito;
		Utils.esperaHastaAparece(driver, Constantes.DivMensajeOk, 30);
		String textoActual = driver.findElement(By.xpath(Constantes.DivMensajeOk)).getText();
		Utils.vp(driver, resultado, "Mensaje servicios y firmas seleccionados", true, textoActual.indexOf(textoEsperado) > -1, "FALLO");
		Utils.clickEnElVisible(driver, Constantes.LinkValidarOtroMomento);

		Utils.esperarProcesandoPeticionMejorado(driver);

		// Se validan divs de izquierda y derecha con datos de alta de usuario
		// String
		// cadenaIzquierdaEsperada="C�digo de usuario:"+UsuarioAlta.getCodUsuario()+Character.toString((char)10)+"Nombre completo:"+UsuarioAlta.getNombreUsuario()+Character.toString((char)10)+"Email:"+UsuarioAlta.getMail()+Character.toString((char)10)+"Tipo de documento de identidad:"+UsuarioAlta.getTipoDoc()+Character.toString((char)10)+"N�mero de documento de identidad:"+UsuarioAlta.getNumDoc();
//		  String cadenaIzquierda=driver.findElement(By.xpath(Constantes.DivColumnaIzquierda)).getText();
//		  Utils.vp(driver, resultado, "Contenido parte izquierda datos alta usuario. C�digo del Usuario.", true, cadenaIzquierda.indexOf(UsuarioX.getCodUsuario())>-1,"FALLO");
//		  Utils.vp(driver, resultado, "Contenido parte izquierda datos alta usuario. Nombre del Usuario.", true, cadenaIzquierda.indexOf(UsuarioX.getNombreUsuario())>-1,"FALLO");
//		  Utils.vp(driver, resultado, "Contenido parte izquierda datos alta usuario. Email del Usuario.", true, cadenaIzquierda.indexOf(UsuarioX.getMail())>-1,"FALLO");
//		  Utils.vp(driver, resultado, "Contenido parte izquierda datos alta usuario. Tipo de Documento del Usuario.", true, cadenaIzquierda.indexOf(UsuarioX.getTipoDoc())>-1,"FALLO");
//		  Utils.vp(driver, resultado, "Contenido parte izquierda datos alta usuario. N�mero de Documento del Usuario.", true, cadenaIzquierda.indexOf(UsuarioX.getNumDoc())>-1,"FALLO");
//
//		  //String cadenaDerechaEsperada="Tipo de usuario:Administrador - "+UsuarioAlta.getTipoPoder()+Character.toString((char)10)+"Firma de �rdenes:No Firmante"+Character.toString((char)10)+"Tel�fono:0034"+UsuarioAlta.getTelefono();
//		  String cadenaDerecha=driver.findElement(By.xpath(Constantes.DivColumnaDerecha)).getText();
//		  Utils.vp(driver, resultado, "Contenido parte derecha datos alta usuario. Tipo de Usuario.", true, cadenaDerecha.indexOf(UsuarioX.getTipoPoder())>-1,"FALLO");
//		  Utils.vp(driver, resultado, "Contenido parte derecha datos alta usuario. Firma de �rdenes.", true, cadenaDerecha.indexOf("No firmante")>-1,"FALLO");
//		  Utils.vp(driver, resultado, "Contenido parte derecha datos alta usuario. Tel�fono.", true, cadenaDerecha.indexOf(UsuarioX.getTelefono())>-1,"FALLO");

		Funciones.comprobarDIVSTrasElAlta(driver, resultado, UsuarioX, "", "");

		// Se valida literal sobre servicios y firmas en la p�gina
		Utils.vp(driver, resultado, "Mensaje de Servicios y firmas asignados al usuario no presente", true,
				driver.getPageSource().toLowerCase().contains(Constantes.MensajeServiciosFirmasAsignados.toLowerCase()), "FALLO");

		// Guardamos el usuario dado de alta y pendiente de validar
		UsuarioX.setEstado(Constantes.ESTADO_NO_USAR);
		UsuarioX.guardarUsuarioEnCSV();
		Log.write("Se ha dado de alta el usuario " + UsuarioX.getCodUsuario() + " con estado " + Constantes.ESTADO_PDTEACTIVAR);

		return UsuarioX;

	}


	public static void eliminarUsuario(WebDriver driver, Usuario usuario, Resultados resultado) throws Exception
	{
		String CodUsuario = usuario.getCodUsuario();
		
		// acceder a administraci�n de usuarios
		Utils.accederAdmUsuarios(driver);
		
		// cambiar a frame
		Utils.cambiarFramePrincipal(driver);
			
		Funciones.buscarUsuarioYSeleccionarlo(driver, resultado, CodUsuario);

		// Validar estado usuario filtrado
		if(!Utils.estamosEnLatam())
			Utils.vp(driver, resultado, "Validar estado " + Constantes.ESTADO_ACTIVO, Constantes.ESTADO_ACTIVO, driver.findElement(By.xpath(Constantes.TablaUsuarios_EstadoPrimerUsuario)).getText(),
					"FALLO");

		// Seleccionar usuario filtrado
		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonEliminar);
		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonEliminarConf);
		Utils.esperarProcesandoPeticionMejorado(driver);
		String mensajeOperacion = driver.findElement(By.xpath(Constantes.DivMensajeOk)).getText();
		Utils.vp(driver, resultado, "Mensaje operaci�n realizada correctamente", true, mensajeOperacion.indexOf(CodUsuario + " " + Constantes.haSidoEliminadoCorrectamente) > -1, "FALLO");
		Utils.clickEnElVisible(driver, Constantes.LinkValidarOperacion);

		String mensajeMostrado = driver.findElement(By.xpath(Constantes.DivMensajeOperacionPendiente)).getText().toUpperCase();

		// Se valida el mensaje de Pendiente de baja
		Utils.vp(driver, resultado, "Mensaje Pendiente baja", true,
				mensajeMostrado.indexOf(Constantes.MensajePendienteDeBaja.toUpperCase()) > -1 || mensajeMostrado.toUpperCase().indexOf(Constantes.MensajePendienteBaja.toUpperCase()) > -1, "FALLO");

		if (Utils.isDisplayed(driver, Constantes.LinkDesplegar, 1))
			Utils.clickEnElVisiblePrimero(driver, Constantes.LinkDesplegar);
		
		// Se valida el c�digo, nombre y documento del usuario a eliminar
		String textoDesplegado = Utils.devuelveTextoDelVisiblePrimero(driver, Constantes.DivTextoDesplegado);
		Utils.vp(driver, resultado, "Texto desplegable contiene c�digo del usuario a eliminar", true, textoDesplegado.indexOf(CodUsuario) > -1, "FALLO");
		Utils.vp(driver, resultado, "Texto desplegable contiene nombre del usuario a eliminar", true, textoDesplegado.indexOf(usuario.getNombreUsuario()) > -1, "FALLO");
		Utils.vp(driver, resultado, "Texto desplegable contiene documento del usuario a eliminar", true, textoDesplegado.indexOf(usuario.getNumDoc()) > -1, "FALLO");

		Utils.capturaIntermedia(driver, resultado, "Usuario pendiente de baja");

		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonValidar);
		Utils.validar(driver);
		Utils.validarOperacionOk(driver);

		// Validar que el usuario no existe
		Utils.vp(driver, resultado, "Mensaje No hay usuarios", true, Utils.existeElementoTiempo(driver, By.xpath(Constantes.TextoNoHayUsuarios), 5), "FALLO");
	}


	public static void eliminarUsuarioIE(WebDriver driver, Usuario usuario, Resultados resultado) throws Exception
	{
		String CodUsuario = usuario.getCodUsuario();
		// acceder a administraci�n de usuarios
		Utils.accederAdmUsuarios(driver);
		// cambiar a frame
		WebElement frame = driver.findElement(By.xpath(Constantes.FramePrincipal));
		driver.switchTo().frame(frame);
		driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
		driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(CodUsuario);
		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonBuscar);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Validar estado usuario filtrado
		if(!Utils.estamosEnLatam())
			Utils.vp(driver, resultado, "Validar estado " + Constantes.ESTADO_ACTIVO, Constantes.ESTADO_ACTIVO, driver.findElement(By.xpath(Constantes.TablaUsuarios_EstadoPrimerUsuario)).getText(),
					"FALLO");

		// Seleccionar usuario filtrado
		Utils.clickEnElVisible(driver, Constantes.RadioSeleccionUsuarioFiltrado);
		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonEliminar);
		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonEliminarConf);
		Utils.esperarProcesandoPeticionMejorado(driver);
		String mensajeOperacion = driver.findElement(By.xpath(Constantes.DivMensajeOk)).getText();
		Utils.vp(driver, resultado, "Mensaje operaci�n realizada correctamente", true, mensajeOperacion.indexOf(CodUsuario + " " + Constantes.haSidoEliminadoCorrectamente) > -1, "FALLO");
		Utils.clickEnElVisible(driver, Constantes.LinkValidarOperacion);

		String mensajeMostrado = driver.findElement(By.xpath(Constantes.DivMensajeOperacionPendiente)).getText().toUpperCase();

		// Se valida el mensaje de Pendiente de baja
		Utils.vp(driver, resultado, "Mensaje Pendiente baja", true,
				mensajeMostrado.indexOf(Constantes.MensajePendienteDeBaja.toUpperCase()) > -1 || mensajeMostrado.toUpperCase().indexOf(Constantes.MensajePendienteBaja.toUpperCase()) > -1, "FALLO");

		Utils.clickEnElVisible(driver, "//a[@class='enlaceDespliegue']/img");

		// driver.findElement(By.xpath(Constantes.LinkDesplegar)).click();
		// Se valida el c�digo, nombre y documento del usuario a eliminar
		String textoDesplegado = Utils.devuelveTextoDelVisiblePrimero(driver, Constantes.DivTextoDesplegado);
		Utils.vp(driver, resultado, "Texto desplegable contiene c�digo del usuario a eliminar", true, textoDesplegado.indexOf(CodUsuario) > -1, "FALLO");
		Utils.vp(driver, resultado, "Texto desplegable contiene nombre del usuario a eliminar", true, textoDesplegado.indexOf(usuario.getNombreUsuario()) > -1, "FALLO");
		Utils.vp(driver, resultado, "Texto desplegable contiene documento del usuario a eliminar", true, textoDesplegado.indexOf(usuario.getNumDoc()) > -1, "FALLO");

		Utils.capturaIntermedia(driver, resultado, "Usuario pendiente de baja");

		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonValidar);
		Utils.validar(driver);
		Utils.validarOperacionOk(driver);

		// Validar que el usuario no existe
		Utils.vp(driver, resultado, "Mensaje No hay usuarios", true, Utils.existeElementoTiempo(driver, By.xpath(Constantes.TextoNoHayUsuarios), 5), "FALLO");
	}


	public static void eliminarUsuarioPdteBaja(WebDriver driver, Usuario usuario, Resultados resultado) throws Exception
	{
		String CodUsuario = usuario.getCodUsuario();

		// acceder a administraci�n de usuarios
		Utils.accederAdmUsuarios(driver);
		
		// cambiar a frame
		Utils.cambiarFramePrincipal(driver);
		
		Funciones.buscarUsuarioYSeleccionarlo(driver, resultado, CodUsuario);
		Utils.vp(driver, resultado, "Validar estado " + Constantes.ESTADO_ACTIVO, Constantes.ESTADO_ACTIVO, driver.findElement(By.xpath(Constantes.TablaUsuarios_EstadoPrimerUsuario)).getText(),
				"FALLO");
		
		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonEliminar);
		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonEliminarConf);
		String mensajeOperacion = driver.findElement(By.xpath(Constantes.DivMensajeOk)).getText();
		Utils.vp(driver, resultado, "Mensaje operaci�n realizada correctamente", true, mensajeOperacion.indexOf("usuario " + CodUsuario + " ha sido eliminado correctamente") > -1, "FALLO");

		Utils.clickEnElVisible(driver, Constantes.LinkValidarOperacion);

		String mensajeMostrado = driver.findElement(By.xpath(Constantes.DivMensajeOperacionPendiente)).getText().toUpperCase();

		// Se valida el mensaje de Pendiente de baja
		Utils.vp(driver, resultado, "Mensaje Pendiente baja", true,
				mensajeMostrado.indexOf(Constantes.MensajePendienteDeBaja.toUpperCase()) > -1 || mensajeMostrado.toUpperCase().indexOf(Constantes.MensajePendienteBaja.toUpperCase()) > -1, "FALLO");

	}


	public static void seleccionarDispositivo(WebDriver driver, Usuario usuario) throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String pais = params.getValorPropiedad("pais");

		if(pais.equalsIgnoreCase("Argentina"))
			Funciones.seleccionarDispositivoArgentina(driver, usuario);
		else
			Funciones.seleccionarDispositivoDefecto(driver, usuario);
	}


	public static void seleccionarDispositivoDefecto(WebDriver driver, Usuario usuario) throws Exception
	{
		String dispositivo = usuario.getDispositivo();
		if(!dispositivo.equalsIgnoreCase(""))
		{
			if(dispositivo.equalsIgnoreCase("F�sico"))
			{
				if(Utils.estamosEnLatam())
				{
					Utils.clickEnElVisible(driver, Constantes.TokenHardwareLATAM);
					// Tema: Por qu� seleccionar siempre un dispositivo??? Habr�a que liberarlo antes!
					// Utils.seleccionarComboNOContieneXPath(driver, Constantes.comboDispositivoHardware, "dispositivo");
				}
				else
					Utils.clickEnElVisiblePrimero(driver, Constantes.RadioDispositivoFisico);
			}
			else if(dispositivo.equalsIgnoreCase("M�vil"))
			{
				if(Utils.estamosEnLatam())
				{
					Utils.clickEnElVisible(driver, Constantes.TokenSoftwareLATAM);
					Utils.seleccionarComboNOContieneXPath(driver, Constantes.comboDispositivoSoftware, "tel�fono");
				}


				else
					Utils.clickEnElVisiblePrimero(driver, Constantes.RadioDispositivoMovil);
			}
			else
				Log.write("El tipo de dispositivo asociado al usuario no es ni F�sico ni M�vil. Revisar el c�digo.");
		}
	}


	public static void seleccionarDispositivoArgentina(WebDriver driver, Usuario usuario) throws Exception
	{

		// En Argentina es s�lo TOKEN HARDWARE. No sale nada de esto

//		  String dispositivo = usuario.getDispositivo();
//		  if (!dispositivo.equalsIgnoreCase("")){
//			  if (dispositivo.equalsIgnoreCase("F�sico")) {
//				  if (Utils.estamosEnLatam()) {
//					  Utils.clickEnElVisible(driver, Constantes.TokenHardwareLATAM);
//					  Utils.seleccionarComboNOContieneXPath(driver, Constantes.comboDispositivoHardware, "dispositivo");
//				  }
//				  else driver.findElement(By.xpath(Constantes.RadioDispositivoFisico)).click();
//			  }
//			  else if (dispositivo.equalsIgnoreCase("M�vil")) {
//				  if (Utils.estamosEnLatam()) {
//					  Utils.clickEnElVisible(driver, Constantes.TokenSoftwareLATAM);
//					  Utils.seleccionarComboNOContieneXPath(driver, Constantes.comboDispositivoSoftware, "tel�fono");  
//				  }
//				  
//
//				  else driver.findElement(By.xpath(Constantes.RadioDispositivoMovil)).click();
//			  }
//			  else Log.write("El tipo de dispositivo asociado al usuario no es ni F�sico ni M�vil. Revisar el c�digo.");
//		  }
	}


	public static void clickBotonContinuarAzul(WebDriver driver) throws Exception
	{
		// Bot�n Continuar
		Utils.clickEnElVisible(driver, Constantes.BotonContinuarAzul);
		Utils.esperarProcesandoPeticionMejorado(driver);
	}


	// Tras el alta, se comprueban ciertas cosas fijas del usuario creado, m�s dos opcionales
	public static void comprobarDIVSTrasElAlta(WebDriver driver, Resultados resultado, Usuario usuario, String extra1, String extra2) throws Exception
	{
		Utils.capturaIntermedia(driver, resultado, "Datos del alta de usuario realizada");

		String cadenaIzquierda = driver.findElement(By.xpath(Constantes.DivColumnaIzquierda)).getText().toLowerCase();
		String cadenaDerecha = driver.findElement(By.xpath(Constantes.DivColumnaDerecha)).getText().toLowerCase();
		String codigoUsuario = usuario.getCodUsuario().toLowerCase();
		String nombreUsuario = usuario.getNombreUsuario().toLowerCase();
		String telefono = usuario.getTelefono().toLowerCase();
		String tipoPoder = usuario.getTipoPoder().toLowerCase();

		extra1 = extra1.toLowerCase();
		extra2 = extra2.toLowerCase();

		Utils.vpWarning(driver, resultado, "C�digo del usuario en el resumen del alta creada", true, cadenaIzquierda.indexOf(codigoUsuario) > -1 || cadenaDerecha.indexOf(codigoUsuario) > -1, "FALLO");
		Utils.vpWarning(driver, resultado, "Nombre del usuario en el resumen del alta creada", true, cadenaIzquierda.indexOf(nombreUsuario) > -1 || cadenaDerecha.indexOf(nombreUsuario) > -1, "FALLO");
		Utils.vpWarning(driver, resultado, "Tel�fono del usuario en el resumen del alta creada", true, cadenaIzquierda.indexOf(telefono) > -1 || cadenaDerecha.indexOf(telefono) > -1, "FALLO");

		if(tipoPoder.equalsIgnoreCase("no"))
			Utils.vpWarning(driver, resultado, "Tipo de Poder del usuario en el resumen del alta creada", true,
					cadenaIzquierda.indexOf(Constantes.literalSinPoderes) > -1 || cadenaDerecha.indexOf(Constantes.literalSinPoderes) > -1, "FALLO");
		else
			Utils.vpWarning(driver, resultado, "Tipo de Poder del usuario en el resumen del alta creada", true, cadenaIzquierda.indexOf(tipoPoder) > -1 || cadenaDerecha.indexOf(tipoPoder) > -1,
					"FALLO");

		if(!extra1.equals(""))
			Utils.vpWarning(driver, resultado, "Valor: " + extra1 + " en el resumen del alta creada", true, cadenaIzquierda.indexOf(extra1) > -1 || cadenaDerecha.indexOf(extra1) > -1, "FALLO");
		if(!extra2.equals(""))
			Utils.vpWarning(driver, resultado, "Valor: " + extra2 + " en el resumen del alta creada", true, cadenaIzquierda.indexOf(extra2) > -1 || cadenaDerecha.indexOf(extra2) > -1, "FALLO");

	}


	// Alta gen�rica de un administrador no firmante al que le pasas el tipo de Poder: Puede ser "Sin poderes"
	public static Usuario altaAdministrador(WebDriver driver, Resultados resultado, String tipoPoder) throws Exception
	{

		// Creamos un usuario Aleatorio para el alta
		Usuario UsuarioAlta = Funciones.crearUsuarioAleatorio("default", tipoPoder, "NO");


		// acceder a administraci�n de usuarios
		Utils.accederAdmUsuarios(driver);

		// cambiar a frame
		Utils.esperarProcesandoPeticionMejorado(driver);
		WebElement frame = driver.findElement(By.xpath(Constantes.FramePrincipal));
		driver.switchTo().frame(frame);
		Utils.clickEnElVisible(driver, Constantes.BotonAccederAltaUsuario);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Informamos el alta
		Funciones.informarAlta(driver, UsuarioAlta);

//		if(Utils.isDisplayed(driver, Constantes.AceptarCondiciones, 5))
//			Utils.clickEnElVisiblePrimero(driver, Constantes.AceptarCondiciones);

		Utils.capturaIntermedia(driver, resultado, "Se informan datos en el alta de usuario");

		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonAltaUsuario);

		if(Utils.isDisplayed(driver, Constantes.BotonContinuar, 5))
			Utils.clickEnElVisiblePrimero(driver, Constantes.BotonContinuar);
		
		aceptarTratamientoDeDatosDeUsuario(driver, resultado, true);

		Utils.esperarProcesandoPeticionMejorado(driver);

		// Se valida que hay 3 opciones de perfilado
		List<WebElement> opcionesPerfilado = driver.findElements(By.xpath(Constantes.LabelOpcionesPerfilado));
		Utils.vp(driver, resultado, "Validar n�mero opciones perfilado", 3, opcionesPerfilado.size(), "FALLO");

		// Seleccionar Todo Contratado
		Utils.clickEnElVisiblePrimero(driver, Constantes.CheckTodoContratado);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Validamos que sale el DIV de todo contratado
		Utils.vpWarning(driver, resultado, "T�tulo Asignar Todo lo contratado", true, Utils.existeElementoTiempo(driver, By.xpath(Constantes.TituloTodoContratado), 15), "FALLO");

		// Bot�n Continuar Azul
		Funciones.clickBotonContinuarAzul(driver);

		Utils.validar(driver);

		// Se valida mensaje de operaci�n realizada correctamente
		Utils.validarOperacionOk(driver);


		// Se validan divs de izquierda y derecha con datos de alta de usuario
		Funciones.comprobarDIVSTrasElAlta(driver, resultado, UsuarioAlta, "Usuario", "");

		// Se valida literal sobre servicios y firmas en la p�gina
		Utils.vp(driver, resultado, "Aparece mensaje de Servicios y firmas asignados al usuario", true,
				driver.getPageSource().toLowerCase().contains(Constantes.MensajeServiciosFirmasAsignados.toLowerCase()), "FALLO");

		// Guardamos el usuario dado de alta y validado
		UsuarioAlta.setEstado(Constantes.ESTADO_NO_USAR);
		UsuarioAlta.guardarUsuarioEnCSV();
		Log.write("Se ha dado de alta el usuario " + UsuarioAlta.getCodUsuario() + " con estado " + Constantes.ESTADO_ACTIVO);

		return UsuarioAlta;

	}


	public static Usuario altaAdministradorTipo(WebDriver driver, Resultados resultado, String tipoPoder, String tipoFirma) throws Exception
	{

		// Creamos un usuario Aleatorio para el alta
		Usuario UsuarioAlta = Funciones.crearUsuarioAleatorio("default", tipoPoder, tipoFirma);


		// acceder a administraci�n de usuarios
		Utils.accederAdmUsuarios(driver);

		// cambiar a frame
		Utils.esperarProcesandoPeticionMejorado(driver);
		WebElement frame = driver.findElement(By.xpath(Constantes.FramePrincipal));
		driver.switchTo().frame(frame);
		Utils.clickEnElVisible(driver, Constantes.BotonAccederAltaUsuario);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Informamos el alta
		Funciones.informarAlta(driver, UsuarioAlta);

//		if(Utils.isDisplayed(driver, Constantes.AceptarCondiciones, 5))
//			Utils.clickEnElVisiblePrimero(driver, Constantes.AceptarCondiciones);

		Utils.capturaIntermedia(driver, resultado, "Se informan datos en el alta de usuario");

		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonAltaUsuario);

		if(Utils.isDisplayed(driver, Constantes.BotonContinuar, 5))
			Utils.clickEnElVisiblePrimero(driver, Constantes.BotonContinuar);
		
		aceptarTratamientoDeDatosDeUsuario(driver, resultado, true);

		Utils.esperarProcesandoPeticionMejorado(driver);

		// Se valida que hay 3 opciones de perfilado
		List<WebElement> opcionesPerfilado = driver.findElements(By.xpath(Constantes.LabelOpcionesPerfilado));
		Utils.vpWarning(driver, resultado, "Validar n�mero opciones perfilado", 3, opcionesPerfilado.size(), "FALLO");

		// Seleccionar Todo Contratado
		Utils.clickEnElVisiblePrimero(driver, Constantes.CheckTodoContratado);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Validamos que sale el DIV de todo contratado
		Utils.vpWarning(driver, resultado, "T�tulo Asignar Todo lo contratado", true, Utils.existeElementoTiempo(driver, By.xpath(Constantes.TituloTodoContratado), 15), "FALLO");

		// Bot�n Continuar Azul
		Funciones.clickBotonContinuarAzul(driver);

		Utils.validar(driver);

		// Se valida mensaje de operaci�n realizada correctamente
		Utils.validarOperacionOk(driver);


		// Se validan divs de izquierda y derecha con datos de alta de usuario
		Funciones.comprobarDIVSTrasElAlta(driver, resultado, UsuarioAlta, "Usuario", "");

		// Se valida literal sobre servicios y firmas en la p�gina
		Utils.vp(driver, resultado, "Aparece mensaje de Servicios y firmas asignados al usuario", true,
				driver.getPageSource().toLowerCase().contains(Constantes.MensajeServiciosFirmasAsignados.toLowerCase()), "FALLO");

		// Guardamos el usuario dado de alta y validado
		UsuarioAlta.setEstado(Constantes.ESTADO_NO_USAR);
		UsuarioAlta.guardarUsuarioEnCSV();
		Log.write("Se ha dado de alta el usuario " + UsuarioAlta.getCodUsuario() + " con estado " + Constantes.ESTADO_ACTIVO);

		return UsuarioAlta;

	}


	// La pantalla que da la bienvenida al nuevo usuario y hay que pulsar HOME
	public static void pantallaBienvenidaUsuarioNuevo(WebDriver driver) throws Exception
	{
		
		if (Utils.isDisplayed(driver, Constantes.divCerrarVentanaModal, 1))
			Utils.clickEnElVisiblePrimero(driver, Constantes.divCerrarVentanaModal);

		WebElement frame;

		try
		{
			frame = driver.findElement(By.xpath(Constantes.FrameCampana));
			driver.switchTo().frame(frame);
		}
		catch(Exception e1)
		{

		}

		Utils.esperarVisibleTiempoSinSalir(driver, Constantes.DivBienvenida, 1);

		if(Utils.isDisplayed(driver, Constantes.LinkHome, 0))
			Utils.clickEnElVisible(driver, Constantes.LinkHome);
		
		if (Utils.isDisplayed(driver, Constantes.divCerrarVentanaModal, 0))
			Utils.clickEnElVisiblePrimero(driver, Constantes.divCerrarVentanaModal);
		
		//if (Utils.isDisplayedSinBefore(driver, Constantes.continuarCampania,0))
			//Atomic.click(driver, Constantes.continuarCampania);
		
		Utils.aceptarCampaniaSiSale(driver);
		
		Utils.esperarVisibleTiempo(driver, Constantes.BotonDesconectar, 10);
	}


	public static void recorrerFrames(WebDriver driver) throws Exception
	{
		final List<WebElement> iframes = driver.findElements(By.tagName("iframe"));
		for(WebElement iframe : iframes)
		{
			try
			{
				Log.write(iframe.getAttribute("name"));
				driver.switchTo().frame(iframe);
				Log.write(Utils.isDisplayed(driver, Constantes.ActivarTokenOtroMomento, 3));
				driver.switchTo().defaultContent();
			}
			catch(Exception e)
			{
				// TODO: handle exception
			}
		}
	}


	// Crea un usuario en la aplicaci�n no administrador y firmante en estado Activo
	public static Usuario altaUsuarioNoAdminTokenFisicoAsignado(WebDriver driver, Resultados resultado, Token token) throws Exception
	{

		// Creamos un usuario Aleatorio para el alta
		Usuario UsuarioAlta = Funciones.crearUsuarioAleatorio("default", "NO", "Apoderado");

		// Como es con Token F�sico, se lo asignamos
		UsuarioAlta.setDispositivo("F�sico");

		// acceder a administraci�n de usuarios
		Utils.accederAdmUsuarios(driver);

		// cambiar a frame
		Utils.esperarProcesandoPeticionMejorado(driver);
		Utils.cambiarFramePrincipal(driver);
		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonAccederAltaUsuario);

		// Informamos el alta
		Funciones.informarAlta(driver, UsuarioAlta);

		// Al tener dispositivo, se debe marcar su radio button asociado
		Funciones.seleccionarDispositivo(driver, UsuarioAlta);

		// Se selecciona el dispositivo f�sico
		if(Utils.estamosEnLatam())
			Utils.seleccionarComboContieneXPath(driver, Constantes.ComboDispositivoLATAM, token.getNumero());
		else
			Utils.seleccionarComboContieneXPath(driver, Constantes.ComboDispositivo, token.getNumero().replace("-", "").replace(" ", ""));

//		Utils.clickEnElVisiblePrimero(driver, Constantes.AceptarCondiciones);

		Utils.capturaIntermedia(driver, resultado, "Se informan datos en el alta de usuario");

		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonAltaUsuario);

		if(Utils.isDisplayed(driver, Constantes.BotonContinuar, 3))
			Utils.clickEnElVisible(driver, Constantes.BotonContinuar);

		
		aceptarTratamientoDeDatosDeUsuario(driver, resultado, true);
		
		Utils.esperarProcesandoPeticionMejorado(driver);
				
		// Se valida que hay 3 opciones de perfilado
		List<WebElement> opcionesPerfilado = driver.findElements(By.xpath(Constantes.LabelOpcionesPerfilado));
		Utils.vp(driver, resultado, "Validar n�mero opciones perfilado", 3, opcionesPerfilado.size(), "FALLO");

		// Seleccionar Todo Contratado
		Utils.clickEnElVisiblePrimero(driver, Constantes.CheckTodoContratado);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Validamos que sale el DIV de todo contratado
		Utils.vpWarning(driver, resultado, "T�tulo Asignar Todo lo contratado", true, Utils.existeElementoTiempo(driver, By.xpath(Constantes.TituloTodoContratado), 15), "FALLO");

		Utils.capturaIntermedia(driver, resultado, "Se selecciona asignar todos los servicios contratados en el alta de usuario");

		// Bot�n Continuar Azul
		Funciones.clickBotonContinuarAzul(driver);

		Utils.validar(driver);

		// Se valida mensaje de operaci�n realizada correctamente
		Utils.validarOperacionOk(driver);


		// Se validan divs de izquierda y derecha con datos de alta de usuario
		Funciones.comprobarDIVSTrasElAlta(driver, resultado, UsuarioAlta, "Usuario", "");

		// Se valida literal sobre servicios y firmas en la p�gina
		Utils.vp(driver, resultado, "Aparece mensaje de Servicios y firmas asignados al usuario", true,
				driver.getPageSource().toLowerCase().contains(Constantes.MensajeServiciosFirmasAsignados.toLowerCase()), "FALLO");

		// Guardamos el usuario dado de alta y validado
		UsuarioAlta.setEstado(Constantes.ESTADO_NO_USAR);
		UsuarioAlta.guardarUsuarioEnCSV();
		Log.write("Se ha dado de alta el usuario " + UsuarioAlta.getCodUsuario() + " con estado " + Constantes.ESTADO_ACTIVO);

		return UsuarioAlta;

	}


	// Crea un usuario en la aplicaci�n no administrador y firmante en estado Activo
	public static Usuario altaUsuarioFirmanteSinTokenTodoContratado(WebDriver driver, Resultados resultado) throws Exception
	{

		// Creamos un usuario Aleatorio para el alta
		Usuario UsuarioAlta = Funciones.crearUsuarioAleatorio("default", "NO", "Apoderado");

		// Lo damos de alta sin token
		UsuarioAlta.setDispositivo("NO");

		// acceder a administraci�n de usuarios
		Utils.accederAdmUsuarios(driver);

		// cambiar a frame
		Utils.esperarProcesandoPeticionMejorado(driver);
		Utils.cambiarFramePrincipal(driver);
		Utils.clickEnElVisible(driver, Constantes.BotonAccederAltaUsuario);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Informamos el alta
		Funciones.informarAlta(driver, UsuarioAlta);

//		Utils.clickEnElVisible(driver, Constantes.AceptarCondiciones);

		Utils.capturaIntermedia(driver, resultado, "Se informan datos en el alta de usuario");

		Utils.clickEnElVisible(driver, Constantes.BotonAltaUsuario);

		// Se valida el texto de aviso sobre dispositivo al finalizar el paso 1 del alta
		Utils.esperaHastaApareceSinSalir(driver, Constantes.DivAltaSinToken, 5);
		String textoDiv = driver.findElement(By.xpath(Constantes.DivAltaSinToken)).getText();
		String textoEsperado = Constantes.recuerdeAsignarDispositivo;
		Utils.vp(driver, resultado, "Texto del div sobre dispositivo", true, textoDiv.indexOf(textoEsperado) > -1, "FALLO");
		Utils.clickEnElVisible(driver, Constantes.BotonContinuar);

		aceptarTratamientoDeDatosDeUsuario(driver, resultado, true);
		
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Se valida que hay 3 opciones de perfilado
		List<WebElement> opcionesPerfilado = driver.findElements(By.xpath(Constantes.LabelOpcionesPerfilado));
		Utils.vp(driver, resultado, "Validar n�mero opciones perfilado", 3, opcionesPerfilado.size(), "FALLO");

		// Seleccionar Todo Contratado
		Utils.clickEnElVisible(driver, Constantes.CheckTodoContratado);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Validamos que sale el DIV de todo contratado
		Utils.vpWarning(driver, resultado, "T�tulo Asignar Todo lo contratado", true, Utils.existeElementoTiempo(driver, By.xpath(Constantes.TituloTodoContratado), 15), "FALLO");

		Utils.capturaIntermedia(driver, resultado, "Se selecciona asignar todos los servicios contratados en el alta de usuario");

		// Bot�n Continuar Azul
		Funciones.clickBotonContinuarAzul(driver);

		Utils.validar(driver);

		// Se valida mensaje de operaci�n realizada correctamente
		Utils.validarOperacionOk(driver);


		// Se validan divs de izquierda y derecha con datos de alta de usuario
		Funciones.comprobarDIVSTrasElAlta(driver, resultado, UsuarioAlta, Constantes.literalUsuario, "");

		// Se valida literal sobre servicios y firmas en la p�gina
		Utils.vp(driver, resultado, "Aparece mensaje de Servicios y firmas asignados al usuario", true,
				driver.getPageSource().toLowerCase().contains(Constantes.MensajeServiciosFirmasAsignados.toLowerCase()), "FALLO");

		// Guardamos el usuario dado de alta y validado
		UsuarioAlta.setEstado(Constantes.ESTADO_NO_USAR);
		UsuarioAlta.guardarUsuarioEnCSV();
		Log.write("Se ha dado de alta el usuario " + UsuarioAlta.getCodUsuario() + " con estado " + Constantes.ESTADO_ACTIVO);

		return UsuarioAlta;

	}


	public static Usuario altaUsuarioFirmanteSinEmailTodoContratado(WebDriver driver, Resultados resultado) throws Exception
	{

		// Creamos un usuario Aleatorio para el alta
		Usuario UsuarioAlta = Funciones.crearUsuarioAleatorio("default", "NO", "Apoderado");

		// Lo damos de alta sin token
		UsuarioAlta.setDispositivo("NO");

		// acceder a administraci�n de usuarios
		Utils.accederAdmUsuarios(driver);

		// cambiar a frame
		Utils.esperarProcesandoPeticionMejorado(driver);
		WebElement frame = driver.findElement(By.xpath(Constantes.FramePrincipal));
		driver.switchTo().frame(frame);
		Utils.clickEnElVisible(driver, Constantes.BotonAccederAltaUsuario);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Informamos el alta
		Funciones.informarAltaSpainSinMail(driver, UsuarioAlta);

//		Utils.clickEnElVisiblePrimero(driver, Constantes.AceptarCondiciones);

		Utils.capturaIntermedia(driver, resultado, "Se informan datos en el alta de usuario");

		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonAltaUsuario);

		// Se valida el texto de aviso sobre dispositivo al finalizar el paso 1 del alta
		String textoDiv = driver.findElement(By.xpath(Constantes.DivAltaSinToken)).getText();
		String textoEsperado = Constantes.recuerdeAsignarDispositivo;
		Utils.vp(driver, resultado, "Texto del div sobre dispositivo", true, textoDiv.indexOf(textoEsperado) > -1, "FALLO");
		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonContinuar);
		
		aceptarTratamientoDeDatosDeUsuario(driver, resultado, true);

		Utils.esperarProcesandoPeticionMejorado(driver);

		// Se valida que hay 3 opciones de perfilado
		List<WebElement> opcionesPerfilado = driver.findElements(By.xpath(Constantes.LabelOpcionesPerfilado));
		Utils.vp(driver, resultado, "Validar n�mero opciones perfilado", 3, opcionesPerfilado.size(), "FALLO");

		// Seleccionar Todo Contratado
		Utils.clickEnElVisiblePrimero(driver, Constantes.CheckTodoContratado);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Validamos que sale el DIV de todo contratado
		Utils.vpWarning(driver, resultado, "T�tulo Asignar Todo lo contratado", true, Utils.existeElementoTiempo(driver, By.xpath(Constantes.TituloTodoContratado), 15), "FALLO");

		Utils.capturaIntermedia(driver, resultado, "Se selecciona asignar todos los servicios contratados en el alta de usuario");

		// Bot�n Continuar Azul
		Funciones.clickBotonContinuarAzul(driver);

		Utils.validar(driver);

		// Se valida mensaje de operaci�n realizada correctamente
		Utils.validarOperacionOk(driver);


		// Se validan divs de izquierda y derecha con datos de alta de usuario
		// Funciones.comprobarDIVSTrasElAlta(driver, resultado, UsuarioAlta,Constantes.literalUsuario,"");

		// Se valida literal sobre servicios y firmas en la p�gina
		Utils.vp(driver, resultado, "Aparece mensaje de Servicios y firmas asignados al usuario", true,
				driver.getPageSource().toLowerCase().contains(Constantes.MensajeServiciosFirmasAsignados.toLowerCase()), "FALLO");

		// Guardamos el usuario dado de alta y validado
		UsuarioAlta.setEstado(Constantes.ESTADO_NO_USAR);
		UsuarioAlta.guardarUsuarioEnCSV();
		Log.write("Se ha dado de alta el usuario " + UsuarioAlta.getCodUsuario() + " con estado " + Constantes.ESTADO_ACTIVO);

		return UsuarioAlta;

	}


	// Crea un usuario y lo bloquea
	public static Usuario bloquearUsuario(WebDriver driver, Resultados resultado) throws Exception
	{

		Usuario usuarioPrueba = Funciones.altaUsuarioActivo(driver, resultado);

		// acceder a administraci�n de usuarios
		Utils.accederAdmUsuarios(driver);

		// cambiar a frame
		WebElement frame = driver.findElement(By.xpath(Constantes.FramePrincipal));
		driver.switchTo().frame(frame);

		// Buscamos el usuario
		Funciones.buscarUsuarioYSeleccionarlo(driver, resultado, usuarioPrueba);

		// Validar estado usuario filtrado
		Utils.vp(driver, resultado, "Validar estado " + Constantes.ESTADO_ACTIVO, Constantes.ESTADO_ACTIVO, driver.findElement(By.xpath(Constantes.TablaUsuarios_EstadoPrimerUsuario)).getText(),
				"FALLO");

		// Bloqueamos
		Utils.clickEnElVisible(driver, Constantes.BotonBloquear);
		Utils.clickEnElVisible(driver, Constantes.BotonBloquearConf);
		String mensajeOperacion = driver.findElement(By.xpath(Constantes.DivMensajeOk)).getText();
		Utils.vp(driver, resultado, "Mensaje: " + mensajeOperacion, true, mensajeOperacion.indexOf("usuario " + usuarioPrueba.getCodUsuario() + " ha sido bloqueado correctamente") > -1, "FALLO");

		Utils.clickEnElVisible(driver, Constantes.LinkValidarOperacion);

		// Se valida el mensaje de Bloquear usuario
		Utils.vp(driver, resultado, "Mensaje Bloquear usuario", true, driver.findElement(By.xpath(Constantes.DivMensajeOperacionPendiente)).getText().contains("Bloquear usuario"), "FALLO");

		if (Utils.isDisplayed(driver, Constantes.LinkDesplegar, 1))	
			Utils.clickEnElVisiblePrimero(driver, Constantes.LinkDesplegar);

		// Se valida el c�digo de usuario a bloquear
		String textoDesplegado = Utils.devuelveTextoDelVisiblePrimero(driver, Constantes.DivTextoDesplegado);
		Utils.vp(driver, resultado, "Validaci�n texto desplegable contiene el c�digo del usuario", true, textoDesplegado.indexOf(usuarioPrueba.getCodUsuario()) > -1, "FALLO");

		Utils.capturaIntermedia(driver, resultado, "Usuario pendiente de bloquear");

		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonValidar);
		Utils.validar(driver);
		Utils.validarOperacionOk(driver);

		return usuarioPrueba;
	}


	// Crea un usuario y lo deja Pendiente de Bloquear
	public static Usuario pendientebloquearUsuario(WebDriver driver, Resultados resultado) throws Exception
	{

		Usuario usuarioPrueba = Funciones.altaUsuarioActivo(driver, resultado);

		// acceder a administraci�n de usuarios
		Utils.accederAdmUsuarios(driver);

		// cambiar a frame
		WebElement frame = driver.findElement(By.xpath(Constantes.FramePrincipal));
		driver.switchTo().frame(frame);

		// Buscamos el usuario
		Funciones.buscarUsuarioYSeleccionarlo(driver, resultado, usuarioPrueba);

		// Validar estado usuario filtrado
		Utils.vp(driver, resultado, "Validar estado " + Constantes.ESTADO_ACTIVO, Constantes.ESTADO_ACTIVO, driver.findElement(By.xpath(Constantes.TablaUsuarios_EstadoPrimerUsuario)).getText(),
				"FALLO");

		// Seleccionar usuario filtrado
		// Ya viene seleccionado
		// Utils.clickEnElVisible(driver, Constantes.RadioSeleccionUsuarioFiltrado);
		Utils.clickEnElVisible(driver, Constantes.BotonBloquear);
		Utils.clickEnElVisible(driver, Constantes.BotonBloquearConf);
		String mensajeOperacion = driver.findElement(By.xpath(Constantes.DivMensajeOk)).getText();
		Utils.vp(driver, resultado, "Mensaje: " + mensajeOperacion, true, mensajeOperacion.indexOf("usuario " + usuarioPrueba.getCodUsuario() + " ha sido bloqueado correctamente") > -1, "FALLO");

		Utils.clickEnElVisible(driver, Constantes.LinkValidarOperacion);

		// Se valida el mensaje de Bloquear usuario
		Utils.vp(driver, resultado, "Mensaje Bloquear usuario", true, driver.findElement(By.xpath(Constantes.DivMensajeOperacionPendiente)).getText().contains("Bloquear usuario"), "FALLO");

		if (Utils.isDisplayed(driver, Constantes.LinkDesplegar, 1))
			Utils.clickEnElVisiblePrimero(driver, Constantes.LinkDesplegar);

		// Se valida el c�digo de usuario a bloquear
		String textoDesplegado = Utils.devuelveTextoDelVisiblePrimero(driver, Constantes.DivTextoDesplegado);
		Utils.vp(driver, resultado, "Validaci�n texto desplegable contiene el c�digo del usuario", true, textoDesplegado.indexOf(usuarioPrueba.getCodUsuario()) > -1, "FALLO");

		Utils.capturaIntermedia(driver, resultado, "Usuario pendiente de bloquear");

		Utils.clickEnElVisible(driver, Constantes.BotonValidarGenerico);
		Utils.clickEnElVisible(driver, Constantes.LinkValidarOtroMomento);

		return usuarioPrueba;
	}


	public static Usuario pendienteDesbloquearUsuario(WebDriver driver, Resultados resultado) throws Exception
	{

		// Buscamos si existe un usuario bloqueado
		Usuario usuarioPrueba = Funciones.devuelveUsuarioPorTipo("", "", Constantes.ESTADO_BLOQ);

		if(usuarioPrueba == null)
		{
			usuarioPrueba = Funciones.bloquearUsuario(driver, resultado);
		}
		else
		{
			Log.write("Se coge el usuario bloqueado " + usuarioPrueba.getCodUsuario() + " para esta prueba");
		}

		usuarioPrueba.setEstado(Constantes.ESTADO_NO_USAR);
		usuarioPrueba.guardarUsuarioEnCSV();

		// acceder a administraci�n de usuarios
		Utils.accederAdmUsuarios(driver);

		Funciones.buscarUsuarioYSeleccionarlo(driver, resultado, usuarioPrueba);

		// Validar estado usuario filtrado
		Utils.vp(driver, resultado, "Validar estado bloqueado", Constantes.ESTADO_BLOQ, driver.findElement(By.xpath(Constantes.TablaUsuarios_EstadoPrimerUsuario)).getText(), "FALLO");
		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonDesbloquear);
		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonDesbloquearConf);
		String mensajeOperacion = driver.findElement(By.xpath(Constantes.DivMensajeOk)).getText();
		Utils.vp(driver, resultado, "Mensaje" + mensajeOperacion, true, mensajeOperacion.indexOf("usuario " + usuarioPrueba.getCodUsuario() + " ha sido desbloqueado correctamente") > -1, "FALLO");
		Utils.clickEnElVisible(driver, Constantes.LinkValidarOperacion);
		
		// Se valida el mensaje de Desbloquear usuario
		Utils.vp(driver, resultado, "Mensaje Desbloquear usuario", true, driver.findElement(By.xpath(Constantes.DivMensajeOperacionPendiente)).getText().contains("Desbloquear usuario"), "FALLO");

		Utils.capturaIntermedia(driver, resultado, "Usuario pendiente de desbloquear");

		Utils.clickEnElVisible(driver, Constantes.BotonValidarGenerico);
		Utils.clickEnElVisible(driver, Constantes.LinkValidarOtroMomento);

		return usuarioPrueba;
	}


	public static void desconectar(WebDriver driver)
	{
		driver.switchTo().defaultContent();
		Utils.clickJS(driver, Constantes.BotonDesconectar);
		if(Utils.isAlertPresent(driver))
			driver.switchTo().alert().accept();
		
		try {
			if (Utils.isDisplayed(driver, Constantes.cerrarVentanaModalGenerico, 0))
				Utils.clickEnElVisiblePrimero(driver, Constantes.cerrarVentanaModalGenerico);
		} catch (Exception e) {
			
		}
	}


	public static void firmaClaveOperaciones(WebDriver driver, Usuario Usuario) throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String pais = params.getValorPropiedad("pais");

		if(!pais.equalsIgnoreCase("Compass"))
		{
			// Se selecciona m�todo de firma por clave de operaciones
			if(Utils.estamosEnLatam())
				Usuario.setClaveOp("qwerty99");
			else
				Usuario.setClaveOp("claveop01");
			Usuario.guardarUsuarioEnCSV();
			
			if (Utils.isDisplayed(driver,  Constantes.RadioFirmaClaveOperaciones, 0)) {
			
				Utils.clickEnElVisiblePrimero(driver, Constantes.RadioFirmaClaveOperaciones);
				Utils.introducirTextoEnElVisible2(driver, Constantes.CampoNuevaClaveOperaciones, Usuario.getClaveOp());
				Utils.introducirTextoEnElVisible2(driver, Constantes.CampoConfirmarNuevaClaveOperaciones, Usuario.getClaveOp());
				Utils.clickEnElVisible(driver, Constantes.BotonContinuarAzulGrandote);
				Thread.sleep(4500);
			}	
			
			Utils.esperarProcesandoPeticionMejorado(driver);
			
			if (Utils.isDisplayed(driver, Constantes.botonActivarDatosPersonales, 5))
				Utils.clickEnElVisiblePrimero(driver, Constantes.botonActivarDatosPersonales);
			
//			Utils.esperarVisibleTiempoSinSalir(driver, Constantes.FrameCampana, 12);
//			Thread.sleep(4500);
			
			// Pantalla de Bienvenida
			Funciones.pantallaBienvenidaUsuarioNuevo(driver);
		}


	}


	// S�lo utilizado en CPS_017 - Local Compass
	public static void firmaClaveOperacionesFake(WebDriver driver, Usuario Usuario) throws Exception
	{
		// Se selecciona m�todo de firma por clave de operaciones
		if(Utils.estamosEnLatam())
			Usuario.setClaveOp("qwerty99");
		else
			Usuario.setClaveOp("claveop01");
		Usuario.guardarUsuarioEnCSV();

		if(Utils.isDisplayed(driver, Constantes.RadioFirmaClaveOperaciones, 3))
			Utils.clickEnElVisiblePrimero(driver, Constantes.RadioFirmaClaveOperaciones);

		Utils.introducirTextoEnElVisible2(driver, Constantes.CampoNuevaClaveOperaciones, Usuario.getClaveOp());
		Utils.introducirTextoEnElVisible2(driver, Constantes.CampoConfirmarNuevaClaveOperaciones, Usuario.getClaveOp());

		//		driver.findElement(By.xpath(Constantes.CampoNuevaClaveOperaciones)).sendKeys(Usuario.getClaveOp());		
//		driver.findElement(By.xpath(Constantes.CampoConfirmarNuevaClaveOperaciones)).sendKeys(Usuario.getClaveOp());
		
		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonContinuarAzulGrandote);
		// Pantalla de Bienvenida
		// Funciones.pantallaBienvenidaUsuarioNuevo(driver);

	}


	public static void seleccionarTipoPoder(WebDriver driver, String tipoPoder) throws Exception
	{
		
		Utils.seleccionarComboXPath(driver, Constantes.comboTipoPoderGenerico, tipoPoder);
		
		
//		List<WebElement> listaElementos = driver.findElements(By.xpath(Constantes.comboTipoPoderGenerico));
//		WebElement combo = null;
//
//		for(int i = 0; i < listaElementos.size(); ++i)
//		{
//			if(listaElementos.get(i).isDisplayed() && listaElementos.get(i).isEnabled())
//				combo = listaElementos.get(i);
//		}
//
//		List<WebElement> opcionesCombo = combo.findElements(By.tagName("option"));
//		for(WebElement opcion : opcionesCombo)
//		{
//			if(opcion.getText().equalsIgnoreCase(tipoPoder))
//				Utils.clickJS(driver, opcion);
//		}

	}


	public static void seleccionarTipoPermiso(WebDriver driver, String tipoPermiso) throws Exception
	{
		
		Utils.seleccionarComboXPath(driver, Constantes.comboTipoPermisoGenerico, tipoPermiso);
		

//		List<WebElement> listaElementos = driver.findElements(By.xpath(Constantes.comboTipoPermisoGenerico));
//		WebElement combo = null;
//
//		for(int i = 0; i < listaElementos.size(); ++i)
//		{
//			if(listaElementos.get(i).isDisplayed() && listaElementos.get(i).isEnabled())
//				combo = listaElementos.get(i);
//		}
//
//		List<WebElement> opcionesCombo = combo.findElements(By.tagName("option"));
//		for(WebElement opcion : opcionesCombo)
//		{
//			if(opcion.getText().equalsIgnoreCase(tipoPermiso))
//				Utils.clickJS(driver, opcion);
//		}

	}


	// Crea un usuario en la aplicaci�n administrador solidario y firmante en estado Activo
	public static Usuario altaUsuarioTodoContratado(WebDriver driver, Resultados resultado) throws Exception
	{
		// Creamos un usuario Aleatorio para el alta
		Usuario UsuarioAlta = Funciones.crearUsuarioAleatorio("default", "Solidario-Indistinto", "Apoderado");

		// Como es con Token F�sico, se lo asignamos
		UsuarioAlta.setDispositivo("F�sico");

		// acceder a administraci�n de usuarios
		Utils.accederAdmUsuarios(driver);

		// cambiar a frame
		Utils.esperarProcesandoPeticionMejorado(driver);
		Utils.cambiarFramePrincipal(driver);
		Utils.clickEnElVisible(driver, Constantes.BotonAccederAltaUsuario);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Informamos el alta
		Funciones.informarAlta(driver, UsuarioAlta);

		// Al tener dispositivo, se debe marcar su radio button asociado
		Funciones.seleccionarDispositivo(driver, UsuarioAlta);

//		Utils.clickEnElVisiblePrimero(driver, Constantes.AceptarCondiciones);

		Utils.capturaIntermedia(driver, resultado, "Se informan datos en el alta de usuario");

		Utils.clickEnElVisiblePrimero(driver, Constantes.BotonAltaUsuario);

		if(Utils.isDisplayed(driver, Constantes.DivAltaSinToken, 5))
		{
			// Se valida el texto de aviso sobre dispositivo al finalizar el paso 1 del alta
			String textoDiv = driver.findElement(By.xpath(Constantes.DivAltaSinToken)).getText();
			String textoEsperado = Constantes.recuerdeAsignarDispositivo;
			Utils.vp(driver, resultado, "Texto del div sobre dispositivo", true, textoDiv.indexOf(textoEsperado) > -1, "FALLO");
			Utils.clickEnElVisiblePrimero(driver, Constantes.BotonContinuar);
		}
		
		aceptarTratamientoDeDatosDeUsuario(driver, resultado, true);

		Utils.esperarProcesandoPeticionMejorado(driver);

		// Se valida que hay 3 opciones de perfilado
		List<WebElement> opcionesPerfilado = driver.findElements(By.xpath(Constantes.LabelOpcionesPerfilado));
		Utils.vp(driver, resultado, "Validar n�mero opciones perfilado", 3, opcionesPerfilado.size(), "FALLO");

		// Seleccionar Todo Contratado
		Utils.clickEnElVisible(driver, Constantes.CheckTodoContratado);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Validamos que sale el DIV de todo contratado
		Utils.vpWarning(driver, resultado, "T�tulo Asignar Todo lo contratado", true, Utils.existeElementoTiempo(driver, By.xpath(Constantes.TituloTodoContratado), 15), "FALLO");

		Utils.capturaIntermedia(driver, resultado, "Se selecciona asignar todos los servicios contratados en el alta de usuario");

		// Bot�n Continuar Azul
		Funciones.clickBotonContinuarAzul(driver);

		Utils.validar(driver);

		// Se valida mensaje de operaci�n realizada correctamente
		Utils.validarOperacionOk(driver);


		// Se validan divs de izquierda y derecha con datos de alta de usuario
		Funciones.comprobarDIVSTrasElAlta(driver, resultado, UsuarioAlta, "Administrador", "");

		// Se valida literal sobre servicios y firmas en la p�gina
		Utils.vp(driver, resultado, "Aparece mensaje de Servicios y firmas asignados al usuario", true,
				driver.getPageSource().toLowerCase().contains(Constantes.MensajeServiciosFirmasAsignados.toLowerCase()), "FALLO");

		// Guardamos el usuario dado de alta y validado
		UsuarioAlta.setEstado(Constantes.ESTADO_NO_USAR);
		UsuarioAlta.guardarUsuarioEnCSV();
		Log.write("Se ha dado de alta el usuario " + UsuarioAlta.getCodUsuario() + " con estado " + Constantes.ESTADO_ACTIVO);

		return UsuarioAlta;

	}


	public static String obtenerNombreInternoTokenMovil(Usuario usuario)
	{
		String NombreInternoDispositivo = "";
		if(usuario.getPais().equals("Espa�a"))
			NombreInternoDispositivo = "BB-03-0034" + usuario.getTelefono();
		return NombreInternoDispositivo;
	}


	public static String obtenerNumeroTokenMovil(Usuario usuario)
	{
		String NumeroDispositivo = "";
		String Telefono = usuario.getTelefono();
		if(usuario.getPais().equals("Espa�a"))
			NumeroDispositivo = "00 34 " + Telefono;
		return NumeroDispositivo;
	}


	// Se le asigna un token al usuario pasado por par�metro, devuelve el token
	public static Token asignarTokenUsuario(WebDriver driver, Resultados resultado, Usuario usuarioDestino, Usuario usuarioValidacion) throws Exception
	{

		if(Utils.estamosEnLatam())
			return Funciones.asignarTokenUsuarioLATAM(driver, resultado, usuarioDestino, usuarioValidacion);

		else
		{

			TestProperties params = new TestProperties(Utils.getProperties());
			params.cargarPropiedades();
			Token token = Token.obtenerTokenPruebas(params.getValorPropiedad("referenciaPorDefecto"));

			Funciones.liberarTokenUsuario(driver, token, usuarioValidacion, resultado);

			token.setTipo(Constantes.TIPO_TOKEN_FISICO);

			// Comprobamos que el token est� disponible
			Utils.vp(driver, resultado, "Token disponible", Constantes.ESTADO_DISP_DISPONIBLE, driver.findElement(By.xpath(Constantes.TablaDispositivos_EstadoPrimerDispositivo)).getText(), "FALLO");

			// Asignamos el usuario
			Utils.clickEnElVisiblePrimero(driver, Constantes.TablaDispositivos_SeleccionarPrimerDispositivo);
			Utils.clickEnElVisiblePrimero(driver, Constantes.BotonAsignarDispositivo);
			Utils.introducirTextoEnElVisible2(driver, Constantes.CampoBuscarUsuariosSinDispositivo, usuarioDestino.getCodUsuario());
			
			Utils.clickEnElVisible(driver, Constantes.BotonBuscarUsuariosSinDispositivo);
			Utils.esperarProcesandoPeticionMejorado(driver);
			Utils.clickEnElVisible(driver, Constantes.RadioSeleccionUsuarioAsignarDisp);

			Utils.capturaIntermedia(driver, resultado, "Asignar dispositivo a usuario");

			Utils.clickEnElVisible(driver, Constantes.BotonAsignarDispositivo2);
			
			String mensajeOperacion = driver.findElement(By.xpath(Constantes.DivMensajeOk)).getText();
			String parteMensajeEsperado = "El dispositivo " + token.getNumero() + " ha sido asignado correctamente al usuario " + usuarioDestino.getNombreUsuario();
			Utils.vp(driver, resultado, "Mensaje dispositivo asignado correctamente", true, mensajeOperacion.indexOf(parteMensajeEsperado) > -1, "FALLO");
			
			// Pulsar enlace Validar operaci�n
			Utils.clickEnElVisible(driver, Constantes.LinkValidarOperacion);
			
			// Comprobar literal asignar dispositivo
			Utils.vp(driver, resultado, "Mensaje Asignar dispositivo", true, driver.findElement(By.xpath(Constantes.DivMensajeOperacionPendiente)).getText().contains("Asignar dispositivo"), "FALLO");
			
			// Expandir y validar codigo de usuario
			Thread.sleep(1500);
			
			if (Utils.isDisplayed(driver, Constantes.LinkDesplegar, 1))
				Utils.clickEnElVisible(driver, Constantes.LinkDesplegar);
			
			String textoDesplegado = Utils.devuelveTextoDelVisiblePrimero(driver, Constantes.DivTextoDesplegado);
			Utils.vp(driver, resultado, "Texto desplegado contiene c�digo del usuario", true, textoDesplegado.indexOf(usuarioDestino.getCodUsuario()) > -1, "FALLO");

			// Validar operacion
			Utils.clickEnElVisible(driver, Constantes.BotonValidar);
			Utils.validarUsuario(driver, usuarioValidacion, null);

			// Validar mensaje operaci�n realizada correctamente
			Utils.validarOperacionOk(driver);

			token.setCodigoUsuario(usuarioDestino.getCodUsuario());
			token.setEstado(Constantes.ESTADO_DISP_ACTIVO);

			// Guardamos el token solicitado
			token.guardarTokenEnCSV();

			return token;
		}
	}


	public static Token asignarTokenUsuarioLATAM(WebDriver driver, Resultados resultado, Usuario usuarioDestino, Usuario usuarioValidacion) throws Exception
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		Token token = Token.obtenerTokenPruebas(params.getValorPropiedad("referenciaPorDefecto"));

		// Liberamos el token si es necesario
		Funciones.liberarTokenUsuario(driver, token, usuarioValidacion, resultado);

		token.setTipo(Constantes.TIPO_TOKEN_FISICO);

		// Comprobamos que el token est� disponible
		Utils.vp(driver, resultado, "Token disponible", Constantes.ESTADO_DISP_DISPONIBLE, driver.findElement(By.xpath(Constantes.TablaDispositivos_EstadoPrimerDispositivo)).getText(), "FALLO");

		// Asignamos el usuario
		Utils.clickEnElVisible(driver, Constantes.botonAsignarDispositivoLATAM);
		Utils.esperarProcesandoPeticionMejorado(driver);

		Funciones.buscarUsuarioYSeleccionarloParaToken(driver, resultado, usuarioDestino);
		Utils.seleccionarComboContieneXPath(driver, Constantes.comboSeleccionarToken, token.getNumero());

		Utils.clickEnElVisible(driver, Constantes.botonAsignarLATAM);
		Utils.esperarProcesandoPeticionMejorado(driver);

		Utils.capturaIntermedia(driver, resultado, "Asignar dispositivo a usuario");
		Utils.esperarProcesandoPeticionMejorado(driver);

		String mensajeOperacion = driver.findElement(By.xpath(Constantes.DivMensajeOk)).getText();
		String parteMensajeEsperado = "ha sido asignado correctamente";
		Utils.vp(driver, resultado, "Mensaje dispositivo asignado correctamente", true, mensajeOperacion.indexOf(parteMensajeEsperado) > -1, "FALLO");

		// Comprobar literal asignar dispositivo
		Utils.vp(driver, resultado, "Mensaje Asignar dispositivo", true, driver.findElement(By.xpath(Constantes.DivMensajeOperacionPendiente)).getText().contains("Asignar dispositivo"), "FALLO");
		
		// Expandir y validar codigo de usuario
		if (Utils.isDisplayed(driver, Constantes.LinkDesplegar, 1))
			Utils.clickEnElVisiblePrimero(driver, Constantes.LinkDesplegar);
		
		String textoDesplegado = Utils.devuelveTextoDelVisiblePrimero(driver, Constantes.DivTextoDesplegado);
		Utils.vp(driver, resultado, "Texto desplegado contiene c�digo del usuario", true, textoDesplegado.indexOf(usuarioDestino.getCodUsuario()) > -1, "FALLO");

		// Validar operacion
		Utils.clickEnElVisible(driver, Constantes.BotonValidar);
		Utils.validarUsuario(driver, usuarioValidacion, null);

		// Validar mensaje operaci�n realizada correctamente
		Utils.validarOperacionOk(driver);

		if(("".equals(resultado.getBufferErrores().toString())))
		{
			token.setCodigoUsuario(usuarioDestino.getCodUsuario());
			token.setEstado(Constantes.ESTADO_DISP_ACTIVO);
			// Guardamos el token solicitado
			token.guardarTokenEnCSV();
		}

		return token;

	}


	public static void liberarToken(WebDriver driver, Token token, Resultados resultado) throws Exception
	{
		// Desasignamos el token si est� asignado a alg�n usuario
		// Acceder a gesti�n de dispositivos
		Utils.accederGestionDispositivos(driver);

		// Cambiar a frame
		Utils.cambiarFramePrincipal(driver);

		// Filtramos por n�mero de token
//		driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
//		if (Utils.estamosEnLatam())driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(token.getNumeroFormateado());
//		else driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(token.getNumero());
//		driver.findElement(By.xpath(Constantes.BotonBuscarDispositivo)).click();

		Funciones.BuscarTokenYSeleccionarlo(driver, token);

		// Si est� Preasignado y sin exclamaci�n, es porque el usuario que lo tiene tiene algo pendiente, se lo rechazamos
		if(driver.findElement(By.xpath(Constantes.TablaDispositivos_EstadoPrimerDispositivo)).getText().equalsIgnoreCase(Constantes.PreAsignado)
				&& !Utils.isDisplayed(driver, Constantes.imagenAlerta, 7))
		{
			String codigoUsuario = driver.findElement(By.xpath(Constantes.TablaDispositivos_CodigoUsuarioPrimerDispositivo)).getText();

			// Buscamos al usuario
			driver.switchTo().defaultContent();
			Utils.accederAdmUsuarios(driver);
			Funciones.buscarUsuarioYSeleccionarlo(driver, resultado, codigoUsuario);

			// Rechazamos
			Utils.clickEnElVisible(driver, Constantes.BotonValidarGenerico);
			Utils.clickEnElVisible(driver, Constantes.BotonRechazarGenerico);
			Utils.validar(driver);

			driver.switchTo().defaultContent();
			// Acceder a gesti�n de dispositivos
			Utils.accederGestionDispositivos(driver);
			// Cambiar a frame
			Utils.cambiarFramePrincipal(driver);
			// Filtramos por n�mero de token
//			driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
//			driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(token.getNumero());
//			driver.findElement(By.xpath(Constantes.BotonBuscarDispositivo)).click();	

			Funciones.BuscarTokenYSeleccionarlo(driver, token);
		}

		// Si tiene algo pendiente lo validamos y rechazamos
		boolean visible = Utils.isDisplayed(driver, Constantes.imagenAlerta, 7);
		if(visible)
		{
			Utils.clickEnElVisiblePrimero(driver, Constantes.TablaDispositivos_SeleccionarPrimerDispositivo);
			Utils.clickEnElVisible(driver, Constantes.BotonValidarTokenGenerico);
			Utils.clickEnElVisible(driver, Constantes.BotonRechazarGenerico);
			Utils.validar(driver);
//			//Filtramos por n�mero de token
//			driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
//			driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(token.getNumero());
//			Utils.clickEnElVisible(driver,Constantes.BotonBuscarDispositivo);

			Funciones.BuscarTokenYSeleccionarlo(driver, token);
		}

		// Si est� asignado a alg�n usuario, se desasigna
		if(!driver.findElement(By.xpath(Constantes.TablaDispositivos_CodigoUsuarioPrimerDispositivo)).getText().equals(""))
		{
			// Desasignar dispositivo
			Utils.clickEnElVisiblePrimero(driver, Constantes.TablaDispositivos_SeleccionarPrimerDispositivo);
			Utils.clickEnElVisible(driver, Constantes.BotonDesasignarDispositivo);

			Utils.capturaIntermedia(driver, resultado, "Desasignar dispositivo a usuario");

			Utils.clickEnElVisible(driver, Constantes.BotonDesasignarDispositivo2);
			
			// Validar mensaje: El dispositivo ha sido desasignado correctamente
			String mensajeOperacion = driver.findElement(By.xpath(Constantes.DivMensajeOk)).getText();
			Utils.vp(driver, resultado, "Mensaje dispositivo desasignado correctamente", true, mensajeOperacion.indexOf("desasignado correctamente") > -1, "FALLO");
			
			// Pulsar enlace Validar operaci�n
			Utils.clickEnElVisible(driver, Constantes.LinkValidarOperacion);
			
			// Comprobar literal Desasignar dispositivo
			Utils.vp(driver, resultado, "Mensaje Desasignar dispositivo", true, driver.findElement(By.xpath(Constantes.DivMensajeOperacionPendiente)).getText().contains("Desasignar dispositivo"), "FALLO");
			
			// Validar operaci�n
			Utils.clickEnElVisible(driver, Constantes.BotonValidar);
			Utils.validar(driver);
			// Validar mensaje operaci�n realizada correctamente
			Utils.validarOperacionOk(driver);
			token.eliminarTokenEnCSV();
			// Volvemos a filtrar por el token
//			driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
//			driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(token.getNumero());
//			Utils.clickEnElVisible(driver, Constantes.BotonBuscarDispositivo);
			Funciones.BuscarTokenYSeleccionarlo(driver, token);
		}
	}


	public static Token obtenerTokenLibre(WebDriver driver, Resultados resultado, Token token, String referencia) throws Exception
	{

		// Acceder a gesti�n de dispositivos
		Utils.accederGestionDispositivos(driver);

		// Cambiar a frame
		Utils.cambiarFramePrincipal(driver);

		Utils.esperarProcesandoPeticionMejorado(driver);
		Utils.esperarVisibleTiempoSinSalir(driver, Constantes.CampoBusqueda, 15);

		driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();

		Utils.clickEnElVisible(driver, Constantes.BotonAccederFiltrar);
		// driver.findElement(By.xpath(Constantes.CheckFiltroEstadoActivo)).click();
		Utils.clickEnElVisiblePrimero(driver, Constantes.CheckFiltroTokenPlus);

		Utils.clickEnElVisible(driver, Constantes.BotonFiltrar);
		Utils.esperarProcesandoPeticionMejorado(driver);

		String tokenActivoXpath = "//td[@headers='numDispositivo'][not (contains(text(),'" + token.getNumero() + "'))]";

		String tokenActivo = driver.findElement(By.xpath(tokenActivoXpath)).getText();

		Token tokenNuevo = new Token(referencia, tokenActivo);

		return tokenNuevo;
	}


	// Se libera un token con el usuario indicado
	public static void liberarTokenUsuario(WebDriver driver, Token token, Usuario usuario, Resultados resultado) throws Exception
	{
		// Desasignamos el token si est� asignado a alg�n usuario
		// Acceder a gesti�n de dispositivos
		Utils.accederGestionDispositivos(driver);
		// Cambiar a frame
		Utils.cambiarFramePrincipal(driver);
		// Filtramos por n�mero de token
//		driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
//		if (Utils.estamosEnLatam()) driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(token.getNumeroFormateado());
//		else driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(token.getNumero());
//		driver.findElement(By.xpath(Constantes.BotonBuscarDispositivo)).click();

		Funciones.BuscarTokenYSeleccionarlo(driver, token);

		// Si est� Preasignado y sin exclamaci�n, es porque el usuario que lo tiene tiene algo pendiente, se lo rechazamos
		if(driver.findElement(By.xpath(Constantes.TablaDispositivos_EstadoPrimerDispositivo)).getText().equalsIgnoreCase(Constantes.PreAsignado)
				&& !Utils.isDisplayed(driver, Constantes.imagenAlerta, 7))
		{
			String codigoUsuario = driver.findElement(By.xpath(Constantes.TablaDispositivos_CodigoUsuarioPrimerDispositivo)).getText();

			// Buscamos al usuario
			driver.switchTo().defaultContent();
			Utils.accederAdmUsuarios(driver);
			Funciones.buscarUsuarioYSeleccionarlo(driver, resultado, codigoUsuario);

			// Rechazamos
			Utils.clickEnElVisible(driver, Constantes.BotonValidarGenerico);
			Utils.clickEnElVisible(driver, Constantes.BotonRechazarGenerico);
			Utils.validar(driver);

			driver.switchTo().defaultContent();
			// Acceder a gesti�n de dispositivos
			Utils.accederGestionDispositivos(driver);
			// Cambiar a frame
			Utils.cambiarFramePrincipal(driver);
			// Filtramos por n�mero de token
//			driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
//			driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(token.getNumero());
//			driver.findElement(By.xpath(Constantes.BotonBuscarDispositivo)).click();	

			Funciones.BuscarTokenYSeleccionarlo(driver, token);
		}

		// Si tiene algo pendiente lo validamos y rechazamos
		boolean visible = Utils.isDisplayed(driver, Constantes.imagenAlerta, 7);
		if(visible)
		{
			Utils.clickEnElVisiblePrimero(driver, Constantes.TablaDispositivos_SeleccionarPrimerDispositivo);
			Utils.clickEnElVisible(driver, Constantes.BotonValidarTokenGenerico);
			Utils.clickEnElVisible(driver, Constantes.BotonRechazarGenerico);
			Utils.validar(driver);
			// Filtramos por n�mero de token
//			driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
//			driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(token.getNumero());
//			driver.findElement(By.xpath(Constantes.BotonBuscarDispositivo)).click();

			Funciones.BuscarTokenYSeleccionarlo(driver, token);
		}

		// Si est� asignado a alg�n usuario, se desasigna
		if(!driver.findElement(By.xpath(Constantes.TablaDispositivos_CodigoUsuarioPrimerDispositivo)).getText().equals(""))
		{
			// Desasignar dispositivo
			Utils.clickEnElVisiblePrimero(driver, Constantes.TablaDispositivos_SeleccionarPrimerDispositivo);
			Utils.clickEnElVisiblePrimero(driver, Constantes.BotonDesasignarDispositivo);

			Utils.capturaIntermedia(driver, resultado, "Desasignar dispositivo a usuario");

			Utils.clickEnElVisiblePrimero(driver, Constantes.BotonDesasignarDispositivo2);
			
			// Validar mensaje: El dispositivo ha sido desasignado correctamente
			String mensajeOperacion = driver.findElement(By.xpath(Constantes.DivMensajeOk)).getText();
			Utils.vp(driver, resultado, "Mensaje dispositivo desasignado correctamente", true,
					mensajeOperacion.indexOf("El dispositivo " + token.getNumero() + " ha sido desasignado correctamente") > -1, "FALLO");
			// Pulsar enlace Validar operaci�n
			Utils.clickEnElVisible(driver, Constantes.LinkValidarOperacion);
			// Comprobar literal Desasignar dispositivo
			Utils.vp(driver, resultado, "Mensaje Desasignar dispositivo", true, driver.findElement(By.xpath(Constantes.DivMensajeOperacionPendiente)).getText().contains("Desasignar dispositivo"), "FALLO");
			// Validar operaci�n
			Utils.clickEnElVisiblePrimero(driver, Constantes.BotonValidar);
			Utils.validarUsuario(driver, usuario, null);
			// Validar mensaje operaci�n realizada correctamente
			Utils.validarOperacionOk(driver);
			token.eliminarTokenEnCSV();
			// Volvemos a filtrar por el token
//			driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
//			driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(token.getNumero());
//			driver.findElement(By.xpath(Constantes.BotonBuscarDispositivo)).click();
			Funciones.BuscarTokenYSeleccionarlo(driver, token);
		}
	}


	public static int devuelveConfiguracionesCompletas(WebDriver driver) throws Exception
	{

		String xpath = Constantes.revisarConfiguracion;
		
		Utils.esperaHastaApareceSinSalir(driver, xpath, 20);
		
		int contador = 0;

		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
					contador++;
			}

		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}

		return contador;
	}


	public static int clickEnElPrimerRegistroConCombo(WebDriver driver, WebElement tabla, String valor) throws Exception
	{
		try
		{
			WebElement table = tabla;
			List<WebElement> allRows = table.findElements(By.tagName("tr"));

			int encontrado = -1;

			for(int i = 0; i < allRows.size() && encontrado == -1; i++)
			{
				WebElement row = allRows.get(i);
				List<WebElement> cells = row.findElements(By.tagName("td"));

				for(WebElement cell : cells)
				{
					if(cell.getText().indexOf(valor) > -1)
					{
						encontrado = i;
						Utils.clickJS(driver, cells.get(0).findElement(By.tagName("input")));
						if(!cells.get(0).findElement(By.tagName("input")).isSelected())
							Utils.clickJS(driver, cells.get(0).findElement(By.tagName("input")));
						break;
					}
				}
			}

			return encontrado;
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� la tabla: " + tabla.getTagName());
		}
	}


	public static String clickCheckAleatorioDevuelveValue(WebDriver driver, String xpath) throws Exception
	{
		try
		{
			List<WebElement> listaElementos = driver.findElements(By.xpath(xpath));
			List<WebElement> listaElementosVisibles = new ArrayList<WebElement>();

			for(int i = 0; i < listaElementos.size(); ++i)
			{
				if(listaElementos.get(i).isDisplayed())
				{
					listaElementosVisibles.add(listaElementos.get(i));
				}
			}
			Random random = new Random();
			int randomNum = random.nextInt(listaElementosVisibles.size());

			Utils.clickJS(driver, listaElementosVisibles.get(randomNum));
			return listaElementosVisibles.get(randomNum).getAttribute("value");
		}
		catch(Exception e)
		{
			Log.write(e);
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}
	}


	public static String devuelveUsuarioNoVacio(WebDriver driver, Resultados resultado) throws Exception
	{
		try
		{

			List<WebElement> allUsers = driver.findElements(By.xpath(Constantes.TablaUsuariosNombres));

			int usuariosVacios = 0;
			String encontrado = "";

			for(int i = 0; i < allUsers.size(); i++)
			{
				if(!allUsers.get(i).getText().equalsIgnoreCase(""))
				{
					encontrado = allUsers.get(i).getText();
				}
				if(allUsers.get(i).getText().equalsIgnoreCase(""))
				{
					usuariosVacios++;
				}
			}

			Utils.vpWarning(driver, resultado, "NO Existen usuarios con c�digo vac�o en la aplicaci�n", false, usuariosVacios > 0, "FALLO");

			if(encontrado.equals(""))
				throw new Exception("ERROR. No existen usuarios v�lidos en la aplicaci�n. Todos tienen nombre vac�o.");
			else
				return encontrado;

		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� la tabla: " + Constantes.TablaUsuarios);
		}
	}


	public static void AceptarAltaSinTokenSiSale(WebDriver driver, Resultados resultado) throws Exception
	{
		if(Utils.isDisplayed(driver, Constantes.DivAltaSinToken, 4))
		{
			String textoDiv = driver.findElement(By.xpath(Constantes.DivAltaSinToken)).getText();
			String textoEsperado = Constantes.recuerdeAsignarDispositivo;
			Utils.vp(driver, resultado, "Texto del div sobre dispositivo", true, textoDiv.indexOf(textoEsperado) > -1, "FALLO");
			Utils.clickEnElVisiblePrimero(driver, Constantes.BotonContinuar);
		}
	}


	public static void BuscarTokenYSeleccionarlo(WebDriver driver, Token token) throws Exception
	{
		
		Utils.esperaHastaApareceSinSalir(driver, Constantes.CampoBusqueda, 10);
		
		if(!Utils.isDisplayed(driver, Constantes.CampoBusqueda, 7))
		{
			Utils.cambiarFramePrincipal(driver);
		}
		driver.findElement(By.xpath(Constantes.CampoBusqueda)).clear();
		
		if(Utils.estamosEnLatam())
			driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(token.getNumeroFormateado());
		else
			driver.findElement(By.xpath(Constantes.CampoBusqueda)).sendKeys(token.getNumero());
		
		Utils.clickEnElVisible(driver, Constantes.BotonBuscarDispositivo);
		
		Utils.esperarProcesandoPeticionMejorado(driver);

	}


	public static void cambiarPaisCMP(WebDriver driver, String paisDestino) throws Exception
	{

		if(Utils.isDisplayed(driver, Constantes.botonSalto, 5))
			Utils.clickEnElVisible(driver, Constantes.botonSalto);
		else if(Utils.isDisplayed(driver, Constantes.CountryJumper, 1))
			Utils.clickEnElVisible(driver, Constantes.CountryJumper);

		if(paisDestino.equals("Espa�a"))
			Utils.clickEnElVisible(driver, Constantes.SaltoAEspana);
		else if(paisDestino.equals("Chile"))
			Utils.clickEnElVisible(driver, Constantes.SaltoAChile);
		else if(paisDestino.equals("Global"))
			Utils.clickEnElVisible(driver, Constantes.SaltoAGlobal);
	}


	public static Usuario altaUsuarioActivoYAccedidoLATAM(WebDriver driver, Resultados resultado) throws Exception
	{

		Usuario UsuarioAlta = Funciones.altaUsuarioActivo(driver, resultado);

		Utils.desconectar(driver);

		Utils.primerLogin(driver, UsuarioAlta, resultado);

		// Alg�n d�a esto no valdr� porque redirigir� bien
		Utils.navegarLogin(driver);
		Utils.loginTipoV7(driver, UsuarioAlta);
		Funciones.firmaClaveOperacionesFake(driver, UsuarioAlta);

		Utils.navegarLogin(driver);

		return UsuarioAlta;
	}


	public static void filtroCuandoTimeOut(WebDriver driver, Resultados resultado) throws Exception
	{
		Utils.clickEnElVisible(driver, Constantes.BotonAccederFiltrar);
		Utils.clickEnElVisible(driver, Constantes.CheckFiltroTipoUsuarioTodos);
		// Utils.clickEnElVisible(driver,Constantes.CheckFiltroTipoAdministrador);
		Utils.clickEnElVisible(driver, Constantes.CheckFiltroEstadoTodos);
		Thread.sleep(1500);

		Utils.clickEnElVisible(driver, Constantes.CheckFiltroEstadoTodos);

		Utils.capturaIntermedia(driver, resultado, "Filtrando usuarios");

		Utils.clickEnElVisible(driver, Constantes.BotonFiltrar);
		Utils.esperarProcesandoPeticionMejorado(driver);
	}


	public static String convertirFormatoMoneda(String cantidad)
	{

		double money = Double.parseDouble(cantidad);

		money = Math.round(money * 1000) / 1000;

		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		String moneyString = formatter.format(money);

		return moneyString.replace(" �", "");

	}


	public static String obtenerSaldoCuentaOrigen(WebDriver driver, String numCuentaOri)
	{

		String saldoCuentaOri = "//div[contains(@id,'cuenta')][contains(text(),'" + numCuentaOri + "')]/following-sibling::div/span[contains(@id,'saldoCuenta')]";

		String saldo = driver.findElement(By.xpath(saldoCuentaOri)).getText();

		return saldo;
	}


	


	public static String obtenerSemilla(String fich, String ref, String usuario) throws Exception
	{
		FileReader fr = new FileReader(fich);
		BufferedReader bf = new BufferedReader(fr);
		String sCadena;
		String cadenaSemilla = "";
		
		usuario = usuario.toUpperCase();
		
		while(((sCadena = bf.readLine()) != null))
		{
			if((sCadena.contains(ref + usuario)) && ((sCadena.contains("semilla:"))))
				cadenaSemilla = sCadena;
		}
		fr.close();

		if(cadenaSemilla.equals(""))
			throw new Exception("No se encontr� la semilla para la referencia: " + ref + " y usuario: " + usuario);

		int indice = cadenaSemilla.lastIndexOf("semilla:");
		cadenaSemilla = cadenaSemilla.substring(indice + 9, cadenaSemilla.length());
		return cadenaSemilla;
	}
	
	private static String getIVTicket(String fich, String ref, String usuario) throws Exception
	{
		FileReader fr = new FileReader(fich);
		BufferedReader bf = new BufferedReader(fr);
		String sCadena;
		String cadenaIVTicket = "";
		String textoABuscar = "iv-ticket=[";
		
		while(((sCadena = bf.readLine()) != null))
		{
			if((sCadena.contains(ref + usuario)) && ((sCadena.contains(textoABuscar))))
				cadenaIVTicket = sCadena;
		}
		fr.close();

		if(cadenaIVTicket.equals(""))
			throw new Exception("No se encontr� el iv-ticket para la referencia: " + ref + " y usuario: " + usuario);

		int indice = cadenaIVTicket.indexOf(textoABuscar);
		
		String cadenaIVTicketRestante = cadenaIVTicket.substring(indice, cadenaIVTicket.length());
		
		int indice2 = cadenaIVTicketRestante.indexOf("]");
		
		cadenaIVTicket= cadenaIVTicket.substring(indice + textoABuscar.length(), indice+indice2);

		
		return cadenaIVTicket;
	}
	
	
	public static void firmarOrdenDispositivoMovil(WebDriver driver) throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		int intentosValidacion = 0;
		boolean validacion = false;
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosFijos");
		try
		{
			Usuario UsuarioValidar = Usuario.obtenerUsuarioPorReferenciaCodigo(rutaCSV, params.getValorPropiedad("referenciaPorDefecto"), params.getValorPropiedad("usuarioPorDefecto"));
			if(UsuarioValidar == null)
				throw new Exception("No hay usuario para realizar la validaci�n");

			while(!validacion && intentosValidacion < 3)
			{
				Thread.sleep(2000);
				Utils.introducirTextoEnElVisible2(driver, Constantes.firmasCampoResultadoFormula, UsuarioValidar.getClaveOp());
				Utils.clickEnElVisible(driver, Constantes.firmasLinkSMS);
				String mensaje = "Introduzca clave de token del usuario por defecto";

				if(intentosValidacion > 0)
					mensaje = mensaje + ". Tiene " + (3 - intentosValidacion) + " intento(s) m�s";

				// driver.findElement(By.id("claveToken")).sendKeys(Main.computePin(Semilla,null));
				String resul = JOptionPane.showInputDialog(null, mensaje, "Informaci�n", JOptionPane.INFORMATION_MESSAGE);
				Utils.introducirTextoEnElVisible2(driver, Constantes.firmasCampoDispositivoSeguridad, resul);
				// JOptionPane.showMessageDialog(null, mensaje,"Informaci�n",JOptionPane.INFORMATION_MESSAGE);
				Utils.clickEnElVisible(driver, Constantes.firmasBotonContinuarFirma);
				// Thread.sleep(2000);

				Utils.esperarProcesandoPeticionFirmas(driver);
				if(Utils.isDisplayed(driver, Constantes.firmasErrorCrendenciales, 10))
					intentosValidacion++;
				else
					validacion = true;
				if(intentosValidacion == 3)
					Log.write("Se han superado el n�mero de reintentos de validaci�n permitidos");
			}
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se ha podido realizar la firma correctamente");
		}
	}


	public static void firmarOrdenDispositivoMovilDesdeLog(WebDriver driver, String ref, String usuario) throws Exception
	{
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosFijos");
		String rutaLogsFirmas = params.getValorPropiedad("rutaLogsFirmas");
		String ficheroLogsFirmas = params.getValorPropiedad("nombreFicheroLogsfirmas");
		String userMaquina = params.getValorPropiedad("userLogsFirmas");
		String passMaquina = params.getValorPropiedad("passLogsFirmas");
		String nombreMaquina = params.getValorPropiedad("maquinaLogsFirmas");

		try
		{
			Usuario UsuarioValidar = Usuario.obtenerUsuarioPorReferenciaCodigo(rutaCSV, params.getValorPropiedad("referenciaPorDefecto"), params.getValorPropiedad("usuarioPorDefecto"));
			if(UsuarioValidar == null)
				throw new Exception("No hay usuario para realizar la validaci�n");
			Thread.sleep(5000);
			Utils.esperaHastaAparece(driver, Constantes.firmasCampoResultadoFormula, 10);
			Utils.introducirTextoEnElVisible2(driver, Constantes.firmasCampoResultadoFormula, UsuarioValidar.getClaveOp());
			Utils.crearBloqueo();
			Date ahora = new Date();
			Utils.clickEnElVisible(driver, Constantes.firmasLinkSMS);
			Utils.esperarVisibleTiempo(driver, Constantes.firmasLinkDispositivoFisico, 30);
			String ficheroLogsfirmas = Utils.downloadParticularFileSFTP(rutaLogsFirmas, ficheroLogsFirmas, userMaquina, passMaquina, nombreMaquina, ahora);
			String semilla = Funciones.obtenerSemilla(ficheroLogsfirmas, ref, usuario);
			Utils.introducirTextoEnElVisible2(driver, Constantes.firmasCampoDispositivoSeguridad, semilla);
			Utils.clickEnElVisible(driver, Constantes.firmasBotonContinuarFirma);
			Utils.esperarProcesandoPeticionFirmas(driver);
			Utils.liberarBloqueo();
			Thread.sleep(5000);
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se ha podido realizar la firma correctamente");
		}
	}


	public static String transformaCadenaaNumeroCash(String cadenaNum)
	{
		String entero = cadenaNum.substring(0, cadenaNum.indexOf(","));
		String decimal = cadenaNum.substring(cadenaNum.indexOf(","), cadenaNum.length());
		String cadenaTransformada = "";
		String enteroReves = "";

		for(int i = entero.length() - 1; i >= 0; i--)
		{
			enteroReves = enteroReves + entero.charAt(i);
		}

		for(int i = enteroReves.length() - 1; i >= 0; i--)
		{
			cadenaTransformada = cadenaTransformada + enteroReves.charAt(i);
			if((i % 3 == 0) && (i > 0))
			{
				cadenaTransformada = cadenaTransformada + ".";
			}
		}

		return cadenaTransformada + decimal;
	}


	public static String ibanToBocf(final String iban)
	{
		// Eliminamos los cuatro primeros digitos
		String bancoOficinaDCCuenta = iban.substring(4);

		// Se elimina el digito control y la contraPartida y el folio se completan con ceros

		String banco = bancoOficinaDCCuenta.substring(0, 4);
		String oficina = bancoOficinaDCCuenta.substring(4, 8);
		// String digitoControl = bancoOficinaDCCuenta.substring(8,10);
		String contraPartida = bancoOficinaDCCuenta.substring(10, 13);
		String folio = bancoOficinaDCCuenta.substring(13, 19);

		final String bocf = banco + oficina + "0" + contraPartida + "0" + folio;

		return bocf;
	}


	public static void clickEnCheckFicheroConcretoImpagados(WebDriver driver, String xpath) throws Exception
	{

		try
		{
			WebElement elemento = driver.findElement(By.xpath(xpath));

			if(elemento.isDisplayed())
				Utils.clickJS(driver, elemento);
			Utils.esperarProcesandoPeticionDefaultContentVolviendoAPPal(driver);
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se encontr� el elemento: " + xpath);
		}

	}


	public static String obtenerAsuntoDesdeInformacionCuentas(WebDriver driver, Resultados resultado) throws Exception
	{

		String cuenta = "";

		// Acceder a editar perfil (cuidado con espacio en blanco al final del texto del span Editar perfil
		Utils.clickEnElVisible(driver, Constantes.EntrarMiPerfil);
		Utils.clickEnElVisible(driver, Constantes.EntrarEditarPerfil);
		Utils.esperarProcesandoPeticionMejorado(driver);

		// Cambiar a frame
		WebElement frame = driver.findElement(By.xpath(Constantes.FramePrincipal));
		driver.switchTo().frame(frame);

		// Se accede a la pesta�a Servicios y firmas
		Utils.clickEnElVisible(driver, Constantes.PestanaServiciosFirmas);

		// Vamos a por una cuenta
		Utils.clickEnElVisible(driver, Constantes.enlaceRevisarInformacionCuentas);
//		Utils.esperarMasGenerico(driver);

		if(Utils.isDisplayed(driver, Constantes.masEspecifico, 2))
			Utils.clickEnElVisible(driver, Constantes.masEspecifico);

		Utils.clickEnElVisible(driver, Constantes.masHistoricoLiteral);

		Utils.clickEnElVisible(driver, Constantes.radioExcepcionesEspecifico);

		WebElement tablaVisible = Utils.devuelveElementoVisible(driver, Constantes.tablaCuentasGlobalesGenerico);
		int filas = Utils.numeroFilasTabla(driver, tablaVisible);
		Utils.vp(driver, resultado, "Comprobaci�n tabla Cuentas Globales tiene datos. Informaci�n Cuentas.", true, filas > 0, "FALLO");

		if(Utils.isDisplayed(driver, Constantes.tablaCuentasAlias, 5))
		{
			cuenta = driver.findElement(By.xpath(Constantes.tablaCuentasAlias)).getAttribute("tooltiptext");
		}
		else if(Utils.isDisplayed(driver, Constantes.tablaCuentasSinAlias, 5))
		{
			cuenta = driver.findElement(By.xpath(Constantes.tablaCuentasSinAlias)).getText().trim();
		}


		return cuenta;

	}


	public static String obtenerAsuntoDesdeSaldosYMovimientos(WebDriver driver, Resultados resultado)
	{
		try
		{
//			driver.switchTo().defaultContent();
//			Utils.clickEnElVisible(driver, Constantes.InformacionDeCuentas);
//			Thread.sleep(3500);
//			Utils.clickEnElVisible(driver, Constantes.SaldosYMovimientos);
//			Utils.cambiarFramePrincipal(driver);
//			Utils.esperarProcesandoPeticionMejorado(driver);
//			String cuenta = driver.findElement(By.xpath(Constantes.cuentaSaldosMovimientos)).getText().trim();
//			return cuenta;

			// Acceder a Personalizar cuentas

			if(Utils.isDisplayed(driver, Constantes.EntrarMiPerfil, 1))
				Utils.clickEnElVisible(driver, Constantes.EntrarMiPerfil);
			else
			{
				driver.switchTo().defaultContent();
				Utils.clickEnElVisible(driver, Constantes.EntrarMiPerfil);
			}


			Utils.clickEnElVisible(driver, Constantes.EntrarPersonalizarCuentas);
			Utils.esperarProcesandoPeticionMejorado(driver);

			// Cambiar a frame
			WebElement frame = driver.findElement(By.xpath(Constantes.FramePrincipal));
			driver.switchTo().frame(frame);

			String cuenta = driver.findElement(By.xpath(Constantes.cuentaPersonalizarCuentas)).getText().trim();
			return cuenta;


		}
		catch(Exception e)
		{
			return "";
		}
	}
	
	
	public static void accederEntradaDeMenu (WebDriver driver, String nombreEntradaMenu) throws Exception {
		  
		  //Puede que est� oculto
		  if (!Utils.isDisplayed(driver, Constantes.buscarMenu, 2)){
			  if (Utils.isDisplayed(driver, Constantes.tresRayitasMenu, 1))
				  Utils.clickEnElVisiblePrimero(driver, Constantes.tresRayitasMenu);
		  }
		
		  Utils.introducirTextoEnElVisible2(driver, Constantes.buscarMenu, nombreEntradaMenu);
		  Thread.sleep(1500);
		  
		  if (!Utils.isDisplayed(driver, Constantes.entradaDeMenu, 3)){
			  Utils.introducirTextoEnElVisible2(driver, Constantes.buscarMenu, nombreEntradaMenu);
			  Thread.sleep(3000);
		  }
		  
		  //Utils.clickEnElVisible(driver, Constantes.entradaDeMenu);
		  
		  //Si solo aparece un resultado, no pone en negrita
		  if (Utils.numeroElementosVisibles(driver, Constantes.enlaceEntradaDeMenu)==1)
			  Utils.clickEnElVisible(driver, Constantes.enlaceEntradaDeMenu);
		  else
			  Utils.clickEnElVisible(driver, Constantes.entradaDeMenuTerminal);
		  
		  //driver.findElement(By.xpath("//*[@class='kyop-li-search ui-menu-item']")).click();;
		  Utils.esperarProcesandoPeticionMejorado(driver);
	}
	
	public static void accederEntradaDeMenuNoContieneUnTexto (WebDriver driver,  String textoAContener, String textoAEvitar ) throws Exception {
		
		  Utils.introducirTextoEnElVisible2(driver, Constantes.buscarMenu, textoAContener);
		  Thread.sleep(1500);
		  
		  if (!Utils.isDisplayed(driver, Constantes.entradaDeMenu, 3)){
			  Utils.introducirTextoEnElVisible2(driver, Constantes.buscarMenu, textoAContener);
			  Thread.sleep(3000);
		  }

		  Utils.esperaHastaApareceSinSalir(driver, Constantes.enlaceEntradaDeMenu, 5);
		  Thread.sleep(1500);
		  
		  List<WebElement> listaElementos = driver.findElements(By.xpath(Constantes.enlaceEntradaDeMenu));
		  WebElement elemento = null;

		  for(int i = 0; i < listaElementos.size(); ++i)
		  {
				if(listaElementos.get(i).isDisplayed()){
					elemento = listaElementos.get(i);
					
					String contenidoTexto = elemento.getText();
					
					if (contenidoTexto.contains(textoAContener) && !contenidoTexto.contains(textoAEvitar)){
						Thread.sleep(1500);
						Utils.clickJS(driver, elemento);
						break;
					}
				}
				
		  }
	
		  Utils.esperarProcesandoPeticionMejorado(driver);
		
	}
	
	public static void accederEntradaDeMenuPrimero (WebDriver driver, String nombreEntradaMenu) throws Exception {
		  
		  //Puede que est� oculto
		  if (!Utils.isDisplayed(driver, Constantes.buscarMenu, 2)){
			  if (Utils.isDisplayed(driver, Constantes.tresRayitasMenu, 1))
				  Utils.clickEnElVisiblePrimero(driver, Constantes.tresRayitasMenu);
		  }
		
		  Utils.introducirTextoEnElVisible2(driver, Constantes.buscarMenu, nombreEntradaMenu);
		 
		  if (!Utils.isDisplayed(driver, Constantes.entradaDeMenu, 3)){
			  Utils.introducirTextoEnElVisible2(driver, Constantes.buscarMenu, nombreEntradaMenu);
			  Thread.sleep(3000);
		  }
		  Utils.clickEnElVisiblePrimero(driver, Constantes.entradaDeMenu);

		  Utils.esperarProcesandoPeticionMejorado(driver);
	}
	
	public static void compruebaRastroMigas (WebDriver driver, Resultados resultado, String migaAComprobar) throws Exception {
		
		String migaActual = driver.findElement(By.xpath(Constantes.rastroMigaActual)).getText();

		Utils.vp(driver, resultado, "Comprobaci�n Rastro Migas enlace Operativa "+migaAComprobar+". Miga Actual: '"
				+ migaActual.replaceAll("\n", " ").toUpperCase() + "'", true, migaActual.toLowerCase().indexOf(migaAComprobar.toLowerCase()) > -1, "FALLO");

		
	}
	
	public static void seleccionarDispositivoAleatorio(WebDriver driver, Usuario usuario,Resultados resultado) throws Exception

	{
		Select select=new Select(Utils.devuelveElementoVisibleYEnabled(driver, Constantes.ComboDispositivo));
		
		List<WebElement> opcionesCombo = select.getOptions();
		
		boolean encontrado=false;
	
		for (int i = 0 ; i<=opcionesCombo.size() && !encontrado; i++)
		{	
			
			if (!opcionesCombo.get(i).getText().equals("")) 
			{
				Utils.clickJS(driver, opcionesCombo.get(i));
				encontrado=true;
		
			}
		}
		
		
		if (encontrado==false)
		{
		Log.write("No se han encontrado tokens f�sicos disponibles");
		}
				
		
	}
	
public static Usuario altaUsuarioTokenFisicoAleatorio (WebDriver driver, Resultados resultado) throws Exception {
		
		// Creamos un usuario Aleatorio para el alta
		Usuario UsuarioAlta = Funciones.crearUsuarioAleatorio("default", "Solidario-Indistinto", "Apoderado");

		// Como es con Token F�sico, se lo asignamos
		UsuarioAlta.setDispositivo("F�sico");

		// acceder a administraci�n de usuarios
		Utils.accederAdmUsuarios(driver);

		// cambiar a frame
		Utils.esperarProcesandoPeticionMejorado(driver);
		Utils.cambiarFramePrincipal(driver);
		Utils.clickEnElVisible(driver, Constantes.BotonAccederAltaUsuario);

		// Informamos el alta
		Funciones.informarAlta(driver, UsuarioAlta);

		// Al tener dispositivo, se debe marcar su radio button asociado
		Utils.clickEnElVisible(driver, Constantes.RadioDispositivoFisico);
		Funciones.seleccionarDispositivoAleatorio(driver, UsuarioAlta, resultado);
//		Utils.clickEnElVisible(driver, Constantes.AceptarCondiciones);
		Utils.capturaIntermedia(driver, resultado, "Se informan datos en el alta de usuario");

		Utils.clickEnElVisible(driver,Constantes.BotonAltaUsuario);

		// Se valida el texto de aviso sobre dispositivo al finalizar el paso 1 del alta, si aparece

		if(Utils.isDisplayed(driver, Constantes.DivAltaSinToken, 4))
		{
			String textoDiv = driver.findElement(By.xpath(Constantes.DivAltaSinToken)).getText();
			String textoEsperado = Constantes.recuerdeAsignarDispositivo;
			Utils.vp(driver, resultado, "Texto del div sobre dispositivo", true, textoDiv.indexOf(textoEsperado) > -1, "FALLO");
			Utils.clickEnElVisible(driver, Constantes.BotonContinuar);
		}
		
		aceptarTratamientoDeDatosDeUsuario(driver, resultado, true);

		Utils.esperarProcesandoPeticionMejorado(driver);

		// Se valida que hay 3 opciones de perfilado
		List<WebElement> opcionesPerfilado = driver.findElements(By.xpath(Constantes.LabelOpcionesPerfilado));
		Utils.vp(driver, resultado, "Validar n�mero opciones perfilado", 3, opcionesPerfilado.size(), "FALLO");

		// Seleccionar Todo Contratado y se  valida si se muestra el combo para elegir TIPO DE PODER 
		Utils.clickEnElVisiblePrimero(driver, Constantes.CheckTodoContratado);
		Utils.esperarProcesandoPeticionMejorado(driver);
				
		// Validamos que sale el DIV de todo contratado
		Utils.vpWarning(driver, resultado, "T�tulo Asignar Todo lo contratado", true, Utils.existeElementoTiempo(driver, By.xpath(Constantes.TituloTodoContratado), 15), "FALLO");

		Utils.capturaIntermedia(driver, resultado, "Se selecciona asignar todos los servicios contratados en el alta de usuario");

		// Bot�n Continuar Azul
		Funciones.clickBotonContinuarAzul(driver);

		Utils.validar(driver);

		// Se valida mensaje de operaci�n realizada correctamente
		Utils.validarOperacionOk(driver);

		// Se validan divs de izquierda y derecha con datos de alta de usuario
		Funciones.comprobarDIVSTrasElAlta(driver, resultado, UsuarioAlta, Constantes.literalUsuario, "");

		// Se valida literal sobre servicios y firmas en la p�gina
		Utils.vp(driver, resultado, "Aparece mensaje de Servicios y firmas asignados al usuario", true,
				driver.getPageSource().toLowerCase().contains(Constantes.MensajeServiciosFirmasAsignados.toLowerCase()), "FALLO");

		// Guardamos el usuario dado de alta y validado
		UsuarioAlta.setEstado(Constantes.ESTADO_NO_USAR);
		UsuarioAlta.guardarUsuarioEnCSV();
		Log.write("Se ha dado de alta el usuario " + UsuarioAlta.getCodUsuario() + " con estado " + Constantes.ESTADO_ACTIVO);
		
		
		return UsuarioAlta;
		
	}

	public static String obtenerIVTicket(String ref, String usuario, String rutaFichero, String nombreficheroLogs) throws Exception
	{
		
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();		
		String entorno = params.getValorPropiedad("entorno");
		
		String passMaquina = "";
		String userMaquina = "";
		String nombreMaquina = "";

		if (entorno.equalsIgnoreCase("Integrado")) {
			
			userMaquina = Funciones.getEIUser();
			passMaquina = Funciones.getEIPass();
			nombreMaquina = "ai003";
			
		}
		
		else if (entorno.equalsIgnoreCase("Desarrollo")) {
			
			userMaquina = "xakynf1d";
			passMaquina = "acceso10";
			nombreMaquina = "ad003";
			rutaFichero = "/logs/de/BaD/kynf/online/multipais/web/logs/";
			
		}
		
		
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		String dateInString = Utils.GetTodayDate();
		 
		Date fecha = formatter.parse(dateInString + " 00:00:00");
		

	
		try
		{
			//Si ficheroLogs es de hoy no lo descargamos
			File ficheroAntiguo = new File("results/"+nombreficheroLogs);
			String ficheroLogs = "";
			
			if (ficheroAntiguo.exists() && fecha.compareTo(new Date(ficheroAntiguo.lastModified())) < 0) {
				ficheroLogs = ficheroAntiguo.getAbsolutePath();
			} else {
				
				ficheroLogs = Utils.downloadParticularFileSFTP(rutaFichero, nombreficheroLogs, userMaquina, passMaquina, nombreMaquina, fecha);

			}
			String ivTicket = Funciones.getIVTicket(ficheroLogs, ref, usuario);
			return ivTicket;
		}
		catch(Exception e)
		{
			throw new Exception("ERROR. No se pudo obtener el iv-ticket para la referencia: "+ref+" y usuario: "+usuario);
		}
	}
	
	
	public static String getEIUser () {
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		
		String eiUser;
		
		try
		{
			eiUser = params.getValorPropiedad("EI_USER").toLowerCase();
		}
		catch(Exception e)
		{
			eiUser = Constantes.usuarioSFT_PEI;
		}
		
		if(eiUser == null || eiUser.equalsIgnoreCase(""))
			eiUser = Constantes.usuarioSFT_PEI;
		
		return eiUser;
	}
	
	public static String getEIPass () {
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		
		String eiPass;
		
		try
		{
			eiPass = params.getValorPropiedad("EI_PASS").toLowerCase();
		}
		catch(Exception e)
		{
			eiPass = Constantes.passSFTP_EI;
		}
		
		if(eiPass == null || eiPass.equalsIgnoreCase(""))
			eiPass = Constantes.passSFTP_EI;
		
		return eiPass;
	}
	
	public static void mostrarInputOcultoSubiendoFichero (WebDriver driver, String fichero) throws Exception {
		
		//comprobamos si aparece el selector de mapas
		if (Utils.isDisplayed(driver, "//div[@class='ui-selector']", 15))
		{
			//si aparece seleccionamos el est�ndar AEB
			Utils.clickEnElVisiblePrimero(driver, "//div[@class='ui-selector']/div[@class='selected-option']");
			Utils.clickEnElVisiblePrimero(driver, "//div[text()='Est�ndar AEB']");
			Thread.sleep(1500);
		}
		
		//Obtenemos el elemento input oculto
		WebElement element = driver.findElement(By.xpath(Constantes.inputrecuadroSubirfichero));
		
		//Modificamos el estilo del elemento para que sea visible y habilitado
		Utils.ejecutarJS(driver, "document.querySelector('" + Constantes.inputrecuadroSubirficheroCSS + "').setAttribute('style', '');");
		
		//Subimos el fichero
		File archivoParaSubir = new File(fichero);
		element.sendKeys(archivoParaSubir.getAbsolutePath());

		// SUBIDA DEL FICHERO a AZURE

		String dns = Constantes.dnsAzure;

		TestApplet test = new TestApplet();
		test.SubirFicheroAzure(archivoParaSubir.getAbsolutePath());

		String ficheroSubido = dns + "/" + archivoParaSubir.getName();
		
		String codigoFicheroSubido = "<a Style=\"color: #2AABDF;\" href=\""
				+ ficheroSubido
				+ "\" target=\"_blank\" download=\""+archivoParaSubir.getName()+"\"> "+archivoParaSubir.getName()+" </a>";

		
//		Log.write("Vamos a incorporar el siguiente fichero: "+"<html><a href='../"+ fichero + "' target='_blank'>" + new File(fichero).getName() + "</a></html>");
		Log.write("Vamos a incorporar el siguiente fichero: "+"<html>"+codigoFicheroSubido+"</html>");

		Thread.sleep(2500);
	}
	
	public static void introducirClaveAcceso (WebDriver driver, String claveAcceso) throws Exception {
		
		if (Utils.isDisplayed(driver, Constantes.campoClaveAcceso, 5)){
			Utils.introducirTextoEnElVisible2(driver, Constantes.campoClaveAcceso, claveAcceso);
			Utils.clickEnElVisible(driver, Constantes.botonContinuarAcceso);
			Utils.esperarProcesandoPeticionMejorado(driver);				
		}
		
	}
	
	public static boolean estamosConNuevosEstilos (WebDriver driver) throws Exception {
		return Utils.isDisplayed(driver, Constantes.xpathArqInformacional, 5);
	}
	
	public static void firmarOrdenDispositivoMovilDesdeLogSinClaveOP(WebDriver driver, String ref, String usuario) throws Exception
	{
		
		//Este m�todo usa el PauServer.log
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosFijos");
		String rutaLogsFirmas = params.getValorPropiedad("rutaLogsFirmas");
		String ficheroLogsFirmas = params.getValorPropiedad("nombreFicheroLogsfirmas");
		String userMaquina = params.getValorPropiedad("userLogsFirmas");
		String passMaquina = params.getValorPropiedad("passLogsFirmas");
		String nombreMaquina = params.getValorPropiedad("maquinaLogsFirmas");
		
		String entorno = params.getValorPropiedad("entorno");

		if (!entorno.equalsIgnoreCase("Produccion")) {
			try
			{
				Usuario UsuarioValidar = Usuario.obtenerUsuarioPorReferenciaCodigo(rutaCSV, params.getValorPropiedad("referenciaPorDefecto"), params.getValorPropiedad("usuarioPorDefecto"));
				if(UsuarioValidar == null)
					throw new Exception("No hay usuario para realizar la validaci�n");
				Thread.sleep(5000);
				Utils.esperaHastaAparece(driver, Constantes.firmasLinkSMS, 10);
				//Utils.introducirTextoEnElVisible2(driver, Constantes.firmasCampoResultadoFormula, UsuarioValidar.getClaveOp());
				Utils.crearBloqueo();
				Date ahora = new Date();
				Utils.clickEnElVisible(driver, Constantes.firmasLinkSMS);
				Utils.esperarVisibleTiempo(driver, Constantes.firmasLinkDispositivoFisico, 30);
				String ficheroLogsfirmas = Utils.downloadParticularFileSFTP(rutaLogsFirmas, ficheroLogsFirmas, userMaquina, passMaquina, nombreMaquina, ahora);
				String semilla = Funciones.obtenerOTP(ficheroLogsfirmas, ref, usuario);
				Utils.introducirTextoEnElVisible2(driver, Constantes.firmasCampoDispositivoSeguridad, semilla);
				Utils.clickEnElVisible(driver, Constantes.firmasBotonContinuarFirma);
				Utils.esperarProcesandoPeticionFirmas(driver);
				Utils.liberarBloqueo();
				Thread.sleep(5000);
			}
			catch(Exception e)
			{
				throw new Exception("ERROR. No se ha podido realizar la firma correctamente");
			}
		}
		
		
	}
	
	public static String obtenerOTP(String fich, String ref, String usuario) throws Exception
	{
		FileReader fr = new FileReader(fich);
		BufferedReader bf = new BufferedReader(fr);
		String sCadena;
		String cadenaSemilla = "";
		String cadenaSemillaRTS = "";
		int indice;
		
		usuario = usuario.toUpperCase();
		
		while(((sCadena = bf.readLine()) != null))
		{
			if((sCadena.contains(ref + usuario)) && ((sCadena.contains("\"otp\":"))))
				cadenaSemilla = sCadena;
			if((sCadena.contains(ref + usuario)) && ((sCadena.contains("OTP generada ["))))
				cadenaSemillaRTS = sCadena;
		}
		fr.close();

		if(cadenaSemilla.equals("")&&cadenaSemillaRTS.equals(""))
			throw new Exception("No se encontr� la semilla para la referencia: " + ref + " y usuario: " + usuario);

		
		if (!cadenaSemillaRTS.equals("")){
			indice = cadenaSemillaRTS.lastIndexOf("OTP generada [");
			cadenaSemillaRTS = cadenaSemillaRTS.substring(indice +  14, cadenaSemillaRTS.length()-1);
			cadenaSemilla=cadenaSemillaRTS;
		}
			
		else if(!cadenaSemilla.equals("")){
			indice = cadenaSemilla.lastIndexOf("\"otp\":");
			cadenaSemilla = cadenaSemilla.substring(indice -2 + 9, cadenaSemilla.length() - 2);
		}
		
				
		return cadenaSemilla;
	}
	
	public static boolean estamosConLoginNuevo (WebDriver driver) throws Exception {
		return !Utils.isDisplayed(driver, Constantes.logoCabeceraDegradadoAntiguo, 3);
	}
	
	public static void mostrarInputOcultoSubiendoFicheroMapaDedicado (WebDriver driver, String fichero, String mapaDedicado) throws Exception {
		
//		//comprobamos si aparece el selector de mapas
		if (Utils.isDisplayed(driver, "//div[@class='ui-selector']", 15))
		{
			//si aparece seleccionamos el est�ndar AEB
			Utils.clickEnElVisiblePrimero(driver, "//div[@class='ui-selector']/div[@class='selected-option']");
			Utils.clickEnElVisiblePrimero(driver, "//div[text()='"+mapaDedicado+"']");
		}
		
//		Utils.seleccionarUISelectorContieneXPath(driver, Constantes.comboGenericoSubidaFichero, mapaDedicado);
		
		//Obtenemos el elemento input oculto
		WebElement element = driver.findElement(By.xpath(Constantes.inputrecuadroSubirfichero));
		
		//Modificamos el estilo del elemento para que sea visible y habilitado
		Utils.ejecutarJS(driver, "document.querySelector('" + Constantes.inputrecuadroSubirficheroCSS + "').setAttribute('style', '');");
		
		//Subimos el fichero
		File archivoParaSubir = new File(fichero);
		element.sendKeys(archivoParaSubir.getAbsolutePath());

		// SUBIDA DEL FICHERO a AZURE

		String dns = Constantes.dnsAzure;

		TestApplet test = new TestApplet();
		test.SubirFicheroAzure(archivoParaSubir.getAbsolutePath());

		String ficheroSubido = dns + "/" + archivoParaSubir.getName();
		
		String codigoFicheroSubido = "<a Style=\"color: #2AABDF;\" href=\""
				+ ficheroSubido
				+ "\" target=\"_blank\" download=\""+archivoParaSubir.getName()+"\"> "+archivoParaSubir.getName()+"  </a>";

		
//				Log.write("Vamos a incorporar el siguiente fichero: "+"<html><a href='../"+ fichero + "' target='_blank'>" + new File(fichero).getName() + "</a></html>");
		Log.write("Vamos a incorporar el siguiente fichero: "+"<html>"+codigoFicheroSubido+"</html>");
		
		
		Thread.sleep(2500);	
	}

	
	public static void irAPortada (WebDriver driver) throws Exception {
		
		if (!Utils.isDisplayed(driver, Constantes.logoBBVA, 5)){
			driver.switchTo().defaultContent();
		}
		
		Utils.clickEnElVisiblePrimero(driver, Constantes.logoBBVA);
			
		
		
	}


	public static void comprobarPorcentajeValidacion(WebDriver driver, Resultados resultado, String porcentajeRestante, List<String> listaNombresValidadores) throws Exception
	{
		
		double porcentajeRestanteDouble = Double.parseDouble(porcentajeRestante);
		int porcentajeValidadoDouble = 100 - Integer.parseInt(porcentajeRestante);
		int porcentajeValidadoAlternativo = 99 - Integer.parseInt(porcentajeRestante);
		
		double porcentajeValidadoDoubleRound = Math.round((100.00 - Double.parseDouble(porcentajeRestante) - 0.01)*100.00)/100.00;
		
		String contenidoDesplegado = driver.findElement(By.xpath(Constantes.DivDesplegado)).getText().toLowerCase();

		//Utils.vp(driver, resultado, "Porcentaje del " + porcentaje + " de validaci�n", true, contenidoDesplegado.indexOf(porcentaje) > -1, "FALLO");

		Utils.vp(driver, resultado, "Comprobamos que aparece el texto Pendiente de Validar el "+ porcentajeRestante, true, contenidoDesplegado.contains(porcentajeRestante), "FALLO");

		//Verifico la barra restante
		Utils.vp(driver, resultado, "Comprobamos que la barra restante es: "+ porcentajeRestanteDouble, true, Utils.isDisplayed(driver, "//div[(@class='barra_abajoResto')][contains(@style,'"+porcentajeRestante+"')]",5), "FALLO");

		//Verifico la barra pintada
		Utils.vp(driver, resultado, "Comprobamos que la barra pintada es: "+ porcentajeValidadoDouble, true, Utils.isDisplayed(driver, "//div[contains(@class,'barra_abajoPorcentaje')][contains(@style,'"+String.valueOf(porcentajeValidadoDouble)+"') or contains(@style,'"+String.valueOf(porcentajeValidadoDoubleRound)+"') or contains(@style,'"+String.valueOf(porcentajeValidadoAlternativo)+"')]",5), "FALLO");


		//Verifico los validadores
		for (int i = 0; i < listaNombresValidadores.size(); i++) {
			Utils.vp(driver, resultado, "Comprobamos que aparece el nombre del validador: " + listaNombresValidadores.get(i), true, contenidoDesplegado.toUpperCase().contains(listaNombresValidadores.get(i).toUpperCase()), "FALLO");			
		}

		
		
	}
	
	public static void accederAgendaComun(WebDriver driver) throws Exception
	{
		// Acceder a la agenda Com�n
		driver.switchTo().defaultContent();
		Utils.esperarVisibleTiempo(driver, Constantes.botonAgendaVisible, 10);
		Atomic.click(driver, Constantes.botonAgendaVisible);
		Utils.esperarProcesandoPeticionMejorado(driver);
		Utils.cambiarFramePrincipal(driver);
		Utils.esperaHastaApareceSinSalir(driver, Constantes.nuevoContactoAgenda, 15);
		Thread.sleep(1000);

	}
	
	public static void aceptarTratamientoDeDatosDeUsuario(WebDriver driver, Resultados resultado, boolean aceptar) throws Exception 
	{
		
		Utils.esperaHastaApareceSinSalir(driver, Constantes.botonAceptarTratamientoDatosUsuario, 2);
			
		if (aceptar)
			Utils.clickEnElVisible(driver, Constantes.botonAceptarTratamientoDatosUsuario);
		else
			Utils.clickEnElVisible(driver, Constantes.botonCancelarTratamientoDatosUsuario);
		

//		Utils.esperaHastaApareceSinSalir(driver, Constantes.iframeTratamientoDatosUsuario, 5);
//		
//		Utils.vp(driver, resultado, "Aparece el mensaje para el Tratamiendo de Datos de Usuario", true, Utils.isDisplayed(driver, Constantes.iframeTratamientoDatosUsuario, 2), "FALLO");
//		
//		if (Utils.isDisplayed(driver, Constantes.iframeTratamientoDatosUsuario, 1)) {
//			
//			Utils.cambiarFrameDado(driver, Constantes.iframeTratamientoDatosUsuario);
//			
//			//Se comprueba que se muestre el body del iframe.
//			Utils.vp(driver, resultado, "Se verifica que exista la campa�a de Tratamiento de Datos de Usuario", true, Utils.isDisplayed(driver, Constantes.bodyTratamientoDatosPersonales, 5), "FALLO");
//			Utils.capturaIntermedia(driver, resultado, "Se muestra el iframe de la campa�a de Tratamiento de Datos de Usuario");	
//			
//			//Se verifica que aparezca en la campa�a el titulo de POL�TICA DE PROTECCI�N DE DATOS DE USUARIOS
//			Utils.vp(driver, resultado, "Se comprueba que aparezca el titulo de 'POL�TICA DE PROTECCI�N DE DATOS DE USUARIOS'", true,Utils.existeElemento(driver, Constantes.tituloTratamientoDatosUsuario, 5), "FALLO");
//			
//			if (!Utils.isDisplayed(driver, Constantes.botonAceptarTratamientoDatosUsuario, 2)){			
//				driver.switchTo().defaultContent();
//				if (!Utils.isDisplayed(driver, Constantes.botonAceptarTratamientoDatosUsuario, 1))
//					Utils.cambiarFramePrincipal(driver);
//			}
//						
//			Utils.vp(driver, resultado, "Se comprueba que exista el bot�n 'Aceptar' la politica de tratamiento de datos de usuario", true, Utils.existeElemento(driver, "//button[(@id='aceptarPolitica')]", 5), "FALLO");
//			Utils.vp(driver, resultado, "Se comprueba que exista el bot�n 'Cancelar' la politica de tratamiento de datos de usuario", true, Utils.isDisplayed(driver, Constantes.botonCancelarTratamientoDatosUsuario, 5), "FALLO");
//		
//			if (aceptar)
//				Utils.clickEnElVisible(driver, Constantes.botonAceptarTratamientoDatosUsuario);
//			else
//				Utils.clickEnElVisible(driver, Constantes.botonCancelarTratamientoDatosUsuario);
//		}
//		
//		else if (Utils.isDisplayed(driver, Constantes.botonAceptarTratamientoDatosUsuario, 1)) {
//			Utils.clickEnElVisible(driver, Constantes.botonAceptarTratamientoDatosUsuario);
//		}
		
	}
	
	public static String devuelveNombreUsuarioLogado () throws Exception {
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String usuarioPorDefecto = params.getValorPropiedad("usuarioPorDefecto");
		String referenciaPorDefecto = params.getValorPropiedad("referenciaPorDefecto");
		String entorno = params.getValorPropiedad("entorno");
	
		Usuario UsuarioPrueba = Funciones.devuelveUsuarioFijo(referenciaPorDefecto, usuarioPorDefecto, entorno);
		String nombreUsuario = UsuarioPrueba.getNombreUsuario();
		return nombreUsuario;
	}
	
	public static void eliminarOrdenDispositivoMovilDesdeLogSinClaveOP(WebDriver driver, String ref, String usuario) throws Exception
	{
		
		//Este m�todo usa el PauServer.log
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaCSV = params.getValorPropiedad("rutaDatosFijos");
		String rutaLogsFirmas = params.getValorPropiedad("rutaLogsFirmas");
		String ficheroLogsFirmas = params.getValorPropiedad("nombreFicheroLogsfirmas");
		String userMaquina = params.getValorPropiedad("userLogsFirmas");
		String passMaquina = params.getValorPropiedad("passLogsFirmas");
		String nombreMaquina = params.getValorPropiedad("maquinaLogsFirmas");
		
		String entorno = params.getValorPropiedad("entorno");

		if (!entorno.equalsIgnoreCase("Produccion")) {
			try
			{
				Usuario UsuarioValidar = Usuario.obtenerUsuarioPorReferenciaCodigo(rutaCSV, params.getValorPropiedad("referenciaPorDefecto"), params.getValorPropiedad("usuarioPorDefecto"));
				if(UsuarioValidar == null)
					throw new Exception("No hay usuario para realizar la validaci�n");
				Thread.sleep(5000);
				Utils.esperaHastaAparece(driver, Constantes.firmasLinkSMS, 10);
				//Utils.introducirTextoEnElVisible2(driver, Constantes.firmasCampoResultadoFormula, UsuarioValidar.getClaveOp());
				Utils.crearBloqueo();
				Date ahora = new Date();
				Utils.clickEnElVisible(driver, Constantes.firmasLinkSMS);
				Utils.esperarVisibleTiempo(driver, Constantes.firmasLinkDispositivoFisico, 30);
				String ficheroLogsfirmas = Utils.downloadParticularFileSFTP(rutaLogsFirmas, ficheroLogsFirmas, userMaquina, passMaquina, nombreMaquina, ahora);
				String semilla = Funciones.obtenerOTP(ficheroLogsfirmas, ref, usuario);
				Utils.introducirTextoEnElVisible2(driver, Constantes.firmasCampoDispositivoSeguridad, semilla);
				Utils.clickEnElVisible(driver, Constantes.BotonEliminarOrdenPendiente);
				Utils.liberarBloqueo();
				Thread.sleep(5000);
			}
			catch(Exception e)
			{
				throw new Exception("ERROR. No se ha podido eliminar la orden");
			}
		}
		
		
	}



	
}
