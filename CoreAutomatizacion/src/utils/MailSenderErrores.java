package utils;


import java.io.FileReader;

import com.csvreader.CsvReader;

import junit.framework.TestCase;


@SuppressWarnings("unused")
public class MailSenderErrores extends TestCase
{

	@Override
	public void setUp() throws Exception
	{

	}


	public void testEnviaMail() throws Exception
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		//String archivoHTML = "results/ResumenEjecucionPruebas.html";
		
		String archivoCSV = params.getValorPropiedad("rutaResultados");
		CsvReader reader = new CsvReader(new FileReader(archivoCSV));
		

		int resultadosERROR = 0;
		int resultadosERROR2 = 0;



		while(reader.readRecord())
		{
			String testName = reader.get(1);
			String resul = reader.get(2);

			if(resul.indexOf("ERROR") > -1)
				resultadosERROR++;

			else if(resul.equalsIgnoreCase("ERROR*"))
				resultadosERROR2++;



		}
		
		
		if (resultadosERROR>0 || resultadosERROR2>0) {		
			String uuaa = Utils.devuelveUUAAParaMail();	
			Mail.enviaMail("Resumen pruebas Autom�ticas "+uuaa, "Hola, os adjuntamos los resultados de la ejecuci�n autom�tica. El resumen de �sta, ser�a el siguiente:", "","automatizacion-bbva.group@bbva.com");
		}
	}


	@Override
	public void tearDown() throws Exception
	{

	}
}
