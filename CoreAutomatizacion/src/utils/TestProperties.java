package utils;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;


public class TestProperties
{

	// Atributos de la clase
	private Properties propiedades;

	private String archivo;


	public TestProperties(String archivo)
	{
		propiedades = new Properties();
		this.archivo = archivo;
		cargarPropiedades();

	}


	/**
	 * Carga en el objeto de propiedades los datos leidos del archivo
	 * 
	 * @return boolean
	 */
	public boolean cargarPropiedades()
	{
		try
		{
			propiedades.load(new FileInputStream(archivo));
			return true;
		}
		catch(IOException e)
		{
			e.printStackTrace();
			return false;
		}
	}


	/**
	 * Permite leer todas las propiedades almacenadas en el archivo mostrando su
	 * respectivo valor
	 */
	public void readPropiedades()
	{
		// Obtengo una enumeracion de llaves, cada llave permitira
		// obtener un valor dentro del properties
		@SuppressWarnings("rawtypes")
		Enumeration llaves = propiedades.keys();

		// Recorro llave por llave y obtengo su valor
		while(llaves.hasMoreElements())
		{
			String llave = (String)llaves.nextElement();
			System.out.println(llave + "=" + propiedades.getProperty(llave));
		}
	}


	public String getValorPropiedad(String propiedad)
	{
		return propiedades.getProperty(propiedad);
	}


	public void updateProperties(String archivoOrigen, String propiedad, String valor) throws Exception
	{
		FileInputStream in = new FileInputStream(archivoOrigen);
		Properties props = new Properties();
		props.load(in);
		in.close();

		FileOutputStream out = new FileOutputStream("resources/restEjecucion.properties");
		props.setProperty(propiedad, valor);
		props.store(out, null);
		out.close();
	}
	
	public void updateSameProperties(String archivoOrigen, String propiedad, String valor) throws Exception
	{
		FileInputStream in = new FileInputStream(archivoOrigen);
		Properties props = new Properties();
		props.load(in);
		in.close();

		FileOutputStream out = new FileOutputStream(archivoOrigen);
		props.setProperty(propiedad, valor);
		props.store(out, null);
		out.close();
	}

}
