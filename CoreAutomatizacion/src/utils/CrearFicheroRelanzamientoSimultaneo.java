package utils;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import junit.framework.TestCase;


public class CrearFicheroRelanzamientoSimultaneo extends TestCase
{

    public void setUp() throws Exception
    {}


    //1 llamada Xq hay una utilidad que es cocheEScSim ... 8 ?
    
    
    public void testCrearFichero() throws Exception
    {
        TestProperties params = new TestProperties(Utils.getProperties());
        params.cargarPropiedades();

        int numeroEjecucionesSimultaneas = Integer.parseInt(params.getValorPropiedad("numeroEjecucionesSimultaneas"));

        String statText = new File("CocheEscobaSimultaneo.bat").getAbsolutePath();
        System.out.println(statText);
        FileOutputStream is = new FileOutputStream(statText);
        OutputStreamWriter osw = new OutputStreamWriter(is);
        Writer w = new BufferedWriter(osw);

        for(int i = 0; i < numeroEjecucionesSimultaneas; i++)
        {
            w.write("START java -jar MainNuevo.jar CocheEscobaSimultaneo " + i + "\n");
            w.write("timeout 8\n");
            // w.write("START CMD.EXE /C ^(java -jar Main.jar "+i+"^> results\\testMain"+i+".txt^)\n");
        }

        w.write("set timeOut=1\n" + ":bucle\n" + "if %timeOut% GTR 600 goto :end\n" + "if exist \"resources\\\"\\\\*.running (\n" + "Set /A timeOut+=1\n" + "timeout 30\n"
                + "goto :bucle\n" + ") else (\n" + "goto :end\n" + ")\n" + ":end\n" 
//                + "java -jar Main.jar CocheEscoba\n"
                + "java -jar Main.jar ActualizarCSVResultados\n" + "java -jar Main.jar MailSender >results\\MailSender.txt 2>&1"
                
                );


        w.close();

    }


    public void tearDown() throws Exception
    {

    }

}