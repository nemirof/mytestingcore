package utils;


import java.io.File;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ActualizarC204 extends TestCase
{
	public String m_nombre = this.getClass().getSimpleName();

	public String m_codigo = "UTI_0009";

	int contador = 0;

	boolean cadenaValida = false;

	String codigoFicheroHTML = "";

	String codigoFicheroCSV = "";

	public String codigoFicheroC204 = "";


	@Before
	public void setUp() throws Exception
	{
		new Resultados(m_nombre);
	}


	@Test
	public void testActualizarCSVResultados() throws Exception
	{

		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		
		
		
			File c204 = Utils.crearC204();
			String nombreC204 = c204.getName();

//			// SUBIDA DEL FICHERO C204
//
//			String dns = Constantes.dnsAzure;
//
//			TestApplet test = new TestApplet();
//			test.SubirC204Azure(c204.getAbsolutePath());
//
//			String ficheroC204 = dns + "/" + nombreC204;
//			
//			codigoFicheroC204 = "<a Style=\"color: #2AABDF;\" href=\""+ ficheroC204
//					+ "\" target=\"_blank\" download=\""+nombreC204+"\"> <img alt=\"Abrir\" border=\"0\" src=\"https://sites.google.com/a/bbva.com/plataforma_banca_empresas/gestion-y-rendimiento/icGuardarT.gif\">Guardar</a>";

			Utils.crearFicheroTemporal(System.getProperty("user.dir")+"/resources/temp.txt", nombreC204);

	}


	@After
	public void tearDown() throws Exception
	{
//	  Utils.guardarEjecucion(driver,resultado);
	}
}
