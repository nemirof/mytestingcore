package utils;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class DB2Connection
{

	public static Connection DB2connect(String... alias)
	{
		
		String jdbcClassName = "com.ibm.db2.jcc.DB2Driver";
		
		String url = "jdbc:db2://ai_db2_tlsb:50300/BTLBH001";
		String user = "xatlsb1i";//"xakynf1i";
		String password = "gdepssdd";//"gmessdd0";
		
		if (alias.length>0 && alias[0].equalsIgnoreCase("KYGU_EI_DB2")) {		
			url = "jdbc:db2://ai_db2_tlsb:50300/btlsb001";
			user = "xakygu1i";//"xakynf1i";
			password = "gdepssdd";//"gmessdd0";
		}
		

		Connection connection = null;
		try
		{
			// Load class into memory
			Class.forName(jdbcClassName);
			
			// Establish connection
			connection = DriverManager.getConnection(url, user, password);

			Log.write("Conexi�n correcta con DB2.");
			
		}
		
		catch(Exception e)
		{
			System.out.println(e.toString());
		}

		return connection;

	}


	public static String select(String query, String nombreColumna, String... alias) throws SQLException
	{
		Connection connection = DB2connect(alias);
		
		Statement st = null;
		String valor = "";

		try
		{
			
			st = connection.createStatement();
			ResultSet rs = st.executeQuery(query);

			while(rs.next())
			{
				valor = rs.getString(nombreColumna);

			}
			
			Log.write("Se ejecuta correctamente la query");
			
		}
		catch(RuntimeException e)
		{
			e.printStackTrace();
			Log.write("runtime: " + e.toString());
		}		
		catch(Exception e)
		{
			e.printStackTrace();
			Log.write("exception: " +e.toString());
		}
		finally
		{
			
			if(st != null)
			{
				st.close();
			}

			if(connection != null)
			{
				connection.close();
			}
			
		}

		return valor;

	}


	public static boolean update(String query, String... alias) throws Exception
	{
		Connection connection = DB2connect(alias);

		Statement st = null;
		boolean resul = false;

		try
		{
			st = connection.createStatement();
			st.executeUpdate(query);
			connection.commit();

			Log.write("Se ejecuta correctamente el update en DB2");
			resul = true;

		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
		finally
		{
			if(st != null)
			{
				st.close();
			}

			if(connection != null)
			{
				connection.close();
			}
		}
		return resul;


	}

//    public static void main (String[] args){
//    	
//    	String query = "SELECT * FROM TLSB.TTLSBCAB AS TTLSBCAB, TLSB.TTLSBPOR AS TTLSBPOR WHERE TTLSBCAB.COD_CLASEORD IN ('S9R','S9D') AND TTLSBCAB.COD_ESTACASH = '005' AND (TTLSBPOR.COD_CLIECASH = TTLSBCAB.COD_CLIECASH AND TTLSBPOR.COD_CLASEORD = TTLSBCAB.COD_CLASEORD AND TTLSBPOR.COD_IDORDEN = TTLSBCAB.COD_IDORDEN)";
//    	String updateDevoluciones = "UPDATE TLSB.TTLSBCAB SET FEC_PROCESCA = '20151119' WHERE COD_CLIECASH = '0023000120072852' AND COD_CLASEORD = 'S9D' AND COD_IDORDEN = 'hszujkvg'";
//    	String updateRechazo = "UPDATE TLSB.TTLSBCAB SET FEC_PROCESCA = '20151118' WHERE COD_CLIECASH = '0023000120072852' AND COD_CLASEORD = 'S9R' AND COD_IDORDEN = 'hszujyiv'";
//    	
//    	try {
//    		update(updateDevoluciones);
//    		String valor = select(query,"COD_IDORDEN");
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//    }

}
