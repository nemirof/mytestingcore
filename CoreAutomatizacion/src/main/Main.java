package main;


import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import junit.framework.Test;
import junit.framework.TestSuite;
import utils.SeleccionarUUAALanzamiento;
import utils.SeleccionarUUAALanzamientoLatam;
import utils.TestProperties;
import utils.Utils;
import utils.UtilsRest;

import com.csvreader.CsvReader;


public class Main
{

	public static List<String> listaCasosARELanzar = new ArrayList<String>();


	@SuppressWarnings({"rawtypes", "unchecked"})
	public static Test suite(int numeroOcurrencia, int numeroCasosSimultaneos) throws Exception
	{

		TestSuite suite = new TestSuite();
		
		
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String tipoEjecucion = params.getValorPropiedad("tipoEjecucion");
		
		
		//Si no hay TipoEjecucion, lo de siempre
		if (tipoEjecucion==null || tipoEjecucion.equalsIgnoreCase("")) {

			String pathsLanzamiento[] = Utils.getPathsLanzamiento();
	
			for(String path : pathsLanzamiento)
			{
				addTestCaseToSuite(suite, path, numeroOcurrencia, numeroCasosSimultaneos);
			}

		} 
		
		//Si hay tipo, que coincida
		else {
			
			//Todos los csv de la carpeta
			
			File[] pathsLanzamiento = Utils.devuelveArchivosDeUnDirectorio("lanzamiento/", ".csv");
			
			for(File archivo : pathsLanzamiento)
			{
				String ruta = archivo.getAbsolutePath();
				addTestCaseToSuitePorTipo(suite, ruta, numeroOcurrencia, numeroCasosSimultaneos,tipoEjecucion);
			}
		}
		
		
		
		return suite;
	}


	private static void addTestCaseToSuite(final TestSuite suite, final String archivo, int numeroOcurrencia, int numeroCasosSimultaneos) throws Exception
	{

		String outputFile = archivo;
		
		File archivoLanzamiento = new File (archivo);
		
		if (!archivoLanzamiento.exists())
			throw new Exception ("No existe el fichero: "+archivo);

		CsvReader reader = new CsvReader(new FileReader(outputFile));

		System.out.println("Casos a lanzar:");

		int i = 1;

		while(reader.readRecord())
		{
			String nombreCaso = reader.get(1);
			String lanzar = reader.get(2);
			String dependencia = reader.get(3);
			if(lanzar.equalsIgnoreCase("SI"))
			{
				if(numeroCasosSimultaneos == 1)
				{
					System.out.println(nombreCaso);
					Class prueba = Class.forName(nombreCaso);
					suite.addTestSuite(prueba);
				}
				else
				{
					// Campo dependencia informado, todos van a hilo 0
					//TODO solo agregar si padre no ha fallado
					if(numeroOcurrencia == 0)
					{
						if(!dependencia.equals(""))
						{
							System.out.println(nombreCaso);
							Class prueba = Class.forName(nombreCaso);
							suite.addTestSuite(prueba);
						}
					}
					else
					{
						if(dependencia.equals(""))
						{
							if((numeroOcurrencia + i) % (numeroCasosSimultaneos - 1) == 0)
							{
								System.out.println(nombreCaso);
								Class prueba = Class.forName(nombreCaso);
								suite.addTestSuite(prueba);
							}
							i++;
						}

					}
				}
			}

		}
		reader.close();
	}
	
	private static void addTestCaseToSuitePorTipo(final TestSuite suite, final String archivo, int numeroOcurrencia, int numeroCasosSimultaneos, String tipoEjecucion) throws Exception
	{

		String outputFile = archivo;

		CsvReader reader = new CsvReader(new FileReader(outputFile));

		System.out.println("Casos a lanzar:");

		int i = 1;

		while(reader.readRecord())
		{
			String nombreCaso = reader.get(1);
			String lanzar = reader.get(2);
			String dependencia = reader.get(3);
			String tipoCaso = reader.get(4);
			
			//S�lo se a�ade si es del tipo de la ejecucion
			if (tipoCaso.equalsIgnoreCase(tipoEjecucion)) {
			
				if(lanzar.equalsIgnoreCase("SI"))
				{
					if(numeroCasosSimultaneos == 1)
					{
						System.out.println(nombreCaso);
						Class prueba = Class.forName(nombreCaso);
						suite.addTestSuite(prueba);
					}
					else
					{
						// Campo dependencia informado, todos van a hilo 0
						//TODO solo agregar si padre no ha fallado
						if(numeroOcurrencia == 0)
						{
							if(!dependencia.equals(""))
							{
								System.out.println(nombreCaso);
								Class prueba = Class.forName(nombreCaso);
								suite.addTestSuite(prueba);
							}
						}
						else
						{
							if(dependencia.equals(""))
							{
								if((numeroOcurrencia + i) % (numeroCasosSimultaneos - 1) == 0)
								{
									System.out.println(nombreCaso);
									Class prueba = Class.forName(nombreCaso);
									suite.addTestSuite(prueba);
								}
								i++;
							}
	
						}
					}
				}
			}
		}
		reader.close();
	}


	@SuppressWarnings({"rawtypes", "unchecked"})
	public static Test suiteCocheEscoba() throws Exception
	{
		TestSuite suite = new TestSuite();
		
		
		
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String tipoEjecucion = params.getValorPropiedad("tipoEjecucion");
		
		//Si no hay TipoEjecucion, lo de siempre
		if (tipoEjecucion==null || tipoEjecucion.equalsIgnoreCase("")) {

			String pathsLanzamiento[] = Utils.getPathsLanzamiento();
	
			for(String path : pathsLanzamiento)
			{
				addCocheEscobaTestSuite(suite, path);
			}
			
		}
		
		else {
			
			//Todos los csv de la carpeta
			
			File[] pathsLanzamiento = Utils.devuelveArchivosDeUnDirectorio("lanzamiento/", ".csv");
			
			for(File archivo : pathsLanzamiento)
			{
				String ruta = archivo.getAbsolutePath();
				addCocheEscobaTestSuitePorTipo(suite, ruta, tipoEjecucion);
			}
		}
		
		return suite;
	}


	private static void addCocheEscobaTestSuite(final TestSuite suite, final String outputFile) throws Exception
	{

		CsvReader reader = new CsvReader(new FileReader(outputFile));

		System.out.println("Casos a RE-lanzar:");


		while(reader.readRecord())
		{
			String nombreCaso = reader.get(1);
			String testID = reader.get(0);
			String lanzar = reader.get(2);
			String dependeDe = reader.get(3);

			// Los de instalaci�n no los repetimos.
			if(!nombreCaso.startsWith("instalacion."))
			{

				// Se podr�a marcar que han sido ejecutados por segunda vez para que el informe lo tuviera
				// en cuenta


				// Si se tiene que lanzar, pero depende de uno previo, a�adimos �ste y del que depende
				if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(testID).equalsIgnoreCase("ERROR") && !dependeDe.equalsIgnoreCase(""))
				{

					if(!dependeDe.equalsIgnoreCase(testID))
					{
						//String casoPadre = Utils.devuelveCasoDelQueDepende(dependeDe);
						String casoPadre = Utils.nombreCaso(dependeDe);

						// S�lo a�adimos si no estaba ya
						if(!listaCasosARELanzar.contains(casoPadre))
						{
							listaCasosARELanzar.add(casoPadre);
							Class pruebaPadre = Class.forName(casoPadre);
							suite.addTestSuite(pruebaPadre);
							System.out.println("Caso Padre: " + casoPadre);
						}

					}

					if(!listaCasosARELanzar.contains(nombreCaso))
					{
						listaCasosARELanzar.add(nombreCaso);
						Class prueba = Class.forName(nombreCaso);
						suite.addTestSuite(prueba);
						System.out.println("Caso Hijo: " + nombreCaso);
					}


				}

				// Errores sin dependencia
				else if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(testID).equalsIgnoreCase("ERROR"))
				{
					if(!listaCasosARELanzar.contains(nombreCaso))
					{
						listaCasosARELanzar.add(nombreCaso);
						System.out.println(nombreCaso);
						Class prueba = Class.forName(nombreCaso);
						suite.addTestSuite(prueba);
					}

				}

				// NO EJECUTADOS que dependen de otro que haya fallado
				else if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(testID).equalsIgnoreCase("NO EJECUTADO") && !dependeDe.equalsIgnoreCase(""))
				{
					// Miramos si el Padre FALL�
					String casoPadre = Utils.devuelveCodigoCasoDelQueDepende(dependeDe);
					if(Utils.estadoCaso(casoPadre).equals("ERROR"))
					{
						if(!listaCasosARELanzar.contains(nombreCaso))
						{
							listaCasosARELanzar.add(nombreCaso);
							System.out.println(nombreCaso);
							Class prueba = Class.forName(nombreCaso);
							suite.addTestSuite(prueba);
						}
					}
				}
			}
		}

		reader.close();
	}
	
	private static void addCocheEscobaTestSuitePorTipo(final TestSuite suite, final String outputFile, String tipoEjecucion) throws Exception
	{

		CsvReader reader = new CsvReader(new FileReader(outputFile));

		System.out.println("Casos a RE-lanzar:");


		while(reader.readRecord())
		{
			String nombreCaso = reader.get(1);
			String testID = reader.get(0);
			String lanzar = reader.get(2);
			String dependeDe = reader.get(3);
			String tipoCaso = reader.get(4);

			// Los de instalaci�n no los repetimos.
			if(!nombreCaso.startsWith("instalacion."))
			{

				// Se podr�a marcar que han sido ejecutados por segunda vez para que el informe lo tuviera
				// en cuenta

				if (tipoCaso.equalsIgnoreCase(tipoEjecucion)) {

				// Si se tiene que lanzar, pero depende de uno previo, a�adimos �ste y del que depende
					if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(testID).equalsIgnoreCase("ERROR") && !dependeDe.equalsIgnoreCase(""))
					{
	
						if(!dependeDe.equalsIgnoreCase(testID))
						{
							String casoPadre = Utils.devuelveCasoDelQueDepende(dependeDe);
	
							// S�lo a�adimos si no estaba ya
							if(!listaCasosARELanzar.contains(casoPadre))
							{
								listaCasosARELanzar.add(casoPadre);
								Class pruebaPadre = Class.forName(casoPadre);
								suite.addTestSuite(pruebaPadre);
								System.out.println("Caso Padre: " + casoPadre);
							}
	
						}
	
						if(!listaCasosARELanzar.contains(nombreCaso))
						{
							listaCasosARELanzar.add(nombreCaso);
							Class prueba = Class.forName(nombreCaso);
							suite.addTestSuite(prueba);
							System.out.println("Caso Hijo: " + nombreCaso);
						}
	
	
					}
	
					// Errores sin dependencia
					else if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(testID).equalsIgnoreCase("ERROR"))
					{
						if(!listaCasosARELanzar.contains(nombreCaso))
						{
							listaCasosARELanzar.add(nombreCaso);
							System.out.println(nombreCaso);
							Class prueba = Class.forName(nombreCaso);
							suite.addTestSuite(prueba);
						}
	
					}
	
					// NO EJECUTADOS que dependen de otro que haya fallado
					else if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(testID).equalsIgnoreCase("NO EJECUTADO") && !dependeDe.equalsIgnoreCase(""))
					{
						// Miramos si el Padre FALL�
						String casoPadre = Utils.devuelveCodigoCasoDelQueDepende(dependeDe);
						if(Utils.estadoCaso(casoPadre).equals("ERROR"))
						{
							if(!listaCasosARELanzar.contains(nombreCaso))
							{
								listaCasosARELanzar.add(nombreCaso);
								System.out.println(nombreCaso);
								Class prueba = Class.forName(nombreCaso);
								suite.addTestSuite(prueba);
							}
						}
					}
				}
			}
		}

		reader.close();
	}


	@SuppressWarnings({"unchecked", "rawtypes"})
	public static void main(String[] args) throws Exception
	{

		// Cargamos las propiedades del properties
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaResultados = params.getValorPropiedad("rutaResultados");
		String ejecutarREST = params.getValorPropiedad("serviciosREST");
		String pais = params.getValorPropiedad("pais");
		int numeroCasosSimultaneos = Integer.parseInt(params.getValorPropiedad("numeroEjecucionesSimultaneas"));

		// Si nos llega como argumento EliminarUsuarios ejecutamos esa utilidad
		if(args.length == 1 && args[0].equals("EliminarUsuarios"))
		{
			Class utilidad = Class.forName("utils." + args[0]);
			junit.textui.TestRunner.run(utilidad);
		}

		else if(args.length == 1 && args[0].equals("CrearFicheroLanzamiento"))
		{
			Class utilidad = Class.forName("utils." + args[0]);
			junit.textui.TestRunner.run(utilidad);
		}

		// Servicios REST
		else if(args.length == 1 && args[0].equals("REST"))
		{


			String entorno = params.getValorPropiedad("entorno");

			if(entorno.equalsIgnoreCase("Produccion"))
			{
				System.out.println("La prueba de los servicios REST s�lo aplica en Entornos de Desarrollo e Integrado");
			}

			else
			{

				if(ejecutarREST.equalsIgnoreCase("SI"))
				{

					Class utilidad = Class.forName("utils.RestClientMejorado");
					Class datosRest = Class.forName("utils.DatosParaRest");
					Class subirCSV = Class.forName("utils.REST_ActualizarCSVResultados");
					Class actualizarC204Rest = Class.forName("utils.REST_ActualizarC204");

					// Escribimos informaci�n de traza
					System.out.println("Empieza la ejecuci�n de los servicios REST...");

					java.util.Date hoy = new java.util.Date();
					SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
					String horaInicio = formatter2.format(hoy);

					TestSuite suiteRest = new TestSuite();

					suiteRest.addTestSuite(datosRest);
					suiteRest.addTestSuite(utilidad);

					// Ejecutamos la suite de REST
					junit.textui.TestRunner.run(suiteRest);

					java.util.Date fin = new java.util.Date();
					String horaFin = formatter2.format(fin);
					UtilsRest.CSVToTableRest(hoy, horaInicio, horaFin, null);

					// Actualizar C204
					suiteRest = new TestSuite();
					suiteRest.addTestSuite(actualizarC204Rest);
					junit.textui.TestRunner.run(suiteRest);

					suiteRest = new TestSuite();
					suiteRest.addTestSuite(subirCSV);
					junit.textui.TestRunner.run(suiteRest);

				}
			}
		}

		else if(args.length == 1 && args[0].contains("REST"))
		{


			String entorno = params.getValorPropiedad("entorno");

			if(entorno.equalsIgnoreCase("Produccion"))
			{
				System.out.println("La prueba de los servicios REST s�lo aplica en Entornos de Desarrollo e Integrado");
			}

			else
			{

				if(ejecutarREST.equalsIgnoreCase("SI"))
				{

					Class utilidad = Class.forName("utils.RestClientMejorado");

					Class datosRest = null;

					if(args[0].equals("REST1"))
						datosRest = Class.forName("utils.DatosParaRest1");
					else if(args[0].equals("REST2"))
						datosRest = Class.forName("utils.DatosParaRest2");
					else if(args[0].equals("REST3"))
						datosRest = Class.forName("utils.DatosParaRest3");
					else if(args[0].equals("REST4"))
						datosRest = Class.forName("utils.DatosParaRest4");

					// Escribimos informaci�n de traza
					System.out.println("Empieza la ejecuci�n de los servicios REST...");

					if(args.length == 0)
						junit.textui.TestRunner.run(suite(0, 1));
					else
					{
						System.out.println("REST Hilo: " + args[0]);
						Utils.crearFicheroRunning(args[0]);

						TestSuite suiteRest = new TestSuite();
						suiteRest.addTestSuite(datosRest);
						suiteRest.addTestSuite(utilidad);

						// Ejecutamos la suite de REST
						junit.textui.TestRunner.run(suiteRest);
					}


					if(args.length > 0)
						Utils.borrarFicheroRunning(args[0]);

				}
			}
		}

		else if(args.length == 1 && args[0].equals("SubirEjecucionREST"))
		{

			Class utilidad = Class.forName("utils.RestClientMejorado");

			java.util.Date hoy = new java.util.Date();
			SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
			String horaInicio = formatter2.format(hoy);

			TestSuite suiteRest = new TestSuite();

			suiteRest.addTestSuite(utilidad);

			// Ejecutamos la suite de REST
			junit.textui.TestRunner.run(suiteRest);

			java.util.Date fin = new java.util.Date();
			String horaFin = formatter2.format(fin);
			UtilsRest.CSVToTableRest(hoy, horaInicio, horaFin, null);

			Class subirCSV = Class.forName("utils.REST_ActualizarCSVResultados");
			Class actualizarC204Rest = Class.forName("utils.REST_ActualizarC204");

			// Actualizar C204
			suiteRest = new TestSuite();
			suiteRest.addTestSuite(actualizarC204Rest);
			junit.textui.TestRunner.run(suiteRest);

			suiteRest = new TestSuite();
			suiteRest.addTestSuite(subirCSV);
			junit.textui.TestRunner.run(suiteRest);

		}

		else if(args.length == 1 && args[0].equals("ActualizarCSVResultados"))
		{
			String SubeGoogleDrive = params.getValorPropiedad("actualizarGoogleDrive");
			if(SubeGoogleDrive.equalsIgnoreCase("SI"))
			{
				Class utilidad = Class.forName("utils." + args[0]);
				junit.textui.TestRunner.run(utilidad);
			}
		}
		
		else if(args.length == 1 && args[0].equals("SeleccionarUUAALanzamiento"))
		{
			
				SeleccionarUUAALanzamiento lanzamientoGlobal = new SeleccionarUUAALanzamiento();
				lanzamientoGlobal.main();
			
		}
		
		else if(args.length == 1 && args[0].equals("SeleccionarUUAALanzamientoLatam"))
		{
			
				SeleccionarUUAALanzamientoLatam lanzamientoGlobal = new SeleccionarUUAALanzamientoLatam();
				lanzamientoGlobal.main();
			
		}
		
		else if(args.length == 1 && args[0].equals("ActualizarCSVGlobal"))
		{

				Class utilidad = Class.forName("utils." + args[0]);
				junit.textui.TestRunner.run(utilidad);
			
		}
		
		else if(args.length == 1 && args[0].startsWith("UTILIDAD_"))
		{
			//Para llamar cualquier utilidad en formato: UTILIDAD_PAQUETE.NOMBRECLASE
			String nombreClase = args[0].substring(args[0].indexOf("_") + 1);
			Class utilidad = Class.forName(nombreClase);
			junit.textui.TestRunner.run(utilidad);
			
		}

		else if(args.length == 1 && args[0].equals("ObtenerJson"))
		{
			Class utilidad = Class.forName("utils." + args[0]);
			junit.textui.TestRunner.run(utilidad);
		}

		else if(args.length == 1 && args[0].equals("ActualizarC204"))
		{
			String generarFichero = params.getValorPropiedad("generarFichero");
			if(generarFichero.equalsIgnoreCase("SI"))
			{
				Class utilidad = Class.forName("utils." + args[0]);
				junit.textui.TestRunner.run(utilidad);
			}
		}

		else if(args.length == 1 && args[0].equals("EliminarAlertasCorreos"))
		{
			Class utilidad = Class.forName("utils." + args[0]);
			junit.textui.TestRunner.run(utilidad);
		}

		else if(args.length == 1 && args[0].equals("MailSender"))
		{
			// Cogemos la propiedad de si hay que reejecutar tras la ejecuci�n regular
			String enviaMail = params.getValorPropiedad("enviaMail");
			if(enviaMail.equalsIgnoreCase("SI"))
			{
				Class utilidad = Class.forName("utils." + args[0]);
				junit.textui.TestRunner.run(utilidad);
			}
		}
		
		else if(args.length == 1 && args[0].equals("MailSenderParams"))
		{
			// Cogemos la propiedad de si hay que reejecutar tras la ejecuci�n regular
			String enviaMail = params.getValorPropiedad("enviaMail");
			if(enviaMail.equalsIgnoreCase("SI"))
			{
				Class utilidad = Class.forName("utils." + args[0]);
				junit.textui.TestRunner.run(utilidad);
			}
		}

		else if(args.length > 0 && args[0].equals("CocheEscoba"))
		{
			
//			FileOutputStream fichero = new FileOutputStream("results/CocheEscoba.html", true);
//			PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);
//
//			System.setOut(ficheroSalida);
//			System.setErr(ficheroSalida);


			// Cogemos la propiedad de si hay que reejecutar tras la ejecuci�n regular
			String reEjecutarFallidos = params.getValorPropiedad("reEjecutarFallidos");

			// Ejecutamos si adem�s de pasarle el par�metro cocheEscoba, est� en el fichero properties
			// O bien forzamos desde fuera el bat con SI
			if(reEjecutarFallidos.equalsIgnoreCase("SI") || (args.length > 1 && args[1].equals("SI")))
			{
				
				//Eliminamos los running que pudiera haber
				Utils.borrarTodosFicherosConExtension("resources/",".running");

				// Escribimos informaci�n de traza
				System.out.println("Empieza la RE-Ejecuci�n de los casos fallidos...");

				// Fecha de inicio
				java.util.Date hoy = new java.util.Date();
				SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
				String horaInicio = formatter2.format(hoy);

				junit.textui.TestRunner.run(suiteCocheEscoba());
				//C204
				String generarFichero = params.getValorPropiedad("generarFichero");
				if(generarFichero.equalsIgnoreCase("SI"))
				{
					Class utilidad = Class.forName("utils.ActualizarC204");
					junit.textui.TestRunner.run(utilidad);
				}

				// Si nos ped�an el resultado en HTML, lo creamos
				String formatoResultados = params.getValorPropiedad("formatoResultados");

				java.util.Date fin = new java.util.Date();
				String horaFin = formatter2.format(fin);


				if(formatoResultados.indexOf("html") > -1)
				{
					// Leemos la hora de inicio si alg�n hilo ya la dej�
					Utils.crearBloqueo("timeLock", 60);
					File time = new File("resources/time.txt");
					if(time.exists())
					{
						Scanner scanner = new Scanner(time, "UTF-8");
						String text = scanner.useDelimiter("\\A").next();
						horaInicio = text;
						scanner.close();
						time.delete();
					}
					Utils.liberarBloqueo("timeLock");
					Utils.CSVToTableNew(hoy, horaInicio, horaFin, listaCasosARELanzar);
				}
			}
			
			
		}
		
		else if(args.length > 0 && args[0].equals("EsperaYCocheEscoba"))
		{
			
			//Esperamos x minutos
			String timeOutEjecucion = params.getValorPropiedad("timeOutEjecucion");
			
			if (timeOutEjecucion==null || timeOutEjecucion.equalsIgnoreCase("")) 
				timeOutEjecucion = "300";
			
			int timeOutGlobal = Integer.parseInt(timeOutEjecucion) * 6;
			
			int contador = 1;
			
			boolean hayRunning = Utils.numeroFicherosEnRutaConUnPatron("resources/",".running")>0;
			
			while (contador < timeOutGlobal && hayRunning) {
				
				if (Utils.numeroFicherosEnRutaConUnPatron("resources/",".running")==0) 
					hayRunning = false;
				
				else
					Thread.sleep(10000);

				contador = contador + 1;
				
				
			}
			
//			FileOutputStream fichero = new FileOutputStream("results/CocheEscoba.html", true);
//			PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);
//
//			System.setOut(ficheroSalida);
//			System.setErr(ficheroSalida);


			// Cogemos la propiedad de si hay que reejecutar tras la ejecuci�n regular
			String reEjecutarFallidos = params.getValorPropiedad("reEjecutarFallidos");

			// Ejecutamos si adem�s de pasarle el par�metro cocheEscoba, est� en el fichero properties
			// O bien forzamos desde fuera el bat con SI
			if(reEjecutarFallidos.equalsIgnoreCase("SI") || (args.length > 1 && args[1].equals("SI")))
			{
				
				//Eliminamos los running que pudiera haber
				Utils.borrarTodosFicherosConExtension("resources/",".running");

				// Escribimos informaci�n de traza
				System.out.println("Empieza la RE-Ejecuci�n de los casos fallidos...");

				// Fecha de inicio
				java.util.Date hoy = new java.util.Date();
				SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
				String horaInicio = formatter2.format(hoy);

				junit.textui.TestRunner.run(suiteCocheEscoba());
				//C204
				String generarFichero = params.getValorPropiedad("generarFichero");
				if(generarFichero.equalsIgnoreCase("SI"))
				{
					Class utilidad = Class.forName("utils.ActualizarC204");
					junit.textui.TestRunner.run(utilidad);
				}

				// Si nos ped�an el resultado en HTML, lo creamos
				String formatoResultados = params.getValorPropiedad("formatoResultados");

				java.util.Date fin = new java.util.Date();
				String horaFin = formatter2.format(fin);


				if(formatoResultados.indexOf("html") > -1)
				{
					// Leemos la hora de inicio si alg�n hilo ya la dej�
					Utils.crearBloqueo("timeLock", 60);
					File time = new File("resources/time.txt");
					if(time.exists())
					{
						Scanner scanner = new Scanner(time, "UTF-8");
						String text = scanner.useDelimiter("\\A").next();
						horaInicio = text;
						scanner.close();
						time.delete();
					}
					Utils.liberarBloqueo("timeLock");
					Utils.CSVToTableNew(hoy, horaInicio, horaFin, listaCasosARELanzar);
				}
			}
			
			
		}


		else
		// Si no tenemos ning�n argumento, ejecuci�n est�ndar con fichero de lanzamiento
		{
			// Si existe el fichero de resultados y la fecha es de hace m�s de tres horas, lo borramos
			File archivoResultados = new File(rutaResultados);

			boolean alreadyExists = archivoResultados.exists();
			Date fecha = new Date(System.currentTimeMillis() - (3 * 60 * 60 * 1000));

			if(alreadyExists && fecha.compareTo(new Date(archivoResultados.lastModified())) > 0)
			{
				try
				{
					archivoResultados.delete();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}

			// Escribimos informaci�n de traza
			System.out.println("Empieza la ejecuci�n...");

			// Ejecutamos la suite
			java.util.Date hoy = new java.util.Date();
			SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
			String horaInicio = formatter2.format(hoy);

			if(args.length == 0)
				junit.textui.TestRunner.run(suite(0, 1));
			else
			{
				try {
					
					System.out.println("Hilo: " + args[0]);
					Utils.crearFicheroRunning(args[0]);
					junit.textui.TestRunner.run(suite(Integer.parseInt(args[0]), numeroCasosSimultaneos));
					
				} catch (Exception e) {
					Utils.borrarFicheroRunning(args[0]);
					e.printStackTrace();
					System.exit(0);
				}
			}

			// Si nos ped�an el resultado en HTML, lo creamos
			String formatoResultados = params.getValorPropiedad("formatoResultados");

			java.util.Date fin = new java.util.Date();
			String horaFin = formatter2.format(fin);

			// Nos guardamos la hora de inicio por si re-ejecutamos
			Utils.crearBloqueo("timeLock",60);
			File time = new File("resources/time.txt");
			FileWriter fileHtm = new FileWriter(time.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fileHtm);
			bw.write(horaInicio);
			bw.close();
			Utils.liberarBloqueo("timeLock");
			
			// Borramos el fichero de running
			if(args.length > 0)
				Utils.borrarFicheroRunning(args[0]);


			// Si se ped�a el formato HTML, se genera
			if(formatoResultados.indexOf("html") > -1)
			{
				try
				{
					Utils.CSVToTableNew(hoy, horaInicio, horaFin, null);
				}
				catch(FileNotFoundException e)
				{
					// Puede ser que un hilo no tenga nada que ejecutar y har�a el html sin nada
					System.out.println("Error controlado. Hilo que no tiene casos a ejecutar");
				}
				catch(Exception e)
				{
					// Error raro
					e.printStackTrace();
				}

			}


		}

	}
}
