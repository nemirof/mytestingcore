package main;


import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.junit.experimental.ParallelComputer;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import utils.SeleccionarUUAALanzamiento;
import utils.SeleccionarUUAALanzamientoLatam;
import utils.TestProperties;
import utils.Utils;
import utils.UtilsRest;

import com.csvreader.CsvReader;


public class MainNuevo
{
	
	public static List<String> listaCasosDependientes= new ArrayList<String>();
	
	public static List<String> listaCasosResto = new ArrayList<String>();

	public static List<String> listaCasosARELanzar = new ArrayList<String>();
	
	public static List<String> listaCasosDependientesARELanzar = new ArrayList<String>();
	
	public static List<String> listaCasosRestoARELanzar = new ArrayList<String>();
	


	@SuppressWarnings({"rawtypes", "unchecked"})
//	public static Test suite(int numeroOcurrencia, int numeroCasosSimultaneos) throws Exception
//	{
//
//		TestSuite suite = new TestSuite();
//		
//		
//		TestProperties params = new TestProperties(Utils.getProperties());
//		params.cargarPropiedades();
//
//		String tipoEjecucion = params.getValorPropiedad("tipoEjecucion");
//		
//		
//		//Si no hay TipoEjecucion, lo de siempre
//		if (tipoEjecucion==null || tipoEjecucion.equalsIgnoreCase("")) {
//
//			String pathsLanzamiento[] = Utils.getPathsLanzamiento();
//	
//			for(String path : pathsLanzamiento)
//			{
//				addTestCaseToSuite(suite, path, numeroOcurrencia, numeroCasosSimultaneos);
//			}
//
//		} 
//		
//		//Si hay tipo, que coincida
//		else {
//			
//			//Todos los csv de la carpeta
//			
//			File[] pathsLanzamiento = Utils.devuelveArchivosDeUnDirectorio("lanzamiento/", ".csv");
//			
//			for(File archivo : pathsLanzamiento)
//			{
//				String ruta = archivo.getAbsolutePath();
//				addTestCaseToSuitePorTipo(suite, ruta, numeroOcurrencia, numeroCasosSimultaneos,tipoEjecucion);
//			}
//		}
//		
//		
//		
//		return suite;
//	}
	
	private static Class<?>[] suite(int numeroOcurrencia, int numeroCasosSimultaneos) throws Exception
	{

		//Class<?>[] suite = new ArrayList<String>();
		//List<?> suite = new ArrayList<>();
		List<Class> suite = new ArrayList<Class>();
		
		
		
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String tipoEjecucion = params.getValorPropiedad("tipoEjecucion");
		
		
		//Si no hay TipoEjecucion, lo de siempre
		if (tipoEjecucion==null || tipoEjecucion.equalsIgnoreCase("")) {

			String pathsLanzamiento[] = Utils.getPathsLanzamiento();
	
			for(String path : pathsLanzamiento)
			{
				addTestCaseToSuite(suite, path, numeroOcurrencia, numeroCasosSimultaneos);
			}

		} 
		
		//Si hay tipo, que coincida
		else {
			
			//Todos los csv de la carpeta
			
			File[] pathsLanzamiento = Utils.devuelveArchivosDeUnDirectorio("lanzamiento/", ".csv");
			
			for(File archivo : pathsLanzamiento)
			{
				String ruta = archivo.getAbsolutePath();
				addTestCaseToSuitePorTipo(suite, ruta, numeroOcurrencia, numeroCasosSimultaneos,tipoEjecucion);
			}
		}
		
		
		
		return suite.toArray(new Class<?>[suite.size()]);
	}
	
	@SuppressWarnings("rawtypes")
	private static void enviaMail() throws Exception {
		
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		
		//Mail Sender
		String enviaMail = params.getValorPropiedad("enviaMail");
		if(enviaMail.equalsIgnoreCase("SI"))
		{
			Class utilidad = Class.forName("utils.MailSender");
			junit.textui.TestRunner.run(utilidad);
		}
	}
	
	@SuppressWarnings("rawtypes")
	private static void actualizarCSVResultados () throws Exception {
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		
		//Actualizar CSVResultados
		String SubeGoogleDrive = params.getValorPropiedad("actualizarGoogleDrive");
		if(SubeGoogleDrive.equalsIgnoreCase("SI"))
		{
			Class utilidad = Class.forName("utils.ActualizarCSVResultados");
			junit.textui.TestRunner.run(utilidad);
		}
	}
	
	@SuppressWarnings("rawtypes")
	private static void crearC204 () throws Exception {
		
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		
		//C204
		String generarFichero = params.getValorPropiedad("generarFichero");
		if(generarFichero.equalsIgnoreCase("SI"))
		{
			Class utilidad = Class.forName("utils.ActualizarC204");
			junit.textui.TestRunner.run(utilidad);
		}

		Utils.crearInformeHTML(listaCasosARELanzar);
	}
	
	
	public static void cocheEscoba (String[] args) throws Exception {
		
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		
		Utils.borrarTodosFicherosConExtension("resources/",".lock");
		
		Utils.crearFicheroTemporal("resources//fichero.cocheEscoba", "");

//		FileOutputStream fichero = new FileOutputStream("results/CocheEscoba.html", true);
//		PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);
//		System.setOut(ficheroSalida);
//		System.setErr(ficheroSalida);


		// Cogemos la propiedad de si hay que reejecutar tras la ejecuci�n regular
		String reEjecutarFallidos = params.getValorPropiedad("reEjecutarFallidos");

		// Ejecutamos si adem�s de pasarle el par�metro cocheEscoba, est� en el fichero properties
		// O bien forzamos desde fuera el bat con SI
		if(reEjecutarFallidos.equalsIgnoreCase("SI") || (args.length > 1 && args[1].equals("SI")))
		{
			
			//Eliminamos los running que pudiera haber
			Utils.borrarTodosFicherosConExtension("resources/",".running");

			// Escribimos informaci�n de traza
			System.out.println("Empieza la RE-Ejecuci�n de los casos fallidos...");

			JUnitCore.runClasses(suiteCocheEscoba());
			
			//junit.textui.TestRunner.run(suiteCocheEscoba());

		}
		
		//C204

		crearC204();
		
		Utils.crearInformeHTML(listaCasosARELanzar);
		
		Utils.borrarFicheroTemporal("resources//fichero.cocheEscoba");

	}
	
	public static void cocheEscobaGrid (String[] args, String... horaInicio) throws Exception {
		
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		
		Utils.borrarTodosFicherosConExtension("resources/",".lock");
		
		Utils.crearFicheroTemporal("resources//fichero.cocheEscoba", "");

//		FileOutputStream fichero = new FileOutputStream("results/CocheEscoba.html", true);
//		PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);
//
//		System.setOut(ficheroSalida);
//		System.setErr(ficheroSalida);


		// Cogemos la propiedad de si hay que reejecutar tras la ejecuci�n regular
		String reEjecutarFallidos = params.getValorPropiedad("reEjecutarFallidos");

		// Ejecutamos si adem�s de pasarle el par�metro cocheEscoba, est� en el fichero properties
		// O bien forzamos desde fuera el bat con SI
		if(reEjecutarFallidos.equalsIgnoreCase("SI") || (args.length > 1 && args[1].equals("SI")))
		{
			
			//Eliminamos los running que pudiera haber
			Utils.borrarTodosFicherosConExtension("resources/",".running");

			// Escribimos informaci�n de traza
			System.out.println("Empieza la RE-Ejecuci�n de los casos fallidos...");

			List<Class<?>[]> arrayCocheEscoba = suiteCocheEscobaDependientesYNoDependientes();
			Class<?>[] dependientes = arrayCocheEscoba.get(0);
			Class<?>[] resto = arrayCocheEscoba.get(1);
			
			//No Dependientes
			System.out.println("Lanzamos los NO DEPENDIENTES simult�neamente...");
			JUnitCore.runClasses(new ParallelComputer(true, true), resto);	
			
			//Dependientes
			System.out.println("Lanzamos los dependientes..");
			JUnitCore.runClasses(dependientes);
			
			//junit.textui.TestRunner.run(suiteCocheEscoba());

		}
		
		//C204

		crearC204();
		
		Utils.crearInformeHTML(listaCasosARELanzar, horaInicio);
		
		Utils.borrarFicheroTemporal("resources//fichero.cocheEscoba");

	}
	
	public static Class<?>[] suiteCocheEscobaSimultaneo(int numeroOcurrencia, int numeroCasosSimultaneos) throws Exception
    {
        //TestSuite suite = new TestSuite();
        List<Class> suite = new ArrayList<Class>();
        
        
        TestProperties params = new TestProperties(Utils.getProperties());
        params.cargarPropiedades();

        String tipoEjecucion = params.getValorPropiedad("tipoEjecucion");
        
        //Si no hay TipoEjecucion, lo de siempre
        if (tipoEjecucion==null || tipoEjecucion.equalsIgnoreCase("")) {

            String pathsLanzamiento[] = Utils.getPathsLanzamiento();
    
            for(String path : pathsLanzamiento)
            {
                //addCocheEscobaTestSuite o meter condicion de errores dentro (hecha opcion 2 )
                
//                addCocheEscobaTestSuite(suite, path);
                addCocheEscobaTestSuiteSimultaneo(suite, path,numeroOcurrencia,numeroCasosSimultaneos);
            }
            
        }
        
        else {
            
            //Todos los csv de la carpeta
            
            File[] pathsLanzamiento = Utils.devuelveArchivosDeUnDirectorio("lanzamiento/", ".csv");
            
            for(File archivo : pathsLanzamiento)
            {
                String ruta = archivo.getAbsolutePath();
                addCocheEscobaTestSuitePorTipo(suite, ruta, tipoEjecucion);
            }
        }
        
        return suite.toArray(new Class<?>[suite.size()]);
    }
	
	@SuppressWarnings("rawtypes")
	private static void addCocheEscobaTestSuiteSimultaneo(final List<Class> suite, final String archivo, int numeroOcurrencia, int numeroCasosSimultaneos) throws Exception
    {

        String outputFile = archivo;
        
        File archivoLanzamiento = new File (archivo);
        
        if (!archivoLanzamiento.exists())
            throw new Exception ("No existe el fichero: "+archivo);

        CsvReader reader = new CsvReader(new FileReader(outputFile));

        System.out.println("Casos a RE-lanzar:");

        int i = 1;

        while(reader.readRecord())
        {
            String codigoCaso = reader.get(0);
            String nombreCaso = reader.get(1);
            String lanzar = reader.get(2);
            String dependeDe = reader.get(3);
            
            if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(codigoCaso).equalsIgnoreCase("ERROR"))
            {
                if(numeroCasosSimultaneos == 1)
                {
                    System.out.println(nombreCaso);
                    Class prueba = Class.forName(nombreCaso);
                    suite.add(prueba);
                }
                else
                {
                    // Campo dependencia informado, todos van a hilo 0
                    //TODO solo agregar si padre no ha fallado
                    if(numeroOcurrencia == 0)
                    {
                        if(!dependeDe.equals(""))
                        {

                            String casoPadre = Utils.nombreCaso(dependeDe);
                            
                            // S�lo a�adimos si no estaba ya
                            if(!listaCasosARELanzar.contains(casoPadre))
                            {
                                listaCasosARELanzar.add(casoPadre);
                                Class pruebaPadre = Class.forName(casoPadre);
                                suite.add(pruebaPadre);
                                System.out.println("Caso Padre: " + casoPadre);
                            }
                            
                            if(!listaCasosARELanzar.contains(nombreCaso))
                            {
                                System.out.println(nombreCaso);
                                Class prueba = Class.forName(nombreCaso);
                                suite.add(prueba);
                            }
                            
                        }
                    }
                    else
                    {
                        if(dependeDe.equals(""))
                        {
                            if((numeroOcurrencia + i) % (numeroCasosSimultaneos - 1) == 0)
                            {
                                System.out.println(nombreCaso);
                                Class prueba = Class.forName(nombreCaso);
                                suite.add(prueba);
                            }
                            i++;
                        }
                        

                    }
                }
            }
            
            // NO EJECUTADOS que dependen de otro que haya fallado
        else if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(codigoCaso).equalsIgnoreCase("NO EJECUTADO") && !dependeDe.equalsIgnoreCase(""))
        {
            
            if(numeroOcurrencia == 0)
            {
                // Miramos si el Padre FALL�
                String casoPadre = Utils.devuelveCodigoCasoDelQueDepende(dependeDe);
                if(Utils.estadoCaso(casoPadre).equals("ERROR"))
                    {
                        if(!listaCasosARELanzar.contains(nombreCaso))
                        {
                            listaCasosARELanzar.add(nombreCaso);
                            System.out.println(nombreCaso);
                            Class prueba = Class.forName(nombreCaso);
                            suite.add(prueba);
                        }
                    }
            }
        }
            
            

        }
        reader.close();
    }

	@SuppressWarnings("rawtypes")
	private static void addTestCaseToSuite(final List<Class> suite, final String archivo, int numeroOcurrencia, int numeroCasosSimultaneos) throws Exception
	{

		String outputFile = archivo;
		
		File archivoLanzamiento = new File (archivo);
		
		if (!archivoLanzamiento.exists())
			throw new Exception ("No existe el fichero: "+archivo);

		CsvReader reader = new CsvReader(new FileReader(outputFile));

		System.out.println("Casos a lanzar:");

		int i = 1;

		while(reader.readRecord())
		{
			String nombreCaso = reader.get(1);
			String lanzar = reader.get(2);
			String dependencia = reader.get(3);
			if(lanzar.equalsIgnoreCase("SI"))
			{
				if(numeroCasosSimultaneos == 1)
				{
					System.out.println(nombreCaso);
					Class prueba = Class.forName(nombreCaso);
					suite.add(prueba);
				}
				else
				{
					// Campo dependencia informado, todos van a hilo 0
					//TODO solo agregar si padre no ha fallado
					if(numeroOcurrencia == 0)
					{
						if(!dependencia.equals(""))
						{
							System.out.println(nombreCaso);
							Class prueba = Class.forName(nombreCaso);
							suite.add(prueba);
						}
					}
					else
					{
						if(dependencia.equals(""))
						{
							if((numeroOcurrencia + i) % (numeroCasosSimultaneos - 1) == 0)
							{
								System.out.println(nombreCaso);
								Class prueba = Class.forName(nombreCaso);
								suite.add(prueba);
							}
							i++;
						}

					}
				}
			}

		}
		reader.close();
	}
	
	@SuppressWarnings("rawtypes")
	private static void addTestCaseToSuitePorTipo(final List<Class> suite, final String archivo, int numeroOcurrencia, int numeroCasosSimultaneos, String tipoEjecucion) throws Exception
	{

		String outputFile = archivo;

		CsvReader reader = new CsvReader(new FileReader(outputFile));

		System.out.println("Casos a lanzar:");

		int i = 1;

		while(reader.readRecord())
		{
			String nombreCaso = reader.get(1);
			String lanzar = reader.get(2);
			String dependencia = reader.get(3);
			String tipoCaso = reader.get(4);
			
			//S�lo se a�ade si es del tipo de la ejecucion
			if (tipoCaso.equalsIgnoreCase(tipoEjecucion)) {
			
				if(lanzar.equalsIgnoreCase("SI"))
				{
					if(numeroCasosSimultaneos == 1)
					{
						System.out.println(nombreCaso);
						Class prueba = Class.forName(nombreCaso);
						suite.add(prueba);
					}
					else
					{
						// Campo dependencia informado, todos van a hilo 0
						//TODO solo agregar si padre no ha fallado
						if(numeroOcurrencia == 0)
						{
							if(!dependencia.equals(""))
							{
								System.out.println(nombreCaso);
								Class prueba = Class.forName(nombreCaso);
								suite.add(prueba);
							}
						}
						else
						{
							if(dependencia.equals(""))
							{
								if((numeroOcurrencia + i) % (numeroCasosSimultaneos - 1) == 0)
								{
									System.out.println(nombreCaso);
									Class prueba = Class.forName(nombreCaso);
									suite.add(prueba);
								}
								i++;
							}
	
						}
					}
				}
			}
		}
		reader.close();
	}


	@SuppressWarnings({"rawtypes", "unchecked"})
	public static Class<?>[] suiteCocheEscoba() throws Exception
	{
		//TestSuite suite = new TestSuite();
		List<Class> suite = new ArrayList<Class>();
		
		
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String tipoEjecucion = params.getValorPropiedad("tipoEjecucion");
		
		//Si no hay TipoEjecucion, lo de siempre
		if (tipoEjecucion==null || tipoEjecucion.equalsIgnoreCase("")) {

			String pathsLanzamiento[] = Utils.getPathsLanzamiento();
	
			for(String path : pathsLanzamiento)
			{
				addCocheEscobaTestSuite(suite, path);
			}
			
		}
		
		else {
			
			//Todos los csv de la carpeta
			
			File[] pathsLanzamiento = Utils.devuelveArchivosDeUnDirectorio("lanzamiento/", ".csv");
			
			for(File archivo : pathsLanzamiento)
			{
				String ruta = archivo.getAbsolutePath();
				addCocheEscobaTestSuitePorTipo(suite, ruta, tipoEjecucion);
			}
		}
		
		return suite.toArray(new Class<?>[suite.size()]);
	}
	
	@SuppressWarnings({"rawtypes"})
	public static List<Class<?>[]> suiteCocheEscobaDependientesYNoDependientes() throws Exception
	{
		//TestSuite suite = new TestSuite();
		List<Class> suiteDependientes = new ArrayList<Class>();
		List<Class> suiteResto = new ArrayList<Class>();
		
		
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String pathsLanzamiento[] = Utils.getPathsLanzamiento();
	
		for(String path : pathsLanzamiento)
			addCocheEscobaGridTestSuite(suiteDependientes, suiteResto, path);
		
		Class<?>[] arrayDependientes = suiteDependientes.toArray(new Class<?>[suiteDependientes.size()]);
		Class<?>[] arrayResto = suiteResto.toArray(new Class<?>[suiteResto.size()]);
		
		List<Class<?>[]> listaDeListasDeCasos = new ArrayList<Class<?>[]>();
		listaDeListasDeCasos.add(arrayDependientes);
		listaDeListasDeCasos.add(arrayResto);
		
		return listaDeListasDeCasos;
	}
	
	@SuppressWarnings({"rawtypes"})
	public static List<Class<?>[]> suiteDependientesYNoDependientes() throws Exception
	{
		//TestSuite suite = new TestSuite();
		List<Class> suiteDependientes = new ArrayList<Class>();
		List<Class> suiteResto = new ArrayList<Class>();
		
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();

		String pathsLanzamiento[] = Utils.getPathsLanzamiento();
	
		for(String path : pathsLanzamiento)
			suiteGrid(suiteDependientes, suiteResto, path);
		
		Class<?>[] arrayDependientes = suiteDependientes.toArray(new Class<?>[suiteDependientes.size()]);
		Class<?>[] arrayResto = suiteResto.toArray(new Class<?>[suiteResto.size()]);
		
		List<Class<?>[]> listaDeListasDeCasos = new ArrayList<Class<?>[]>();
		listaDeListasDeCasos.add(arrayDependientes);
		listaDeListasDeCasos.add(arrayResto);
		
		return listaDeListasDeCasos;
	}
	
	@SuppressWarnings("rawtypes")
	private static void addCocheEscobaGridTestSuite(final List<Class> suiteDependientes, final List<Class> suiteResto, final String outputFile) throws Exception
	{

		CsvReader reader = new CsvReader(new FileReader(outputFile));

		System.out.println("Casos a RE-lanzar:");


		while(reader.readRecord())
		{
			String nombreCaso = reader.get(1);
			String testID = reader.get(0);
			String lanzar = reader.get(2);
			String dependeDe = reader.get(3);

			// Los de instalaci�n no los repetimos.
			if(!nombreCaso.startsWith("instalacion."))
			{


				// Si se tiene que lanzar, pero depende de uno previo, a�adimos �ste y del que depende
				if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(testID).equalsIgnoreCase("ERROR") && !dependeDe.equalsIgnoreCase(""))
				{

					if(!dependeDe.equalsIgnoreCase(testID))
					{
						//String casoPadre = Utils.devuelveCasoDelQueDepende(dependeDe);
						String casoPadre = Utils.nombreCaso(dependeDe);

						// S�lo a�adimos si no estaba ya
						if(!listaCasosDependientesARELanzar.contains(casoPadre))
						{
							listaCasosDependientesARELanzar.add(casoPadre);
							Class pruebaPadre = Class.forName(casoPadre);
							suiteDependientes.add(pruebaPadre);
							System.out.println("Caso Padre: " + casoPadre);
						}

					}

					if(!listaCasosDependientesARELanzar.contains(nombreCaso))
					{
						listaCasosDependientesARELanzar.add(nombreCaso);
						Class prueba = Class.forName(nombreCaso);
						suiteDependientes.add(prueba);
						System.out.println("Caso Hijo: " + nombreCaso);
					}


				}

				// Errores sin dependencia
				else if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(testID).equalsIgnoreCase("ERROR"))
				{
					if(!listaCasosRestoARELanzar.contains(nombreCaso))
					{
						listaCasosRestoARELanzar.add(nombreCaso);
						System.out.println(nombreCaso);
						Class prueba = Class.forName(nombreCaso);
						suiteResto.add(prueba);
					}

				}

				// NO EJECUTADOS que dependen de otro que haya fallado
				else if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(testID).equalsIgnoreCase("NO EJECUTADO") && !dependeDe.equalsIgnoreCase(""))
				{
					// Miramos si el Padre FALL�
					String casoPadre = Utils.devuelveCodigoCasoDelQueDepende(dependeDe);
					if(Utils.estadoCaso(casoPadre).equals("ERROR"))
					{
						if(!listaCasosDependientesARELanzar.contains(nombreCaso))
						{
							listaCasosDependientesARELanzar.add(nombreCaso);
							System.out.println(nombreCaso);
							Class prueba = Class.forName(nombreCaso);
							suiteResto.add(prueba);
						}
					}
				}
			}
		}

		reader.close();
	}
	
	@SuppressWarnings("rawtypes")
	private static void suiteGrid(final List<Class> suiteDependientes, final List<Class> suiteResto, final String outputFile) throws Exception
	{
		
		CsvReader reader = new CsvReader(new FileReader(outputFile));

		
		while(reader.readRecord())
		{
			String nombreCaso = reader.get(1);
			String lanzar = reader.get(2);
			String dependeDe = reader.get(3);

			// Casos Dependientes
			if(lanzar.equalsIgnoreCase("SI") && !dependeDe.equalsIgnoreCase(""))
			{

				if(!listaCasosDependientes.contains(nombreCaso))
				{
					listaCasosDependientes.add(nombreCaso);
					System.out.println(nombreCaso);
					Class prueba = Class.forName(nombreCaso);
					suiteDependientes.add(prueba);
				}


			}

			// Casos sin dependencia
			else if(lanzar.equalsIgnoreCase("SI"))
			{
				if(!listaCasosResto.contains(nombreCaso))
				{
					listaCasosResto.add(nombreCaso);
					System.out.println(nombreCaso);
					Class prueba = Class.forName(nombreCaso);
					suiteResto.add(prueba);
				}

			}
			
		}

		reader.close();
	}


	@SuppressWarnings("rawtypes")
	private static void addCocheEscobaTestSuite(final List<Class> suite, final String outputFile) throws Exception
	{

		CsvReader reader = new CsvReader(new FileReader(outputFile));

		System.out.println("Casos a RE-lanzar:");


		while(reader.readRecord())
		{
			String nombreCaso = reader.get(1);
			String testID = reader.get(0);
			String lanzar = reader.get(2);
			String dependeDe = reader.get(3);

			// Los de instalaci�n no los repetimos.
			if(!nombreCaso.startsWith("instalacion."))
			{

				// Se podr�a marcar que han sido ejecutados por segunda vez para que el informe lo tuviera
				// en cuenta


				// Si se tiene que lanzar, pero depende de uno previo, a�adimos �ste y del que depende
				if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(testID).equalsIgnoreCase("ERROR") && !dependeDe.equalsIgnoreCase(""))
				{

					if(!dependeDe.equalsIgnoreCase(testID))
					{
						//String casoPadre = Utils.devuelveCasoDelQueDepende(dependeDe);
						String casoPadre = Utils.nombreCaso(dependeDe);

						// S�lo a�adimos si no estaba ya
						if(!listaCasosARELanzar.contains(casoPadre))
						{
							listaCasosARELanzar.add(casoPadre);
							Class pruebaPadre = Class.forName(casoPadre);
							suite.add(pruebaPadre);
							System.out.println("Caso Padre: " + casoPadre);
						}

					}

					if(!listaCasosARELanzar.contains(nombreCaso))
					{
						listaCasosARELanzar.add(nombreCaso);
						Class prueba = Class.forName(nombreCaso);
						suite.add(prueba);
						System.out.println("Caso Hijo: " + nombreCaso);
					}


				}

				// Errores sin dependencia
				else if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(testID).equalsIgnoreCase("ERROR"))
				{
					if(!listaCasosARELanzar.contains(nombreCaso))
					{
						listaCasosARELanzar.add(nombreCaso);
						System.out.println(nombreCaso);
						Class prueba = Class.forName(nombreCaso);
						suite.add(prueba);
					}

				}

				// NO EJECUTADOS que dependen de otro que haya fallado
				else if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(testID).equalsIgnoreCase("NO EJECUTADO") && !dependeDe.equalsIgnoreCase(""))
				{
					// Miramos si el Padre FALL�
					String casoPadre = Utils.devuelveCodigoCasoDelQueDepende(dependeDe);
					if(Utils.estadoCaso(casoPadre).equals("ERROR"))
					{
						if(!listaCasosARELanzar.contains(nombreCaso))
						{
							listaCasosARELanzar.add(nombreCaso);
							System.out.println(nombreCaso);
							Class prueba = Class.forName(nombreCaso);
							suite.add(prueba);
						}
					}
				}
			}
		}

		reader.close();
	}
	
	@SuppressWarnings("rawtypes")
	private static void addCocheEscobaTestSuitePorTipo(final List<Class> suite, final String outputFile, String tipoEjecucion) throws Exception
	{

		CsvReader reader = new CsvReader(new FileReader(outputFile));

		System.out.println("Casos a RE-lanzar:");


		while(reader.readRecord())
		{
			String nombreCaso = reader.get(1);
			String testID = reader.get(0);
			String lanzar = reader.get(2);
			String dependeDe = reader.get(3);
			String tipoCaso = reader.get(4);

			// Los de instalaci�n no los repetimos.
			if(!nombreCaso.startsWith("instalacion."))
			{

				// Se podr�a marcar que han sido ejecutados por segunda vez para que el informe lo tuviera
				// en cuenta

				if (tipoCaso.equalsIgnoreCase(tipoEjecucion)) {

				// Si se tiene que lanzar, pero depende de uno previo, a�adimos �ste y del que depende
					if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(testID).equalsIgnoreCase("ERROR") && !dependeDe.equalsIgnoreCase(""))
					{
	
						if(!dependeDe.equalsIgnoreCase(testID))
						{
							String casoPadre = Utils.devuelveCasoDelQueDepende(dependeDe);
	
							// S�lo a�adimos si no estaba ya
							if(!listaCasosARELanzar.contains(casoPadre))
							{
								listaCasosARELanzar.add(casoPadre);
								Class pruebaPadre = Class.forName(casoPadre);
								suite.add(pruebaPadre);
								System.out.println("Caso Padre: " + casoPadre);
							}
	
						}
	
						if(!listaCasosARELanzar.contains(nombreCaso))
						{
							listaCasosARELanzar.add(nombreCaso);
							Class prueba = Class.forName(nombreCaso);
							suite.add(prueba);
							System.out.println("Caso Hijo: " + nombreCaso);
						}
	
	
					}
	
					// Errores sin dependencia
					else if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(testID).equalsIgnoreCase("ERROR"))
					{
						if(!listaCasosARELanzar.contains(nombreCaso))
						{
							listaCasosARELanzar.add(nombreCaso);
							System.out.println(nombreCaso);
							Class prueba = Class.forName(nombreCaso);
							suite.add(prueba);
						}
	
					}
	
					// NO EJECUTADOS que dependen de otro que haya fallado
					else if(lanzar.equalsIgnoreCase("SI") && Utils.estadoCaso(testID).equalsIgnoreCase("NO EJECUTADO") && !dependeDe.equalsIgnoreCase(""))
					{
						// Miramos si el Padre FALL�
						String casoPadre = Utils.devuelveCodigoCasoDelQueDepende(dependeDe);
						if(Utils.estadoCaso(casoPadre).equals("ERROR"))
						{
							if(!listaCasosARELanzar.contains(nombreCaso))
							{
								listaCasosARELanzar.add(nombreCaso);
								System.out.println(nombreCaso);
								Class prueba = Class.forName(nombreCaso);
								suite.add(prueba);
							}
						}
					}
				}
			}
		}

		reader.close();
	}


	@SuppressWarnings({"unchecked", "rawtypes"})
	public static void main(String[] args) throws Exception
	{

		// Cargamos las propiedades del properties
		TestProperties params = new TestProperties(Utils.getProperties());
		params.cargarPropiedades();
		String rutaResultados = params.getValorPropiedad("rutaResultados");
		String ejecutarREST = params.getValorPropiedad("serviciosREST");
		String pais = params.getValorPropiedad("pais");
		int numeroCasosSimultaneos = Integer.parseInt(params.getValorPropiedad("numeroEjecucionesSimultaneas"));

		// Si nos llega como argumento EliminarUsuarios ejecutamos esa utilidad
		if(args.length == 1 && args[0].equals("EliminarUsuarios"))
		{
			Class utilidad = Class.forName("utils." + args[0]);
			junit.textui.TestRunner.run(utilidad);
		}

		else if(args.length == 1 && args[0].equals("CrearFicheroLanzamiento"))
		{
			Class utilidad = Class.forName("utils." + args[0]);
			junit.textui.TestRunner.run(utilidad);
		}
		
		else if(args.length == 1 && args[0].equals("CrearFicheroLanzamientoSinMail"))
		{
			Class utilidad = Class.forName("utils." + args[0]);
			junit.textui.TestRunner.run(utilidad);
		}
		
		else if(args.length == 1 && args[0].equals("CrearFicheroRelanzamientoSimultaneo"))
		{
			Class utilidad = Class.forName("utils." + args[0]);
			junit.textui.TestRunner.run(utilidad);
		}
		
		else if (args.length == 1 && args[0].equals("CocheEscobaGrid")) 
		{
			cocheEscobaGrid(args);
		}

		// Servicios REST
		else if(args.length == 1 && args[0].equals("REST"))
		{


			String entorno = params.getValorPropiedad("entorno");

			if(entorno.equalsIgnoreCase("Produccion"))
			{
				System.out.println("La prueba de los servicios REST s�lo aplica en Entornos de Desarrollo e Integrado");
			}

			else
			{

				if(ejecutarREST.equalsIgnoreCase("SI"))
				{

					Class utilidad = Class.forName("utils.RestClientMejorado");
					Class datosRest = Class.forName("utils.DatosParaRest");
					Class subirCSV = Class.forName("utils.REST_ActualizarCSVResultados");
					Class actualizarC204Rest = Class.forName("utils.REST_ActualizarC204");

					// Escribimos informaci�n de traza
					System.out.println("Empieza la ejecuci�n de los servicios REST...");

					java.util.Date hoy = new java.util.Date();
					SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
					String horaInicio = formatter2.format(hoy);

					TestSuite suiteRest = new TestSuite();

					suiteRest.addTestSuite(datosRest);
					suiteRest.addTestSuite(utilidad);

					// Ejecutamos la suite de REST
					junit.textui.TestRunner.run(suiteRest);

					java.util.Date fin = new java.util.Date();
					String horaFin = formatter2.format(fin);
					UtilsRest.CSVToTableRest(hoy, horaInicio, horaFin, null);

					// Actualizar C204
					suiteRest = new TestSuite();
					suiteRest.addTestSuite(actualizarC204Rest);
					junit.textui.TestRunner.run(suiteRest);

					suiteRest = new TestSuite();
					suiteRest.addTestSuite(subirCSV);
					junit.textui.TestRunner.run(suiteRest);

				}
			}
		}

		else if(args.length == 1 && args[0].contains("REST"))
		{


			String entorno = params.getValorPropiedad("entorno");

			if(entorno.equalsIgnoreCase("Produccion"))
			{
				System.out.println("La prueba de los servicios REST s�lo aplica en Entornos de Desarrollo e Integrado");
			}

			else
			{

				if(ejecutarREST.equalsIgnoreCase("SI"))
				{

					Class utilidad = Class.forName("utils.RestClientMejorado");

					Class datosRest = null;

					if(args[0].equals("REST1"))
						datosRest = Class.forName("utils.DatosParaRest1");
					else if(args[0].equals("REST2"))
						datosRest = Class.forName("utils.DatosParaRest2");
					else if(args[0].equals("REST3"))
						datosRest = Class.forName("utils.DatosParaRest3");
					else if(args[0].equals("REST4"))
						datosRest = Class.forName("utils.DatosParaRest4");

					// Escribimos informaci�n de traza
					System.out.println("Empieza la ejecuci�n de los servicios REST...");

					if(args.length == 0){
						//junit.textui.TestRunner.run(suite(0, 1));
						JUnitCore.runClasses(suite(0, 1));
					}
					else
					{
						System.out.println("REST Hilo: " + args[0]);
						Utils.crearFicheroRunning(args[0]);

						TestSuite suiteRest = new TestSuite();
						suiteRest.addTestSuite(datosRest);
						suiteRest.addTestSuite(utilidad);

						// Ejecutamos la suite de REST
						junit.textui.TestRunner.run(suiteRest);
					}


					if(args.length > 0)
						Utils.borrarFicheroRunning(args[0]);

				}
			}
		}

		else if(args.length == 1 && args[0].equals("SubirEjecucionREST"))
		{

			Class utilidad = Class.forName("utils.RestClientMejorado");

			java.util.Date hoy = new java.util.Date();
			SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
			String horaInicio = formatter2.format(hoy);

			TestSuite suiteRest = new TestSuite();

			suiteRest.addTestSuite(utilidad);

			// Ejecutamos la suite de REST
			junit.textui.TestRunner.run(suiteRest);

			java.util.Date fin = new java.util.Date();
			String horaFin = formatter2.format(fin);
			UtilsRest.CSVToTableRest(hoy, horaInicio, horaFin, null);

			Class subirCSV = Class.forName("utils.REST_ActualizarCSVResultados");
			Class actualizarC204Rest = Class.forName("utils.REST_ActualizarC204");

			// Actualizar C204
			suiteRest = new TestSuite();
			suiteRest.addTestSuite(actualizarC204Rest);
			junit.textui.TestRunner.run(suiteRest);

			suiteRest = new TestSuite();
			suiteRest.addTestSuite(subirCSV);
			junit.textui.TestRunner.run(suiteRest);

		}

		else if(args.length == 1 && args[0].equals("ActualizarCSVResultados"))
		{
			String SubeGoogleDrive = params.getValorPropiedad("actualizarGoogleDrive");
			if(SubeGoogleDrive.equalsIgnoreCase("SI"))
			{
				Class utilidad = Class.forName("utils." + args[0]);
				junit.textui.TestRunner.run(utilidad);
			}
		}
		
		else if(args.length == 1 && args[0].equals("SeleccionarUUAALanzamiento"))
		{
			
				SeleccionarUUAALanzamiento lanzamientoGlobal = new SeleccionarUUAALanzamiento();
				lanzamientoGlobal.main();
			
		}
		
		else if(args.length == 1 && args[0].equals("SeleccionarUUAALanzamientoLatam"))
		{
			
			SeleccionarUUAALanzamientoLatam lanzamientoGlobal = new SeleccionarUUAALanzamientoLatam();
			lanzamientoGlobal.main();
			
		}
		
		else if(args.length == 1 && args[0].equals("ActualizarCSVGlobal"))
		{

				Class utilidad = Class.forName("utils." + args[0]);
				junit.textui.TestRunner.run(utilidad);
			
		}
		
		else if(args.length == 1 && args[0].startsWith("UTILIDAD_"))
		{
			//Para llamar cualquier utilidad en formato: UTILIDAD_PAQUETE.NOMBRECLASE
			String nombreClase = args[0].substring(args[0].indexOf("_") + 1);
			Class utilidad = Class.forName(nombreClase);
			junit.textui.TestRunner.run(utilidad);
			
		}

		else if(args.length == 1 && args[0].equals("ObtenerJson"))
		{
			Class utilidad = Class.forName("utils." + args[0]);
			junit.textui.TestRunner.run(utilidad);
		}

		else if(args.length == 1 && args[0].equals("ActualizarC204"))
		{
			String generarFichero = params.getValorPropiedad("generarFichero");
			if(generarFichero.equalsIgnoreCase("SI"))
			{
				Class utilidad = Class.forName("utils." + args[0]);
				junit.textui.TestRunner.run(utilidad);
			}
		}

		else if(args.length == 1 && args[0].equals("EliminarAlertasCorreos"))
		{
			Class utilidad = Class.forName("utils." + args[0]);
			junit.textui.TestRunner.run(utilidad);
		}

		else if(args.length == 1 && args[0].equals("MailSender"))
		{
			// Cogemos la propiedad de si hay que reejecutar tras la ejecuci�n regular
			String enviaMail = params.getValorPropiedad("enviaMail");
			if(enviaMail.equalsIgnoreCase("SI"))
			{
				Class utilidad = Class.forName("utils." + args[0]);
				junit.textui.TestRunner.run(utilidad);
			}
		}
		
		else if(args.length == 1 && args[0].equals("MailSenderParams"))
		{
			// Cogemos la propiedad de si hay que reejecutar tras la ejecuci�n regular
			String enviaMail = params.getValorPropiedad("enviaMail");
			if(enviaMail.equalsIgnoreCase("SI"))
			{
				Class utilidad = Class.forName("utils." + args[0]);
				junit.textui.TestRunner.run(utilidad);
			}
		}

		else if(args.length > 0 && args[0].equals("CocheEscoba"))
		{
			Utils.crearFicheroTemporal("resources//fichero.cocheEscoba", "");

//			FileOutputStream fichero = new FileOutputStream("results/CocheEscoba.html", true);
//			PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);
//
//			System.setOut(ficheroSalida);
//			System.setErr(ficheroSalida);


			// Cogemos la propiedad de si hay que reejecutar tras la ejecuci�n regular
			String reEjecutarFallidos = params.getValorPropiedad("reEjecutarFallidos");

			// Ejecutamos si adem�s de pasarle el par�metro cocheEscoba, est� en el fichero properties
			// O bien forzamos desde fuera el bat con SI
			if(reEjecutarFallidos.equalsIgnoreCase("SI") || (args.length > 1 && args[1].equals("SI")))
			{
				
				//Eliminamos los running que pudiera haber
				Utils.borrarTodosFicherosConExtension("resources/",".running");

				// Escribimos informaci�n de traza
				System.out.println("Empieza la RE-Ejecuci�n de los casos fallidos...");

				//TODO
				// Aqu� hay que meter que los dependientes secuenciales y el resto con Parallel.
				// Para ello tendremos que devolver dos tipos de suites
				JUnitCore.runClasses(suiteCocheEscoba());
				
				//junit.textui.TestRunner.run(suiteCocheEscoba());

			}
			
			//C204

			String generarFichero = params.getValorPropiedad("generarFichero");
			if(generarFichero.equalsIgnoreCase("SI"))
			{
				Class utilidad = Class.forName("utils.ActualizarC204");
				junit.textui.TestRunner.run(utilidad);
			}
			
			Utils.crearInformeHTML(listaCasosARELanzar);
			
			Utils.borrarFicheroTemporal("resources//fichero.cocheEscoba");
			
			
		}
		
		else if (args.length > 0 && args[0].equals("EsperarAlCocheEscobaYRunning")) {
			
			Thread.sleep(10000);
			
			File rutaActual = new File (System.getProperty("user.dir"));
			
			System.out.println("Ruta donde vamos a buscar los running: " + rutaActual.getParentFile().getAbsolutePath());
			
			Collection filesEscoba = FileUtils.listFiles(rutaActual.getParentFile(), new RegexFileFilter("^(.*?\\.cocheEscoba)"), DirectoryFileFilter.DIRECTORY);
			Collection filesRunning = FileUtils.listFiles(rutaActual.getParentFile(), new RegexFileFilter("^(.*?\\.running)"), DirectoryFileFilter.DIRECTORY);

			//Esperamos x minutos
			String timeOutEjecucion = "60";

			int timeOutGlobal = Integer.parseInt(timeOutEjecucion) * 6;
			
			int contador = 1;
			
			boolean hayCochesEscoba = filesEscoba.size()>0;
			boolean hayRunnings = filesRunning.size()>0;
			System.out.println("timeOutGlobal="+timeOutGlobal);
			
			while (contador < timeOutGlobal && (hayCochesEscoba||hayRunnings)) {
				
				
				hayCochesEscoba = FileUtils.listFiles(rutaActual.getParentFile(), new RegexFileFilter("^(.*?\\.cocheEscoba)"), DirectoryFileFilter.DIRECTORY).size()>0;
				hayRunnings = FileUtils.listFiles(rutaActual.getParentFile(), new RegexFileFilter("^(.*?\\.running)"), DirectoryFileFilter.DIRECTORY).size()>0;
				
				System.out.println("HayCocheEscoba="+hayCochesEscoba);
				System.out.println("hayRunnings="+hayRunnings);
				System.out.println("contador="+contador);
				
				
				if (hayCochesEscoba || hayRunnings) 
					Thread.sleep(10000);

				contador = contador + 1;

			}
		}
		
		else if (args.length > 0 && args[0].equals("EsperarAlCocheEscoba")) {
			
			File rutaActual = new File (System.getProperty("user.dir"));
			Collection files = FileUtils.listFiles(rutaActual.getParentFile(), new RegexFileFilter("^(.*?\\.cocheEscoba)"), DirectoryFileFilter.DIRECTORY);
			
			//Esperamos x minutos
			String timeOutEjecucion = "60";

			int timeOutGlobal = Integer.parseInt(timeOutEjecucion) * 6;
			
			int contador = 1;
			
			boolean hayCochesEscoba = files.size()>0;
			
			while (contador < timeOutGlobal && hayCochesEscoba) {
				
				if (FileUtils.listFiles(rutaActual.getParentFile(), new RegexFileFilter("^(.*?\\.cocheEscoba)"), DirectoryFileFilter.DIRECTORY).size()==0) 
					hayCochesEscoba = false;
				
				else
					Thread.sleep(10000);

				contador = contador + 1;
				
				
			}
		}
		
		else if(args.length > 0 && args[0].equals("EsperaYCocheEscoba"))
		{

			//Esperamos x minutos
			String timeOutEjecucion = params.getValorPropiedad("timeOutEjecucion");
			
			if (timeOutEjecucion==null || timeOutEjecucion.equalsIgnoreCase("")) 
				timeOutEjecucion = "300";
			
			int timeOutGlobal = Integer.parseInt(timeOutEjecucion) * 6;
			
			int contador = 1;
			
			boolean hayRunning = Utils.numeroFicherosEnRutaConUnPatron("resources/",".running")>0;
			
			while (contador < timeOutGlobal && hayRunning) {
				
				if (Utils.numeroFicherosEnRutaConUnPatron("resources/",".running")==0) 
					hayRunning = false;
				
				else
					Thread.sleep(10000);

				contador = contador + 1;
				
				
			}

			cocheEscoba(args);

			// Si nos ped�an el resultado en HTML, lo creamos
			Utils.crearInformeHTML(listaCasosARELanzar);
			

		}
		
		else if(args.length > 0 && args[0].equals("CocheEscoba_Actualizar_Mail"))
		{

			cocheEscoba(args);
			
			actualizarCSVResultados();
			
			enviaMail();
			
		}
		
		else if(args.length > 0 && args[0].equals("Grid_SoloLanzar"))
		{
			Utils.crearFicheroRunning("Grid");
			JUnitCore.runClasses(new ParallelComputer(true, true), suite(0, 1));
			Utils.borrarFicheroRunning("Grid");
			cocheEscobaGrid(args);
			crearC204();	
			
		}
		
		else if(args.length > 0 && args[0].equals("SoloEspera"))
		{
			
			
			
			//Esperamos x minutos
			String timeOutEjecucion = params.getValorPropiedad("timeOutEjecucion");
			
			if (timeOutEjecucion==null || timeOutEjecucion.equalsIgnoreCase("")) 
				timeOutEjecucion = "300";
			
			int timeOutGlobal = Integer.parseInt(timeOutEjecucion) * 6;
			
			int contador = 1;
			
			boolean hayRunning = Utils.numeroFicherosEnRutaConUnPatron("resources/",".running")>0;
			
			while (contador < timeOutGlobal && hayRunning) {
				
				if (Utils.numeroFicherosEnRutaConUnPatron("resources/",".running")==0) 
					hayRunning = false;
				
				else
					Thread.sleep(10000);

				contador = contador + 1;
				
				
			}

			
			
		}
		
		 else if(args.length > 0 && args[0].equals("GeneraInformes")) {
				crearC204();
				Utils.crearInformeHTML(listaCasosARELanzar);
			    actualizarCSVResultados();
		 }
		
        else if(args.length > 0 && args[0].equals("CocheEscobaSimultaneo"))
        {
            
//            FileOutputStream fichero = new FileOutputStream("results/CocheEscobaSimultaneo.html", true);
//            PrintStream ficheroSalida = new PrintStream(new BufferedOutputStream(fichero), true);
//
//            System.setOut(ficheroSalida);
//            System.setErr(ficheroSalida);


            // Cogemos la propiedad de si hay que reejecutar tras la ejecuci�n regular
            String reEjecutarFallidos = params.getValorPropiedad("reEjecutarFallidos");

            // Ejecutamos si adem�s de pasarle el par�metro cocheEscoba, est� en el fichero properties
            // O bien forzamos desde fuera el bat con SI
            if(reEjecutarFallidos.equalsIgnoreCase("SI") || (args.length > 1 && args[1].equals("SI")))
            {
            
                
                // Escribimos informaci�n de traza
                System.out.println("Empieza la RE-Ejecuci�n Simult�nea de los casos fallidos...");

                // Fecha de inicio
                java.util.Date hoy = new java.util.Date();
                SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
                String horaInicio = formatter2.format(hoy);
                
                Utils.crearFicheroRunning(args[1]);
                System.out.println("Hilo: " + args[1]);
//                JUnitCore.runClasses(suiteCocheEscobaSimultaneo());  //OCU                         //4
//                JUnitCore.runClasses(suiteCocheEscobaSimultaneo(Integer.parseInt(args[0]), numeroCasosSimultaneos));
                JUnitCore.runClasses(suiteCocheEscobaSimultaneo(Integer.parseInt(args[1]), numeroCasosSimultaneos));
//                JUnitCore.runClasses(suiteCocheEscobaSimultaneo(0, numeroCasosSimultaneos));
                
                Utils.borrarFicheroRunning(args[1]);
                
                //C204
                String generarFichero = params.getValorPropiedad("generarFichero");
                if(generarFichero.equalsIgnoreCase("SI"))
                {
                    Class utilidad = Class.forName("utils.ActualizarC204");
                    junit.textui.TestRunner.run(utilidad);
                }

                // Si nos ped�an el resultado en HTML, lo creamos
                String formatoResultados = params.getValorPropiedad("formatoResultados");

                java.util.Date fin = new java.util.Date();
                String horaFin = formatter2.format(fin);


                if(formatoResultados.indexOf("html") > -1)
                {
                    // Leemos la hora de inicio si alg�n hilo ya la dej�
                    Utils.crearBloqueo("timeLock", 60);
                    File time = new File("resources/time.txt");
                    if(time.exists())
                    {
                        Scanner scanner = new Scanner(time, "UTF-8");
                        String text = scanner.useDelimiter("\\A").next();
                        horaInicio = text;
                        scanner.close();
                        time.delete();
                    }
                    Utils.liberarBloqueo("timeLock");
                    Utils.CSVToTableNew(hoy, horaInicio, horaFin, listaCasosARELanzar);
                }
            }
            
            
        }


		else
		// Si no tenemos ning�n argumento, ejecuci�n est�ndar con fichero de lanzamiento
		{
			// Si existe el fichero de resultados y la fecha es de hace m�s de tres horas, lo borramos
			File archivoResultados = new File(rutaResultados);

			boolean alreadyExists = archivoResultados.exists();
			Date fecha = new Date(System.currentTimeMillis() - (3 * 60 * 60 * 1000));
			
			String ipHub = params.getValorPropiedad("ipHub");

			if(alreadyExists && fecha.compareTo(new Date(archivoResultados.lastModified())) > 0)
			{
				try
				{
					archivoResultados.delete();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}

			// Escribimos informaci�n de traza
			System.out.println("Empieza la ejecuci�n...");
			
			//Borramos los .lock que pueden dar problemas
			Utils.borrarTodosFicherosConExtension("resources/",".lock");

			// Ejecutamos la suite
			java.util.Date hoy = new java.util.Date();
			SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
			String horaInicio = formatter2.format(hoy);

			if(args.length == 0){
				
				//Si est� puesta la IP del hub, los lanzo todos a la vez, si no, como antes, secuencial
				

				if (ipHub==null || ipHub.equals("")){
					System.out.println("ipHub="+ipHub + " - Lanzamiento normal");
					JUnitCore.runClasses(suite(0, 1));
				}
				else{
					
					System.out.println("ipHub="+ipHub + " - Lanzamiento en Grid");
					
					//Creamos un fichero temporal mientras se est� ejecutando el GRID
					Utils.borrarFicheroRunning("Grid");
					Utils.crearFicheroRunning("Grid");
					
					//Creamos dos suites de casos, NO dependientes y dependientes
					List<Class<?>[]> arrayCasos = suiteDependientesYNoDependientes();
					Class<?>[] dependientes = arrayCasos.get(0);
					Class<?>[] resto = arrayCasos.get(1);
					
					//NO dependientes
					System.out.println("Lanzamos los NO dependientes simult�neamente...");

					JUnitCore.runClasses(new ParallelComputer(true, true), resto);	
					
					//Dependientes
					System.out.println("Lanzamos los dependientes secuencialmente...");
					JUnitCore.runClasses(dependientes);
					
//					JUnitCore.runClasses(new ParallelComputer(true, true), suite(0, 1));

					Utils.borrarFicheroRunning("Grid");
					
					cocheEscobaGrid(args, horaInicio);							
					
					actualizarCSVResultados();
					
					enviaMail();
					
				}
				
			}
			else
			{
				try {
					
					System.out.println("Hilo: " + args[0]);
					Utils.crearFicheroRunning(args[0]);
					JUnitCore.runClasses(suite(Integer.parseInt(args[0]), numeroCasosSimultaneos));
					
				} catch (Exception e) {
					Utils.borrarFicheroRunning(args[0]);
					e.printStackTrace();
					System.exit(0);
				}
			}
			
			
			if (ipHub==null || ipHub.equalsIgnoreCase("")){

				// Si nos ped�an el resultado en HTML, lo creamos
				String formatoResultados = params.getValorPropiedad("formatoResultados");
	
				java.util.Date fin = new java.util.Date();
				String horaFin = formatter2.format(fin);
	
				// Nos guardamos la hora de inicio por si re-ejecutamos
				Utils.crearBloqueo("timeLock",60);
				File time = new File("resources/time.txt");
				FileWriter fileHtm = new FileWriter(time.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fileHtm);
				bw.write(horaInicio);
				bw.close();
				Utils.liberarBloqueo("timeLock");
				
				// Borramos el fichero de running
				if(args.length > 0)
					Utils.borrarFicheroRunning(args[0]);
	
	
				// Si se ped�a el formato HTML, se genera
				if(formatoResultados.indexOf("html") > -1)
				{
					try
					{
						Utils.CSVToTableNew(hoy, horaInicio, horaFin, null);
					}
					catch(FileNotFoundException e)
					{
						// Puede ser que un hilo no tenga nada que ejecutar y har�a el html sin nada
						System.out.println("Error controlado. Hilo que no tiene casos a ejecutar");
					}
					catch(Exception e)
					{
						// Error raro
						e.printStackTrace();
					}
	
				}
			}


		}

	}
}
